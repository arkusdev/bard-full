({
	doLoad: function (cmp) {
        var spinner = cmp.find("spinner");
        $A.util.removeClass(spinner, "slds-hide");

		var campaignId = cmp.get("v.campaignId");
		var type = cmp.get("v.tabId");

        var action = cmp.get("c.fetchCampaignFlightsByType");
        
        action.setParams({
            "campaignId": campaignId,
			"type": type
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (cmp.isValid() && state == "SUCCESS") {
				var flights = response.getReturnValue();
				cmp.set("v.flights", flights);
                
            } else if (state == "ERROR") {
                var errors = action.getError();
                
                if (errors[0] && errors[0].message) {                         
                    console.error(errors[0].message);

					var params = {
                        title: 'Uh Oh!',
                        mode: 'dismissible',
                        message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
                        type: 'error'
                    }
                    
					_apostletech.showToast(params);
                }
            }

			$A.util.addClass(spinner, "slds-hide");
        });
        
        $A.enqueueAction(action);
	}
})