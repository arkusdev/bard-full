({
	cancel: function(cmp, event, helper) {
        cmp.find("confirm-modal").notifyClose();
	},

	doUpdate: function(cmp, event, helper) {
		var campaignId = cmp.get("v.campaignId");
        var memberId = cmp.get("v.memberId");

		var event = $A.get("e.c:CampaignMemberUpdated");

		event.setParams({
			"campaignId": campaignId,
			"memberId": memberId
		});

		event.fire();		

		cmp.find("confirm-modal").notifyClose();
	}
})