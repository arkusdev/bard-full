({
	onInit : function(cmp, event, helper) {
		var alertType = cmp.get("v.alertType");

		var divClass = 'slds-notify slds-notify_alert slds-theme_alert-texture slds-m-top_small slds-m-bottom_xx-small ';
		var spanClass = 'slds-icon_container slds-m-right_x-small slds-m-left_x-small ';
		var spanTitle = '';
		var iconName = '';
	
		switch(alertType) {
			case 'warning':
				divClass += 'slds-theme_warning';
				spanClass += 'slds-icon-utility-warning';
				spanTitle = 'Warning';
				iconName = 'utility:warning';
			break;
			case 'info':
				divClass += 'slds-theme_info';
				spanClass += 'slds-icon-utility-user';
				spanTitle = 'Info';
				iconName = 'utility:user';
			break;
			case 'error':
				divClass += 'slds-theme_error';
				spanClass += 'slds-icon-utility-error';
				spanTitle = 'Error';
				iconName = 'utility:error';
			break;
		}
		
		cmp.set("v.divClass", divClass);	
		cmp.set("v.spanClass", spanClass);
		cmp.set("v.spanTitle", spanTitle);
		cmp.set("v.iconName", iconName);
	}
})