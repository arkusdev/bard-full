({
    handlePaste : function(component, event, helper) {
        var params = event.getParam('arguments').params;
        var name = params.name;
        var tableNumber = name.split('/')[1];
        var cell = name.split('/')[0].replace('input_', '');
        helper.formatCells(params.component, params.value, tableNumber, cell, params.rows, params.columns, params.disabledRows);
    },
    changeToPasted : function(component, event, helper) {
        var params = event.getParam('arguments').params;
        helper.changeToPasted(params.component, true);
    },
    showReminder : function(component, event, helper){
        var params = event.getParam('arguments').params;
        helper.showReminder(params.component, params.header, params.message);
    },
    showErrorToast : function(component, event, helper){
        var params = event.getParam('arguments').params;
        helper.showErrorToast(params.errorMessage);
    }
})