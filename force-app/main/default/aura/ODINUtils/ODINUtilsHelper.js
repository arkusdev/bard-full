({
    formatCells : function(component, xls, tableNumber, cell, tableRows, tableColumns, disabledRows) {
        var self = this;
        var setPasted = false;
        var formattedValues = '';
        var allCells = xls.split(/\t/);
        if (allCells.length > 1){
            allCells.forEach(function(cell, index) {
                if (cell.split(/\b(\s)/).filter(c => c != ' ').length == 1)
                    formattedValues += accounting.formatMoney(cell) + (index == allCells.length - 1 ? '' : '\t');
                else{
                    var cells = cell.split(/\b(\s)/).filter(c => c != ' ');
                    cells.forEach(function(otherCell) {
                        var formatMoney = accounting.formatMoney(otherCell);
                        if (formatMoney != '$0.00') 
                            formattedValues += formatMoney + ' ';
                    });
                    formattedValues = formattedValues.substring(0, formattedValues.length - 1) + (index == allCells.length - 1 ? '' : '\t');
                }        
            });
        }else formattedValues = accounting.formatMoney(xls);

        var rows = formattedValues.split(' ');

        var startPosX = parseInt(cell.substring(0, 1));
        var startPosY = parseInt(cell.substring(1));

        var inputs = component.find('validInput');
        var totalRows = rows.length;

        for(var yPos = 0; yPos < totalRows; yPos++){
            if (!setPasted){
                self.changeToPasted(component, false);
                setPasted = true;
            }
            var row = rows[yPos];
            var cells = row.split(/\t/gm);
            var totalCells = cells.length;
            var numberOfCells = startPosY - 1;
            for (var xPos = 0; xPos < totalCells; xPos++){
                if (numberOfCells < tableColumns){
                    var currentRow = (startPosX + yPos - 1) * tableColumns;
                    var currentCell = currentRow + numberOfCells + (((tableRows * tableColumns) - (disabledRows * tableColumns)) * (tableNumber ? tableNumber : 0));
                    if (inputs[currentCell]){
                        var cellValue = cells[xPos].replace(/ /g, '');
                        var currentValueFormated = cellValue == '' ? '' : cellValue;
                        var currentValueNumber = currentValueFormated == '' ? '' : (currentValueFormated == '$NaN.undefined' ? self.showErrorToast($A.get("$Label.c.Max_Integer_Error_Message")) : accounting.unformat(currentValueFormated));
                        var currentValue = isNaN(currentValueNumber) ? '' : currentValueNumber;
                        inputs[currentCell].set('v.value', currentValue);
                    }
                    numberOfCells++;
                }
            }
        }
    },
    changeToPasted : function(component, pasted){
        component.set('v.pasted', pasted);
    },
    showReminder : function(component, header, message){
        var modalBody;
        $A.createComponent("c:customPrompt", {"header" : header, "message" : message},
        function(content, status) {
            if (status === "SUCCESS") {
                modalBody = content;
                component.find('overlayLib').showCustomModal({
                    body: modalBody,
                    showCloseButton: true,
                    cssClass: "mymodal"
                })
                .then(function (overlay) {
                    component._overlay = overlay;
                    setTimeout(function(){
                        //close the popover after 3 seconds
                        if (component._overlay) {
                            component._overlay.close();
                        }
                    }, 3000)
                });
            }
        });
    },
    showErrorToast : function(errorMessage) {
        var errorMessage = !errorMessage ? $A.get("$Label.c.GF_GMA_Error_Message") : errorMessage;
        var toastEvent = $A.get("e.force:showToast");
        const emailExpression = /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/;
        var matches = emailExpression.exec(errorMessage);
        var email = matches && matches.length > 0 ? matches[0] : '';
        if (email.length > 0) errorMessage = errorMessage.replace(email, '{0}');
        toastEvent.setParams({
            "title": "Error",
            "message": errorMessage,
            "messageTemplate": errorMessage,
            "messageTemplateData": email != '' ? [
                {
                    url: 'mailto:' + email,
                    label: email
                }
            ] : [],
            "type": "error"

        });
        toastEvent.fire();
    }
})