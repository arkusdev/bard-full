({
    onloadData : function(component, event, helper) {
        var self = this;
        self.regionWithFinancials = new Map();
        var action = component.get("c.GetRegionOptions");
        action.setParams({projectId: component.get('v.projectId')});
        action.setCallback(this, function(response) {
            var regionOptions = [{ "value": "", "label": "--None--" }]; 

            var values = response.getReturnValue();
            for (var key in values){
                if (component.get("v.selectedValue") == key) {
                    regionOptions.push({ "value": key, "label": values[key], selected: true });    
                } else {
                    regionOptions.push({ "value": key, "label":  values[key]});
                }
                self.regionWithFinancials.set(key, key != values[key]);
            }
            component.set("v.picklistValues", regionOptions);
        })
        $A.enqueueAction(action);
    },
    handleChange : function(component, event, helper) {
        var self = this;
        var compEvent = component.getEvent("regionSelected");
        var selectedValue = component.get("v.selectedValue");
        var hasFinancials = self.regionWithFinancials.get(selectedValue);
        compEvent.setParams({
            "region": selectedValue,
            "hasFinancials": hasFinancials
        });
        compEvent.fire();

    },
    close : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    regionWithFinancials : {}
})