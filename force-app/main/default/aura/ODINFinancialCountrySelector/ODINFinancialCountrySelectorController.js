({
    doInit : function(component, event, helper) {
        helper.onloadData(component,event,helper);
    },
    handleChange : function(component, event, helper) {
        helper.handleChange(component, event, helper);
    },
    close : function(component, event, helper) {
        helper.close(component, event, helper);
    }
})