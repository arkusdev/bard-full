({
    onInit : function(component) {
        var action = component.get("c.getFinancialInfoByProjectAndRegion");
        action.setParams({  "recordId" : component.get("v.recordId"),
                            "region" : component.get("v.selectedRegion")
                        });         

        action.setCallback(this, function(response) {
            var financialInfo = response.getReturnValue();

            component.set('v.financials', financialInfo.financialInfoWrapper);
            component.set('v.questions', financialInfo.projectQuestions);
            component.set('v.selectedInterest', financialInfo.interest);
        })
        $A.enqueueAction(action);
    }
})