({
    onInit : function(component, event, helper) {
        helper.onInit(component);
    },
    handleDoneClick : function(component) {
        component.set('v.closeModal', true);
        component.find("overlayLib").notifyClose();
    },
    handleBackClick : function(component) {
        component.find("overlayLib").notifyClose();
    }
})