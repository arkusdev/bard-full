({
	serverCallIssued: function(cmp, evt, h) {
		let active = evt.getParam('active');
        let serverCallsRunning = Number.parseInt(cmp.get('v.serverCallsRunning'));

        if (active) serverCallsRunning++;
        else serverCallsRunning--;

        cmp.set('v.isBusy', serverCallsRunning > 0);
        cmp.set('v.serverCallsRunning', serverCallsRunning);
	}
})