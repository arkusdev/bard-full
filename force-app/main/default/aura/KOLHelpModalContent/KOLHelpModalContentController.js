({
	doInit : function(cmp, event, helper) {
		var helpMap = new Map();

		helpMap.set("Provider Name", "Searches for any matching part of the Contact's first and last name.");
		helpMap.set("Facility Type", "Searches the Contact's Primary Account for a match. Hospital includes VA Hospitals. OBL includes Surgery Center, Lab, and Access Centers.");
		helpMap.set("Society Memberships", "Searches any matching part of the Contact's Society Registrations.");
		helpMap.set("BDPI Division", "Searches all Divisions related to a Contact for a match on the Division field.");
		helpMap.set("BDPI Product Category", "Searches all Divisions related to a Contact for a match on the Product Category field. This field is controlled by the Division field, so not all results will show if you have a Division selected in the search.");
		helpMap.set("Specialty", "Searches for a match on the Contact Specialty field.");
		helpMap.set("Area of Focus", "Searches for a match on the Contact Area of Focus field.");
		helpMap.set("Active BD Contract", "Searches the Contracts related to a Contact by the Contract End Date field.");
		helpMap.set("Flagged as KOL", "Used to determine if the 'Is KOL' checkbox on the Contact record is checked or unchecked.");
		helpMap.set("Contract Services", "Searches the Contracts related to a Contact for a match on the Contract Services field.");
		helpMap.set("Region by Time Zone", "Searches a Contact's Licenses by state, with states grouped together by Time Zone.");
		helpMap.set("Licensed State", "Searches a Contact's Licenses by state.");

		var items = [];
		
		helpMap.forEach(function(value, key) {
			items.push({
				key: key,
				value: value
			});		
		});

		cmp.set("v.helpItems", items);
	}
})