({
	selectRecord: function(cmp, event, helper){      
		var record = cmp.get("v.record");

		var ev = cmp.getEvent("recordSelected");

		ev.setParams({
			"record": record 
		});  

		ev.fire();
	}
})