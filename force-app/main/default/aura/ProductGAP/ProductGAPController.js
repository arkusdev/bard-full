({
	init: function(cmp, evt, h) {
        h.util = cmp.find('util');
        //h.serverCallEvt = $A.get('e.c:PDSe_ServerCallIssued');
	},
    changeUser: function (cmp, evt, h) {
        cmp.set('v.user', evt.getParam('userId'));
        h.getCalculations(cmp);
    },
    changePeriod: function (cmp, evt, h) {
        cmp.set('v.period', evt.getParam('period'));
        h.getCalculations(cmp);
    },
    distributorSwitched: function (cmp, evt, h) {
        cmp.set('v.distributor', evt.getParam('active'));
        h.getCalculations(cmp);
    },
    viewAsSwitched: function(cmp, evt, h){
        var viewAsGraph = evt.getParam('active');
        cmp.set('v.viewAsGraph', viewAsGraph);
    },
    filter: function(cmp, evt, h) {
        h.util.filter({cmp:cmp, h:h, view:'product'});
        if(cmp.get('v.viewAsGraph')){
            cmp.set('v.isBusy', true);
        }
        setTimeout(function(){ 
            h.setTableDataAfterFilter(cmp, evt, cmp.get('v.tableData'));
            cmp.set('v.isBusy', false);
        }, 2000);
    },
    sort: function (cmp, evt, h) {
        var ds = evt.currentTarget.dataset;
        if (!ds || !ds.sort) return;

        h.sort(cmp, ds.sort);
    },
    filterByAccount: function(cmp, evt, h) {
        cmp.set('v.accountID', evt.getParam('accountID'));
        h.getCalculations(cmp);
    },
    cancelAccountFilter: function(cmp, evt, h) {
        cmp.set('v.accountID', undefined);
        h.getCalculations(cmp);
    },
    nextPage: function(cmp, evt, h) {
        h.nextPage(cmp);
    },
    prevPage: function(cmp, evt, h) {
        h.prevPage(cmp);
    },
    firstPage: function(cmp, evt, h) {
        h.firstPage(cmp);
    },
    lastPage: function(cmp, evt, h) {
        h.lastPage(cmp);
    }
})