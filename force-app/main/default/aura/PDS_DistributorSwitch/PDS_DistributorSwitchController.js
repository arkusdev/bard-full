({
    init: function(cmp, evt, h) {
        cmp.find('switchValue').set('v.checked', true);
    },
	switchActive: function(cmp, evt, h) {
        // This is to fix a bug in the base lightning input where
        // the onclick event is fired twice
        evt.preventDefault();
        h.activateSwitch(cmp);
	}
})