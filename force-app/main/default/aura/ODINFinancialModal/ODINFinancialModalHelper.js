({
    onloadData : function(component,event,helper) {
        component.set("v.componentId", "FCS");         
    },

    regionSelected : function(component, event, helper) {
        component.set("v.selectedRegion", event.getParam("region"));
        component.set("v.viewButtonDisabled", !event.getParam("hasFinancials"));
    },

    interestSelected : function(component, event, helper) {
        var selectedInterest = event.getParam("interest");
        component.set("v.selectedInterest", selectedInterest);
        component.set('v.noInterestSelected', selectedInterest && selectedInterest.Level__c == '0 = No Interest');
    },

    questionSelected : function(component, event, helper) {
        component.set("v.questions", event.getParam("questions"));
        var answerList = [];
        component.get("v.questions").questions.forEach(element => {
            var answer = element.answer;
            answerList.push({"Question__c" : answer.Question__c, 
                            "Id" : answer.Id, 
                            "Answer__c" : answer.Answer__c,
                            "Region__c" : answer.Region__c ,
                            "Created_From__c": "GMA"
                        });
        })
        component.get("v.questions").requiredQuestions.forEach(element => {
            var answer = element.answer;
            answerList.push({"Question__c" : answer.Question__c, 
                            "Id" : answer.Id, 
                            "Answer__c" : answer.Answer__c,
                            "Region__c" : answer.Region__c,
                            "Created_From__c": "GMA" 
                        });
        })
        component.set("v.answers", answerList);
        
    },

    financialSelected : function(component, event, helper) {
        component.set("v.financials", event.getParam("financials"));
        component.set("v.isValid", event.getParam("isValid"));
        // if (component.get("v.financials").hasFinancials == true && (component.get("v.financials").financialsProjectPerYearList == null || component.get("v.financials").financialsProjectPerYearList.size == 0)) {
        //     helper.handleNextClick(component, event, helper);
        // }
    },

    handleNextClick : function(component, event, helper) {
        var self = this;
        var componentId = component.get("v.componentId");
        
        if (componentId == "FCS") {
            if (component.get("v.selectedRegion") != "") {
                component.set("v.selectedInterest", null);
                component.set("v.componentId", "FIS");
                component.set("v.cancelLabel", "Back"); 
                component.set("v.step", "2"); 
            } else {
                helper.showCountryToast(component, event, helper);
            }

        } else if (componentId == "FIS") {

            if (component.get("v.selectedInterest") && (component.get("v.selectedInterest").Level__c || component.get("v.selectedInterest").Level__c == "")) {
                var noInterestSelected = component.get("v.noInterestSelected");
                if (noInterestSelected){
                    component.set("v.componentId", "FPF");
                    component.set("v.nextLabel", "Save");
                    component.set("v.step", "6"); 
                } else {
                    component.set("v.questions", null);
                    component.set("v.componentId", "FQF");
                    component.set("v.step", "3"); 
                }
            } else {
                helper.showInterestToast(component, event, helper);
            }
        
        } else if (componentId == "FQF") {
            if (helper.isValidQuestions(component, event, helper)) {
                component.set("v.financials", null);
                component.set("v.componentId", "FPF");
                component.set("v.step", "4"); 
            } else {
                helper.showQuestionToast(component, event, helper);
            }
        
        } else if (componentId == "FPF") {
            if (component.get("v.financials") && component.get("v.financials") != null && component.get("v.financials").startYear != null) {
                if (component.get("v.isValid")) {
                    helper.createFinancialInfo(component);
                } else {
                    helper.showProductInvalidToast(component, event, helper);
                }
            } else {
                helper.showProductToast(component, event, helper);
            }
        
        } else if (componentId == "F5YF") {
            if (component.get("v.isValid")) {
                var interest = component.get("v.selectedInterest");
                interest.No_Interest_Reason__c = '';
                interest.No_Interest_Reason_Text__c = '';
                interest.Created_From__c = 'GMA';
                var action = component.get("c.UpdateFinancialInfo");
                action.setParams({  "jsonWrapper" : JSON.stringify(component.get("v.financials")),
                                    "interest" : interest,
                                    "answers" : component.get("v.answers")
                                });
                action.setCallback(this, function(response) {
                    if (response.getState() == 'SUCCESS'){
                        helper.showSuccessToast(component);
                        $A.get("e.force:closeQuickAction").fire();
                    }else{
                        var errors = action.getError();
                        self.util.showErrorToast({ 'errorMessage': errors ? errors[0].message : undefined});
                    }
                })
                $A.enqueueAction(action);
            } else {
                helper.showProductInvalidToast(component, event, helper);
            }
        } else if (componentId == "FNI") {
            //Save here
            var interest = component.get("v.selectedInterest");
            var action = component.get("c.UpdateFinancialInfo");
            action.setParams({  "jsonWrapper" : JSON.stringify(component.get("v.financials")),
                                "interest" : interest,
                                "answers" : component.get("v.answers")
                            });
            action.setCallback(this, function(response) {
                if (response.getState() == 'SUCCESS'){
                    helper.showSuccessToast(component);
                    $A.get("e.force:closeQuickAction").fire();
                }else{
                    var errors = action.getError();
                    console.log(JSON.stringify(errors));
                    self.util.showErrorToast({ 'errorMessage': errors ? errors[0].message : undefined});
                }
            })
            $A.enqueueAction(action);

            component.set("v.cancelLabel", "Back"); 
        }
    },

    handleCancelClick : function(component, event, helper) {
        component.set('v.saveDisabled', false);
        var componentId = component.get("v.componentId");
        if (componentId == "FCS") {
            $A.get("e.force:closeQuickAction").fire();
        } else if (componentId == "FIS") {
            component.set("v.componentId", "FCS");
            component.set("v.cancelLabel", "Cancel"); 
            component.set("v.step", "1"); 
        } else if (componentId == "FQF") {
            var noInterestSelected = component.get("v.noInterestSelected");
            if (noInterestSelected){
                component.set("v.componentId", "FNI");
                component.set("v.step", "6"); 
            }else{
                component.set("v.nextLabel", "Next");
                component.set("v.componentId", "FIS");
                component.set("v.step", "2"); 
            }
        } else if (componentId == "FPF") {
            var noInterestSelected = component.get("v.noInterestSelected");
            if (noInterestSelected){
                component.set("v.componentId", "FIS");
                component.set("v.step", "2");
                component.set("v.nextLabel", "Next");
            }else{
                component.set("v.componentId", "FQF");
                component.set("v.step", "3"); 
            }
        } else if (componentId == "F5YF") {
            // if (component.get("v.financials").hasFinancials  == true) {
            //     component.set("v.componentId", "FQF");
            //     component.set("v.nextLabel", "Next");
            //     component.set("v.step", "3");
            // } else {
                console.log(component.get("v.financials").startYear);
                component.set("v.componentId", "FPF");
                component.set("v.nextLabel", "Next");
                component.set("v.step", "4"); 
            //}
        } else if (componentId == "FNI") {
            component.set("v.nextLabel", "Next");
            component.set("v.componentId", "FIS");
            component.set("v.step", "2");
            component.set("v.noInterestReason", "");
        }
    },

    isValidQuestions : function(component,event,helper) {
        var isValid = true;
        component.get("v.questions").requiredQuestions.forEach(element => {
            if (element.answer.Answer__c == "" || element.answer.Answer__c == null || element.answer.Answer__c.trim() == '') {
                isValid = false;
            }
        });
        return isValid;
    },

    showSuccessToast : function(component) {
        var self = this;
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "The records have been created/updated successfully.",
            "type": "success"

        });
        toastEvent.fire();
        self.util.showReminder({component: component, header: 'Reminder', message: 'Close out all remaining open tasks'});
    },

    showQuestionToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Missing Answers",
            "message": "Please provide answers for all the required questions."
        });
        toastEvent.fire();
    },

    showCountryToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Missing Region",
            "message": "Please select a Region."
        });
        toastEvent.fire();
    },

    showInterestToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Missing Level",
            "message": "Please select an Interest level."
        });
        toastEvent.fire();
    },

    showProductToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Missing Start Year",
            "message": "Please select a Start Year."
        });
        toastEvent.fire();
    },

    showProductInvalidToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Invalid Values",
            "message": "Please correct the invalid values."
        });
        toastEvent.fire();
    },

    createFinancialInfo : function(component){
        var self = this;
        var action = component.get("c.CreateFinancialInfo");
        var financials = component.get("v.financials");
        financials.startYear = component.get('v.firstYear');
        action.setParams({  "recordId" : component.get("v.recordId"),
                            "region" : component.get("v.selectedRegion"),
                            "jsonWrapper" : JSON.stringify(financials)
                        });
        action.setCallback(this, function(response) {
            component.set("v.financials", response.getReturnValue());
            if (component.get('v.noInterestSelected')){
                var interest = component.get("v.selectedInterest");
                var action = component.get("c.UpdateFinancialInfo");
                action.setParams({  "jsonWrapper" : JSON.stringify(component.get("v.financials")),
                                    "interest" : interest,
                                    "answers" : component.get("v.answers")
                                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state == "SUCCESS") {
                        self.showSuccessToast(component);
                        $A.get("e.force:closeQuickAction").fire();
                    }else{
                        var errors = action.getError();
                        console.log(JSON.stringify(errors));
                        self.util.showErrorToast({ 'errorMessage': errors ? errors[0].message : undefined});
                    }
                })
                $A.enqueueAction(action);

                component.set("v.cancelLabel", "Back"); 
            }else{
                component.set("v.componentId", "F5YF");
                component.set("v.nextLabel", "Save");
                component.set("v.step", "5"); 
            }
        })
        $A.enqueueAction(action);
    },
    handleViewClick : function(component){
        var modalBody;
        var attributes = {
            header: 'Review ' + component.get('v.selectedRegion') + ' Opportunity & Questions',
            recordId: component.get('v.recordId'),
            selectedRegion: component.get('v.selectedRegion'),
            closeModal: component.getReference('v.closeModal'),
            from: 'GMA'
        };
        $A.createComponent("c:ODINViewAllPage", attributes,
        function(content, status) {
            if (status === "SUCCESS") {
                component.set('v.viewSelected', true);
                modalBody = content;
                component.find('overlayLib').showCustomModal({
                    body: modalBody,
                    showCloseButton: true,
                    cssClass: "mymodal",
                    closeCallback: function() {
                        if (component.get('v.closeModal'))
                            component.find("overlayLib").notifyClose();
                    }
                })
            }
        });
    },
    util : {}
})