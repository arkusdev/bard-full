({
    doInit : function(component, event, helper) {
        helper.util = component.find('util');
        helper.onloadData(component,event,helper);
    },
    regionSelected : function(component, event, helper) {
        helper.regionSelected(component, event, helper);
    },
    interestSelected : function(component, event, helper) {
        helper.interestSelected(component, event, helper);
    },
    questionSelected : function(component, event, helper) {
        helper.questionSelected(component, event, helper);
    },
    financialSelected : function(component, event, helper) {
        helper.financialSelected(component, event, helper);
    },
    handleNextClick : function(component, event, helper) {
        helper.handleNextClick(component, event, helper);
    },
    handleCancelClick : function(component, event, helper) {
        helper.handleCancelClick(component, event, helper);
    },
    handleViewClick : function(component, event, helper) {
        helper.handleViewClick(component);
    }
})