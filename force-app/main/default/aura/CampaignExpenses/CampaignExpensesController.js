({
	doInit : function(cmp, event, helper) {
		var device = $A.get("$Browser.formFactor");

		cmp.set("v.desktopFlg", (device.toLowerCase() === 'desktop'));

        var action = cmp.get("c.allowAccess");
               
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (cmp.isValid() && state == "SUCCESS") {
				var accessMap = new Map();
				accessMap = response.getReturnValue();

				cmp.set("v.accessMap", accessMap);

				for(var key in accessMap) {
					var allow = accessMap[key];

					if (allow === true) {
						cmp.find(key + "-comp").loadComponent(allow);
					}
				}					

            } else if (state == "ERROR") {
                var errors = action.getError();
                
                if (errors[0] && errors[0].message) {                         
                    console.error(errors[0].message);

                    var params = {
                        title: 'Uh Oh!',
                        mode: 'dismissible',
                        message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
                        type: 'error'
                    }
                    
					_apostletech.showToast(params);
                }
            }
        });
        
        $A.enqueueAction(action);		
	},

	cancel: function(cmp, event, helper) {
		$A.get("e.force:closeQuickAction").fire();
	}
})