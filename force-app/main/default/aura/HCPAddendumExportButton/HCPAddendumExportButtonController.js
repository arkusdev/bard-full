({
    redirectToHCP : function(component, event, helper) { 
        var campaignId = component.get("v.recordId");
        var reportType= component.get("v.selectedVal");
        window.location.href = '/apex/HCP_Report?Id='+campaignId+'&type='+reportType;		
    },
    redirectToHCP_view : function(component, event, helper) {
        var campaignId = component.get("v.recordId");
        var reportType= component.get("v.selectedVal");
        window.open('/apex/HCP_Report_View?Id='+campaignId+'&type='+reportType,'_blank');
    },
    doInit: function(component, event, helper) {
        var opts = [
            {class: "optionClass",label: "All",value: "All"},
                  {class: "optionClass",label: "Meals",value: "Meals"},
                  {class: "optionClass",label: "Flights",value: "Flights"},
                  {class: "optionClass",label: "Hotels",value: "Hotels"},
                  {class: "optionClass",label: "Transportation",value: "Transportation"},
                  {class: "optionClass",label: "Additional Course Costs",value: "Additional Course Costs"},
                  {class: "optionClass",label: "Proctor Fees",value: "Proctor Fees"}
            
        ];
        component.find("InputSelectDynamic").set("v.options", opts);
    }
})