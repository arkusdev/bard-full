({
	doSave: function (cmp, closeForm) {
		var fields = [{
			id: 'passenger',
			type: 'text'
		}, {
			id: 'cost',
			type: 'number'				
		}, {
			id: 'airline',
			type: 'text'				
		}, {
			id: 'confirmationNumber',
			type: 'text'				
		}, {
			id: 'departDateTime',
			type: 'datetime'				
		}, {
			id: 'arriveDateTime',
			type: 'datetime'				
		}];

		var validFlg = _apostletech.isValid(cmp, fields);

		if (validFlg === true) {
			var flight = cmp.get("v.flight");
			var flightLegs = cmp.get("v.flightLegs");
			var legs = [];
			var placeholder = [];

			flightLegs.forEach(function(item){
				legs.push({
					flightLegId: item.flightLegId,
					name: item.name,
					origin: item.origin,
					destination: item.destination,
					departDateTime: item.departDateTime,
					arriveDateTime: item.arriveDateTime,
					airline: item.airline,
					confirmationNumber: item.confirmationNumber,
					flightNumber: item.flightNumber,
					gate: item.gate,
					terminal: item.terminal,
					type: item.type,
					notes: item.notes
				});
			});	
		
			var dto = {
				flightId: flight.flightId,
				campaignId: flight.campaignId,
				attendeeId: flight.attendeeId,
				attendeeName: flight.attendeeName,
				type: flight.type,
				name: flight.name,
				cost: flight.cost,
				flightLegs: legs
			};

			var action = cmp.get("c.saveCampaignFlight");
        
			action.setParams({
				"dto": JSON.stringify(dto)
			});
        
			action.setCallback(this, function(response) {
				var state = response.getState();
            
				if (cmp.isValid() && state == "SUCCESS") {
					var event = $A.get("e.c:CampaignFlightDataChanged");
					event.fire();

					if (closeForm == true) {
						cmp.find("flight-modal").notifyClose();
					}

					var params = {
						title: 'Successfully Saved!',
						mode: 'dismissible',
						message: 'Flight successfully saved to campaign!',
						type: 'success'
					}
                    
					_apostletech.showToast(params);
                
				} else if (state == "ERROR") {
					var errors = action.getError();
                
					if (errors[0] && errors[0].message) {                         
						console.error(errors[0].message);

						var params = {
							title: 'Uh Oh!',
							mode: 'dismissible',
							message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
							type: 'error'
						}
                    
						_apostletech.showToast(params);
					}
				}
			});
        
			$A.enqueueAction(action);		
		}
	}
})