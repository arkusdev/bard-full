({
	onInit: function(cmp, event, helper) { 
		var flight = cmp.get("v.flight");

		var availablePassengers = [];

        for(var key in flight.availablePassengers) {
			availablePassengers.push({
				key: key,
				value: flight.availablePassengers[key]
			});
        }

		cmp.set("v.availablePassengers", availablePassengers);
		cmp.set("v.flightLegs", flight.flightLegs);
	},

	setPassenger: function(cmp, event, helper) {
		var flight = cmp.get("v.flight");
		var availablePassengers = cmp.get("v.availablePassengers");

		var value = cmp.find("passenger").get("v.value");

		if (value !== '') {
			var passenger = availablePassengers.find(item => item.key === value);

			if (passenger === null || passenger === undefined) { return; }

			flight.attendeeId = passenger.key;
			flight.attendeeName = passenger.value;
		} else {
			flight.attendeeId = null;
			flight.attendeeName = null;
		}

		cmp.set("v.flight", flight);
	},

	setDestination: function(cmp, event, helper) {
		var flightLegs = cmp.get("v.flightLegs");
		var type = event.getSource().get("v.name");
		var value;
		
		flightLegs.forEach(function(item){
			if (item.type === type) { 
				value = item.origin;
			}
		});			

		flightLegs.forEach(function(item){
			if (item.type !== type) { 
				item.destination = value;
			}
		});					

		cmp.set("v.flightLegs", flightLegs);
	},

	setOrigin: function(cmp, event, helper) {
		var flightLegs = cmp.get("v.flightLegs");
		var type = event.getSource().get("v.name");
		var value;
		
		flightLegs.forEach(function(item){
			if (item.type === type) { 
				value = item.destination;
			}
		});			

		flightLegs.forEach(function(item){
			if (item.type !== type) { 
				item.origin = value;
			}
		});					

		cmp.set("v.flightLegs", flightLegs);	
	},

	cancel: function(cmp, event, helper) {
		cmp.find("flight-modal").notifyClose();
	},

	saveAndNew: function(cmp, event, helper) {
		//helper.doSave(cmp, false);
	},

	save: function(cmp, event, helper) {
		helper.doSave(cmp, true);
	}
})