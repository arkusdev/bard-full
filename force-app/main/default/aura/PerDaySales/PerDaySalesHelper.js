({
    getCalculations: function(cmp) {
        var self = this;
        var params = {
            userId: cmp.get('v.user'),
            period: cmp.get('v.period'),
            distributor: cmp.get('v.distributor')
        }
        cmp.set('v.isBusy', true);
        this.serverAction(cmp, 'getCalculations', params)
        .then(function(data) {
            data = self.prepareTableData(data, cmp);
            var allData = data.tableData;
            cmp.set('v.allData', allData);
            cmp.set('v.filteredData', allData);
            self.setToDateColumnName(cmp);
            cmp.set('v.response', data);
            cmp.set('v.isBusy', false);
            self.setChartData(cmp);
        }, function (errors) {
            var msg = errors.length > 0 ? errors[0].message : 'An unexpected error occurred';
            alert(msg);
            cmp.set('v.isBusy', false);
        });
    },
    serverAction : function(component, method, params) {
        var self = this;
        self.serverCallIssued(true);
        return new Promise(function(resolve, reject) {
            var action = component.get('c.' + method);
            if(params != null)
                action.setParams(params);
    
            action.setCallback(self, function(response){
                self.serverCallIssued(false);
                var state = response.getState();
                if(state == 'SUCCESS')
                    resolve.call(this, response.getReturnValue());
                else if(state == 'ERROR')
                    reject.call(this, response.getError());
            });
    
            $A.enqueueAction(action);
        });
    },
    prepareTableData: function(data, component) {
        var categories = [];
        var self = this;
        self.chartData = new Map();
        var categoriesFromGraph = [];
        data.tableData.forEach(function(x, i) {
            x.mtdBaseIsNegative = x.mtdBase < 0;
            x.mtdQuotaIsNegative = x.mtdQuota < 0;
            x.vsBaseIsNegative = x.mtdVsBase < 0;
            x.vsQuotaIsNegative = x.mtdVsQuota < 0;
            x.mtdFullQuotaIsNegative = x.mtdFullQuota < 0;
            x.periodVsQuotaIsNegative = x.periodVsQuota < 0;
            
            x.mtdSales = x.mtdSales;
            x.mtdBase = x.mtdBase;
            x.mtdQuota = x.mtdQuota;
            x.mtdVsBase = x.mtdVsBase;
            x.mtdVsQuota = x.mtdVsQuota;
            x.dailyAvg = x.dailyAvg;
            x.projection = x.projection;
            x.mtdFullQuota = x.mtdFullQuota;
            x.periodVsQuota = x.periodVsQuota;
            var categoryName = x.catName;
            if (categoryName != 'Total' && !categories.includes(categoryName)){
                categories.push({
                    label: categoryName,
                    value: categoryName,
                    selected: false
                });
                self.chartData.set(categoryName, {
                    'Sales' : x.mtdSales
                });
                categoriesFromGraph.push(categoryName);
            }
        });
        categories.unshift({
            label: 'All Categories',
            value: 'all',
            selected: true
        });
        component.set('v.category', 'all');
        component.set('v.categories', categories);
        component.set('v.categoriesFromGraph', categoriesFromGraph);

        var empty = {
            catName: undefined,
            dailyAvg: undefined,
            projection: undefined,
            mtdBase: undefined,
            mtdQuota: undefined,
            mtdVsBase: undefined,
            mtdVsQuota: undefined,
            mtdFullQuota: undefined,
            periodVsQuota: undefined
        }

        // create empty row before totals
        if (data.tableData.length > 0) {
            var copied = data.tableData.pop();
            data.tableData.push(empty);
            data.tableData.push(copied);
        } else {
            data.tableData.push(empty);
        }

        return data;
    },
    setToDateColumnName: function(cmp) {
        var period = cmp.get('v.period');
        var letter = '';
        if (period === 'month') letter = 'M';
        else if (period === 'quarter') letter = 'Q';
        else if (period === 'year') letter = 'Y';
        cmp.set('v.toDateColumnName', letter);
    },
    serverCallIssued: function (active) {
        var event = $A.get('e.c:PDSe_ServerCallIssued');
        event.setParams({active:active});
        event.fire();
    },
    filterCategory: function(cmp, category){
        var data = cmp.get('v.allData');
        var dataFiltered = category == 'all' ? data : data.filter(d => {
            return d.catName == category;
        });
        cmp.set('v.filteredData', dataFiltered);
        this.setChartData(cmp);
    },
    setChartData: function(cmp){
        var self = this;
        var categories = cmp.get('v.categoriesFromGraph');
        var allDatasets = self.getDatasets(categories)[0];
        var datasets = [];
        let colors = [];

        let category = cmp.get('v.category');
        let filteredDataforChart = cmp.get('v.filteredData');        
        
        var el = cmp.find('barChart').getElement();
        var ctx = el.getContext('2d');
        var randomColor = '';
        
        if (allDatasets){
            if(category && category != 'all'){
                categories = [category];
                filteredDataforChart.forEach(element => {
                    if(element.catName == category && element.mtdSales != undefined){
                        datasets.push(element.mtdSales);
                        randomColor = '#' + Math.floor(Math.random()*16777215).toString(16);
                        colors.push(randomColor);
                    }
                })
            } else {
                allDatasets.forEach(function(value, key){
                    value.forEach(x => {
                        randomColor = '#' + Math.floor(Math.random()*16777215).toString(16);
                        datasets.push(x);
                        colors.push(randomColor);
                    });
                });
            }
        }        
        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: categories,
                datasets: [{
                    label: 'Sales',
                    data: datasets,
                    backgroundColor: colors
                }]
            },
            options: {
                events: ['click'],
                scales: {
                    yAxes: [{
                        ticks: {
                            callback: function(value, index, values) {
                            return value.toLocaleString("en-US",{style:"currency", currency:"USD"});
                            }
                        }
                    }]
                }
            }
        });
    },
    getDatasets: function(categories){
        var self = this;
        var chartDataPerColumn = new Map();
        var datasets = [];
        var chartData = self.chartData;
        categories.forEach(c => {
            var chartDataNode = chartData.get(c);
            if (chartDataNode && Object.keys(chartDataNode)){
                Object.keys(chartDataNode).forEach(key => {
                    if (chartDataPerColumn.get(key))
                        chartDataPerColumn.get(key).push(chartData.get(c)[key])
                    else
                        chartDataPerColumn.set(key, [chartData.get(c)[key]]);
                });
            }
            datasets.push(chartDataPerColumn);
        });
        return datasets;
    },
    chartData: {},
    chartDataPerColumn: {}
})