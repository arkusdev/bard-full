({
	changeUser: function (cmp, evt, h) {
        cmp.set('v.user', evt.getParam('userId'));
        h.getCalculations(cmp);
    },
    changePeriod: function (cmp, evt, h) {
        cmp.set('v.period', evt.getParam('period'));
        h.getCalculations(cmp);
    },
    distributorSwitched: function (cmp, evt, h) {
        cmp.set('v.distributor', evt.getParam('active'));
        h.getCalculations(cmp);
    },
    viewAsSwitched: function(cmp, evt, h){
        var viewAsGraph = evt.getParam('active');
        cmp.set('v.viewAsGraph', viewAsGraph);
    },
    changeCategory: function (cmp, evt, h) {
        h.filterCategory(cmp, cmp.get('v.category'));
    }
})