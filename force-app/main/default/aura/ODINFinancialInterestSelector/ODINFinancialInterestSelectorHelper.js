({
    onloadData : function(component,event,helper) {
        var action = component.get("c.GetInterestOptions");
        action.setParams({  "recordId" : component.get("v.recordId"),
                            "region" : component.get("v.selectedRegion")
                        });

        var level;
        if (component.get("v.interest") && component.get("v.interest") != null) {
            level = component.get("v.interest").Level__c;
        }                

        action.setCallback(this, function(response) {
            var interest = response.getReturnValue();

            if (level != null && level != "") {
                interest.preSelectedLevel.Level__c = level;
            }

            if (interest.preSelectedLevel != null) {
                component.set("v.interest", interest.preSelectedLevel);
            }

            var interestOptions = [{ "value": "", "label": "--None--" }]; 
            
            interest.levels.forEach(element => {
                
                if (interest.preSelectedLevel != null && element == interest.preSelectedLevel.Level__c) {
                    interestOptions.push({ "value": element, "label": element, "selected" : true });
                    component.set("v.selectedValue", interest.preSelectedLevel.Level__c);
                } else {
                    interestOptions.push({ "value": element, "label": element });
                }                    
            });
            component.set("v.picklistValues", interestOptions);

        })
        $A.enqueueAction(action);
    },
    handleChange : function(component, event, helper) {
        var interest = component.get("v.interest");
        
        if (interest != null) {
            if (interest.Level__c != component.get("v.selectedValue")) {
                interest.Level__c = component.get("v.selectedValue");
            }
        }
        var compEvent = component.getEvent("interestSelected");
        compEvent.setParams({interest : interest});
        compEvent.fire();
    },
    close : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})