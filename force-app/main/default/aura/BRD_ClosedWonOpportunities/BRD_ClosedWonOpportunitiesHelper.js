({
	getOpportunities: function(cmp) {
        var self = this;
		var action = cmp.get('c.getOpportunities');
        action.setStorable();
        cmp.set('v.isBusy', true);
        action.setCallback(this, function(response) {
            var state, data;
            state = response.getState();
            if (state === 'SUCCESS') {
                data = response.getReturnValue();
                self.setDateToDisplay(cmp, data);
                cmp.set('v.opps', data);
            } else {
                console.error('There was an error while retrieving the opportunities', response.getError());
            }
            cmp.set('v.isBusy', false);
        });
        $A.enqueueAction(action);
	},
    sort: function(cmp, field) {
        var list = cmp.get('v.opps');
        switch(field) {
            case 'CloseDate':
                this.sortDate(cmp, field, list);
                break;
            case 'Name': case 'Account.Name': case 'Product_Category__c': case 'Product_Group__c':
                this.sortString(cmp, field, list);
                break;
            case 'of_Units__c': case 'Amount':
                this.sortNumber(cmp, field, list);
                break;
        }
    },
    sortDate: function(cmp, field, list) {
        var self = this;
        var direction = this.setDirection(cmp, field);

        list.sort(function(opp, next) {
            var currValue = self.getInnerValue(opp, field);
            var nextValue = self.getInnerValue(next, field);

            if (direction === 'ASC') {
                return new Date(currValue) - new Date(nextValue);
            } else {
                return new Date(nextValue) - new Date(currValue);
            }
            
        });

        this.setSorting(cmp, list, field, direction);
    },
    sortString: function(cmp, field, list) {
        var self = this;
        var direction = this.setDirection(cmp, field);

        list.sort(function(opp, next) {
            var currValue = self.getInnerValue(opp, field);
            var nextValue = self.getInnerValue(next, field);

            if (direction === 'ASC') {
                return ('' + currValue).localeCompare(nextValue);
            } else {
                return ('' + nextValue).localeCompare(currValue);
            }
            
        });

        this.setSorting(cmp, list, field, direction);
    },
    sortNumber: function(cmp, field, list) {
        var direction = this.setDirection(cmp, field);

        list.sort(function(opp, next) {
            var a = Number.parseFloat(opp[field]);
            var b = Number.parseFloat(next[field]);
            if (direction === 'ASC') {
                return a > b;
            } else {
                return a < b;
            }
        });

        this.setSorting(cmp, list, field, direction);
    },
    setDirection: function(cmp, field) {
        var dir;
        if (cmp.get('v.sortField') === field) {
            dir = cmp.get('v.sortDirection') === 'ASC' ? 'DESC' : 'ASC';
        } else {
            dir = 'ASC';
        }
        return dir;
    },
    setSorting: function(cmp, list, field, direction) {
        cmp.set('v.opps', list);
        cmp.set('v.sortField', field);
        cmp.set('v.sortDirection', direction);
    },
    getInnerValue: function(wrapper, field) {
        var innerValue = Object.assign({}, wrapper);
        field.split('.').forEach(function(innerField) {
            if (innerField != '') innerValue = innerValue[innerField];
        });
        return innerValue;
    },
    setDateToDisplay: function(cmp, data) {
        if (data && typeof data.forEach === 'function') {
            data.forEach(function(row) {
                if (row.CloseDate) {
                    var cd, mm, dd;
                    cd = new Date(row.CloseDate + 'T00:00:00');
                    mm = cd.getMonth() + 1;
                    dd = cd.getDate();
                    if (String.prototype.padStart) {
                        mm = mm.toString().padStart(2, '0');
                        dd = dd.toString().padStart(2, '0');
                    } else {
                        if (mm < 10) mm = '0' + mm;
                        if (dd < 10) mm = '0' + mm;
                    }
                    row.CloseDateToDisplay = mm + '-' + dd + '-' + cd.getFullYear();
                }
            })
        }
    }
})