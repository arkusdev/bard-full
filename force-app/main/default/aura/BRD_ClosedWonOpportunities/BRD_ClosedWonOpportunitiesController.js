({
	init: function(cmp, evt, h) {
		h.getOpportunities(cmp);
	},
    sort: function(cmp, evt, h) {
        var field = evt.currentTarget.dataset.field;
        h.sort(cmp, field);
    },
    oppSelected: function(cmp, e, h) {
        var closeDate = e.target.dataset && e.target.dataset.closedate ? e.target.dataset.closedate : (e.currentTarget.dataset ? e.currentTarget.dataset.closedate : undefined);
        var accountId = e.target.dataset && e.target.dataset.accountid ? e.target.dataset.accountid : (e.currentTarget.dataset ? e.currentTarget.dataset.accountid : undefined);
        if (closeDate && accountId) {
            var event = $A.get('e.c:BRD_OpportunitySelected');
            event.setParams({
                closeDate: closeDate,
                accountId: accountId
            });
            event.fire();
        }
    }
})