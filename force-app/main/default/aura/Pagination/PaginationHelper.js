({
	raiseEvent: function(cmp) {
		var event = cmp.getEvent("pageEvent");

		event.setParams({
			"pageNumber": cmp.get("v.currentPageNumber")
		});

		event.fire();
	}
})