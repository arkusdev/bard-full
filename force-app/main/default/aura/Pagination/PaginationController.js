({
    firstPage: function(cmp, event, helper) {
        cmp.set("v.currentPageNumber", 1);
		helper.raiseEvent(cmp);
    },
    prevPage: function(cmp, event, helper) {
        cmp.set("v.currentPageNumber", Math.max(cmp.get("v.currentPageNumber")-1, 1));
		helper.raiseEvent(cmp);
    },
    nextPage: function(cmp, event, helper) {
        cmp.set("v.currentPageNumber", Math.min(cmp.get("v.currentPageNumber")+1, cmp.get("v.maxPageNumber")));
		helper.raiseEvent(cmp);
    },
    lastPage: function(cmp, event, helper) {
        cmp.set("v.currentPageNumber", cmp.get("v.maxPageNumber"));
		helper.raiseEvent(cmp);
    }
})