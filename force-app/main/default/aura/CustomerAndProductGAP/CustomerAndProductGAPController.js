({
	init: function(cmp, evt, h) {
        //h.serverCallEvt = $A.get('e.c:PDSe_ServerCallIssued');
        h.util = cmp.find('util');
        cmp.find('workspace').isConsoleNavigation().then(function(isConsoleNavigation) {
            cmp.set('v.isConsole', isConsoleNavigation);
        }, function(error) {
            console.error(error);
            h.showToast('Error retrieving the data', error.body.message, 'error');
        });
	},
    changeUser: function (cmp, evt, h) {
        cmp.set('v.user', evt.getParam('userId'));
        h.getCalculations(cmp);
    },
    changePeriod: function (cmp, evt, h) {
        cmp.set('v.period', evt.getParam('period'));
        h.getCalculations(cmp);
    },
    distributorSwitched: function (cmp, evt, h) {
        cmp.set('v.distributor', evt.getParam('active'));
        h.getCalculations(cmp);
    },
    viewAsSwitched: function(cmp, evt, h){
        var viewAsGraph = evt.getParam('active');
        cmp.set('v.viewAsGraph', viewAsGraph);
    },
    filter: function(cmp, evt, h) {
        h.util.filter({cmp:cmp, h:h, view:'customer'});
        if(cmp.get('v.viewAsGraph')){
            cmp.set('v.isBusy', true);
        }
        setTimeout(function(){ 
            h.setTableDataAfterFilter(cmp, evt, cmp.get('v.tableData'));
            cmp.set('v.isBusy', false);
        }, 2000);
    },
    sort: function (cmp, evt, h) {
        var ds = evt.currentTarget.dataset;
        if (!ds || !ds.sort) return;

        h.sort(cmp, ds.sort);
    },
    filterByAccount: function (cmp, evt, h) {
        var accountID = evt.currentTarget.dataset ? evt.currentTarget.dataset.accountid : '';
        var filter = $A.get('e.c:PDS_FilterProductsByAccount');
        filter.setParams({accountID:accountID});
        filter.fire();
    },
    nextPage: function(cmp, evt, h) {
        h.nextPage(cmp);
    },
    prevPage: function(cmp, evt, h) {
        h.prevPage(cmp);
    },
    firstPage: function(cmp, evt, h) {
        h.firstPage(cmp);
    },
    lastPage: function(cmp, evt, h) {
        h.lastPage(cmp);
    },
    goToAccount: function (cmp, evt, h) {
        if (!cmp.get('v.isConsole')) return;
        cmp.find('workspace').openTab({
            recordId: evt.target.dataset.url,
            focus: true
        }).then(function(tabId) { console.log('New tab id: ' + tabId)},
                function(error) {console.error(error)})
    }
})