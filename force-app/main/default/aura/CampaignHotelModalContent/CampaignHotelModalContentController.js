({
	onTabChanged: function(cmp, event, helper) {
		var tab = event.getSource();
		var tabId = tab.get("v.id");

		helper.initTabOptions(cmp, tabId);	

		var userInitFlg = cmp.get("v.userInitFlg");

		if (userInitFlg === false) {
			helper.initTabOptions(cmp, 'users-tab');	
		}
	},

	cancel: function(cmp, event, helper) {
		cmp.find("hotel-modal").notifyClose();
	},

	saveAndNew: function(cmp, event, helper) {
		//helper.doSave(cmp, false);
	},

	save: function(cmp, event, helper) {
		helper.doSave(cmp, true);
	},

	add: function(cmp, event, helper) {
		var btn = event.getSource().getLocalId();

		var attendeeId = cmp.find(btn.substring(btn.indexOf("-") + 1).trim()).get("v.value");
		
        if (attendeeId === null || attendeeId === undefined || attendeeId === '') { 
            var toast = $A.get("e.force:showToast");
            
            toast.setParams({
                title: 'Select ' + flight.type,
                mode: 'dismissible',
                message: 'You must make a selection from the Guest picklist before clicking Add.',
                type: 'warning'
            });
            
            toast.fire();
            
            return; 
        }

		var checkInDate = cmp.find("checkInDate").get("v.value");
		var checkOutDate = cmp.find("checkOutDate").get("v.value");

		if (checkInDate === null || checkOutDate === null) {
            var toast = $A.get("e.force:showToast");
            
            toast.setParams({
                title: 'Select ' + flight.type,
                mode: 'dismissible',
                message: 'You must enter a check in and check out date before clicking Add.',
                type: 'warning'
            });
            
            toast.fire();
            
            return; 
		}

		var ms = 86400000; //Milliseconds per day

		var cid = new Date(checkInDate);
		var cod = new Date(checkOutDate);

		var utc1 = Date.UTC(cid.getFullYear(), cid.getMonth(), cid.getDate());
		var utc2 = Date.UTC(cod.getFullYear(), cod.getMonth(), cod.getDate());

		var dateDiff = Math.floor((utc2 - utc1) / ms);

		var dates = [];

		for(var i = 0; i <= dateDiff - 1; i++) { 
			var d = new Date(checkInDate);

			d.setTime(d.getTime() + (d.getTimezoneOffset() * 60 * 1000));
			d.setDate(d.getDate() + i);

			var month = ('0' + (d.getMonth() + 1)).slice(-2);
			
			dates.push({
				reservationDate: d.getFullYear()  + '-' + month + '-' + d.getDate(),
				reservationDateDisplay: (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear()
			});
		}
		
		var reservations = [];
		var availableAttr = 'v.available';
		var selectedAttr = 'v.selected';
	
		if (btn === 'add-member') {
			availableAttr += 'Members';
			selectedAttr += 'Members';
		} else {		
			availableAttr += 'Users';
			selectedAttr += 'Users';	
		}

		var available = cmp.get(availableAttr);
		var selected = cmp.get(selectedAttr);

		var attendee = available.find(item => item.attendeeId === attendeeId);

		if (attendee === null || attendee === undefined) { return; }

		var prevDate = {
			reservationDate: null,
			reservationDateDisplay: null
		};

		var nextDate = {
			reservationDate: null,
			reservationDateDisplay: null
		};

		dates.forEach(function(item, i) {
			if (i === 0) {
				var d = new Date(item.reservationDate);

				d.setTime(d.getTime() + (d.getTimezoneOffset() * 60 * 1000));
				d.setDate(d.getDate() - 1);
			
				var month = ('0' + (d.getMonth() + 1)).slice(-2);

				prevDate.reservationDate = d;
				prevDate.reservationDateDisplay = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
			} 

			if (i === dates.length - 1) {
				var d = new Date(item.reservationDate);

				d.setTime(d.getTime() + (d.getTimezoneOffset() * 60 * 1000));
				d.setDate(d.getDate() + 1);
			
				var month = ('0' + (d.getMonth() + 1)).slice(-2);

				nextDate.reservationDate = d;
				nextDate.reservationDateDisplay = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
			}	

			reservations.push({
				hotelAttendeeId: null,
				attendeeName: item.attendeeName,
				confirmationNumber: item.confirmationNumber,
				cost: 0,
				costDisplay: '$0.00',
				name: attendee.attendeeName,
				present: true,
				reservationDate: item.reservationDate,
				reservationDateDisplay: item.reservationDateDisplay
			});
		});

		selected.push({
			attendeeId: attendeeId,
			attendeeName: attendee.attendeeName,
			confirmationNumber: attendee.confirmationNumber,
			prevReservationDate: prevDate.reservationDate,
			prevReservationDateDisplay: prevDate.reservationDateDisplay,
			nextReservationDate: nextDate.reservationDate,
			nextReservationDateDisplay: nextDate.reservationDateDisplay,
			reservations: reservations
		});	

		available = available.filter(function(item) { 
			return item.attendeeId !== attendee.attendeeId; 
		});

		cmp.set(selectedAttr, selected);
		cmp.set(availableAttr, available);
	},

	addReservation: function(cmp, event, helper) {
		var btn = event.getSource().getLocalId();
		var name = event.getSource().get("v.name");		
		var attendeeId = name.substring(0, name.indexOf("|")).trim();
		var date = name.substring(name.indexOf("|") + 1).trim();

		var d = new Date(date);
		d.setTime(d.getTime() + (d.getTimezoneOffset() * 60 * 1000));
		var month = ('0' + (d.getMonth() + 1)).slice(-2);
		
		var reservationDate = d.getFullYear()  + '-' + month + '-' + d.getDate();
		var reservationDateDisplay = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();

		var selectedAttr = 'v.selected';
		var prevFlg = false;

		switch(btn) {
			case 'add-member-reservation-prev':
				selectedAttr += 'Members';
				prevFlg = true;
				break;
			case 'add-member-reservation-next':
				selectedAttr += 'Members';
				break;
			case 'add-user-reservation-prev':
				selectedAttr += 'Users';
				prevFlg = true;
				break;
			case 'add-user-reservation-next':
				selectedAttr += 'Users';
				break;
		}

		var selectedValues = cmp.get(selectedAttr);
		var attendee = selectedValues.find(item => item.attendeeId === attendeeId);

		if (attendee === null || attendee === undefined) { return; }

		var obj = {
			hotelAttendeeId: null,
			attendeeName: attendee.attendeeName,
			confirmationNumber: attendee.confirmationNumber,
			cost: 0,
			costDisplay: '$0.00',
			name: attendee.attendeeName,
			present: true,
			reservationDate: reservationDate,
			reservationDateDisplay: reservationDateDisplay
		};

		if (prevFlg) {
			attendee.reservations.splice(0, 0, obj);
		} else {
			attendee.reservations.push(obj);		
		}

		var prevDate = {
			reservationDate: null,
			reservationDateDisplay: null
		};

		var nextDate = {
			reservationDate: null,
			reservationDateDisplay: null
		};

		attendee.reservations.forEach(function(item, i){
			if (i === 0) {
				var d = new Date(item.reservationDate);

				d.setTime(d.getTime() + (d.getTimezoneOffset() * 60 * 1000));
				d.setDate(d.getDate() - 1);
			
				var month = ('0' + (d.getMonth() + 1)).slice(-2);

				prevDate.reservationDate = d;
				prevDate.reservationDateDisplay = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
			} 

			if (i === attendee.reservations.length - 1) {
				var d = new Date(item.reservationDate);

				d.setTime(d.getTime() + (d.getTimezoneOffset() * 60 * 1000));
				d.setDate(d.getDate() + 1);
			
				var month = ('0' + (d.getMonth() + 1)).slice(-2);

				nextDate.reservationDate = d;
				nextDate.reservationDateDisplay = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
			}				
		});

		var obj = {
			attendeeId: attendeeId,
			attendeeName: attendee.attendeeName,
			confirmationNumber: attendee.confirmationNumber,
			prevReservationDate: prevDate.reservationDate,
			prevReservationDateDisplay: prevDate.reservationDateDisplay,
			nextReservationDate: nextDate.reservationDate,
			nextReservationDateDisplay: nextDate.reservationDateDisplay,
			reservations: attendee.reservations
		};

		var selected = [];

		selectedValues.forEach(function(item) {
			if (item.attendeeId !== attendeeId) {
				//Not the campaign member we've selected to add a day to so just re-add everything
				selected.push({
					attendeeId: item.attendeeId,
					attendeeName: item.attendeeName,
					confirmationNumber: item.confirmationNumber,
					prevReservationDate: item.prevReservationDate,
					prevReservationDateDisplay: item.prevReservationDateDisplay,
					nextReservationDate: item.nextReservationDate,
					nextReservationDateDisplay: item.nextReservationDateDisplay,
					reservations: item.reservations
				});
			} else {
				selected.push({
					attendeeId: obj.attendeeId,
					attendeeName: obj.attendeeName,
					confirmationNumber: obj.confirmationNumber,
					prevReservationDate: obj.prevReservationDate,
					prevReservationDateDisplay: obj.prevReservationDateDisplay,
					nextReservationDate: obj.nextReservationDate,
					nextReservationDateDisplay: obj.nextReservationDateDisplay,
					reservations: obj.reservations
				});					
			}
		});

		cmp.set(selectedAttr, selected);
	},

	removeReservation: function(cmp, event, helper) {
		var btn = event.getSource().getLocalId();
		var name = event.getSource().get("v.name");
		var attendeeId = name.substring(0, name.indexOf("|")).trim();
		var reservationDate = name.substring(name.indexOf("|") + 1).trim();

		var selectedValues = [];
		var selectedAttr = 'v.selected';

		if (btn === 'remove-member-reservation') {
			selectedAttr += 'Members';
		} else {
			selectedAttr += 'Users';
		}
		
		var selectedValues = cmp.get(selectedAttr);		
		var attendee = selectedValues.find(item => item.attendeeId === attendeeId);

		if (attendee === null || attendee === undefined) { return; }

		if (attendee.reservations.length === 1) {
			helper.removeAttendee(cmp, attendeeId, 'remove-member');
		} else {
			var selected = [];

			var prevDate = {
				reservationDate: null,
				reservationDateDisplay: null
			};

			var nextDate = {
				reservationDate: null,
				reservationDateDisplay: null
			};

			var reservations = attendee.reservations.filter(function(item) {
				return item.reservationDate !== reservationDate;
			});

			reservations.forEach(function(item, i){
				if (i === 0) {
					var d = new Date(item.reservationDate);

					d.setTime(d.getTime() + (d.getTimezoneOffset() * 60 * 1000));
					d.setDate(d.getDate() - 1);
			
					var month = ('0' + (d.getMonth() + 1)).slice(-2);

					prevDate.reservationDate = d;
					prevDate.reservationDateDisplay = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
				} 

				if (i === reservations.length - 1) {
					var d = new Date(item.reservationDate);

					d.setTime(d.getTime() + (d.getTimezoneOffset() * 60 * 1000));
					d.setDate(d.getDate() + 1);
			
					var month = ('0' + (d.getMonth() + 1)).slice(-2);

					nextDate.reservationDate = d;
					nextDate.reservationDateDisplay = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
				}				
			});

			var obj = {
				attendeeId: attendeeId,
				attendeeName: attendee.attendeeName,
				confirmationNumber: attendee.confirmationNumber,
				prevReservationDate: prevDate.reservationDate,
				prevReservationDateDisplay: prevDate.reservationDateDisplay,
				nextReservationDate: nextDate.reservationDate,
				nextReservationDateDisplay: nextDate.reservationDateDisplay,
				reservations: reservations
			};

			selectedValues.forEach(function(item) {
				if (item.attendeeId !== attendeeId) {
					//Not the campaign member we've selected to remove a day from so just re-add everything
					selected.push({
						attendeeId: item.attendeeId,
						attendeeName: item.attendeeName,
						confirmationNumber: item.confirmationNumber,
						prevReservationDate: item.prevReservationDate,
						prevReservationDateDisplay: item.prevReservationDateDisplay,
						nextReservationDate: item.nextReservationDate,
						nextReservationDateDisplay: item.nextReservationDateDisplay,
						reservations: item.reservations
					});
				} else {
					selected.push({
						attendeeId: obj.attendeeId,
						attendeeName: obj.attendeeName,
						confirmationNumber: obj.confirmationNumber,
						prevReservationDate: obj.prevReservationDate,
						prevReservationDateDisplay: obj.prevReservationDateDisplay,
						nextReservationDate: obj.nextReservationDate,
						nextReservationDateDisplay: obj.nextReservationDateDisplay,
						reservations: obj.reservations
					});					
				}
			});

			cmp.set(selectedAttr, selected);	
		}
	},

	removeAttendee: function(cmp, event, helper) {
		var btn = event.getSource().getLocalId();
		var attendeeId = event.getSource().get("v.name");

		helper.removeAttendee(cmp, attendeeId, btn);
	},

	enableAdd: function(cmp, event, helper) {
		var checkInDate = cmp.find("checkInDate").get("v.value");
		var checkOutDate = cmp.find("checkOutDate").get("v.value");

		var flg = ((checkInDate === undefined || checkOutDate === undefined || checkInDate === null || checkOutDate === null));

		cmp.set("v.disableAdd", flg);
	}

	/*
	synchReservationDates: function(cmp, event, helper) {
		var selectedMembers = cmp.get("v.selectedMembers");
		var selectedUsers = cmp.get("v.selectedUsers");

		var checkInDate = cmp.find("checkInDate").get("v.value");
		var checkOutDate = cmp.find("checkOutDate").get("v.value");

		if (checkInDate === null || checkOutDate === null) {
            var toast = $A.get("e.force:showToast");
            
            toast.setParams({
                title: 'Select ' + flight.type,
                mode: 'dismissible',
                message: 'You must enter a check in and check out date before clicking Add.',
                type: 'warning'
            });
            
            toast.fire();
            
            return; 
		}

		var ms = 86400000; //Milliseconds per day

		var cid = new Date(checkInDate);
		var cod = new Date(checkOutDate);

		var utc1 = Date.UTC(cid.getFullYear(), cid.getMonth(), cid.getDate());
		var utc2 = Date.UTC(cod.getFullYear(), cod.getMonth(), cod.getDate());

		var dateDiff = Math.floor((utc2 - utc1) / ms);
			
		var dates = [];

		for(var i = 0; i <= dateDiff - 1; i++) { 
			var d = new Date(checkInDate);

			d.setTime(d.getTime() + (d.getTimezoneOffset() * 60 * 1000));
			d.setDate(d.getDate() + i);
			
			var month = ('0' + (d.getMonth() + 1)).slice(-2);

			dates.push({
				reservationDate: d.getFullYear()  + '-' + month + '-' + d.getDate(),
				reservationDateDisplay: (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear()
			});
		}
		
		var selectedMembers =  cmp.get("v.selectedMembers");
		var selectedUsers =  cmp.get("v.selectedUsers");

		selectedMembers.forEach(function(m) {
			dates.forEach(function(d, i) { 
				var memberDates = m.reservations.filter(function(item) { 
					return (item.reservationDate === d.reservationDate); 
				});

				if (memberDates && memberDates.length <= 0) {
					m.reservations.push({
						hotelAttendeeId: null,
						attendeeId: m.attendeeId,
						attendeeName: m.attendeeName,
						cost: 0,
						costDisplay: '$0.00',
						name: m.attendeeName,
						present: true,
						reservationDate: d.reservationDate,
						reservationDateDisplay: d.reservationDateDisplay,
						isEvenIndex: (i % 2 == 0)
					});	
				}
			});			
		});

		selectedUsers.forEach(function(u) {
			dates.forEach(function(d, i) { 
				var userDates = u.reservations.filter(function(item) { 
					return (item.reservationDate === d.reservationDate); 
				});

				if (userDates && userDates.length <= 0) {
					u.reservations.push({
						hotelAttendeeId: null,
						attendeeId: u.attendeeId,
						attendeeName: u.attendeeName,
						cost: 0,
						costDisplay: '$0.00',
						name: u.attendeeName,
						present: true,
						reservationDate: d.reservationDate,
						reservationDateDisplay: d.reservationDateDisplay,
						isEvenIndex: (i % 2 == 0)
					});	
				}
			});			
		});

		cmp.set("v.selectedMembers", selectedMembers);
		cmp.set("v.selectedUsers", selectedUsers);
	},
	*/
})