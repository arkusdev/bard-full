({
	doSave: function (cmp, closeForm) {
		var fields = [{
			id: 'name',
			type: 'text'
		}, {
			id: 'checkInDate',
			type: 'date'				
		}, {
			id: 'checkOutDate',
			type: 'date'				
		}, {
			id: 'member-confirmationNumber',
			type: 'text'				
		}, {
			id: 'member-cost',
			type: 'number'				
		}, {
			id: 'user-confirmationNumber',
			type: 'text'				
		}, {
			id: 'user-cost',
			type: 'number'				
		}];

		var validFlg = _apostletech.isValid(cmp, fields);

		console.log(validFlg);

		if (validFlg === true) {
			var hotel = cmp.get("v.hotel");

			var members = cmp.get("v.selectedMembers");
			var users = cmp.get("v.selectedUsers");

			var selectedMembers = [];
			var selectedUsers = [];

			members.forEach(function(m){
				m.reservations.forEach(function(mr) {
					selectedMembers.push({
						hotelAttendeeId: mr.hotelAttendeeId,
						attendeeId: m.attendeeId,
						cost: mr.cost,
						name: mr.name,
						present: mr.present,
						reservationDate: mr.reservationDate,
						confirmationNumber: m.confirmationNumber
					});
				});	
			});

			users.forEach(function(u){
				u.reservations.forEach(function(ur){
					selectedUsers.push({
						hotelAttendeeId: ur.hotelAttendeeId,
						attendeeId: u.attendeeId,
						cost: ur.cost,
						name: ur.name,
						present: ur.present,
						reservationDate: ur.reservationDate,
						confirmationNumber: u.confirmationNumber
					});
				});	
			});
	
			var dto = {
				hotelId: hotel.hotelId,
				campaignId: hotel.campaignId,
				name: hotel.name,
				checkInDate: hotel.checkInDate,
				checkOutDate: hotel.checkOutDate,
				roomBlock: hotel.roomBlock,
				additionalAttendees: (hotel.additionalAttendees === '' ? 0 : hotel.additionalAttendees),
				additionalAttendeesCost: (hotel.additionalAttendeesCost === '' ? 0 : hotel.additionalAttendeesCost),
				notes: hotel.notes,
				selectedMembers: selectedMembers,
				selectedUsers: selectedUsers
			};

			var action = cmp.get("c.saveCampaignHotel");
        
			action.setParams({
				"dto": JSON.stringify(dto)
			});

			action.setCallback(this, function(response) {
				var state = response.getState();
            
				if (cmp.isValid() && state == "SUCCESS") {
					var event = $A.get("e.c:CampaignHotelDataChanged");
					event.fire();

					if (closeForm == true) {
						cmp.find("hotel-modal").notifyClose();
					}

					var params = {
						title: 'Successfully Saved!',
						mode: 'dismissible',
						message: 'Hotel successfully saved to campaign!',
						type: 'success'
					}
                    
					_apostletech.showToast(params);
                
				} else if (state == "ERROR") {
					var errors = action.getError();
                
					if (errors[0] && errors[0].message) {                         
						console.error(errors[0].message);

						var params = {
							title: 'Uh Oh!',
							mode: 'dismissible',
							message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
							type: 'error'
						}
                    
						_apostletech.showToast(params);
					}
				}
			});
        
			$A.enqueueAction(action);		
		}
	},

	initTabOptions: function(cmp, tabId) {
		var hotel = cmp.get("v.hotel");
		var memberInitFlg = cmp.get("v.memberInitFlg");
		var userInitFlg = cmp.get("v.userInitFlg");

		var availableAttr = 'v.available';
		var selectedAttr = 'v.selected';
		var availableValues = [];
		var selectedValues = [];

		if (tabId === 'members-tab') { 
			hotel.availableMembers.forEach(function(item){
				availableValues.push({
					attendeeId: item.attendeeId,
					attendeeName: item.attendeeName
				});
			});	

			if (memberInitFlg === true) {
				var selectedMembers = cmp.get("v.selectedMembers");

				selectedMembers.forEach(function(item) {
					var reservations = [];
					var attendeeName = '';
					var confirmationNumber = '';

					var prevDate = {
						reservationDate: null,
						reservationDateDisplay: null
					};

					var nextDate = {
						reservationDate: null,
						reservationDateDisplay: null
					};

					item.reservations.forEach(function(res, i) {
						attendeeName = res.attendeeName;
						confirmationNumber = res.confirmationNumber;

						if (i === 0) {
							var d = new Date(res.reservationDate);

							d.setTime(d.getTime() + (d.getTimezoneOffset() * 60 * 1000));
							d.setDate(d.getDate() - 1);
			
							var month = ('0' + (d.getMonth() + 1)).slice(-2);

							prevDate.reservationDate = d;
							prevDate.reservationDateDisplay = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
						} 

						if (i === item.reservations.length - 1) {
							var d = new Date(res.reservationDate);

							d.setTime(d.getTime() + (d.getTimezoneOffset() * 60 * 1000));
							d.setDate(d.getDate() + 1);
			
							var month = ('0' + (d.getMonth() + 1)).slice(-2);

							nextDate.reservationDate = d;
							nextDate.reservationDateDisplay = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
						}	

						reservations.push({
							hotelAttendeeId: res.hotelAttendeeId,
							attendeeName: res.attendeeName,
							confirmationNumber: res.confirmationNumber,
							cost: res.cost,
							costDisplay: res.costDisplay,
							name: res.name,
							present: res.present,
							reservationDate: res.reservationDate,
							reservationDateDisplay: res.reservationDateDisplay
						});						
					});	
					
					selectedValues.push({
						attendeeId: item.attendeeId,
						attendeeName: attendeeName,
						confirmationNumber: confirmationNumber,
						prevReservationDate: prevDate.reservationDate,
						prevReservationDateDisplay: prevDate.reservationDateDisplay,
						nextReservationDate: nextDate.reservationDate,
						nextReservationDateDisplay: nextDate.reservationDateDisplay,
						reservations: reservations
					});				
				});			
			} else {
				var memberMap = new Map();
				memberMap = hotel.selectedMemberMap;
                
				for(var key in memberMap) {
					var reservations = [];
					var attendeeName = '';
					var confirmationNumber = '';

					var prevDate = {
						reservationDate: null,
						reservationDateDisplay: null
					};

					var nextDate = {
						reservationDate: null,
						reservationDateDisplay: null
					};

					memberMap[key].forEach(function(item, i) {
						attendeeName = item.attendeeName;
						confirmationNumber = item.confirmationNumber;

						if (i === 0) {
							var d = new Date(item.reservationDate);

							d.setTime(d.getTime() + (d.getTimezoneOffset() * 60 * 1000));
							d.setDate(d.getDate() - 1);
			
							var month = ('0' + (d.getMonth() + 1)).slice(-2);

							prevDate.reservationDate = d;
							prevDate.reservationDateDisplay = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
						} 

						if (i === memberMap[key].length - 1) {
							var d = new Date(item.reservationDate);

							d.setTime(d.getTime() + (d.getTimezoneOffset() * 60 * 1000));
							d.setDate(d.getDate() + 1);
			
							var month = ('0' + (d.getMonth() + 1)).slice(-2);

							nextDate.reservationDate = d;
							nextDate.reservationDateDisplay = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
						}						

						reservations.push({
							hotelAttendeeId: item.hotelAttendeeId,
							attendeeName: item.attendeeName,
							confirmationNumber: item.confirmationNumber,
							cost: item.cost,
							costDisplay: item.costDisplay,
							name: item.name,
							present: item.present,
							reservationDate: item.reservationDate,
							reservationDateDisplay: item.reservationDateDisplay
						});
					});

					selectedValues.push({
						attendeeId: key,
						attendeeName: attendeeName,
						confirmationNumber: confirmationNumber,
						prevReservationDate: prevDate.reservationDate,
						prevReservationDateDisplay: prevDate.reservationDateDisplay,
						nextReservationDate: nextDate.reservationDate,
						nextReservationDateDisplay: nextDate.reservationDateDisplay,
						reservations: reservations
					});
				}

				cmp.set("v.memberInitFlg", true);
			}

			availableAttr += 'Members';
			selectedAttr += 'Members';			
		} else {
			hotel.availableUsers.forEach(function(item){
				availableValues.push({
					attendeeId: item.attendeeId,
					attendeeName: item.attendeeName
				});
			});	

			if (userInitFlg === true) {
				var selectedUsers = cmp.get("v.selectedUsers");

				selectedUsers.forEach(function(item) {
					var reservations = [];
					var attendeeName = '';
					var confirmationNumber = '';

					var prevDate = {
						reservationDate: null,
						reservationDateDisplay: null
					};

					var nextDate = {
						reservationDate: null,
						reservationDateDisplay: null
					};
					
					item.reservations.forEach(function(res, i) {
						attendeeName = res.attendeeName;
						confirmationNumber = res.confirmationNumber;

						if (i === 0) {
							var d = new Date(res.reservationDate);

							d.setTime(d.getTime() + (d.getTimezoneOffset() * 60 * 1000));
							d.setDate(d.getDate() - 1);
			
							var month = ('0' + (d.getMonth() + 1)).slice(-2);

							prevDate.reservationDate = d;
							prevDate.reservationDateDisplay = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
						} 

						if (i === item.reservations.length - 1) {
							var d = new Date(res.reservationDate);

							d.setTime(d.getTime() + (d.getTimezoneOffset() * 60 * 1000));
							d.setDate(d.getDate() + 1);
			
							var month = ('0' + (d.getMonth() + 1)).slice(-2);

							nextDate.reservationDate = d;
							nextDate.reservationDateDisplay = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
						}	

						reservations.push({
							hotelAttendeeId: res.hotelAttendeeId,
							attendeeName: res.attendeeName,
							confirmationNumber: res.confirmationNumber,
							cost: res.cost,
							costDisplay: res.costDisplay,
							name: res.name,
							present: res.present,
							reservationDate: res.reservationDate,
							reservationDateDisplay: res.reservationDateDisplay
						});						
					});	
					
					selectedValues.push({
						attendeeId: item.attendeeId,
						attendeeName: attendeeName,
						confirmationNumber: confirmationNumber,
						prevReservationDate: prevDate.reservationDate,
						prevReservationDateDisplay: prevDate.reservationDateDisplay,
						nextReservationDate: nextDate.reservationDate,
						nextReservationDateDisplay: nextDate.reservationDateDisplay,
						reservations: reservations
					});									
				});
			} else {
				var userMap = new Map();
				userMap = hotel.selectedUserMap;

				for(var key in userMap) {
					var reservations = [];
					var attendeeName = '';
					var confirmationNumber = '';

					var prevDate = {
						reservationDate: null,
						reservationDateDisplay: null
					};

					var nextDate = {
						reservationDate: null,
						reservationDateDisplay: null
					};

					userMap[key].forEach(function(item, i) {
						attendeeName = item.attendeeName;
						confirmationNumber = item.confirmationNumber;

						if (i === 0) {
							var d = new Date(item.reservationDate);

							d.setTime(d.getTime() + (d.getTimezoneOffset() * 60 * 1000));
							d.setDate(d.getDate() - 1);
			
							var month = ('0' + (d.getMonth() + 1)).slice(-2);

							prevDate.reservationDate = d;
							prevDate.reservationDateDisplay = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
						} 

						if (i === userMap[key].length - 1) {
							var d = new Date(item.reservationDate);

							d.setTime(d.getTime() + (d.getTimezoneOffset() * 60 * 1000));
							d.setDate(d.getDate() + 1);
			
							var month = ('0' + (d.getMonth() + 1)).slice(-2);

							nextDate.reservationDate = d;
							nextDate.reservationDateDisplay = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
						}			

						reservations.push({
							hotelAttendeeId: item.hotelAttendeeId,
							attendeeName: item.attendeeName,
							confirmationNumber: item.confirmationNumber,
							cost: item.cost,
							costDisplay: item.costDisplay,
							name: item.name,
							present: item.present,
							reservationDate: item.reservationDate,
							reservationDateDisplay: item.reservationDateDisplay
						});
					});

					selectedValues.push({
						attendeeId: key,
						attendeeName: attendeeName,
						confirmationNumber: confirmationNumber,
						prevReservationDate: prevDate.reservationDate,
						prevReservationDateDisplay: prevDate.reservationDateDisplay,
						nextReservationDate: nextDate.reservationDate,
						nextReservationDateDisplay: nextDate.reservationDateDisplay,
						reservations: reservations
					});
				}
				
				cmp.set("v.userInitFlg", true);
			}

			availableAttr += 'Users';	
			selectedAttr += 'Users';					
		}

		cmp.set(availableAttr, availableValues);
		cmp.set(selectedAttr, selectedValues);	
	},

	removeAttendee: function(cmp, attendeeId, type) {
		var selectedAttr = 'v.selected';
		var availableAttr = 'v.available';

		if (type === 'remove-member') { 
			selectedAttr += 'Members';
			availableAttr += 'Members';
		} else {
			selectedAttr += 'Users';
			availableAttr += 'Users';
		}

		var selectedValues = cmp.get(selectedAttr);
		var available = cmp.get(availableAttr);

		var attendee = selectedValues.find(item => item.attendeeId === attendeeId);

		if (attendee === null || attendee === undefined) { return; }

		var selected = selectedValues.filter(function(item) { 
			return item.attendeeId !== attendeeId; 
		});

		available.push({
			attendeeId: attendeeId,
			attendeeName: attendee.attendeeName
		});	

		cmp.set(selectedAttr, selected);
		cmp.set(availableAttr, available);	
	}
})