({
	doInit: function(cmp, event, helper) {
		helper.doInit(cmp);
	},

	getProductCategories: function(cmp, event, helper) {
		helper.getProductCategories(cmp);
	},

	getLicensedStates: function(cmp, event, helper) {
		helper.getLicensedStates(cmp);
	},

	keyUp: function(cmp, event, helper) {
		if(event.keyCode === 13) {
			helper.getKOLContacts(cmp, event);
		}
	},

	getKOLContacts: function(cmp, event, helper) {
		helper.getKOLContacts(cmp, event);
	},

	handlePageChanged: function(cmp, event, helper) {
		helper.handlePageChanged(cmp, event);
	},

	renderData: function(cmp, event, helper) {
		helper.renderData(cmp);
	},

	clearForm: function(cmp, event, helper) {
		helper.clearForm(cmp);
	},

	removeFilter: function(cmp, event, helper) {
		helper.removeFilter(cmp, event);
	},

	navToContact: function(cmp, event, helper) {
		helper.navToContact(cmp, event);
	},

	explainOther: function(cmp, event, helper) {
		helper.explainOther(cmp, event);
	},

	showInfoModal: function(cmp, event, helper) {
		helper.showInfoModal(cmp, event);
	}
})