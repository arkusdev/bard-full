({
	doInit: function(cmp) {
		var action = cmp.get("c.fetchPickListValues");

		action.setParams({
			"fieldNames": cmp.get("v.picklistFields")
		});

		action.setCallback(this, function(response) {
			var state = response.getState();

			if (cmp.isValid() && state == "SUCCESS") {
				var picklistMap = new Map();
				picklistMap = response.getReturnValue();

				for(var field in picklistMap) {
					var attribute = 'v.';

					switch(field) {
						case 'contract_services':
							attribute += 'contractServices';
							break;
						case 'division':
							attribute += 'divisions'
							break;
						case 'product_category':
							attribute += 'productCategories';
							break;
						case 'surgeon_specialty':
							attribute += 'surgeonSpecialties';
							break;
						case 'surgical_technique':
							attribute += 'surgicalTechniques';
							break;
						case 'licensed_regions':
							attribute += 'licensedRegions';
							break;
						case 'licensed_states':
							attribute += 'licensedStates';
							break;
					}
					
					if (attribute != 'v.') {
						var values = [];

						for(var key in picklistMap[field]) {
							values.push({
								key: key,
								value: picklistMap[field][key]
							});
						}

						cmp.set(attribute, values);
					}
				}				
			} else if (state == "ERROR") {
				var errors = action.getError();

				if (errors[0] && errors[0].message) {                         
					console.log(errors[0].message);
					this.showToast('Uh Oh!', 'dismissible', 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.', 'error');
				}
			}
		});

		$A.enqueueAction(action);
	},

	getProductCategories: function(cmp) {
		var divisionName = cmp.find("division").get("v.value");

		var action = cmp.get("c.fetchProductCategories");

		action.setParams({
			"divisionName": divisionName
		});

		action.setCallback(this, function(response) {
			var state = response.getState();

			if (cmp.isValid() && state == "SUCCESS") { 
				var products = response.getReturnValue();

				var values = [];

				products.forEach(function(item) {
					values.push({
						key: item,
						value: item
					});					
				});

				cmp.set('v.productCategories', values);

			} else if (state == "ERROR") {
				var errors = action.getError();

				if (errors[0] && errors[0].message) {                         
					console.log(errors[0].message);
					this.showToast('Uh Oh!', 'dismissible', 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.', 'error');
				}
			}		
		});

		$A.enqueueAction(action);
	},

	getLicensedStates: function(cmp) {
		var regionNames = cmp.find("licensed_regions").get("v.value");

		var action = cmp.get("c.fetchLicensedStates");

		action.setParams({
			"regionNames": regionNames
		});

		action.setCallback(this, function(response) {
			var state = response.getState();

			if (cmp.isValid() && state == "SUCCESS") { 
				var states = new Map();
				states = response.getReturnValue();

				var values = [];

				for(var key in states) {
					values.push({
						key: key,
						value: states[key]
					});
				}

				cmp.set('v.licensedStates', values);
							
			} else if (state == "ERROR") {
				var errors = action.getError();

				if (errors[0] && errors[0].message) {                         
					console.log(errors[0].message);
					this.showToast('Uh Oh!', 'dismissible', 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.', 'error');
				}
			}	
		});

		$A.enqueueAction(action);
	},

	getKOLContacts: function(cmp, event) {
		this.toggleSpinner(cmp, true);

		var paramsVM = [];
		var paramsMap = cmp.get("v.paramsMap");

		for(var item in paramsMap) {
			var itemValue = '';

			switch(item) {
				case 'contract_services':
					//Make sure I've filled out the explain other input when Other is selected before we hit the server
					if (cmp.find(item).get("v.value").split(';').indexOf('Other') > -1) {
						var element = cmp.find("contract_service_desc_div");

						if (cmp.find("contract_service_desc").get("v.value") === '') {
							$A.util.addClass(element, 'is-required slds-has-error');

							this.showToast('Contract Service Error', 'dismissible', 'You must provide a value for this field when Other is selected as a Contract Service', 'error');
							this.toggleSpinner(cmp, false);
							return; 					
						} else {
							$A.util.removeClass(element, 'is-required slds-has-error');
						} 
					}

					itemValue = cmp.find(item).get("v.value");
					break;

				case 'licensed_states':
					var states = cmp.find(item).get("v.value").split(';');

					states.forEach(function(state) {
						itemValue += state.substring(state.indexOf('-') + 1).trim() + ';';
					});

					itemValue = itemValue.substring(0, itemValue.length - 1);
					break;

				default:
					itemValue =  cmp.find(item).get("v.value");
					break;
			}

			if (itemValue !== null && itemValue !== undefined && itemValue !== '') {
				var vm = {
					paramName: item,
					paramValue: itemValue
				}

				if (vm.paramValue != null && vm.paramValue.length) {
					paramsVM.push(vm);
				}			
			}
		}

		if (paramsVM.length > 0) {
			var action = cmp.get("c.fetchKOLContacts");

			var params = JSON.stringify(paramsVM);
			var contractServiceDesc = cmp.find("contract_service_desc").get("v.value");

			action.setParams({
				"params": params,
				"contractServiceDesc": contractServiceDesc
			});

			action.setCallback(this, function(response) {
				var state = response.getState();

				if (cmp.isValid() && state == "SUCCESS") { 
					var contacts = response.getReturnValue();
                    
					if (contacts.length > 0) {
						this.toggleSpinner(cmp, false);

						this.showTable(cmp, true);

						cmp.set("v.contacts", contacts);
						cmp.set("v.maxPage", Math.floor((contacts.length + 9) / 10));

						this.renderData(cmp);

						//Dynamically add span-->badges for each filter criteria to the filter-badges div so users can remove/clear filter criteria one by one by one
						//Be sure to reload the data each time a criteria item is removed via the removeFilter
						var components = [];

						paramsVM.forEach(function(param, i) {
							var displayName = paramsMap[param.paramName];

							components.push([
								"aura:html", {
									"tag": "span",
									"body": displayName + "  ",
									 "HTMLAttributes": {
										"id": "span-" + param.paramName,
										"class": "slds-badge slds-badge_lightest"
									}
								}
							]);

							components.push([
								"lightning:buttonIcon", {
									"aura:id": "btn-" + param.paramName,
									"iconName": "utility:close",
									"variant": "bare",
									"size": "small",
									"alternativeText": "Remove " + displayName + " Filter",
									"onclick": cmp.getReference("c.removeFilter")						
								}
							]);								
						});

						$A.createComponents(components,
							function (items, status, errorMessage) {
								if (status === "SUCCESS") {
									$A.getCallback(function() {
										cmp.set("v.body", []);
									});

									var badges = cmp.get("v.body");
									
									for (var i = 0; i < items.length; i++) {
										var span = items[i];

										var btn = items[i + 1];

										var spanBody = span.get("v.body");

										spanBody.push(btn);
										span.set("v.body", spanBody)										

										badges.push(span);
									}

									cmp.set("v.body", badges);
								}
							}
						);
					} else {
						this.showToast('Oops!', 'dismissible', 'No data was found that matched the entered criteria.', 'info');	
						this.showTable(cmp, false);
						this.toggleSpinner(cmp, false);
					}

				} else if (state == "ERROR") {
					var errors = action.getError();

					if (errors[0] && errors[0].message) {                         
						console.log(errors[0].message);
						this.showToast('Uh Oh!', 'dismissible', 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.', 'error');
						this.toggleSpinner(cmp, false);
					}
				}
			});

			$A.enqueueAction(action);		
		} else {
			this.showToast('Oops!', 'dismissable', 'You must provide at least one search parameter in order to perform this search.', 'warning');
			this.showTable(cmp, false);
			this.toggleSpinner(cmp, false);
		}
	},

	handlePageChanged: function(cmp, event) {
		var pageNumber = event.getParam("pageNumber");

		cmp.set("v.pageNumber", pageNumber);

		this.renderData(cmp);
	},

	renderData: function(cmp) {
		var contacts = cmp.get("v.contacts");
		var pageNumber = cmp.get("v.pageNumber");
		var pageRecords = contacts.slice(((pageNumber - 1) * 10), (pageNumber * 10));

		cmp.set("v.pagedContacts", pageRecords);
	},

	showToast: function(title, mode, message, type) {
		var toast = $A.get("e.force:showToast");

		toast.setParams({
			title: title,
			mode: mode,
			message: message,
			type: type
		});

		toast.fire();		
	},

	showTable: function(cmp, visible) {
		var kol = cmp.find("kol-results");

		var remove = 'slds-hide';
		var add = 'slds-show';

		if (!visible) {
			remove = 'slds-show';
			add = 'slds-hide';
		}

		cmp.set("v.body", []); //Remove any dynamically created components from DOM

		$A.util.removeClass(kol, remove);
		$A.util.addClass(kol, add);
	},

	toggleSpinner: function(cmp, visible) {
        var spinner = cmp.find("spinner");
		
		if (visible === true) {
			$A.util.removeClass(spinner, "slds-hide");	
		} else {
			$A.util.addClass(spinner, "slds-hide");	
		}
        
	},

	clearForm: function(cmp) {
		this.showTable(cmp, false);

		var paramsMap = cmp.get("v.paramsMap");

		for(var item in paramsMap) {
			cmp.find(item).set("v.value", "");
		}

		this.getLicensedStates(cmp);
		this.getProductCategories(cmp);

		var other = cmp.find("contract_service_desc_layout_item");

		$A.util.removeClass(other, 'slds-show');
		$A.util.addClass(other, 'slds-hide');					

		var element = cmp.find("contract_service_desc_layout_item");
		$A.util.removeClass(element, 'is-required slds-has-error');

		cmp.set("v.otherRequired", "false");
	},
	
	removeFilter: function(cmp, event) {
		var id = event.getSource().getLocalId().substring(4);

		cmp.find(id).set("v.value", "");

		this.getKOLContacts(cmp, event);
	},

	navToContact: function(cmp, event) {
		var id = event.srcElement.id;

        var e = $A.get("e.force:navigateToSObject");

        e.setParams({
            "recordId": id
        });

        e.fire();
	},

	explainOther: function(cmp, event) {
		var id = event.getSource().getLocalId();
		var services = cmp.find(id).get("v.value").split(';');
		var container = cmp.find("contract_service_desc_layout_item");
		var element = cmp.find("contract_service_desc_layout_item");

		if (services.indexOf('Other') > -1) {
			$A.util.removeClass(container, 'slds-hide');
			$A.util.addClass(container, 'slds-show');	
			
			cmp.set("v.otherRequired", "true");				
		} else {
			$A.util.removeClass(container, 'slds-show');
			$A.util.addClass(container, 'slds-hide');
			
			$A.util.removeClass(element, 'is-required slds-has-error');

			cmp.set("v.otherRequired", "false");
			cmp.find("contract_service_desc").set("v.value", "");
		}
	},

	showInfoModal: function(cmp, event) {
		$A.createComponents([
			["c:KOLHelpModalHeader", {}],
			["c:KOLHelpModalContent", {}],
			["c:KOLHelpModalFooter", {}]
		], function(components, status) {
			if(status === "SUCCESS") {
				var header = components[0];
				var content = components[1];
				var footer = components[2];

				cmp.find("info-modal").showCustomModal({
					header: header,
					body: content,
					footer: footer,
					showCloseButton: true
				})
			}
		});
	}
})