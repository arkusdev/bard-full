({
	load: function (cmp, event, helper) {
		var params = event.getParam('arguments');

		if (params) {
			helper.doLoad(cmp, params);
			helper.hasRremainingSeats(cmp);
		}
	},

	add: function(cmp, event, helper) {
		helper.doAdd(cmp, event);
		helper.doLoad(cmp, null);
	},

	remove: function(cmp, event, helper) {
		var campaignId = cmp.get("v.id");
        var memberId = event.getSource().get("v.name");
        var type = cmp.get("v.tabValue");
		var key = cmp.get("v.tabKey");
		var members = cmp.get("v.campaignMembers");

        if (memberId === null || memberId === undefined || memberId === '') { 
			var params = {
                title: 'Delete ' + tabKey,
                mode: 'dismissible',
                message: 'Unable to find the correct ' + tabKey + ' to delete.  Please refresh the page and try again.',
                type: 'warning'
			}
                    
			_apostletech.showToast(params);
            
            return; 
        }

		cmp.set("v.selectedMemberId", memberId);

		var member = members.find(item => item.personId === memberId);

		//Don't confirm the waitlist
		switch(key) {
			case 'proctor':
			case 'attendee': 
			case 'user': 
				$A.createComponents([
					["c:ConfirmModalHeader", {
						"headerText": "Delete " + type + "?",
						"iconName": "utility:delete"
					}],
					["c:ConfirmModalContent", {
						"contentText": "Are you sure you want to delete " + member.fullName + " from this campaign?  This action will also remove all associated expense items."
					}],
					["c:ConfirmDeleteCampaignMemberModalFooter", {
						"campaignId": campaignId,
						"memberId": memberId,
						"type": type,
						"key": key,
						"memberFullName": member.fullName 
					}]
				], function(components, status) {
					if(status === "SUCCESS") {
						var header = components[0];
						var content = components[1];
						var footer = components[2];

						cmp.find("confirm-modal").showCustomModal({
							header: header,
							body: content,
							footer: footer,
							showCloseButton: false
						});
					}
				});
				break;
			
			default:
				var obj = {
					campaignId: campaignId,
					memberId: memberId,
					type: type,
					memberFullName: member.fullName
				};

				helper.doRemove(cmp, obj);
				helper.doLoad(cmp, null);
		}
	},

	handleDocumentDataChanged: function(cmp, event, helper) {
		var key = cmp.get("v.tabKey");	
        var type = cmp.get("v.tabValue");

		if (key == undefined || key == null || type == undefined || type == null) { return; }

		var params = {
			tabKey: key,
			tabValue: type
		};

		if (params && (params.tabKey === 'attendee' || params.tabKey === 'user')) {
			helper.doLoad(cmp, params);
		}
	},

	handleMemberDeleted: function(cmp, event, helper) {
		var selectedMemberId = cmp.get("v.selectedMemberId");

		var obj = {
			campaignId: event.getParam("campaignId"),
			memberId: event.getParam("memberId"),
			type: event.getParam("type"),
			memberFullName: event.getParam("memberFullName")
		};		

		//Handles an application level event.  All components will catch this event.
		//Only run the server side logic against the selected member
		if (obj.memberId === selectedMemberId) {
			helper.doRemove(cmp, obj);

			var params = {
				tabKey: event.getParam("key"),
				tabValue: event.getParam("type")
			};

			helper.doLoad(cmp, params);

			helper.raiseDataChanged();
		}
	},

	update: function(cmp, event, helper) {
		var memberId = event.getSource().get("v.name");
		var tabKey = cmp.get("v.tabKey");

		if (tabKey === 'attendee') {
			//If moving an attendee to waitlist then we need to first check if there are any expense items
			var campaignId = cmp.get("v.id");
			var members = cmp.get("v.campaignMembers");
			var type = (tabKey === 'attendee' ? 'Waitlist' : 'Attendee');

			cmp.set("v.selectedMemberId", memberId);

			var member = members.find(item => item.personId === memberId);

			var action = cmp.get("c.hasExpenseItems");

			action.setParams({
				"campaignId": campaignId,
				"memberId": memberId
			});
        
			action.setCallback(this, function(response) {
				var state = response.getState();
            
				if (cmp.isValid() && state == "SUCCESS") {
					var hasExpenseItems = response.getReturnValue();
				
					if (hasExpenseItems === true) {
						$A.createComponents([
							["c:ConfirmModalHeader", {
								"headerText": "Move to " + type.toLowerCase() + "?",
								"iconName": "action:change_owner"
							}],
							["c:ConfirmModalContent", {
								"contentText": "Are you sure you want to move " + member.fullName + " to the " + type.toLowerCase() + "?  This action will remove all expense items associated to this " + tabKey + "."
							}],
							["c:ConfirmUpdateCampaignMemberModalFooter", {
								"campaignId": campaignId,
								"memberId": memberId,
								"type": type
							}]
						], function(components, status) {
							if(status === "SUCCESS") {
								var header = components[0];
								var content = components[1];
								var footer = components[2];

								cmp.find("confirm-modal").showCustomModal({
									header: header,
									body: content,
									footer: footer,
									showCloseButton: false
								});
							}
						});						
					} else {
						helper.doUpdate(cmp, memberId);
						helper.doLoad(cmp, null);			
					}
              
				} else if (state == "ERROR") {
					var errors = action.getError();
                
					if (errors[0] && errors[0].message) {      
						console.error(errors[0].message);

						var params = {
							title: 'Uh Oh!',
							mode: 'dismissible',
							message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
							type: 'error'
						}
                    
						_apostletech.showToast(params);
					}
				}
			});
        
			$A.enqueueAction(action);	
		} else {
			helper.doUpdate(cmp, memberId);
			helper.doLoad(cmp, null);
		}
	},

	handleMemberUpdated: function(cmp, event, helper) {
		var selectedMemberId = cmp.get("v.selectedMemberId");

		var obj = {
			campaignId: event.getParam("campaignId"),
			memberId: event.getParam("memberId")
		};		

		if (obj.memberId === selectedMemberId) {
			helper.doUpdate(cmp, obj.memberId);
			helper.doLoad(cmp, null);

			helper.raiseDataChanged();
		}
	},

	handleRefreshView: function(cmp, event, helper) {
		helper.hasRremainingSeats(cmp);
	},

	manageCredentials: function(cmp, event, helper) {
		var members = cmp.get("v.campaignMembers");
		var attendeeId = event.getSource().get("v.name");
		var type = cmp.get("v.tabKey");	

		var attendee = members.find(item => item.personId === attendeeId);

		$A.createComponents([
			["c:CampaignAttendeeDocumentModalContent", {
				"campaignId": cmp.get("v.id"),
				"attendeeId": attendeeId,
				"attendeeName": attendee.fullName,
				"type": type
			}]
		], function(components, status) {
			if(status === "SUCCESS") {
				var content = components[0];

				cmp.find("doc-modal").showCustomModal({
					body: content,
					showCloseButton: true
				});
			}
		});						
	}
})