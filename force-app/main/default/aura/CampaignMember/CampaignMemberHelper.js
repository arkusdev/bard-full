({
    doLoad: function (cmp, params) {
        var spinner = cmp.find("spinner");
        $A.util.removeClass(spinner, "slds-hide");
        
        var key;
        var type;
        
        if (params !== null) {
            key = params.tabKey;
            type = params.tabValue;		
            
            cmp.set("v.tabKey", key);
            cmp.set("v.tabValue", type);
        } else {
            key = cmp.get("v.tabKey");
            type = cmp.get("v.tabValue");
        }

        var action = cmp.get("c.fetchAllCampaignMembers");
        
        action.setParams({
            "campaignId": cmp.get("v.id"),
            "type": type
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (cmp.isValid() && state == "SUCCESS") {
                var memberMap = new Map();
                memberMap = response.getReturnValue();
                
                for(var key in memberMap) {
					var attribute = 'v.' + key;
					var values = memberMap[key];
                    
					switch (key) {
						case 'campaignMembers':                  
							cmp.set(attribute, values);
						break;

						case 'omitIdList':
							var omit = [];

							values.forEach(function(item) {
								omit.push(item.personId);
							});

							cmp.set(attribute, omit);
						break;
					}
                }
                
            } else if (state == "ERROR") {
                var errors = action.getError();
                
                if (errors[0] && errors[0].message) {                         
                    console.error(errors[0].message);

                    var params = {
                        title: 'Uh Oh!',
                        mode: 'dismissible',
                        message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
                        type: 'error'
                    }
                    
					_apostletech.showToast(params);
                }
            }
            
            $A.util.addClass(spinner, "slds-hide");
        });
        
        $A.enqueueAction(action);
    },
    
    doAdd: function(cmp, event) {
        var campaignId = cmp.get("v.id");
		var selectedRecord = cmp.get("v.selectedRecord");
        var type = cmp.get("v.tabValue");
        
        if (selectedRecord.Id === null || selectedRecord.Id === undefined || selectedRecord.Id === '') { 
            var params = {
                title: 'Select ' + type,
                mode: 'dismissible',
                message: 'You must make a selection from the ' + type + ' picklist before clicking Add.',
                type: 'warning'
            }
                    
			_apostletech.showToast(params);
            
            return; 
        }

        var action = cmp.get("c.addMember");
        
        action.setParams({
            "campaignId": campaignId,
            "memberId": selectedRecord.Id,
            "type": type
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (cmp.isValid() && state == "SUCCESS") {              
                var params = {
                    title: 'Successfully Added!',
                    mode: 'dismissible',
                    message: selectedRecord.Name + ' was successfully added to campaign as a ' +  type + '!' ,
                    type: 'success'
                }
                    
				_apostletech.showToast(params);                

				$A.get('e.force:refreshView').fire(); //Need to refresh Remaing Seats field

				var event = $A.get("e.c:ApostleTechLookupClearSelectedRecord");
				event.fire();
            } else if (state == "ERROR") {
                var errors = action.getError();
                
                if (errors[0] && errors[0].message) {
                    var error = JSON.parse(errors[0].message);
                    
                    console.error(error);

                    var params = {
                        title: error.name,
                        mode: 'dismissible',
                        message: error.message,
                        type: 'error'
                    }
                    
					_apostletech.showToast(params);
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    
    doRemove: function(cmp, obj) {
        var action = cmp.get("c.removeMember");
        
        action.setParams({
            "campaignId": obj.campaignId,
            "memberId": obj.memberId,
            "type": obj.type	
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (cmp.isValid() && state == "SUCCESS") {
                var params = {
                    title: 'Successfully Removed!',
                    mode: 'dismissible',
                    message: obj.memberFullName + ' successfully removed from campaign!' ,
                    type: 'success'
                }
                    
				_apostletech.showToast(params);

				cmp.set("v.selectedMemberId", null);

				$A.get('e.force:refreshView').fire(); //Need to refresh Remaing Seats field
            } else if (state == "ERROR") {
                var errors = action.getError();
                
                if (errors[0] && errors[0].message) {                         
                    console.error(errors[0].message);

                    var params = {
                        title: 'Uh Oh!',
                        mode: 'dismissible',
                        message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
                        type: 'error'
                    }
                    
					_apostletech.showToast(params);
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    
    doUpdate: function(cmp, memberId) {
        var campaignId = cmp.get("v.id");
        var tabKey = cmp.get("v.tabKey");
        var type = (tabKey === 'attendee' ? 'Waitlist' : 'Attendee');
        var tabValue = cmp.get("v.tabValue");
		var members = cmp.get("v.campaignMembers");

        if (memberId === null || memberId === undefined || memberId === '') { 
            var params = {
                title: 'Move ' + tabKey,
                mode: 'dismissible',
                message: 'Unable to find the correct ' + tabKey + ' to move.  Please refresh the page and try again.',
                type: 'warning'
            }
                    
			_apostletech.showToast(params);
            
            return; 
        }

		var member = members.find(item => item.personId === memberId);
        
        var action = cmp.get("c.updateMember");
        
        action.setParams({
            "campaignId": campaignId,
            "memberId": memberId,
            "type": type
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (cmp.isValid() && state == "SUCCESS") {
				var params = {
                    title: 'Successfully Moved!',
                    mode: 'dismissible',
                    message: member.fullName + ' successfully moved to the ' + type + (type === 'Attendee' ? ' list' : '') + ' under this campaign!' ,
                    type: 'success'
				}
                    
				_apostletech.showToast(params);

				$A.get('e.force:refreshView').fire(); //Need to refresh Remaing Seats field
            } else if (state == "ERROR") {
                var errors = action.getError();
                
                if (errors[0] && errors[0].message) {                         
                    console.error(errors[0].message);

					var params = {
						title: 'Uh Oh!',
						mode: 'dismissible',
						message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
						type: 'error'
					}
                    
					_apostletech.showToast(params);
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    
    hasRremainingSeats: function(cmp) {
        var key = cmp.get("v.tabKey");
        
        switch(key) {
            case 'proctor':
                cmp.set("v.disableAdd", false);
                cmp.set("v.disableMove", false);
                break;
                
            default:
                var campaignId = cmp.get("v.id");
                
                var action = cmp.get("c.hasAvailableSeats");
                
                action.setParams({
                    "campaignId": campaignId
                });
                
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    
                    if (cmp.isValid() && state == "SUCCESS") {
                        var hasRremainingSeats = response.getReturnValue();
                        
                        if (key === 'attendee') {
                            cmp.set("v.disableMove", false);
                            cmp.set("v.disableAdd", !hasRremainingSeats);
                        } else {
                            cmp.set("v.disableMove", !hasRremainingSeats);
                            cmp.set("v.disableAdd", false);	
                        }
                        
                    } else if (state == "ERROR") {
                        var errors = action.getError();
                        
                        if (errors[0] && errors[0].message) {                         
                            console.error(errors[0].message);
                            //If this throws and error then do nothing else.  Server side logic will catch this anyways.
                        }
                    }
                });
                
                $A.enqueueAction(action);
                break;
        }
    },

	raiseDataChanged: function() {
		var mdc = $A.get("e.c:CampaignMealDataChanged");
		mdc.fire();

		var fdc = $A.get("e.c:CampaignFlightDataChanged");
		fdc.fire();

		var hdc = $A.get("e.c:CampaignHotelDataChanged");
		hdc.fire();

		var tdc = $A.get("e.c:CampaignTransportationDataChanged");
		tdc.fire();

		var idc = $A.get("e.c:CampaignIncidentalDataChanged");
		idc.fire();

		var pdc = $A.get("e.c:CampaignProctorFeeDataChanged");
		pdc.fire();		
	}
})