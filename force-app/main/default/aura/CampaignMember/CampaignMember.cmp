<aura:component controller="CampaignMemberController">
    <ltng:require scripts="{!$Resource.apostletech}" />
	<aura:handler event="c:CampaignMemberDeleted" action="{!c.handleMemberDeleted}" />
	<aura:handler event="c:CampaignMemberUpdated" action="{!c.handleMemberUpdated}" />
	<aura:handler event="c:CampaignAttendeeDocumentDataChanged" action="{!c.handleDocumentDataChanged}" />
	<aura:handler event="force:refreshView" action="{!c.handleRefreshView}" />

    <aura:attribute name="id" type="Id" />
	<aura:attribute name="selectedMemberId" type="Id" />
    <aura:attribute name="disableAdd" type="boolean" default="false" />
    <aura:attribute name="disableMove" type="boolean" default="false" />
    <aura:attribute name="tabKey" type="string" />
    <aura:attribute name="tabValue" type="string" />
    <aura:attribute name="campaignMembers" type="List" />
	<aura:attribute name="omitIdList" type="List" />
	<aura:attribute name="selectedRecord" type="sObject" default="{}"/>
    
    <aura:method name="loadComponent" action="{!c.load}" description="Method that refreshes data with selected tab">
        <aura:attribute name="tabKey" type="string" />
        <aura:attribute name="tabValue" type="string" />
    </aura:method> 
    <lightning:spinner aura:id="spinner" variant="brand" alternativeText="Loading members ..." title="Loading members ..." class="slds-hide"/>
	<lightning:overlayLibrary aura:id="confirm-modal"/>
	<lightning:overlayLibrary aura:id="doc-modal"/>	
    <lightning:layout >
        <lightning:layoutItem size="12" smallDeviceSize="12" mediumDeviceSize="12" largeDeviceSize="12">
            <lightning:layout class="slds-gutters slds-m-vertical_medium slds-grid--vertical-align-end">
                <lightning:layoutItem size="10" smallDeviceSize="10" mediumDeviceSize="10" largeDeviceSize="10" padding="around-small">				
					<aura:if isTrue="{!v.tabKey != 'user'}">
						<c:ApostleTechLookup objectAPIName="Contact" iconName="standard:contact" record="{!v.selectedRecord}" label="{!v.tabValue}" omitIdList="{!v.omitIdList}" />
					<aura:set attribute="else">
						<c:ApostleTechLookup objectAPIName="User" iconName="standard:user" record="{!v.selectedRecord}" label="{!v.tabValue}" omitIdList="{!v.omitIdList}" />
					</aura:set>
					</aura:if>
                </lightning:layoutItem>
                <lightning:layoutItem size="2" smallDeviceSize="2" mediumDeviceSize="2" largeDeviceSize="2" padding="around-small">
                    <div class="slds-float_right"> 
                        <lightning:button aura:id="add-member" iconName="utility:adduser" variant="brand" label="Add" title="{!'Add ' + v.tabValue}" onclick="{!c.add}" disabled="{!v.disableAdd}" />
                    </div>
                </lightning:layoutItem>
            </lightning:layout>
			<aura:if isTrue="{!v.disableAdd}">
				<lightning:layout>
					<lightning:layoutItem size="12" smallDeviceSize="12" mediumDeviceSize="12" largeDeviceSize="12" padding="around-small">				
						<c:CampaignMemberAlert alertText="There are no more seats available for this campaign." alertType="error" />
					</lightning:layoutItem>
				</lightning:layout>				
			</aura:if>
            <hr />
            <ul class="slds-has-dividers_bottom-space">
                <aura:iteration items="{!v.campaignMembers}" var="dto">
                    <li class="slds-item">
                        <lightning:tile label="{!dto.fullName}" href="{!dto.pageURL}">
                            <aura:set attribute="media">
                                <lightning:avatar src="{!dto.photoURL}" alternativeText="{!dto.fullName}" fallbackIconName="standard:avatar" class="slds-avatar slds-avatar_circle slds-avatar_large" />
                            </aura:set>
                            <aura:if isTrue="{!v.tabKey == 'proctor'}">
								<aura:if isTrue="{!dto.alertReqFieldMissingFlg}">
									<c:CampaignMemberAlert alertText="{!dto.fullName + ' is missing a CV attachment, a Bio, or an Image.'}" alertType="warning" />
								</aura:if>
								<aura:if isTrue="{!dto.alertNoContractFlg}">
									<c:CampaignMemberAlert alertText="{!dto.fullName + ' does not have any contracts on file.'}" alertType="warning" />
								</aura:if>
								<aura:if isTrue="{!dto.alertExpiringContractFlg}">
									<c:CampaignMemberAlert alertText="{!dto.fullName + ' has at least one contract set to expire in the next 90 days!'}" alertType="warning" />
								</aura:if>
								<aura:if isTrue="{!dto.alertExpiredContractFlg}">
									<c:CampaignMemberAlert alertText="{!dto.fullName + ' has at least one contract expired before the campaign end date.'}" alertType="warning" />
								</aura:if>
                            </aura:if>
							<aura:if isTrue="{!dto.alertOverlappingCourseFlg}">
								<c:CampaignMemberAlert alertText="{!dto.fullName + ' is already associated to another Course during this timeframe.'}" alertType="warning" />
							</aura:if>
							<aura:if isTrue="{!dto.alertMissingCredsFlg}">
								<aura:if isTrue="{!dto.credsDueDayCount >= 0}">
									<c:CampaignMemberAlert alertText="{!dto.fullName + ' is missing credential documents due in ' + dto.credsDueDayCount + ' days!.'}" alertType="warning" />
                                <aura:set attribute="else">
                                    <c:CampaignMemberAlert alertText="{!dto.fullName + ' is missing credential documents past the due date!'}" alertType="error" />                    
                                </aura:set>
								</aura:if>	
							</aura:if>
                            <div class="slds-tile__detail">
                                <lightning:layout >
                                    <lightning:layoutItem size="10" smallDeviceSize="10" mediumDeviceSize="10" largeDeviceSize="10" padding="around-small">
                                        <dl class="slds-list_horizontal slds-wrap">
                                            <aura:if isTrue="{!v.tabKey != 'user'}">
                                                <dt class="slds-item_label slds-text-color_weak slds-truncate" title="Account">Account:</dt>
                                                <dd class="slds-item_detail slds-truncate" title="Account Name">{!dto.accountName}</dd>
                                                <dt class="slds-item_label slds-text-color_weak slds-truncate" title="Email">Email:</dt>
                                                <dd class="slds-item_detail slds-truncate slds-tile__meta" title="Email">{!dto.email}</dd>
                                                <dt class="slds-item_label slds-text-color_weak slds-truncate" title="Phone Number">Phone:</dt>
                                                <dd class="slds-item_detail slds-truncate" title="Phone">{!dto.phone}</dd>
                                                <dt class="slds-item_label slds-text-color_weak slds-truncate" title="Bio">Bio:</dt>
                                                <dd class="slds-item_detail slds-truncate" title="Bio">{!dto.bio}</dd>
												<aura:if isTrue="{!v.tabKey == 'attendee'}">
													<dt class="slds-item_label slds-text-color_weak slds-truncate" title="Credentialing Documents Received">Credentialing Documents Received:</dt>
													<dd class="slds-item_detail slds-truncate" title="Credentialing Documents Received">{!dto.credTracking}</dd>
												</aura:if>
                                                <aura:if isTrue="{!dto.hasContracts}">
                                                    <dd class="slds-item_detail slds-text-color_weak slds-truncate" title="Contract(s)">
                                                        <ul>
                                                            <aura:iteration items="{!dto.contracts}" var="c">
                                                                <li>Contract #: <a href="{!c.pageURL}">{!c.contractNumber}</a></li>
                                                                <li>
                                                                    <ul class="slds-list_dotted slds-is-nested">
                                                                        <li>Contract Services: {!c.contractServices}</li>
                                                                        <li>Start Date: {!c.startDate}</li>
                                                                        <li>
                                                                            End Date: {!c.endDate}
                                                                            <aura:if isTrue="{!and(v.tabKey == 'proctor', c.alertFlg)}">
                                                                                <span class="slds-icon_container slds-icon-utility-warning slds-m-left_small" title="Warning - Contract set to expire within 90 days">
                                                                                    <lightning:icon iconName="utility:warning" variant="warning" size="x-small"/>
                                                                                </span>
                                                                            </aura:if>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </aura:iteration>
                                                        </ul>
                                                    </dd>			
                                                    <aura:set attribute="else">
                                                        <dd class="slds-item_detail slds-text-color_weak slds-truncate" title="Contract(s)">No contract history with this contact</dd>
                                                    </aura:set>
                                                </aura:if>
                                            <aura:set attribute="else">
                                                <dt class="slds-item_label slds-text-color_weak slds-truncate" title="Title">Title:</dt>
                                                <dd class="slds-item_detail slds-truncate" title="Title">{!dto.title}</dd>
                                                <dt class="slds-item_label slds-text-color_weak slds-truncate" title="Email">Email:</dt>
                                                <dd class="slds-item_detail slds-truncate slds-tile__meta" title="Email">{!dto.email}</dd>
                                                <dt class="slds-item_label slds-text-color_weak slds-truncate" title="Phone Number">Phone:</dt>
                                                <dd class="slds-item_detail slds-truncate" title="Phone">{!dto.phone}</dd>
												<dt class="slds-item_label slds-text-color_weak slds-truncate" title="Credentialing Documents Received">Credentialing Documents Received:</dt>
												<dd class="slds-item_detail slds-truncate" title="Credentialing Documents Received">{!dto.credTracking}</dd>
                                            </aura:set>
                                            </aura:if>	
                                        </dl>
                                    </lightning:layoutItem>	
                                    <lightning:layoutItem size="2" smallDeviceSize="2" mediumDeviceSize="2" largeDeviceSize="2" padding="around-small">
                                        <lightning:layout multipleRows="true" verticalAlign="center" horizontalAlign="end" pullToBoundary="small">
                                            <lightning:layoutItem size="12" smallDeviceSize="12" mediumDeviceSize="12" largeDeviceSize="12" padding="around-small">	
                                                <aura:if isTrue="{!or(v.tabKey == 'attendee', v.tabKey == 'user')}">
                                                    <div class="slds-float_right"> 
														<lightning:button aura:id="manage-creds" name="{!dto.personId}" iconName="utility:file" variant="neutral" title="Track credential documents" onclick="{!c.manageCredentials}">Credentials</lightning:button>
                                                    </div>
                                                </aura:if>
                                            </lightning:layoutItem>	
                                            <lightning:layoutItem size="12" smallDeviceSize="12" mediumDeviceSize="12" largeDeviceSize="12" padding="around-small">	
                                                <div class="slds-float_right"> 
                                                    <lightning:button aura:id="remove-member" name="{!dto.personId}" iconName="utility:delete" variant="Destructive" title="{!'Remove ' + v.tabKey + ' from event'}" onclick="{!c.remove}">Remove</lightning:button>
                                                </div>
                                            </lightning:layoutItem>	
                                            <lightning:layoutItem size="12" smallDeviceSize="12" mediumDeviceSize="12" largeDeviceSize="12" padding="around-small">	
                                                <aura:if isTrue="{!or(v.tabKey == 'attendee', v.tabKey == 'waitlist')}">
                                                    <div class="slds-float_right"> 
                                                        <lightning:button aura:id="move-member" name="{!dto.personId}" iconName="action:change_owner" variant="Brand" title="{!'Move ' + v.tabKey}" onclick="{!c.update}" disabled="{!v.disableMove}">
                                                            <aura:if isTrue="{!v.tabKey == 'attendee'}">Waitlist<aura:set attribute="else">Attendees</aura:set></aura:if>
                                                        </lightning:button>
                                                    </div>
                                                </aura:if>
                                            </lightning:layoutItem>	
                                        </lightning:layout>
                                    </lightning:layoutItem>	
                                </lightning:layout>
                            </div>
                        </lightning:tile>
                    </li>
                </aura:iteration>
            </ul>
        </lightning:layoutItem>
    </lightning:layout>
</aura:component>