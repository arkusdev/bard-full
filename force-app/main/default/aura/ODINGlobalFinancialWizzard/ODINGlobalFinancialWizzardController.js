({
    doInit : function(component, event, helper) {
        helper.util = component.find('util');
        helper.onloadData(component, event, helper);
    },
    close : function(component, event, helper) {
        helper.close(component, event, helper);
    },
    handleCancelClick : function(component, event, helper) {
        if (component.get('v.viewSelected')){
            component.set('v.viewSelected', false);
        }else{
            helper.handleCancelClick(component, event, helper);
        }
    },
    handleNextClick : function(component, event, helper) {
        helper.handleNextClick(component, event, helper);
    },
    handleInterestChange : function(component, event, helper) {
        helper.handleInterestChange(component, event, helper);
    },
    handleFinancialChange : function(component, event, helper) {
        helper.handleFinancialChange(component, event, helper);
    },
    handleChange : function(component, event, helper) {
        helper.handleChange(component);
    },
    handleSaveDraft : function(component, event, helper) {
        helper.handleSaveDraft(component, event, helper);
    },
    handleProductChange : function(component, event, helper) {
        helper.handleProductChange(component, event, helper);
    },
    handlePaste : function(component, event, helper) {
        var value = event.getSource().get("v.value");
        var name = event.getSource().get("v.name");
        if(value && (value.toString().endsWith('.') || value.toString().endsWith('0')) && !value.includes('	')){
            return;
        }
        helper.handlePaste(component, value, name);
    },
    changeToPasted : function(component, event, helper) {
        event.getSource().set("v.value", '');
        helper.changeToPasted(component, true);
    },
    handleRegionChange : function(component, event, helper){
        helper.handleRegionChange(component, event);
    },
    noInterestChanged : function(component, event){
        component.set('v.saveDisabled', !event.getParam("value"));
    },
    handleViewClick : function(component, event, helper){
        component.set('v.viewSelected', true);
        helper.handleViewClick(component);
    },
    handleDoneClick : function(){
        $A.get("e.force:closeQuickAction").fire();
    }
})