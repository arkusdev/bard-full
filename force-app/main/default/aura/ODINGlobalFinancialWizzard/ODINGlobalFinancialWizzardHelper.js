({  
    onloadData : function(component,event,helper) {
        var self = this;
        self.countryWithFinancials = new Map();
        var action = component.get("c.GetProjectInfo");

        action.setParams({"recordId" : component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var info = response.getReturnValue();
            if (!info.hasProduct) {
                helper.showNoProductToast(component, event, helper);
                $A.get("e.force:closeQuickAction").fire();
            } else {
                if (info.quarter && info.quarter != null) {
                    component.find("Launch_Quarter__c").set("v.value", info.quarter);
                    component.get("v.financial").Launch_Quarter__c = info.quarter;
                }
                if (info.year && info.year != null) {
                    component.find("Year__c").set("v.value", info.year);
                    component.get("v.financial").Year__c = info.year;
                }
            }
            var regions = [];
            var regionsLabels = info.regionsLabels;
            for (var key in regionsLabels) {
                var item = {
                    key: key,
                    label: regionsLabels[key]
                }
                regions.push(item);
            }
            var countryHasFinancials = info.countryHasFinancials;
            for (var key in countryHasFinancials){
                self.countryWithFinancials.set(key, countryHasFinancials[key]);
            }
            component.find('Region__c').set('v.value', regions[0].key);
            component.set('v.hasFinancials', self.countryWithFinancials.get(component.get('v.financial').Country__c));
            component.set('v.regions', regions);
        });
        $A.enqueueAction(action);     
    },
    
    close : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },

    handleNextClick : function(component, event, helper) {
        var self = this;
        var step = component.get("v.step");
        if (step == "1") {
            component.set('v.noInterestSelected', (Array.isArray(component.find('Level__c')) ? component.find('Level__c')[component.find('Level__c').length - 1].get('v.value') : component.find('Level__c').get('v.value')) == '0 = No Interest');
            if (helper.isValidCountrySection(component, event, helper)) {
                var noInterestSelected = component.get("v.noInterestSelected");
                if (component.get("v.draftSaved")) {
                    if (component.get("v.financials").length > 0) {
                        component.set("v.cancelLabel", "Back");
                        component.set("v.headerText", "Enter Year One Financials for " + component.get("v.interest").Country__c);
                        component.set("v.step", "2");
                    } else {
                        component.set("v.cancelLabel", "Back");
                        component.set("v.headerText", "Confirm or Edit Financials for " + component.get("v.interest").Country__c);
                        component.set("v.step", "3");
                    }
                } else {
                    component.get("v.financial").Project__c = component.get("v.recordId");
                    var action = component.get("c.GetFinancialInfo");
                    action.setParams({  
                                        "recordId" : component.get("v.recordId"),
                                        "country" : component.get("v.interest").Country__c,
                                        "fromViewAll" : false
                                    });
                    action.setCallback(this, function(response) {
                        var financials = response.getReturnValue();
                        if (financials.length > 0 && !financials[0].hasData) {
                            component.set("v.financials", financials);
                            if (noInterestSelected){
                                self.createFinancialInfo(component);
                                component.set("v.step", "5"); 
                                component.set("v.headerText", "Enter the reason why you are not interested");
                                component.set("v.cancelLabel", "Back");
                                component.set("v.nextLabel", "Save");
                                component.set('v.saveDisabled', component.get("v.interest").No_Interest_Reason__c == undefined);
                            }else{
                                component.set("v.financials", financials);
                                component.set("v.cancelLabel", "Back");
                                component.set("v.headerText", "Enter Year One Financials for " + component.get("v.interest").Country__c);
                                component.set("v.step", "2");
                            }
                        } else {
                            component.set("v.financials", financials);
                            component.set("v.cancelLabel", "Back");
                            var action = component.get("c.CreateFinancialInfo");
                            action.setParams({  
                                                "sampleFinancial" : component.get("v.financial"),
                                                "financials" : []
                                            });
                            action.setCallback(this, function(response) {
                                var wrapper = response.getReturnValue();
                                component.set("v.wrapper", wrapper);
                                if (noInterestSelected){
                                    component.set("v.step", "5"); 
                                    component.set("v.headerText", "Enter the reason why you are not interested");
                                    component.set("v.cancelLabel", "Back");
                                    component.set("v.nextLabel", "Save");
                                    component.set('v.saveDisabled', component.get("v.interest").No_Interest_Reason__c == undefined);
                                }else{
                                    component.set("v.headerText", "Enter Year One Financials for " + component.get("v.interest").Country__c);
                                    component.set("v.step", "2");
                                }
                            })
                            $A.enqueueAction(action);
                        }
                    })
                    $A.enqueueAction(action);
                }
            } else {
                helper.showCountryToast(component, event, helper);
            }
        } else if (step == "2") {
            if (helper.isValidProductSection(component, event, helper)) {
                if (component.get("v.draftSaved")) {
                    component.set("v.headerText", "Confirm or Edit Financials for " + component.get("v.interest").Country__c);
                    component.set("v.step", "3");
                
                } else {
                    if (!component.get('v.financials')[0].hasData){
                        self.createFinancialInfo(component);
                    }
                    else{
                        var wrapper = component.get('v.wrapper');
                        var index = 0;
                        wrapper.financialsProjectPerYearList.forEach(element => {
                            element[0].record = component.get('v.financials')[index].record;
                            index++;
                        });
                        //wrapper.financialsProjectPerYearList[0][0].record = component.get('v.financials')[0].record;
                        component.set('v.wrapper', wrapper);
                        helper.handleChange(component);
                    }
                    component.set("v.headerText", "Confirm or Edit Financials for " + component.get("v.interest").Country__c);
                    component.set("v.step", "3");
                }
                
            } else {
                helper.showProductInvalidToast(component, event, helper);
            }
        
        } else if (step == "3") {
            if (helper.isValidProductSection(component, event, helper)) {

                var action = component.get("c.GetProjectQuestions");
                action.setParams({  "recordId" : component.get("v.recordId"),
                                    "country" : component.get("v.interest").Country__c,
                                    "region" : component.get("v.interest").Region__c,
                                });
                action.setCallback(this, function(response) {
                    var wrapper = response.getReturnValue();
                    component.set("v.questionWrapper", wrapper);
                    component.set("v.headerText", "Specify Financial Assumptions and Answer Questions for " + component.get("v.interest").Country__c);
                    component.set("v.nextLabel", "Save");
                    component.set("v.step", "4");
                })
                $A.enqueueAction(action);
            
            } else {
                helper.showProductInvalidToast(component, event, helper);
            }
        } else if (step == "4") {
            if (helper.isValidQuestions(component, event, helper)) {
                var answers = []
                component.get("v.questionWrapper").requiredQuestions.forEach(element => {
                    element.answer.Created_From__c = 'GF';
                    answers.push(element.answer);
                });
                component.get("v.questionWrapper").questions.forEach(element => {
                    element.answer.Created_From__c = 'GF';
                    answers.push(element.answer);
                });
                component.get("v.interest").Created_From__c = 'GF';
                component.get("v.interest").Project__c = component.get("v.recordId");
                component.get("v.interest").No_Interest_Reason_Text__c = '';
                component.get("v.interest").No_Interest_Reason__c = '';
                var action = component.get("c.FinalSave");
                action.setParams({  "financialJson" : JSON.stringify(component.get("v.wrapper")),
                                    "answers" : answers,
                                    "interest" : component.get("v.interest"),
                                    "sampleFinancial" : component.get("v.financial")
                                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state == "SUCCESS") {
                        helper.showSuccessToast(component, event, helper);
                        $A.get("e.force:closeQuickAction").fire();
                    }else{
                        var errors = action.getError();
                        self.util.showErrorToast({ 'errorMessage': errors ? errors[0].message : undefined});
                    }
                });
                $A.enqueueAction(action);
            } else {
                helper.showQuestionToast(component, event, helper);
            }
        } else if (step == "5"){
            if (component.get("v.draftSaved")) {
                if (component.get("v.financials").length > 0) {
                    component.set("v.cancelLabel", "Back");
                    component.set("v.headerText", "Enter Year One Financials for " + component.get("v.interest").Country__c);
                    component.set("v.step", "2");
                } else {
                    component.set("v.cancelLabel", "Back");
                    component.set("v.headerText", "Confirm or Edit Financials for " + component.get("v.interest").Country__c);
                    component.set("v.step", "3");
                }
            } else {
                component.get("v.interest").Project__c = component.get("v.recordId");
                component.get("v.financial").Project__c = component.get("v.recordId");
                component.get("v.interest").Created_From__c = component.get('v.createdFrom');
                component.get("v.interest").No_Interest_Reason_Text__c = component.get('v.noInterestReasonText');
                component.get("v.interest").No_Interest_Reason__c = component.get('v.noInterestReason');

                var action = component.get("c.FinalSave");
                action.setParams({  "financialJson" : JSON.stringify(component.get("v.wrapper")),
                                    "answers" : [],
                                    "interest" : component.get("v.interest"),
                                    "sampleFinancial" : component.get("v.financial")
                                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state == "SUCCESS") {
                        helper.showSuccessToast(component, event, helper);
                        $A.get("e.force:closeQuickAction").fire();
                    }else{
                        var errors = action.getError();
                        self.util.showErrorToast({ 'errorMessage': errors ? errors[0].message : undefined});
                    }
                });
                $A.enqueueAction(action);
            }
        }
    },

    handleCancelClick : function(component, event, helper) {
        var step = component.get("v.step");
        component.set('v.saveDisabled', false);
        if (step == "1") {
            $A.get("e.force:closeQuickAction").fire();
        } else if (step == "2") {
            component.set("v.cancelLabel", "Cancel");
            component.set("v.headerText", "Select Country and Year");
            component.set("v.step", "1");
            helper.loadForm(component, event, helper);
        } else if (step == "3") {
            if (component.get('v.noInterestSelected')){
                component.set("v.step", "5");
                component.set("v.nextLabel", "Save");
                component.set("v.headerText", "Enter the reason why you are not interested");
            }else{
                if (component.get("v.financials").length > 0) {
                    component.set("v.headerText", "Enter Year One Financials for " + component.get("v.interest").Country__c);
                    component.set("v.step", "2");
                } else {
                    component.set("v.cancelLabel", "Cancel");
                    component.set("v.headerText", "Select Country and Year");
                    component.set("v.step", "1");
                    helper.loadForm(component, event, helper);
                }
            }
        } else if (step == "4") {
            component.set("v.headerText", "Confirm or Edit Financials for " + component.get("v.interest").Country__c);
            component.set("v.nextLabel", "Next");
            component.set("v.step", "3");
        } else if (step == "5"){
            component.set("v.step", "1");
            component.set("v.nextLabel", "Next");
            component.set("v.headerText", "Select Country and Year");
            helper.loadForm(component, event, helper);
        }
    },

    loadForm : function(component, event, helper) {
        var level = Array.isArray(component.find("Level__c")) ? component.find("Level__c")[component.find("Level__c").length - 1] : component.find("Level__c");
        var country = Array.isArray(component.find("Country__c")) ? component.find("Country__c")[component.find("Country__c").length - 1] : component.find("Country__c");
        var region = Array.isArray(component.find("Region__c")) ? component.find("Region__c")[component.find("Region__c").length - 1] : component.find("Region__c");
        var regionSelector = Array.isArray(component.find("RegionSelector")) ? component.find("RegionSelector")[component.find("RegionSelector").length - 1] : component.find("RegionSelector");
        var year = Array.isArray(component.find("Year__c")) ? component.find("Year__c")[component.find("Year__c").length - 1] : component.find("Year__c");
        var launchQuarter = Array.isArray(component.find("Launch_Quarter__c")) ? component.find("Launch_Quarter__c")[component.find("Launch_Quarter__c").length - 1] : component.find("Launch_Quarter__c");

        level.set("v.value", component.get("v.interest.Level__c"));
        country.set("v.value", component.get("v.interest.Country__c"));
        region.set("v.value", component.get("v.interest.Region__c"));
        regionSelector.set("v.value", component.get("v.interest.Region__c"));

        year.set("v.value", component.get("v.financial.Year__c"));
        launchQuarter.set("v.value", component.get("v.financial.Launch_Quarter__c"));
    },

    handleInterestChange : function(component, event, helper) {
        var field = event.getSource().getLocalId();
        var value = event.getSource().get("v.value");
        var level = Array.isArray(component.find('Level__c')) ? component.find('Level__c')[component.find('Level__c').length - 1] : component.find('Level__c');
        component.set('v.noInterestSelected', level.get('v.value') == '0 = No Interest');
        var oldValue = component.get("v.interest").Country__c;
        component.set(`v.interest.${field}`, value);

        if (field == "Country__c" || field == "Region__c") {
            helper.handleFinancialChange(component, event, helper);
            component.set("v.draftSaved", false);
        }
        
        if (field == "Country__c" && value != oldValue) {
            component.set('v.hasFinancials', this.countryWithFinancials.get(value));
            var action = component.get("c.GetInterestWYear");
            action.setParams({  "recordId" : component.get("v.recordId"),
                                "country" : component.get("v.interest").Country__c
                            });
            action.setCallback(this, function(response) {
                var wrapper = response.getReturnValue();
                if (wrapper.interest) {
                    component.set("v.interest", wrapper.interest);
                    component.set("v.noInterestReason", wrapper.interest.No_Interest_Reason__c);
                    component.set("v.noInterestReasonText", wrapper.interest.No_Interest_Reason_Text__c);
                }else{
                    component.set("v.interest.Id", undefined);
                }
                var level = null;
                if (wrapper.interest != null) {
                    level = wrapper.interest.Level__c;
                }

                if (component.find("Level__c").length > 0) {
                    component.find("Level__c")[component.find("Level__c").length - 1].set("v.value", level);
                } else {
                    component.find("Level__c").set("v.value", level);
                }

                if (wrapper.year) {

                    if (component.find("Year__c").length > 0) {
                        component.find("Year__c")[component.find("Year__c").length - 1].set("v.value", wrapper.year);
                    } else {
                        component.find("Year__c").set("v.value", wrapper.year);
                    }
                    component.get("v.financial").Year__c = wrapper.year;
                    
                }
                if (wrapper.quarter) {
                    component.find("Launch_Quarter__c").set("v.value", wrapper.quarter);
                }

            })
            $A.enqueueAction(action);

        }
    },

    handleFinancialChange : function(component, event, helper) {
        
        var field = event.getSource().getLocalId();
        var value = event.getSource().get("v.value");
        component.set(`v.financial.${field}`, value);
        if (field == "Year__c") {
            component.set("v.draftSaved", false);
        }
    },

    isValidCountrySection : function(component, event, helper) {
        var isValid = true;
        if (component.get("v.interest").Country__c == null || component.get("v.interest").Country__c == "" || component.get("v.financial").Year__c == null || component.get("v.financial").Year__c == "") {
            isValid = false;
        }
        return isValid;
    },

    isValidProductSection : function(component,event,helper) {
        var isValid = true;
        if (component.find('validInput')) {
            
            component.find('validInput').forEach(element => {
                /*
                if (isNaN(element.get("v.value"))) {
                    isValid = false;
                }
                */
                element.reportValidity();
                if (element.get('v.validity') && !element.get('v.validity').valid) {
                    isValid = false;
                }
                
            });
        }
        return isValid;
    },

    isValidQuestions : function(component,event,helper) {
        var isValid = true;
        component.get("v.questionWrapper").requiredQuestions.forEach(element => {
            if (element.answer.Answer__c == "" || element.answer.Answer__c == null || element.answer.Answer__c.trim() == '') {
                isValid = false;
            }
        });
        return isValid;
    },

    handleChange : function(component) {
        var wrapper = component.get("v.wrapper");

        wrapper.financialsProjectPerYearList.forEach(element => {
            
            element.forEach(financial => {
                if (financial.record.ASP__c && financial.record.Units__c) {
                    financial.revenue = Number(financial.record.ASP__c * financial.record.Units__c).toFixed(0);
                } else {
                    financial.revenue = null;
                }

                if (financial.record.Non_Rev_Unit__c && financial.record.Units__c) {
                    financial.totalUnit = parseInt(Number(financial.record.Non_Rev_Unit__c) + Number(financial.record.Units__c));
                } else if (financial.record.Non_Rev_Unit__c) {
                    financial.totalUnit = financial.record.Non_Rev_Unit__c;
                } else {
                    financial.totalUnit = parseInt(financial.record.Units__c);
                }
                financial.record.Units__c = parseFloat(String(financial.record.Units__c).trim());
                financial.record.ASP__c = parseFloat(String(financial.record.ASP__c).trim());
                financial.record.Cannibalized_Units__c = parseInt(String(financial.record.Cannibalized_Units__c).trim());
                financial.record.Cannibalized_ASP__c = parseFloat(String(financial.record.Cannibalized_ASP__c).trim());
                financial.record.Non_Rev_Unit__c = parseInt(String(financial.record.Non_Rev_Unit__c).trim());
            });

        });
        component.set("v.wrapper", wrapper);
    },

    handleSaveDraft : function(component, event, helper) {
        component.set("v.draftSaved", true);
        helper.showDraftToast(component, event, helper);
    },

    handleProductChange : function(component, event, helper) {
        component.set("v.draftSaved", false);
    },

    showCountryToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Missing Values",
            "message": "Please fill all the required fields with valid values."
        });
        toastEvent.fire();
    },

    showProductInvalidToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Invalid Values",
            "message": "Please correct the invalid values."
        });
        toastEvent.fire();
    },

    showQuestionToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Missing Answers",
            "message": "Please provide answers for all the required questions."
        });
        toastEvent.fire();
    },

    showNoProductToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "The Project has no Products",
            "message": "Please add products to the project to continue adding financials.",
            "type":"warning"
        });
        toastEvent.fire();
    },
    
    showSuccessToast : function(component, event, helper) {
        var self = this;
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "The records have been created/updated successfully.",
            "type": "success"

        });
        toastEvent.fire();
        self.util.showReminder({component: component, header: 'Reminder', message: 'Close out all remaining open tasks'});
    },

    showDraftToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Draft Saved",
            "message": "A draft was saved with the latest changes on the financials",
            "type": "info"
        });
        toastEvent.fire();
    },
    handlePaste : function(component, text, name) {
        var self = this;
        self.util.handlePaste({component: component, value: text, name: name, rows: 7, columns: 10, disabledRows: 2});
        //self.formatCells(component, text, name.replace('input_', ''));
    },
    changeToPasted : function(component){
        var self = this;
        self.util.changeToPasted({component: component});
    },
    handleRegionChange : function(component, event) {
        var regionValue = event.getSource().get('v.value');
        var region = Array.isArray(component.find('Region__c')) ? component.find('Region__c')[component.find('Region__c').length - 1] : component.find('Region__c');
        region.set('v.value', regionValue);
    },
    createFinancialInfo : function(component){
        var self = this;
        var financials = [];
        component.get("v.financials").forEach(element => {
            financials.push(element.record);
        });
        
        var action = component.get("c.CreateFinancialInfo");
        action.setParams({
                            "sampleFinancial" : component.get("v.financial"),
                            "financials" : financials
                        });
        action.setCallback(this, function(response) {
            var wrapper = response.getReturnValue();
            wrapper.financialsProjectPerYearList.forEach(element => {
    
                element.forEach(financial => {
                    if (financial.record.ASP__c && financial.record.ASP__c != null && financial.record.ASP__c != '') {
                        var stringToFloat = parseFloat(financial.record.ASP__c);
                        if (stringToFloat) {
                            financial.record.ASP__c = stringToFloat.toFixed(2);
                        } else {
                            financial.record.ASP__c = financial.record.ASP__c.toFixed(2);
                        }
                    }
                    if (financial.record.Units__c && financial.record.Units__c != null && financial.record.Units__c != '') {
                        var stringToFloat = parseFloat(financial.record.Units__c);
                        if (stringToFloat) {
                            financial.record.Units__c = stringToFloat.toFixed(2);
                        } else {
                            financial.record.Units__c = financial.record.Units__c.toFixed(2);
                        }
                    }
                });
            });
            component.set("v.wrapper", wrapper);
            self.handleChange(component);
        })
        $A.enqueueAction(action);
    },
    handleViewClick : function(component){
        var recordId = component.get("v.recordId");
        var country = component.find('Country__c').get('v.value');
        var region = component.find('Region__c').get('v.value');
        component.get("v.financial").Project__c = recordId;
        var action = component.get("c.getFinancialInfoByProjectAndCountry");
        action.setParams({  "recordId" : recordId,
                            "financial" : component.get('v.financial'),
                            "country" : country,
                            "region" : region
                        });         

        action.setCallback(this, function(response) {
            var financialInfo = response.getReturnValue();
            component.set('v.wrapper', financialInfo.financialWrapper);
            component.set('v.questionWrapper', financialInfo.projectQuestions);
            component.set('v.financials', financialInfo.financialInfo);
            component.set('v.interest', financialInfo.interest.interest);
        })
        $A.enqueueAction(action);
    },
    util: {},
    countryWithFinancials: {}
})