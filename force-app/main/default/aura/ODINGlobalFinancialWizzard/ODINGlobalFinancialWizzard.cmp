<aura:component controller="ODINGlobalFinancialWizzardController" implements="flexipage:availableForAllPageTypes,flexipage:availableForRecordHome,force:hasRecordId,force:lightningQuickAction,lightning:actionOverride" access="global">

    <ltng:require styles="{!$Resource.ODINCSSResource}" />
    <ltng:require styles="{!$Resource.toastStyles}" />

    <aura:attribute name="recordId" type="String"/>
    <aura:attribute name="headerText" type="String" default="Select Country and Year"/>
    <aura:attribute name="cancelLabel" type="String" default="Cancel"/>
    <aura:attribute name="backLabel" type="String" default="Back"/>
    <aura:attribute name="nextLabel" type="String" default="Next"/>
    <aura:attribute name="step" type="String" default="1"/>
    <aura:attribute name="interest" type="Interest__c" default="{}"/>
    <aura:attribute name="financial" type="Financial__c" default="{}"/>
    <aura:attribute name="wrapper" type="Object"/>
    <aura:attribute name="questionWrapper" type="Object"/>
    <aura:attribute name="financials" type="List"/>
    <aura:attribute name="draftSaved" type="Boolean" default="false"/>
    <aura:attribute name="pasted" type="Boolean" default="false"/>
    <aura:attribute name="noInterestReasonText" type="String" default=""/>
    <aura:attribute name="noInterestReason" type="String" default=""/>
    <aura:attribute name="noInterestSelected" type="Boolean" default="false"/>
    <aura:attribute name="regions" type="List"/>
    <aura:attribute name="createdFrom" type="String" default="GF"/>
    <aura:attribute name="saveDisabled" type="Boolean" default="false"/>
    <aura:attribute name="viewSelected" type="Boolean" default="false"/>
    <aura:attribute name="viewLabel" type="String" default="View"/>
    <aura:attribute name="doneLabel" type="String" default="Done"/>
    <aura:attribute name="hasFinancials" type="Boolean" default="false"/>

    <aura:handler name="init" value="{!this}" action="{!c.doInit}"/>

    <c:ODINUtils aura:id="util"/>
    <lightning:overlayLibrary aura:id="overlayLib"/>

    <div>
            
        <section role="dialog" tabindex="-1" class="slds-modal slds-fade-in-open slds-modal_large" aria-labelledby="header43" aria-modal="true" aria-describedby="modal-content-id-1">
            
            <div class="{!'slds-modal__container ' + (v.step == '2' || v.step == '3' ? 'bigModal' : '')}">

                <header class="slds-modal__header">
                    <button class="slds-button slds-button--icon-inverse slds-modal__close" onclick="{!c.close}">
                        <lightning:icon iconName="utility:close" size="medium" variant="bare"/>
                        <span class="slds-assistive-text">Close</span>
                    </button>
                    <h2 class="slds-text-heading_medium">Global Financials</h2>
                </header>
            
                <div class="slds-modal__content slds-grow slds-p-around_medium" >
                    <div class="header3">
                        <h3 class="slds-text-heading_small">{!v.headerText}</h3>
                        <aura:if isTrue="{!v.step == '3'}">
                            <div class="reminder-text">
                                <h4>Reminder: When copy and pasting from Excel, ensure the data has no special formats, dollar signs, commas.</h4>
                                <h4>We recommend using General or Number format and eliminating excess decimal places</h4>
                            </div>
                        </aura:if>
                    </div>
                    
                    <aura:if isTrue="{!v.step == '1' || v.step == '5' || v.viewSelected}">
                        <lightning:recordEditForm
                            aura:id="interestForm"
                            objectApiName="Interest__c">

                            <div class="slds-form_horizontal">
                                <aura:if isTrue="{!v.step == '1'}">
                                    <lightning:select label="Region" aura:id="RegionSelector" onchange="{!c.handleRegionChange}" required="true" disabled="{!v.viewSelected}">
                                        <aura:iteration items="{!v.regions}" var="r">
                                            <option value="{!r.key}" text="{!r.label}" />
                                        </aura:iteration>
                                    </lightning:select>
                                    <lightning:inputField disabled="{!v.viewSelected}" fieldName="Region__c" aura:id="Region__c" onchange="{!c.handleInterestChange}" required="true" class="slds-hide"/>
                                    <lightning:inputField disabled="{!v.viewSelected}" fieldName="Country__c" aura:id="Country__c" onchange="{!c.handleInterestChange}" required="true"/>
                                    <lightning:inputField disabled="{!v.viewSelected}" fieldName="Level__c" aura:id="Level__c" onchange="{!c.handleInterestChange}"/>
                                    <aura:set attribute="else">
                                        <lightning:inputField disabled="{!v.viewSelected}" fieldName="Created_From__c" aura:id="Created_From__c" value="{!v.createdFrom}" class="slds-hide"/>
                                        <lightning:inputField disabled="{!v.viewSelected}" required="true" fieldName="No_Interest_Reason__c" aura:id="No_Interest_Reason__c" value="{!v.noInterestReason}" onchange="{!c.noInterestChanged}"/>
                                        <lightning:inputField disabled="{!v.viewSelected}" fieldName="No_Interest_Reason_Text__c" aura:id="No_Interest_Reason_Text__c" value="{!v.noInterestReasonText}"/>
                                    </aura:set>
                                </aura:if>
                            </div>
                        </lightning:recordEditForm>

                        <aura:if isTrue="{!v.step == '1'}">
                            <lightning:recordEditForm
                                aura:id="financialForm"
                                objectApiName="Financial__c">

                                <div class="slds-form_horizontal">

                                    <lightning:inputField disabled="{!v.viewSelected}" fieldName="Year__c" aura:id="Year__c" onchange="{!c.handleFinancialChange}" required="true" />
                                    <lightning:inputField disabled="{!v.viewSelected}" fieldName="Launch_Quarter__c" aura:id="Launch_Quarter__c" onchange="{!c.handleFinancialChange}"/>
                                </div>
                            </lightning:recordEditForm>
                        </aura:if>
                    </aura:if>

                    <aura:if isTrue="{!v.step == '2'}">
                        
                        <aura:iteration items="{!v.financials}" var="item">
                            
                            <div class="productContainer">
                                
                                <label class="slds-form-element__label productLabel">{!item.productName}</label>
                                
                                <div class="productGrid">
                                    <label class="slds-form-element__label gridPos1">Rev. Units #</label>
                                    <lightning:input disabled="{!v.viewSelected}" class="gridPos2" aura:id="validInput"  type="number" value="{!item.record.Units__c}" min="0" onkeyup="{!c.handleProductChange}" step="0.01"/>
                                    <label class="slds-form-element__label gridPos3">Unit Growth %</label>
                                    <lightning:input disabled="{!v.viewSelected}" class="gridPos4" aura:id="validInput" type="number" value="{!item.record.Unit_Growth__c}" step="0.01" onkeyup="{!c.handleProductChange}"/>
                                    <label class="slds-form-element__label gridPos5">ASP $</label>
                                    <lightning:input disabled="{!v.viewSelected}" class="gridPos6" aura:id="validInput"  type="number" value="{!item.record.ASP__c}" min="0" step="0.01" onkeyup="{!c.handleProductChange}"/>
                                    <label class="slds-form-element__label gridPos7">ASP Growth %</label>
                                    <lightning:input disabled="{!v.viewSelected}" class="gridPos8" aura:id="validInput" type="number" value="{!item.record.ASP_Growth__c}" step="0.01" onkeyup="{!c.handleProductChange}"/>
                                </div>
                                <div class="productGrid">
                                    <label class="slds-form-element__label gridPos1">Cann. Units #</label>
                                    <lightning:input disabled="{!v.viewSelected}" class="gridPos2" aura:id="validInput"  type="number" value="{!item.record.Cannibalized_Units__c}" min="0" onkeyup="{!c.handleProductChange}"/>
                                    <label class="slds-form-element__label gridPos5">Cann. ASP $</label>
                                    <lightning:input disabled="{!v.viewSelected}" class="gridPos6" aura:id="validInput" type="number" value="{!item.record.Cannibalized_ASP__c}" step="0.01" onkeyup="{!c.handleProductChange}"/>
                                </div>
                                <div class="productGrid">
                                    <label class="slds-form-element__label gridPos1">Non-Rev Units %
                                        <span style="vertical-align: text-bottom;">
                                            <c:ODINHelpText helpText="{!$Label.c.ODIN_Non_Rev_Units_Label}"/>
                                        </span>
                                    </label>
                                    <lightning:input disabled="{!v.viewSelected}" class="gridPos2" aura:id="validInput" type="number" value="{!item.record.Non_Unit_Growth__c}" step="0.01" onkeyup="{!c.handleProductChange}"/>
                                </div>
                            </div>
                        </aura:iteration>
                    
                    </aura:if>

                    <aura:if isTrue="{!v.step == '3' || v.viewSelected}">
                        
                        <aura:iteration items="{!v.wrapper.financialsProjectPerYearList}" var="perYearList" indexVar="indx">
                            <div class="slds-border_bottom productContainer">
                                <label class="slds-form-element__label productLabel">{!perYearList[0].productName}</label>

                                <div class="productGrid10">
                                    <label class="slds-form-element__label grid10Pos3 yearLabel">{!v.wrapper.years[0]}</label>
                                    <label class="slds-form-element__label grid10Pos4 yearLabel">{!v.wrapper.years[1]}</label>
                                    <label class="slds-form-element__label grid10Pos5 yearLabel">{!v.wrapper.years[2]}</label>
                                    <label class="slds-form-element__label grid10Pos6 yearLabel">{!v.wrapper.years[3]}</label>
                                    <label class="slds-form-element__label grid10Pos7 yearLabel">{!v.wrapper.years[4]}</label>
                                    <label class="slds-form-element__label grid10Pos8 yearLabel">{!v.wrapper.years[5]}</label>
                                    <label class="slds-form-element__label grid10Pos9 yearLabel">{!v.wrapper.years[6]}</label>
                                    <label class="slds-form-element__label grid10Pos10 yearLabel">{!v.wrapper.years[7]}</label>
                                    <label class="slds-form-element__label grid10Pos11 yearLabel">{!v.wrapper.years[8]}</label>
                                    <label class="slds-form-element__label grid10Pos12 yearLabel">{!v.wrapper.years[9]}</label>
                                </div>
                                
                                <div class="productGrid10">
                                    <label class="slds-form-element__label grid10Pos2">Revenue $</label>
            
                                    <lightning:input class="grid10Pos3 revenueSize" type="number" value="{!perYearList[0].revenue}" disabled="true" />
            
                                    <lightning:input class="grid10Pos4 revenueSize" type="number" value="{!perYearList[1].revenue}" disabled="true" />

                                    <lightning:input class="grid10Pos5 revenueSize" type="number" value="{!perYearList[2].revenue}" disabled="true" />

                                    <lightning:input class="grid10Pos6 revenueSize" type="number" value="{!perYearList[3].revenue}" disabled="true" />

                                    <lightning:input class="grid10Pos7 revenueSize" type="number" value="{!perYearList[4].revenue}" disabled="true" />

                                    <lightning:input class="grid10Pos8 revenueSize" type="number" value="{!perYearList[5].revenue}" disabled="true" />

                                    <lightning:input class="grid10Pos9 revenueSize" type="number" value="{!perYearList[6].revenue}" disabled="true" />

                                    <lightning:input class="grid10Pos10 revenueSize" type="number" value="{!perYearList[7].revenue}" disabled="true" />

                                    <lightning:input class="grid10Pos11 revenueSize" type="number" value="{!perYearList[8].revenue}" disabled="true" />

                                    <lightning:input class="grid10Pos12 revenueSize" type="number" value="{!perYearList[9].revenue}" disabled="true" />
                                </div>
                                
                                <div class="productGrid10">
                                    <label class="slds-form-element__label grid10Pos2">Rev. Units #</label>
            
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_11/' + indx}" class="grid10Pos3" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[0].record.Units__c}" min="0" onblur="{!c.handleChange}" step="0.01" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"/>
            
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_12/' + indx}" class="grid10Pos4" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[1].record.Units__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_13/' + indx}" class="grid10Pos5" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[2].record.Units__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_14/' + indx}" class="grid10Pos6" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[3].record.Units__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_15/' + indx}" class="grid10Pos7" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[4].record.Units__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_16/' + indx}" class="grid10Pos8" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[5].record.Units__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_17/' + indx}" class="grid10Pos9" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[6].record.Units__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_18/' + indx}" class="grid10Pos10" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[7].record.Units__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_19/' + indx}" class="grid10Pos11" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[8].record.Units__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_110/' + indx}" class="grid10Pos12" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[9].record.Units__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
                                    
                                </div>
                                
                                <div class="productGrid10">
                                    
                                    <label class="slds-form-element__label grid10Pos2">ASP $</label>
                                    
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_21/' + indx}" class="grid10Pos3" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[0].record.ASP__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_22/' + indx}" class="grid10Pos4" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[1].record.ASP__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_23/' + indx}" class="grid10Pos5" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[2].record.ASP__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_24/' + indx}" class="grid10Pos6" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[3].record.ASP__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_25/' + indx}" class="grid10Pos7" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[4].record.ASP__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_26/' + indx}" class="grid10Pos8" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[5].record.ASP__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
            
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_27/' + indx}" class="grid10Pos9" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[6].record.ASP__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
            
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_28/' + indx}" class="grid10Pos10" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[7].record.ASP__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
            
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_29/' + indx}" class="grid10Pos11" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[8].record.ASP__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
            
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_210/' + indx}" class="grid10Pos12" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[9].record.ASP__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
                                    
                                </div>

                                <div class="productGrid10">
                                    <label class="slds-form-element__label grid10Pos2">Cann. Units #</label>
            
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_31/' + indx}" class="grid10Pos3" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[0].record.Cannibalized_Units__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}" />
            
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_32/' + indx}" class="grid10Pos4" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[1].record.Cannibalized_Units__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}" />
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_33/' + indx}" class="grid10Pos5" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[2].record.Cannibalized_Units__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}" />
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_34/' + indx}" class="grid10Pos6" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[3].record.Cannibalized_Units__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}" />
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_35/' + indx}" class="grid10Pos7" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[4].record.Cannibalized_Units__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}" />
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_36/' + indx}" class="grid10Pos8" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[5].record.Cannibalized_Units__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}" />
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_37/' + indx}" class="grid10Pos9" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[6].record.Cannibalized_Units__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}" />
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_38/' + indx}" class="grid10Pos10" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[7].record.Cannibalized_Units__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}" />
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_39/' + indx}" class="grid10Pos11" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[8].record.Cannibalized_Units__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}" />
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_310/' + indx}" class="grid10Pos12" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[9].record.Cannibalized_Units__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}" />
                                    
                                </div>

                                <div class="productGrid10">
                                    <label class="slds-form-element__label grid10Pos2">Cann. ASP $</label>
                                    
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_41/' + indx}" class="grid10Pos3" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[0].record.Cannibalized_ASP__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_42/' + indx}" class="grid10Pos4" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[1].record.Cannibalized_ASP__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_43/' + indx}" class="grid10Pos5" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[2].record.Cannibalized_ASP__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_44/' + indx}" class="grid10Pos6" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[3].record.Cannibalized_ASP__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_45/' + indx}" class="grid10Pos7" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[4].record.Cannibalized_ASP__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_46/' + indx}" class="grid10Pos8" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[5].record.Cannibalized_ASP__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
            
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_47/' + indx}" class="grid10Pos9" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[6].record.Cannibalized_ASP__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
            
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_48/' + indx}" class="grid10Pos10" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[7].record.Cannibalized_ASP__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
            
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_49/' + indx}" class="grid10Pos11" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[8].record.Cannibalized_ASP__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
            
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_410/' + indx}" class="grid10Pos12" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[9].record.Cannibalized_ASP__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}"  step="0.01"/>
                                    
                                </div>

                                <div class="productGrid10">
                                    <label class="slds-form-element__label grid10Pos2">Non-Rev Units #
                                        <span style="vertical-align: text-bottom;">
                                            <c:ODINHelpText helpText="{!$Label.c.ODIN_Non_Rev_Units_Label}"/>
                                        </span>
                                    </label>
            
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_51/' + indx}" class="grid10Pos3" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[0].record.Non_Rev_Unit__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}" />
            
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_52/' + indx}" class="grid10Pos4" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[1].record.Non_Rev_Unit__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}" />
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_53/' + indx}" class="grid10Pos5" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[2].record.Non_Rev_Unit__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}" />
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_54/' + indx}" class="grid10Pos6" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[3].record.Non_Rev_Unit__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}" />
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_55/' + indx}" class="grid10Pos7" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[4].record.Non_Rev_Unit__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}" />
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_56/' + indx}" class="grid10Pos8" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[5].record.Non_Rev_Unit__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}" />
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_57/' + indx}" class="grid10Pos9" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[6].record.Non_Rev_Unit__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}" />
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_58/' + indx}" class="grid10Pos10" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[7].record.Non_Rev_Unit__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}" />
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_59/' + indx}" class="grid10Pos11" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[8].record.Non_Rev_Unit__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}" />
                                
                                    <lightning:input disabled="{!v.viewSelected}" aura:id="validInput" name="{!'input_510/' + indx}" class="grid10Pos12" type="{!v.pasted ? 'text' : 'number'}" value="{!perYearList[9].record.Non_Rev_Unit__c}" min="0" onblur="{!c.handleChange}" onchange="{!c.handlePaste}" onpaste="{!c.changeToPasted}" />
                                    
                                </div>

                                <div class="productGrid10">
                                    <label class="slds-form-element__label grid10Pos2">Total Unit Requirement</label>
            
                                    <lightning:input class="grid10Pos3" type="number" value="{!perYearList[0].totalUnit}" disabled="true" />
            
                                    <lightning:input class="grid10Pos4" type="number" value="{!perYearList[1].totalUnit}" disabled="true" />

                                    <lightning:input class="grid10Pos5" type="number" value="{!perYearList[2].totalUnit}" disabled="true" />

                                    <lightning:input class="grid10Pos6" type="number" value="{!perYearList[3].totalUnit}" disabled="true" />

                                    <lightning:input class="grid10Pos7" type="number" value="{!perYearList[4].totalUnit}" disabled="true" />

                                    <lightning:input class="grid10Pos8" type="number" value="{!perYearList[5].totalUnit}" disabled="true" />

                                    <lightning:input class="grid10Pos9" type="number" value="{!perYearList[6].totalUnit}" disabled="true" />

                                    <lightning:input class="grid10Pos10" type="number" value="{!perYearList[7].totalUnit}" disabled="true" />

                                    <lightning:input class="grid10Pos11" type="number" value="{!perYearList[8].totalUnit}" disabled="true" />

                                    <lightning:input class="grid10Pos12" type="number" value="{!perYearList[9].totalUnit}" disabled="true" />
                                </div>
                            </div>
                        </aura:iteration>

                    </aura:if>

                    <aura:if isTrue="{!v.step == '4' || v.viewSelected}">

                        <label class="slds-form-element__label questionLabel">Please provide answers to the following questions:</label>
                        
                        <aura:iteration items="{!v.questionWrapper.requiredQuestions}" var="q">
                            <lightning:textarea name="{!q.question.Question__c}" label="{!q.question.Question__c}" value="{!q.answer.Answer__c}" required="true" disabled="{!v.viewSelected}"/>
                        </aura:iteration>
                        
                        <aura:iteration items="{!v.questionWrapper.questions}" var="q">
                            <lightning:textarea name="{!q.question.Question__c}" label="{!q.question.Question__c}" value="{!q.answer.Answer__c}" disabled="{!v.viewSelected}"/>
                        </aura:iteration>
                    </aura:if>
                </div>

                <footer class="slds-modal__footer slds-grid slds-grid_align-spread">
                        
                    <lightning:button variant="Neutral" label="{!v.viewSelected ? v.backLabel : v.cancelLabel}" title="{!v.viewSelected ? v.backLabel : v.cancelLabel}" onclick="{!c.handleCancelClick}"/>
                    <aura:if isTrue="{!v.viewSelected == false}">
                        <lightning:progressIndicator currentStep="{!v.step}" type="base" variant="base">
                            <lightning:progressStep label="Country" value="1"/>
                            <aura:if isTrue="{!v.noInterestSelected}">
                                <lightning:progressStep label="No Interest" value="5"/>
                                <aura:set attribute="else">
                                    <lightning:progressStep label="Financial Input" value="2"/>
                                    <lightning:progressStep label="Confirm Financials" value="3"/>
                                    <lightning:progressStep label="Questions" value="4"/>
                                </aura:set>
                            </aura:if>
                        </lightning:progressIndicator>
                    </aura:if>
                    <aura:if isTrue="{!v.viewSelected == false}">
                        <aura:if isTrue="{!v.step == '3'}">
                                <lightning:button variant="success" label="Save Draft" title="Save Draft" onclick="{!c.handleSaveDraft}"  disabled="{!v.saveDisabled}"/>    
                        </aura:if>
                        <aura:if isTrue="{!v.step == '1'}">
                            <lightning:button variant="Neutral" label="{!v.viewLabel}" title="{!v.viewLabel}" onclick="{!c.handleViewClick}" disabled="{!!v.hasFinancials}"/>
                        </aura:if>
                        <lightning:button variant="brand" label="{!v.nextLabel}" title="{!v.nextLabel}" onclick="{!c.handleNextClick}" disabled="{!v.saveDisabled}"/>
                        <aura:set attribute="else">
                            <lightning:button variant="brand" label="{!v.doneLabel}" title="{!v.doneLabel}" onclick="{!c.handleDoneClick}"/>
                        </aura:set>
                    </aura:if>
                </footer>
            </div>
        </section>
    </div>
    <aura:if isTrue="{!v.viewSelected}">
        <aura:html tag="style">
            .slds-modal__container {
                height: 100vh !important;
                width: 80% !important;
            }
            .slds-p-around--medium {
                padding: 0px;
            }
        </aura:html>
    </aura:if>
</aura:component>