({
	init: function(cmp, evt, h) {
		h.setViewOptions(cmp);
	},
    changePeriod: function(cmp, evt, h) {
        var event = $A.get('e.c:PeriodViewChanged');
        var value = evt.getParam('value');
        event.setParams({
            period: value
        });
        event.fire();
        cmp.set('v.selectedViewOption', value);
    },
    collapse: function(cmp, evt, h) {
        var current = cmp.get('v.collapsed');
        cmp.set('v.collapsed', !current);
    }
})