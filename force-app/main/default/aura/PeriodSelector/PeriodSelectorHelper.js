({
	setViewOptions: function(cmp) {
        var options = [
            {'label': 'Month', 'value': 'month'},
            {'label': 'Quarter', 'value': 'quarter'},
            {'label': 'Year', 'value': 'year'}
        ];
        cmp.set('v.viewOptions', options);
    }
})