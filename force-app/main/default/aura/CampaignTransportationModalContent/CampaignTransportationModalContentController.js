({
	cancel: function(cmp, event, helper) {
		cmp.find("tran-modal").notifyClose();
	},

	saveAndNew: function(cmp, event, helper) {
		//helper.doSave(cmp, false);
	},

	save: function(cmp, event, helper) {
		helper.doSave(cmp, true);
	},

	onTabChanged: function(cmp, event, helper) {
		var tab = event.getSource();
		var tabId = tab.get("v.id");

		helper.initListOptions(cmp, tabId);	

		var userInitFlg = cmp.get("v.userInitFlg");

		if (userInitFlg === false) {
			helper.initListOptions(cmp, 'users-tab');	
		}
	}
})