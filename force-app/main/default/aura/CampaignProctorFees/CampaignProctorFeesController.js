({
	onInit: function(cmp, event, helper) {
		var params = event.getParam('arguments');

		if (params) {
            cmp.set("v.allowAccess", params.allowAccess);

			if (params.allowAccess === true) {
				helper.doLoad(cmp);
			}
		}
	},

	handleDataChanged: function(cmp, event, helper) {
		var allow = cmp.get("v.allowAccess");

		if (allow === true) {
			helper.doLoad(cmp);			
			$A.get("e.force:refreshView").fire(); //Refresh the main page to refresh the budgetting fields
		}
	},

	doCreate: function (cmp, event, helper) {
		var campaignId = cmp.get("v.campaignId");

        var action = cmp.get("c.createNewCampaignProctorFee");
        
        action.setParams({
			"campaignId": campaignId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (cmp.isValid() && state == "SUCCESS") {
                var dto = response.getReturnValue();

				if (dto !== null && dto !== undefined) {
					$A.createComponents([
						["c:CampaignProctorFeeModalContent", {
							"fee": dto
						}]
					], function(components, status) {
						if(status === "SUCCESS") {
							var content = components[0];

							cmp.find("fee-modal").showCustomModal({
								body: content,
								showCloseButton: true
							});
						}
					});					
				} else {
					var params = {
						title: 'Permission!',
						mode: 'dismissible',
						message: 'You do not have permission to create a proctor fee.',
						type: 'warning'
					}
                    
					_apostletech.showToast(params);
				}
            } else if (state == "ERROR") {
                var errors = action.getError();
                
                if (errors[0] && errors[0].message) {                         
                    console.error(errors[0].message);

                    var params = {
                        title: 'Uh Oh!',
                        mode: 'dismissible',
                        message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
                        type: 'error'
                    }
                    
					_apostletech.showToast(params);
                }
            }
        });
        
        $A.enqueueAction(action);
	}
})