({
    getCalculations: function(cmp) {
        var self = this;
        let sce = self.serverCallIssued(true);

        var currentCache = this.cache.find(x => x.id === this.getCachedId(cmp));
        if (currentCache) {
            return this.finalAssignment(cmp, currentCache.data);
        }

        if (!cmp.get('v.isBusy')) {
            var params = this.util.getCalculationMethodParams({cmp:cmp, view:'opportunity', isLoop:false});
            cmp.set('v.isBusy', true);
            this.serverAction(cmp, 'getCalculations', params)
            .then(function(data) {
                self.tempUsers = Array.from(data.users);
                self.tempUsers.shift();
                if (self.tempUsers.length > 0) {
                    self.loopCalculations(cmp, self.tempUsers[0].Id, data, sce);
                } else {
                    self.finalAssignment(cmp, data, sce);
                }
            }, function (errors) {
                var msg = errors.length > 0 ? errors[0].message : 'An unexpected error occurred';
                alert(msg);
                cmp.set('v.isBusy', false);
            });
        } else {
            window.setTimeout($A.getCallback(function(){ self.getCalculations(cmp); }), 1000);
        }
    },
    loopCalculations: function(cmp, userId, prevData, sce) {
        var self = this;
        var params = this.util.getCalculationMethodParams({cmp:cmp, view:'opportunity', isLoop:true, userId:userId});
        this.serverAction(cmp, 'getCalculations', params)
        .then(function(data) {
            data = self.util.sumSalesOpp({prevData:prevData, newData:data});
            self.tempUsers.shift();
            if (self.tempUsers.length > 0) {
                self.loopCalculations(cmp, self.tempUsers[0].Id, data, sce);
            } else {
                self.finalAssignment(cmp, data, sce);
            }
        }, function (errors) {
            var msg = errors.length > 0 ? errors[0].message : 'An unexpected error occurred';
            alert(msg);
            cmp.set('v.isBusy', false);
        });
    },
    serverAction : function(component, method, params) {
        var self = this;
        return new Promise(function(resolve, reject) {
            var action = component.get('c.' + method);
            if(params != null)
                action.setParams(params);
    
            action.setCallback(self, function(response){
                var state = response.getState();
                if(state == 'SUCCESS')
                    resolve.call(this, response.getReturnValue());
                else if(state == 'ERROR')
                    reject.call(this, response.getError());
            });
    
            $A.enqueueAction(action);
        });
    },
    finalAssignment: function (cmp, data, sce) {
        this.cache.push({id:this.getCachedId(cmp), data:data});
        this.util.finalAssignment({cmp:cmp, data:data, h:this, view:'opportunity'});
        this.serverCallIssued(false, sce);
        this.util.setSalesChartData({cmp:cmp, tableData:data.tableData});
    },
    setTableDataAfterFilter: function(component, event, tabledata){
        this.util.setSalesChartData({cmp:component, tableData:tabledata});
    },
    getCachedId: function (cmp) {
        return cmp.get('v.period') + cmp.get('v.user') + cmp.get('v.distributor') + (cmp.get('v.accountID') ? cmp.get('v.accountID') : '');
    },
    sort: function (cmp, dsSort, firstData) {
        this.util.sortOpps({cmp:cmp, dsSort:dsSort, firstData:firstData});
    },
    prevPage: function(cmp) {
        this.util.prevPage({cmp:cmp});
    },
    nextPage: function(cmp) {
        this.util.nextPage({cmp:cmp});
    },
    firstPage: function(cmp) {
        this.util.firstPage({cmp:cmp});
    },
    lastPage: function(cmp) {
        this.util.lastPage({cmp:cmp});
    },
    serverCallIssued: function (active, evt) {
        var event = evt ? evt : $A.get('e.c:PDSe_ServerCallIssued');
        event.setParams({active:active});
        event.fire();
        return $A.get('e.c:PDSe_ServerCallIssued');
    },
    tempUsers: [],
    originalTable: [],
    originalOppTable: [],
    util: {},
    cache: [],
    sellingDays: 0,
    ongoing: 0
})