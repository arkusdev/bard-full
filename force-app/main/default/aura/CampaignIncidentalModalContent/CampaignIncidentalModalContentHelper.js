({
	doSave: function (cmp, closeForm) {
		var fields = [{
			id: 'description',
			type: 'text'
		}, {
			id: 'purchaseDateTime',
			type: 'datetime'				
		}, {
			id: 'totalCost',
			type: 'number'			
		}];

		var validFlg = _apostletech.isValid(cmp, fields);

		if (validFlg === true) {
			var incidental = cmp.get("v.incidental");

			var members = cmp.get("v.selectedMembers");
			var users = cmp.get("v.selectedUsers");

			var selectedMembers = [];
			var selectedUsers = [];

			members.forEach(function(item){
				selectedMembers.push({
					"value": item,
					"label": ''
				});
			});

			users.forEach(function(item){
				selectedUsers.push({
					"value": item,
					"label": ''
				});
			});

			var dto = {
				incidentalId: incidental.incidentalId,
				description: incidental.description,
				campaignId: incidental.campaignId,
				purchaseDateTime: incidental.purchaseDateTime,
				totalCost: incidental.totalCost,
				transferOfValue: incidental.transferOfValue,
				notes: incidental.notes,
				selectedMembers: selectedMembers,
				selectedUsers: selectedUsers
			};

			var action = cmp.get("c.saveCampaignIncidental");
        
			action.setParams({
				"dto": JSON.stringify(dto)
			});
        
			action.setCallback(this, function(response) {
				var state = response.getState();
            
				if (cmp.isValid() && state == "SUCCESS") {
					var event = $A.get("e.c:CampaignIncidentalDataChanged");
					event.fire();

					if (closeForm == true) {
						cmp.find("incidental-modal").notifyClose();
					}

					var params = {
						title: 'Successfully Saved!',
						mode: 'dismissible',
						message: 'Incidental successfully saved to campaign!',
						type: 'success'
					}
                    
					_apostletech.showToast(params);
                
				} else if (state == "ERROR") {
					var errors = action.getError();
                
					if (errors[0] && errors[0].message) {                         
						console.error(errors[0].message);

						var params = {
							title: 'Uh Oh!',
							mode: 'dismissible',
							message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
							type: 'error'
						}
                    
						_apostletech.showToast(params);
					}
				}
			});
        
			$A.enqueueAction(action);		
		}
	},

	initListOptions: function(cmp, tabId) {
		var incidental = cmp.get("v.incidental");
		var memberInitFlg = cmp.get("v.memberInitFlg");
		var userInitFlg = cmp.get("v.userInitFlg");

		var selectedMembers = [];
		var selectedUsers = [];		
		var availableAttr = 'v.available';
		var selectedAttr = 'v.selected';
		var availableValues = [];
		var selectedValues = [];

		if (tabId === 'members-tab') {		
			incidental.availableMembers.forEach(function(item){
				availableValues.push({
					"value": item.value,
					"label": item.label
				});
			});	

			if (memberInitFlg === true) {
				selectedMembers = cmp.get("v.selectedMembers");

				selectedMembers.forEach(function(item){
					selectedValues.push(item);
				});	
			} else {
				selectedMembers = incidental.selectedMembers;

				selectedMembers.forEach(function(item){
					selectedValues.push(item.value);
				});	

				cmp.set("v.memberInitFlg", true);
			}

			availableAttr += 'Members';
			selectedAttr += 'Members';
		} else {
			incidental.availableUsers.forEach(function(item){
				availableValues.push({
					"value": item.value,
					"label": item.label
				});
			});	

			if (userInitFlg === true) {
				selectedUsers = cmp.get("v.selectedUsers");

				selectedUsers.forEach(function(item){
					selectedValues.push(item);
				});	
			} else {
				selectedUsers = incidental.selectedUsers;

				selectedUsers.forEach(function(item){
					selectedValues.push(item.value);
				});	

				cmp.set("v.userInitFlg", true);
			}

			availableAttr += 'Users';	
			selectedAttr += 'Users';				
		}
		
		cmp.set(availableAttr, availableValues);
		cmp.set(selectedAttr, selectedValues);	
	}
})