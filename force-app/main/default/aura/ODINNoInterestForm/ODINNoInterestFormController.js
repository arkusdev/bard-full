({
    doInit : function(component, event, helper) {
        component.set('v.saveDisabled', component.get('v.interest.No_Interest_Reason__c') == undefined);
        component.set('v.interest.Created_From__c', 'GMA');
        //component.set('v.interest.No_Interest_Reason_Text__c', '');
    },
    onchange : function(component, event){
        component.set('v.saveDisabled', !event.getParam("value"));
    },
    close : function(component, event, helper) {
        helper.close();
    }
})