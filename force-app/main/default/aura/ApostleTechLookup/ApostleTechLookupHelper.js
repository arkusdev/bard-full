({
	searchHelper : function(cmp, event, searchKeyword) {
		let objName = cmp.get("v.objectAPIName");
		let omitIdList = cmp.get("v.omitIdList");
		let action = cmp.get("c.fetchLookUpValues");
        let params = {
            "searchKeyword": searchKeyword,
            "objName": objName
        };
        if (omitIdList !== null && omitIdList !== undefined) params.omitIdList = omitIdList;

		/*if (omitIdList !== null && omitIdList !== undefined) {
			action.setParams({
				"searchKeyword": searchKeyword,
				"objName": objName,
				"omitIdList": omitIdList
			});	
		} else {
			action.setParams({
				"searchKeyword": searchKeyword,
				"objName": objName
			});			
		}*/

        //$A.util.addClass(cmp.find("spinner"), "slds-show");
        //$A.util.removeClass(cmp.find("spinner"), "slds-hide");
        this.show(cmp, 'spinner');
        action.setParams(params);
		action.setCallback(this, function(response) {
            this.hide(cmp, 'spinner');
			//$A.util.removeClass(cmp.find("spinner"), "slds-show");
            //$A.util.addClass(cmp.find("spinner"), "slds-hide");

			if (response.getState() === "SUCCESS") {
				let records = response.getReturnValue();
                let message = records.length < 1 ? 'No results found...' : '';
                cmp.set("v.message", message);
				/*if (records.length < 1) {
					cmp.set("v.message", 'No results found...');
				} else {
					cmp.set("v.message", '');
				}*/
				cmp.set("v.records", records);
            } else {
                console.error('Callback error', response.getError()[0]);
            }
		});

		$A.enqueueAction(action);
	},

	open: function(cmp, auraId) {
		var element = cmp.find(auraId);
		$A.util.addClass(element, 'slds-is-open');
		$A.util.removeClass(element, 'slds-is-close');	
	}, 

	close: function(cmp, auraId) {
		var element = cmp.find(auraId);
		$A.util.addClass(element, 'slds-is-close');
		$A.util.removeClass(element, 'slds-is-open');	
	},

	hide: function(cmp, auraId) {
		var element = cmp.find(auraId);
		$A.util.addClass(element, 'slds-hide');
		$A.util.removeClass(element, 'slds-show');	
	},

	show: function(cmp, auraId) {
		var element = cmp.find(auraId); 
		$A.util.addClass(element, 'slds-show');
		$A.util.removeClass(element, 'slds-hide');	
	}
})