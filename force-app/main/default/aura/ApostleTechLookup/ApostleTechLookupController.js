({
    init: function (cmp, evt, h) {
        document.addEventListener('click', $A.getCallback(function(evt) {
            //let resultsList = cmp.find('search-results');
            //if (evt.target.id !== 'searchResults') h.close(cmp, 'search-results');
            let targetElement = evt.target || {};
            do {
                // This is a click inside. Do nothing, just return.
                if (targetElement.id === 'searchResults') return;
                // Go up the DOM
                targetElement = targetElement.parentNode;
            } while (targetElement);
            // if we didn't find our element, close the results
            h.close(cmp, 'search-results');
        }));
    },
    onFocus : function(cmp, event, helper){
        //$A.util.addClass(cmp.find("spinner"), "slds-show");
        if (cmp.get('v.records').length > 0) helper.open(cmp, 'search-results');
        //helper.searchHelper(cmp, event, '');
    },
    
    onBlur : function(cmp,event,helper){       
        //cmp.set("v.records", null);
        //helper.close(cmp, 'search-results');
    },
    
    keyPressController : function(cmp, event, helper) {
        let searchKeyword = cmp.get("v.searchKeyword");
        let previousSearch = cmp.get('v.previousSearch');
        
        if(searchKeyword.length > 2 && previousSearch != searchKeyword) {
            // debounce search so it doesn't call apex for every single key pressed
            window.clearTimeout(cmp.get('v.timeoutSearch'));
            let timeoutSearch = window.setTimeout($A.getCallback(function() {
                searchKeyword = cmp.get("v.searchKeyword");
                helper.open(cmp, 'search-results');
                helper.searchHelper(cmp, event, searchKeyword);
            }), 1000);
            cmp.set('v.timeoutSearch', timeoutSearch);
        } else if (searchKeyword.length < 1) {   
            cmp.set("v.records", []); 
            helper.close(cmp, 'search-results');
        }
    },
    
    clear :function(cmp, event, helper){
        helper.hide(cmp, 'pill');
        helper.show(cmp, 'lookup-field');
        helper.show(cmp, 'search-icon');
        
        cmp.set("v.searchKeyword", '');
        cmp.set("v.records", []);
        cmp.set("v.record", {});   
    },
    
    handleRecordSelected : function(cmp, event, helper) {
        var record = event.getParam("record");
        
        cmp.set("v.record", record); 
        
        helper.show(cmp, 'pill');
        helper.close(cmp, 'search-results');
        helper.hide(cmp, 'lookup-field');
        helper.hide(cmp, 'search-icon');
    }
})