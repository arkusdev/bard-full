({	
	doEdit: function (cmp, event, helper) {
		var meal = cmp.get("v.meal");

        var action = cmp.get("c.fetchCampaignMeal");

        action.setParams({
            "mealId": meal.mealId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (cmp.isValid() && state == "SUCCESS") {
                var dto = response.getReturnValue();

				if (dto !== null && dto !== undefined) { 
					$A.createComponents([
						["c:CampaignMealModalContent", {
							"meal": dto
						}]
					], function(components, status) {
						if(status === "SUCCESS") {
							var content = components[0];

							cmp.find("meal-modal").showCustomModal({
								body: content,
								showCloseButton: true
							});
						}
					});				
				} else {
					var params = {
						title: 'Permission!',
						mode: 'dismissible',
						message: 'You do not have permission to edit a meal expense.',
						type: 'warning'
					}
                    
					_apostletech.showToast(params);				
				}
                
            } else if (state == "ERROR") {
                var errors = action.getError();
                
                if (errors[0] && errors[0].message) {                         
                    console.error(errors[0].message);

                    var params = {
                        title: 'Uh Oh!',
                        mode: 'dismissible',
                        message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
                        type: 'error'
                    }
                    
					_apostletech.showToast(params);
                }
            }
        });
        
        $A.enqueueAction(action);
	},

	doDelete: function (cmp, event, helper) {
		var meal = cmp.get("v.meal");

		$A.createComponents([
			["c:ConfirmModalHeader", {
				"headerText": "Delete Meal?",
				"iconName": "utility:delete"
			}],
			["c:ConfirmModalContent", {
				"contentText": "Are you sure you want to delete " + meal.name + " from this campaign?"
			}],
			["c:ConfirmDeleteCampaignMealModalFooter", {
				"mealId": meal.mealId
			}]
		], function(components, status) {
			if(status === "SUCCESS") {
				var header = components[0];
				var content = components[1];
				var footer = components[2];

				cmp.find("confirm-modal").showCustomModal({
					header: header,
					body: content,
					footer: footer,
					showCloseButton: false
				});
			}
		});
	}
})