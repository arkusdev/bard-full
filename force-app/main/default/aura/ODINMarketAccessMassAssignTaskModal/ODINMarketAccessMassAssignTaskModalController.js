({
    close : function(component, event, helper) {
        helper.close(component, event, helper);
    },
    back : function(component) {
        component.set('v.step', 1);
    },
    handleNextClick : function(component, event, helper) {
        component.set('v.step', 2);
        helper.onloadData(component, event, helper);
    },
    handleSaveClick : function(component, event, helper) {
        helper.handleSaveClick(component, event, helper);
    },
    updateSelectedText: function(component, event, helper) {
        helper.updateSelectedText(component, event, helper);
    }
})