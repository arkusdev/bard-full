({
    onloadData: function(component,event,helper) {
        var action = component.get("c.GetTeam");
        action.setParams({  recordId : component.get("v.recordId")  });
        action.setCallback(this, function(response) {
            var wrapper = response.getReturnValue();
            wrapper.task.Subject = component.get('v.subject');
            wrapper.task.Created_From__c = 'Assign Market Access Task';
            wrapper.roles = wrapper.roles.filter(r => r.projectRole.Country__c && r.projectRole.Country__c.length > 0);
            component.set("v.wrapper", wrapper);
            var selectedRows = [];
            var selectedIds =[];
            component.get("v.wrapper").roles.forEach(function(record) {
                record.userName = record.projectRole.User__r.Name;
                record.role = record.projectRole.Role__c;
                record.region = record.projectRole.Region__c;
                record.country = record.projectRole.Country__c;
                record.Id = record.projectRole.Id;
                record.checked = true;
                
                selectedRows.push(record.projectRole);
                selectedIds.push(record.Id);
            });
            for (var ht in wrapper.helpTexts){
                var inputs = component.find('input');
                inputs.forEach(input => {
                    input.set('v.fieldLevelHelp', wrapper.helpTexts[input.get('v.label')]);
                });
            }
            component.set("v.selectedTeams", selectedRows);
            component.set("v.teamColumns", [
                                                { label: 'Name', fieldName: 'userName', type: 'text'},
                                                { label: 'Role', fieldName: 'role', type: 'text' },
                                                { label: 'Region', fieldName: 'region', type: 'text' },
                                                { label: 'Country', fieldName: 'country', type: 'text' }
                                            ]);
            component = component.find("teamTable");
            component.set("v.selectedRows", selectedIds);
        });
        
        $A.enqueueAction(action);
    },

    close : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },

    handleSaveClick : function(component, event, helper) {
        if (component.get("v.wrapper").task.Subject != "" && component.get("v.wrapper").task.Subject != null && component.get("v.wrapper").task.ActivityDate != "" && component.get("v.wrapper").task.ActivityDate != null) {
            
            if (component.get("v.selectedTeams") && component.get("v.selectedTeams").length > 0) {
                
                var action = component.get("c.Save");
                action.setParams({  task : component.get("v.wrapper").task,
                                    roles : component.get("v.selectedTeams"),
                                });
                action.setCallback(this, function(response) {
                    helper.showSuccessToast(component, event, helper);
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                });
                
                $A.enqueueAction(action);
            } else {
                helper.showEmptyToast(component, event, helper);    
            }
        } else {
            helper.showErrorToast(component, event, helper);
        }
    },

    updateSelectedText: function(component, event, helper) {
        var selectedRows = event.getParam('selectedRows');
        var projectRoles = [];
        selectedRows.forEach(function(record) {
            projectRoles.push(record.projectRole);
        });
        component.set("v.selectedTeams", projectRoles);
    },

    showSuccessToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "The records have been created successfully.",
            "type": "success"

        });
        toastEvent.fire();
    },

    showErrorToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Missing Required Fields",
            "message": "Please complete all the required fields."
        });
        toastEvent.fire();
    },

    showEmptyToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Missing Team",
            "message": "Please select at least one Project Team Member."
        });
        toastEvent.fire();
    }

})