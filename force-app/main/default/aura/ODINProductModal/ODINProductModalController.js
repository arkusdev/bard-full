({
    doInit : function(component, event, helper) {
        helper.onloadData(component,event,helper);
    },
    close : function(component, event, helper) {
        helper.close(component, event, helper);
    },
    handleNextClick : function(component, event, helper) {
        helper.handleNextClick(component, event, helper);
    },
    handleCancelClick : function(component, event, helper) {
        helper.handleCancelClick(component, event, helper);
    }

})