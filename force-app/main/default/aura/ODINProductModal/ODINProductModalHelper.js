({
    onloadData : function(component, event, helper) {
        if (component.get("v.isEdit")) {
            var action = component.get("c.GetProducts");
            action.setParams({"recordId" : component.get("v.recordId")});
            action.setCallback(this, function(response) {
                
                var productOptions = [{ "value": "", "label": "--None--" }]; 

                response.getReturnValue().forEach(element => {
                    if (component.get("v.selectedProduct") == element.Id) {
                        productOptions.push({ "value": element.Id, "label": element.Name, selected: true });    
                    } else {
                        productOptions.push({ "value": element.Id, "label": element.Name });
                    }
                });
                if (productOptions.length > 1) {

                    component.set("v.productValues", productOptions);
                    component.set("v.headerText", "Choose a Product");

                } else {
                    helper.showNoProductWFinancialsToast(component, event, helper);
                    $A.get("e.force:closeQuickAction").fire();
                }
            });
            $A.enqueueAction(action); 
        } else {
            var action = component.get("c.GetProductTypes");
            action.setCallback(this, function(response) {
                var regionOptions = [{ "value": "", "label": "--None--" }]; 

                response.getReturnValue().forEach(element => {
                    if (component.get("v.selectedValue") == element) {
                        regionOptions.push({ "value": element, "label": element, selected: true });    
                    } else {
                        regionOptions.push({ "value": element, "label": element });
                    }
                });

                component.set("v.picklistValues", regionOptions);
            });
            $A.enqueueAction(action);    
        }
    },
    
    close : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },

    handleNextClick : function(component, event, helper) {
        
        
        if (component.get("v.step") == "1") {
            
            if (component.get("v.isEdit")) {
                
                if (component.get("v.selectedProduct") && component.get("v.selectedProduct") != "" && component.get("v.selectedProduct") != null) {
                    
                    var action = component.get("c.GetFinancials");
                    action.setParams({
                                        "recordId" : component.get("v.recordId"),
                                        "productId" : component.get("v.selectedProduct")
                                    });
                    action.setCallback(this, function(response) {
                        var wrapper = response.getReturnValue();
                        wrapper.financials.forEach(financial => {
                            
                            if (financial.Estimated_Unit_Cost__c && financial.Estimated_Unit_Cost__c != null && financial.Estimated_Unit_Cost__c != '') {
                                var stringToFloat = parseFloat(financial.Estimated_Unit_Cost__c);
                                if (stringToFloat) {
                                    financial.Estimated_Unit_Cost__c = stringToFloat.toFixed(2);
                                } else {
                                    financial.Estimated_Unit_Cost__c = financial.Estimated_Unit_Cost__c.toFixed(2);
                                }
                            }

                            if (financial.Cannibalized_Unit_Cost__c && financial.Cannibalized_Unit_Cost__c != null && financial.Cannibalized_Unit_Cost__c != '') {
                                stringToFloat = parseFloat(financial.Cannibalized_Unit_Cost__c);
                                if (stringToFloat) {
                                    financial.Cannibalized_Unit_Cost__c = stringToFloat.toFixed(2);
                                } else {
                                    financial.Cannibalized_Unit_Cost__c = financial.Cannibalized_Unit_Cost__c.toFixed(2);
                                }
                            }
                        });

                        component.set("v.wrapper", wrapper);
                        component.set("v.headerText", "Confirm 10 Year Unit Costs for " + component.get("v.wrapper.product.Name"));
                        component.set("v.nextLabel", "Save");
                        component.set("v.cancelLabel", "Back");
                        component.set("v.step", "3");
                        component.set("v.bigModal", true);
                    })
                    $A.enqueueAction(action);
                } else {
                    helper.showNoProductToast(component, event, helper);
                }

            } else {
                if (component.get("v.productName") && component.get("v.productName") != "" && component.get("v.productName").trim() != "") {
                    component.set("v.cancelLabel", "Back");
                    component.set("v.headerText", "Unit Costs");
                    component.set("v.step", "2");
                } else {
                    helper.showProductNameToast(component, event, helper);
                }
            }

        } else if (component.get("v.step") == "2") {
            
            if (helper.isValid(component, event, helper)) {
                var action = component.get("c.CreateProduct");
                action.setParams({  "recordId" : component.get("v.recordId"),
                                    "productName" : component.get("v.productName"),
                                    "type" : component.get("v.selectedValue"),
                                    "sampleFinancial" : component.get("v.sampleFinancial")
                                });
                action.setCallback(this, function(response) {
                    var wrapper = response.getReturnValue();
                    wrapper.financials.forEach(financial => {
                        
                        if (financial.Estimated_Unit_Cost__c && financial.Estimated_Unit_Cost__c != null && financial.Estimated_Unit_Cost__c != '') {
                            var stringToFloat = parseFloat(financial.Estimated_Unit_Cost__c);
                            if (stringToFloat) {
                                financial.Estimated_Unit_Cost__c = stringToFloat.toFixed(2);
                            } else {
                                financial.Estimated_Unit_Cost__c = financial.Estimated_Unit_Cost__c.toFixed(2);
                            }
                        }

                        if (financial.Cannibalized_Unit_Cost__c && financial.Cannibalized_Unit_Cost__c != null && financial.Cannibalized_Unit_Cost__c != '') {
                            stringToFloat = parseFloat(financial.Cannibalized_Unit_Cost__c);
                            if (stringToFloat) {
                                financial.Cannibalized_Unit_Cost__c = stringToFloat.toFixed(2);
                            } else {
                                financial.Cannibalized_Unit_Cost__c = financial.Cannibalized_Unit_Cost__c.toFixed(2);
                            }
                        }
                    });

                    component.set("v.wrapper", wrapper);
                    component.set("v.headerText", "Confirm 10 Year Unit Costs for " + component.get("v.productName"));
                    component.set("v.nextLabel", "Save");
                    component.set("v.step", "3");
                    component.set("v.bigModal", true);
                })
                $A.enqueueAction(action);
            } else {
                helper.showInvalidToast(component, event, helper);
            }

        } else if (component.get("v.step") == "3") {
            if (helper.isValid(component, event, helper)) {
                if (component.get("v.isEdit")) {
                    var action = component.get("c.UpdateFinancials");
                    action.setParams({"financials" : component.get("v.wrapper").financials});
                    action.setCallback(this, function(response) {
                        helper.showSuccessToast(component, event, helper);
                        $A.get("e.force:closeQuickAction").fire();
                    })
                    $A.enqueueAction(action);
                } else {
                    var action = component.get("c.SaveFinancials");
                    action.setParams({  "financials" : component.get("v.wrapper").financials,
                                        "product" : component.get("v.wrapper").product
                                    });
                    action.setCallback(this, function(response) {
                        helper.showSuccessToast(component, event, helper);
                        $A.get("e.force:closeQuickAction").fire();
                    })
                    $A.enqueueAction(action);
                }
            } else {
                helper.showInvalidToast(component, event, helper);
            }
        }
    },

    handleCancelClick : function(component, event, helper) {
        if (component.get("v.step") == "1") {
            $A.get("e.force:closeQuickAction").fire();
        } else if (component.get("v.step") == "2") {
            component.set("v.headerText", "Choose the Product Name");
            component.set("v.cancelLabel", "Cancel");
            component.set("v.step", "1");
        } else if (component.get("v.step") == "3") {
            if (component.get("v.isEdit")) {

                component.set("v.headerText", "Choose a Product");
                component.set("v.cancelLabel", "Cancel");
                component.set("v.nextLabel", "Next");
                component.set("v.step", "1");

            } else {

                component.set("v.headerText", "Unit Costs");
                component.set("v.nextLabel", "Next");
                component.set("v.step", "2");
            }
        }
    },

    showSuccessToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "The records have been created successfully.",
            "type": "success"

        });
        toastEvent.fire();
    },

    showProductNameToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Missing Name",
            "message": "Please add a Product Name."
        });
        toastEvent.fire();
    },

    showNoProductToast: function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Missing Product",
            "message": "Please select a Product."
        });
        toastEvent.fire();
    },

    showInvalidToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Invalid Values",
            "message": "Please correct the invalid values."
        });
        toastEvent.fire();
    },

    showNoProductWFinancialsToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "No Products Found",
            "message": "There were no Products with 'Product' Financials found for editing."
        });
        toastEvent.fire();
    },

    isValid : function(component,event,helper) {
        var isValid = true;
        if (component.find('validInput')) {
            component.find('validInput').forEach(element => {
                //var value = element.get("v.value");
                if (element.get('v.validity') && !element.get('v.validity').valid) {
                    isValid = false;
                }
            });
        }
        console.log(isValid);
        return isValid;
    }
})