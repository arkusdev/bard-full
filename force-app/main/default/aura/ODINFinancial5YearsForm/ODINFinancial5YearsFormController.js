({
    doInit : function(component, event, helper) {
        helper.util = component.find('util');
        helper.doInit(component,event,helper);
    },
    handleChange : function(component, event, helper) {
        helper.handleChange(component,event,helper);
    },
    close : function(component, event, helper) {
        helper.close(component, event, helper);
    },
    handlePaste : function(component, event, helper) {
        var value = event.getSource().get("v.value");
        var name = event.getSource().get("v.name");
        if(value && (value.toString().endsWith('.') || value.toString().endsWith('0')) && !value.includes('	')){
            return;
        }
        helper.handlePaste(component, value, name);
    },
    changeToPasted : function(component, event, helper) {
        event.getSource().set("v.value", '');
        helper.changeToPasted(component, true);
    }
})