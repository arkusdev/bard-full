({
    doInit : function(component, event, helper) {
        component.set("v.wrapper", component.get("v.receivedWrapper"));
        var wrapper = component.get("v.wrapper");

        if(wrapper && wrapper.financialsProjectPerYearList){
            wrapper.financialsProjectPerYearList.forEach(element => {
                
                element.aspPerUnits = parseInt(element.financial.ASP__c * element.financial.Units__c);
                element.marketSizePerPotential = parseInt((element.aspPerUnits * element.financial.Potential_BD_Share__c) / 100);
                if (element.financial.ASP__c && element.financial.ASP__c != null && element.financial.ASP__c != '') {
                    var stringToFloat = parseFloat(element.financial.ASP__c);
                    if (stringToFloat) {
                        element.financial.ASP__c = stringToFloat.toFixed(2);
                    } else {
                        element.financial.ASP__c = element.financial.ASP__c.toFixed(2);
                    }
                }

            });
        }
        component.set("v.wrapper", wrapper);
    },

    handleChange : function(component,event,helper) {
        var wrapper = component.get("v.wrapper");

        wrapper.financialsProjectPerYearList.forEach(element => {
            if (element.financial.ASP__c && element.financial.Units__c) {
                var aspPerUnits = element.financial.ASP__c * element.financial.Units__c;
                element.aspPerUnits = parseInt(aspPerUnits);
            } else {
                element.aspPerUnits = null;
            }

            if (element.aspPerUnits && element.financial.Potential_BD_Share__c) {
                element.marketSizePerPotential = parseInt((element.aspPerUnits * element.financial.Potential_BD_Share__c) / 100);
            } else {
                element.marketSizePerPotential = null;
            }

        });
        component.set("v.wrapper", wrapper);
        var compEvent = component.getEvent("financialSelected");
        compEvent.setParams({financials : wrapper, isValid : helper.isValid(component,event,helper)});
        compEvent.fire();
    },
    close : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },

    isValid : function(component,event,helper) {
        var isValid = true;
        if (component.find('validInput')) {
            component.find('validInput').forEach(element => {
                element.reportValidity();
                if (element.get('v.validity') && !element.get('v.validity').valid) {
                    isValid = false;
                }
            });
        }
        return isValid;
    },
    handlePaste : function(component, text, name) {
        var self = this;
        self.util.handlePaste({component: component, value: text, name: name, rows: 2, columns: 5, disabledRows: 1});
        //self.formatCells(component, text, name.replace('input_', ''));
    },
    changeToPasted : function(component){
        var self = this;
        self.util.changeToPasted({component: component});
    },
    util: {}
})