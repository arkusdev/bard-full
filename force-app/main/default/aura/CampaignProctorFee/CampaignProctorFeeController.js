({	
	doEdit: function (cmp, event, helper) {
		var fee = cmp.get("v.fee");

        var action = cmp.get("c.fetchCampaignProctorFee");

        action.setParams({
            "feeId": fee.feeId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (cmp.isValid() && state == "SUCCESS") {
                var dto = response.getReturnValue();

				if (dto !== null && dto !== undefined) { 
					$A.createComponents([
						["c:CampaignProctorFeeModalContent", {
							"fee": dto
						}]
					], function(components, status) {
						if(status === "SUCCESS") {
							var content = components[0];

							cmp.find("fee-modal").showCustomModal({
								body: content,
								showCloseButton: true
							});
						}
					});				
				} else {
					var params = {
						title: 'Permission!',
						mode: 'dismissible',
						message: 'You do not have permission to edit a proctor fee.',
						type: 'warning'
					}
                    
					_apostletech.showToast(params);				
				}
                
            } else if (state == "ERROR") {
                var errors = action.getError();
                
                if (errors[0] && errors[0].message) {                         
                    console.error(errors[0].message);

                    var params = {
                        title: 'Uh Oh!',
                        mode: 'dismissible',
                        message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
                        type: 'error'
                    }
                    
					_apostletech.showToast(params);
                }
            }
        });
        
        $A.enqueueAction(action);
	},

	doDelete: function (cmp, event, helper) {
		var fee = cmp.get("v.fee");

		$A.createComponents([
			["c:ConfirmModalHeader", {
				"headerText": "Delete Proctor Fee?",
				"iconName": "utility:delete"
			}],
			["c:ConfirmModalContent", {
				"contentText": "Are you sure you want to delete " + fee.name + " from this campaign?"
			}],
			["c:ConfirmDeleteCampaignProctorFeeModalFooter", {
				"feeId": fee.feeId
			}]
		], function(components, status) {
			if(status === "SUCCESS") {
				var header = components[0];
				var content = components[1];
				var footer = components[2];

				cmp.find("confirm-modal").showCustomModal({
					header: header,
					body: content,
					footer: footer,
					showCloseButton: false
				});
			}
		});
	}
})