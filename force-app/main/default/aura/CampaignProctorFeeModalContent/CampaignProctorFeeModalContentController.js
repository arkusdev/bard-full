({
	onInit: function(cmp, event, helper) { 
		var fee = cmp.get("v.fee");

		var availableProctors = [];

        for(var key in fee.availableProctors) {
			availableProctors.push({
				proctorId: key,
				proctorName: fee.availableProctors[key]
			});
        }

		cmp.set("v.availableProctors", availableProctors);

		var transferOfValueOptions = [];

		for(var key in fee.transferOfValueOptions) {
			transferOfValueOptions.push({
				key: key,
				value: fee.transferOfValueOptions[key]
			});
		}

		cmp.set("v.transferOfValueOptions", transferOfValueOptions);
	},

	cancel: function(cmp, event, helper) {
		cmp.find("fee-modal").notifyClose();
	},

	saveAndNew: function(cmp, event, helper) {
		//helper.doSave(cmp, false);
	},

	save: function(cmp, event, helper) {
		helper.doSave(cmp, true);
	},

	setProctor: function(cmp, event, helper) {
		var fee = cmp.get("v.fee");

		var proctorId = cmp.find("proctor").get("v.value");
		var proctors = cmp.get("v.availableProctors");

		var proctor = proctors.find(item => item.proctorId === proctorId);

		if (proctor === null || proctor === undefined) { return; }
		
		fee.proctorId = proctorId;
		fee.proctorName = proctor.proctorName;

		cmp.set("v.fee", fee);
	}
})