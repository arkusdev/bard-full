({
	doSave: function (cmp, closeForm) {
		var fields = [{
			id: 'proctor',
			type: 'text'
		}, {
			id: 'totalCost',
			type: 'number'			
		}];

		var validFlg = _apostletech.isValid(cmp, fields);

		if (validFlg === true) {
			var fee = cmp.get("v.fee");

			var dto = {
				feeId: fee.feeId,
				campaignId: fee.campaignId,
				proctorId: fee.proctorId,
				proctorName: fee.proctorName,
				totalCost: fee.totalCost,
				transferOfValue: fee.transferOfValue,
				notes: fee.notes
			};

			var action = cmp.get("c.saveCampaignProctorFee");
        
			action.setParams({
				"dto": JSON.stringify(dto)
			});
        
			action.setCallback(this, function(response) {
				var state = response.getState();
            
				if (cmp.isValid() && state == "SUCCESS") {
					var event = $A.get("e.c:CampaignProctorFeeDataChanged");
					event.fire();

					if (closeForm == true) {
						cmp.find("fee-modal").notifyClose();
					}

					var params = {
						title: 'Successfully Saved!',
						mode: 'dismissible',
						message: 'Proctor Fee successfully saved to campaign!',
						type: 'success'
					}
                    
					_apostletech.showToast(params);
                
				} else if (state == "ERROR") {
					var errors = action.getError();
                
					if (errors[0] && errors[0].message) {                         
						console.error(errors[0].message);

						var params = {
							title: 'Uh Oh!',
							mode: 'dismissible',
							message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
							type: 'error'
						}
                    
						_apostletech.showToast(params);
					}
				}
			});
        
			$A.enqueueAction(action);		
		}
	}
})