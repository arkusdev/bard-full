({
    getContactFields: function (cmp) {
        let action = cmp.get('c.getContactFields');
        action.setParams({ contactId: cmp.get('v.recordId')});
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') cmp.set('v.contact', response.getReturnValue());
            else console.error('Could not retrieve Contact information');
        });
        $A.enqueueAction(action);
    },
	loadUpcomingContactCampaigns: function (cmp, event) {
		var action = cmp.get("c.fetchUpcomingCampaignsByContact");

		action.setParams({
			"contactId": cmp.get("v.recordId")
		});

		action.setCallback(this, function(response) {
			var state = response.getState();

			if (cmp.isValid() && state == "SUCCESS") {
				var campaigns = response.getReturnValue();

				cmp.set("v.registeredCampaigns", campaigns);
			} else if (state == "ERROR") {
				var errors = action.getError();

				if (errors[0] && errors[0].message) {                         
					console.error(errors[0].message);

					var params = {
						title: 'Uh Oh!',
						mode: 'dismissible',
						message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
						type: 'error'
					}
                    
					_apostletech.showToast(params);
				}
			}
		});

		$A.enqueueAction(action);
	},

	loadPicklistValues: function (cmp, event) {
		var action = cmp.get("c.fetchPickListValues");

		action.setParams({
			"fieldNames": cmp.get("v.picklistFields")
		});

		action.setCallback(this, function(response) {
			var state = response.getState();

			if (cmp.isValid() && state == "SUCCESS") {
				var picklistMap = new Map();
				picklistMap = response.getReturnValue();

				for(var field in picklistMap) {
					var attribute = 'v.';

					switch(field) {
						case 'division':
							attribute += 'divisions'
							break;
						case 'product_category':
							attribute += 'productCategories';
							break;
					}
					
					if (attribute != 'v.') {
						var values = [];

						for(var key in picklistMap[field]) {
							values.push({
								key: key,
								value: picklistMap[field][key]
							});
						}

						cmp.set(attribute, values);
					}
				}				
			} else if (state == "ERROR") {
				var errors = action.getError();

				if (errors[0] && errors[0].message) {                         
					console.error(errors[0].message);

					var params = {
						title: 'Uh Oh!',
						mode: 'dismissible',
						message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
						type: 'error'
					}
                    
					_apostletech.showToast(params);
				}
			}
		});

		$A.enqueueAction(action);	
	},

	getProductCategories: function(cmp) {
		var divisionName = cmp.find("division").get("v.value");

		var action = cmp.get("c.fetchproductCategories");

		action.setParams({
			"divisionName": divisionName
		});

		action.setCallback(this, function(response) {
			var state = response.getState();

			if (cmp.isValid() && state == "SUCCESS") { 
				var products = response.getReturnValue();

				var values = [];

				products.forEach(function(item) {
					values.push({
						key: item,
						value: item
					});					
				});

				cmp.set('v.productCategories', values);

			} else if (state == "ERROR") {
				var errors = action.getError();

				if (errors[0] && errors[0].message) {                         
					console.error(errors[0].message);

					var params = {
						title: 'Uh Oh!',
						mode: 'dismissible',
						message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
						type: 'error'
					}
                    
					_apostletech.showToast(params);
				}
			}		
		});

		$A.enqueueAction(action);
	},

	clearForm: function(cmp) {
		this.showTable(cmp, false);

		var paramsMap = cmp.get("v.paramsMap");

		for(var item in paramsMap) {
			cmp.find(item).set("v.value", "");
		}

		this.getProductCategories(cmp);
	},


	showTable: function(cmp, visible) {
		var course = cmp.find("course-results");

		var remove = 'slds-hide';
		var add = 'slds-show';

		if (!visible) {
			remove = 'slds-show';
			add = 'slds-hide';
		}

		$A.util.removeClass(course, remove);
		$A.util.addClass(course, add);
	},

	loadUpcomingCourses: function(cmp, showToast) {
		var paramsVM = [];
		var paramsMap = cmp.get("v.paramsMap");

		for(var item in paramsMap) {
			var itemValue = cmp.find(item).get("v.value");

			if (itemValue !== null && itemValue !== undefined && itemValue !== '') {
				var vm = {
					paramName: item,
					paramValue: itemValue
				}

				if (vm.paramValue != null && vm.paramValue.length) {
					paramsVM.push(vm);
				}			
			}
		}

		var action = cmp.get("c.fetchUpcomingCampaigns");

		var params = JSON.stringify(paramsVM);

		action.setParams({
			"contactId": cmp.get("v.recordId"),
			"params": params
		});

		action.setCallback(this, function(response) {
			var state = response.getState();

			if (cmp.isValid() && state == "SUCCESS") {
				var campaigns = response.getReturnValue();

				this.showTable(cmp, true);

				cmp.set("v.upcomingCampaigns", campaigns);
				console.log(campaigns);
				if (campaigns.length > 0) { 
					cmp.set("v.maxPage", Math.floor((campaigns.length + 9) / 10));

					this.renderData(cmp);					
				} else {
					if (showToast == true) {
						var params = {
							title: 'Oops!',
							mode: 'dismissible',
							message: 'No data was found that matched the entered criteria.',
							type: 'info'
						}
                    
						_apostletech.showToast(params);
					}
				}
			} else if (state == "ERROR") {
				var errors = action.getError();

				if (errors[0] && errors[0].message) {                         
					console.error(errors[0].message);

					var params = {
						title: 'Uh Oh!',
						mode: 'dismissible',
						message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
						type: 'error'
					}
                    
					_apostletech.showToast(params);
				}
			}
		});

		$A.enqueueAction(action);
	},

	addCampaignMember: function(cmp, campaignId, type) {
		var memberId = cmp.get("v.recordId");

		var action = cmp.get("c.addMember");

		action.setParams({
			"campaignId": campaignId,
			"memberId": memberId,
			"type": type
		});

		action.setCallback(this, function(response) {
			var state = response.getState();

			if (cmp.isValid() && state == "SUCCESS") {
				var memberName = response.getReturnValue();

				var params = {
					title: 'Successfully Added!',
					mode: 'dismissible',
					message: memberName + ' was successfully added to campaign as a ' +  type + '!' ,
					type: 'success'
				}
                    
				_apostletech.showToast(params);

			} else if (state == "ERROR") {
				var errors = action.getError();

				if (errors[0] && errors[0].message) {
					var error = JSON.parse(errors[0].message);

					console.error(error);

					var params = {
						title: error.name,
						mode: 'dismissible',
						message: error.message,
						type: 'error'
					}
                    
					_apostletech.showToast(params);
				}
			}
		});

		$A.enqueueAction(action);
	},

    contactIsValid: function (cmp) {
        let contact = cmp.get('v.contact');
        let result = contact.Email && contact.MobilePhone && contact.Surgeon_Specialty__c;
        return result;
    },

    showContactError: function () {
        var params = {
            title: 'Contact error',
            mode: 'dismissible',
            message: 'In order to attend or waitlist a course, contacts must have a speciality, email address and mobile phone number. Please fill those out and resubmit.',
            type: 'error'
        }

        _apostletech.showToast(params);
    },
	handlePageChanged: function(cmp, event) {
		var pageNumber = event.getParam("pageNumber");

		cmp.set("v.pageNumber", pageNumber);

		this.renderData(cmp);
	},

	renderData: function(cmp) {
		var campaigns = cmp.get("v.upcomingCampaigns");
		var pageNumber = cmp.get("v.pageNumber");
		var pageRecords = campaigns.slice(((pageNumber - 1) * 10), (pageNumber * 10));

		cmp.set("v.pagedUpcomingCampaigns", pageRecords);
	}
})