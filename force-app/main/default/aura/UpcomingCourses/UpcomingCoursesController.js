({
	loadTab: function (cmp, event, helper) {
		
		var selectedTab = cmp.get("v.selectedTab");
		var id = selectedTab;

		//If the event.target is null or undefined then this script is being fired 
		//from the init event so let's just keep the id to the defaulted selectedTab (proctor)
		if (event.target !== null && event.target !== undefined) {
        	//remove the '-tab__item' from the target id
        	id = event.target.id.substring(0, event.target.id.indexOf('-'));
            
			if (id === selectedTab) { return; }  //Get out if the user just clicks the same tab they're already on
		} 

		var tabMap = cmp.get("v.tabMap");

		for(var item in tabMap) { 
			var tab = cmp.find(item + '-tab');
			var div = cmp.find(item + '-div');   
			
			if(item === id) {
				$A.util.addClass(tab, 'slds-is-active'); 
                
        		$A.util.addClass(div, 'slds-show');        
        		$A.util.removeClass(div, 'slds-hide'); 
                                
				cmp.set("v.selectedTab", item);
			} else {
				$A.util.removeClass(tab, 'slds-is-active');
                
				$A.util.removeClass(div, 'slds-show');
        		$A.util.addClass(div, 'slds-hide');                
			}			      			
		}

		var spinner = cmp.find("spinner");
		$A.util.removeClass(spinner, "slds-hide");

		if (id === 'registered') {
			helper.loadUpcomingContactCampaigns(cmp, event);
		} else {
			var upcomingInit = cmp.get("v.upcomingInit");

			if (upcomingInit == true) {
				helper.loadUpcomingCourses(cmp, false);
			} else {
				helper.loadPicklistValues(cmp, event);
			}
		}
		
		$A.util.addClass(spinner, "slds-hide");
	},

	loadUpcomingContactCampaigns: function (cmp, event, helper) {
		var spinner = cmp.find("spinner");
		$A.util.removeClass(spinner, "slds-hide");

		helper.loadUpcomingContactCampaigns(cmp, event);
		
		$A.util.addClass(spinner, "slds-hide");
	},

    contactLoaded: function(cmp, evt, h) {
        if (!cmp.get('v.contact')) h.getContactFields(cmp);
    },
	getProductCategories: function(cmp, event, helper) { 
		helper.getProductCategories(cmp);
	},

	addCampaignMemberAttendee: function (cmp, event, helper) {
		var campaignId = event.getSource().get("v.name");

        if (helper.contactIsValid(cmp)) {
            helper.addCampaignMember(cmp, campaignId, 'Attendee');
            helper.loadUpcomingCourses(cmp, false);
        } else {
            helper.showContactError();
        }
	},

	addCampaignMemberWaitlist: function (cmp, event, helper) {
		var campaignId = event.getSource().get("v.name");

        if (helper.contactIsValid(cmp)) {
            helper.addCampaignMember(cmp, campaignId, 'Waitlist');
            helper.loadUpcomingCourses(cmp, false);
        } else {
            helper.showContactError();
        }
	},

	clearForm: function(cmp, event, helper) {
		helper.clearForm(cmp);
	},

	loadUpcomingCourses: function(cmp, event, helper) {
		var spinner = cmp.find("spinner");
		$A.util.removeClass(spinner, "slds-hide");

		helper.loadUpcomingCourses(cmp, true);
		cmp.set("v.upcomingInit", true);

		$A.util.addClass(spinner, "slds-hide");
	},

	handlePageChanged: function(cmp, event, helper) {
		helper.handlePageChanged(cmp, event);
	},
	close : function(cmp, event, helper) {
		$A.get("e.force:closeQuickAction").fire();
	}
})