({
    fetchUsers: function (cmp) {
        var self = this;
        var action = cmp.get('c.getUsers');
        action.setParams({
            "userId": null
        });
        action.setStorable();
        action.setCallback(this, function(response) {
            var state, data;
            state = response.getState();
            if (state === 'SUCCESS') {
                data = response.getReturnValue();
                cmp.set('v.isRM', false);
                cmp.set('v.brUser', data);
                let users = data.subordinates;
                users.unshift(data.user);
                var firstUser = self.setUsers(cmp, users);
                var secondUser = self.setGrandchilds(cmp);
                var user = secondUser ? secondUser : firstUser;
                self.changeUser(cmp, user, true);
                
            } else {
                var msg = response.getError().length > 0 ? response.getError()[0].message : 'An unexpected error occurred';
                alert(msg);
            }
        });
        $A.enqueueAction(action);
    },
    setUsers: function(cmp, users) {
        var self = this;
        var preUsers = cmp.get('v.users');
        if (preUsers.length === 0 && users && users.length > 0) {
            let childSelected = self.setChilds(cmp, users);
            return childSelected;
        }
    },
    setChilds: function(cmp, users){
        var userOptions = [];
        users.forEach(function(user) {
            var labelName = user.LastName === 'All' ? 'All' : user.user.Territory__c ? (user.user.Territory__c + ' - ' + user.user.Name) : (user.user.District__c ? '#' + user.user.Alias.replace('DM0' , '').replace('DM' , '') + ' - ' + user.user.Name : user.user.Name);
            userOptions.push({'label': labelName, 'value': user.user ? user.user.Id : user.Id});
        });
        cmp.set('v.users', userOptions);
        if (userOptions.length >= 2) {
            cmp.set('v.selectedUserId', userOptions[1].value);
            cmp.set('v.selectedUserName', userOptions[1].label.split(' - ')[1]);
            userOptions[1].selected = true;

            return userOptions[1].value;
        } else if (userOptions.length > 0) {
            cmp.set('v.selectedUserId', userOptions[0].value);
            cmp.set('v.selectedUserName', userOptions[0].label.split(' - ')[1]);
            return userOptions[0].value;
        }
    },
    setGrandchilds: function(cmp){
        var self = this;
        var userOptions = [{label : 'All', 'value' : cmp.get('v.selectedUserId')}];
        let selectedId = cmp.get('v.selectedUserId');
        let childs = cmp.get('v.brUser').subordinates;

        let childSelected = childs.filter(c => c.Id == selectedId || (c.user && c.user.Id == selectedId))[0];
        if (!childSelected){
            cmp.set('v.selectedUserId', selectedId);
            cmp.set('v.selectedUserRMId', selectedId);
            self.setAllGrandChilds(cmp);
        }else{
            let grandSons = childSelected.subordinates;
            if (childSelected.LastName == 'All' && cmp.get('v.rmUsers').length > 0)
            {
                cmp.set('v.selectedUserRMId', userOptions[0].value);
                self.setAllGrandChilds(cmp);
            }
            if (grandSons && grandSons.length > 0){
                if (!cmp.get('v.initialized')){
                    let users = cmp.get('v.users');
                    cmp.set('v.users', users);
                    cmp.set('v.initialized', true);
                }
                grandSons.forEach(function(user) {
                    var labelName = user.LastName === 'All' ? 'All' : user.user.Territory__c ? (user.user.Territory__c + ' - ' + user.user.Name) : user.user.Name;
                    userOptions.push({'label': labelName, 'value': labelName == 'All' ? cmp.get('v.selectedUserId') : user.user.Id});
                });
                cmp.set('v.rmUsers', userOptions);
                if (userOptions.length >= 2){
                    cmp.set('v.selectedUserId', userOptions.length > 1 ? userOptions[1].value : userOptions[0].value);
                    cmp.set('v.selectedUserRMId', userOptions[1].value);
                    userOptions[1].selected = true;
                    return userOptions[1].value;
                }else{
                    if (userOptions.length > 0){
                        cmp.set('v.selectedUserId', userOptions[0].value);
                        cmp.set('v.selectedUserRMId', userOptions[0].value);
                        return userOptions[0].value;
                    }
                }
            }
        }
    },
    changeUser: function (cmp, newValue, fromInit) {
        var self = this;
        if (!newValue) return;
        if (!fromInit){
            var gcValue = self.setGrandchilds(cmp);
            this.setSelectedUser(cmp, newValue);
            if (gcValue) newValue = gcValue;
        }

        self.changeUserEvt(cmp, newValue);
    },
    changeUserRM: function (cmp, newValue) {
        var self = this;
        if (!newValue) return;
        self.changeUserEvt(cmp, newValue);
    },
    changeUserEvt: function(cmp, newValue){
        var newValueId = newValue.user ? newValue.user.Id : newValue;

        var event = $A.get('e.c:PDS_UserChanged');
        event.setParams({userId:newValueId});
        event.fire();
        
        this.setSelectedUser(cmp, newValueId);
    },
    setSelectedUser: function(cmp, value) {
        var user = cmp.get('v.users').find(function(x) { return x.value === value});
        if (user) {
        	cmp.set('v.selectedUserId', user.value);
            cmp.set('v.selectedUserName', user.label.split(' - ').length > 1 ? user.label.split(' - ')[1] : user.label);
        }
    },
    setAllGrandChilds: function(cmp) {
        var user = cmp.get('v.brUser');
        var userOptions = [{'label' : 'All' , 'value' : user.subordinates[0].Id}];
        user.subordinates.shift();
        user.subordinates.forEach(s => {
            s.subordinates.forEach(ss => {
                var labelName = ss.user.Territory__c ? (ss.user.Territory__c + ' - ' + ss.user.Name) : (ss.user.District__c ? '#' + ss.user.Alias.replace('DM0' , '').replace('DM' , '') + ' - ' + ss.user.Name : ss.user.Name);
                userOptions.push({'label': labelName, 'value': ss.user.Id});
            });
        });
        cmp.set('v.rmUsers', userOptions);
    }
})