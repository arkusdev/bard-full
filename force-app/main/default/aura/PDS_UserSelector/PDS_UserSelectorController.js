({
    init: function(cmp, evt, h) {
        h.fetchUsers(cmp);
    },
	changeUser: function (cmp, evt, h) {
        //var value = evt.getParam('value');
        //h.changeUser(cmp, value);
        h.changeUser(cmp, cmp.get('v.selectedUserId'));
    },
    changeUserRM: function (cmp, evt, h) {
        //var value = evt.getParam('value');
        //h.changeUser(cmp, value);
        h.changeUserRM(cmp, cmp.get('v.selectedUserRMId'));
    },
    collapse: function(cmp, evt, h) {
        var current = cmp.get('v.collapsed');
        cmp.set('v.collapsed', !current);
    }
})