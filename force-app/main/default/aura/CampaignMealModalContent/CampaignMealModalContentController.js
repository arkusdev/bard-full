({
	onInit:  function(cmp, event, helper) { 
		var action = cmp.get("c.fetchAllStateOptions");
               
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (cmp.isValid() && state == "SUCCESS") {
				var states = new Map();
				states = response.getReturnValue();

				var values = [];

				for(var key in states) {
					values.push({
						value: key,
						label: states[key]
					});
				}

				cmp.set('v.provinceOptions', values);
                
            } else if (state == "ERROR") {
                var errors = action.getError();
                
                if (errors[0] && errors[0].message) {                         
                    console.error(errors[0].message);

					var params = {
						title: 'Uh Oh!',
						mode: 'dismissible',
						message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
						type: 'error'
					}
                    
					_apostletech.showToast(params);
                }
            }
        });
        
        $A.enqueueAction(action);	
	},

	cancel: function(cmp, event, helper) {
		cmp.find("meal-modal").notifyClose();
	},

	saveAndNew: function(cmp, event, helper) {
		//helper.doSave(cmp, false);
	},

	save: function(cmp, event, helper) {
		helper.doSave(cmp, true);
	},

	onTabChanged: function(cmp, event, helper) {
		var tab = event.getSource();
		var tabId = tab.get("v.id");

		helper.initListOptions(cmp, tabId);	

		var userInitFlg = cmp.get("v.userInitFlg");

		if (userInitFlg === false) {
			helper.initListOptions(cmp, 'users-tab');	
		}
	}
})