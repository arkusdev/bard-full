({
	doSave: function (cmp, closeForm) {
		var fields = [{
			id: 'location',
			type: 'text'
		}, {
			id: 'mealDate',
			type: 'date'				
		}, {
			id: 'totalCost',
			type: 'number'			
		}];

		var validFlg = _apostletech.isValid(cmp, fields);

		if (validFlg === true) {
			var meal = cmp.get("v.meal");

			var members = cmp.get("v.selectedMembers");
			var users = cmp.get("v.selectedUsers");

			var selectedMembers = [];
			var selectedUsers = [];

			members.forEach(function(item){
				selectedMembers.push({
					"value": item,
					"label": ''
				});
			});

			users.forEach(function(item){
				selectedUsers.push({
					"value": item,
					"label": ''
				});
			});

			var dto = {
				mealId: meal.mealId,
				name: meal.name,
				campaignId: meal.campaignId,
				type: meal.type,
				streetAddress: meal.streetAddress,
				city: meal.city,
				state: meal.state,
				postalCode: meal.postalCode,
				country: meal.country,
				notes: meal.notes,
				additionalAttendees: (meal.additionalAttendees === '' ? 0 : meal.additionalAttendees),
				locationName: meal.locationName,
				mealDate: meal.mealDate,
				totalCost: meal.totalCost,
				selectedMembers: selectedMembers,
				selectedUsers: selectedUsers
			};

			var action = cmp.get("c.saveCampaignMeal");
        
			action.setParams({
				"dto": JSON.stringify(dto)
			});
        
			action.setCallback(this, function(response) {
				var state = response.getState();
            
				if (cmp.isValid() && state == "SUCCESS") {
					var event = $A.get("e.c:CampaignMealDataChanged");
					event.fire();

					if (closeForm == true) {
						cmp.find("meal-modal").notifyClose();
					}

					var params = {
						title: 'Successfully Saved!',
						mode: 'dismissible',
						message: 'Meal successfully saved to campaign!',
						type: 'success'
					}
                    
					_apostletech.showToast(params);
                
				} else if (state == "ERROR") {
					var errors = action.getError();
                
					if (errors[0] && errors[0].message) {                         
						console.error(errors[0].message);

						var params = {
							title: 'Uh Oh!',
							mode: 'dismissible',
							message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
							type: 'error'
						}
                    
						_apostletech.showToast(params);
					}
				}
			});
        
			$A.enqueueAction(action);		
		}
	},

	initListOptions: function(cmp, tabId) {
		var meal = cmp.get("v.meal");
		var memberInitFlg = cmp.get("v.memberInitFlg");
		var userInitFlg = cmp.get("v.userInitFlg");
		var selectedMembers = [];
		var selectedUsers = [];		

		var availableAttr = 'v.available';
		var selectedAttr = 'v.selected';
		var availableValues = [];
		var selectedValues = [];

		if (tabId === 'members-tab') {		
			meal.availableMembers.forEach(function(item){
				availableValues.push({
					"value": item.value,
					"label": item.label
				});
			});	

			if (memberInitFlg === true) {
				selectedMembers = cmp.get("v.selectedMembers");

				selectedMembers.forEach(function(item){
					selectedValues.push(item);
				});	
			} else {
				selectedMembers = meal.selectedMembers;

				selectedMembers.forEach(function(item){
					selectedValues.push(item.value);
				});	

				cmp.set("v.memberInitFlg", true);
			}

			availableAttr += 'Members';
			selectedAttr += 'Members';
		} else {
			meal.availableUsers.forEach(function(item){
				availableValues.push({
					"value": item.value,
					"label": item.label
				});
			});	

			if (userInitFlg === true) {
				selectedUsers = cmp.get("v.selectedUsers");

				selectedUsers.forEach(function(item){
					selectedValues.push(item);
				});	
			} else {
				selectedUsers = meal.selectedUsers;

				selectedUsers.forEach(function(item){
					selectedValues.push(item.value);
				});	

				cmp.set("v.userInitFlg", true);
			}

			availableAttr += 'Users';	
			selectedAttr += 'Users';				
		}
		
		cmp.set(availableAttr, availableValues);
		cmp.set(selectedAttr, selectedValues);	
	}
})