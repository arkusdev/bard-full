({
	cancel: function(cmp, event, helper) {
        cmp.find("confirm-modal").notifyClose();
	},

	doDelete: function(cmp, event, helper) {
		var campaignId = cmp.get("v.campaignId");
        var memberId = cmp.get("v.memberId");
        var type = cmp.get("v.type");
		var key = cmp.get("v.key");
		var memberFullName = cmp.get("v.memberFullName");

		var event = $A.get("e.c:CampaignMemberDeleted");

		event.setParams({
			"campaignId": campaignId,
			"memberId": memberId,
			"type": type,
			"key": key,
			"memberFullName": memberFullName
		});

		event.fire();		

		cmp.find("confirm-modal").notifyClose();
	}
})