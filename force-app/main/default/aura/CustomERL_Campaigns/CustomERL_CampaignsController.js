({
    oppUpdated: function (cmp, evt, h) {
        var eventParams = evt.getParams();
        if (eventParams.changeType === 'LOADED') {
            var opp = cmp.get('v.opp');
            if (opp.Physician_Champion__c) cmp.set('v.contactId', opp.Physician_Champion__c);
        }
    }
})