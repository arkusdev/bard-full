({
    showErrorMessage : function(component, event) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": event.getParam('title'),
            "message": event.getParam('message'),
            "type": event.getParam('type')
        });
        toastEvent.fire();
    }
})