({
    loadData : function(component) {
		var self = this;
		self.tasksAndSubject = new Map();
        var action = component.get("c.getTaskFormInfo");

        action.setParams({
            projectId: component.get('v.projectId'),
			team: component.get('v.team')
        });

		action.setCallback(this, function(response) {
			var state = response.getState();

			if (state == "SUCCESS") {
				var value = response.getReturnValue();
                component.set('v.preFormInfo', value);

				value.tasksPreFormInfo.forEach(element => {
					self.tasksAndSubject.set(element.taskType, element.subject);
				});

				var action2 = component.get("c.getTableData");
				action2.setCallback(this, function(response) {
					var state = response.getState();
					if (state == "SUCCESS") {
						var value2 = response.getReturnValue();
						component.set('v.tableData', value2);
					} else {
						var errors = action.getError();

						if (errors[0] && errors[0].message) {                         
							console.error(errors[0].message);
						}
					}
				});
				$A.enqueueAction(action2);
			} else {
				var errors = action.getError();

				if (errors[0] && errors[0].message) {                         
					console.error(errors[0].message);
				}
			}
		});
		$A.enqueueAction(action);
    },
	handleChange : function(component, value){
		var self = this;
		component.set('v.subject', self.tasksAndSubject.get(value));
	},
	tasksAndSubject : {}
})