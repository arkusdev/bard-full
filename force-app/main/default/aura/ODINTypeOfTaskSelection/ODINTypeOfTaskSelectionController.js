({
    onInit : function(component, event, helper) {
        helper.loadData(component);
    },
    handleChange : function(component, event, helper) {
        helper.handleChange(component, event.getSource().get("v.value"));
    }
})