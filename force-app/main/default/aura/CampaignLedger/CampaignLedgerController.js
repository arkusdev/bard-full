({
	onInit: function(cmp, event, helper) {
		var reportType = cmp.get("v.reportType");

		switch(reportType) {
			case 'Detail':
				helper.loadDetailTotals(cmp, event);
			break;

			default:
				helper.loadSummaryTotals(cmp, event);
		}
       	
	},

	handleDataChanged: function(cmp, event, helper) {
		var reportType = cmp.get("v.reportType");

		switch(reportType) {
			case 'Detail':
				helper.loadDetailTotals(cmp, event);
			break;

			default:
				helper.loadSummaryTotals(cmp, event);
		}
	}
})