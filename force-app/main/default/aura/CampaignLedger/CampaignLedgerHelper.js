({
	loadSummaryTotals: function(cmp, event) {
		var spinner = cmp.find("spinner");
        $A.util.removeClass(spinner, "slds-hide");

		var campaignId = cmp.get("v.recordId");

        var action = cmp.get("c.fetchSummaryTotals");
        
        action.setParams({
            "campaignId": campaignId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (cmp.isValid() && state == "SUCCESS") {
				var ledgerMap = response.getReturnValue();

				var data = [];

				for(var key in ledgerMap) {
					var value = ledgerMap[key];

					data.push({
						name: key,
						total: (value > 0 ? _apostletech.formatCurrency(value) : '')
					});
				}
				
				cmp.set("v.data", data);

            } else if (state == "ERROR") {
                var errors = action.getError();
                
                if (errors[0] && errors[0].message) {                         
                    console.error(errors[0].message);

                    var params = {
                        title: 'Uh Oh!',
                        mode: 'dismissible',
                        message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
                        type: 'error'
                    }
                    
					_apostletech.showToast(params);
                }
            }

			$A.util.addClass(spinner, "slds-hide");
        });
        
        $A.enqueueAction(action);		
	},

	loadDetailTotals: function(cmp, event) {
		var spinner = cmp.find("spinner");
        $A.util.removeClass(spinner, "slds-hide");

		var columns = [
			{ name: 'Meals' }, 
			{ name: 'Flights' }, 
			{ name: 'Hotel - Present' }, 
			{ name: 'Hotel - Not Present' }, 
			{ name: 'Transportation' }, 
			{ name: 'Incidentals' },
			{ name: 'Total' }
		];

		cmp.set("v.columns", columns);

		var campaignId = cmp.get("v.recordId");

        var action = cmp.get("c.fetchDetailTotals");
        
        action.setParams({
            "campaignId": campaignId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (cmp.isValid() && state == "SUCCESS") {
				var ledgerMap = response.getReturnValue();

				var data = [];
				var expenses = [];

				for(var memberType in ledgerMap) {
					for(var memberIdentifier in ledgerMap[memberType]) {

						var mealExpense = 0; 
						var flightExpsense = 0; 
						var hotelPresentExpense = 0; 
						var hotelNotPresentExpense = 0; 
						var transportationExpense = 0;
						var incidentalsExpense = 0;
						var totalExpense = 0;

						for(var expenseType in ledgerMap[memberType][memberIdentifier]) {
							var expense = ledgerMap[memberType][memberIdentifier][expenseType];
							
							switch(expenseType) {
								case columns[0].name:
									mealExpense = expense;
								break;
								case columns[1].name:
									flightExpsense = expense;
								break;
								case columns[2].name:
									hotelPresentExpense = expense;
								break;
								case columns[3].name:
									hotelNotPresentExpense = expense;
								break;
								case columns[4].name:
									transportationExpense = expense;
								break;
								case columns[5].name:
									incidentalsExpense = expense;
								break;
								case columns[6].name:
									totalExpense = expense;
								break;
							}
						}
		
						data.push({
							name: memberIdentifier.substring(memberIdentifier.indexOf('|') + 1),
							meals: (mealExpense > 0 ? _apostletech.formatCurrency(mealExpense) : ''),
							flights: (flightExpsense > 0 ? _apostletech.formatCurrency(flightExpsense) : ''),
							hotelPresent: (hotelPresentExpense > 0 ? _apostletech.formatCurrency(hotelPresentExpense) : ''),
							hotelNotPresent: (hotelNotPresentExpense > 0 ? _apostletech.formatCurrency(hotelNotPresentExpense) : ''),
							transportation: (transportationExpense > 0 ? _apostletech.formatCurrency(transportationExpense) : ''),
							incidentals: (incidentalsExpense > 0 ? _apostletech.formatCurrency(incidentalsExpense) : ''),
							total: (totalExpense > 0 ? _apostletech.formatCurrency(totalExpense) : '')
						});
					}
				}
				
				cmp.set("v.data", data);
                
            } else if (state == "ERROR") {
                var errors = action.getError();
                
                if (errors[0] && errors[0].message) {                         
                    console.error(errors[0].message);

                    var params = {
                        title: 'Uh Oh!',
                        mode: 'dismissible',
                        message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
                        type: 'error'
                    }
                    
					_apostletech.showToast(params);
                }
            }

			$A.util.addClass(spinner, "slds-hide");
        });
        
        $A.enqueueAction(action);		
	}
})