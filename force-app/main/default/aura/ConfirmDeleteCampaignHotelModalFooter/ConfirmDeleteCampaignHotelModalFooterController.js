({
	cancel: function(cmp, event, helper) {
        cmp.find("confirm-modal").notifyClose();
	},

	doDelete: function (cmp, event, helper) {
		var hotelId = cmp.get("v.hotelId");

        var action = cmp.get("c.deleteCampaignHotel");
        
        action.setParams({
            "hotelId": hotelId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (cmp.isValid() && state == "SUCCESS") {
				var event = $A.get("e.c:CampaignHotelDataChanged");
				event.fire();

				cmp.find("confirm-modal").notifyClose();

                var params = {
                    title: 'Successfully Removed!',
                    mode: 'dismissible',
                    message: 'Hotel successfully removed from campaign!',
                    type: 'success'
                }
                    
				_apostletech.showToast(params);

            } else if (state == "ERROR") {
                var errors = action.getError();
                
                if (errors[0] && errors[0].message) {                         
                    console.error(errors[0].message);

                    var params = {
                        title: 'Uh Oh!',
                        mode: 'dismissible',
                        message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
                        type: 'error'
                    }
                    
					_apostletech.showToast(params);	
                }
            }
        });
        
        $A.enqueueAction(action);
	}
})