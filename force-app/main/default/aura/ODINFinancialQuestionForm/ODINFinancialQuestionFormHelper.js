({
    onloadData : function(component,event,helper) {
        var action = component.get("c.GetProjectQuestions");
        action.setParams({  "recordId" : component.get("v.recordId"),
                            "region" : component.get("v.selectedRegion")
                        });
        action.setCallback(this, function(response) {
            var wrapper = response.getReturnValue();
            if (component.get("v.wrapper") && component.get("v.wrapper") != null) { 
                if (component.get("v.wrapper").questions.length > 0) {
                    wrapper.questions = component.get("v.wrapper").questions;
                }
                if (component.get("v.wrapper").requiredQuestions.length > 0) {
                    wrapper.requiredQuestions = component.get("v.wrapper").requiredQuestions;
                }
            }
            component.set("v.wrapper", wrapper);
            helper.handleChange(component,event,helper);
        })
        $A.enqueueAction(action);   
    },

    handleChange : function(component,event,helper) {
        var compEvent = component.getEvent("questionSelected");
        compEvent.setParams({questions : component.get("v.wrapper")});
        compEvent.fire();
    },
    close : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})