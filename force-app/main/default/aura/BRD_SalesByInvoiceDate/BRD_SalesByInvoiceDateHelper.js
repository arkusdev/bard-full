({
	loadSales: function(cmp, e) {
        var self = this;
		var closeDate = e.getParam('closeDate');
        var accountId = e.getParam('accountId');
		var action = cmp.get('c.getSales');

        action.setParams({
            closeDate: new Date(closeDate),
            accountId: accountId
        });
        action.setStorable();
        cmp.set('v.isBusy', true);
        action.setCallback(this, function(response) {
            var state, data;
            state = response.getState();
            if (state === 'SUCCESS') {
                data = response.getReturnValue();
                cmp.set('v.months', self.getMonthsNames(cmp, data.months));
                cmp.set('v.sales', data.sales);
            } else {
                console.error('There was an error while retrieving the sales', response.getError());
            }
            cmp.set('v.isBusy', false);
        });
        $A.enqueueAction(action);
	},
    getMonthsNames: function(cmp, months) {
        var result = [];
        if (typeof months.forEach === 'function') {
            months.forEach(function(month) {
                var d = (new Date(month)).toUTCString().split(', ')[1].split(' ')[1];
                result.push(d);
            });
        }
        //for (var i = 0; i < 10; i++) { result = result.concat(result)}
        return result;
    },
    roundNumbers: function(cmp, sales) {
        if (typeof sales.forEach === 'function') {
            sales.forEach(function(sale) {
                if (typeof sale.values.forEach === 'function') {
                    var newVals = [];
                    sale.values.forEach(function(value) {
                        newVals.push(Number.parseInt(value.toString().split('.')[0]));
                    });
                    sale.values = newVals;
                }
            });
        }
        return sales;
    },
    fillEmtpyCells: function(cmp, sales) {
        /*if (typeof sales.forEach === 'function') {
            var maxlen = 0;
            sales.forEach(function(sale) {
                maxlen = sale.values.length > maxlen ? sale.values.length : maxlen;
            });
            sales.forEach(function(sale) {
                var diff = maxlen - sale.values.length;
                if (diff > 0) {
                    for (var i = 0; i < diff; i++) {
                        sale.values.push(undefined);
                    }
                }
            });
        }*/
        return sales;
    }
})