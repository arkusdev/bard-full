({
    onloadData : function(component,event,helper) {
        var selYear = component.get("v.selectedYear");

        var action = component.get("c.GetFinancialInfo");
        action.setParams({  "recordId" : component.get("v.recordId"),
                            "region" : component.get("v.selectedRegion")
                        });
        action.setCallback(this, function(response) {
            var wrapper = response.getReturnValue();
            component.set('v.originalFirstYear' , wrapper.startYear);
            component.set('v.firstYear', wrapper.startYear);
            if (selYear && selYear != "") {
                wrapper.startYear = selYear;
            }

            if (component.get("v.financial") && component.get("v.financial") != null) {
                wrapper.financial = component.get("v.financial");
            }
            component.set("v.wrapper", wrapper);

            var yearOptions = [{ "value": "", "label": "--None--"}]; 
            
            wrapper.years.forEach(element => {
                
                if (selYear && selYear != "") {
                    if (selYear == element) {
                        yearOptions.push({ "value": element, "label": element, "selected": true});
                    } else {
                        yearOptions.push({ "value": element, "label": element });
                    }
                } else {
                    if (component.get("v.wrapper").startYear == element) {
                        
                        yearOptions.push({ "value": element, "label": element, "selected": true});

                    } else {
                        yearOptions.push({ "value": element, "label": element });
                    }
                }

            });
            var financialsPerYear = wrapper.financialsProjectPerYearList;
            component.set('v.selectedYear', component.get('v.originalFirstYear'));
            component.set('v.selectedRecord', financialsPerYear.length > 0 ? financialsPerYear[0].name : '');
            component.set("v.yearValues", yearOptions);
            component.set("v.projectName", wrapper.financial.name);
            helper.handleChange(component,event,helper);
        })
        $A.enqueueAction(action); 
    },

    handleChange : function(component,event,helper) {
        if (!component.get('v.noInterestSelected')) helper.changeInputValues(component, component.find('startYear').get('v.value'));
        var compEvent = component.getEvent("financialSelected");
        compEvent.setParams({financials : component.get("v.wrapper"), isValid : helper.isValid(component,event,helper)});
        compEvent.fire();
    },

    changeInputValues : function(component, yearSelected){
        if (yearSelected && component.get('v.selectedYear') != yearSelected){
            var wrapper = component.get('v.wrapper');
            var firstYear = component.get('v.firstYear');
            if (!firstYear) component.set('v.firstYear', wrapper.startYear);
            var index = yearSelected - component.get('v.originalFirstYear');
            if (component.get('v.originalFirstYear') != component.get('v.firstYear')) component.set('v.firstYear', component.get('v.originalYear'));
            component.set('v.firstYear', yearSelected);
            if (wrapper.financialsProjectPerYearList[index]){
                var financial = wrapper.financialsProjectPerYearList[index];
                component.set('v.selectedRecord', financial.name);
                component.set('v.wrapper.financial.financial', financial.financial);
            }else{
                component.set('v.selectedRecord', '');
                component.set('v.wrapper.financial.financial', {});
                //component.set('v.wrapper.financial.financial', null);
                //component.set('v.wrapper.financial.name', '');
                //component.set('v.wrapper.financial.financial', null);
            }
        }
    },

    isValid : function(component,event,helper) {
        var isValid = true;
        if (component.find('validInput')) {
            component.find('validInput').forEach(element => {
                
                if (element.get('v.validity') && !element.get('v.validity').valid) {
                    isValid = false;
                }
            });
        }
        return isValid;
    },

    close : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})