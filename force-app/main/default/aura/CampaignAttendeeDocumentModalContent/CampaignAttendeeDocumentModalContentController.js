({
	onInit: function(cmp, event, helper) { 
		var action = cmp.get("c.fetchCampaignAttendeeDocuments");

		action.setParams({
			"attendeeId": cmp.get("v.attendeeId"),
			"campaignId": cmp.get("v.campaignId"),
			"type": cmp.get("v.type")
		});

		action.setCallback(this, function(response) {
			var state = response.getState();

			if (cmp.isValid() && state == "SUCCESS") {
				var values = response.getReturnValue();

				var documents = [];
				var selected = [];

				values.forEach(function(item){
					documents.push({
						"value": item.memberDocId,
						"label": item.campaignDocName
					});

					if(item.received === true) { 
						selected.push(item.memberDocId); 
					}
				});

				cmp.set("v.documents", documents);
				cmp.set("v.selected", selected);
				
			} else if (state == "ERROR") {
				var errors = action.getError();

				if (errors[0] && errors[0].message) {                         
					console.error(errors[0].message);

                    var params = {
                        title: 'Uh Oh!',
                        mode: 'dismissible',
                        message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
                        type: 'error'
                    }
                    
					_apostletech.showToast(params);	
				}
			}
		});

		$A.enqueueAction(action);
	},

	save: function(cmp, event, helper) { 
		var documents = cmp.get("v.documents");
		var selected = cmp.get("v.selected");
		var attendeeName = cmp.get("v.attendeeName");
		var dto = [];

		documents.forEach(function(item) {
			dto.push({
				memberDocId: item.value,
				received: selected.includes(item.value)
			});
		});

        var action = cmp.get("c.saveCampaignAttendeeDocuments");
        
        action.setParams({
            "dto": JSON.stringify(dto)
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (cmp.isValid() && state == "SUCCESS") {
				var ev = $A.get("e.c:CampaignAttendeeDocumentDataChanged");
				ev.fire();

				cmp.find("doc-modal").notifyClose();

				var params = {
                    title: 'Successfully Saved!',
                    mode: 'dismissible',
                    message: attendeeName + ' credential data successfully saved!',
                    type: 'success'
				}
                    
				_apostletech.showToast(params);	
                
            } else if (state == "ERROR") {
                var errors = action.getError();
                
                if (errors[0] && errors[0].message) {                         
                    console.error(errors[0].message);

                    var params = {
                        title: 'Uh Oh!',
                        mode: 'dismissible',
                        message: 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.',
                        type: 'error'
                    }
                    
					_apostletech.showToast(params);	
                }
            }
        });
        
        $A.enqueueAction(action);
	},

	cancel: function(cmp, event, helper) { 
		cmp.find("doc-modal").notifyClose();
	},

    handleChange: function (cmp, event) {
		var value = event.getParam("value");
        cmp.set("v.selected", value);
    }
})