({
    oppUpdated: function (cmp, evt, h) {
        var eventParams = evt.getParams();
        if (eventParams.changeType === 'LOADED') {
            var opp = cmp.get('v.opp');
            cmp.set('v.accountId', opp.AccountId);
            var and = " AND ";
            var cond1 = "Type = 'WavelinQ - Proctor'";
            var cond2 = and + "RecordType.Name LIKE '%WavelinQ%'";
            var cond3 = and + "Opportunity__c = '" + cmp.get('v.recordId') + "'";
            var cond4 = opp.Physician_Champion__c ? and + "ContactId = '" + opp.Physician_Champion__c + "'" : "";
            var filter = cond1 + cond2 + cond3 + cond4;
            cmp.set('v.filter', filter);
        }
    }
})