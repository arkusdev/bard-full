({
    getCalculationMethodParams: function (cmp, view, isLoop, userId) {
        var result = {
            period: cmp.get('v.period'),
            view: view,
            distributor: cmp.get('v.distributor')
        };

        if (view === 'product') Object.assign(result, {accountID:cmp.get('v.accountID')});

        if (isLoop) Object.assign(result, {userId:userId, topUser:false});
        else Object.assign(result, {userId: cmp.get('v.user'), topUser:true});

        return result;
    },
    // previously named 'consolidateLoopsByUser'
    sumSales: function(prevData, newData) {
        let self = this;
        //let data = JSON.parse(JSON.stringify(prevData));
        //prevData.irsaAggr = [];
        //prevData.shadAggr = [];
        /*let prevSales = prevData.irsaAggr.filter(x => x.rt !== 'Distributor');
        let newSales = newData.irsaAggr.filter(x => x.rt !== 'Distributor');
        let irsaKeys = this.getUniqueAggrKeys(prevSales, newSales);

        let prevDists = prevData.irsaAggr.filter(x => x.rt === 'Distributor');
        let newDists = newData.irsaAggr.filter(x => x.rt === 'Distributor');
        let distKeys = this.getUniqueAggrKeys(prevDists, newDists);
        
        let prevShad = prevData.shadAggr.map(x => x);
        let newShad = newData.shadAggr.map(x => x);
        let shadKeys = this.getUniqueAggrKeys(prevShad, newShad);

        irsaKeys.forEach(function (key) {
            let item = self.processKeys(key, prevSales, newSales);
            let match = prevData.irsaAggr.find(b => { return key === item.itemKey && b.rt !== 'Distributor' });
            if (match) match.sales = item.sales;
            else prevData.irsaAggr.push(item);
        });
        distKeys.forEach(function (key) {
            let item = self.processKeys(key, prevDists, newDists);
            let match = prevData.irsaAggr.find(b => { return key === item.itemKey && b.rt === 'Distributor' });
            if (match) match.sales = item.sales;
            else prevData.irsaAggr.push(item);
        });
        shadKeys.forEach(function (key) {
            let item = self.processKeys(key, prevShad, newShad);
            let match = prevData.shadAggr.find(b => { return key === item.itemKey });
            if (match) match.sales = item.sales;
            else prevData.shadAggr.push(item);
        });
        return prevData;*/

        newData.irsaAggr.forEach(function(a) {
            var matchItem = prevData.irsaAggr.find(function(b) { 
                var keyA = self.genKey(a);
                var keyB = self.genKey(b);
                return keyA === keyB && a.rt === b.rt;
            });
            if (matchItem) {
                matchItem.sales += a.sales;
            } else {
                prevData.irsaAggr.push(a);
            }
        });
        newData.shadAggr.forEach(function(a) {
            var matchItem = prevData.shadAggr.find(function(b) { 
                var keyA = self.genKey(a);
                var keyB = self.genKey(b);
                return keyA === keyB;
            });
            if (matchItem) {
                matchItem.sales += a.sales;
            } else {
                prevData.shadAggr.push(a);
            }
        });
        newData.oppsList.forEach(function(a) {
            prevData.oppsList.push(a);
        });
        
        return prevData;
    },
    sumSalesOpp: function(prevData, newData) {
        let self = this;

        newData.irsaAggr.forEach(function(a) {
            
            prevData.irsaAggr.push(a);
            
        });
        newData.shadAggr.forEach(function(a) {
            
            prevData.shadAggr.push(a);
            
        });
        newData.oppsList.forEach(function(a) {
            prevData.oppsList.push(a);
        });
        
        return prevData;
    },
    getUniqueAggrKeys: function (prevList, newList) {
        let keySet = new Set(prevList.concat(newList).map(b => { return this.genKey(b) }));
        return Array.from(keySet);
    },
    genKey: function (item) {
        const cs = '%&%';
        return (item.Account__c ? (item.accName + cs + item.accShip) : item.Product_Group__c) + cs + item.CAT__c + cs + item.Reporting_Product_Group__c;
    },
    processKeys: function (key, prevList, newList) {
        let prevItem = prevList.find(b => { return key === this.genKey(b) });
        let newItem = newList.find(b => { return key === this.genKey(b) });
        let salesVal = (prevItem ? prevItem.sales : 0) + (newItem ? newItem.sales : 0)
        let item = JSON.parse(JSON.stringify((prevItem ? prevItem : newItem)));
        item.sales = salesVal;
        return item;
    },
    // previously named 'completeLoopCalc'
    setBaselineAndGap: function(data, cmp, setOriginal) {
        if (setOriginal) cmp.set('v.daysConfig', {daysInPriorYear:data.daysInPriorYear,currentWorkingDays:data.currentWorkingDays});
        else Object.assign(data, cmp.get('v.daysConfig'));

        data.tableData.forEach(function(item) {
            item.sales = item.irsaSales;
            item.tdBaseline = item.shadSales / data.daysInPriorYear * data.currentWorkingDays;
            item.gap = item.irsaSales - item.tdBaseline;
            item.gapPercentage = item.tdBaseline !== 0 ? (item.irsaSales / item.tdBaseline) : undefined;
            
            //round values - doing this after all values have been calculated
            item.tdBaselineNoRounding = item.tdBaseline;
            item.gapNoRounding = item.gap;
            item.gapPercentageNoRounding = item.gapPercentage;
            item.sales = Math.trunc(item.sales);
            item.tdBaseline = Math.trunc(item.tdBaseline);
            item.gap = Math.trunc(item.gap);
            // get rid of confusing -0
            if (item.gapPercentage === 0 && (1/item.gapPercentage) === -Infinity) item.gapPercentage = 0;
        });
        return data;
    },
    // previously named 'prepareTableData'
    addTotalsAndTrends: function(data) {
        if (!data.tableData || !data.tableData.filter) return data;

        var newTable = data.tableData.filter(x => { return x.accountProduct !== undefined || x.accountProduct !== 'Total'});

        
        newTable.forEach(function(x, i) {
            x.salesIsNegative = x.sales < 0;
            x.tdBaselineIsNegative = x.tdBaseline < 0;
            x.gapIsNegative = x.gap < 0;
            x.gapPercentageIsNegative = x.gapPercentage < 0;
            if (x.accountProduct !== 'Total') {
                if (x.gap < 0) {
                    x.trend = 'negative';
                } else if (x.gap > 0) {
                    x.trend = 'positive';
                } else {
                    x.trend = 'neutral';
                }
            }
			//if (x.accountID) x.accountUrl = '/' + x.accountID;
        });
        
        var totals = {
            accountProduct:'Total',
            sales: newTable.map(x => x.sales).reduce(function(a,b){return a+b}, 0),
            tdBaseline: newTable.map(x => x.tdBaseline).reduce(function(a,b){return a+b}, 0),
            gap: newTable.map(x => x.gap).reduce(function(a,b){return a+b}, 0),
            gapPercentage: newTable.map(x => x.gapPercentageNoRounding).reduce(function(a,b){return a+b}, 0)
        };
        
        if (totals.gap < 0) {
            totals.trend = 'negative';
        } else if (totals.gap > 0) {
            totals.trend = 'positive';
        } else {
            totals.trend = 'neutral';
        }

        var empty = {
            accountProduct: undefined,
            shipToID: undefined,
            city: undefined,
            trend: undefined,
            sales: undefined,
            tdBaseline: undefined,
            gap: undefined,
            gapPercentage: undefined
        }

        //if (view === 'customer') Object.assign(empty, {shipToID: undefined,city: undefined});

        newTable.push(empty);
        newTable.push(totals);
        data.tableData = newTable;

        return data;
    },
    setFiltersFromTableData: function(cmp, tableData, view, oppsList, catList) {
        if (!tableData) return;
        
        var catFilters          = [];
        var pgFilters           = [];
        var stgFilters          = [];
        var reportingFilters    = [];
        var productFilters    = [];

        if (view != 'opportunity') {
            tableData.forEach(function(row) {
            
            
                var prevCat = catFilters.find(x => { return x.value === row.category });
                if (!prevCat) {
                    catFilters.push({
                        label: row.category,
                        value: row.category
                    });
                }
                var prevReporting = reportingFilters.find(x => { return x.value === row.reportingPG });
                if (!prevReporting) {
                    if (row.reportingPG.toUpperCase().startsWith('LUTONIX')){
                        if (!reportingFilters.find(x => { return x.value === 'LUTONIX ALL' })){
                            reportingFilters.push({
                                label: 'LUTONIX ALL',
                                value: 'LUTONIX ALL'
                            });
                        }
                    }
                    reportingFilters.push({
                        label: row.reportingPG,
                        value: row.reportingPG
                    });
                }
                var prevPG = pgFilters.find(x => { return x.value === row.productGroup });
                if (!prevPG) {
                    if (row.productGroup.toUpperCase().startsWith('LUTONIX')){
                        if (!pgFilters.find(x => { return x.value === 'LUTONIX ALL' })){
                            pgFilters.push({
                                label: 'LUTONIX ALL',
                                value: 'LUTONIX ALL'
                            });
                        }
                    }
                    pgFilters.push({
                        label: row.productGroup,
                        value: row.productGroup
                    });
                }
            });
        }

        if (view === 'opportunity') {
            oppsList.forEach(function(row) {
                var prevStage = stgFilters.find(x => { return x.value === row.StageName });
                if (!prevStage) {
                    if (row.StageName === 'Closed Won') {
                        stgFilters.push({
                            label: row.StageName,
                            value: row.StageName,
                            selected: true
                        });
                    } else {
                        stgFilters.push({
                            label: row.StageName,
                            value: row.StageName
                        });
                    }
                }
                var prevProductGroup = productFilters.find(x => { return x.value === row.Product_Group__c });
                if (!prevProductGroup && row.Product_Group__c) {
                    if (row.Product_Group__c.toUpperCase().startsWith('LUTONIX')){
                        if (!productFilters.find(x => { return x.value === 'LUTONIX ALL' })){
                            productFilters.push({
                                label: 'LUTONIX ALL',
                                value: 'LUTONIX ALL'
                            });
                        }
                    }
                    productFilters.push({
                        label: row.Product_Group__c.toUpperCase(),
                        value: row.Product_Group__c
                    });
                }
            });
            stgFilters.sort(function(a, b) {
                return a.value.localeCompare(b.value);
            });
            stgFilters.unshift({'label':'All Closed','value':'allClosed'});
            stgFilters.unshift({'label':'All Open','value':'allOpen'});
            stgFilters.unshift({'label':'All Stages','value':'all'});
            cmp.set('v.stages', stgFilters);
            if (cmp.find('stageCB')) cmp.find('stageCB').set('v.value', 'Closed Won');

            productFilters.sort(function(a, b) {
                return a.value.localeCompare(b.value);
            });
            productFilters.unshift({'label':'All Product Groups','value':'all'});
            cmp.set('v.productGroups', productFilters);
        } else {

            catFilters.sort(function(a, b) {
                return a.value.localeCompare(b.value);
            });
            
            if (catList && catList.length > 1) {
                var foundQuota;
                var quotaList = [];
                var foundNSC;
                var NSCList = [];

                catList.forEach(function(row) {

                    if (row.Quota__c) {
                        quotaList.push(row.MasterLabel);
                        var prevQuota = catFilters.find(x => { return x.value === row.MasterLabel});
                        if (prevQuota)
                            foundQuota = true;
                    }
                    if (row.NSC__c) {
                        NSCList.push(row.MasterLabel);
                        var prevNSC = catFilters.find(x => { return x.value === row.MasterLabel});
                        if (prevNSC)
                            foundNSC = true;
                    }
                });
                if (foundQuota) {

                    catFilters.unshift({
                        'label' : 'All Quota',
                        'value' : quotaList.join(',')
                    });
                }
                if (foundNSC) {

                    catFilters.unshift({
                        'label' : 'All NSC',
                        'value' : NSCList.join(',')
                    });
                }
            }
            

            catFilters.unshift({'label':'All Categories','value':'all'});
            cmp.set('v.categories', catFilters);
            if (cmp.find('catCB')) cmp.find('catCB').set('v.value', 'all');

            reportingFilters.sort(function(a, b) {
                return a.value.localeCompare(b.value);
            });

            reportingFilters.unshift({'label':'All Product Groups','value':'all'});
            cmp.set('v.reportingPGs', reportingFilters);
            if (cmp.find('reportigPG')) cmp.find('reportigPG').set('v.value', 'all');

            pgFilters.sort(function(a, b) {
                return a.value.localeCompare(b.value);
            });
            
            pgFilters.unshift({'label':'All Reporting Product Groups','value':'all'});
    
            cmp.set('v.groups', pgFilters);
            cmp.find('pgCB').set('v.value', 'all');
        }


    },
    unifyValues: function (data, setOriginal, h, view) {
        if (!data.tableData || !data.tableData.forEach) return data;

        if (setOriginal) {
            h.originalTable = JSON.parse(JSON.stringify(data.tableData));
        }
        var newTable = [];
        data.tableData.forEach(function(row) {
            var prev;
            if (view === 'product') prev = newTable.find(x => { return x.accountProduct === row.accountProduct });
            else if (view === 'customer') prev = newTable.find(x => { return x.accountID === row.accountID && x.shipToID === row.shipToID });
            else if (view === 'opportunity') prev = newTable.find(x => { return x.accountProductOwner === row.accountProductOwner });

            if (prev) {
                prev.irsaSales += row.irsaSales;
                prev.shadSales += row.shadSales;
            } else {
                newTable.push(row);
            }
        });
        data.tableData = newTable;
        return data;
    },
    unifyWithOpps: function (cmp, data, setOriginal, h) {
        if (setOriginal) {
            h.originalOppTable = JSON.parse(JSON.stringify(data.oppsList));
            h.sellingDays = JSON.parse(JSON.stringify(data.totalWorkingDays));
            h.ongoing = JSON.parse(JSON.stringify(data.currentWorkingDays));
        } else {
            data.oppsList = h.originalOppTable;
            data.totalWorkingDays = h.sellingDays;
            data.currentWorkingDays = h.ongoing;
        }

        if (!data.tableData || !data.tableData.forEach || !data.oppsList || !data.oppsList.forEach) return data;

        var stage = cmp.get('v.stage');
        var productGroup = cmp.get('v.productGroup');

        var tableData = [];

        data.oppsList.forEach(function(row) {
            var newRow = row;
            var productRow;
            if (row.Product_Group__c) {
                var rowProductGroup = row.Product_Group__c.toLowerCase().includes('wavelinq') ? 'wavelinq' : row.Product_Group__c.toLowerCase();
                var pgLC = rowProductGroup + row.OwnerId + row.AccountId;
                productRow = data.tableData.find(x => { return x.accountProductOwner === pgLC });
            }
            var allLutonixCondition = productGroup == 'LUTONIX ALL' && row.Product_Group__c.toUpperCase().startsWith('LUTONIX');
            if ((stage === 'all' || row.StageName === stage || (stage === 'allOpen' && !row.IsClosed) || (stage === 'allClosed' && row.IsClosed)) && (productGroup === 'all' || row.Product_Group__c === productGroup) || allLutonixCondition) {
                newRow.owner = row.Owner.Name;
                newRow.accountProduct = row.Product_Group__c ? row.Product_Group__c.toUpperCase() : '';
                newRow.accountName = row.Account.Name;
                newRow.age = row.Age__c;
                newRow.closeDate = row.CloseDate;
                newRow.stage = row.StageName;
                newRow.amount = row.Amount != null ? row.Amount : 0;
                newRow.amountIsNegative = newRow.amount < 0;
                newRow.carryOver = row.Carry_Over__c != null ? row.Carry_Over__c : 0;
                newRow.carryOverIsNegative = newRow.carryOver < 0;
                newRow.shipToId = row.Account.Ship_to_ID__c;
                newRow.fy = newRow.amount - newRow.carryOver;
                newRow.fyIsNegative = newRow.fy < 0;
                newRow.closes = row.Closing_in__c <= 30 && row.Closing_in__c >= 0;
                
                if (productRow) {
                    newRow.sales = productRow.sales;
                    newRow.salesIsNegative = productRow.sales < 0;
                } else {
                    newRow.sales = 0;
                }

                newRow.fyProjection = ((newRow.sales / data.currentWorkingDays) * (data.totalWorkingDays - data.currentWorkingDays)) + newRow.sales;
                newRow.fyProjectionIsNegative = newRow.fyProjection < 0;
                newRow.nextFYProjection = (newRow.sales / data.currentWorkingDays) * 252;
                newRow.nextFYProjectionIsNegative = newRow.nextFYProjection < 0;

                tableData.push(newRow);
            }
        });
        data.tableData = tableData;
        cmp.set('v.tableData', tableData);
        cmp.set('v.isBusy', false);
        return data;
    },
    finalAssignment: function(cmp, data, h, view) {
        this.joinRecords(data, view);
        this.setFiltersFromTableData(cmp, data.tableData, view, data.oppsList, data.catList);
        this.modifyValuesPostFilter(cmp, data, view, true, h);
        this.setToDateColumnName(cmp);
        // this.checkPaginationButtons(cmp, cmp.get('v.start'), data.tableData.length);
        
        this.firstPage(cmp, data.tableData);

        cmp.set('v.sortReverse', ',false');
    },
    joinRecords: function(data, view) {
        var cs = '%&%';
        let irsaKeys = data.irsaAggr.map(x => {
            let prefix;
            if (view != 'opportunity') {
                prefix = x.Account__c ? (x.accName + cs + x.accShip) : x.Product_Group__c;
            } else {
                prefix = x.Product_Group__c + cs + x.OwnerId + cs + x.Account__c;
            }
            x.itemKey = prefix + cs + x.CAT__c + cs + x.Reporting_Product_Group__c;
            return x.itemKey;
        });
        let shadKeys = data.shadAggr.map(x => {
            let prefix;
            if (view != 'opportunity') {
                prefix = x.Account__c ? (x.accName + cs + x.accShip) : x.Product_Group__c;
            } else {
                prefix = x.Product_Group__c + cs + x.OwnerId + cs + x.Account__c;
            }
            x.itemKey = prefix + cs + x.CAT__c + cs + x.Reporting_Product_Group__c;
            return x.itemKey;
        });


        data.tableData = [];
        let uniqueKeys = Array.from(new Set(irsaKeys.concat(shadKeys)));
        uniqueKeys.forEach(function(k) {
            let irsa;
            let irsas = data.irsaAggr.filter(x => k === x.itemKey);

            // join normal sales and distributor
            if (irsas.length > 0) {
                irsa = JSON.parse(JSON.stringify(irsas[0]));
                if (irsas.length === 1 && irsa.rt === 'Distributor') {
                    irsa.sales = irsa.sales / data.totalWorkingDays * data.currentWorkingDays;
                } else {
                    let salesValue = irsas.reduce((a,b) => {
                        let bVal = b.rt === 'Distributor' ? b.sales / data.totalWorkingDays * data.currentWorkingDays : b.sales;
                        return a + bVal;
                    }, 0);
                    
                    irsa.sales = salesValue;
                }
            }
            let shad = data.shadAggr.find(x => k === x.itemKey);
            if (!irsa) irsa = {sales: 0};
            if (!shad) shad = {sales: 0};
            let row = {};
            row.irsaSales = irsa.sales;
            row.shadSales = shad.sales;
            row.category = irsa.CAT__c ? irsa.CAT__c : shad.CAT__c;
            row.productGroup = irsa.Reporting_Product_Group__c ? irsa.Reporting_Product_Group__c : shad.Reporting_Product_Group__c;
            row.itemKey = k;
            row.OwnerId = irsa.OwnerId;
            row.reportingPG = irsa.Category__c ? irsa.Category__c : shad.Category__c;
            if (view === 'customer') {
                row.accountID = irsa.Account__c ? irsa.Account__c : shad.Account__c;
                row.accountProduct = irsa.accName ? irsa.accName : shad.accName;
                row.shipToID = irsa.accShip ? irsa.accShip : shad.accShip;
                row.city = irsa.accCity ? irsa.accCity : shad.accCity;
            } else if (view === 'product') {
                row.accountProduct = irsa.Product_Group__c ? irsa.Product_Group__c : shad.Product_Group__c;
            } else if (view === 'opportunity') {
                row.accountProduct = irsa.Product_Group__c ? irsa.Product_Group__c.toLowerCase() : shad.Product_Group__c.toLowerCase();
                var accountID = irsa.Account__c ? irsa.Account__c : shad.Account__c;
                row.accountProductOwner = row.accountProduct + row.OwnerId + accountID;
            }

            data.tableData.push(row);
        });

    },
    modifyValuesPostFilter: function(cmp, data, view, setOriginal, h) {
        data = this.unifyValues(data, setOriginal, h, view);
        data = this.setBaselineAndGap(data, cmp, setOriginal);
        
        if (view === 'opportunity') {
            data = this.unifyWithOpps(cmp, data, setOriginal, h);
            //this.sortOpps(cmp, 'string,accountProduct', data.tableData);
            if (!setOriginal) {
                this.firstPage(cmp, data.tableData);
            }
        } else {
            data = this.addTotalsAndTrends(data);
            this.sort(cmp, 'string,accountProduct', data.tableData);
        }
    },
    isHandleByFront: function (data) {
        return data.irsaAggr && data.shadAggr && data.irsaAggr.length > 0 && data.shadAggr.length > 0;
    },
    sort: function (cmp, dsSort, firstData) {
        var isFirstData = firstData !== undefined;
        cmp.set('v.isBusy', true);
        window.setTimeout($A.getCallback(function() {
            var sortType = dsSort.split(',')[0];
            var sortField = dsSort.split(',')[1];
            var reverseField = cmp.get('v.sortReverse').split(',')[0];
            var reverseValue = JSON.parse(cmp.get('v.sortReverse').split(',')[1].toLowerCase());
            var tableData = firstData ? firstData : cmp.get('v.tableData');
            var totals = tableData.splice(-2, 2);
            
            if (sortField === reverseField && !isFirstData) reverseValue = !reverseValue;
            else reverseValue = false;
            
            if (sortType === 'string') {
                tableData.sort(function(a, b) {
                    //var result;
                    var aField = a[sortField] ? a[sortField] : '';
                    var bField = b[sortField] ? b[sortField] : '';

                    //if ((reverseValue && !bField) || (!reverseValue && ! aField)) result = -1;
                    if (reverseValue) return bField.localeCompare(aField);
                    else return aField.localeCompare(bField);
                    
                    //return result;
                });
            } else if (sortType === 'number') {
                tableData.sort(function(a, b) {
                    var result = 0;
                    
                    if (a[sortField] === undefined) result = -1;
                    else if (reverseValue) result = b[sortField] - a[sortField];
                    else result = a[sortField] - b[sortField];

                    return result;
                });
            } else if (sortType === 'trend') {
                tableData.sort(function(a, b) {
                    var result = 0;
                    if (!a[sortField]) return result = -1;
                    var trends = {negative:'a',neutral:'b',positive:'c'};
                    var current = trends[a[sortField]];
                    var next = trends[b[sortField]];

                    if (current < next) result = -1;
                    else if (current > next) result = 1;
                    
                    if (reverseValue) result = result * -1;
                    return result;
                });
            }
            
            tableData = tableData.concat(totals);
            cmp.set('v.tableData', tableData);
            reverseField = sortField;
            cmp.set('v.sortReverse', reverseField+','+reverseValue);
            cmp.set('v.isBusy', false);
        }));
    },
    sortOpps: function (cmp, dsSort, firstData) {
        var isFirstData = firstData !== undefined;
        cmp.set('v.isBusy', true);
        //window.setTimeout($A.getCallback(function() {
            var sortType = dsSort ? dsSort.split(',')[0] : '';
            var sortField = dsSort ? (dsSort.split(',').length > 0 ? dsSort.split(',')[1] : '') : '';
            var reverseField = cmp.get('v.sortReverse').split(',')[0];
            var reverseValue = JSON.parse(cmp.get('v.sortReverse').split(',')[1].toLowerCase());
            var tableData = firstData ? firstData : cmp.get('v.tableData');
            
            if (sortField === reverseField && !isFirstData) reverseValue = !reverseValue;
            else reverseValue = false;
            
            if (sortType === 'string') {
                tableData.sort(function(a, b) {
                    //var result;
                    var aField = a[sortField] ? a[sortField] : '';
                    var bField = b[sortField] ? b[sortField] : '';

                    if (reverseValue) return bField.localeCompare(aField);
                    else return aField.localeCompare(bField);
                    
                    //return result;
                });
            } else if (sortType === 'number') {
                tableData.sort(function(a, b) {
                    var result = 0;
                    
                    if (a[sortField] === undefined) result = -1;
                    else if (reverseValue) result = b[sortField] - a[sortField];
                    else result = a[sortField] - b[sortField];

                    return result;
                });
            } else if (sortType === 'trend') {
                tableData.sort(function(a, b) {
                    var result = 0;
                    if (!a[sortField]) return result = -1;
                    var trends = {negative:'a',neutral:'b',positive:'c'};
                    var current = trends[a[sortField]];
                    var next = trends[b[sortField]];

                    if (current < next) result = -1;
                    else if (current > next) result = 1;
                    
                    if (reverseValue) result = result * -1;
                    return result;
                });
            }
            
            cmp.set('v.tableData', tableData);
            reverseField = sortField;
            cmp.set('v.sortReverse', reverseField+','+reverseValue);
            cmp.set('v.isBusy', false);
        //}));
    },
    setToDateColumnName: function(cmp) {
        var period = cmp.get('v.period');
        var letter = '';

        if (period === 'month') letter = 'M';
        else if (period === 'quarter') letter = 'Q';
        else if (period === 'year') letter = 'Y';

        cmp.set('v.toDateColumnName', letter);
    },
    nextPage: function(cmp) {
        var pageSize = cmp.get('v.pageSize');
        var tableData = cmp.get('v.tableData');
        var start = cmp.get('v.start') + pageSize;
        var end = cmp.get('v.end') + pageSize;
        
        if (start < tableData.length) {
            cmp.set('v.start', start);
            cmp.set('v.end', end);
        }
        this.checkPaginationButtons(cmp, start, tableData.length);
        cmp.find('pgCB').focus();
    },
    prevPage: function(cmp) {
        var pageSize = cmp.get('v.pageSize');
        var tableData = cmp.get('v.tableData');
        var start = cmp.get('v.start') - pageSize;
        var end = cmp.get('v.end') - pageSize;
        
        if (start >= 0) {
            cmp.set('v.start', start);
            cmp.set('v.end', end);
        }
        this.checkPaginationButtons(cmp, start, tableData.length);
        cmp.find('pgCB').focus();
    },
    firstPage: function(cmp, table) {
        var pageSize = cmp.get('v.pageSize');
        var tableData = table ? table : cmp.get('v.tableData');
        var start = 0;
        var end = pageSize;

        if (start < tableData.length) {
            cmp.set('v.start', start);
            cmp.set('v.end', end);
        }
        this.checkPaginationButtons(cmp, start, tableData.length);
        if (!table) cmp.find('pgCB').focus();
    },
    lastPage: function(cmp) {
        var pageSize = cmp.get('v.pageSize');
        var tableData = cmp.get('v.tableData');
        var diff = tableData.length % pageSize;
        var start = tableData.length - (diff > 0 ? diff : pageSize);
        var end = start + pageSize;
        
        if (start < tableData.length) {
            cmp.set('v.start', start);
            cmp.set('v.end', end);
        }
        this.checkPaginationButtons(cmp, start, tableData.length);
        cmp.find('pgCB').focus();
    },
    checkPaginationButtons: function (cmp, start, len) {
        var pageSize = cmp.get('v.pageSize');
        var disablePrev = start - 1 <= 0;
        var disableNext = start + pageSize >= len;
        cmp.set('v.disableFirst', disablePrev);
        cmp.set('v.disablePrev', disablePrev);
        cmp.set('v.disableNext', disableNext);
        cmp.set('v.disableLast', disableNext);

        var leg = (start/pageSize + 1) + '/' + Math.ceil(len/pageSize);
        cmp.set('v.pagLegend', leg);
    },
    filter: function(cmp, h, view) {
        cmp.set('v.isBusy', true);
        var newTable = [];
        var tableData = JSON.parse(JSON.stringify(h.originalTable));
        
        var category;
        var reportingPG;
        if (view != 'opportunity') {
            category = cmp.get('v.category');
            reportingPG = cmp.get('v.reportingPG');
        }
        
        var group = cmp.get('v.group');

        tableData.forEach(function(row) {
            var newRow = JSON.parse(JSON.stringify(row));
            
            if (view != 'opportunity') {
                var hasGroup = group === 'all' || row.productGroup === group 
                    || group.toUpperCase() === 'LUTONIX ALL' && row.productGroup.startsWith('LUTONIX');    
                var hasCategory = category === 'all' || row.category === category;
                
                category.split(',').forEach(function(catRow) {
                    if (row.category === catRow) {
                        hasCategory = true;
                    }
                });

                var hasReportingPG = reportingPG === 'all' || row.reportingPG === reportingPG;
                
                if (!(hasCategory && hasGroup && hasReportingPG)) {
                    newRow.irsaSales = 0;
                    newRow.shadSales = 0;
                }

                
            }
            newTable.push(newRow);
        });

        this.modifyValuesPostFilter(cmp, {tableData:newTable}, view, false, h);
        //h.firstPage(cmp);
        cmp.set('v.isBusy', false);
    },
    isTablet: function() {
        return document.querySelectorAll('.container.tablet').length > 0;
        //return $A.getRoot().elements.filter(x=> { return (x.classList ? x.classList.contains('container') && x.classList.contains('tablet'): false) }).length > 0;
    },
    isPhone: function() {
        return document.querySelectorAll('.container.phone').length > 0;
        //return $A.getRoot().elements.filter(x=> { return (x.classList ? x.classList.contains('container') && x.classList.contains('phone'): false) }).length > 0;
    },
    isMobile: function() {
        return this.isTablet() || this.isPhone();
    },
    setSalesChartData: function(cmp, tableData){
        var self = this;
        self.setSalesByPG(cmp, tableData);
        var datasets = [];
        var colors = [];
        
        var el = cmp.find('barChart').getElement();
        var ctx = el.getContext('2d');
        var randomColor = '';
        cmp.get('v.chartData').forEach(function(value, key){
            randomColor = '#' + Math.floor(Math.random()*16777215).toString(16);
            datasets.push(value);
            colors.push(randomColor);
        });
        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: cmp.get('v.productGroupsLabels'),
                datasets: [{
                    label: 'Sales',
                    data: datasets,
                    backgroundColor: colors
                }]
            },
            options: {
                events: ['click'],
                scales: {
                    yAxes: [{
                        ticks: {
                            callback: function(value, index, values) {
                            return value.toLocaleString("en-US",{style:"currency", currency:"USD"});
                            }
                        }
                    }]
                }
            }
        });
    },
    setSalesByPG: function(cmp, tableData){
        var productGroups = [];
        var chartData = new Map();
        tableData.forEach(td => {
                var productGroup = td.productGroup || td.Product_Group__c;
                var sales = td.sales;
                if (productGroup && sales && sales > 0){
                    if (chartData.get(productGroup)){
                        var previousValue = chartData.get(productGroup);
                        var newValue = previousValue + sales;
                        chartData.set(productGroup, newValue);
                    }else{
                        chartData.set(productGroup, sales);
                        productGroups.push(productGroup);
                    }
                }
            }
        );
        cmp.set('v.productGroupsLabels', productGroups);
        cmp.set('v.chartData', chartData);
    }
})