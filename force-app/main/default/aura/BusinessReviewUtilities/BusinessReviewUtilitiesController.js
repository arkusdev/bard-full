({
    getCalculationMethodParams: function (cmp, event, h) {
        var p = event.getParam('arguments').params;
        return h.getCalculationMethodParams(p.cmp, p.view, p.isLoop, p.userId);
    },
    // previously named 'consolidateLoopsByUser'
    sumSales: function(cmp, event, h) {
        var p = event.getParam('arguments').params;
        return h.sumSales(p.prevData, p.newData);
    },
    sumSalesOpp: function(cmp, event, h) {
        var p = event.getParam('arguments').params;
        return h.sumSalesOpp(p.prevData, p.newData);
    },
    // previously named 'completeLoopCalc'
    setBaselineAndGap: function(cmp, event, h) {
        var p = event.getParam('arguments').params;
        return h.setBaselineAndGap(p.data, p.cmp, p.setOriginal);
    },
    // previously named 'prepareTableData'
    addTotalsAndTrends: function(cmp, event, h) {
        var p = event.getParam('arguments').params;
        return h.addTotalsAndTrends(p.data, p.view);
    },
    setFiltersFromTableData: function(cmp, event, h) {
        var p = event.getParam('arguments').params;
        h.setFiltersFromTableData(p.cmp, p.data.tableData, p.view, p.data.oppsList, p.data.catList);
    },
    unifyValues: function(cmp, event, h) {
        var p = event.getParam('arguments').params;
        return h.unifyValues(p.data, p.setOriginal);
    },
    finalAssignment: function(cmp, event, h) {
        var p = event.getParam('arguments').params;
        h.finalAssignment(p.cmp, p.data, p.h, p.view);
    },
    modifyValuesPostFilter: function(cmp, event, h) {
        var p = event.getParam('arguments').params;
        h.modifyValuesPostFilter(p.cmp, p.data, p.view, p.setOriginal, p.h);
    },
    sort: function (cmp, event, h) {
        var p = event.getParam('arguments').params;
        h.sort(p.cmp, p.dsSort, p.firstData);
    },
    sortOpps: function (cmp, event, h) {
        var p = event.getParam('arguments').params;
        h.sortOpps(p.cmp, p.dsSort, p.firstData);
    },
    setToDateColumnName: function(cmp, event, h) {
        var p = event.getParam('arguments').params;
        h.setToDateColumnName(p.cmp);
    },
    nextPage: function(cmp, event, h) {
        var p = event.getParam('arguments').params;
        h.nextPage(p.cmp);
    },
    prevPage: function(cmp, event, h) {
        var p = event.getParam('arguments').params;
        h.prevPage(p.cmp);
    },
    firstPage: function(cmp, event, h) {
        var p = event.getParam('arguments').params;
        h.firstPage(p.cmp);
    },
    lastPage: function(cmp, event, h) {
        var p = event.getParam('arguments').params;
        h.lastPage(p.cmp);
    },
    checkPaginationButtons: function (cmp, event, h) {
        var p = event.getParam('arguments').params;
        h.checkPaginationButtons(p.cmp, p.start, p.len);
    },
    filter: function(cmp, event, h) {
        var p = event.getParam('arguments').params;
        h.filter(p.cmp, p.h, p.view);
    },
    isMobile: function(cmp, evt, h) {
        return h.isMobile();
    },
    setSalesChartData: function(cmp, evt, h) {
        var p = evt.getParam('arguments').params;
        return h.setSalesChartData(p.cmp, p.tableData);
    }
})