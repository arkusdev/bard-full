({
	activateSwitch: function(cmp) {
        var switchValue = !cmp.find('switchValue').get('v.checked');
        cmp.find('switchValue').set('v.checked', switchValue);

		var event = $A.get('e.c:BR_SwitchView');
        event.setParams({active:switchValue});
        event.fire();
	}
})