({
    onloadData : function(component,event,helper) {
        component.set("v.isDM",false);
        component.set("v.isRM",false);
        var action = component.get("c.checkProfiles");
        action.setParams({"KOLId":component.get("v.recordId")});
        action.setCallback(this, function(response){
            if(response.getState()=="SUCCESS"){
                var result = response.getReturnValue();
                console.log('YESSSS');
                console.log(result);
                component.set("v.kolInfo",result.kolInfo);
                component.set("v.hasKOLAttachment",result.hasKOLFile);
                var DMLabel = $A.get("$Label.c.DM_KOL_Roles");
                var RMLabel = $A.get("$Label.c.RM_KOL_Roles");
                if((!$A.util.isEmpty(result.DMRMName) && DMLabel.includes(result.DMRMName)) 
                   && result.kolInfo.NominationStage__c == 'DM Nomination'){
                    component.set("v.isDM",true);
                    if(result.kolInfo.Send_Approval_to_RM__c == 'Yes'){
                        
                        component.find('DMButton').set("v.disabled",true);
                    }
                }
                else if((!$A.util.isEmpty(result.DMRMName) && RMLabel.includes(result.DMRMName)) && result.kolInfo.NominationStage__c == 'RM Review'){                    
                    component.set("v.isRM",true);
                    if(result.kolInfo.Send_Approval_to_RM__c == 'No'){
                        component.find('RMReject').set("v.disabled",true);
                    }
                    
                    if(result.kolInfo.Send_Approval_to_RM_Manager__c == 'Yes'){                        
                        component.find('RMButton').set("v.disabled",true);
                        component.find('RMReject').set("v.disabled",true);
                        component.find('FinalReject').set("v.disabled",true);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    changeStatus : function(component,event,statusMessage,KOLType) {
        var toastEvent = $A.get("e.force:showToast");
        var kolDet = component.get("v.kolInfo");
        console.log('****'+kolDet);
        if(!component.get("v.hasKOLAttachment") && KOLType != 'Rejected'){
            toastEvent.setParams({
                "title": "Error!",
                "message": "Please ensure you have uploaded the nominee CV to the nomination form.",
                "duration":10,
                "type":'error'
            });
            toastEvent.fire();
        }
        else if(KOLType == 'BacktoDM' && $A.util.isEmpty(kolDet.RM_Comments__c)){
            
            /*toastEvent.setParams({
                "title": "Error!",
                "message": "Please enter comments on RM Comments field.",
                "duration":10,
                "type":'error'
            });
            toastEvent.fire();*/
            $A.util.removeClass(component.find("rmcomments"), 'slds-hide');
            $A.util.addClass(component.find("rmcomments"), 'slds-show');
        }
            else{
                if(KOLType == 'BacktoDM'){
                    component.set("v.kolInfo.NominationStage__c",'DM Nomination');
                    component.set("v.kolInfo.Send_Approval_to_RM__c",'No');
                    component.set("v.kolInfo.Reviewed_Rejected_By__c", $A.get("$SObjectType.CurrentUser.Id"));
                }else if(KOLType == 'VPReview'){
                    component.set("v.kolInfo.Send_Approval_to_RM_Manager__c",'Yes');
                    component.set("v.kolInfo.NominationStage__c",'Final Approval');
                }else if(KOLType == 'Rejected'){
                    component.set("v.kolInfo.NominationStage__c",'Rejected');
                    component.set("v.kolInfo.Reviewed_Rejected_By__c", $A.get("$SObjectType.CurrentUser.Id"));
                }else{
                    component.set("v.kolInfo.Send_Approval_to_RM__c",'Yes');
                    component.set("v.kolInfo.RM_Comments__c",'');
                    component.set("v.kolInfo.NominationStage__c",'RM Review');
                }
                
                var action = component.get("c.changeKOLStatus");
                action.setParams({"kolInfo":component.get("v.kolInfo")});
                action.setCallback(this, function(response){
                    if(response.getState()=="SUCCESS"){
                        var result = response.getReturnValue();
                        if(result.includes('Fail')){
                            var finalError = '';
                            if(result.includes('Please enter values for all fields') || result.includes('A required field is empty')){
                                finalError = 'Please review this record and ensure all fields are completed before resubmitting.';
                            }
                            else if(result.includes('You cannot modify the records once it is sent to approval')){
                                finalError = 'You cannot modify the records once it is sent to approval';
                            }
                            toastEvent.setParams({
                                "title": "Error!",
                                "message": finalError,
                                "duration":10,
                                "type":'error'
                            });
                        }
                        else{
                            console.log('***'+result);
                            $A.get('e.force:refreshView').fire();
                            toastEvent.setParams({
                                "title": "Success!",
                                "message": statusMessage,
                                "duration":10,
                                "type":'success'
                            });
                        }
                        toastEvent.fire();
                    }
                });
                $A.enqueueAction(action);
                
            }
    },
    
    updateRM_Comments : function(component,event) {
        var validComments = component.find("rmComments").checkValidity();
        if(!validComments){
            component.find("rmComments").showHelpMessageIfInvalid();
        }else{
            component.set("v.kolInfo.NominationStage__c",'DM Nomination');
            component.set("v.kolInfo.Send_Approval_to_RM__c",'No');
            component.set("v.kolInfo.Reviewed_Rejected_By__c", $A.get("$SObjectType.CurrentUser.Id"));
            var action = component.get("c.updateRMCommentsApex");
            action.setParams({"kolInfo":component.get("v.kolInfo")});
            var toastEvent = $A.get("e.force:showToast");
            action.setCallback(this, function(response){
                if(response.getState()=="SUCCESS"){
                    var result = response.getReturnValue();
                    $A.util.removeClass(component.find("rmcomments"), 'slds-show');
                    $A.util.addClass(component.find("rmcomments"), 'slds-hide');
                    console.log('***'+result);
                    $A.get('e.force:refreshView').fire();
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": 'Sent back to DM',
                        "duration":10,
                        "type":'success'
                    });
                    toastEvent.fire();
                }
            });
            $A.enqueueAction(action);
        }
        
    }
})