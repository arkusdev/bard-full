({
    doInit : function(component, event, helper) {
        helper.onloadData(component,event,helper);
    },
    RMStatus : function(component, event, helper) {
        helper.changeStatus(component,event,'KOL Nomination sent for Final Approval.','VPReview');
    },
    backtoDM : function(component, event, helper) {       
        helper.changeStatus(component,event,'KOL Nomination sent back to DM Nomination.','BacktoDM');
    },
    RejectStatus : function(component, event, helper) {
        helper.changeStatus(component,event,'KOL Nomination has been marked as rejected.','Rejected');
    },
    DMStatus : function(component, event, helper) {
        helper.changeStatus(component,event,'KOL Nomination sent for RM Approval.','RM Review');
    },
    updateRMComments : function(component, event, helper) {        
        helper.updateRM_Comments(component,event);
    }
})