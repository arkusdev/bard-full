({
	loadTab: function (cmp, event) {
		var selectedTab = cmp.get("v.selectedTab");
		var id = selectedTab;

		//If the event.target is null or undefined then this script is being fired 
		//from the init event so let's just keep the id to the defaulted selectedTab (proctor)
		if (event.target !== null && event.target !== undefined) {
        	//remove the '-tab__item' from the target id
        	id = event.target.id.substring(0, event.target.id.indexOf('-'));
            
			if (id === selectedTab) { return; }  //Get out if the user just clicks the same tab they're already on
		} 

		var tabMap = cmp.get("v.tabMap");

		for(var item in tabMap) { 
			var tab = cmp.find(item + '-tab');
			var div = cmp.find(item + '-div');   
			
			if(item === id) {
				$A.util.addClass(tab, 'slds-is-active'); 
                
        		$A.util.addClass(div, 'slds-show');        
        		$A.util.removeClass(div, 'slds-hide'); 
                                
				cmp.set("v.selectedTab", item);

				var comp = cmp.find(item + '-comp');
				comp.loadComponent(item, tabMap[item]);
			} else {
				$A.util.removeClass(tab, 'slds-is-active');
                
				$A.util.removeClass(div, 'slds-show');
        		$A.util.addClass(div, 'slds-hide');                
			}			      			
		}
	}
})