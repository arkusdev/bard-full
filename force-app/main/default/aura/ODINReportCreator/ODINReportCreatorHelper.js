({
    showMissingReportToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "No Report Selected",
            "message": "Please select a Report.",
            "type":"warning"
        });
        toastEvent.fire();
    }
})