({
    doInit : function(component, event, helper) {
    var action = component.get("c.GetReportList");
        
        action.setCallback(this, function(response) {
            
            var options = [{ "value": "", "label": "--None--" }]; 

            response.getReturnValue().forEach(element => {
                
                options.push({ "value": element.Id, "label": element.Name });

            });
            component.set("v.reports", options);

        });
        $A.enqueueAction(action); 
    },

    handleSelectExcel : function(component, event, helper) {
        if (component.get("v.selectedReport") && component.get("v.selectedReport") != null && component.get("v.selectedReport") != '') {
            var url = '/apex/ODINReportCreatorExcel?projectId=' + component.get("v.recordId") + '&reportId=' + component.get("v.selectedReport");
            window.open(url,'_blank');
        } else {
            helper.showMissingReportToast(component, event, helper);
        }
    }
    
})