({
	onInit: function(cmp, event, helper) { 
		var action = cmp.get("c.initListVariables");

		action.setParams({
			"campaignId": cmp.get("v.recordId")
		});

		action.setCallback(this, function(response) {
			var state = response.getState();

			if (cmp.isValid() && state == "SUCCESS") {
				var listVariableMap = new Map();
				listVariableMap = response.getReturnValue();

				for(var attribute in listVariableMap) {
					var values = [];

					for(var key in listVariableMap[attribute]) {
						values.push({
							key: key,
							value: listVariableMap[attribute][key]
						});
					}

					cmp.set('v.' + attribute, values);
				}				
			} else if (state == "ERROR") {
				var errors = action.getError();

				if (errors[0] && errors[0].message) {                         
					console.error(errors[0].message);
					this.showToast('Uh Oh!', 'dismissible', 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.', 'error');
				}
			}
		});

		$A.enqueueAction(action);	
	},

	save: function(cmp, event, helper) {
		var fields = [{
			id: 'name',
			type: 'text'
		}, {
			id: 'technique',
			type: 'text'				
		}, {
			id: 'subLedger',
			type: 'text'			
		}];

		var validFlg = _apostletech.isValid(cmp, fields);

		var selectedRecord = cmp.get("v.selectedRecord");

		if (selectedRecord.Id == null || selectedRecord.Id == undefined || selectedRecord.Id == '') {
			validFlg = false;
		}

		if (validFlg === true) {
			var dto = {
				campaignName: cmp.find("name").get("v.value"),
				facilityId: selectedRecord.Id,
				technique: cmp.find("technique").get("v.value"),
				subLedger: cmp.find("subLedger").get("v.value")
			};

			var action = cmp.get("c.cloneCampaign");

			action.setParams({
				"campaignId": cmp.get("v.recordId"),
				"dto": JSON.stringify(dto)
			});

			action.setCallback(this, function(response) {
				var state = response.getState();

				if (cmp.isValid() && state == "SUCCESS") {
					var campaignId = response.getReturnValue();

					$A.get("e.force:closeQuickAction").fire();

					var toast = $A.get("e.force:showToast");
                
					toast.setParams({
						title: 'Successfully Saved!',
						mode: 'dismissible',
						message: 'New campaign ' + dto.campaignName + ' successfully created with credential documents!',
						type: 'success'
					});
                
					toast.fire();				

					var nav = $A.get("e.force:navigateToSObject");

					nav.setParams({
					  "recordId": campaignId
					});

					nav.fire();

				} else if (state == "ERROR") {
					var errors = action.getError();

					if (errors[0] && errors[0].message) {                         
						console.error(errors[0].message);
						this.showToast('Uh Oh!', 'dismissible', 'An unexpected error has occuring while processing this request.  Please try again and contact your Salesforce administrator if it continues.', 'error');
					}
				}
			});

			$A.enqueueAction(action);			
		}
	},

	cancel: function(cmp, event, helper) {
		$A.get("e.force:closeQuickAction").fire();
	}
})