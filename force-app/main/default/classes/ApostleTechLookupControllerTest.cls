@isTest
public class ApostleTechLookupControllerTest {
	@testSetup
	static void createResources() {
		User u = [
			SELECT Id, Name, Phone, Email, Title, IsActive
			FROM User 
			WHERE Email = 'bd@apostletech.com'
		][0]; 

		System.runAs(u) {
			List<Account> accts = TestDataFactory.createAccounts(1);

			List<Contact> contacts = TestDataFactory.createContacts(accts, 15);
		}
	}

	@isTest
	static void fetchLookUpValues_UnitTest() {
        List<Contact> contacts = [SELECT LastName FROM Contact WHERE Name LIKE 'FirstName%'];
        List<String> cIDs = new List<String>();
		for(Contact c :contacts) {
			cIDs.add(c.Id);
		}
        Test.setFixedSearchResults(cIDs);
        List<sObject> objs = ApostleTechLookupController.fetchLookUpValues('FirstName', 'Contact');
		System.assert(!objs.isEmpty());
	}

	@isTest
	static void fetchLookUpValues_Overload_UnitTest() {
        List<Account> accs = [SELECT Id FROM Account WHERE Name LIKE 'Test Account%' LIMIT 1];
        List<Contact> omittedContacts = TestDataFactory.createContacts(accs, 1);
		/*List<Contact> contacts = [
			SELECT Id
			FROM Contact
			ORDER BY Id
			LIMIT 5
		];*/

		List<String> omit = new List<String>();
		for(Contact c :omittedContacts) {
			omit.add(c.Id);
		}

        List<Contact> contacts = [SELECT LastName FROM Contact WHERE Name LIKE 'FirstName%'];
        List<String> cIDs = new List<String>();
		for(Contact c :contacts) {
			cIDs.add(c.Id);
		}
        Test.setFixedSearchResults(cIDs);
		List<sObject> objs = ApostleTechLookupController.fetchLookUpValues('FirstName', 'Contact', omit);

		System.assert(!objs.isEmpty());	
	}
}