public class CampaignFlightController {
	@auraEnabled
	public static List<CampaignFlightDTO> fetchCampaignFlightsByType(id campaignId, string type) {
		List<CampaignFlightDTO> dtos = new List<CampaignFlightDTO>();

		if (UserUtil.canAccessFlightExpenses()) { 
			List<Campaign_Flight__c> flights = [
				SELECT Id, Campaign__c, Type__c, Name, Cost__c, Contact__c, Contact__r.Name, User__c, User__r.Name, 
					(SELECT Id, Name, Type__c, Terminal__c, Gate__c, Airline__c, Confirmation_Number__c, Flight_Number__c, 
							Origin__c, Destination__c, Depart_Date_Time__c, Arrive_Date_Time__c, 
							Notes__c
					 FROM Campaign_Flight_Legs__r)
				FROM Campaign_Flight__c
				WHERE Campaign__c = :campaignId AND Type__c = :type
			];

			for(Campaign_Flight__c cf :flights) {
				dtos.add(new CampaignFlightDTO(cf));
			}		
		}

		return dtos;
	}

	@auraEnabled
	public static CampaignFlightDTO fetchCampaignFlight(id flightId) {
		try {

			if (!UserUtil.canAccessFlightExpenses()) { return null; }

			Campaign_Flight__c flight = [
				SELECT Id, Campaign__c, Type__c, Name, Cost__c, Contact__c, Contact__r.Name, User__c, User__r.Name, 
					(SELECT Id, Name, Type__c, Terminal__c, Gate__c, Airline__c, Confirmation_Number__c, Flight_Number__c, 
							Origin__c, Destination__c, Depart_Date_Time__c, Arrive_Date_Time__c, 
							Notes__c
					 FROM Campaign_Flight_Legs__r)
				FROM Campaign_Flight__c
				WHERE Id = :flightId
			][0];

			Map<Id, string> availPassengers = fetchAvailablePassengers(flight);

			CampaignFlightDTO dto = new CampaignFlightDTO(flight, availPassengers);

			return dto;		

		} catch(QueryException ex) {
			return null;
		}
	}

	@auraEnabled
	public static CampaignFlightDTO createNewCampaignFlight(id campaignId, string type) {
		if (!UserUtil.canUpsertFlightExpenses()) { return null; }

		Campaign_Flight__c flight = new Campaign_Flight__c(
			Campaign__c = campaignId,
			Type__c = type
		);

		Map<Id, string> availPassengers = fetchAvailablePassengers(flight);

		CampaignFlightDTO dto = new CampaignFlightDTO(flight, availPassengers);

		return dto;
	}

	private static Map<Id, string> fetchAvailablePassengers(Campaign_Flight__c flight) { 
		Map<Id, string> passengers = new Map<Id, string>();

		if(flight.Type__c.equalsIgnoreCase('Attendee')) {
			List<CampaignMember> cms = [
				SELECT ContactId, Contact.Name 
				FROM CampaignMember 
				WHERE CampaignId = :flight.Campaign__c
				AND Type__c != 'Waitlist'			
			];

			for(CampaignMember cm :cms) {
				passengers.put(cm.ContactId, cm.Contact.Name);
			}
		} else {
			List<Campaign_User__c> cus = [
				SELECT User__c, User__r.Name 
				FROM Campaign_User__c
				WHERE Campaign__c = :flight.Campaign__c	
			];

			for(Campaign_User__c cu :cus) {
				passengers.put(cu.User__c, cu.User__r.Name);
			}		
		}

		return passengers;
	}

	@auraEnabled
	public static void deleteCampaignFlight(id flightId) {
		if (UserUtil.canDeleteFlightExpenses()) { 
			Campaign_Flight__c cf = new Campaign_Flight__c(
				Id = flightId
			);

			delete cf;		
		}
	}

	@auraEnabled
	public static void saveCampaignFlight(string dto) {
		if (UserUtil.canUpsertFlightExpenses()) { 
			CampaignFlightDTO flight = ((CampaignFlightDTO)System.JSON.deserializeStrict(dto, CampaignFlightDTO.Class));

			Campaign_Flight__c cf = new Campaign_Flight__c(
				Id = flight.flightId,
				Campaign__c = flight.campaignId,
				Name = flight.attendeeName,
				Type__c = flight.type,
				Cost__c = flight.cost,		
				Contact__c = (flight.type.equalsIgnoreCase('Attendee') ? flight.attendeeId : null),
				User__c = (flight.type.equalsIgnoreCase('Attendee') ? null : flight.attendeeId)
			);

			upsert cf;

			saveCampaignFlightLegs(flight, cf);		
		}
	}

	private static void saveCampaignFlightLegs(CampaignFlightDTO flight, Campaign_Flight__c cf) {
		List<Campaign_Flight_Leg__c> legs = new List<Campaign_Flight_Leg__c>();

		for(CampaignFlightLegDTO fl :flight.flightLegs) {
			legs.add(new Campaign_Flight_Leg__c(
				Id = fl.flightLegId,
				Campaign_Flight__c = cf.Id,
				Name = fl.name,
				Origin__c = fl.origin,
				Destination__c = fl.destination,
				Depart_Date_Time__c = fl.departDateTime,
				Arrive_Date_Time__c = fl.arriveDateTime,
				Airline__c = fl.airline,
				Confirmation_Number__c = fl.confirmationNumber,
				Flight_Number__c = fl.flightNumber,
				Gate__c = fl.gate,
				Terminal__c = fl.terminal,
				Type__c = fl.type,
				Notes__c = fl.notes
			));	
		}

		upsert legs;
	}
}