public class CampaignMealController  {
	@auraEnabled
	public static List<CampaignMealDTO> fetchCampaignMealsByType(id campaignId, string type) {
		List<CampaignMealDTO> dtos = new List<CampaignMealDTO>();

		if (UserUtil.canAccessMealExpenses()) { 
			List<Campaign_Meal__c> meals = [
				SELECT Id, Campaign__c, Name, Type__c, Location_Name__c, Meal_Date__c, Total_Cost__c, 
				Additional_Attendees__c, Attendee_Count__c, Total_Attendees__c, Cost_Per_Attendee__c,
				Street_Address__c, City__c, State__c, Postal_Code__c, Country__c, Notes__c,
					(SELECT Contact__c, Contact__r.Name, User__c, User__r.Name FROM Campaign_Meal_Attendees__r)
				FROM Campaign_Meal__c
				WHERE Campaign__c = :campaignId AND Type__c = :type
			];

			for(Campaign_Meal__c cm :meals) {
				dtos.add(new CampaignMealDTO(cm));
			}		
		}

		return dtos;
	}

	@auraEnabled
	public static CampaignMealDTO fetchCampaignMeal(id mealId) {
		try {

			if (!UserUtil.canAccessMealExpenses()) { return null; }

			Campaign_Meal__c meal = [
				SELECT Id, Campaign__c, Name, Type__c, Location_Name__c, Meal_Date__c, Total_Cost__c, 
				Additional_Attendees__c, Attendee_Count__c, Total_Attendees__c, Cost_Per_Attendee__c,
				Street_Address__c, City__c, State__c, Postal_Code__c, Country__c, Notes__c,
					(SELECT Contact__c, Contact__r.Name, User__c, User__r.Name FROM Campaign_Meal_Attendees__r)
				FROM Campaign_Meal__c
				WHERE Id = :mealId
			][0];

			List<CampaignMember> availMembers = fetchCampaignMembers(meal.Campaign__c);
			List<Campaign_User__c> availUsers = fetchCampaignUsers(meal.Campaign__c);
			
			CampaignMealDTO dto = new CampaignMealDTO(meal, availMembers, availUsers);

			return dto;		

		} catch(QueryException ex) {
			return null;		
		}
	}

	@auraEnabled
	public static CampaignMealDTO createNewCampaignMeal(id campaignId, string type) {
		if (!UserUtil.canUpsertMealExpenses()) { return null; }

		Campaign_Meal__c meal = new Campaign_Meal__c(
			Campaign__c = campaignId,
			Type__c = type
		);

		List<CampaignMember> availMembers = fetchCampaignMembers(campaignId);
		List<Campaign_User__c> availUsers = fetchCampaignUsers(campaignId);

		CampaignMealDTO dto = new CampaignMealDTO(meal, availMembers, availUsers);

		return dto;
	}

	private static List<CampaignMember> fetchCampaignMembers(id campaignId) { 
		return [
			SELECT ContactId, Contact.Name 
			FROM CampaignMember 
			WHERE CampaignId = :campaignId
			AND Type__c != 'Waitlist'
		];	
	}

	private static List<Campaign_User__c> fetchCampaignUsers(id campaignId) { 
		return [
			SELECT User__c, User__r.Name 
			FROM Campaign_User__c
			WHERE Campaign__c = :campaignId
		];
	}

	@auraEnabled
	public static void deleteCampaignMeal(id mealId) {
		Campaign_Meal__c cm = new Campaign_Meal__c(
			Id = mealId
		);

		delete cm;
	}

	@auraEnabled
	public static void saveCampaignMeal(string dto) {
		if (UserUtil.canUpsertMealExpenses()) { 
			CampaignMealDTO meal = ((CampaignMealDTO)System.JSON.deserializeStrict(dto, CampaignMealDTO.Class));

			Campaign_Meal__c cm = new Campaign_Meal__c(
				Id = meal.mealId,
				Name = meal.type + ' - ' + meal.mealDate.format(),
				Campaign__c = meal.campaignId,
				Type__c = meal.type,
				Street_Address__c = meal.streetAddress,
				City__c = meal.city, 
				State__c = meal.state,
				Postal_Code__c = meal.postalCode,
				Country__c = meal.country,
				Notes__c = meal.notes,
				Additional_Attendees__c = (meal.additionalAttendees != null ? meal.additionalAttendees : 0),
				Location_Name__c = meal.locationName,
				Meal_Date__c = meal.mealDate,
				Total_Cost__c = meal.totalCost
			);

			upsert cm;

			saveCampaignMealMembers(meal, cm);
			saveCampaignMealUsers(meal, cm);		
		}
	}

	private static void saveCampaignMealMembers(CampaignMealDTO meal, Campaign_Meal__c cm) {
		if (!meal.selectedMembers.isEmpty()) {
			Set<Id> selected = new Set<Id>();
			Set<Id> existing = new Set<Id>();

			for(CampaignMealAttendeeDTO cma :meal.selectedMembers) {
				selected.add(cma.value);
			}

			//First let's remove any members that were removed
			List<Campaign_Meal_Attendee__c> deleteMembers = [
				SELECT Id 
				FROM Campaign_Meal_Attendee__c
				WHERE Campaign_Meal__c = :cm.Id 
				AND Contact__c NOT IN :selected
				AND User__c = NULL
			];

			if (!deleteMembers.isEmpty()) {
				delete deleteMembers;
			}

			List<Campaign_Meal_Attendee__c> existingMembers = [
				SELECT Id, Contact__c 
				FROM Campaign_Meal_Attendee__c
				WHERE Campaign_Meal__c = :cm.Id 
				AND Contact__c IN :selected
			];

			List<Campaign_Meal_Attendee__c> newMembers = new List<Campaign_Meal_Attendee__c>();

			if (!existingMembers.isEmpty()) {
				for(Campaign_Meal_Attendee__c em :existingMembers) {
					if (!existing.contains(em.Contact__c)) { 
						existing.add(em.Contact__c); 
					}
				}

				for(CampaignMealAttendeeDTO cma :meal.selectedMembers) {
					if (!existing.contains(cma.value)) {
						newMembers.add(new Campaign_Meal_Attendee__c(
							Campaign_Meal__c = cm.Id,
							Name = cma.value + ': ' + cm.Name,
							Contact__c = cma.value
						));					
					}
				}
			} else {
				for(CampaignMealAttendeeDTO cma :meal.selectedMembers) {
					newMembers.add(new Campaign_Meal_Attendee__c(
						Campaign_Meal__c = cm.Id,
						Name = cma.value + ': ' + cm.Name,
						Contact__c = cma.value
					));
				}				
			}

			if (!newMembers.isEmpty()) {
				insert newMembers;
			}
		} else {
			List<Campaign_Meal_Attendee__c> deleteMembers = [
				SELECT Id 
				FROM Campaign_Meal_Attendee__c
				WHERE Campaign_Meal__c = :cm.Id 
				AND Contact__c != NULL
				AND User__c = NULL
			];

			if (!deleteMembers.isEmpty()) {
				delete deleteMembers;
			}
		}
	}

	private static void saveCampaignMealUsers(CampaignMealDTO meal, Campaign_Meal__c cm) {
		if (!meal.selectedUsers.isEmpty()) {
			Set<Id> selected = new Set<Id>();
			Set<Id> existing = new Set<Id>();

			for(CampaignMealAttendeeDTO cma :meal.selectedUsers) {
				selected.add(cma.value);
			}

			//First let's remove any members that were removed
			List<Campaign_Meal_Attendee__c> deleteUsers = [
				SELECT Id 
				FROM Campaign_Meal_Attendee__c
				WHERE Campaign_Meal__c = :cm.Id 
				AND User__c NOT IN :selected
				AND Contact__c = NULL
			];

			if (!deleteUsers.isEmpty()) {
				delete deleteUsers;
			}

			List<Campaign_Meal_Attendee__c> existingMembers = [
				SELECT Id, User__c 
				FROM Campaign_Meal_Attendee__c
				WHERE Campaign_Meal__c = :cm.Id 
				AND User__c IN :selected
			];

			List<Campaign_Meal_Attendee__c> newMembers = new List<Campaign_Meal_Attendee__c>();

			if (!existingMembers.isEmpty()) {
				for(Campaign_Meal_Attendee__c em :existingMembers) {
					if (!existing.contains(em.User__c)) { 
						existing.add(em.User__c); 
					}
				}

				for(CampaignMealAttendeeDTO cma :meal.selectedUsers) {
					if (!existing.contains(cma.value)) {
						newMembers.add(new Campaign_Meal_Attendee__c(
							Campaign_Meal__c = cm.Id,
							Name = cma.value + ': ' + cm.Name,
							User__c = cma.value
						));					
					}
				}
			} else {
				for(CampaignMealAttendeeDTO cma :meal.selectedUsers) {
					newMembers.add(new Campaign_Meal_Attendee__c(
						Campaign_Meal__c = cm.Id,
						Name = cma.value + ': ' + cm.Name,
						User__c = cma.value
					));
				}				
			}

			if (!newMembers.isEmpty()) {
				insert newMembers;
			}
		} else {
			List<Campaign_Meal_Attendee__c> deleteUsers = [
				SELECT Id 
				FROM Campaign_Meal_Attendee__c
				WHERE Campaign_Meal__c = :cm.Id 
				AND User__c != NULL
				AND Contact__c = NULL
			];

			if (!deleteUsers.isEmpty()) {
				delete deleteUsers;
			}
		}
	}

	@auraEnabled
	public static Map<string, string> fetchAllStateOptions() {
		return PicklistUtil.fetchAllStateOptions();
	}
}