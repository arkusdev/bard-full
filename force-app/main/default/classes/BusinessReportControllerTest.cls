@isTest
public class BusinessReportControllerTest {
    @testSetup
    public static void setup () {
        // create user
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias='brdst',
                          Email='brdst@testorg.com', 
                          EmailEncodingKey='UTF-8',
                          LastName='Testing',
                          LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US',
                          ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName='brdst@testorg.com');
        insert u;
        System.runAs(u) {
            Account acc = new Account(Name='TestAccount');
            insert acc;
            
            RecordType opprt = [SELECT Id FROM RecordType WHERE SObjectType = 'Opportunity' LIMIT 1][0];

            List<Opportunity> opps = new List<Opportunity>();
            // create 2 opps as closed won and within last 12 months
            Opportunity opp1 = new Opportunity();
            opp1.Name = 'BRD_TestOpp1';
            opp1.StageName = 'Closed Won';
            opp1.CloseDate = Date.today();
            opp1.PO_Number__c = '0001';
            opps.add(opp1);
            Opportunity opp2 = new Opportunity();
            opp2.Name = 'BRD_TestOpp2';
            opp2.StageName = 'Closed Won';
            opp2.CloseDate = Date.today();
            opp2.PO_Number__c = '0002';
            opps.add(opp2);
            // create 1 opp that's not closed
            Opportunity opp3 = new Opportunity();
            opp3.Name = 'BRD_TestOpp3';
            opp3.StageName = 'Prospecting';
            opp3.CloseDate = Date.today();
            opps.add(opp3);
            // create 1 opp closed won before last 12 months
            Opportunity opp4 = new Opportunity();
            opp4.Name = 'BRD_TestOpp4';
            opp4.StageName = 'Closed Won';
            opp4.CloseDate = Date.today().addMonths(-14);
            opp4.PO_Number__c = '0004';
            opps.add(opp4);

            for (Opportunity o : opps) {
                o.AccountId = acc.Id;
                o.RecordTypeId = opprt.Id;
            }
            insert opps;

            RecordType rtIrsa = [SELECT Id FROM RecordType WHERE SObjectType = 'Sales__c' AND Name = 'IRSA'];
            RecordType rtShad = [SELECT Id FROM RecordType WHERE SObjectType = 'Sales__c' AND Name = 'SHAD'];
            List<Sales__c> sales = new List<Sales__c>();
            // create 1 sales as IRSA for current month for $100 (invoiceDate = closeDate)
            Sales__c sale1 = new Sales__c();
            sale1.Invoice_Date__c = Date.today();
            sale1.RecordTypeId = rtIrsa.Id;
            sale1.OwnerId = u.Id;
            sale1.Category__c = 'BRD_Category_A';
            sale1.Account__c = acc.Id;
            sale1.Sales__c = 100;
            sales.add(sale1);
            // create 2 sales as SHAD for last month for $100 each (invoiceDate = closeDate)
            Sales__c sale2 = new Sales__c();
            sale2.Invoice_Date__c = Date.today().addMonths(-1);
            sale2.RecordTypeId = rtShad.Id;
            sale2.OwnerId = u.Id;
            sale2.Category__c = 'BRD_Category_A';
            sale2.Account__c = acc.Id;
            sale2.Sales__c = 200;
            sales.add(sale2);
            Sales__c sale3 = new Sales__c();
            sale3.Invoice_Date__c = Date.today().addMonths(-1);
            sale3.RecordTypeId = rtShad.Id;
            sale3.OwnerId = u.Id;
            sale3.Category__c = 'BRD_Category_B';
            sale3.Account__c = acc.Id;
            sale3.Sales__c = 300;
            sales.add(sale3);
            // create 1 sales as IRSA for last month for $100 (invoiceDate = closeDate)
            Sales__c sale4 = new Sales__c();
            sale4.Invoice_Date__c = Date.today().addMonths(-1);
            sale4.RecordTypeId = rtIrsa.Id;
            sale4.OwnerId = u.Id;
            sale4.Category__c = 'BRD_Category_A';
            sale4.Account__c = acc.Id;
            sale4.Sales__c = 400;
            sales.add(sale4);
            
            insert sales;
        }
    }

	@isTest
    public static void testOpportunities () {
        User u = getTestUser();
        List<Opportunity> results = new List<Opportunity>();
        System.runAs(u) {
            results = BusinessReportController.getOpportunities();
        }
        System.assertEquals(2, results.size());
    }

    @isTest
    public static void testSales () {
        Date closeDate = Date.today().addMonths(-3);
        User u = getTestUser();
        Account a = getTestAccount();
        BusinessReportController.SalesResult results = new BusinessReportController.SalesResult();
        System.runAs(u) {
            results = BusinessReportController.getSales(closeDate, a.Id);
        }
        System.assertEquals(2, results.months.size());
        System.assertEquals(2, results.sales.size());
        Integer valuesCount = 0;
        for (BusinessReportController.SalesWrapper sw : results.sales) {
            for (Decimal d : sw.values) {
                if (d > 0) valuesCount++;
            }
        }
        System.assertEquals(3, valuesCount);
    }

    private static User getTestUser () {
        return [SELECT Id, Name FROM User WHERE UserName = 'brdst@testorg.com'][0];
    }
    private static Account getTestAccount () {
        return [SELECT Id, Name FROM Account WHERE Name = 'TestAccount'][0];
    }
}