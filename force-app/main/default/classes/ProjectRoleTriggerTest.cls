@isTest
public with sharing class ProjectRoleTriggerTest {
    @TestSetup
    static void makeData(){
        Project__c project = new Project__c();
        project.Name = 'Test_1';
        insert project;

        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias='brdst',
                          Email='brdst@testorg.com', 
                          EmailEncodingKey='UTF-8',
                          LastName='Testing',
                          LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US',
                          ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName='brdst@testorg.com');
        insert u;
    }

    static testMethod void UpdateProjectRolesTest() {
        String productValue = 'ESKD';

        Project__c project = new Project__c();
        project.Platform_Disease_State__c = productValue;
        project.Name = 'Test_2';

        Test.startTest();
            insert project;
            Project_Role__c role = new Project_Role__c();
            role.User__c = [SELECT Id FROM User WHERE UserName = 'brdst@testorg.com'].Id;
            role.Project__c = [SELECT Id FROM Project__c WHERE Name = 'Test_2'].Id;
            role.Role__c = 'Marketing';
            insert role;
            List<Project_Role__c> roleList = [select User__c, User__r.Name, Region__c from Project_Role__c where Project__c =: project.Id AND User__c !=: UserInfo.getUserId()];
        Test.stopTest();

        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias='brdst',
                          Email='brdst@testorg.com', 
                          EmailEncodingKey='UTF-8',
                          LastName='Testing Man',
                          LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US',
                          ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName='brdst@testorg.test');
        insert u;

        roleList[0].User__c = u.Id;

        update roleList[0];

        system.assertEquals(1, [SELECT Id FROM CollaborationGroupMember WHERE MemberId =: u.Id].size());
    }

    @IsTest
    static void addNewTeamMemberTest(){
        Project_Role__c role = new Project_Role__c();
        role.User__c = [SELECT Id FROM User WHERE UserName = 'brdst@testorg.com'].Id;
        role.Project__c = [SELECT Id FROM Project__c].Id;
        Test.startTest();
            insert role;
        Test.stopTest();
        
        System.assert(![SELECT Id FROM CollaborationGroupMember].isEmpty(), 'No group member was added to the chatter group');
    }

    @IsTest
    static void deleteTeamMemberTest(){
        Project_Role__c role = new Project_Role__c();
        role.User__c = [SELECT Id FROM User WHERE UserName = 'brdst@testorg.com'].Id;
        role.Project__c = [SELECT Id FROM Project__c].Id;
        Test.startTest();
            insert role;
            delete role;
        Test.stopTest();
        
        System.assert([SELECT Id FROM CollaborationGroupMember WHERE MemberId =: role.User__c].isEmpty(), 'The group member wasnt deleted');
    }
}