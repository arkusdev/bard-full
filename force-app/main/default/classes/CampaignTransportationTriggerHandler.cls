public class CampaignTransportationTriggerHandler extends TriggerHandler {
	/* overrides */
	protected override void afterInsert() {
		SynchCampaignCost();
	}

	protected override void afterUpdate() {
		SynchCampaignCost();
	}

	protected override void afterDelete() {
		SynchCampaignCost();
	}

	/* private methods */
	private void SynchCampaignCost() {
		Set<Id> campaignIds = new Set<Id>();

		if (trigger.isInsert) {
			for(Campaign_Transportation__c item : (List<Campaign_Transportation__c>)trigger.new) {
				campaignIds.add(item.Campaign__c);
			}
		} else if (trigger.isUpdate) {
            for(Id item :((Map<Id, Campaign_Transportation__c>)trigger.newMap).keySet()) {
				Campaign_Transportation__c obj = ((Map<Id, Campaign_Transportation__c>)trigger.newMap).get(item);

                if (((Map<Id, Campaign_Transportation__c>)trigger.oldMap).get(item).Total_Cost__c != obj.Total_Cost__c) {
					campaignIds.add(obj.Campaign__c);
                }
            } 			
		} else {
			for(Campaign_Transportation__c item : (List<Campaign_Transportation__c>)trigger.old) {
				campaignIds.add(item.Campaign__c);
			}			
		}

		if(!campaignIds.isEmpty()) {
			CampaignTriggerUtil.UpdateCampaignCost(campaignIds);	
		}
	}
}