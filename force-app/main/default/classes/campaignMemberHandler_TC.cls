/*
    Class Name : campaignMemberHandler_TC
    Created by : Apostle tech dev
*/
@isTest
private class campaignMemberHandler_TC
{
    static testmethod void myUnitTest()
    {
        RecordType rt = [SELECT id,Name 
                             FROM RecordType 
                             WHERE SobjectType='campaign' AND Name='Conference'];
        campaign cm = new campaign();
        cm.name = 'test';
        cm.Technique__c = 'CTO';
        cm.SubLedger__c = 'test';
        insert cm;
        
        Account acc = new account();
        acc.name = 'test acc';
        insert acc;
        
        Contact c = new contact();
        c.lastname = 'test contact';
        c.accountid = acc.id;
        insert c;
        
        campaignmember cmm = new campaignmember();
        cmm.campaignid = cm.id;
        cmm.contactid = c.id;
        cmm.type__c = 'proctor';
        insert cmm;
        
        delete cmm;
        
    }
}