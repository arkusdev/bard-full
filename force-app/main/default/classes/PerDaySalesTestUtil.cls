@isTest
public class PerDaySalesTestUtil {
    public static void createData () {
        Profile adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        UserRole dmRole = [SELECT Id FROM UserRole WHERE Name = 'West Coast - FSV - DM' LIMIT 1];
        UserRole tmRole = [SELECT Id FROM UserRole WHERE Name = 'West Coast- FSV - TM' LIMIT 1];
        List<AggregateResult> ars = [SELECT MAX(Territory__c) max FROM User];  
        String territoryN = ars.size() > 0 ? String.valueOf(Integer.valueOf(ars[0].get('max'))+1) : '001';
        // create users
        User dm = new User(LastName = 'TestDM',
                           Alias = 'tdm',
                           Email = 'test.dm@perdaysales.com',
                           Username = 'test.dm@perdaysales.com',
                           Territory__c = '001',
                           UserRoleId = dmRole.Id
                          );
        User tm = new User(LastName = 'TestTM',
                             Alias = 'ttm',
                             Email = 'test.biopsy.csm@perdaysales.com',
                             Username = 'test.biopsy.csm@perdaysales.com',
                             Territory__c = '002',
                             UserRoleId = tmRole.Id
                            );
        List<User> users = new List<User>();
        users.add(dm);
        users.add(tm);
        for (User u : users) {
            u.ProfileId = adminProfile.id;
            u.TimeZoneSidKey = 'GMT';
            u.LanguageLocaleKey = 'en_US';
            u.EmailEncodingKey = 'UTF-8';
            u.LocaleSidKey = 'en_US';
            u.isActive = true;
            u.Territory__c = territoryN;

            Integer nextTN = Integer.valueOf(territoryN) + 1;
            if (nextTN < 10) territoryN = '00' + String.valueOf(nextTN);
            if (nextTN < 100) territoryN = '0' + String.valueOf(nextTN);
            else territoryN = String.valueOf(nextTN);
        }
        insert users;

        // create quota records, 1 for each user
        // create irsa records, matching previous quota, 1 each user
        // create 1 irsa w/o matching quota and vice versa
        Date invoiceDate = Date.today().addDays(-1);
        //String dateName = ((Datetime)invoiceDate).formatGmt('E');
        /*while (dateName.equals('Sat') || dateName.equals('Sun')) {
            invoiceDate = invoiceDate.addDays(-1);
            dateName = ((Datetime)invoiceDate).formatGmt('E');
        }*/
        Boolean isSameMonth = invoiceDate.month() == Date.today().month();
        String salesRT = isSameMonth ? 'IRSA' : 'SHAD';
        RecordType irsaRT = [SELECT Id FROM RecordType WHERE Name = :salesRT AND SObjectType = 'Sales__c' LIMIT 1];
        RecordType quotaRT = [SELECT Id FROM RecordType WHERE Name = 'Quota' AND SObjectType = 'Sales__c' LIMIT 1];

        System.runAs(dm) {
            Account a = new Account(Name='PerDaySales Test Account');
            insert a;

            List<Sales__c> dmPair = new List<Sales__c>();

            Sales__c irsaNOMatchSE = new Sales__c();
            irsaNOMatchSE.Sales__c = 100;
            irsaNOMatchSE.CAT__c = 'No Matching Quota';
            irsaNOMatchSE.OwnerId = dm.Id;
            irsaNOMatchSE.RecordTypeId = irsaRT.Id;

            Sales__c quotaNOMatchSE = new Sales__c();
            quotaNOMatchSE.CAT__c = 'No Matching IRSA';
            quotaNOMatchSE.Base__c = 80;
            quotaNOMatchSE.Quota__c = 40;
            quotaNOMatchSE.OwnerId = dm.Id;
            quotaNOMatchSE.RecordTypeId = quotaRT.Id;

            Sales__c irsaMatchSE = new Sales__c();
            irsaMatchSE.Sales__c = 100;
            irsaMatchSE.CAT__c = 'Cat1';
            irsaMatchSE.OwnerId = dm.Id;
            irsaMatchSE.RecordTypeId = irsaRT.Id;

            Sales__c quotaMatchSE = new Sales__c();
            quotaMatchSE.CAT__c = 'Cat1';
            quotaMatchSE.Base__c = 80;
            quotaMatchSE.Quota__c = 40;
            quotaMatchSE.OwnerId = dm.Id;
            quotaMatchSE.RecordTypeId = quotaRT.Id;

            Sales__c irsaMatchB = new Sales__c();
            irsaMatchB.Sales__c = 100;
            irsaMatchB.CAT__c = 'Cat2';
            irsaMatchB.OwnerId = tm.Id;
            irsaMatchB.RecordTypeId = irsaRT.Id;

            Sales__c quotaMatchB = new Sales__c();
            quotaMatchB.CAT__c = 'Cat2';
            quotaMatchB.Base__c = 80;
            quotaMatchB.Quota__c = 40;
            quotaMatchB.OwnerId = tm.Id;
            quotaMatchB.RecordTypeId = quotaRT.Id;

            dmPair.add(irsaNOMatchSE);
            dmPair.add(quotaNOMatchSE);
            dmPair.add(irsaMatchSE);
            dmPair.add(quotaMatchSE);
            dmPair.add(irsaMatchB);
            dmPair.add(quotaMatchB);
            for (Sales__c s : dmPair) {
                s.Account__c = a.Id;
                s.Invoice_Date__c = invoiceDate;
                s.Reporting_Product_Group__c = 'Test Reporting Product Group';
                s.Product_Group__c = 'Test Product Group';
            }
            insert dmPair;
        }
    }
    public static User getDMUser () {
        return [SELECT Id FROM User WHERE LastName = 'TestDM' LIMIT 1];
    }
    public static User getTMUser () {
        return [SELECT Id, UserRoleId FROM User WHERE LastName = 'TestTM' LIMIT 1];
    }
}