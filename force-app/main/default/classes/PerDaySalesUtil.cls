public with sharing class PerDaySalesUtil {
    public static BRUser getBRUser(String userId) {
        
        Map<Id, List<User>> userChilds = new Map<Id, List<User>>();
        
        for (User user : [SELECT Id, UserRole.Name, UserRole.ParentRoleId, Territory__c, District__c, Name, Alias FROM User
                          WHERE
                          (UserRole.Name LIKE '% - DM'  AND District__c != null) OR
                          (UserRole.Name LIKE '% - TM' AND Territory__c != null)
                          ORDER BY Alias, Territory__c ASC]) {
            if (userChilds.containsKey(user.UserRole.ParentRoleId)) {
                List<User> users = userChilds.get(user.UserRole.ParentRoleId);
                users.add(user);
                userChilds.put(user.UserRole.ParentRoleId, users);
            } else {
                userChilds.put(user.UserRole.ParentRoleId, new List<User> { user });
            }
        }

        userId = String.isBlank(userId) ? UserInfo.getUserId() : userId;
        User selectedUser = [SELECT Id, Territory__c, Name, UserRoleId, Alias, UserRole.Name FROM User WHERE Id =:userId][0];

        BRUser newBRUser = new BRUser(0, selectedUser, new List<BRUser>());
        setSubordinates(newBRUser, 0, userChilds);
        return newBRUser;
    }

    private static void setSubordinates(BRUser someBRUser, Integer level, Map<Id, List<User>> userChilds){
        level++;
        List<User> childs = userChilds.get(someBRUser.user.UserRoleId);
        if (childs != null){
            for (User child : childs){
                BRUser newBRUserChild = new BRUser(level, child, new List<BRUser>());
                someBRUser.subordinates.add(newBRUserChild);
                //setSubordinates(someBRUser, level + 1);
            }
        }
        level++;
        //someBRUser.subordinates[0].subordinates.add(someBRUser);
        for (BRUser brChild : someBRUser.subordinates) {
            List<User> grandsons = userChilds.get(brChild.user.UserRoleId);
            if (grandsons != null){
                for (User grandSon : grandsons) {
                    BRUser newBRUserGrandson = new BRUser(level, grandSon, new List<BRUser>());
                    brChild.subordinates.add(newBRUserGrandson);
                }
            }
        }
    }

    public static List<User> getUsers(String userId) {
        //Boolean paramEmpty = String.isBlank(userId);
        userId = String.isBlank(userId) ? UserInfo.getUserId() : userId;
        User selectedUser = [SELECT Id, Territory__c, Name, UserRoleId, Alias, UserRole.Name FROM User WHERE Id =:userId WITH SECURITY_ENFORCED][0];

        Map<String, Integer> roles = getAllSubRolesFromUser(selectedUser.UserRoleId);
        List<User> users = new List<User>{selectedUser};
        users.addAll([SELECT Id, Territory__c, Name, UserRoleId, Alias, UserRole.Name FROM User WHERE UserRoleId IN :roles.keyset() AND UserRoleId != :selectedUser.UserRoleId AND isActive = true AND Territory__c != NULL WITH SECURITY_ENFORCED ORDER BY Territory__c]);
    
        //if (!paramEmpty && users.size() > 1) { users.remove(0); }
        for (User u : users) {
            u.Alias += ' - level: ' + String.valueOf(roles.get(u.UserRoleId));
        }
        return users;
    }

    public static Map<String, Integer> getAllSubRolesFromUser(String userRoleId) {
        Integer level = 0;
        Map<String, Integer> userIDs = new Map<String, Integer>();
        userIDs.put(userRoleId, level);
        Map<String, Integer> result = getAllSubRoleIds(userIDs, level);
        userIDs.putAll(result);
        return userIDs;
    }

    public static Map<String, Integer> getAllSubRoleIds(Map<String, Integer> roleIds, Integer level) {
        level++;
        Map<String, Integer> currentRoleIds = new Map<String, Integer>();
        
        // get all of the roles underneath the passed roles
        for(UserRole userRole :[SELECT Id FROM UserRole WHERE ParentRoleId IN :roleIds.keyset() AND ParentRoleID != null]) {
            currentRoleIds.put(userRole.Id, level);
        }
        // continue on until there are no more roles
        if(currentRoleIds.size() > 0) {
            currentRoleIds.putAll(getAllSubRoleIds(currentRoleIds, level));
        }
        return currentRoleIds;
    }

    public static Set<ID> getParentRoleId(Set<ID> roleIds) {
        Set<ID> currentRoleIds = new Set<ID>();
        // get all of the parent roles.
        for(UserRole ur :[SELECT Id, ParentRoleId FROM UserRole WHERE Id IN: roleIds]) {
            currentRoleIds.add(ur.ParentRoleId);
        }
        // continue on until there's no more roles
        if(currentRoleIds.size() > 0) {
            currentRoleIds.addAll(getParentRoleId(currentRoleIds));
        }

        return currentRoleIds;
    }

    /*public static List<Date> getHolidays (Boolean lastYear) {
        String period = lastYear ? 'LAST_FISCAL_YEAR' : 'THIS_FISCAL_YEAR';
        String query = 'SELECT ActivityDate, IsRecurrence FROM Holiday WHERE IsRecurrence = TRUE or ActivityDate = '+period+' order by activitydate';
        List<Holiday> holidays = Database.query(query);//[SELECT ActivityDate FROM Holiday];

        List<Date> hDates = new List<Date>();
        for (Holiday h : holidays) {
            // create dates for past year and the year before, to match possible previous fiscal year dates.
            //Date h1 = Date.newInstance((Date.today()).year(), h.ActivityDate.month(), h.ActivityDate.day());
            //hDates.add(h1);if (h.IsRecurrence) {
            //    Date h2 = Date.newInstance((Date.today().addYears(-1)).year(), h.ActivityDate.month(), h.ActivityDate.day());
            //    Date h3 = Date.newInstance((Date.today().addYears(-2)).year(), h.ActivityDate.month(), h.ActivityDate.day());
            //    hDates.add(h2);
            //    hDates.add(h3);
            //}
            Integer year = 0;
            if (h.IsRecurrence) {
                Date normalized = Date.newInstance((Date.today()).year(), h.ActivityDate.month(), h.ActivityDate.day());
                year = normalized.year();
                if (normalized > Date.today()) {
                    year--;
                }
            } else {
                year = h.ActivityDate.year();
            }
            
            Date d = Date.newInstance(year, h.ActivityDate.month(), h.ActivityDate.day());
            hDates.add(d);
        }
        return hDates;
    }*/

    public static void fillWorkingDays (String period, PerDaySalesController.PeriodInfo info) {
        CurrentDaysWrapper cdw = fillWorkingDays(period);
        info.currentWorkingDays = cdw.currentWorkingDays;
        info.totalWorkingDays = cdw.totalWorkingDays;
        info.thisMonthCurrentDays = cdw.thisMonthCurrentDays;
        info.thisMonthTotalDays = cdw.thisMonthTotalDays;
    }
    public static void fillWorkingDays (String period, CustomerAndProductGapController.CalculationsResponse response) {
        CurrentDaysWrapper cdw = fillWorkingDays(period);
        response.currentWorkingDays = cdw.currentWorkingDays;
        response.totalWorkingDays = cdw.totalWorkingDays;
        response.thisMonthOngoingSellingDays = cdw.thisMonthCurrentDays;
        response.thisMonthTotalSellingDays = cdw.thisMonthTotalDays;
    }
    public static Integer getTotalWorkingDays(String period) {
        CurrentDaysWrapper cdw = fillWorkingDays(period);
        return cdw.totalWorkingDays;
    }
    public static CurrentDaysWrapper fillWorkingDays (String period) {
        CurrentDaysWrapper cdw = new CurrentDaysWrapper();
        String literalPeriod = getPeriodDateLiteral(period);
        if (period.equals('last_year')) period = 'year';
        //List<Date> holidays = getHolidays(false);
        List<String> notAllowed = new List<String>{'Sat','Sun'};
        Period thisPeriod = Database.query('SELECT StartDate, EndDate FROM Period WHERE Type=:period AND StartDate =' + literalPeriod + ' AND EndDate =' + literalPeriod + ' WITH SECURITY_ENFORCED');
        //Period thisPeriod = [SELECT StartDate, EndDate FROM Period WHERE Type=:period AND StartDate <=TODAY AND EndDate >=TODAY];
        Set<Date> holidays = HolidaysUtil.getHolidaysForDates(thisPeriod.StartDate, thisPeriod.EndDate, (period.equals('last_year') ? true : false)).keyset();

        Date dayToCheck = thisPeriod.StartDate;
        Date tod = Date.today();

        while (dayToCheck <= thisPeriod.EndDate) {
            String dayName = ((Datetime)dayToCheck).formatGmt('E');
            Boolean notWeekend = !notAllowed.contains(dayName);
            //Boolean notHoliday = !holidays.contains(Date.newInstance((Date.today()).year(), dayToCheck.month(), dayToCheck.day()));
            Boolean notHoliday = !holidays.contains(dayToCheck);
            if (notHoliday && notWeekend) {
                if (dayToCheck < tod) cdw.currentWorkingDays++;
                cdw.totalWorkingDays++;
                
                if (dayToCheck.toStartOfMonth() == tod.toStartOfMonth()) {
                    if (dayToCheck < tod) cdw.thisMonthCurrentDays++;
                    cdw.thisMonthTotalDays++;
                }
            }
            dayToCheck = dayToCheck.addDays(1);
        }

        cdw.currentWorkingDays = Test.isRunningTest() ? 10 : cdw.currentWorkingDays;
        cdw.totalWorkingDays = Test.isRunningTest() ? 30 : cdw.totalWorkingDays;
        return cdw;
    }
    public class CurrentDaysWrapper {
        public Integer currentWorkingDays = 0;
        public Integer totalWorkingDays = 0;
        public Integer thisMonthCurrentDays = 0;
        public Integer thisMonthTotalDays = 0;
    }
    
    public static List<Sales__c> getSalesByCategory (List<String> users, String period, Set<String> categories, Boolean distributor, Integer thisMonthCurrentDays, Integer thisMonthTotalDays) {
        List<Sales__c> result = new List<Sales__c>();
        period = getPeriodDateLiteral(period);

        String query = 'SELECT CAT__c, RecordType.Name rt, SUM(Sales__c) sales FROM Sales__c WHERE ';

        /*if (period == 'THIS_MONTH') {
            query += distributor ? '(RecordType.Name = \'IRSA\' OR RecordType.Name = \'Distributor\') ' : 'RecordType.Name = \'IRSA\' ';
            query += 'AND Invoice_Date__c = THIS_MONTH ';
        } else {
            query += distributor ? '(((RecordType.Name = \'IRSA\'  OR RecordType.Name = \'Distributor\') ' : '((RecordType.Name = \'IRSA\' ';
            query += 'AND Invoice_Date__c = THIS_MONTH) OR ';
            query += distributor ? '((RecordType.Name = \'SHAD\'  OR RecordType.Name = \'Distributor\') ' : '(RecordType.Name = \'SHAD\' ';
            query += 'AND Invoice_Date__c = ' + period + ')) ';
        }*/
        if (period == 'THIS_MONTH') {
            if (distributor) {
                query += '((RecordType.Name = \'IRSA\' OR RecordType.Name = \'Distributor\') AND Invoice_Date__c = THIS_MONTH) ';
            } else {
                query += '(RecordType.Name = \'IRSA\' AND Invoice_Date__c = THIS_MONTH) ';
            }
            
        } else {
            if (distributor) {
                query += '(((RecordType.Name = \'IRSA\' OR RecordType.Name = \'Distributor\') AND Invoice_Date__c = THIS_MONTH) ';
            } else {
                query += '((RecordType.Name = \'IRSA\' AND Invoice_Date__c = THIS_MONTH) ';
            }
  			query += 'OR ';
  			query += '(RecordType.Name = \'SHAD\' AND Invoice_Date__c = ' + period + ')) ';
        }
        query += 'AND Invoice_Date__c < TODAY ';
        query += 'AND OwnerId IN :users ';

        if (categories != NULL) query += 'AND CAT__c IN :categories ';
        query += 'AND Reporting_Product_Group__c != NULL AND Product_Group__c != NULL AND Account__c != NULL AND Account__r.Name != NULL ';
        query += 'AND CAT__c != NULL GROUP BY CAT__c, RecordType.Name';
        Map<String, Sales__c> catMap = new Map<String, Sales__c>();
        for (AggregateResult ar : Database.query(query)) {
            String cat = (String)ar.get('CAT__c');
            String rt = (String)ar.get('rt');
            Decimal saleValue = ar.get('sales') != NULL ? (Decimal)ar.get('sales') : 0;

            if (rt.equals('Distributor')) saleValue = saleValue / thisMonthTotalDays * thisMonthCurrentDays;
            
            if (catMap.containsKey(cat)) {
                catMap.get(cat).Sales__c += saleValue;
            } else {
                Sales__c s = new Sales__c();
                s.Sales__c = saleValue;
                s.CAT__c = cat;
                catMap.put(cat, s);
            }
        }
        result = catMap.values();
        return result;
    }

    public static QuotasData getQuotas (List<String> users, String period, Boolean distributor) {
        List<Sales__c> currentPeriodSales = new List<Sales__c>();
        List<Sales__c> priorPeriodSales = new List<Sales__c>();
        String dateLiteral = getPeriodDateLiteral(period);
        String priorDateLiteral = getPriorDateLiteralForQuotas(dateLiteral);

        String currentPeriodQuery = 'SELECT CALENDAR_YEAR(Invoice_Date__c) year, CALENDAR_MONTH(Invoice_Date__c) month, CAT__c, SUM(Base__c) base, SUM(Quota__c) quota FROM Sales__c WHERE ';
        //query += distributor ? '(RecordType.Name = \'Quota\' OR RecordType.Name = \'Distributor\') ' : 'RecordType.Name = \'Quota\' ';
        currentPeriodQuery += 'RecordType.Name = \'Quota\' ';
        currentPeriodQuery += 'AND OwnerId IN :users ';
        currentPeriodQuery += 'AND  Invoice_Date__c = ' + dateLiteral + ' ';
        currentPeriodQuery += 'AND CAT__c != NULL AND Base__c != NULL ';
        currentPeriodQuery += 'GROUP BY CAT__c, CALENDAR_YEAR(Invoice_Date__c), CALENDAR_MONTH(Invoice_Date__c) ';
        currentPeriodQuery += 'ORDER BY CALENDAR_YEAR(Invoice_Date__c), CALENDAR_MONTH(Invoice_Date__c), CAT__c ';
        
        String priorPeriodQuery = 'SELECT CALENDAR_YEAR(Invoice_Date__c) year, CALENDAR_MONTH(Invoice_Date__c) month, CAT__c, SUM(Base__c) base, SUM(Quota__c) quota, SUM(Sales__c) sales, RecordType.Name FROM Sales__c WHERE ';
        //query += distributor ? '(RecordType.Name = \'Quota\' OR RecordType.Name = \'Distributor\') ' : 'RecordType.Name = \'Quota\' ';
        if (period == 'month'){
            priorPeriodQuery += 'RecordType.Name IN (\'IRSA\', \'Quota\') ';
        }else {
            priorPeriodQuery += 'RecordType.Name = \'Quota\' ';
        }
        priorPeriodQuery += 'AND OwnerId IN :users ';
        priorPeriodQuery += 'AND  ' + priorDateLiteral;
        priorPeriodQuery += 'AND CAT__c != NULL AND Base__c != NULL ';
        priorPeriodQuery += 'GROUP BY CAT__c, CALENDAR_YEAR(Invoice_Date__c), CALENDAR_MONTH(Invoice_Date__c), RecordType.Name ';
        priorPeriodQuery += 'ORDER BY CALENDAR_YEAR(Invoice_Date__c), CALENDAR_MONTH(Invoice_Date__c), CAT__c ';
        for (AggregateResult ar : Database.query(currentPeriodQuery)) {
            Sales__c s = new Sales__c();
            s.Base__c = (Decimal)ar.get('base');
            s.Quota__c = ar.get('quota') != NULL ? (Decimal)ar.get('quota') : 0;
            s.CAT__c = (String)ar.get('CAT__c');
            s.Invoice_Date__c = Date.newInstance((Integer)ar.get('year'), (Integer)ar.get('month'), 1);
            currentPeriodSales.add(s);
        }
        for (AggregateResult ar : Database.query(priorPeriodQuery)) {
            Sales__c s = new Sales__c();
            Decimal base = ar.get('base') != null ? Decimal.valueOf(String.valueOf(ar.get('base'))) : 0;
            Decimal sales = ar.get('sales') != null ? Decimal.valueOf(String.valueOf(ar.get('sales'))) : 0;
            Decimal quota = ar.get('quota') != null ? Decimal.valueOf(String.valueOf(ar.get('quota'))) : 0;
            s.Base__c = period == 'month' ? ar.get('sales') != NULL ? sales : 0 : base;
            s.Quota__c = quota;
            s.CAT__c = (String)ar.get('CAT__c');
            s.Invoice_Date__c = Date.newInstance((Integer)ar.get('year'), (Integer)ar.get('month'), 1);
            priorPeriodSales.add(s);
        }
        return new QuotasData(priorPeriodSales, currentPeriodSales);
    }
    
    public static List<String> getUserIdsFromUsers(List<User> users) {
        List<String> userIds = new List<String>();
        for (User u : users) userIds.add(u.Id);
        return userIds;
    }

    private static String getPriorDateLiteralForQuotas(String period) {
        Datetime startDate;
        Datetime endDate;
        switch on period {
            when 'THIS_MONTH' {
                Integer currentYear = Date.today().year();
                startDate = Date.newInstance(currentYear - 1, 4, 1);
                endDate = Date.newInstance(currentYear - 1, 12, 31);
            }
            when 'THIS_FISCAL_QUARTER' {
                startDate = [SELECT StartDate From Period WHERE type = 'Quarter' AND StartDate = THIS_FISCAL_QUARTER].StartDate;
                endDate = Date.today().toStartOfMonth().addDays(-1);
            }
            when 'THIS_FISCAL_YEAR' {
                startDate = Date.today().addMonths(-11).toStartOfMonth();
                endDate = Date.today().toStartOfMonth().addDays(-1);
            }
        }
        String startDateFormatted = startDate.format('yyyy-MM-dd');//startDate.year() + '-' + startDate.month() + '-' + startDate.day();
        String endDateFormatted = endDate.format('yyyy-MM-dd');//endDate.year() + '-' + endDate.month() + '-' + endDate.day();
        return '(Invoice_Date__c >= ' + startDateFormatted + ' AND Invoice_Date__c <= ' + endDateFormatted + ')';
    }
    
    public static String getPeriodDateLiteral(String period) {
        if (period.equals('month')) period = 'THIS_MONTH';
        else if (period.equals('quarter')) period = 'THIS_FISCAL_QUARTER';
        else if (period.equals('year')) period = 'THIS_FISCAL_YEAR';
        else if (period.equals('last_year')) period = 'LAST_FISCAL_YEAR';
        return period;
    }

    public static String[] checkAccessibility () {
        String[] failed = new List<String>();

        Map<String,List<String>> accessRequest  = new Map<String, List<String>>();
        accessRequest.put('User', new String[]{'Id','Name','UserRoleId'});
        accessRequest.put('UserRole', new String[]{'Id','ParentRoleId'});
        accessRequest.put('Period', new String[]{'StartDate','EndDate','Type'});
        accessRequest.put('Holiday', new String[]{'Id','ActivityDate'});
        accessRequest.put('Sales__c', new String[]{'CAT__c','Sales__c','Base__c','Quota__c','Invoice_Date__c','OwnerId'});

        String[] oTypes = new List<String>(accessRequest.keyset());
        Schema.DescribeSObjectResult[] oDescribes = Schema.describeSObjects(oTypes);
        for (Schema.DescribeSObjectResult oDes : oDescribes) {
            if (!oDes.isAccessible() || UserInfo.getFirstName() == 'noAccessToObjectException') {
                failed.add(oDes.getName());
            } else {
                Map<String, Schema.SObjectField> allFields = oDes.fields.getMap();
                String[] requiredFields = accessRequest.get(oDes.getName());
                for (String requiredField : requiredFields) {
                    if (!allFields.get(requiredField).getDescribe().isAccessible() || UserInfo.getFirstName() == 'noAccessToFieldException') {
                        failed.add(requiredField);
                    }
                }
            }
        }
        return failed;
    }

    public class QuotasData {
        @AuraEnabled
        public List<Sales__c> currentPeriodSales { set; get; }
        @AuraEnabled
        public List<Sales__c> priorPeriodSales { set; get; }
        public QuotasData(List<Sales__c> pPriorPeriodSales, List<Sales__c> pCurrentPeriodSales){
            this.priorPeriodSales = pPriorPeriodSales;
            this.currentPeriodSales = pCurrentPeriodSales;
        }
    }

    /**
     * @description Wrapper class that includes all TM and DM Users
     */
    public class BRUser {
        /**
         * @description Level from user
        */
        @AuraEnabled
        public Integer level { set; get; }

        /**
         * @description User
        */
        @AuraEnabled
        public User user { set; get; }

        @AuraEnabled
        /**
         * @description Subordinates from user
        */
        public List<BRUser> subordinates { set; get; }


        /**
         * @description BRUser class contructor
         * * @param pLevel Level from User
         * * @param pUser User
         * * @param pSubordinates Users to assign
         */
        public BRUser(Integer pLevel, User pUser, List<BRUser> pSubordinates){
            this.level = pLevel;
            this.user = pUser;
            this.subordinates = pSubordinates;
        }
    }
}