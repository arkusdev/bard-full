public class CampaignAttendeeDocumentController {
	@auraEnabled
	public static List<CampaignAttendeeDocumentDTO> fetchCampaignAttendeeDocuments(id attendeeId, id campaignId, string type) {
		List<CampaignAttendeeDocumentDTO> dtos = new List<CampaignAttendeeDocumentDTO>();

		string keyField = (type.equalsIgnoreCase('attendee') ? 'Contact__c' : 'User__c');

		string selectStatement = 'SELECT Id, Campaign_Document__c, Campaign_Document__r.Name, Name, Contact__c, Contact__r.Name, User__c, User__r.Name, Received__c ' +
								 'FROM Campaign_Attendee_Document__c ' +
								 'WHERE ' + keyField + ' = :attendeeId ' +
								 'AND Campaign_Document__r.Campaign__c = :campaignId ' +
								 'ORDER BY Campaign_Document__r.Name';

		List<Campaign_Attendee_Document__c> docs = Database.query(selectStatement);
		
		for(Campaign_Attendee_Document__c cmd :docs) {
			dtos.add(new CampaignAttendeeDocumentDTO(cmd));
		}

		return dtos;
	}

	@auraEnabled
	public static void saveCampaignAttendeeDocuments(string dto) {
		List<CampaignAttendeeDocumentDTO> dtos = ((List<CampaignAttendeeDocumentDTO>)System.JSON.deserializeStrict(dto, List<CampaignAttendeeDocumentDTO>.Class));

		List<Campaign_Attendee_Document__c> docs = new List<Campaign_Attendee_Document__c>();

		for(CampaignAttendeeDocumentDTO cmd :dtos) {
			docs.add(new Campaign_Attendee_Document__c(
				Id = cmd.memberDocId,
				Received__c = cmd.received
			));
		}

		update docs;
	}
}