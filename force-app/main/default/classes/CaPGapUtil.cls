public with sharing class CaPGapUtil {
    public static void fillResponseInfo (String userId, String period, CustomerAndProductGapController.CalculationsResponse response) {
        PerDaySalesUtil.fillWorkingDays(period, response);
        response.daysInPriorYear = PerDaySalesUtil.getTotalWorkingDays('year');// last_year
        response.users = PerDaySalesUtil.getUsers(userId);
    }

    public static List<AggregateResult> getIRSASalesAggr (List<String> users, String period, String view, String accountID, Boolean distributor, Integer thisMonthTotalDays, Integer thisMonthOngoingDays) {
        period = PerDaySalesUtil.getPeriodDateLiteral(period);
        String query = ConstructQueryBody(period, view, accountID, distributor);

        /*Map<String, Sales__c> keyMap = new Map<String, Sales__c>();
        for (AggregateResult ar : Database.query(query)) {
            String cat = (String)ar.get('CAT__c');
            String rt = (String)ar.get('rt');
            String reportingGroup = (String)ar.get('Reporting_Product_Group__c');
            String key = cat + reportingGroup;
            Decimal saleValue = ar.get('sales') != NULL ? (Decimal)ar.get('sales') : 0;
            
            if (view.equals('customer')) {
                String acc = (String)ar.get('Account__c');
                String accShip = (String)ar.get('accShip');
                String accCity = (String)ar.get('accCity');
                key += (acc + accShip + accCity);
            } else if (view.equals('product')) {
                String productGroup = (String)ar.get('Product_Group__c');
                key += productGroup;
            }

            if (rt.equals('Distributor')) saleValue = saleValue / thisMonthTotalDays * thisMonthOngoingDays;
            
            if (keyMap.containsKey(key)) {
                keyMap.get(key).Sales__c += saleValue;
            } else {
                Sales__c s = new Sales__c();
                s.Sales__c = saleValue;
                s.CAT__c = cat;
                s.Reporting_Product_Group__c = reportingGroup;
                keyMap.put(key, s);
            }
        }*/

        return Database.query(query);
    }
    
    public static List<AggregateResult> getPriorFiscalYearSalesAggr (List<String> users, String view, String accountID) {
        String query = ConstructQueryBody('LAST_FISCAL_YEAR', view, accountID, null);
        return Database.query(query);
    }

    public static String ConstructQueryBody (String period, String view, String accountID, Boolean distributor) {
        String selects, generalCondtions, rtCondition, groupby, orderby;
        String query = '';

        selects = 'SUM(Sales__c) sales, CAT__c, Reporting_Product_Group__c, Category__c FROM Sales__c WHERE ';
        //if (period != 'LAST_FISCAL_YEAR') selects = 'RecordType.Name rt, ' + selects;
        if (period != 'LAST_FISCAL_YEAR' && distributor) selects = 'RecordType.Name rt, ' + selects;

        if (period == 'THIS_MONTH') {
            if (distributor) {
                rtCondition = '((RecordType.Name = \'IRSA\' OR RecordType.Name = \'Distributor\') AND Invoice_Date__c = THIS_MONTH) ';
            } else {
                rtCondition = '(RecordType.Name = \'IRSA\' AND Invoice_Date__c = THIS_MONTH) ';
            }
        } else if (period == 'LAST_FISCAL_YEAR') {
            rtCondition = 'RecordType.Name = \'SHAD\' AND Invoice_Date__c = LAST_FISCAL_YEAR ';
        } else {
            if (distributor) {
                rtCondition = '(((RecordType.Name = \'IRSA\' OR RecordType.Name = \'Distributor\') AND Invoice_Date__c = THIS_MONTH) ';
            } else {
                rtCondition = '((RecordType.Name = \'IRSA\' AND Invoice_Date__c = THIS_MONTH) ';
            }
  			rtCondition += 'OR ';
  			rtCondition += '(RecordType.Name = \'SHAD\' AND Invoice_Date__c = ' + period + ')) ';
        }

        generalCondtions = 'AND Invoice_Date__c < TODAY ';
        generalCondtions += 'AND OwnerId IN :users ';
        generalCondtions += 'AND CAT__c != NULL AND Reporting_Product_Group__c != NULL AND Product_Group__c != NULL ';
        generalCondtions += 'AND Account__c != NULL AND Account__r.Name != NULL ';

        if (view.equals('customer')){
            selects = 'SELECT Account__c, Account__r.Name accName, Account__r.Ship_to_ID__c accShip, Account__r.ShippingCity accCity, ' + selects;
            groupby = 'GROUP BY Account__c, Account__r.Name, Account__r.Ship_to_ID__c, Account__r.ShippingCity, CAT__c, Reporting_Product_Group__c, Category__c ';
            orderby = 'ORDER BY Account__r.Name, Account__r.Ship_to_ID__c, Account__r.ShippingCity';
        } else if (view.equals('product')) {
            if (String.isNotBlank(accountID)) generalCondtions += 'AND Account__c =:accountID ';
            selects = 'SELECT Product_Group__c, ' + selects;
            groupby = 'GROUP BY Product_Group__c, OwnerId, CAT__c, Reporting_Product_Group__c, Category__c ';
            orderby = 'ORDER BY Product_Group__c ';
        } else if (view.equals('opportunity')) {
            if (String.isNotBlank(accountID)) generalCondtions += 'AND Account__c =:accountID ';
            selects = 'SELECT Product_Group__c, OwnerId, Account__c, ' + selects;
            groupby = 'GROUP BY Product_Group__c, OwnerId, CAT__c, Reporting_Product_Group__c, Category__c, Account__c ';
            orderby = 'ORDER BY Product_Group__c ';
        }
        //if (period != 'LAST_FISCAL_YEAR') groupby += ', RecordType.Name ';
        if (period != 'LAST_FISCAL_YEAR' && distributor) groupby += ', RecordType.Name ';

        query = selects + rtCondition + generalCondtions +  ' WITH SECURITY_ENFORCED ' +groupby + orderby;
        system.debug('#######view: ' + view);
        system.debug('#######selects: ' + selects);
        system.debug('#######rtCondition: ' + rtCondition);
        system.debug('#######generalCondtions: ' + generalCondtions);
        system.debug('#######groupby: ' + groupby);
        system.debug('#######orderby: ' + orderby);

        return query;
    }

    public static List<Opportunity> getOpportunities(List<Id> userIds) {
        
        return [select  Age__c,
                        Amount,
                        StageName,
                        IsClosed,
                        CloseDate,
                        Closing_in__c,
                        Carry_Over__c,
                        Product_Group__c,
                        OwnerId,
                        Owner.Name,
                        AccountId,
                        Account.Name,
                        Account.Ship_to_ID__c
                from Opportunity
                where OwnerId IN: userIds
                AND (CloseDate = LAST_FISCAL_YEAR OR CloseDate = THIS_FISCAL_YEAR)
                WITH SECURITY_ENFORCED
                ORDER BY Product_Group__c ASC,
                Amount DESC];
    }

    public static List<Category_Combination__mdt> getCategories() {
        List<Category_Combination__mdt> cats = new List<Category_Combination__mdt>();
        for (Category_Combination__mdt mdt : [select    MasterLabel, 
                                                        Quota__c, 
                                                        NSC__c 
                                                from Category_Combination__mdt 
                                                ORDER BY MasterLabel ASC])
            cats.add(mdt);
        return cats;
    }
}