@IsTest
private class ODINGlobalFinancialWizzardControllerTest {

    static testMethod void GetProjectInfoTest() {
        Project__c project = new Project__c();
        project.Name = 'Test_1';
        project.Launch_Year__c = Project__c.Launch_Year__c.getDescribe().getPicklistValues()[0].getLabel();
        project.Launch_Quarter__c = Project__c.Launch_Quarter__c.getDescribe().getPicklistValues()[0].getLabel();
        insert project;

        Proposed_Product__c product1 = new Proposed_Product__c();
        product1.Project__c = project.Id;
        product1.Name = 'Product 1';
        insert product1;

        Test.startTest();
            ODINGlobalFinancialWizzardController.ProjectInfo info = ODINGlobalFinancialWizzardController.GetProjectInfo(project.Id);
        Test.stopTest();

        system.assert(info.hasProduct);
        system.assertEquals(project.Launch_Year__c, info.year);
        system.assertEquals(project.Launch_Quarter__c, info.quarter);
    }

    static testMethod void GetProjectInfoHasProductFalseTest() {
        Project__c project = new Project__c();
        project.Name = 'Test_2';
        insert project;

        Test.startTest();
            ODINGlobalFinancialWizzardController.ProjectInfo info = ODINGlobalFinancialWizzardController.GetProjectInfo(project.Id);
        Test.stopTest();

        system.assert(!info.hasProduct);
    }

    static testMethod void GetInterestWYearTest() {
        Project__c project = new Project__c();
        project.Name = 'Test_3';
        insert project;

        Proposed_Product__c product1 = new Proposed_Product__c();
        product1.Project__c = project.Id;
        product1.Name = 'Product 1';
        insert product1;

        Interest__c interest = new Interest__c();
        interest.Project__c = project.Id;
        interest.Region__c = 'Canada';
        interest.Country__c = 'Canada';
        insert interest;

        RecordType recordType = [select Id 
                                from RecordType 
                                where SObjectType = 'Financial__c' 
                                and RecordType.DeveloperName = 'Global_Financial']; 

        List<Financial__c> finList = new List<Financial__c>();
        Financial__c fin1 = new Financial__c();
        fin1.Project__c = project.Id;
        fin1.Region__c = 'Canada';
        fin1.Country__c = 'Canada';
        fin1.Year__c = '2019';
        fin1.RecordTypeId = recordType.Id;
        finList.add(fin1);

        Financial__c fin2 = new Financial__c();
        fin2.Project__c = project.Id;
        fin2.Region__c = 'Canada';
        fin2.Country__c = 'Canada';
        fin2.Year__c = '2020';
        fin2.RecordTypeId = recordType.Id;
        finList.add(fin2);

        insert finList;

        Test.startTest();
            ODINGlobalFinancialWizzardController.InterestWYear iwy = ODINGlobalFinancialWizzardController.GetInterestWYear(project.Id, 'Canada');
        Test.stopTest();

        system.assertEquals(interest.Id, iwy.interest.Id);
        system.assertEquals('2019', iwy.year);
    }

    static testMethod void GetFinancialInfoTest() {
        Project__c project = new Project__c();
        project.Name = 'Test_4';
        insert project;

        List<Proposed_Product__c> productList = new List<Proposed_Product__c>();
        Proposed_Product__c product1 = new Proposed_Product__c();
        product1.Project__c = project.Id;
        product1.Name = 'Product 1';
        productList.add(product1);

        Proposed_Product__c product2 = new Proposed_Product__c();
        product2.Project__c = project.Id;
        product2.Name = 'Product 2';
        productList.add(product2);
        insert productList;

        RecordType recordType = [select Id 
                                from RecordType 
                                where SObjectType = 'Financial__c' 
                                and RecordType.DeveloperName = 'Global_Financial']; 

        Financial__c fin1 = new Financial__c();
        fin1.Project__c = project.Id;
        fin1.Region__c = 'Canada';
        fin1.Country__c = 'Canada';
        fin1.Year__c = '2019';
        fin1.Proposed_Product__c = product1.Id;
        fin1.RecordTypeId = recordType.Id;
        insert fin1;

        Test.startTest();
            List<ODINGlobalFinancialWizzardController.Financial> finList = ODINGlobalFinancialWizzardController.GetFinancialInfo(project.Id, 'Canada', false);
        Test.stopTest();

        system.assertEquals(2, finList.size());
        system.assertEquals(product1.Id, finList[0].record.Proposed_Product__c);
        system.assertEquals(product1.Name, finList[0].productName);
        system.assertEquals(product2.Id, finList[1].record.Proposed_Product__c);
        system.assertEquals(product2.Name, finList[1].productName);
    }

    static testMethod void CreateFinancialInfoTest() {
        Project__c project = new Project__c();
        project.Name = 'Test_5';
        insert project;

        List<Proposed_Product__c> productList = new List<Proposed_Product__c>();
        Proposed_Product__c product1 = new Proposed_Product__c();
        product1.Project__c = project.Id;
        product1.Name = 'Product 1';
        productList.add(product1);

        Proposed_Product__c product2 = new Proposed_Product__c();
        product2.Project__c = project.Id;
        product2.Name = 'Product 2';
        productList.add(product2);
        insert productList;

        RecordType recordType = [select Id 
                                from RecordType 
                                where SObjectType = 'Financial__c' 
                                and RecordType.DeveloperName = 'Global_Financial']; 

        String year = '2019';
        List<Financial__c> financialList = new List<Financial__c>();
        for (Integer i = 0; i < 10; i++) {
            Financial__c financial = new Financial__c();
            financial.Project__c = project.Id;
            financial.Proposed_Product__c = product1.Id;
            financial.Country__c = 'Canada';
            financial.Region__c = 'Canada';
            financial.Year__c = String.valueOf(Integer.valueOf(year) + i);
            financial.Units__c = 10;
            financial.ASP__c = 20;
            financial.RecordTypeId = recordType.Id;
            financialList.add(financial);
        }
        insert financialList;

        Financial__c sampleFinancial = new Financial__c();
        sampleFinancial.Region__c = 'Canada';
        sampleFinancial.Country__c = 'Canada';
        sampleFinancial.Year__c = '2019';
        sampleFinancial.Launch_Quarter__c = 'Q1';
        sampleFinancial.Project__c = project.Id;

        Financial__c productFinancial = new Financial__c();
        productFinancial.Project__c = project.Id;
        productFinancial.Proposed_Product__c = product2.Id;
        productFinancial.Units__c = 10;
        productFinancial.ASP__c = 20;
        productFinancial.Unit_Growth__c = 10;
        productFinancial.Non_Unit_Growth__c = 2;
        productFinancial.Cannibalized_Unit_Growth__c = 50;
        productFinancial.Cannibalized_ASP__c = 30;
        productFinancial.Cannibalized_Units__c = 40;
        productFinancial.ASP_Growth__c = 10;
        productFinancial.RecordTypeId = recordType.Id;

        Test.startTest();
            ODINGlobalFinancialWizzardController.FinancialWrapper wrapper = ODINGlobalFinancialWizzardController.CreateFinancialInfo(sampleFinancial, new List<Financial__c>{productFinancial});
        Test.stopTest();

        system.assertEquals(10, wrapper.years.size());
        system.assertEquals(2, wrapper.financialsProjectPerYearList.size());
        for (List<ODINGlobalFinancialWizzardController.Financial> finList : wrapper.financialsProjectPerYearList) {
            system.assertEquals(10, finList.size());
        }
    }

    static testMethod void CreateFinancialInfoWProductTest() {
        Project__c project = new Project__c();
        project.Name = 'Test_6';
        insert project;

        Proposed_Product__c product1 = new Proposed_Product__c();
        product1.Project__c = project.Id;
        product1.Name = 'Product 1';
        insert product1;

        RecordType recordType = [select Id 
                                from RecordType 
                                where SObjectType = 'Financial__c' 
                                and RecordType.DeveloperName = 'Global_Financial']; 

        RecordType recordTypeProduct = [select Id 
                                from RecordType 
                                where SObjectType = 'Financial__c' 
                                and RecordType.DeveloperName = 'Product'];     

        String year = '2019';
        List<Financial__c> financialList = new List<Financial__c>();
        for (Integer i = 0; i < 10; i++) {
            Financial__c financial = new Financial__c();
            financial.Project__c = project.Id;
            financial.Proposed_Product__c = product1.Id;
            financial.Year__c = String.valueOf(Integer.valueOf(year) + i);
            financial.Units__c = 10;
            financial.ASP__c = 20;
            financial.RecordTypeId = recordTypeProduct.Id;
            financial.Year_Sequence__c = i + 1;
            financialList.add(financial);
        }
        insert financialList;

        Financial__c sampleFinancial = new Financial__c();
        sampleFinancial.Region__c = 'Canada';
        sampleFinancial.Country__c = 'Canada';
        sampleFinancial.Year__c = year;
        sampleFinancial.Launch_Quarter__c = 'Q1';
        sampleFinancial.Project__c = project.Id;

        Financial__c productFinancial = new Financial__c();
        productFinancial.Project__c = project.Id;
        productFinancial.Proposed_Product__c = product1.Id;
        productFinancial.Units__c = 10;
        productFinancial.ASP__c = 20;
        productFinancial.Unit_Growth__c = 10;
        productFinancial.Non_Unit_Growth__c = 2;
        productFinancial.Cannibalized_Unit_Growth__c = 50;
        productFinancial.Cannibalized_ASP__c = 30;
        productFinancial.Cannibalized_Units__c = 40;
        productFinancial.ASP_Growth__c = 10;
        productFinancial.RecordTypeId = recordType.Id;

        Test.startTest();
            ODINGlobalFinancialWizzardController.FinancialWrapper wrapper = ODINGlobalFinancialWizzardController.CreateFinancialInfo(sampleFinancial, new List<Financial__c>{productFinancial});
        Test.stopTest();

        List<Id> productIDs = new List<Id>();
        for (ODINGlobalFinancialWizzardController.Financial financial : wrapper.financialsProjectPerYearList[0]) {
            system.assert(!String.isBlank(financial.record.Product__c));
            productIDs.add(financial.record.Product__c);
        }
        Map<Id, Financial__c> finMap = new Map<Id, Financial__c>([select Year__c 
                                                                    from Financial__c 
                                                                    where Project__c =: project.Id
                                                                    and RecordTypeId =: recordTypeProduct.Id
                                                                    and Proposed_Product__c =: product1.Id
                                                                    and Id IN: productIDs]);
        for (ODINGlobalFinancialWizzardController.Financial financial : wrapper.financialsProjectPerYearList[0])
            system.assertEquals(financial.record.Year__c, finMap.get(financial.record.Product__c).Year__c);
        
    }

    static testMethod void CreateFinancialInfoWProductTest2() {
        Project__c project = new Project__c();
        project.Name = 'Test_7';
        insert project;

        Proposed_Product__c product1 = new Proposed_Product__c();
        product1.Project__c = project.Id;
        product1.Name = 'Product 1';
        insert product1;

        RecordType recordType = [select Id 
                                from RecordType 
                                where SObjectType = 'Financial__c' 
                                and RecordType.DeveloperName = 'Global_Financial']; 

        RecordType recordTypeProduct = [select Id 
                                from RecordType 
                                where SObjectType = 'Financial__c' 
                                and RecordType.DeveloperName = 'Product'];     

        String year = '2019';
        List<Financial__c> financialList = new List<Financial__c>();
        for (Integer i = 0; i < 10; i++) {
            Financial__c financial = new Financial__c();
            financial.Project__c = project.Id;
            financial.Proposed_Product__c = product1.Id;
            financial.Year__c = String.valueOf(Integer.valueOf(year) + i);
            financial.Units__c = 10;
            financial.ASP__c = 20;
            financial.RecordTypeId = recordTypeProduct.Id;
            financial.Year_Sequence__c = i + 1;
            financialList.add(financial);

            Financial__c financial2 = new Financial__c();
            financial2.Project__c = project.Id;
            financial2.Proposed_Product__c = product1.Id;
            financial2.Country__c = 'Canada';
            financial2.Region__c = 'Canada';
            financial2.Year__c = String.valueOf(Integer.valueOf(year) + i);
            financial2.Units__c = 10;
            financial2.ASP__c = 20;
            financial2.RecordTypeId = recordType.Id;
            financialList.add(financial2);
        }
        insert financialList;

        Financial__c sampleFinancial = new Financial__c();
        sampleFinancial.Region__c = 'Canada';
        sampleFinancial.Country__c = 'Canada';
        sampleFinancial.Year__c = year;
        sampleFinancial.Launch_Quarter__c = 'Q1';
        sampleFinancial.Project__c = project.Id;

        Financial__c productFinancial = new Financial__c();
        productFinancial.Project__c = project.Id;
        productFinancial.Proposed_Product__c = product1.Id;
        productFinancial.Units__c = 10;
        productFinancial.ASP__c = 20;
        productFinancial.Unit_Growth__c = 10;
        productFinancial.Non_Unit_Growth__c = 2;
        productFinancial.Cannibalized_Unit_Growth__c = 50;
        productFinancial.Cannibalized_ASP__c = 30;
        productFinancial.Cannibalized_Units__c = 40;
        productFinancial.ASP_Growth__c = 10;
        productFinancial.RecordTypeId = recordType.Id;

        Test.startTest();
            ODINGlobalFinancialWizzardController.FinancialWrapper wrapper = ODINGlobalFinancialWizzardController.CreateFinancialInfo(sampleFinancial, new List<Financial__c>{productFinancial});
        Test.stopTest();

        List<Id> productIDs = new List<Id>();
        for (ODINGlobalFinancialWizzardController.Financial financial : wrapper.financialsProjectPerYearList[0]) {
            system.assert(!String.isBlank(financial.record.Product__c));
            productIDs.add(financial.record.Product__c);
        }
        Map<Id, Financial__c> finMap = new Map<Id, Financial__c>([select Year__c 
                                                                    from Financial__c 
                                                                    where Project__c =: project.Id
                                                                    and RecordTypeId =: recordTypeProduct.Id
                                                                    and Proposed_Product__c =: product1.Id
                                                                    and Id IN: productIDs]);
        for (ODINGlobalFinancialWizzardController.Financial financial : wrapper.financialsProjectPerYearList[0])
            system.assertEquals(financial.record.Year__c, finMap.get(financial.record.Product__c).Year__c);
        
    }

    static testMethod void GetProjectQuestionsTest() {
        Project__c project = new Project__c();
        project.Name = 'Test_8';
        insert project;

        List<Question__c> questionList = new List<Question__c>();
        Question__c question1 = new Question__c();
        question1.Question__c = 'Test';
        question1.Project__c = project.Id;
        question1.Required__c = true;
        question1.Show_In_Global_Financials__c = true;
        questionList.add(question1);

        Question__c question2 = new Question__c();
        question2.Question__c = 'Test2';
        question2.Project__c = project.Id;
        question2.Required__c = false;
        question2.Show_In_Global_Financials__c = true;
        questionList.add(question2);

        insert questionList;

        Answer__c answer = new Answer__c();
        answer.Question__c = question1.Id;
        answer.Answer__c = 'Test';
        answer.Region__c = 'Canada';
        answer.Country__c = 'Canada';
        insert answer;

        Test.startTest();
            ODINGlobalFinancialWizzardController.QuestionWrapper wrapper = ODINGlobalFinancialWizzardController.GetProjectQuestions(project.Id, 'Canada', 'Canada');
        Test.stopTest();

        for (ODINGlobalFinancialWizzardController.QuestionWAnswer qwa : wrapper.requiredQuestions) {
            if (qwa.question.Id == question1.Id)
                system.assertEquals(answer.Id, qwa.answer.Id);
        }

        for (ODINGlobalFinancialWizzardController.QuestionWAnswer qwa : wrapper.questions) {
            if (qwa.question.Id == question2.Id)
                system.assertEquals(null, qwa.answer.Id);
        }
    }

    static testMethod void FinalSaveTest() {
        Project__c project = new Project__c();
        project.Name = 'Test_9';
        insert project;

        List<Proposed_Product__c> productList = new List<Proposed_Product__c>();
        Proposed_Product__c product1 = new Proposed_Product__c();
        product1.Project__c = project.Id;
        product1.Name = 'Product 1';
        productList.add(product1);

        Proposed_Product__c product2 = new Proposed_Product__c();
        product2.Project__c = project.Id;
        product2.Name = 'Product 2';
        productList.add(product2);
        insert productList;

        RecordType recordType = [select Id 
                                from RecordType 
                                where SObjectType = 'Financial__c' 
                                and RecordType.DeveloperName = 'Global_Financial']; 

        String year = '2019';
        List<Financial__c> financialList = new List<Financial__c>();
        for (Integer i = 0; i < 10; i++) {
            Financial__c financial = new Financial__c();
            financial.Project__c = project.Id;
            financial.Proposed_Product__c = product1.Id;
            financial.Country__c = 'Canada';
            financial.Region__c = 'Canada';
            financial.Year__c = String.valueOf(Integer.valueOf(year) + i);
            financial.Units__c = 10;
            financial.ASP__c = 20;
            financial.RecordTypeId = recordType.Id;
            financialList.add(financial);
        }
        insert financialList;

        Financial__c sampleFinancial = new Financial__c();
        sampleFinancial.Region__c = 'Canada';
        sampleFinancial.Country__c = 'Canada';
        sampleFinancial.Year__c = '2019';
        sampleFinancial.Launch_Quarter__c = 'Q1';
        sampleFinancial.Project__c = project.Id;

        Financial__c productFinancial = new Financial__c();
        productFinancial.Project__c = project.Id;
        productFinancial.Proposed_Product__c = product2.Id;
        productFinancial.Units__c = 10;
        productFinancial.ASP__c = 20;
        productFinancial.Unit_Growth__c = 10;
        productFinancial.Cannibalized_Unit_Growth__c = 50;
        productFinancial.Cannibalized_ASP__c = 30;
        productFinancial.Cannibalized_Units__c = 40;
        productFinancial.ASP_Growth__c = 10;
        productFinancial.RecordTypeId = recordType.Id;

        
        ODINGlobalFinancialWizzardController.FinancialWrapper wrapper = ODINGlobalFinancialWizzardController.CreateFinancialInfo(sampleFinancial, new List<Financial__c>{productFinancial});

        Question__c question1 = new Question__c();
        question1.Question__c = 'Test';
        question1.Project__c = project.Id;
        question1.Required__c = true;
        question1.Show_In_Global_Financials__c = true;
        insert question1;

        Answer__c answer = new Answer__c();
        answer.Question__c = question1.Id;
        answer.Answer__c = 'Test';
        answer.Region__c = 'Canada';
        answer.Country__c = 'Canada';

        Interest__c interest = new Interest__c();
        interest.Project__c = project.Id;
        interest.Region__c = 'Canada';
        interest.Country__c = 'Canada';

        String jsonString = JSON.serialize(wrapper);

        Test.startTest();
            ODINGlobalFinancialWizzardController.FinalSave(jsonString, new List<Answer__c>{answer}, interest, sampleFinancial);
        Test.stopTest();

        

        financialList = [select Id 
                        from Financial__c 
                        where Project__c =: project.Id
                        and Country__c = 'Canada'
                        and RecordTypeId =: recordType.Id];

        system.assertEquals(20, financialList.size());
        system.assertNotEquals(null, interest.Id);
        system.assertNotEquals(null, answer.Id);
    }
}