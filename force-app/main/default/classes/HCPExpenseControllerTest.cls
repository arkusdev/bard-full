@isTest
public class HCPExpenseControllerTest {
    @testSetup
    public static void method1() {    
        
        User u = [
            SELECT Id, Name, Phone, Email, Title, IsActive
            FROM User 
            WHERE Email = 'bd@apostletech.com'
        ][0]; 
        
        System.runAs(u) {
            List<Account> accts = TestDataFactory.createAccounts(1);
			List<Contact> proctorContacts = TestDataFactory.createContacts(accts, 2);
			List<Contact> attendeeContacts = TestDataFactory.createContacts(accts, 4);
            
            List<User> bdUsers = TestDataFactory.createUsers(2);
            
            List<Campaign> campaigns = TestDataFactory.createCampaigns(accts[0], 1);

			List<CampaignMember> proctorMembers = TestDataFactory.createCampaignMembers(campaigns[0], proctorContacts, 'Proctor');
			List<CampaignMember> attendeeMembers = TestDataFactory.createCampaignMembers(campaigns[0], attendeeContacts, 'Attendee');
			List<Campaign_User__c> userMembers = TestDataFactory.createCampaignUsers(campaigns[0], bdUsers);
            
			List<Campaign_Meal__c> meals = TestDataFactory.createCampaignMeals(campaigns[0], 1);
			TestDataFactory.createCampaignMealAttendee(meals[0], attendeeMembers);
			TestDataFactory.createCampaignMealAttendee(meals[0], userMembers);

			List<Campaign_Flight__c> attendeeFlights = TestDataFactory.createCampaignFlights(campaigns[0], attendeeMembers, 'Attendee');
			TestDataFactory.createCampaignFlightLegs(attendeeFlights);
			List<Campaign_Flight__c> staffFlights = TestDataFactory.createCampaignFlights(campaigns[0], userMembers, 'BD Staff');
			TestDataFactory.createCampaignFlightLegs(staffFlights);

			List<Campaign_Hotel__c> hotels = TestDataFactory.createCampaignHotels(campaigns[0], 1);
			TestDataFactory.createCampaignHotelAttendees(hotels[0], attendeeMembers);
			TestDataFactory.createCampaignHotelAttendees(hotels[0], userMembers);

			List<Campaign_Incidental__c> incidentals = TestDataFactory.createCampaignIncidental(campaigns[0], 1);
			TestDataFactory.createCampaignIncidentalAttendee(incidentals[0], attendeeMembers);
			TestDataFactory.createCampaignIncidentalAttendee(incidentals[0], userMembers);

			List<Campaign_Transportation__c> trans = TestDataFactory.createCampaignTransportations(campaigns[0], 1);
			TestDataFactory.createCampaignTransportationAttendee(trans[0], attendeeMembers);
			TestDataFactory.createCampaignTransportationAttendee(trans[0], userMembers);

			List<Campaign_Proctor_Fee__c> fees = TestDataFactory.createCampaignProctorFees(campaigns[0], proctorMembers);
        }
    }
  
    @isTest
    public static void TestMethod1() {
        
        Account acc = [Select Id,Name from account];
        
        Campaign campInfo = [select Id,Name from Campaign where Facility__c =: acc.id][0];
        //Contact con = [Select Id,Name from Contact WHERE Id IN (SELECT ContactId FROM CampaignMember WHERE CampaignId = :campInfo.Id)];
        Campaign_Meal__c cMeal = [select Id,Name from Campaign_Meal__c where Campaign__c =:campInfo.Id][0];
        System.debug('cMeal:::::'+cMeal);
        Campaign_Meal_Attendee__c cMealAtt = [select Id,Name,Campaign_Meal__c from Campaign_Meal_Attendee__c where Campaign_Meal__r.Campaign__c =: campInfo.Id][0];
        System.debug('cMealAtt:::::'+cMealAtt);
        Campaign_Flight__c cFlights = [Select Id,Name from Campaign_Flight__c where Campaign__c =:campInfo.Id][0];
        System.debug('cFlights:::::'+cFlights);
        apexpages.currentpage().getparameters().put('id',campInfo.Id);
        apexpages.currentpage().getparameters().put('type' , 'All');
        HCPExpenseController clsRef = new HCPExpenseController();
    }
}