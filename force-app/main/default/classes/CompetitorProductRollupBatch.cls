global class CompetitorProductRollupBatch implements Database.Batchable<sObject>{

    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'select Opportunity__c, Opportunity__r.Competitor_Product__c from Competitor__c ORDER BY Opportunity__c';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Id> oppIds = new List<Id>();
        for (Competitor__c competitor : (List<Competitor__c>) scope) {
            if (String.isBlank(competitor.Opportunity__r.Competitor_Product__c))
                oppIds.add(competitor.Opportunity__c);
        }

        CompetitorUtils.AddRemoveProductOnOpp(oppIds);
    }

   global void finish(Database.BatchableContext BC){
   }
}