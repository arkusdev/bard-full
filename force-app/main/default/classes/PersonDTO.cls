public class PersonDTO {
	@auraEnabled 
	public Id personId { get; set; } 
	@auraEnabled 
	public string fullName { get; set; } 
	@auraEnabled 
	public string phone { get; set; } 
	@auraEnabled 
	public string email { get; set; } 
	@auraEnabled 
	public string photoURL { get; set; } 
	@auraEnabled 
	public string bio { get; set; } 
	@auraEnabled 
	public string accountName { get; set; }
	@auraEnabled 
	public string pageURL { get; set; }
	@auraEnabled
	public List<ContractDTO> contracts { get; set; }
	@auraEnabled
	public boolean hasContracts { get; set; }
	@auraEnabled
	public boolean isActive { get; set; }
	@auraEnabled 
	public string title { get; set; } 
	@auraEnabled 
	public string credTracking { get; set; } 
	@auraEnabled
	public boolean alertReqFieldMissingFlg { get; set; }
	@auraEnabled
	public boolean alertNoContractFlg { get; set; }
	@auraEnabled 
	public boolean alertExpiringContractFlg { get; set; } 
	@auraEnabled
	public boolean alertExpiredContractFlg { get; set; }
	@auraEnabled
	public boolean alertOverlappingCourseFlg { get; set; }
	@auraEnabled
	public boolean alertMissingCredsFlg { get; set; }
	@auraEnabled
	public integer credsDueDayCount { get; set; }

	public PersonDTO(Contact c) {
		this.initDTO(c);

		if (c.Contracts__r.size() > 0) {
			for(Contract item :c.Contracts__r) {
				this.contracts.add(new ContractDTO(item));
			}	
		}

		if (!this.contracts.isEmpty()) {
			this.hasContracts = true;
			this.alertNoContractFlg = false;

			for(ContractDTO cd :this.contracts) {
				if (cd.alertExpiringContractFlg) { this.alertExpiringContractFlg = true; }
				if (cd.alertExpiredContractFlg) { this.alertExpiredContractFlg = true; }
			}
		}	
	}

	public PersonDTO(Contact c, date campaignStartDate, date campaignEndDate, Map<Id, Set<Id>> overlapMap, Map<Id, Map<Id, Campaign_Attendee_Document__c>> docMap) {
		this.initDTO(c);

		if (c.Contracts__r.size() > 0) {
			for(Contract item :c.Contracts__r) {
				this.contracts.add(new ContractDTO(item, campaignStartDate, campaignEndDate));
			}	
		}

		if (!this.contracts.isEmpty()) {
			this.hasContracts = true;
			this.alertNoContractFlg = false;

			for(ContractDTO cd :this.contracts) {
				if (cd.alertExpiringContractFlg) { this.alertExpiringContractFlg = true; }
				if (cd.alertExpiredContractFlg) { this.alertExpiredContractFlg = true; }
			}
		}

		this.alertOverlappingCourseFlg = (overlapMap.containsKey(this.personId) ? overlapMap.get(this.personId).size() > 0 : false);

		initCredentialProps(docMap);		
	}

	private void initDTO(Contact c) {
		this.personId = c.Id;
		this.fullName = c.Name;
		this.phone = this.FormatPhoneNumber(c.Phone);
		this.email = c.Email;
		this.photoURL = this.FormatContactPhoto(c.Contact_Photo__c);
		this.bio = (c.Contact_Bio__c != null ? c.Contact_Bio__c.replaceAll('<[^>]+>',' ') : '');
		this.accountName = c.Account.Name;
		this.credTracking = 'N/A';
		this.pageURL = PageNavUtil.getURL(c.Id);
		this.isActive = true;
		this.contracts = new List<ContractDTO>();

		this.hasContracts = false;
		this.alertNoContractFlg = true;
		this.alertReqFieldMissingFlg = (!c.CV_on_File__c || string.isBlank(c.Contact_Bio__c) || string.isBlank(c.Contact_Photo__c));
		this.alertExpiringContractFlg = false;
		this.alertExpiredContractFlg = false;
		this.alertOverlappingCourseFlg = false;
		this.alertMissingCredsFlg = false;
		this.credsDueDayCount = 0;
	}

	private String FormatContactPhoto(String photo){
		if (photo == null || 
			photo.substringBetween('<img', '/img>') == null ||
			photo.substringBetween('<img', '/img>').substringBetween('src="', '"') == null){
			return '';
		}
		return photo.substringBetween('<img', '/img>').substringBetween('src="', '"').replace('amp;', '');
	}

	public PersonDTO(User u, Map<Id, Map<Id, Campaign_Attendee_Document__c>> docMap) {
		this.personId = u.Id;
		this.fullName = u.Name;
		this.phone = this.FormatPhoneNumber(u.Phone);
		this.email = u.Email;
		this.title = u.Title;
		this.pageURL = PageNavUtil.getURL(u.Id);
		this.isActive = u.IsActive;
		this.credTracking = 'N/A';
		this.alertMissingCredsFlg = false;
		this.credsDueDayCount = 0;

		initCredentialProps(docMap);
	}

	public PersonDTO(Id personId, string personName, boolean isActive) {
		this.personId = personId;
		this.fullName = personName;
		this.isActive = isActive;
	}

	private void initCredentialProps(Map<Id, Map<Id, Campaign_Attendee_Document__c>> docMap) {
		if(docMap.containsKey(this.personId) && !docMap.get(this.personId).values().isEmpty()) {
			integer docTotal = docMap.get(this.personId).size();
			integer receivedCount = 0;

			for(Id key :docMap.get(this.personId).keySet()) {
				if (docMap.get(this.personId).get(key).Received__c) { receivedCount++; } 
			}

			if (receivedCount != docTotal) {
				date credsDue = docMap.get(this.personId).values()[0].Campaign_Document__r.Campaign__r.Credentials_Due_Date__c;

				if (credsDue != null) {
					this.alertMissingCredsFlg = true;
					this.credsDueDayCount = date.today().daysBetween(credsDue);				
				}
			}

			this.credTracking = string.valueOf(receivedCount) + ' of ' + string.valueOf(docTotal);
		}	
	}

	private string FormatPhoneNumber(string phone) {
		if (phone != null) {
			string omit = '[^0-9]';
			string digits;
  
			// remove all non numeric
			digits = phone.replaceAll(omit, '');

			// 10 digit: reformat with dashes
			if (digits.length() == 10) {
				return '(' + digits.substring(0, 3) + ') ' +
					   digits.substring(3, 6) + '-' +
					   digits.substring(6, 10);	  
			}

			// 11 digit: if starts with 1, format as 10 digit 
			if (digits.length() == 11) {
				if (digits.substring(0, 1) == '1') {
					return '(' + digits.substring(1, 4) + ') ' +
						   digits.substring(4, 7) + '-' +
						   digits.substring(7, 11);
				}
			}		
		}
 
		// if it isn't a 10 or 11 digit number, return the original because
		// it may contain an extension or special information
		return phone;
	}
}