public class ProjectRoleTriggerHandler extends TriggerHandler {
    public override void afterInsert(){
        ProjectRoleUtils.handleAfterInsert((List<Project_Role__c>)Trigger.new);
    }
    public override void beforeDelete(){
        ProjectRoleUtils.handleBeforeDelete((Map<Id, Project_Role__c>)Trigger.oldMap);
    }
    public override void afterUpdate(){
        ProjectRoleUtils.handleAfterUpdate((Map<Id, Project_Role__c>)Trigger.oldMap, (Map<Id, Project_Role__c>)Trigger.newMap);
    }
}