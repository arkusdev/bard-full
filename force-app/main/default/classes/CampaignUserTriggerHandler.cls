public class CampaignUserTriggerHandler extends TriggerHandler {
	/* overrides */
	protected override void afterInsert() {
		Map<Id, Set<Id>> campaignUserMap = getCampaignUserMap();

		insertCampaignUserDocuments(campaignUserMap);
	}

	protected override void afterDelete() {
		Map<Id, Set<Id>> campaignUserMap = getCampaignUserMap();

		deleteCampaignMeals(campaignUserMap);
		deleteCampaignFlights(campaignUserMap);
		deleteCampaignTransportations(campaignUserMap);
		deleteCampaignIncidentals(campaignUserMap);
		deleteCampaignHotels(campaignUserMap);
		deleteCampaignUserDocuments(campaignUserMap);
	}

	/* private methods */
	private void insertCampaignUserDocuments(Map<Id, Set<Id>> campaignUserMap) {
		if (!campaignUserMap.isEmpty()) { 
			List<Campaign_Document__c> campaignDocs = [
				SELECT Id, Campaign__c
				FROM Campaign_Document__c
				WHERE Campaign__c IN :campaignUserMap.keySet()
			];

			if (!campaignDocs.isEmpty()) {
				Map<Id, Set<Id>> campaignDocMap = new Map<Id, Set<Id>>();	

				for(Campaign_Document__c cd :campaignDocs) {
					if (!campaignDocMap.containsKey(cd.Campaign__c)) {
						campaignDocMap.put(cd.Campaign__c, new Set<Id>{ cd.Id });
					} else {
						campaignDocMap.get(cd.Campaign__c).add(cd.Id);
					}
				}

				if (!campaignDocMap.isEmpty()) {
					List<Campaign_Attendee_Document__c> memberDocs = new List<Campaign_Attendee_Document__c>();

					for(Id campaignId :campaignUserMap.keySet()) {
						if (campaignDocMap.containsKey(campaignId)) {
							for(Id userId :campaignUserMap.get(campaignId)) {
								for(Id docId :campaignDocMap.get(campaignId)) {
									memberDocs.add(new Campaign_Attendee_Document__c(
										Campaign_Document__c = docId,
										User__c = userId,
										Received__c = false
									));
								}
							}
						}
					}

					if (!memberDocs.isEmpty()) {
						insert memberDocs;
					}
				}
			}		
		}
	}

	private Map<Id, Set<Id>> getCampaignUserMap() {
		List<Campaign_User__c> cus = ((List<Campaign_User__c>)(trigger.isInsert ? trigger.new : trigger.old));

		Map<Id, Set<Id>> campaignUserMap = new Map<Id, Set<Id>>();
		
		for(Campaign_User__c cu: cus) {
			if (!campaignUserMap.keySet().contains(cu.Campaign__c)) {
				campaignUserMap.put(cu.Campaign__c, new Set<Id> { cu.User__c });
			} else {
				campaignUserMap.get(cu.Campaign__c).add(cu.User__c);
			}
		}
		
		return campaignUserMap;
	}

	private void deleteCampaignMeals(Map<Id, Set<Id>> campaignUserMap) {
		if (!campaignUserMap.isEmpty()) {
			//Next find all of the meals & associated attendees under every campaign in the map
			List<Campaign_Meal__c> meals = [
				SELECT Id, Campaign__c,
					(SELECT Id, User__c FROM Campaign_Meal_Attendees__r WHERE Contact__c = NULL)
				FROM Campaign_Meal__c
				WHERE Campaign__c IN :campaignUserMap.keySet()
				ORDER BY Campaign__c
			];

			if (!meals.isEmpty()) {
				//Now iterate over the meals & associated attendees
				//Check if each user exists in the map and add the id of the meal rec to be deleted
				List<Campaign_Meal_Attendee__c> attendees = new List<Campaign_Meal_Attendee__c>();

				for(Campaign_Meal__c cm: meals) {
					if (campaignUserMap.keySet().contains(cm.Campaign__c)) {
						for(Campaign_Meal_Attendee__c cma : cm.Campaign_Meal_Attendees__r) {
							if (campaignUserMap.get(cm.Campaign__c).contains(cma.User__c)) {
								attendees.add(new Campaign_Meal_Attendee__c(
									Id = cma.Id
								));
							}					
						}
					}
				}

				if (!attendees.isEmpty()) {
					delete attendees;
				}
			}
		}
	}

	private void deleteCampaignFlights(Map<Id, Set<Id>> campaignUserMap) {
		if (!campaignUserMap.isEmpty()) {
			//Next find all of the flights associated to each user in the map
			List<Campaign_Flight__c> flights = [
				SELECT Id, Campaign__c, User__c
				FROM Campaign_Flight__c
				WHERE Campaign__c IN :campaignUserMap.keySet()
				ORDER BY Campaign__c
			];

			if (!flights.isEmpty()) {
				//Now iterate over the flights & associated attendees
				//Check if each contact exists in the map and add the id of the meal rec to be deleted
				List<Campaign_Flight__c> fd = new List<Campaign_Flight__c>();

				for(Campaign_Flight__c cm: flights) {
					if (campaignUserMap.keySet().contains(cm.Campaign__c)) {
						if (campaignUserMap.get(cm.Campaign__c).contains(cm.User__c)) {
							fd.add(new Campaign_Flight__c(
								id = cm.Id
							));
						}					
					}
				}

				if (!fd.isEmpty()) {
					delete fd;
				}
			}
		}
	}

	private void deleteCampaignTransportations(Map<Id, Set<Id>> campaignUserMap) {
		if (!campaignUserMap.isEmpty()) {
			//Next find all of the flights & associated attendees under every campaign in the map
			List<Campaign_Transportation__c> trans = [
				SELECT Id, Campaign__c,
					(SELECT Id, User__c FROM Campaign_Transportation_Attendees__r WHERE Contact__c = NULL)
				FROM Campaign_Transportation__c
				WHERE Campaign__c IN :campaignUserMap.keySet()
				ORDER BY Campaign__c
			];

			if (!trans.isEmpty()) {
				//Now iterate over the trans & associated attendees
				//Check if each user exists in the map and add the id of the meal rec to be deleted
				List<Campaign_Transportation_Attendee__c> attendees = new List<Campaign_Transportation_Attendee__c>();

				for(Campaign_Transportation__c cm: trans) {
					if (campaignUserMap.keySet().contains(cm.Campaign__c)) {
						for(Campaign_Transportation_Attendee__c cta : cm.Campaign_Transportation_Attendees__r) {
							if (campaignUserMap.get(cm.Campaign__c).contains(cta.User__c)) {
								attendees.add(new Campaign_Transportation_Attendee__c(
									Id = cta.Id
								));
							}					
						}						
					}
				}

				if (!attendees.isEmpty()) {
					delete attendees;
				}
			}
		}
	}

	private void deleteCampaignIncidentals(Map<Id, Set<Id>> campaignUserMap) {
		if (!campaignUserMap.isEmpty()) {
			//Next find all of the incidentals & associated attendees under every campaign in the map
			List<Campaign_Incidental__c> incidentals = [
				SELECT Id, Campaign__c,
					(SELECT Id, User__c FROM Campaign_Incidental_Attendees__r WHERE Contact__c = NULL)
				FROM Campaign_Incidental__c
				WHERE Campaign__c IN :campaignUserMap.keySet()
				ORDER BY Campaign__c
			];

			if (!incidentals.isEmpty()) {
				//Now iterate over the incidentals & associated attendees
				//Check if each user exists in the map and add the id of the meal rec to be deleted
				List<Campaign_Incidental_Attendee__c> attendees = new List<Campaign_Incidental_Attendee__c>();

				for(Campaign_Incidental__c cm: incidentals) {
					if (campaignUserMap.keySet().contains(cm.Campaign__c)) {
						for(Campaign_Incidental_Attendee__c cia : cm.Campaign_Incidental_Attendees__r) {
							if (campaignUserMap.get(cm.Campaign__c).contains(cia.User__c)) {
								attendees.add(new Campaign_Incidental_Attendee__c(
									Id = cia.Id
								));
							}					
						}						
					}
				}

				if (!attendees.isEmpty()) {
					delete attendees;
				}
			}
		}
	}

	private void deleteCampaignHotels(Map<Id, Set<Id>> campaignUserMap) {
		if (!campaignUserMap.isEmpty()) {
			//Next find all of the hotels & associated attendees under every campaign in the map
			List<Campaign_Hotel__c> hotels = [
				SELECT Id, Campaign__c,
					(SELECT Id, User__c FROM Campaign_Hotel_Attendees__r WHERE Contact__c = NULL)
				FROM Campaign_Hotel__c
				WHERE Campaign__c IN :campaignUserMap.keySet()
				ORDER BY Campaign__c
			];

			if (!hotels.isEmpty()) {
				//Now iterate over the hotels & associated attendees
				//Check if each user exists in the map and add the id of the meal rec to be deleted
				List<Campaign_Hotel_Attendee__c> attendees = new List<Campaign_Hotel_Attendee__c>();

				for(Campaign_Hotel__c ch: hotels) {
					if (campaignUserMap.keySet().contains(ch.Campaign__c)) {
						for(Campaign_Hotel_Attendee__c cha : ch.Campaign_Hotel_Attendees__r) {
							if (campaignUserMap.get(ch.Campaign__c).contains(cha.User__c)) {
								attendees.add(new Campaign_Hotel_Attendee__c(
									Id = cha.Id
								));
							}					
						}						
					}
				}

				if (!attendees.isEmpty()) {
					delete attendees;
				}
			}
		}
	}

	private void deleteCampaignUserDocuments(Map<Id, Set<Id>> campaignUserMap) {
		if (!campaignUserMap.isEmpty()) {
			//Next find all of the member docs under every campaign user in the map
			List<Campaign_Attendee_Document__c> docs = [
				SELECT Id, Campaign_Document__r.Campaign__c, User__c
				FROM Campaign_Attendee_Document__c
				WHERE Campaign_Document__r.Campaign__c IN :campaignUserMap.keySet()
				AND Contact__c = NULL
				ORDER BY Campaign_Document__r.Name
			];

			if(!docs.isEmpty()) {
				//Now iterate over the docs & check if each user exists in the map
				//Check if each contact exists in the map and add the id of the doc rec to be deleted
				List<Campaign_Attendee_Document__c> memberDocs = new List<Campaign_Attendee_Document__c>();

				for(Campaign_Attendee_Document__c cmd :docs) {
					if (campaignUserMap.keySet().contains(cmd.Campaign_Document__r.Campaign__c)) {
						if (campaignUserMap.get(cmd.Campaign_Document__r.Campaign__c).contains(cmd.User__c)) {
							memberDocs.add(new Campaign_Attendee_Document__c(
								Id = cmd.Id
							));
						}
					}
				}

				if (!memberDocs.isEmpty()) {
					delete memberDocs;
				}
			}
		}
	}
}