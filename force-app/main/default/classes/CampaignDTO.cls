public class CampaignDTO {
	@auraEnabled 
	public Id campaignId { get; set; } 
	@auraEnabled 
	public string campaignName { get; set; } 
	@auraEnabled 
	public string division { get; set; } 
	@auraEnabled 
	public string productCategory { get; set; } 
	@auraEnabled 
	public string technique { get; set; } 
	@auraEnabled 
	public Date startDate { get; set; } 
	@auraEnabled 
	public string startDateDisplay { get; set; } 
	@auraEnabled 
	public Id facilityId { get; set; } 
	@auraEnabled 
	public string subLedger { get; set; } 
	@auraEnabled 
	public string description { get; set; } 
	@auraEnabled 
	public string proctorNames { get; set; } 
	@auraEnabled 
	public string ownerName { get; set; } 
	@auraEnabled 
	public string pageURL { get; set; }
	@auraEnabled
	public decimal remainingSeats { get; set; }
	@auraEnabled
	public boolean allowAttendees { get; set; }
	@auraEnabled
	public boolean isRegistered { get; set; }

	public CampaignDTO(Campaign c) {
		this.campaignId = c.Id;
		this.campaignName = c.Name;
		this.technique = c.Technique__c;
		this.division = c.Classification__c;
		this.productCategory = c.Product_Category__c;
		this.startDate = c.StartDate;
		this.startDateDisplay = c.StartDate.format();
		this.pageURL = PageNavUtil.getURL(c.Id);
		this.remainingSeats = c.Remaining_Seats__c;
		this.allowAttendees = (c.Remaining_Seats__c > 0);
		this.facilityId = c.Facility__c;
		this.subLedger = c.SubLedger__c;
		this.description = (c.Description != null ? (c.Description.length() >= 255 ? c.Description.substring(0, 251) + ' ...' : c.Description) : '');
		this.ownerName = c.Owner.Name;
		this.isRegistered = false;
		this.proctorNames = '';

		if(!c.CampaignMembers.isEmpty()) {
			for(CampaignMember cm :c.CampaignMembers) {
				this.proctorNames += cm.Contact.Name + ' | ';
			}

			this.proctorNames = this.proctorNames.substring(0, this.proctorNames.length() - 3); 
		}
	}

	public CampaignDTO(Campaign c, Id contactId) {
		this.campaignId = c.Id;
		this.campaignName = c.Name;
		this.technique = c.Technique__c;
		this.division = c.Classification__c;
		this.productCategory = c.Product_Category__c;
		this.startDate = c.StartDate;
		this.startDateDisplay = c.StartDate.format();
		this.pageURL = PageNavUtil.getURL(c.Id);
		this.remainingSeats = c.Remaining_Seats__c;
		this.allowAttendees = (c.Remaining_Seats__c > 0);
		this.facilityId = c.Facility__c;
		this.subLedger = c.SubLedger__c;
		this.isRegistered = false;
		for(CampaignMember cm :c.CampaignMembers) {
			if (cm.ContactId == contactId) {
				this.isRegistered = true;
				break;
			}
		}
	}
}