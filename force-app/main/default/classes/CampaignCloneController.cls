public class CampaignCloneController {
	@auraEnabled
	public static Id cloneCampaign(id campaignId, string dto) {
		CampaignDTO c = ((CampaignDTO)System.JSON.deserializeStrict(dto, CampaignDTO.Class));

		Campaign source = fetchSourceCampaign(campaignId);

		Campaign target = new Campaign(
			Name = c.campaignName,
			Facility__c = c.facilityId,
			Technique__c = c.technique,
			SubLedger__c = c.subLedger,
			Available_Seats__c = source.Available_Seats__c,
			BD_Seats__c = source.BD_Seats__c
		);

		insert target;

		List<Campaign_Document__c> docs = new List<Campaign_Document__c>();

		for(Campaign_Document__c cd :source.Campaign_Documents__r) {
			docs.add(new Campaign_Document__c(
				Campaign__c = target.Id,
				Name = cd.Name
			));
		}

		insert docs;

		return target.Id;
	}

	@auraEnabled
	public static Map<string, Map<string, string>> initListVariables(id campaignId) {
		Map<string, Map<string, string>> picklists = new Map<string, Map<string, string>> {
			'techniques' => PicklistUtil.fetchCampaignTechniques(),
			'documents' => fetchDocumentMap(campaignId)
		};

		return picklists;
	}

	private static Campaign fetchSourceCampaign(id campaignId) {
		return [
			SELECT Available_Seats__c, BD_Seats__c,
				(SELECT Id, Name FROM Campaign_Documents__r ORDER BY Name) 
			FROM Campaign
			WHERE Id = :campaignId
		];
	}

	private static Map<string, string> fetchDocumentMap(id campaignId) {
		Campaign c = fetchSourceCampaign(campaignId);

		Map<string, string> docMap = new Map<string, string>();

		for(Campaign_Document__c cd :c.Campaign_Documents__r) { 
			docMap.put(string.valueOf(cd.Id), cd.Name);
		}

		return docMap;
	}
}