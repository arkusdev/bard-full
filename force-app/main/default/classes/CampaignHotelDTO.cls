public class CampaignHotelDTO {
	@auraEnabled 
	public Id hotelId { get; set; } 
	@auraEnabled 
	public Id campaignId { get; set; } 
	@auraEnabled 
	public string name { get; set; }
	@auraEnabled 
	public date checkInDate { get; set; }
	@auraEnabled 
	public date checkOutDate { get; set; }
	@auraEnabled 
	public string checkInDateDisplay { get; set; }
	@auraEnabled 
	public string checkOutDateDisplay { get; set; }
	@auraEnabled 
	public decimal dayCount { get; set; }
	@auraEnabled 
	public string roomBlock { get; set; }
	@auraEnabled 
	public string notes { get; set; }
	@auraEnabled 
	public decimal guestCount { get; set; }
	@auraEnabled 
	public decimal totalAttendees { get; set; }
	@auraEnabled 
	public decimal totalPresentAttendees { get; set; }
	@auraEnabled 
	public decimal totalAbsentAttendees { get; set; }
	@auraEnabled 
	public decimal additionalAttendees { get; set; }
	@auraEnabled 
	public decimal additionalAttendeesCost { get; set; }
	@auraEnabled
	public string additionalAttendeesCostDisplay { get; set; }
	@auraEnabled 
	public decimal totalCost { get; set; }
	@auraEnabled
	public string totalCostDisplay { get; set; }
	@auraEnabled 
	public decimal totalAttendeeCost { get; set; }
	@auraEnabled
	public string totalAttendeeCostDisplay { get; set; }
	@auraEnabled 
	public decimal totalAbsentCost { get; set; }
	@auraEnabled
	public string totalAbsentCostDisplay { get; set; }
	@auraEnabled 
	public decimal totalPresentCost { get; set; }
	@auraEnabled
	public string totalPresentCostDisplay { get; set; }
	@auraEnabled 
	public List<CampaignHotelAttendeeDTO> availableMembers { get; set; }
	@auraEnabled 
	public List<CampaignHotelAttendeeDTO> selectedMembers { get; set; }
	@auraEnabled 
	public List<CampaignHotelAttendeeDTO> availableUsers { get; set; }
	@auraEnabled 
	public List<CampaignHotelAttendeeDTO> selectedUsers { get; set; }
	@auraEnabled 
	public Map<Id, List<CampaignHotelAttendeeDTO>> selectedMemberMap { get; set; }
	@auraEnabled 
	public Map<Id, List<CampaignHotelAttendeeDTO>> selectedUserMap { get; set; }
	@auraEnabled
	public string attendeesHelpText { get; set; }
	@auraEnabled 
	public boolean isNew { get; set; }

	List<string> args = new string[] { '0','number', '###,###,##0.00' };

	public CampaignHotelDTO(Campaign_Hotel__c ch) {
		this.initDTO(ch);
		this.initMaps();
	}

	public CampaignHotelDTO(Campaign_Hotel__c ch, List<CampaignMember> availMembers, List<Campaign_User__c> availUsers) {
		this.initDTO(ch);

		if (!availMembers.isEmpty()) {
			for(CampaignMember am :availMembers) {
				this.availableMembers.add(new CampaignHotelAttendeeDTO(am));
			}		
		}

		if (!availUsers.isEmpty()) {
			for(Campaign_User__c au :availUsers) {
				this.availableUsers.add(new CampaignHotelAttendeeDTO(au));
			}		
		}

		this.initMaps();
	}

	private void initDTO(Campaign_Hotel__c ch) {
		this.hotelId = ch.Id;
		this.isNew = (ch.Id == null);
		this.campaignId = ch.Campaign__c;
		this.name = ch.Name;
		this.checkInDate = ch.Check_In_Date__c;
		this.checkOutDate = ch.Check_Out_Date__c;
		this.checkInDateDisplay = (this.checkInDate != null ? this.checkInDate.format() : '');
		this.checkOutDateDisplay = (this.checkOutDate != null ? this.checkOutDate.format() : '');
		this.dayCount = (this.isNew ? 0 : ch.Day_Count__c); 
		this.roomBlock = ch.Room_Block__c;
		this.notes = ch.Notes__c;
		this.totalAttendees = ch.Total_Attendees__c;
		this.additionalAttendees = (ch.Additional_Attendees__c != null ? ch.Additional_Attendees__c : 0);
		this.additionalAttendeesCost = (ch.Additional_Attendees_Cost__c != null ? ch.Additional_Attendees_Cost__c : 0);
		this.totalPresentAttendees = ch.Attendee_Present_Count__c;
		this.totalAbsentAttendees = ch.Attendee_Absent_Count__c;
		this.totalCost = (ch.Total_Cost__c != null ? ch.Total_Cost__c : 0);
		this.totalAttendeeCost = (ch.Total_Attendee_Cost__c != null ? ch.Total_Attendee_Cost__c : 0);
		this.totalPresentCost = (ch.Total_Present_Cost__c != null ? ch.Total_Present_Cost__c : 0);
		this.totalAbsentCost = (ch.Total_Absent_Cost__c != null ? ch.Total_Absent_Cost__c : 0);
		this.selectedMembers = new List<CampaignHotelAttendeeDTO>();
		this.selectedUsers = new List<CampaignHotelAttendeeDTO>();
		this.availableMembers = new List<CampaignHotelAttendeeDTO>();
		this.availableUsers = new List<CampaignHotelAttendeeDTO>();
		

		if (!ch.Campaign_Hotel_Attendees__r.isEmpty()) {
			for(Campaign_Hotel_Attendee__c cha :ch.Campaign_Hotel_Attendees__r) {
				if (cha.Contact__c != null) {
					this.selectedMembers.add(new CampaignHotelAttendeeDTO(cha.Contact__c, cha.Contact__r.Name, cha));	
				} else {
					this.selectedUsers.add(new CampaignHotelAttendeeDTO(cha.User__c, cha.User__r.Name, cha));
				}
			}
		}

		this.totalCostDisplay = '$' + string.format(this.totalCost.format(), args);
		this.totalAttendeeCostDisplay = '$' + string.format(this.totalAttendeeCost.format(), args);
		this.totalPresentCostDisplay = '$' + string.format(this.totalPresentCost.format(), args);
		this.totalAbsentCostDisplay = '$' + string.format(this.totalAbsentCost.format(), args);
		this.additionalAttendeesCostDisplay = '$' + string.format(this.additionalAttendeesCost.format(), args);
	}

	private void initMaps() {
		this.selectedMemberMap = new Map<Id, List<CampaignHotelAttendeeDTO>>();
		this.selectedUserMap = new Map<Id, List<CampaignHotelAttendeeDTO>>();
		this.attendeesHelpText = '';

		for(CampaignHotelAttendeeDTO sm :this.selectedMembers) {
			if (!this.selectedMemberMap.containsKey(sm.attendeeId)) {
				this.selectedMemberMap.put(sm.attendeeId, new List<CampaignHotelAttendeeDTO> { sm });
				this.attendeesHelpText += sm.attendeeName + ' | ';
			} else {
				this.selectedMemberMap.get(sm.attendeeId).add(sm);
			}
		}

		for(CampaignHotelAttendeeDTO su :this.selectedUsers) {
			if (!this.selectedUserMap.containsKey(su.attendeeId)) {
				this.selectedUserMap.put(su.attendeeId, new List<CampaignHotelAttendeeDTO> { su });
				this.attendeesHelpText += su.attendeeName + ' | ';
			} else {
				this.selectedUserMap.get(su.attendeeId).add(su);
			}
		}

		if (!string.isEmpty(this.attendeesHelpText)) {
			this.attendeesHelpText = this.attendeesHelpText.substring(0, this.attendeesHelpText.length() - 3);

			if (this.additionalAttendees > 0) {
				this.attendeesHelpText += '.  Plus ' + string.valueOf(this.additionalAttendees) + ' additional attendees.';			
			}		
		}

		this.guestCount = (this.selectedUserMap.size() + this.selectedMemberMap.size() + this.additionalAttendees);	
	}

	public boolean containsAttendee(id attendeeId, string type) {
		if (type.equalsIgnoreCase('Contact')) {
			for(CampaignHotelAttendeeDTO cha :this.selectedMembers) {
				if (cha.attendeeId == attendeeId) { return true; }
			}		
		} else {
			for(CampaignHotelAttendeeDTO cha :this.selectedUsers) {
				if (cha.attendeeId == attendeeId) { return true; }
			}		
		}
		return false;
	}

	public boolean containsAttendeeReservation(id attendeeId, date reservationDate, string type) {
		if (type.equalsIgnoreCase('Contact')) {
			for(CampaignHotelAttendeeDTO cha :this.selectedMembers) {
				if (cha.attendeeId == attendeeId && cha.reservationDate == reservationDate) { return true; }
			}		
		} else {
			for(CampaignHotelAttendeeDTO cha :this.selectedUsers) {
				if (cha.attendeeId == attendeeId && cha.reservationDate == reservationDate) { return true; }
			}		
		}
		return false;		
	}
}