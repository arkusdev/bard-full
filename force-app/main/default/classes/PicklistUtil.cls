public class PicklistUtil {
    public PicklistUtil() {
    
    }

    public static Map<string, Map<string, string>> fetchPickListValues(List<string> fieldNames) {
        Map<string, Map<string, string>> valueMap = new Map<string, Map<string, string>>();

        for(string fn :fieldNames) {
            Map<string, string> options = new Map<string, string>();

            if (!valueMap.containsKey(fn)) {
                if (fn.equalsIgnoreCase(FieldNameUtil.licensed_regions)) {
                    List<Licensed_Region__c> regions = [
                        SELECT Name
                        FROM Licensed_Region__c
                    ];

                    for(Licensed_Region__c r :regions) {
                        options.put(r.Name, r.Name);
                    }
                } else if (fn.equalsIgnoreCase(FieldNameUtil.licensed_states)) {
                    List<Licensed_State__c> states = [
                        SELECT State_Abbreviation__c, Display_Name__c
                        FROM Licensed_State__c
                        ORDER BY Name
                    ];

                    for(Licensed_State__c r :states) {
                        options.put(r.State_Abbreviation__c, r.Display_Name__c);
                    }   
                } else {
                    Schema.DescribeFieldResult fr;
                    List<Schema.PicklistEntry> ple;

                    if (fn.equalsIgnoreCase(FieldNameUtil.contract_services)) {
                        fr = Contract.Contract_Services__c.getDescribe();
                    } else if (fn.equalsIgnoreCase(FieldNameUtil.surgeon_specialty)) {
                        fr = Contact.Surgeon_Specialty__c.getDescribe();
                    } else if (fn.equalsIgnoreCase(FieldNameUtil.division)) {
                        fr = Division__c.Division__c.getDescribe();
                    } else if (fn.equalsIgnoreCase(FieldNameUtil.product_category)) {
                        fr = Division__c.Product_Category__c.getDescribe();
                    } else if (fn.equalsIgnoreCase(FieldNameUtil.surgical_technique)) {
                        fr = Contact.Surgical_Technique__c.getDescribe();
                    } 

                    if (fr != null) {
                        ple = fr.getPicklistValues();

                        for(Schema.PicklistEntry f :ple) {
                            if (fn.equalsIgnoreCase(FieldNameUtil.contract_services) && f.getValue().equalsIgnoreCase('Royalties')) { continue; }
                            if (fn.equalsIgnoreCase(FieldNameUtil.surgeon_specialty)) {
                                if (
                                    f.getValue().equalsIgnoreCase('CARDIOVASCULAR DISEASE') ||      // CARDIOVASCULAR DISEASE f.getValue().equalsIgnoreCase('Cardiovascular Pathologist') ||
                                                                                                    // f.getValue().equalsIgnoreCase('Internal Medicine') ||
                                    f.getValue().equalsIgnoreCase('TECHNOLOGIST - LAB') ||          // TECHNOLOGIST - LAB f.getValue().equalsIgnoreCase('Medical Lab Technologist') ||
                                    //f.getValue().equalsIgnoreCase('PEDIATRIC SURGERY') ||           // PEDIATRIC SURGERY f.getValue().equalsIgnoreCase('Pediatric Surgeon') ||
                                                                                                    // f.getValue().equalsIgnoreCase('Pharmacist') ||
                                                                                                    // TECHNOLOGIST - LAB f.getValue().equalsIgnoreCase('Preoperative Blood Management Technician') ||
                                    f.getValue().equalsIgnoreCase('TECHNOLOGIST - SURGICAL')        // TECHNOLOGIST - SURGICAL f.getValue().equalsIgnoreCase('Surgical Assistant') ||
                                                                                                    // TECHNOLOGIST - SURGICAL f.getValue().equalsIgnoreCase('Surgical Scrub Technician') ||
                                                                                                    // TECHNOLOGIST - SURGICAL f.getValue().equalsIgnoreCase('Surgical Technician') ||
                                                                                                    // TECHNOLOGIST - SURGICAL f.getValue().equalsIgnoreCase('Training Tech')
                                    ) { continue; }
                            }
                            options.put(f.getValue(), f.getValue());
                        }                           
                    }
                }           

                valueMap.put(fn, options);
            }
        }

        return valueMap;
    }

    public static Map<string, string> fetchTransferOfValueOptions(string objName) {
        Map<string, string> options = new Map<string, string>();

        Schema.DescribeFieldResult fr;

        if (objName.equalsIgnoreCase('Campaign_Incidental__c')) {
            fr = Campaign_Incidental__c.Transfer_of_Value__c.getDescribe();
        } else if (objName.equalsIgnoreCase('Campaign_Proctor_Fee__c')) {
            fr = Campaign_Proctor_Fee__c.Transfer_of_Value__c.getDescribe();
        }
        
        if (fr != null) {
            List<Schema.PicklistEntry> ple = fr.getPicklistValues();

            for(Schema.PicklistEntry f :ple) {
                options.put(f.getValue(), f.getValue());
            }                                       
        }

        return options;
    }

    public static Map<string, string> fetchCampaignTechniques() {
        Map<string, string> options = new Map<string, string>();

        Schema.DescribeFieldResult fr = Campaign.Technique__c.getDescribe();

        if (fr != null) {
            List<Schema.PicklistEntry> ple = fr.getPicklistValues();

            for(Schema.PicklistEntry f :ple) {
                options.put(f.getValue(), f.getValue());
            }                                       
        }

        return options;
    }

    public static Map<string, string> fetchAllStateOptions() {
        Map<string, string> options = new Map<string, string>();

        Schema.DescribeFieldResult fr = Licensed_State__c.State_Abbreviation__c.getDescribe();
        List<Schema.PicklistEntry> ple = fr.getPicklistValues();

        for(Schema.PicklistEntry f :ple) {
            options.put(f.getValue(), f.getValue());
        }                               

        return options;
    }

    public static Map<string, string> fetchLicensedStates(string regionNames) {
        List<Licensed_State__c> states = new List<Licensed_State__c>();
        Map<string, string> results = new Map<string, string>();        

        string qry = 'SELECT State_Abbreviation__c, Display_Name__c FROM Licensed_State__c ';

        if (!string.isBlank(regionNames)) {
            List<string> licensedRegions = regionNames.split(';');

            qry += 'WHERE Licensed_Region__r.Name IN :licensedRegions ';
        }

        qry += 'ORDER BY Name';

        states = Database.query(qry);   

        for(Licensed_State__c r :states) {
            results.put(r.State_Abbreviation__c, r.Display_Name__c);
        }   

        return results;
    }

    public static List<string> fetchProductCategories(string divisionName) {
        List<string> results = new List<string>();

        if (!string.isBlank(divisionName)) {
            PicklistUtil util = new PicklistUtil();
            Map<string, List<string>> dependencies = util.getDependentOptions('Division__c', 'Division__c', 'Product_Category__c');

            for(string key :dependencies.keySet()) {
                if(divisionName.equalsIgnoreCase(key)) {
                    for(string item :dependencies.get(key)) {
                        results.add(item);
                    }               
                }
            }
        } else {
            Schema.DescribeFieldResult fr = Division__c.Product_Category__c.getDescribe();
            List<Schema.PicklistEntry> ple = fr.getPicklistValues();

            for(Schema.PicklistEntry f :ple) {
                results.add(f.getValue());
            }
        }

        return results;
    }

    public Map<string, List<string>> getDependentOptions(string objName, string controllingFieldName, string dependentFieldName){
        Map<string,List<string>> results = new Map<string,List<string>>();
        //get the string to sobject global map
        Map<string,Schema.SObjectType> globalMap = Schema.getGlobalDescribe();
        if (!globalMap.containsKey(objName)) { return results; }
        //get the type being dealt with
        Schema.SObjectType pType = globalMap.get(objName);
        Map<string, Schema.SObjectField> fieldMap = pType.getDescribe().fields.getMap();
        //verify field names
        if (!fieldMap.containsKey(controllingFieldName) || !fieldMap.containsKey(dependentFieldName)) { return results; }
        //get the control values   
        List<Schema.PicklistEntry> ctrl_ple = fieldMap.get(controllingFieldName).getDescribe().getPicklistValues();
        //get the dependent values
        List<Schema.PicklistEntry> dep_ple = fieldMap.get(dependentFieldName).getDescribe().getPicklistValues();
        //iterate through the values and get the ones valid for the controlling field name
        PicklistUtil.Bitset objBitSet = new PicklistUtil.Bitset();
        //set up the results
        for(integer controllingIndex = 0; controllingIndex<ctrl_ple.size(); controllingIndex++){            
            //get the pointer to the entry
            Schema.PicklistEntry ctrl_entry = ctrl_ple[controllingIndex];
            //get the label
            string controllingLabel = ctrl_entry.getLabel();
            //create the entry with the label
            results.put(controllingLabel,new List<string>());
        }
        //cater for null and empty
         results.put('', new List<string>());
         results.put(null, new List<string>());
        //check the dependent values
        for(integer dependentIndex = 0; dependentIndex < dep_ple.size(); dependentIndex++){         
            //get the pointer to the dependent index
            Schema.PicklistEntry dep_entry = dep_ple[dependentIndex];
            //get the valid for
            string entryStructure = JSON.serialize(dep_entry);                
            PicklistUtil.TPicklistEntry depPicklistEntry = (PicklistUtil.TPicklistEntry)JSON.deserialize(entryStructure, PicklistUtil.TPicklistEntry.class);
            //if valid for is empty, skip
            if (depPicklistEntry.validFor == null || depPicklistEntry.validFor == '') { continue; }
            //iterate through the controlling values
            for(integer controllingIndex = 0; controllingIndex < ctrl_ple.size(); controllingIndex++){    
                if (objBitSet.testBit(depPicklistEntry.validFor, controllingIndex)){                    
                    //get the label
                    string controllingLabel = ctrl_ple[controllingIndex].getLabel();
                    results.get(controllingLabel).add(depPicklistEntry.label);
                }
            }
        } 
        return results;
    }

    //Credit to:  http://titancronus.com/blog/2014/05/01/salesforce-acquiring-dependent-picklists-in-apex/
    private class Bitset {
        public Map<string, integer> AlphaNumCharCodes { get; set; }
        public Map<string, integer> Base64CharCodes { get; set; }

        public Bitset() {
            LoadCharCodes();
        }

        //Method loads the char codes
        private void LoadCharCodes() {
            AlphaNumCharCodes = new Map<string, integer> {
                'A' => 65, 'B' => 66, 'C' => 67, 'D' => 68, 'E' => 69, 'F' => 70, 'G' => 71, 'H' => 72, 'I' => 73, 'J' => 74,
                'K' => 75, 'L' => 76, 'M' => 77, 'N' => 78, 'O' => 79, 'P' => 80, 'Q' => 81, 'R' => 82, 'S' => 83, 'T' => 84,
                'U' => 85, 'V' => 86, 'W' => 87, 'X' => 88, 'Y' => 89, 'Z' => 90
            };
            Base64CharCodes = new Map<string, integer> ();
            //lower case
            Set<string> pUpperCase = AlphaNumCharCodes.keySet();
            for (string pKey : pUpperCase) {
                //the difference between upper case and lower case is 32
                AlphaNumCharCodes.put(pKey.toLowerCase(), AlphaNumCharCodes.get(pKey) + 32);
                //Base 64 alpha starts from 0 (The ascii charcodes started from 65)
                Base64CharCodes.put(pKey, AlphaNumCharCodes.get(pKey) - 65);
                Base64CharCodes.put(pKey.toLowerCase(), AlphaNumCharCodes.get(pKey) - (65) + 26);
            }
            //numerics
            for (integer i = 0; i <= 9; i++) {
                AlphaNumCharCodes.put(string.valueOf(i), i + 48);
                //base 64 numeric starts from 52
                Base64CharCodes.put(string.valueOf(i), i + 52);
            }
        }

        public boolean testBit(string pValidFor, integer n) {
            //the list of bytes
            List<integer> pBytes = new List<integer> ();
            //multiply by 6 since base 64 uses 6 bits
            integer bytesBeingUsed = (pValidFor.length() * 6) / 8;
            //will be used to hold the full decimal value
            integer pFullValue = 0;

            //must be more than 1 byte
            if (bytesBeingUsed <= 1) { return false; }


            //calculate the target bit for comparison
            integer bit = 7 - (Math.mod(n, 8));
            //calculate the octet that has in the target bit
            integer targetOctet = (bytesBeingUsed - 1) - (n > > bytesBeingUsed);
            //the number of bits to shift by until we find the bit to compare for true or false
            integer shiftBits = (targetOctet * 8) + bit;
            //get the base64bytes
            for (integer i = 0; i<pValidFor.length(); i++) {
                //get current character value
                pBytes.Add((Base64CharCodes.get((pValidFor.Substring(i, i + 1)))));
            }
            //calculate the full decimal value
            for (integer i = 0; i<pBytes.size(); i++) {
                integer pShiftAmount = (pBytes.size() - (i + 1)) * 6; //used to shift by a factor 6 bits to get the value
                pFullValue = pFullValue + (pBytes[i] <<(pShiftAmount));
            }

            //& is to set the same set of bits for testing
            //shift to the bit which will dictate true or false
            integer tBitVal = ((integer) (Math.Pow(2, shiftBits)) & pFullValue)>> shiftBits;
            return tBitVal == 1;
        }
    }


    /*
     * @Summary: Entity to represent a json version of a picklist entry
     * so that the validFor property becomes exposed
     */
    private class TPicklistEntry {
        public string active { get; set; }
        public string defaultValue { get; set; }
        public string label { get; set; }
        public string value { get; set; }
        public string validFor { get; set; }

        public TPicklistEntry() {

        }
    }
}