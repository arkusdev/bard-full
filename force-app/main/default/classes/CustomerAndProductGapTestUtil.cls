@isTest
public class CustomerAndProductGapTestUtil {
	public static void createData () {
        Profile adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        UserRole dmRole = [SELECT Id FROM UserRole WHERE Name = 'South Central - FSO - DM' LIMIT 1];

		List<AggregateResult> ars = [SELECT MAX(Territory__c) max FROM User];  
        String territoryN = ars.size() > 0 ? String.valueOf(Integer.valueOf(ars[0].get('max'))+1) : '001';

        User dm = new User(LastName = 'TestDM',
                           Alias = 'RM tse',
                           Email = 'test.sales.executive@perdaysales.com',
                           Username = 'test.sales.executive@perdaysales.com',
                           Territory__c = '001',
                           UserRoleId = dmRole.Id
                          );
        List<User> users = new List<User>();
        users.add(dm);
        for (User u : users) {
            u.ProfileId = adminProfile.id;
            u.TimeZoneSidKey = 'GMT';
            u.LanguageLocaleKey = 'en_US';
            u.EmailEncodingKey = 'UTF-8';
            u.LocaleSidKey = 'en_US';
            u.isActive = true;
            u.Territory__c = territoryN;

            Integer nextTN = Integer.valueOf(territoryN) + 1;
            if (nextTN < 10) territoryN = '00' + String.valueOf(nextTN);
            if (nextTN < 100) territoryN = '0' + String.valueOf(nextTN);
            else territoryN = String.valueOf(nextTN);
        }
        insert users;

        Date invoiceDate = Date.today().addDays(-1);
        String dateName = ((Datetime)invoiceDate).formatGmt('E');
        /*while (dateName.equals('Sat') || dateName.equals('Sun')) {
            invoiceDate = invoiceDate.addDays(-1);
            dateName = ((Datetime)invoiceDate).formatGmt('E');
        }*/
        Boolean isSameMonth = invoiceDate.month() == Date.today().month();
        RecordType irsaRT = [SELECT Id FROM RecordType WHERE Name = 'IRSA' AND SObjectType = 'Sales__c' LIMIT 1];
        RecordType shadRT = [SELECT Id FROM RecordType WHERE Name = 'SHAD' AND SObjectType = 'Sales__c' LIMIT 1];

        System.runAs(dm) {
            Account acc1 = new Account(Name='TestAccount1');
            acc1.Ship_to_ID__c = 'ThIsIsTeStAcCoUnT1';
            acc1.BillingCity = 'City';
            insert acc1;

            List<Sales__c> dmPair = new List<Sales__c>();

            Sales__c thisYearCustomer = new Sales__c();
            thisYearCustomer.Sales__c = 100;
            thisYearCustomer.OwnerId = dm.Id;
            thisYearCustomer.RecordTypeId = isSameMonth ? irsaRT.Id : shadRT.Id;
            thisYearCustomer.Invoice_Date__c = invoiceDate;
            thisYearCustomer.Account__c = acc1.Id;
            thisYearCustomer.CAT__c = 'CAT__c1';
            thisYearCustomer.Product_Group__c = 'Product_Group1';
            thisYearCustomer.Reporting_Product_Group__c = 'Reporting_Product_Group1';

            Sales__c lastYearCustomer = new Sales__c();
            lastYearCustomer.Sales__c = 100;
            lastYearCustomer.OwnerId = dm.Id;
            lastYearCustomer.RecordTypeId = shadRT.Id;
            lastYearCustomer.Invoice_Date__c = invoiceDate.addYears(-1);
            lastYearCustomer.Account__c = acc1.Id;
            lastYearCustomer.CAT__c = 'CAT__c1';
            lastYearCustomer.Product_Group__c = 'Product_Group1';
            lastYearCustomer.Reporting_Product_Group__c = 'Reporting_Product_Group1';
            
            Sales__c thisYearProduct = new Sales__c();
            thisYearProduct.Sales__c = 100;
            thisYearProduct.OwnerId = dm.Id;
            thisYearProduct.RecordTypeId = isSameMonth ? irsaRT.Id : shadRT.Id;
            thisYearProduct.Invoice_Date__c = invoiceDate;
            thisYearProduct.CAT__c = 'CAT__c1';
            thisYearProduct.Product_Group__c = 'Product_Group1';
            thisYearProduct.Reporting_Product_Group__c = 'Reporting_Product_Group1';

            Sales__c lastYearProduct = new Sales__c();
            lastYearProduct.Sales__c = 100;
            lastYearProduct.OwnerId = dm.Id;
            lastYearProduct.RecordTypeId = shadRT.Id;
            lastYearProduct.Invoice_Date__c = invoiceDate.addYears(-1);
            lastYearProduct.CAT__c = 'CAT__c1';
            lastYearProduct.Product_Group__c = 'Product_Group1';
            lastYearProduct.Reporting_Product_Group__c = 'Reporting_Product_Group1';


            dmPair.add(thisYearCustomer);
            dmPair.add(lastYearCustomer);
            dmPair.add(thisYearProduct);
            dmPair.add(lastYearProduct);
            insert dmPair;
        }
    }
    public static User getDMUser () {
        return [SELECT Id FROM User WHERE LastName = 'TestDM' LIMIT 1];
    }
}