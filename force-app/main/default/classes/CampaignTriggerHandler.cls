public class CampaignTriggerHandler extends TriggerHandler {
	/* overrides */
	protected override void beforeUpdate() {
		validateAvailableSeats();
	}

	protected override void beforeDelete() {
		deleteCampaignUsers();
		deleteCampaignDocs();
	}

	private Set<Id> getCampaignIds() {
		List<Campaign> campaigns = (List<Campaign>)(trigger.isDelete ? trigger.old : trigger.new);

		Set<Id> campaignIds = new Set<Id>();

		for(Campaign item : campaigns) {
			campaignIds.add(item.Id);
		}				

		return campaignIds;
	}

	/* private methods */
	private void validateAvailableSeats() {
		Map<Id, Campaign> newMap = (Map<Id, Campaign>)trigger.newMap;
		Map<Id, Campaign> oldMap = (Map<Id, Campaign>)trigger.oldMap;

		Map<Id, Campaign> campaignMap = new Map<Id, Campaign>();

		for(Id cid :newMap.keySet()) {
			if (oldMap.containsKey(cid) && 
				oldMap.get(cid).Available_Seats__c > newMap.get(cid).Available_Seats__c) {
				campaignMap.put(cid, newMap.get(cid));		
			} 
		}

		if (!campaignMap.isEmpty()) {
			List<AggregateResult> results = [
				SELECT CampaignId, COUNT(Id) AttendeeCount
				FROM CampaignMember
				WHERE CampaignId IN :campaignMap.keySet()
				AND Type__c = 'Attendee'
				GROUP BY CampaignId
			];

			if (!results.isEmpty()) {
				for(AggregateResult ar :results) {
					Id campaignId = (Id)ar.get('CampaignId');
					integer attendeeCount = (integer)ar.get('AttendeeCount');

					if(campaignMap.containsKey(campaignId) &&
					   campaignMap.get(campaignId).Available_Seats__c < attendeeCount) {
						string errorMessage = 'There ' + (attendeeCount > 1 ? 'are ' : 'is ') + string.valueOf(attendeeCount) + 
											  ' Attendees associated this course.  Unable to update the Available Seats to a value less than number of Attendees.';

						campaignMap.get(campaignId).Available_Seats__c.addError(errorMessage);
					}
				}
			}
		}
	}

	private void deleteCampaignUsers() {
		Set<Id> campaignIds = getCampaignIds();

		if(!campaignIds.isEmpty()) {
			List<Campaign_User__c> users = [
				SELECT Id
				FROM Campaign_User__c
				WHERE Campaign__c IN :campaignIds
			];

			if (!users.isEmpty()) {
				delete users;
			}
		}		
	}

	private void deleteCampaignDocs() {
		Set<Id> campaignIds = getCampaignIds();

		if(!campaignIds.isEmpty()) {
			List<Campaign_Document__c> docs = [
				SELECT Id
				FROM Campaign_Document__c
				WHERE Campaign__c IN :campaignIds
			];

			if (!docs.isEmpty()) {
				delete docs;
			}
		}		
	}
}