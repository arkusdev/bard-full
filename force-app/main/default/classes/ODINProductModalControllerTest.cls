@IsTest
private class ODINProductModalControllerTest {

    static testMethod void GetProductTypesTest() {
        Test.startTest();
            List<String> typeList = ODINProductModalController.GetProductTypes();
        Test.stopTest();

        system.assertEquals(Proposed_Product__c.Type__c.getDescribe().getPicklistValues().size(), typeList.size());
    }

    static testMethod void GetProductsTest() {
        Project__c project = new Project__c();
        project.Name = 'Test';
        insert project;

        Proposed_Product__c product = new Proposed_Product__c();
        product.Name = 'Test';
        product.Project__c = project.Id;
        insert product;

        RecordType recordType = [select Id 
                                from RecordType 
                                where SObjectType = 'Financial__c' 
                                and RecordType.DeveloperName = 'Product'];  

        List<Financial__c> financialList = new List<Financial__c>();
        Financial__c fin1 = new Financial__c();
        fin1.Project__c = project.Id;
        fin1.Year__c = String.valueOf(Date.today().year());
        fin1.RecordTypeId = recordType.Id;
        fin1.Proposed_Product__c = product.Id;
        insert fin1;

        Test.startTest();
            List<Proposed_Product__c> ppList = ODINProductModalController.GetProducts(project.Id);
        Test.stopTest();

        system.assertEquals(1, ppList.size());
        system.assertEquals(product.Id, ppList[0].Id);
    }

    static testMethod void GetGetFinancialsTest() {
        Project__c project = new Project__c();
        project.Name = 'Test';
        insert project;

        Proposed_Product__c product = new Proposed_Product__c();
        product.Name = 'Test';
        product.Project__c = project.Id;
        insert product;

        RecordType recordType = [select Id 
                                from RecordType 
                                where SObjectType = 'Financial__c' 
                                and RecordType.DeveloperName = 'Product'];
        List<Financial__c> finList = new List<Financial__c>();
        for (Integer i = 0; i < 15; i++) {
            Financial__c fin1 = new Financial__c();
            fin1.Project__c = project.Id;
            fin1.Year__c = String.valueOf(Date.today().year() + i);
            fin1.RecordTypeId = recordType.Id;
            fin1.Proposed_Product__c = product.Id;
            finList.add(fin1);
        } 
        insert finList;

        Test.startTest();
            ODINProductModalController.FinancialsWrapper wrapper = ODINProductModalController.GetFinancials(project.Id, product.Id);
        Test.stopTest();
        
        system.assertEquals(product.Name, wrapper.product.Name);
        system.assertEquals(10, wrapper.financials.size());
    } 
    
    static testMethod void CreateProductTest() {
        Project__c project = new Project__c();
        project.Name = 'Test';
        insert project;

        Financial__c sample = new Financial__c();
        sample.Estimated_Unit_Cost__c = 10;
        sample.Cannibalized_Unit_Cost__c = 10;
        sample.Unit_Growth__c = 10;
        sample.Cannibalized_Unit_Growth__c = 10;

        String searchText = 'Test';

        String type = Proposed_Product__c.Type__c.getDescribe().getPicklistValues()[0].getLabel();

        Test.startTest();
            ODINProductModalController.FinancialsWrapper wrapper = ODINProductModalController.CreateProduct(project.Id, searchText, type, sample);
        Test.stopTest();

        system.assertEquals(10, wrapper.years.size());
        for (Integer i = 0; i < 10; i++)
            system.assertEquals(String.valueOf(Date.today().addYears(i).year()), wrapper.years[i]);
        
        system.assertEquals(10, wrapper.financials.size());

        for (Integer i = 0; i < 10; i++) {
            system.assertEquals(String.valueOf(Date.today().addYears(i).year()), wrapper.financials[i].Year__c);
            system.assertEquals(project.Id, wrapper.financials[i].Project__c);
        }

        system.assertEquals(project.Id, wrapper.product.Project__c);
        system.assertEquals(searchText, wrapper.product.Name);
    }

    static testMethod void CreateProduct2Test() {
        Date nextYear = Date.today().addYears(1);

        Project__c project = new Project__c();
        project.Name = 'Test';
        project.Launch_Year__c = String.valueOf(nextYear.year());
        insert project;

        Financial__c sample = new Financial__c();
        sample.Estimated_Unit_Cost__c = 10;
        sample.Cannibalized_Unit_Cost__c = 10;
        sample.Unit_Growth__c = 10;
        sample.Cannibalized_Unit_Growth__c = 10;

        String searchText = 'Test';

        String type = Proposed_Product__c.Type__c.getDescribe().getPicklistValues()[0].getLabel();

        Test.startTest();
            ODINProductModalController.FinancialsWrapper wrapper = ODINProductModalController.CreateProduct(project.Id, searchText, type, sample);
        Test.stopTest();

        system.assertEquals(10, wrapper.years.size());
        
        for (Integer i = 0; i < 10; i++)
            system.assertEquals(String.valueOf(nextYear.addYears(i).year()), wrapper.years[i]);
        
        system.assertEquals(10, wrapper.financials.size());

        for (Integer i = 0; i < 10; i++) {
            system.assertEquals(String.valueOf(nextYear.addYears(i).year()), wrapper.financials[i].Year__c);
            system.assertEquals(project.Id, wrapper.financials[i].Project__c);
        }

        system.assertEquals(project.Id, wrapper.product.Project__c);
        system.assertEquals(searchText, wrapper.product.Name);
    }

    static testMethod void SaveFinancialsTest() {
        Project__c project = new Project__c();
        project.Name = 'Test';
        insert project;

        Proposed_Product__c product = new Proposed_Product__c();
        product.Name = 'Test';
        product.Project__c = project.Id;

        RecordType recordType = [select Id 
                                from RecordType 
                                where SObjectType = 'Financial__c' 
                                and RecordType.DeveloperName = 'Product'];  

        List<Financial__c> financialList = new List<Financial__c>();
        Financial__c fin1 = new Financial__c();
        fin1.Project__c = project.Id;
        fin1.Year__c = '2019';
        fin1.RecordTypeId = recordType.Id;
        financialList.add(fin1);

        Financial__c fin2 = new Financial__c();
        fin2.Project__c = project.Id;
        fin2.Year__c = '2020';
        fin2.RecordTypeId = recordType.Id;
        financialList.add(fin2);

        Test.startTest();
            ODINProductModalController.SaveFinancials(financialList, product);
        Test.stopTest();

        List<Proposed_Product__c> prodList = [select Id 
                                                from Proposed_Product__c 
                                                where Project__c =: project.Id 
                                                and Name =: product.Name];
        system.assertEquals(1, prodList.size());
        Integer finCount = [select count() 
                            from Financial__c 
                            where Project__c =: project.Id 
                            and Proposed_Product__c =: prodList[0].Id 
                            and RecordTypeId =: recordType.Id];
        system.assertEquals(2, finCount);
    }

    static testMethod void SaveFinancialsPlus5Test() {
        Project__c project = new Project__c();
        project.Name = 'Test';
        insert project;

        Proposed_Product__c product = new Proposed_Product__c();
        product.Name = 'Test';
        product.Project__c = project.Id;

        RecordType recordType = [select Id 
                                from RecordType 
                                where SObjectType = 'Financial__c' 
                                and RecordType.DeveloperName = 'Product'];  

        List<Financial__c> financialList = new List<Financial__c>();
        Financial__c fin1 = new Financial__c();
        fin1.Project__c = project.Id;
        fin1.Year__c = '2019';
        fin1.RecordTypeId = recordType.Id;
        fin1.Estimated_Unit_Cost__c = 10;
        fin1.Cannibalized_Unit_Cost__c = 10;
        fin1.Year_Sequence__c = 1;
        financialList.add(fin1);

        Test.startTest();
            ODINProductModalController.SaveFinancials(financialList, product);
        Test.stopTest();

        List<Proposed_Product__c> prodList = [select Id 
                                                from Proposed_Product__c 
                                                where Project__c =: project.Id 
                                                and Name =: product.Name];
        system.assertEquals(1, prodList.size());
        Integer finCount = [select count() 
                            from Financial__c 
                            where Project__c =: project.Id 
                            and Proposed_Product__c =: prodList[0].Id 
                            and RecordTypeId =: recordType.Id];
        system.assertEquals(6, finCount);
    }

    static testMethod void UpdateFinancialsTest() {
        Project__c project = new Project__c();
        project.Name = 'Test';
        insert project;

        Proposed_Product__c product = new Proposed_Product__c();
        product.Name = 'Test';
        product.Project__c = project.Id;
        insert product;

        RecordType recordType = [select Id 
                                from RecordType 
                                where SObjectType = 'Financial__c' 
                                and RecordType.DeveloperName = 'Product'];
        
        Financial__c fin1 = new Financial__c();
        fin1.Project__c = project.Id;
        fin1.Year__c = String.valueOf(Date.today().year());
        fin1.RecordTypeId = recordType.Id;
        fin1.Proposed_Product__c = product.Id;
        insert fin1;

        Test.startTest();
            Integer value = 20;
            fin1.Cannibalized_Unit_Cost__c = value;
            ODINProductModalController.UpdateFinancials(new List<Financial__c>{fin1});
        Test.stopTest();

        fin1 = [select Cannibalized_Unit_Cost__c from Financial__c where Id =: fin1.Id];
        
        system.assertEquals(value, fin1.Cannibalized_Unit_Cost__c);
    } 
}