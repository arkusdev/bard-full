public with sharing class ODINGlobalFinancialWizzardController {
    
    @AuraEnabled
    public static ProjectInfo GetProjectInfo(String recordId) {
        ProjectInfo info = new ProjectInfo();
        
        Project__c project = [select    Launch_Quarter__c, 
                                        Launch_Year__c,
                                        (
                                            select Id
                                            from Proposed_Products__r
                                            LIMIT 1
                                        ) ,
                                        (
                                            select Id, Region__c, Country__c, LastModifiedDate
                                            from Market_Opportunities__r
                                            where RecordType.DeveloperName = 'Global_Financial'
                                            order by LastModifiedDate desc
                                        )
                                from Project__c 
                                where Id =: recordId];
        
        if (project.Proposed_Products__r.size() > 0)
            info.hasProduct = true;
        
        info.quarter = project.Launch_Quarter__c;
        info.year = project.Launch_Year__c;
        info.regionsLabels = ODINUtils.GetRegionsLabels(project.Market_Opportunities__r);
        
        for (Financial__c financial : project.Market_Opportunities__r) {
            String country = financial.Country__c;
            if (!info.countryHasFinancials.containsKey(country)){
                info.countryHasFinancials.put(country, true);
            }
        }

        return info;
    }


    @AuraEnabled
    public static InterestWYear GetInterestWYear(String recordId, String country) {
        InterestWYear iwy = new InterestWYear();
        
        List<Interest__c> interestList = [select    Level__c,
                                                    Country__c,
                                                    Region__c,
                                                    No_Interest_Reason__c,
                                                    No_Interest_Reason_Text__c
                                            from Interest__c
                                            where Project__c =: recordId
                                            and Country__c =: country
                                            and Country__c != null
                                            LIMIT 1];
        if (interestList.size() == 1) 
            iwy.interest = interestList[0];

        String year;
        List<Financial__c> financialList = [select  Year__c,
                                                    Launch_Quarter__c 
                                            from Financial__c 
                                            where Project__c =: recordId 
                                            and Country__c =: country
                                            and Country__c != null
                                            and RecordType.DeveloperName = 'Global_Financial'
                                            ORDER BY Year__c ASC 
                                            LIMIT 1];
        if (!financialList.isEmpty()) {
            iwy.year    = financialList[0].Year__c;
            iwy.quarter = financialList[0].Launch_Quarter__c;
        }

        return iwy;
    }

    @AuraEnabled
    public static List<Financial> GetFinancialInfo(String recordId, String country, Boolean fromViewAll) {

        List<Financial> financialList = new List<Financial>();

        Map<Id, Proposed_Product__c> productMap = new Map<Id, Proposed_Product__c>([select Name 
                                                                                    from Proposed_Product__c 
                                                                                    where Project__c =: recordId]);
        Map<Id, Financial__c> productHasFinancialMap = new Map<Id, Financial__c>();
        for (Financial__c financial : [select   Proposed_Product__c, Units__c, Unit_Growth__c, ASP__c, Non_Unit_Growth__c,
                                                Year__c, ASP_Growth__c, Cannibalized_Units__c, Cannibalized_ASP__c, Non_Rev_Unit__c
                                        from Financial__c 
                                        where Project__c =: recordId 
                                        and Country__c =: country 
                                        and Proposed_Product__c IN: productMap.values()
                                        and RecordType.DeveloperName = 'Global_Financial'
                                        order by Year__c ASC]) {
            if (!productHasFinancialMap.containsKey(financial.Proposed_Product__c)){
                productHasFinancialMap.put(financial.Proposed_Product__c, financial);
            }
        }

        for (Proposed_Product__c product : productMap.values()) {
            
            if (!productHasFinancialMap.containsKey(product.Id)) {
                Financial__c financial          = new Financial__c();
                financial.Proposed_Product__c   = product.Id;
                financial.Project__c            = recordId;
                financial.Country__c            = country;

                Financial innerFin = new Financial();
                innerFin.productName = product.Name;
                innerFin.record = financial;
                innerFin.hasData = false;
                financialList.add(innerFin);
            }else {
                Financial innerFin = new Financial();
                innerFin.productName = product.Name;
                innerFin.hasData = true;
                innerFin.record = productHasFinancialMap.get(product.Id);
                if (!fromViewAll) financialList.add(innerFin);
            }
        }
        return financialList;
    }

    @AuraEnabled
    public static FinancialWrapper CreateFinancialInfo(Financial__c sampleFinancial, List<Financial__c> financials) {

        FinancialWrapper wrapper = new FinancialWrapper();
        wrapper.financialsProjectPerYearList = new List<List<Financial>>();
        wrapper.years = new List<String>();

        Map<Id, Proposed_Product__c> productMap = new Map<Id, Proposed_Product__c>([select Name 
                                                                                    from Proposed_Product__c 
                                                                                    where Project__c =: sampleFinancial.Project__c]);
        
        Map<String, Map<Integer, Financial__c>> yearProductMap = new Map<String, Map<Integer, Financial__c>>();
        for (Financial__c financial : [select   Year__c,
                                                Proposed_Product__c 
                                        from Financial__c 
                                        where Project__c =: sampleFinancial.Project__c 
                                        and Proposed_Product__c IN: productMap.values()
                                        and RecordType.DeveloperName = 'Product'
                                        ORDER BY Proposed_Product__c ASC, Year__c ASC]) {
            if (yearProductMap.containsKey(financial.Proposed_Product__c)) {
                yearProductMap.get(financial.Proposed_Product__c).put(Integer.valueOf(financial.Year__c), financial);
            } else {
                yearProductMap.put(financial.Proposed_Product__c, new Map<Integer, Financial__c>{Integer.valueOf(financial.Year__c) => financial});
            }                               
        }                                                                                    

        RecordType recordType = [select Id 
                                from RecordType 
                                where SObjectType = 'Financial__c' 
                                and RecordType.DeveloperName = 'Global_Financial'];                                                                                    

        for (Financial__c financial : financials) {

            Double asp          = financial.ASP__c;
            Double units        = financial.Units__c;
            Double cannASP      = financial.Cannibalized_ASP__c;
            Double cannUnits    = financial.Cannibalized_Units__c;
            Double nonRevUnit;

            List<Financial> financialList = new List<Financial>();
            for (Integer i = 0; i < 10; i++) { 

                if (financial.Non_Unit_Growth__c != null) {
                    nonRevUnit = ((units * financial.Non_Unit_Growth__c) / 100);
                }

                Financial__c newFinancial                   = new Financial__c();
                newFinancial.Year__c                        = String.valueOf(Integer.valueOf(sampleFinancial.Year__c) + i);
                newFinancial.Year_Sequence__c               = i + 1;
                newFinancial.Project__c                     = financial.Project__c;
                newFinancial.Proposed_Product__c            = financial.Proposed_Product__c;
                newFinancial.Unit_Growth__c                 = financial.Unit_Growth__c;
                newFinancial.ASP_Growth__c                  = financial.ASP_Growth__c;
                newFinancial.Cannibalized_Unit_Growth__c    = financial.Cannibalized_Unit_Growth__c;
                newFinancial.Country__c                     = sampleFinancial.Country__c;
                newFinancial.Region__c                      = sampleFinancial.Region__c;
                newFinancial.Launch_Quarter__c              = sampleFinancial.Launch_Quarter__c;
                newFinancial.Non_Unit_Growth__c             = financial.Non_Unit_Growth__c;
                newFinancial.RecordTypeId                   = recordType.Id;

                if (yearProductMap.containsKey(financial.Proposed_Product__c) && yearProductMap.get(financial.Proposed_Product__c).containsKey(Integer.valueOf(newFinancial.Year__c)))
                    newFinancial.Product__c = yearProductMap.get(financial.Proposed_Product__c).get(Integer.valueOf(newFinancial.Year__c)).Id;                    

                newFinancial.ASP__c                 = asp;
                newFinancial.Units__c               = units;
                newFinancial.Cannibalized_ASP__c    = cannASP;
                newFinancial.Cannibalized_Units__c  = Integer.valueOf(cannUnits);
                newFinancial.Non_Rev_Unit__c        = Integer.valueOf(nonRevUnit);

                Financial newInner      = new Financial();
                newInner.productName    = productMap.get(financial.Proposed_Product__c).Name;
                newInner.record         = newFinancial;
                newInner.revenue        = newFinancial.ASP__c != null && newFinancial.Units__c != null ? (Integer)(newFinancial.ASP__c * newFinancial.Units__c) : null;
                newInner.totalUnit      = newFinancial.Non_Rev_Unit__c != null && newFinancial.Units__c != null ? (Integer)(newFinancial.Non_Rev_Unit__c + newFinancial.Units__c) : null;

                financialList.add(newInner);
                wrapper.years.add(newFinancial.Year__c);

                if (financial.Unit_Growth__c != null) {
                    units = ((units * financial.Unit_Growth__c) / 100) + units;
                }

                if (financial.ASP_Growth__c != null) {
                    asp = ((asp * financial.ASP_Growth__c) / 100) + asp;
                }

            }
            wrapper.financialsProjectPerYearList.add(financialList);
        }
        
        Map<String, List<Financial>> financialMap = new Map<String, List<Financial>>();
        Set<String> yearSet = new Set<String>();
        Integer count = 0;
        Financial__c previousFinancial;
        for (Financial__c financial : [select   Proposed_Product__c,
                                                Proposed_Product__r.Name,
                                                Year__c,
                                                Product__c,
                                                Project__c,
                                                Units__c,
                                                ASP__c,
                                                Unit_Growth__c,
                                                ASP_Growth__c,
                                                Country__c,
                                                Region__c,
                                                Cannibalized_Unit_Growth__c,
                                                Cannibalized_ASP__c,
                                                Cannibalized_Units__c,
                                                Non_Rev_Unit__c,
                                                Non_Unit_Growth__c
                                        from Financial__c 
                                        where Project__c =: sampleFinancial.Project__c 
                                        and Country__c =: sampleFinancial.Country__c 
                                        and Proposed_Product__c IN: productMap.values()
                                        and RecordType.DeveloperName = 'Global_Financial'
                                        ORDER BY Proposed_Product__c ASC, Year__c ASC]) {
            
            if (previousFinancial != null && previousFinancial.Proposed_Product__c != financial.Proposed_Product__c) 
                count = 0;
            
            financial.Year__c = String.valueOf(Integer.valueOf(sampleFinancial.Year__c) + count);
            count++;
            previousFinancial = financial;

            if (yearProductMap.containsKey(financial.Proposed_Product__c)) {
                if (yearProductMap.get(financial.Proposed_Product__c).containsKey(Integer.valueOf(financial.Year__c))) {
                    financial.Product__c = yearProductMap.get(financial.Proposed_Product__c).get(Integer.valueOf(financial.Year__c)).Id;
                } else {
                    financial.Product__c = null;
                }
            }

            Financial newInner      = new Financial();
            newInner.productName    = financial.Proposed_Product__r.Name;
            newInner.record         = financial;
            newInner.revenue        = financial.ASP__c != null && financial.Units__c != null ? (Integer)(financial.ASP__c * financial.Units__c) : null;
            newInner.totalUnit      = financial.Non_Rev_Unit__c != null && financial.Units__c != null ? (Integer)(financial.Non_Rev_Unit__c + financial.Units__c) : null;

            if (financialMap.containsKey(financial.Proposed_Product__r.Name)) {
                financialMap.get(financial.Proposed_Product__r.Name).add(newInner);
            } else {
                financialMap.put(financial.Proposed_Product__r.Name, new List<Financial>{newInner});
            }
            
            yearSet.add(financial.Year__c);
        }
        
        
        wrapper.financialsProjectPerYearList.addAll(financialMap.values());

        if (wrapper.years.isEmpty())
            wrapper.years = new List<String>(yearSet);
        return wrapper;
    }

    @AuraEnabled
    public static QuestionWrapper GetProjectQuestions(String recordId, String country, String region) {
        QuestionWrapper wrapper     = new QuestionWrapper();
        wrapper.questions           = new List<QuestionWAnswer>();
        wrapper.requiredQuestions   = new List<QuestionWAnswer>();

        for (Question__c question : [select Question__c,
                                            Required__c, 
                                            (select Country__c, 
                                                    Answer__c,
                                                    Question__c 
                                            from Answers__r 
                                            where Country__c =: country
                                            LIMIT 1) 
                                    from Question__c 
                                    where Project__c =: recordId
                                    and Show_In_Global_Financials__c = true]) {
                                        
            QuestionWAnswer qwa = new QuestionWAnswer();
            qwa.question = question;
            if (question.Answers__r.size() == 1) {
                qwa.answer = question.Answers__r[0];
            } else {
                qwa.answer = new Answer__c();
                qwa.answer.Country__c = country;
                qwa.answer.Question__c = question.Id;
                qwa.answer.Region__c = region;
            }
            if (question.Required__c) {
                wrapper.requiredQuestions.add(qwa);
            } else {
                wrapper.questions.add(qwa);
            }
        }
        return wrapper;
    }

    @AuraEnabled
    public static void FinalSave(String financialJson, List<Answer__c> answers, Interest__c interest, Financial__c sampleFinancial) {
        
        SaveInterest(interest);
        
        SaveAnswers(answers);
        FinancialWrapper wrapper;
        try{
            wrapper = (FinancialWrapper)JSON.deserialize(financialJson, FinancialWrapper.class);
        }catch(Exception exc){
            throw new AuraHandledException(System.Label.Max_Integer_Error_Message);
        }
        List<Financial__c> financialList = new List<Financial__c>();
        for (List<Financial> financialsPerYear : wrapper.financialsProjectPerYearList) {
            
            for (Financial financial : financialsPerYear) {
                financial.record.Launch_Quarter__c = sampleFinancial.Launch_Quarter__c;
                financialList.add(financial.record);
            }
        }
        upsert financialList;
    }

    @AuraEnabled
    public static void SaveInterest(Interest__c interest){
        upsert interest;
    }

    private static void SaveAnswers(List<Answer__c> answers) {

        List<Answer__c> answerList = new List<Answer__c>();
        for (Answer__c answer : answers) {
            if (answer.Answer__c != null || answer.Id != null) 
                answerList.add(answer);
        }
        upsert answerList;
    }

    @AuraEnabled
    public static FinancialInfo getFinancialInfoByProjectAndCountry(String recordId, Financial__c financial, String country, String region) {
        QuestionWrapper projectQuestions = GetProjectQuestions(recordId, country, region);
        List<Financial> financialInfo = GetFinancialInfo(recordId, country, true);
        List<Financial__c> financials = new List<Financial__c>();
        for (Financial fin : financialInfo) {
            financials.add(fin.record);
        }
        FinancialWrapper financialWrapper = CreateFinancialInfo(financial, financials);
        InterestWYear interest = GetInterestWYear(recordId, country);
        return new FinancialInfo(financialWrapper, projectQuestions, financialInfo, interest);
    }

    public class Financial {
        @AuraEnabled
        public String       productName;
        @AuraEnabled
        public Integer      revenue;
        @AuraEnabled
        public Integer      totalUnit;
        @AuraEnabled
        public Financial__c record;
        @AuraEnabled
        public Boolean hasData;
    }

    public class FinancialWrapper {
        @AuraEnabled
        public List<String>             years;
        @AuraEnabled
        public List<List<Financial>>    financialsProjectPerYearList;
    }

    public class QuestionWrapper {
        @AuraEnabled
        public List<QuestionWAnswer>    questions;
        @AuraEnabled
        public List<QuestionWAnswer>    requiredQuestions;
    }

    public class QuestionWAnswer {
        @AuraEnabled
        public Question__c  question;
        @AuraEnabled
        public Answer__c    answer;
    }

    public class InterestWYear {
        @AuraEnabled
        public Interest__c  interest;
        @AuraEnabled
        public String       year;
        @AuraEnabled
        public String       quarter;
    }

    public class ProjectInfo {
        @AuraEnabled
        public Boolean hasProduct = false;
        @AuraEnabled
        public String  year;
        @AuraEnabled
        public String  quarter;
        @AuraEnabled
        public Map<String, String>  regionsLabels;
        @AuraEnabled
        public Map<String, Boolean>  countryHasFinancials;
        public ProjectInfo(){
            this.countryHasFinancials = new Map<String,Boolean>();
        }
    }

    public class FinancialInfo {
        @AuraEnabled
        public FinancialWrapper financialWrapper { set; get; }
        @AuraEnabled
        public QuestionWrapper projectQuestions { set; get; }
        @AuraEnabled
        public List<Financial> financialInfo { set; get; }
        @AuraEnabled
        public InterestWYear interest { set; get; }
        public FinancialInfo(FinancialWrapper pFinancialWrapper, QuestionWrapper pProjectQuestions, List<Financial> pFinancialInfo, InterestWYear pInterest){
            financialWrapper = pFinancialWrapper;
            projectQuestions = pProjectQuestions;
            financialInfo = pFinancialInfo;
            interest = pInterest;
        }
    }
}