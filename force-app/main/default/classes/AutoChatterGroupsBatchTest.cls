@isTest
public with sharing class AutoChatterGroupsBatchTest {
    @isTest
    static void createAutoChatterGroupsTest() {
        Project__c newProject = new Project__c();
        newProject.Name = 'Test_Batch';
        insert newProject;

        Test.startTest();
            Database.executeBatch(new AutoChatterGroupsBatch(), 10);
        Test.stopTest();

        System.assertEquals(2, [SELECT Id FROM CollaborationGroup WHERE Name LIKE '%Test_Batch%'].size(), 'No chatter group created');
    }
}