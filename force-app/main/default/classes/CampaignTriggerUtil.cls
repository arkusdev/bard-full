public class CampaignTriggerUtil {
	public static void UpdateCampaignCost(Set<Id> campaignIds) {
		Map<Id, Campaign> campaignMap = new Map<Id, Campaign>([
			SELECT Id, ActualCost, 
				(SELECT Total_Cost__c FROM Campaign_Meals__r),
				(SELECT Cost__c FROM Campaign_Flights__r),
				(SELECT Total_Cost__c FROM Campaign_Transportations__r),
				(SELECT Total_Cost__c FROM Campaign_Incidentals__r),
				(SELECT Total_Cost__c FROM Campaign_Hotels__r),
				(SELECT Total_Cost__c FROM Campaign_Proctor_Fees__r)
			FROM Campaign
			WHERE Id IN :campaignIds
		]);

		for(Id key :campaignMap.keySet()) {
			decimal totalCost = 0;

			for(Campaign_Meal__c item :campaignMap.get(key).Campaign_Meals__r) {
				totalCost += item.Total_Cost__c;
			}

			for(Campaign_Flight__c item :campaignMap.get(key).Campaign_Flights__r) {
				totalCost += item.Cost__c;
			}

			for(Campaign_Transportation__c item :campaignMap.get(key).Campaign_Transportations__r) {
				totalCost += item.Total_Cost__c;
			}

			for(Campaign_Incidental__c item :campaignMap.get(key).Campaign_Incidentals__r) {
				totalCost += item.Total_Cost__c;
			}

			for(Campaign_Hotel__c item :campaignMap.get(key).Campaign_Hotels__r) {
				totalCost += item.Total_Cost__c;
			}

			for(Campaign_Proctor_Fee__c item:campaignMap.get(key).Campaign_Proctor_Fees__r) {
				totalCost += item.Total_Cost__c;
			}

			campaignMap.get(key).ActualCost = totalCost;
		}

		update campaignMap.values();	
	}
}