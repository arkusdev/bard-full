public with sharing class ProductUsedUtils {
	
	public static Boolean STOP_TRIGGER = false;

	public static void UpdateCases(List<Product_Used__c> newPUList) {
		List<Id> caseIds = new List<Id>();
		List<Id> productIds = new List<Id>();
		for (Product_Used__c pu : newPUList) {
			caseIds.add(pu.Case__c);
			productIds.add(pu.Product__c);
		}

		Map<Id, Case> caseMap = new Map<Id, Case>([select   Product_Group_1__c,
															Product_Group_2__c,
															Product_Group_3__c,
															Product_Group_4__c,
															Product_Group_5__c,
															Product_Group_6__c,
															Product_Group_7__c,
															Quantity_1__c,
															Quantity_2__c,
															Quantity_3__c,
															Quantity_4__c,
															Quantity_5__c,
															Quantity_6__c,
															Quantity_7__c
													from Case
													where Id IN: caseIds]);
		Map<Id, Product__c> productMap = new Map<Id, Product__c>([select Name
																	from Product__c
																	where Id IN: productIds]);
		for (Product_Used__c pu : newPUList) {
			if (caseMap.get(pu.Case__c).Product_Group_1__c == productMap.get(pu.Product__c).Name)
				caseMap.get(pu.Case__c).Quantity_1__c = String.valueOf(Integer.valueOf(pu.Quantity__c));
			else if (caseMap.get(pu.Case__c).Product_Group_2__c == productMap.get(pu.Product__c).Name)
				caseMap.get(pu.Case__c).Quantity_2__c = String.valueOf(Integer.valueOf(pu.Quantity__c));
			else if (caseMap.get(pu.Case__c).Product_Group_3__c == productMap.get(pu.Product__c).Name)
				caseMap.get(pu.Case__c).Quantity_3__c = String.valueOf(Integer.valueOf(pu.Quantity__c));
			else if (caseMap.get(pu.Case__c).Product_Group_4__c == productMap.get(pu.Product__c).Name)
				caseMap.get(pu.Case__c).Quantity_4__c = String.valueOf(Integer.valueOf(pu.Quantity__c));
			else if (caseMap.get(pu.Case__c).Product_Group_5__c == productMap.get(pu.Product__c).Name)
				caseMap.get(pu.Case__c).Quantity_5__c = String.valueOf(Integer.valueOf(pu.Quantity__c));
			else if (caseMap.get(pu.Case__c).Product_Group_6__c == productMap.get(pu.Product__c).Name)
				caseMap.get(pu.Case__c).Quantity_6__c = String.valueOf(Integer.valueOf(pu.Quantity__c));
			else if (caseMap.get(pu.Case__c).Product_Group_7__c == productMap.get(pu.Product__c).Name)
				caseMap.get(pu.Case__c).Quantity_7__c = String.valueOf(Integer.valueOf(pu.Quantity__c));
		}
		CaseUtils.STOP_TRIGGER = true;
		update caseMap.values();
		CaseUtils.STOP_TRIGGER = false;
	}

	public static void CleanCases(List<Product_Used__c> oldPUList) {
		List<Id> caseIds = new List<Id>();
		List<Id> productIds = new List<Id>();
		for (Product_Used__c pu : oldPUList) {
			caseIds.add(pu.Case__c);
			productIds.add(pu.Product__c);
		}

		Map<Id, Case> caseMap = new Map<Id, Case>([select   Product_Group_1__c,
															Product_Group_2__c,
															Product_Group_3__c,
															Product_Group_4__c,
															Product_Group_5__c,
															Product_Group_6__c,
															Product_Group_7__c,
															Quantity_1__c,
															Quantity_2__c,
															Quantity_3__c,
															Quantity_4__c,
															Quantity_5__c,
															Quantity_6__c,
															Quantity_7__c
													from Case
													where Id IN: caseIds]);
		Map<Id, Product__c> productMap = new Map<Id, Product__c>([select Name
																	from Product__c
																	where Id IN: productIds]);
		for (Product_Used__c pu : oldPUList) {
			if (caseMap.get(pu.Case__c).Product_Group_1__c == productMap.get(pu.Product__c).Name) {
				caseMap.get(pu.Case__c).Quantity_1__c = null;
				caseMap.get(pu.Case__c).Product_Group_1__c = null;
			} else if (caseMap.get(pu.Case__c).Product_Group_2__c == productMap.get(pu.Product__c).Name) {
				caseMap.get(pu.Case__c).Quantity_2__c = null;
				caseMap.get(pu.Case__c).Product_Group_2__c = null;
			} else if (caseMap.get(pu.Case__c).Product_Group_3__c == productMap.get(pu.Product__c).Name) {
				caseMap.get(pu.Case__c).Quantity_3__c = null;
				caseMap.get(pu.Case__c).Product_Group_3__c = null;
			} else if (caseMap.get(pu.Case__c).Product_Group_4__c == productMap.get(pu.Product__c).Name) {
				caseMap.get(pu.Case__c).Quantity_4__c = null;
				caseMap.get(pu.Case__c).Product_Group_4__c = null;
			} else if (caseMap.get(pu.Case__c).Product_Group_5__c == productMap.get(pu.Product__c).Name) {
				caseMap.get(pu.Case__c).Quantity_5__c = null;
				caseMap.get(pu.Case__c).Product_Group_5__c = null;
			} else if (caseMap.get(pu.Case__c).Product_Group_6__c == productMap.get(pu.Product__c).Name) {
				caseMap.get(pu.Case__c).Quantity_6__c = null;
				caseMap.get(pu.Case__c).Product_Group_6__c = null;
			} else if (caseMap.get(pu.Case__c).Product_Group_7__c == productMap.get(pu.Product__c).Name) {
				caseMap.get(pu.Case__c).Quantity_7__c = null;
				caseMap.get(pu.Case__c).Product_Group_7__c = null;
			}
		}
		CaseUtils.STOP_TRIGGER = true;
		update caseMap.values();
		CaseUtils.STOP_TRIGGER = false;
	}
}