@IsTest
private class ODINMassAssignTaskModalControllerTest {
    
    static testMethod void GetTeamTest() {
        Project__c project = new Project__c();
        project.Name = 'Test_1';
        insert project;

        Project_Role__c role = new Project_Role__c();
        role.User__c = UserInfo.getUserId();
        role.Project__c = project.Id;
        insert role;

        Test.startTest();
            ODINMassAssignTaskModalController.TaskWrapper wrapper = ODINMassAssignTaskModalController.GetTeam(project.Id);
        Test.stopTest();
        
        system.assert(wrapper.task != null);
        system.assertEquals(project.Id, wrapper.task.WhatId);
        system.assertEquals(project.Name, wrapper.project);
        system.assertEquals(1, wrapper.roles.size());
        system.assertEquals(role.Id, wrapper.roles[0].projectRole.Id);
    }

    static testMethod void SaveTest() {
        Project__c project = new Project__c();
        project.Name = 'Test_2';
        insert project;

        Project_Role__c role = new Project_Role__c();
        role.User__c = UserInfo.getUserId();
        role.Project__c = project.Id;
        insert role;

        Task task = new Task();
        task.WhatId = project.Id;
        task.Subject = 'Test';
        task.Description = 'Test';
        task.ActivityDate = Date.today();

        Test.startTest();
            ODINMassAssignTaskModalController.Save(task, new List<Project_Role__c>{role});
        Test.stopTest();

        Integer taskCount = [select count() from Task where WhatId =: project.Id and OwnerId =: UserInfo.getUserId() and ActivityDate =: Date.today()];

        system.assertEquals(1, taskCount);
    }

    static testmethod void getTaskFormInfo(){
        Project__c project = new Project__c();
        project.Name = 'Test_3';
        insert project;

        ODINMassAssignTaskModalController.PreFormInfo preformInfo = ODINMassAssignTaskModalController.getTaskFormInfo(project.Id, 'Marketing');
        System.assert(preformInfo.tasksPreFormInfo.size() >= 3, 'There are missing options for task types');
        System.assert(preformInfo.tasksPreFormInfo[0].subject.contains(project.Name), 'The name of the project wasnt added in the subject');
        System.assert(preformInfo.description != null, 'The description is empty');
    }

    static testmethod void getTableDataTest(){
        List<ODINMassAssignTaskModalController.RowData> rowData = ODINMassAssignTaskModalController.getTableData();
        System.assert(rowData.size() > 0, 'No data was found for the table');
    }
}