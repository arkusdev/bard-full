public class CampaignDocumentTriggerHandler extends TriggerHandler {
/* overrides */
	protected override void afterInsert() {
		insertCampaignAttendeeDocuments();		
	}

	protected override void beforeDelete() {
		deleteCampaignAttendeeDocuments();
	}

	/* private methods */
	private void insertCampaignAttendeeDocuments() { 
		Set<Id> campaignIds = new Set<Id>();

		for(Campaign_Document__c item : (List<Campaign_Document__c>)trigger.new) {
			campaignIds.add(item.Campaign__c);
		}

		if(!campaignIds.isEmpty()) {
			Map<Id, Map<Id, Set<Id>>> campaignMemberMap = new Map<Id, Map<Id, Set<Id>>>();
			Map<Id, Map<Id, Set<Id>>> campaignUserMap = new Map<Id, Map<Id, Set<Id>>>();
			//campaignId, contactId, set<camp document id>

			List<Campaign> campaigns = [
				SELECT Id, 
					(SELECT ContactId FROM CampaignMembers WHERE Type__c = 'Attendee'),
					(SELECT User__c FROM Campaign_Users__r)
				FROM Campaign
				WHERE Id IN :campaignIds
			];

			for(Campaign c :campaigns) {
				Map<Id, Set<Id>> contactMap = new Map<Id, Set<Id>>();
				Map<Id, Set<Id>> userMap = new Map<Id, Set<Id>>();

				for(CampaignMember cm : c.CampaignMembers) {
					Set<Id> docIds = new Set<Id>();

					for(Campaign_Document__c cd :(List<Campaign_Document__c>)trigger.new) { 
						if (c.Id == cd.Campaign__c) {
							if (!docIds.contains(cd.Id)) { docIds.add(cd.Id); }			
						}
					}

					if (!contactMap.containsKey(cm.ContactId)) { contactMap.put(cm.ContactId, docIds); }
				}

				for(Campaign_User__c cm : c.Campaign_Users__r) {
					Set<Id> docIds = new Set<Id>();

					for(Campaign_Document__c cd :(List<Campaign_Document__c>)trigger.new) { 
						if (c.Id == cd.Campaign__c) {
							if (!docIds.contains(cd.Id)) { docIds.add(cd.Id); }			
						}
					}

					if (!userMap.containsKey(cm.User__c)) { userMap.put(cm.User__c, docIds); }
				}

				if (!campaignMemberMap.containsKey(c.Id)) {
					campaignMemberMap.put(c.Id, contactMap);
				}

				if (!campaignUserMap.containsKey(c.Id)) {
					campaignUserMap.put(c.Id, userMap);
				}
			}

			List<Campaign_Attendee_Document__c> cmds = new List<Campaign_Attendee_Document__c>();

			for(Id campaignId :campaignMemberMap.keySet()) {
				for(Id contactId :campaignMemberMap.get(campaignId).keySet()) {
					for(Id docId :campaignMemberMap.get(campaignId).get(contactId)) {
						cmds.add(new Campaign_Attendee_Document__c(
							Campaign_Document__c = docId,
							Contact__c = contactId,
							Received__c = false
						));
					}
				}
			}

			for(Id campaignId :campaignUserMap.keySet()) {
				for(Id userId :campaignUserMap.get(campaignId).keySet()) {
					for(Id docId :campaignUserMap.get(campaignId).get(userId)) {
						cmds.add(new Campaign_Attendee_Document__c(
							Campaign_Document__c = docId,
							User__c = userId,
							Received__c = false
						));
					}
				}
			}

			if (!cmds.isEmpty()) {
				insert cmds;
			}
		}	
	}

	private void deleteCampaignAttendeeDocuments() {
		Set<Id> documentIds = new Set<Id>();

		for(Campaign_Document__c item : (List<Campaign_Document__c>)trigger.old) {
			documentIds.add(item.Id);
		}			

		if(!documentIds.isEmpty()) {
			List<Campaign_Attendee_Document__c> cmds = [
				SELECT Id
				FROM Campaign_Attendee_Document__c
				WHERE Campaign_Document__c IN :documentIds
			];

			if (!cmds.isEmpty()) {
				delete cmds;
			}
		}		
	}
}