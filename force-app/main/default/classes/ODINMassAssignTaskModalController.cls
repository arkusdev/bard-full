public with sharing class ODINMassAssignTaskModalController {
    
    @AuraEnabled
    public static TaskWrapper GetTeam(String recordId){
        
        TaskWrapper wrapper = new TaskWrapper();
        wrapper.task = new Task();
        wrapper.task.WhatId = recordId;

        wrapper.project = [select Name from Project__c where Id =: recordId].Name;
        
        wrapper.roles = new List<Role>();
        
        for (Project_Role__c pr : [select   Role__c,
                                            Region__c, 
                                            User__c,
                                            Country__c,
                                            User__r.Name
                                    from Project_Role__c 
                                    where Project__c =: recordId
                                    order by Region_Text__c ASC, Country__c ASC]) {
            Role role = new Role();
            role.projectRole = pr;
            wrapper.roles.add(role);
        }

        Map<String, String> helpTexts = new Map<String, String>();
        for (ODIN_Assign_Task_Help_Text__mdt helpText : [SELECT MasterLabel, Help_Text__c 
                                                        FROM ODIN_Assign_Task_Help_Text__mdt]) {
            if (!helpTexts.containsKey(helpText.MasterLabel)){
                helpTexts.put(helpText.MasterLabel, helpText.Help_Text__c);
            }
        }
        wrapper.helpTexts = helpTexts;
        
        return wrapper;
    }

    @AuraEnabled
    public static void Save(Task task, List<Project_Role__c> roles) {
        List<Task> taskList = new List<Task>();
        List<String> userId = new List<String>();

        for (Project_Role__c role : roles) {
            Task teamTask = new Task();
            teamTask.Subject = task.Subject;
            teamTask.WhatId = task.WhatId;
            teamTask.ActivityDate = task.ActivityDate;
            teamTask.Description = task.Description;
            teamTask.OwnerId = role.User__c;
            teamTask.Created_From__c = task.Created_From__c;

            taskList.add(teamTask);
            userId.add(role.User__c);

        }
        insert taskList;

        Map<Id, User> userMap = new Map<Id, User>([select Email from User where Id IN: userId]);

        List<Messaging.SingleEmailMessage>  emailsToSend    = new List<Messaging.SingleEmailMessage>();
        for (Task teamTask : taskList) {
            Messaging.SingleEmailMessage taskEmail = new Messaging.SingleEmailMessage();
            taskEmail.setToAddresses(new List<String>{userMap.get(teamTask.OwnerId).Email});
            taskEmail.setSubject('Task Assignment');
            taskEmail.setPlainTextBody('A new Task with Subject \'' + teamTask.Subject + '\' and Id \'' + teamTask.Id + '\' was assigned to you for the related Project ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + task.WhatId);
            emailsToSend.add(taskEmail);
        }
        Messaging.sendEmail(emailsToSend);
    }

    @AuraEnabled
    public static PreFormInfo getTaskFormInfo(String projectId, String team){
        String projectName = [SELECT Id, Name FROM Project__c WHERE Id =: projectId].Name;
        List<TaskPreFormInfo> tasksPreformInfo = new List<TaskPreFormInfo>();
        for (ODIN_Assign_Task_Info__mdt taskInfo : [SELECT Task_Label__c, Subject__c, Order__c 
                                                    FROM ODIN_Assign_Task_Info__mdt
                                                    WHERE Team__c =: team
                                                    ORDER BY Order__c]) {
            String subject = taskInfo.Subject__c != null ? taskInfo.Subject__c.replace('<<Project Name>>', projectName) : '';
            tasksPreformInfo.add(new TaskPreFormInfo(taskInfo.Task_Label__c, subject));
        }
        return new PreFormInfo(tasksPreformInfo, System.Label.ODIN_Assign_Task_Description, team == 'Marketing Access' ? System.Label.ODIN_Mass_Assign_Task_Footer : System.Label.ODIN_Assign_Task_Footer);
    }

    @AuraEnabled
    public static List<RowData> getTableData(){
        List<RowData> data = new List<RowData>();
        try {
            for (ODIN_Assign_Task_Table__mdt tableData :
                [SELECT Id, Global_Financials_Title__c, Global_Market_Assestment_Title__c,
                Project_Type_Description__c, Project_Type_Title__c
                FROM ODIN_Assign_Task_Table__mdt
                ORDER BY Project_Type_Title__c]){
                    data.add(new RowData(tableData));
            }
            return data;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class RowData {
        @AuraEnabled
        public String globalFinancialTitle { set; get; }
        
        @AuraEnabled
        public String globalMarketAssestmentTitle { set; get; }
        
        @AuraEnabled
        public String projectTypeTitle { set; get; }
        
        @AuraEnabled
        public String projectTypeDescription { set; get; }

        public RowData(ODIN_Assign_Task_Table__mdt tableData){
            this.globalFinancialTitle = tableData.Global_Financials_Title__c;
            this.globalMarketAssestmentTitle = tableData.Global_Market_Assestment_Title__c;
            this.projectTypeTitle = tableData.Project_Type_Title__c;
            this.projectTypeDescription = tableData.Project_Type_Description__c;
        }
    }

    public class PreFormInfo {
        @AuraEnabled
        public List<TaskPreFormInfo> tasksPreFormInfo;
        
        @AuraEnabled
        public String description;

        @AuraEnabled
        public String footerInfo;

        public PreFormInfo(List<TaskPreFormInfo> pTasksPreFormInfo, String pDescription, String pFooterInfo){
            this.tasksPreFormInfo = pTasksPreFormInfo;
            this.description = pDescription;
            this.footerInfo = pFooterInfo;
        }
    }

    public class TaskPreFormInfo {
        @AuraEnabled
        public String taskType;

        @AuraEnabled
        public String subject;

        public TaskPreFormInfo(String pTaskType, String pSubject){
            this.taskType = pTaskType;
            this.subject = pSubject;
        }
    }

    public class TaskWrapper {
        @AuraEnabled
        public string project;
        @AuraEnabled
        public Task task;
        @AuraEnabled
        public List<Role> roles;
        @AuraEnabled
        public Map<String, String> helpTexts;
        @AuraEnabled
        public string team;
    }

    public class Role {
        @AuraEnabled
        public Project_Role__c projectRole;
    }
}