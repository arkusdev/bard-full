@isTest
public class CampaignMemberControllerTest {
    @testSetup
    static void createResources() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias='brdst',
                          Email='bd@apostletech.com', 
                          EmailEncodingKey='UTF-8',
                          LastName='Testing',
                          LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US',
                          ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName='brdst@testorg.com');
        insert u;

        System.runAs(u) {
            List<Account> accts = TestDataFactory.createAccounts(1);

            List<Contact> proctorContacts = TestDataFactory.createContacts(accts, 2);
            List<Contact> attendeeContacts = TestDataFactory.createContacts(accts, 4);
            List<Contact> waitlistContacts = TestDataFactory.createContacts(accts, 1);
            List<Contact> availContacts = TestDataFactory.createContacts(accts, 10);

            List<Contract> contracts = TestDataFactory.createContracts(proctorContacts, 2, false);

            List<User> bdUsers = TestDataFactory.createUsers(2);
            List<User> availUsers = TestDataFactory.createUsers(5);     

            List<Campaign> campaigns = TestDataFactory.createCampaigns(accts[0], 2);

            TestDataFactory.createCampaignDocuments(campaigns[0], 3);

            List<CampaignMember> proctorMembers = TestDataFactory.createCampaignMembers(campaigns[0], proctorContacts, 'Proctor');
            List<CampaignMember> attendeeMembers = TestDataFactory.createCampaignMembers(campaigns[0], attendeeContacts, 'Attendee');
            List<CampaignMember> waitlistMembers = TestDataFactory.createCampaignMembers(campaigns[0], waitlistContacts, 'Waitlist');
            System.debug('waitlistMembers: ' + waitlistMembers);

            List<Campaign_User__c> userMembers = TestDataFactory.createCampaignUsers(campaigns[0], bdUsers);

            TestDataFactory.createCampaignDocuments(campaigns[0], 1);

            TestDataFactory.createCampaignUsers(campaigns[1], bdUsers);
            TestDataFactory.createCampaignDocuments(campaigns[1], 2);

            TestDataFactory.createCampaignMembers(campaigns[1], attendeeContacts, 'Attendee');
            
            List<Campaign_Meal__c> meals = TestDataFactory.createCampaignMeals(campaigns[0], 1);
            TestDataFactory.createCampaignMealAttendee(meals[0], attendeeMembers);
            TestDataFactory.createCampaignMealAttendee(meals[0], userMembers);

            List<Campaign_Flight__c> attendeeFlights = TestDataFactory.createCampaignFlights(campaigns[0], attendeeMembers, 'Attendee');
            TestDataFactory.createCampaignFlightLegs(attendeeFlights);
            List<Campaign_Flight__c> staffFlights = TestDataFactory.createCampaignFlights(campaigns[0], userMembers, 'BD Staff');
            TestDataFactory.createCampaignFlightLegs(staffFlights);

            List<Campaign_Hotel__c> hotels = TestDataFactory.createCampaignHotels(campaigns[0], 1);
            TestDataFactory.createCampaignHotelAttendees(hotels[0], attendeeMembers);
            TestDataFactory.createCampaignHotelAttendees(hotels[0], userMembers);

            List<Campaign_Incidental__c> incidentals = TestDataFactory.createCampaignIncidental(campaigns[0], 1);
            TestDataFactory.createCampaignIncidentalAttendee(incidentals[0], attendeeMembers);
            TestDataFactory.createCampaignIncidentalAttendee(incidentals[0], userMembers);

            List<Campaign_Transportation__c> trans = TestDataFactory.createCampaignTransportations(campaigns[0], 1);
            TestDataFactory.createCampaignTransportationAttendee(trans[0], attendeeMembers);
            TestDataFactory.createCampaignTransportationAttendee(trans[0], userMembers);

            List<Campaign_Proctor_Fee__c> fees = TestDataFactory.createCampaignProctorFees(campaigns[0], proctorMembers);
        }
    }

    @isTest
    static void fetchAllCampaignMembers_Proctors_UnitTest() {
        Campaign c = [
            SELECT Id
            FROM Campaign
        ][0];

        Map<string, List<PersonDTO>> memberMap = CampaignMemberController.fetchAllCampaignMembers(c.Id, 'Proctor');

        System.assertEquals(2, memberMap.size()); //Two key entries in the map (availMembers, campaignMembers)

        for(string key :memberMap.keySet()) {
            if (key.equalsIgnoreCase('availMembers')) {
                System.assertEquals(10, memberMap.get(key).size());
            } else if (key.equalsIgnoreCase('campaignMembers')) {
                System.assertEquals(2, memberMap.get(key).size());
            }
        }
    }

    @isTest
    static void fetchAllCampaignMembers_Attendees_UnitTest() {
        Campaign c = [
            SELECT Id
            FROM Campaign
        ][0];

        Map<string, List<PersonDTO>> memberMap = CampaignMemberController.fetchAllCampaignMembers(c.Id, 'Attendee');

        System.assertEquals(2, memberMap.size()); //Two key entries in the map (availMembers, campaignMembers)

        for(string key :memberMap.keySet()) {
            if (key.equalsIgnoreCase('availMembers')) {
                System.assertEquals(10, memberMap.get(key).size());
            } else if (key.equalsIgnoreCase('campaignMembers')) {
                System.assertEquals(4, memberMap.get(key).size());
            }
        }
    }

    @isTest
    static void fetchAllCampaignMembers_Waitlist_UnitTest() {
        Campaign c = [
            SELECT Id
            FROM Campaign
        ][0];

        Map<string, List<PersonDTO>> memberMap = CampaignMemberController.fetchAllCampaignMembers(c.Id, 'Waitlist');

        System.assertEquals(2, memberMap.size()); //Two key entries in the map (availMembers, campaignMembers)

        for(string key :memberMap.keySet()) {
            if (key.equalsIgnoreCase('availMembers')) {
                System.assertEquals(10, memberMap.get(key).size());
            } else if (key.equalsIgnoreCase('campaignMembers')) {
                System.assertEquals(1, memberMap.get(key).size());
            }
        }
    }

    @isTest
    static void fetchAllCampaignMembers_OverlappingCourse_UnitTest() {
        List<Account> accts = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];

        Campaign c = [
            SELECT Id
            FROM Campaign
        ][0];

        //Create new campaigns
        List<Campaign> campaigns = TestDataFactory.createCampaigns(accts[0], 2);

        //Get a proctor from the existing campaign
        List<Contact> contacts = [
            SELECT Id
            FROM Contact 
            WHERE Id IN (
                SELECT ContactId FROM CampaignMember WHERE CampaignId = :c.Id AND Type__c = 'Proctor'
            )
            LIMIT 1
        ];

        //Tie this proctor to new campaigns
        TestDataFactory.createCampaignMembers(campaigns[0], contacts, 'Proctor');
        TestDataFactory.createCampaignMembers(campaigns[1], contacts, 'Proctor');

        //Execute the search logic
        Map<string, List<PersonDTO>> memberMap = CampaignMemberController.fetchAllCampaignMembers(c.Id, 'Proctor');
    }

    @isTest
    static void fetchAllCampaignUsers_UnitTest() {
        Campaign c = [
            SELECT Id
            FROM Campaign
        ][0];

        Map<string, List<PersonDTO>> userMap = CampaignMemberController.fetchAllCampaignMembers(c.Id, 'BD Staff');

        System.assertEquals(2, userMap.size()); //Two key entries in the map (availMembers, campaignMembers)

        for(string key :userMap.keySet()) {
            if (key.equalsIgnoreCase('availMembers')) {
                System.assert(userMap.get(key).size() > 0);
            } else if (key.equalsIgnoreCase('campaignMembers')) {
                System.assertEquals(2, userMap.get(key).size());
            }
        }
    }

    @isTest
    static void campaign_BeforeUpdate_UnitTest() {
        Campaign c = [
            SELECT Id
            FROM Campaign
        ][0];

        //Reduce the # of avail seats & update
        c.Available_Seats__c = 3;

        boolean success = true;

        try {
            update c;
        } catch (Exception ex) {
            success = false;
        }

        System.assertEquals(false, success);
    }

    @isTest
    static void hasAvailableSeats_UnitTest() {
        Campaign c = [
            SELECT Id
            FROM Campaign
        ][0];

        System.assertEquals(false, CampaignMemberController.hasAvailableSeats(c.Id));
    }

    @isTest
    static void hasAvailableSeats_Exception_UnitTest() {
        Campaign c = [
            SELECT Id
            FROM Campaign
        ][0];

        Campaign c2 = [
            SELECT Id
            FROM Campaign
            WHERE Id != :c.Id
        ][0];

        delete c2;

        try {
            CampaignMemberController.hasAvailableSeats(c2.Id);
        } catch(Exception ex) {
            
        }
    }

    @isTest
    static void addMember_UnitTest() {
        Campaign camp = [
            SELECT Id
            FROM Campaign
        ][0];

        List<Contact> contacts = [
            SELECT Id 
            FROM Contact 
            WHERE Id NOT IN (
                SELECT ContactId FROM CampaignMember WHERE CampaignId = :camp.Id AND ContactId != NULL          
            )
            ORDER BY Name
        ];

        //Add a proctor
        CampaignMemberController.addMember(camp.Id, contacts[0].Id, 'Proctor');

        CampaignMember pcm = [
            SELECT Id
            FROM CampaignMember
            WHERE CampaignId = :camp.Id
            AND ContactId = :contacts[0].Id
            AND Type__c = 'Proctor'
        ];

        System.assertNotEquals(pcm.Id, null);

        //Add an attendee - we're at max capacity so catch the exception and move on
        try {
            CampaignMemberController.addMember(camp.Id, contacts[1].Id, 'Attendee');
        } catch (Exception ex) {
            //Do nothing
        }

        List<CampaignMember> acm = [
            SELECT Id
            FROM CampaignMember
            WHERE CampaignId = :camp.Id
            AND ContactId = :contacts[1].Id
            AND Type__c = 'Attendee'
        ];

        System.assertEquals(acm.size(), 0);

        //Add a user
        List<User> users = [
            SELECT Id
            FROM User
            WHERE Id NOT IN (
                SELECT User__c FROM Campaign_User__c WHERE Campaign__c = :camp.Id AND User__c != NULL               
            ) AND IsActive = true
            AND Profile.Name = 'Bard - Physician Training'
            ORDER BY Name
            LIMIT 1
        ];

        CampaignMemberController.addMember(camp.Id, users[0].Id, 'BD Staff');

        Campaign_User__c cu = [
            SELECT Id
            FROM Campaign_User__c
            WHERE Campaign__c = :camp.Id
            AND User__c = :users[0].Id
        ];

        System.assertNotEquals(cu.Id, null);
    }

    @isTest
    static void addMember_Exception_UnitTest() {
        Campaign c = [
            SELECT Id
            FROM Campaign
        ][0];

        List<Contact> contacts = [
            SELECT Id 
            FROM Contact 
            WHERE Id NOT IN (
                SELECT ContactId FROM CampaignMember WHERE CampaignId = :c.Id AND ContactId != NULL         
            )
            ORDER BY Name
        ];

        Campaign c2 = [
            SELECT Id
            FROM Campaign
            WHERE Id != :c.Id
        ][0];

        delete c2;

        try {
            CampaignMemberController.addMember(c2.Id, contacts[0].Id, 'Proctor');
        } catch(Exception ex) {
            
        }
    }

    @isTest
    static void updateMember_UnitTest() {
        Campaign camp = [
            SELECT Id
            FROM Campaign
        ][0];

        List<CampaignMember> waitlist = [
            SELECT ContactId
            FROM CampaignMember
            WHERE CampaignId = :camp.Id
            AND Type__c = 'Waitlist'
            LIMIT 1     
        ];

        //Update to an attendee - we're at max capacity so catch the exception and move on
        try {
            CampaignMemberController.updateMember(camp.Id, waitlist[0].ContactId, 'Attendee');
        } catch (Exception ex) {
            //Do nothing
        }

        List<CampaignMember> acm = [
            SELECT Id
            FROM CampaignMember
            WHERE CampaignId = :camp.Id
            AND ContactId = :waitlist[0].Id
            AND Type__c = 'Attendee'
        ];

        System.assertEquals(acm.size(), 0);

        List<CampaignMember> attendee = [
            SELECT ContactId
            FROM CampaignMember
            WHERE CampaignId = :camp.Id
            AND Type__c = 'Attendee'
            LIMIT 1     
        ];

        CampaignMemberController.updateMember(camp.Id, attendee[0].ContactId, 'Waitlist');

        CampaignMember wcm = [
            SELECT Id
            FROM CampaignMember
            WHERE CampaignId = :camp.Id
            AND ContactId = :attendee[0].ContactId
            AND Type__c = 'Waitlist'
        ];

        System.assertNotEquals(wcm.Id, null);

        //Now test the move from waitlist to attendee - first make sure there are avail seats
        camp.Available_Seats__c = 10;

        update camp;
        
        CampaignMemberController.updateMember(camp.Id, attendee[0].ContactId, 'Attendee');

        CampaignMember cma = [
            SELECT Id
            FROM CampaignMember
            WHERE CampaignId = :camp.Id
            AND ContactId = :attendee[0].ContactId
            AND Type__c = 'Attendee'
        ];

        System.assertNotEquals(cma.Id, null);
    }

    @isTest
    static void updateMember_Exception_UnitTest() {
        Campaign c = [
            SELECT Id
            FROM Campaign
        ][0];

        List<CampaignMember> waitlist = [
            SELECT ContactId
            FROM CampaignMember
            WHERE CampaignId = :c.Id
            AND Status = 'Waitlist'
            LIMIT 1     
        ];

        Campaign c2 = [
            SELECT Id
            FROM Campaign
            WHERE Id != :c.Id
        ][0];

        delete c2;

        try {
            CampaignMemberController.updateMember(c2.Id, waitlist[0].ContactId, 'Attendee');
        } catch(Exception ex) {
            
        }

        //for code coverage in TriggerHandler.cls
        undelete c2;
    }

    @isTest
    static void removeMember_UnitTest() {
        Campaign camp = [
            SELECT Id
            FROM Campaign
        ][0];

        List<CampaignMember> attendees = [
            SELECT ContactId
            FROM CampaignMember
            WHERE CampaignId = :camp.Id
            AND Type__c = 'Attendee'
            LIMIT 1     
        ];

        CampaignMemberController.removeMember(camp.Id, attendees[0].ContactId, 'Attendee');

        List<CampaignMember> acm = [
            SELECT Id
            FROM CampaignMember
            WHERE CampaignId = :camp.Id
            AND ContactId = :attendees[0].Id
            AND Type__c = 'Attendee'
        ];

        System.assertEquals(acm.size(), 0);

        List<CampaignMember> proctors = [
            SELECT ContactId
            FROM CampaignMember
            WHERE CampaignId = :camp.Id
            AND Type__c = 'Proctor'
            LIMIT 1     
        ];

        CampaignMemberController.removeMember(camp.Id, proctors[0].ContactId, 'Proctor');

        List<CampaignMember> pcm = [
            SELECT Id
            FROM CampaignMember
            WHERE CampaignId = :camp.Id
            AND ContactId = :proctors[0].Id
            AND Type__c = 'Proctor'
        ];

        System.assertEquals(pcm.size(), 0);

        //Add a user
        List<Campaign_User__c> users = [
            SELECT User__c 
            FROM Campaign_User__c 
            WHERE Campaign__c = :camp.Id 
            AND User__c != NULL             
            LIMIT 1
        ];

        CampaignMemberController.removeMember(camp.Id, users[0].User__c, 'BD Staff');

        List<Campaign_User__c> cu = [
            SELECT Id
            FROM Campaign_User__c
            WHERE Campaign__c = :camp.Id
            AND User__c = :users[0].User__c
        ];

        System.assertEquals(0, cu.size());
    }

    @isTest
    static void hasExpenseItems_UnitTest() {
        //Pull a waitlisted member so the controller method will hit each child object for code coverage
        CampaignMember cm = [
            SELECT CampaignId, ContactId
            FROM CampaignMember
            WHERE Type__c = 'Waitlist'
        ][0];

        boolean value = CampaignMemberController.hasExpenseItems(cm.CampaignId, cm.ContactId);

        System.assertEquals(false, value);
    }

    @isTest
    static void searchParamViewModel_UnitTest() {
        SearchParamViewModel vm = new SearchParamViewModel('paramName', 'paramValue');

        System.assertEquals('paramName', vm.ParamName);
        System.assertEquals('paramValue', vm.paramValue);
    }

    @isTest
    static void user_PersonDTO_UnitTest() {
        Campaign c = [
            SELECT Id, StartDate, EndDate
            FROM Campaign
        ][0];

        User u = [
            SELECT Id, Phone
            FROM User 
            WHERE Profile.Name = 'Bard - Physician Training'
        ][0]; 

        u.Phone = '15555555555';

        Map<Id, Map<Id, Campaign_Attendee_Document__c>> docMap = new Map<Id, Map<Id, Campaign_Attendee_Document__c>>();

        List<Campaign_Attendee_Document__c> docs = [
            SELECT Id, User__c, Received__c, Campaign_Document__r.Campaign__r.Credentials_Due_Date__c
            FROM Campaign_Attendee_Document__c
            WHERE Campaign_Document__r.Campaign__c = :c.Id
            AND User__c = :u.Id
            AND Contact__c = NULL
            ORDER BY User__c
        ];

        for(Campaign_Attendee_Document__c cmd: docs) {
            if (!docMap.containsKey(cmd.User__c)) {
                docMap.put(cmd.User__c, new Map<Id, Campaign_Attendee_Document__c> { cmd.Id => cmd });
            } else {
                docMap.get(cmd.User__c).put(cmd.Id, cmd);
            }
        }

        PersonDTO dto = new PersonDTO(u, docMap);

        System.assertEquals(u.Id, dto.personId);
    }

    @isTest
    static void contact_PersonDTO_UnitTest() {
        Campaign c = [
            SELECT Id, StartDate, EndDate
            FROM Campaign
        ][0];

        c.StartDate.addMonths(3);
        c.EndDate.addMonths(3);

        update c;

        Contact con = [
            SELECT Id, Name, Phone, Email, Contact_Photo__c, Contact_Bio__c, CV_on_File__c, Account.Name,
                (SELECT Contract_Services__c, ContractNumber, StartDate, EndDate FROM Contracts__r) 
            FROM Contact
            WHERE Id IN (
                SELECT ContactId
                FROM CampaignMember
                WHERE Type__c = 'Attendee'
            )
        ][0]; 

        Map<Id, Map<Id, Campaign_Attendee_Document__c>> docMap = new Map<Id, Map<Id, Campaign_Attendee_Document__c>>();

        List<Campaign_Attendee_Document__c> docs = [
            SELECT Id, Contact__c, Received__c, Campaign_Document__r.Campaign__r.Credentials_Due_Date__c
            FROM Campaign_Attendee_Document__c
            WHERE Campaign_Document__r.Campaign__c = :c.Id
            AND Contact__c = :con.Id
            AND User__c = NULL
            ORDER BY Contact__c
        ];

        for(Campaign_Attendee_Document__c cmd: docs) {
            if (!docMap.containsKey(cmd.Contact__c)) {
                docMap.put(cmd.Contact__c, new Map<Id, Campaign_Attendee_Document__c> { cmd.Id => cmd });
            } else {
                docMap.get(cmd.Contact__c).put(cmd.Id, cmd);
            }
        }

        Map<Id, Set<Id>> overlapMap = new Map<Id, Set<Id>>();

        PersonDTO dto = new PersonDTO(con, c.StartDate, c.EndDate, overlapMap, docMap);

        System.assertEquals(con.Id, dto.personId);
    }

    @isTest
    static void createRegisteredStatus_Test() {
        List<Account> accts = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        List<Campaign> campaigns = TestDataFactory.createCampaigns(accts[0], 2);        
        Campaign campaign = campaigns[0];
        String statusCreated = CampaignMemberController.createStatus('Registered', campaign.Id);
        CampaignMemberStatus cms = [SELECT Id, SortOrder FROM CampaignMemberStatus WHERE Label = 'Registered'];
        System.assertEquals('Registered', statusCreated, 'The status created is incorrect');
        System.assertEquals(3, cms.SortOrder, 'The sort order for Registered status is incorrect');
    }   

    @isTest
    static void createWaitlistStatus_Test() {
        List<Account> accts = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        List<Campaign> campaigns = TestDataFactory.createCampaigns(accts[0], 2);        
        Campaign campaign = campaigns[0];
        String statusCreated = CampaignMemberController.createStatus('Waitlist', campaign.Id);
        CampaignMemberStatus cms = [SELECT Id, SortOrder FROM CampaignMemberStatus WHERE Label = 'Waitlist'];
        System.assertEquals('Waitlist', statusCreated, 'The status created is incorrect');
        System.assertEquals(4, cms.SortOrder, 'The sort order for Waitlist status is incorrect');
    }   
}