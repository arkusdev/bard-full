public with sharing class UserSubordinatesUtilities {
    private static final String VASCULAR_ROLES_NAMES = 'FSV';
    private static final String VASCULAR_NAMES = 'Vascular';
    
    public static Map<String, Set<String>> getRoleSubordinateUsers() {
        Map<String, Set<String>> subordinates = new Map<String, Set<String>>();
        Map<String, Set<String>> usersByRole = getUsersByRole();
        Map<String, Set<User>> directSubordinates = getDirectSubordinates();
        for (UserRole userRole : [SELECT Id FROM UserRole WHERE ParentRoleId != NULL WITH SECURITY_ENFORCED]) {
            Set<String> users = new Set<String>();
            String userRoleId = userRole.Id;
            Set<String> subRoles = getAllSubRoleIds(new Set<String>{ userRoleId }, directSubordinates);
            if (subRoles != null){
                for (String subrole : subRoles) {
                    users.addAll(usersByRole.get(subRole));
                }
            }
            subordinates.put(userRoleId, users);
        }
        return subordinates;
    }

    private static Set<String> getAllSubRoleIds(Set<String> roleIds, Map<String, Set<User>> directSubordinates) {
        Set<String> currentRoleIds = new Set<String>();
        // get all of the roles underneath the passed roles
        Set<User> users = new Set<User>();
        for (String roleId : roleIds) {
            if (directSubordinates.get(roleId) != null){
                users.addAll(directSubordinates.get(roleId));
            }
        }
        for(User user : users){
            currentRoleIds.add(user.UserRoleId);
        }
        // go fetch some more rolls!
        if(currentRoleIds.size() > 0){
            currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds, directSubordinates));
        }
        return currentRoleIds;
    }

    private static Map<String, Set<User>> getDirectSubordinates() {
        String vascularRoleCondition = '%' + VASCULAR_ROLES_NAMES + '%';
        String vascularCondition = '%' + VASCULAR_NAMES + '%';
        Map<String, Set<User>> parents = new Map<String, Set<User>>();
        for (User user : [SELECT Id, UserRoleId, UserRole.ParentRoleId FROM User WHERE IsActive = TRUE AND UserRole.ParentRoleId != NULL AND (UserRole.Name LIKE: vascularCondition OR UserRole.Name LIKE: vascularRoleCondition)]) {
            String parentId = user.UserRole.ParentRoleId;
            if (parents.containsKey(parentId)){
                parents.get(parentId).add(user);
            }else {
                parents.put(parentId, new Set<User>{ user });
            }
        }
        return parents;
    }

    private static Map<String, Set<String>> getUsersByRole() {
        String vascularRoleCondition = '%' + VASCULAR_ROLES_NAMES + '%';
        String vascularCondition = '%' + VASCULAR_NAMES + '%';
        Map<String, Set<String>> usersByRole = new Map<String, Set<String>>();
        for (User user : [SELECT Id, UserRoleId FROM User WHERE (UserRole.Name LIKE: vascularCondition OR UserRole.Name LIKE: vascularRoleCondition) AND IsActive = TRUE]) {
            if (usersByRole.containsKey(user.UserRoleId)){
                usersByRole.get(user.UserRoleId).add(user.Id);
            }else {
                usersByRole.put(user.UserRoleId, new Set<String>{ user.Id });
            }
        }
        return usersByRole;
    }
}