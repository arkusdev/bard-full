public class CampaignProctorFeeDTO {
	@auraEnabled 
	public Id feeId { get; set; } 
	@auraEnabled 
	public Id campaignId { get; set; } 
	@auraEnabled 
	public string name { get; set; }
	@auraEnabled 
	public Id proctorId { get; set; }
	@auraEnabled 
	public string proctorName { get; set; }
	@auraEnabled 
	public decimal totalCost { get; set; }
	@auraEnabled
	public string totalCostDisplay { get; set; }
	@auraEnabled 
	public string transferOfValue { get; set; }
	@auraEnabled 
	public string notes { get; set; }
	@auraEnabled 
	public boolean isNew { get; set; }
	@auraEnabled 
	public Map<Id, string> availableProctors { get; set; }
	@auraEnabled
	public Map<string, string> transferOfValueOptions { get; set; }

	List<string> args = new string[] { '0','number', '###,###,##0.00' };

	public CampaignProctorFeeDTO(Campaign_Proctor_Fee__c cpf) {
		this.initDTO(cpf);
	}

	public CampaignProctorFeeDTO(Campaign_Proctor_Fee__c cpf, List<CampaignMember> availProctors) {
		this.initDTO(cpf);

		if (!availProctors.isEmpty()) {
			for(CampaignMember ap :availProctors) {
				this.availableProctors.put(ap.ContactId, ap.Contact.Name);
			}		
		}
	}

	private void initDTO(Campaign_Proctor_Fee__c cpf) {
		this.feeId = cpf.Id;
		this.isNew = (cpf.Id == null);
		this.campaignId = cpf.Campaign__c;
		this.name = cpf.Name;
		this.proctorId = cpf.Proctor__c;
		this.proctorName = cpf.Proctor__r.Name;
		this.notes = cpf.Notes__c;
		this.totalCost = (cpf.Total_Cost__c != null ? cpf.Total_Cost__c : 0);
		this.transferOfValue = cpf.Transfer_of_Value__c;
		this.availableProctors = new Map<Id, string>();

		this.totalCostDisplay = '$' + string.format(this.totalCost.format(), args);

		this.transferOfValueOptions = PicklistUtil.fetchTransferOfValueOptions('Campaign_Proctor_Fee__c');
	}
}