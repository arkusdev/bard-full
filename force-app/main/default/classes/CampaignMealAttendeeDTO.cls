public class CampaignMealAttendeeDTO {
	@auraEnabled 
	public Id value { get; set; }
	@auraEnabled 
	public string label { get; set; }

	public CampaignMealAttendeeDTO(Id attendeeId, string attendeeName) {
		this.value = attendeeId;
		this.label = attendeeName;
	}

	public CampaignMealAttendeeDTO(CampaignMember cm) {
		this.value = cm.ContactId;
		this.label = cm.Contact.name;
	}

	public CampaignMealAttendeeDTO(Campaign_User__c cu) {
		this.value = cu.User__c;
		this.label = cu.User__r.name;
	}
}