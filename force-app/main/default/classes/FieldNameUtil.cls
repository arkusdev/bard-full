public class FieldNameUtil {
	public static final string provider_name = 'provider_name';
	public static final string contract_services = 'contract_services';
	public static final string society_registrations = 'society_registrations';
	public static final string division = 'division';
	public static final string product_category = 'product_category';
	public static final string surgeon_specialty = 'surgeon_specialty';
	public static final string surgical_technique = 'surgical_technique';
	public static final string licensed_regions = 'licensed_regions';
	public static final string licensed_states = 'licensed_states';
	public static final string active_bd_contract = 'active_bd_contract';
	public static final string kol_flg = 'kol_flg';
	public static final string facility_type = 'facility_type';
	public static final string course_title = 'course_title';
}