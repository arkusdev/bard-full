public class CampaignExpensesController {
	@auraEnabled
	public static Map<string, boolean> allowAccess() {
		Map<string, boolean> accessMap = new Map<string, boolean> {
			'meals' => UserUtil.canAccessMealExpenses(),
			'flights' => UserUtil.canAccessFlightExpenses(),
			'hotels' => UserUtil.canAccessHotelExpenses(),
			'transportation' => UserUtil.canAccessTransportationExpenses(),
			'incidentals' => UserUtil.canAccessIncidentalExpenses(),
			'proctors' => UserUtil.canAccessProctorExpenses()
		};

		return accessMap;
	}
}