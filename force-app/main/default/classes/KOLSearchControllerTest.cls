@isTest
public class KOLSearchControllerTest {
    @testSetup
    static void createResources() {
        List<Account> accts = TestDataFactory.createAccounts(1);
        List<Contact> contacts = TestDataFactory.createContacts(accts, 5);
        
        TestDataFactory.createContracts(contacts, 2, false);
        TestDataFactory.createContracts(contacts, 2, true);
        TestDataFactory.createDivisions(contacts, 5);
        TestDataFactory.createContactLicenses(contacts, 5);

        //Need a contact with no contract data for code coverage
        TestDataFactory.createContacts(accts, 1);
    }

    //Not using serializing the SearchParam object here in order to fully control code coverage
    //

    @isTest
    static void unitTestSearc() {
        string params = '[' + 
                            '{"paramName":"provider_name","paramValue":"FirstName"},' + 
                            '{"paramName":"facility_type","paramValue":"Hospital"},' + 
                            '{"paramName":"contract_services","paramValue":"Speaking"},' +
                            '{"paramName":"division","paramValue":"Biopsy"},' +
                            '{"paramName":"product_category","paramValue":"Capital"},' +
                            '{"paramName":"surgeon_specialty","paramValue":"CARDIOVASCULAR DISEASE"},' + 
                            '{"paramName":"surgical_technique","paramValue":"CTO"},' + 
                            '{"paramName":"licensed_regions","paramValue":"Mountain"},' + 
                            '{"paramName":"licensed_states","paramValue":"AZ"},' +
                            '{"paramName":"active_bd_contract","paramValue":"Active"},' +
                            '{"paramName":"kol_flg","paramValue":"true"}' + 
                        ']';    

        List<PersonDTO> p = KOLSearchController.fetchKOLContacts(params, '');

        System.assertEquals(5, p.size());
    }

    @isTest
    static void unitTestNoResultsSearch() {
        string params = '[' + 
                            '{"paramName":"provider_name","paramValue":"FirstName"},' + 
                            '{"paramName":"facility_type","paramValue":"Hospital"},' + 
                            '{"paramName":"contract_services","paramValue":"Other"},' +
                            '{"paramName":"active_bd_contract","paramValue":"Expired"},' +
                            '{"paramName":"product_category","paramValue":"Capital"},' +
                            '{"paramName":"division","paramValue":"Biopsy"},' +
                            '{"paramName":"surgeon_specialty","paramValue":"CARDIOVASCULAR DISEASE"},' + 
                            '{"paramName":"surgical_technique","paramValue":"CTO"},' + 
                            '{"paramName":"licensed_states","paramValue":"AZ"},' +
                            '{"paramName":"licensed_regions","paramValue":"Mountain"},' + 
                            '{"paramName":"kol_flg","paramValue":"false"}' + 
                        ']';    

        List<PersonDTO> p = KOLSearchController.fetchKOLContacts(params, 'Test');

        System.assertEquals(0, p.size());
    }

    @isTest
    static void unitTestNoContractSearch() {
        string params = '[' + 
                            '{"paramName":"provider_name","paramValue":"FirstName"},' + 
                            '{"paramName":"facility_type","paramValue":"Hospital"},' + 
                            '{"paramName":"active_bd_contract","paramValue":"None"},' +
                            '{"paramName":"contract_services","paramValue":"Speaking"},' +
                            '{"paramName":"product_category","paramValue":"Capital"},' +
                            '{"paramName":"division","paramValue":"Biopsy"},' +
                            '{"paramName":"surgeon_specialty","paramValue":"CARDIOVASCULAR DISEASE"},' + 
                            '{"paramName":"surgical_technique","paramValue":"CTO"},' + 
                            '{"paramName":"licensed_states","paramValue":"AZ"},' +
                            '{"paramName":"licensed_regions","paramValue":"Mountain"},' + 
                            '{"paramName":"kol_flg","paramValue":"false"}' + 
                        ']';    

        List<PersonDTO> p = KOLSearchController.fetchKOLContacts(params, '');

        System.assertEquals(0, p.size());
    }

    @isTest
    static void unitTestActiveContractSearch() {
        string params = '[' + 
                            '{"paramName":"provider_name","paramValue":"FirstName"},' + 
                            '{"paramName":"facility_type","paramValue":"Hospital"},' + 
                            '{"paramName":"active_bd_contract","paramValue":"Active"},' +
                            '{"paramName":"contract_services","paramValue":"Speaking"},' +
                            '{"paramName":"product_category","paramValue":"Capital"},' +
                            '{"paramName":"division","paramValue":"Biopsy"},' +
                            '{"paramName":"surgeon_specialty","paramValue":"CARDIOVASCULAR DISEASE"},' + 
                            '{"paramName":"surgical_technique","paramValue":"CTO"},' + 
                            '{"paramName":"licensed_states","paramValue":"AZ"},' +
                            '{"paramName":"licensed_regions","paramValue":"Mountain"},' + 
                            '{"paramName":"kol_flg","paramValue":"true"}' + 
                        ']';    

        List<PersonDTO> p = KOLSearchController.fetchKOLContacts(params, '');

        System.assertEquals(5, p.size());
    }

    @isTest
    static void unitTestExpiredContractSearch() {
        string params = '[' + 
                            '{"paramName":"provider_name","paramValue":"FirstName"},' + 
                            '{"paramName":"facility_type","paramValue":"Hospital"},' + 
                            '{"paramName":"active_bd_contract","paramValue":"Expired"},' +
                            '{"paramName":"contract_services","paramValue":"Speaking"},' +
                            '{"paramName":"product_category","paramValue":"Capital"},' +
                            '{"paramName":"division","paramValue":"Biopsy"},' +
                            '{"paramName":"surgeon_specialty","paramValue":"CARDIOVASCULAR DISEASE"},' + 
                            '{"paramName":"surgical_technique","paramValue":"CTO"},' + 
                            '{"paramName":"licensed_states","paramValue":"AZ"},' +
                            '{"paramName":"licensed_regions","paramValue":"Mountain"},' + 
                            '{"paramName":"kol_flg","paramValue":"true"}' + 
                        ']';    

        List<PersonDTO> p = KOLSearchController.fetchKOLContacts(params, '');

        System.assertEquals(0, p.size());
    }

    @isTest
    static void unitTestPickListValues() {
        List<string> fields = new List<String> {
            'contract_services',
            'division', 
            'product_category', 
            'surgeon_specialty',
            'surgical_technique',
            'licensed_regions',
            'licensed_states'       
        };

         Map<string, Map<string, string>> values = KOLSearchController.fetchPickListValues(fields);

         System.assertEquals(7, values.size());

         for(string key :values.keySet()) {
            if(key.equalsIgnoreCase('product_category')) {
                System.assertEquals(14, values.get(key).size());
                break;
            }
         }
    }

    @isTest
    static void unitTestProductCategoriesByDivision() {
        List<string> pc = KOLSearchController.fetchProductCategories('Biopsy');

        System.assertEquals(4, pc.size());

        System.assertEquals('Biopsy', pc.get(0));
        System.assertEquals('Capital', pc.get(1));
        System.assertEquals('Senorx', pc.get(2));
    }

    @isTest
    static void unitTestProductCategoriesNoDivision() {
        List<string> pc = KOLSearchController.fetchProductCategories('');

        System.assertEquals(14, pc.size());
    }

    @isTest
    static void unitTestLicensedStates() {
        Map<string, string> ls = KOLSearchController.fetchLicensedStates('Mountain');

        System.assertEquals(1, ls.size());

        for(string key :ls.keySet()) {
            System.assertEquals('AZ', key);
            System.assertEquals('Arizona - AZ', ls.get(key));
        }
    }

    @isTest
    static void unitTestAllStateOptions() {
        Map<string, string> ls = PicklistUtil.fetchAllStateOptions();

        System.assertEquals(52, ls.size());
    }

    //@isTest
    //static void unitTestPersonDTOUser() {
        ////User u = [
            ////SELECT Id, Name, Phone, Email, Title, IsActive
            ////FROM User 
            ////WHERE Email = 'bd@apostletech.com'
        ////]; 

        ////User u = TestDataFactory.createUsers(1);

        ////PersonDTO dto = new PersonDTO(u);

        ////System.assertEquals(u.Id, dto.personId);

        ////u.Phone = '15555555555';

        ////PersonDTO dto2 = new PersonDTO(u);

        ////System.assertEquals(u.Id, dto2.personId);
    //}

    //@isTest
    //static void unitTestPersonDTOUser2() {
        //User u = [
            //SELECT Id, Name, Phone, Email, Title, IsActive
            //FROM User 
            //WHERE Email = 'bd@apostletech.com'
        //]; 

        //PersonDTO dto = new PersonDTO(u.Id, u.Name, true);

        //System.assertEquals(u.Id, dto.personId);
    //}
    
    //@isTest
    //static void unitTestSearchParam() {
        //SearchParamViewModel vm = new SearchParamViewModel('paramName', 'paramValue');

        //System.assertEquals('paramName', vm.ParamName);
        //System.assertEquals('paramValue', vm.paramValue);
    //}
}