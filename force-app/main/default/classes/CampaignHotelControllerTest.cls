@isTest
public class CampaignHotelControllerTest  {
	@testSetup
	static void createResources() {
		User u = [
			SELECT Id, Name, Phone, Email, Title, IsActive
			FROM User 
			WHERE Email = 'bd@apostletech.com'
		][0]; 

		System.runAs(u) {
			List<Account> accts = TestDataFactory.createAccounts(1);

			List<Contact> proctorContacts = TestDataFactory.createContacts(accts, 1);
			List<Contact> attendeeContacts = TestDataFactory.createContacts(accts, 4);

			List<User> bdUsers = TestDataFactory.createUsers(2);

			List<Campaign> campaigns = TestDataFactory.createCampaigns(accts[0], 1);

			List<CampaignMember> proctorMembers = TestDataFactory.createCampaignMembers(campaigns[0], proctorContacts, 'Proctor');
			List<CampaignMember> attendeeMembers = TestDataFactory.createCampaignMembers(campaigns[0], attendeeContacts, 'Attendee');

			List<Campaign_User__c> userMember = TestDataFactory.createCampaignUsers(campaigns[0], bdUsers);

			List<Campaign_Hotel__c> hotels = TestDataFactory.createCampaignHotels(campaigns[0], 1);

			TestDataFactory.createCampaignHotelAttendees(hotels[0], proctorMembers);
			TestDataFactory.createCampaignHotelAttendees(hotels[0], attendeeMembers);

			TestDataFactory.createCampaignHotelAttendees(hotels[0], userMember);
		}
	}

	@isTest
	static void fetchCampaignHotels_UnitTest() {
		Campaign c = [
			SELECT Id
			FROM Campaign
		][0];

		List<CampaignHotelDTO> hotels = CampaignHotelController.fetchCampaignHotels(c.Id);

		System.assertEquals(1, hotels.size());

		for(CampaignHotelDTO h :hotels) {
			System.assertEquals(5, h.selectedMembers.size());
			System.assertEquals(2, h.selectedUsers.size());
		}
	}

	@isTest
	static void fetchCampaignHotel_UnitTest() {
		Campaign_Hotel__c hotel = [
			SELECT Id, Campaign__c
			FROM Campaign_Hotel__c
			LIMIT 1
		];

		CampaignHotelDTO hotelDTO = CampaignHotelController.fetchCampaignHotel(hotel.Id);

		System.assertEquals(hotel.Campaign__c, hotelDTO.campaignId);
	}

	@isTest
	static void saveCampaignHotel_UnitTest() {
		Campaign c = [
			SELECT Id
			FROM Campaign
		][0];
		
		CampaignHotelDTO dto = CampaignHotelController.createNewCampaignHotel(c.Id);
		
		dto.name = 'New Test Hotel';
		dto.checkInDate = Date.today();
		dto.checkOutDate = Date.today().addDays(1);
		dto.roomBlock = 'A1 - A5';
		dto.additionalAttendees = 1;
		dto.additionalAttendeesCost = 150.00;
		dto.notes = 'Testing 1, 2, 3';

		//Create members/users
		integer i = 0;

		for(CampaignHotelAttendeeDTO cha :dto.availableMembers) {
			for(integer x = 0; x <= 1; x++) {
				dto.selectedMembers.add(new CampaignHotelAttendeeDTO(cha.attendeeId, cha.attendeeName, 'Test Member', Date.today().addDays(x), 150.00, true ));
				dto.selectedMembers[i].confirmationNumber = '12345';
				i++;			
			}
		}

		i = 0;

		for(CampaignHotelAttendeeDTO cha :dto.availableUsers) {
			for(integer x = 0; x <= 1; x++) {
				dto.selectedUsers.add(new CampaignHotelAttendeeDTO(cha.attendeeId, cha.attendeeName, 'Test User', Date.today().addDays(x), 150.00, true));
				dto.selectedUsers[i].confirmationNumber = '54321';
				i++;
			}
		}

		CampaignHotelController.saveCampaignHotel(JSON.serialize(dto));

		//Now remove a single member/user and recall the save operation
		Campaign_Hotel__c ch = [
			SELECT Id
			FROM Campaign_Hotel__c
			WHERE Name = 'New Test Hotel'
			LIMIT 1
		][0];

		CampaignHotelDTO hotel = CampaignHotelController.fetchCampaignHotel(ch.Id);
		
		hotel.selectedMembers.remove(0);
		hotel.selectedUsers.remove(0);

		CampaignHotelController.saveCampaignHotel(JSON.serialize(hotel));

		//Remove them all for code coverage here
		hotel.selectedMembers.clear();
		hotel.selectedUsers.clear();
		
		CampaignHotelController.saveCampaignHotel(JSON.serialize(hotel));

		Contact con = [
			SELECT Id
			FROM Contact
			WHERE Id NOT IN (
				SELECT ContactId FROM CampaignMember WHERE CampaignId = :ch.Id
			)
			LIMIT 1
		][0];

		hotel.containsAttendee(con.Id, 'Contact');
	}

	@isTest
	static void deleteCampaignHotel_UnitTest() {
		Campaign_Hotel__c ch = [
			SELECT Id
			FROM Campaign_Hotel__c
			LIMIT 1
		][0];

		CampaignHotelController.deleteCampaignHotel(ch.Id);
	}
}