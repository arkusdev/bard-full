@IsTest(seeAllData=true)
private class ODINReportCreatorControllerTest {
    
    static testMethod void GetReportListTest() {
        
        Test.startTest();
            List<Report> reportList = ODINReportCreatorController.GetReportList(); 
        Test.stopTest();

        system.assert(reportList.size() > 0);
    }

    static testMethod void ODINReportCreatorControllerTest() {
        Project__c project = new Project__c();
        project.Name = 'Test';
        insert project;

        Proposed_Product__c product1 = new Proposed_Product__c();
        product1.Project__c = project.Id;
        product1.Name = 'Product 1';
        insert product1;

        RecordType recordType = [select Id 
                                from RecordType 
                                where SObjectType = 'Financial__c' 
                                and RecordType.DeveloperName = 'Global_Financial']; 

        RecordType recordTypeProduct = [select Id 
                                from RecordType 
                                where SObjectType = 'Financial__c' 
                                and RecordType.DeveloperName = 'Product'];     

        String year = '2019';
        List<Financial__c> financialList = new List<Financial__c>();
        for (Integer i = 0; i < 10; i++) {
            Financial__c financial = new Financial__c();
            financial.Project__c = project.Id;
            financial.Proposed_Product__c = product1.Id;
            financial.Year__c = String.valueOf(Integer.valueOf(year) + i);
            financial.Units__c = 10;
            financial.ASP__c = 20;
            financial.RecordTypeId = recordTypeProduct.Id;
            financial.Year_Sequence__c = i + 1;
            financialList.add(financial);

            Financial__c financial2 = new Financial__c();
            financial2.Project__c = project.Id;
            financial2.Proposed_Product__c = product1.Id;
            financial2.Country__c = 'United States';
            financial2.Region__c = 'United States';
            financial2.Year__c = String.valueOf(Integer.valueOf(year) + i);
            financial2.Units__c = 10;
            financial2.ASP__c = 20;
            financial2.RecordTypeId = recordType.Id;
            financialList.add(financial2);
        }
        insert financialList;
        Test.startTest();
            for (Report report : ODINReportCreatorController.GetReportList()) {
                PageReference pageRef = Page.ODINReportCreatorExcel;
                pageRef.getParameters().put('projectId', project.Id);
                pageRef.getParameters().put('reportId', report.Id);
                Test.setCurrentPage(pageRef);
                ODINReportCreatorController controller = new ODINReportCreatorController();
                system.assert(controller.columnHeaders.size() > 0);
                system.assert(controller.matrix.size() > 0);
                system.assert(controller.filters.size() > 0);
            }
        Test.stopTest();
    }
}