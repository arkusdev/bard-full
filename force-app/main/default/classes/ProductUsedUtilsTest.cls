@isTest
private class ProductUsedUtilsTest {
	
	static testMethod void UpdateCasesTest() {
		List<Product__c> pList = new List<Product__c>();
		Product__c p1 = new Product__c();
		p1.Name = 'Atlas';
		pList.add(p1);
		Product__c p2 = new Product__c();
		p2.Name = 'Feeding';
		pList.add(p2);
		Product__c p3 = new Product__c();
		p3.Name = 'AV Surgical';
		pList.add(p3);
		Product__c p4 = new Product__c();
		p4.Name = 'Flair';
		pList.add(p4);
		Product__c p5 = new Product__c();
		p5.Name = 'Fluency';
		pList.add(p5);
		Product__c p6 = new Product__c();
		p6.Name = 'VascuTrak ';
		pList.add(p6);
		Product__c p7 = new Product__c();
		p7.Name = 'Conquest';
		pList.add(p7);
		insert pList;

		Case c = new Case();
		c.Subject = 'test';
		c.Product_Group_1__c = p1.Name;
		c.Quantity_1__c = '1';
		c.Product_Group_2__c = p2.Name;
		c.Quantity_2__c = '1';
		c.Product_Group_3__c = p3.Name;
		c.Quantity_3__c = '1';
		c.Product_Group_4__c = p4.Name;
		c.Quantity_4__c = '1';
		c.Product_Group_5__c = p5.Name;
		c.Quantity_5__c = '1';
		c.Product_Group_6__c = p6.Name;
		c.Quantity_6__c = '1';
		c.Product_Group_7__c = p7.Name;
		c.Quantity_7__c = '1';
		Test.startTest();
		insert c;
		Test.stopTest();

		List<Product_Used__c> puList = [select Id from Product_Used__c where Case__c =: c.Id];
		for (Product_Used__c pu : puList)
			pu.Quantity__c = 2;
		
		update puList;
	}

	static testMethod void CleanCasesTest() {
		List<Product__c> pList = new List<Product__c>();
		Product__c p1 = new Product__c();
		p1.Name = 'Atlas';
		pList.add(p1);
		Product__c p2 = new Product__c();
		p2.Name = 'Feeding';
		pList.add(p2);
		Product__c p3 = new Product__c();
		p3.Name = 'AV Surgical';
		pList.add(p3);
		Product__c p4 = new Product__c();
		p4.Name = 'Flair';
		pList.add(p4);
		Product__c p5 = new Product__c();
		p5.Name = 'Fluency';
		pList.add(p5);
		Product__c p6 = new Product__c();
		p6.Name = 'VascuTrak ';
		pList.add(p6);
		Product__c p7 = new Product__c();
		p7.Name = 'Conquest';
		pList.add(p7);
		insert pList;

		Case c = new Case();
		c.Subject = 'test';
		c.Product_Group_1__c = p1.Name;
		c.Quantity_1__c = '1';
		c.Product_Group_2__c = p2.Name;
		c.Quantity_2__c = '1';
		c.Product_Group_3__c = p3.Name;
		c.Quantity_3__c = '1';
		c.Product_Group_4__c = p4.Name;
		c.Quantity_4__c = '1';
		c.Product_Group_5__c = p5.Name;
		c.Quantity_5__c = '1';
		c.Product_Group_6__c = p6.Name;
		c.Quantity_6__c = '1';
		c.Product_Group_7__c = p7.Name;
		c.Quantity_7__c = '1';
		Test.startTest();
		insert c;
		Test.stopTest();

		List<Product_Used__c> puList = [select Id from Product_Used__c where Case__c =: c.Id];

		
		delete puList;
		
	}
}