public with sharing class ProjectUtils {
    
    static List<CollaborationGroupMember> chatterGroupsMembersToDelete = new List<CollaborationGroupMember>();
    static Map<String, String> profiles = new Map<String, String>{ 'Marketing' => '', 'Market Access/HEOR​' => ' – MA/HEOR'};

    public static void AddProjectRoles() {
        Set<String> usersIds = new Set<String>();
        Map<String, Schema.PicklistEntry> regionValues = new Map<String, Schema.PicklistEntry>();
        Map<String, Schema.PicklistEntry> roleValues = new Map<String, Schema.PicklistEntry>();

        Schema.DescribeFieldResult regionFieldResult = Project_Role__c.Region__c.getDescribe();
		List<Schema.PicklistEntry> regionPle = regionFieldResult.getPicklistValues();
		for (Schema.PicklistEntry pickListVal : regionPle) {
			regionValues.put(pickListVal.value, pickListVal);
		}

        Schema.DescribeFieldResult roleFieldResult = Project_Role__c.Role__c.getDescribe();
		List<Schema.PicklistEntry> rolePle = roleFieldResult.getPicklistValues();
		for (Schema.PicklistEntry pickListVal : rolePle) {
			roleValues.put(pickListVal.value, pickListVal);
		}

        List<String> productValues = new List<String>();
        for (Project__c project : (List<Project__c>) trigger.new){
            productValues.add(project.Platform_Disease_State__c);
        }
        
        Map<String, List<ODIN_Role_Assignment_Mapping__mdt>> productToMappingMap = new Map<String, List<ODIN_Role_Assignment_Mapping__mdt>>();
        
        for (ODIN_Role_Assignment_Mapping__mdt mapping : [select    Product__c, 
                                                                    Region__c,
                                                                    Country__c, 
                                                                    User_ID__c,
                                                                    Sub_Region__c, 
                                                                    Role__c 
                                                            from ODIN_Role_Assignment_Mapping__mdt 
                                                            where Product__c IN: productValues]) {
            
            if (productToMappingMap.containsKey(mapping.Product__c)) {
                productToMappingMap.get(mapping.Product__c).add(mapping);
            } else {
                productToMappingMap.put(mapping.Product__c, new List<ODIN_Role_Assignment_Mapping__mdt>{mapping});
            }
            usersIds.add(mapping.User_ID__c);
        }

        Set<Id> activeUsers = new Map<Id, User>([SELECT Id FROM User WHERE Id IN: usersIds AND IsActive = true]).keySet();
        
        List<Project_Role__c> roleList = new List<Project_Role__c>();
        
        for (Project__c project : (List<Project__c>) trigger.new) {
        
            if (productToMappingMap.containsKey(project.Platform_Disease_State__c)) {
                for (ODIN_Role_Assignment_Mapping__mdt mapping : productToMappingMap.get(project.Platform_Disease_State__c)) {
                    String region = regionValues.containsKey(mapping.Region__c) ? mapping.Region__c : (regionValues.containsKey(mapping.Sub_Region__c) ? mapping.Sub_Region__c : '');
                    String roleToAssign = mapping.Role__c;
                    String country = mapping.Country__c == null ? '' : mapping.Country__c;
                    if (activeUsers.contains(mapping.User_ID__c) && region != '' && roleValues.containsKey(roleToAssign) && mapping.User_ID__c != null){
                        Project_Role__c role = new Project_Role__c();
                        role.Region__c = region;
                        role.Role__c = roleToAssign;
                        role.User__c = mapping.User_ID__c;//Test.isRunningTest() ? UserInfo.getUserId() : mapping.User_ID__c;
                        role.Project__c = project.Id;
                        role.Country__c = country;
                        roleList.add(role);
                    }
                }
            }
        }

        insert roleList;
    }

    public static void ChangeLaunchYearFinancials() {
        List<Project__c> projectList = new List<Project__c>();
        
        for(Project__c project : (List<Project__c>)trigger.new) {
            if (project.Launch_Year__c != ((Project__c)trigger.oldMap.get(project.Id)).Launch_Year__c) 
                projectList.add(project);
        }

        List<Financial__c> financialGMAList = new List<Financial__c>();
        List<Financial__c> financialProdList = new List<Financial__c>();
        for (Financial__c financial : [select   Year__c,
                                                Project__c,
                                                Project__r.Launch_Year__c,
                                                Proposed_Product__c,
                                                Region__c,
                                                RecordTypeId,
                                                RecordType.DeveloperName 
                                        from Financial__c 
                                        where Project__c =: projectList 
                                        and RecordType.DeveloperName IN ('Global_Market_Assessment','Product')
                                        ORDER BY Project__c, RecordTypeId, Proposed_Product__c, Region__c, Year__c ASC]) {
            
            if (financial.RecordType.DeveloperName == 'Global_Market_Assessment' ) {
                financialGMAList.add(financial);
            } else {
                financialProdList.add(financial);
            }
        }

        Integer count = 0;
        List<Financial__c> financialList = new List<Financial__c>();
        
        String previousRegion;

        for (Financial__c financial : financialGMAList) {

            if (!String.IsBlank(previousRegion) && previousRegion != financial.Region__c) 
                count = 0;

            financial.Year__c = String.valueOf(Integer.valueOf(financial.Project__r.Launch_Year__c) + count);
            count ++;
            financialList.add(financial);
            previousRegion = financial.Region__c;
        }  

        count = 0;

        Id previousProduct;
        Map<String, Map<String, Financial__c>> productYearFinancialMap = new Map<String, Map<String, Financial__c>>();
        Set<String> yearSet = new Set<String>();
        Set<Id> productFinancialIds = new Set<Id>();
        
        for (Financial__c financial : financialProdList ) {

            if (!String.IsBlank(previousProduct) && previousProduct != financial.Proposed_Product__c) 
                count = 0;

            financial.Year__c = String.valueOf(Integer.valueOf(financial.Project__r.Launch_Year__c) + count);
            count++;
            financialList.add(financial);
            
            previousProduct = financial.Proposed_Product__c;

            if (productYearFinancialMap.containsKey(financial.Proposed_Product__c)) {
                productYearFinancialMap.get(financial.Proposed_Product__c).put(financial.Year__c, financial);
            } else {
                productYearFinancialMap.put(financial.Proposed_Product__c, new Map<String, Financial__c>{financial.Year__c => financial});
            }
            productFinancialIds.add(financial.Id);
            yearSet.add(financial.Year__c);

        }

        update financialList;

        financialList = new List<Financial__c>();
        for (Financial__c financial : [select   Product__c, 
                                                Proposed_Product__c, 
                                                Year__c 
                                        from Financial__c
                                        where RecordType.DeveloperName = 'Global_Financial'
                                        and (Product__c IN: productFinancialIds or Year__c IN: yearSet)]) {
            
            if (productYearFinancialMap.containsKey(financial.Proposed_Product__c)) {
                if (productYearFinancialMap.get(financial.Proposed_Product__c).containsKey(financial.Year__c)) {
                    financial.Product__c = productYearFinancialMap.get(financial.Proposed_Product__c).get(financial.Year__c).Id;
                } else {
                    financial.Product__c = null;
                }
                financialList.add(financial);
            }
        }
        
        update financialList;
    }

    public static void CreateChatterGroups(List<Project__c> projects) {
        ODIN_Project_Auto_Chatter_Group__mdt chatterGroupSettings = ODIN_Project_Auto_Chatter_Group__mdt.getInstance('Chatter_Group_Settings');
        String descriptionMapping = chatterGroupSettings.Description__c;
        String groupNameMapping = chatterGroupSettings.Group_Name__c;
        String accessType = chatterGroupSettings.Access_Type__c;
        Boolean broadcastOnly = chatterGroupSettings.Broadcast_Only__c;
        Boolean allowCustomers = chatterGroupSettings.Allow_Customers__c;
        String ownerId = chatterGroupSettings.Group_Owner__c;
        Set<String> chatterGroupsNames = new Set<String>();

        List<ChatterGroupInfo> chatterGroupsInfo = new List<ChatterGroupInfo>();
        List<CollaborationGroup> chatterGroups = new List<CollaborationGroup>();
        Set<Id> projectsIds = new Set<Id>();

        Map<String, CollaborationGroup> groupsByName = getGroupsByName(projects);

        for (Project__c project : projects) {
            String groupNameValue = groupNameMapping.substringBetween('<<', '>>');
            String groupName = groupNameMapping.replace(groupNameValue, String.valueOf(project.get(groupNameValue))).replace('<<', '').replace('>>', '');

            String owner = ownerId.substringBetween('<<', '>>');
            
            String replaceInDescription = descriptionMapping.substringBetween('<<', '>>');
            String description = descriptionMapping.replace(replaceInDescription, String.valueOf(project.get(replaceInDescription))).replace('<<', '').replace('>>', '');
            
            for (String profileLabel : profiles.values()) {
                CollaborationGroup chatterGroup = new CollaborationGroup();
                String groupNameToAdd = (groupName.length() >= 30 ? groupName.substring(0, 30) : groupName) + profileLabel;
                if (!groupsByName.containsKey(groupNameToAdd.toLowerCase()) && !chatterGroupsNames.contains(groupNameToAdd.toLowerCase())){
                    chatterGroup.Name = groupNameToAdd;
                    chatterGroup.CollaborationType = accessType;
                    chatterGroup.IsBroadcast = broadcastOnly;
                    chatterGroup.Description = description;
                    chatterGroup.CanHaveGuests = allowCustomers;
                    chatterGroup.OwnerId = String.valueOf(project.get(owner));
                    chatterGroups.add(chatterGroup);
                    chatterGroupsNames.add(groupNameToAdd.toLowerCase());
                }else {
                    chatterGroup = groupsByName.get(groupNameToAdd);
                }
                if (chatterGroup != null){
                    chatterGroupsInfo.add(new ChatterGroupInfo(
                        project.Id, project.Name, chatterGroup
                    ));
                    projectsIds.add(project.Id);
                }
            }
        }

        if (!chatterGroups.isEmpty()){
            upsert chatterGroups Name;
        }

        addMembersToGroup(projectsIds, chatterGroupsInfo);
        postToChatter(chatterGroupsInfo);

        if (!chatterGroupsMembersToDelete.isEmpty()){
            delete chatterGroupsMembersToDelete;
        }
    }

    public static void handleAfterUpdate(Map<Id, Project__c> oldProjects, Map<Id, Project__c> newProjects){
        List<CollaborationGroup> groupsToUpdate = new List<CollaborationGroup>();
        Map<String, String> oldToNewProjectNames = new Map<String, String>();

        for (Project__c project : oldProjects.values()) {
            for (String profile : profiles.values()) {
                oldToNewProjectNames.put(project.Name + profile, newProjects.get(project.Id).Name + profile);   
            }
        }

        for (CollaborationGroup chatterGroup : [SELECT Id, Name FROM CollaborationGroup WHERE Name IN: oldToNewProjectNames.keySet()]) {
            String currentGroupName = chatterGroup.Name;
            String newGroupName = oldToNewProjectNames.get(chatterGroup.Name);
            if (currentGroupName != newGroupName){
                chatterGroup.Name = newGroupName;
                groupsToUpdate.add(chatterGroup);
            }
        }
        if (!groupsToUpdate.isEmpty()){
            update groupsToUpdate;
        }
    }

    private static Map<String, CollaborationGroup> getGroupsByName(List<Project__c> projects) {
        Map<String, CollaborationGroup> groupsByName = new Map<String, CollaborationGroup>();
        Set<String> projectsNames = new Set<String>();
        for (Project__c project : projects) {
            for (String profileLabel : profiles.values()) {
                projectsNames.add(project.Name + profileLabel);   
            }
        }
        for (CollaborationGroup collaborationGroup : [SELECT Id, Name, OwnerId FROM CollaborationGroup WHERE Name IN: projectsNames]) {
            groupsByName.put(collaborationGroup.Name.toLowerCase(), collaborationGroup);
        }
        return groupsByName;
    }

    private static void addMembersToGroup(Set<Id> projectsIds, List<ChatterGroupInfo> chatterGroupsInfo) {
        Map<String, CollaborationGroupMember> chatterGroupsMembers = getChatterGroupsMembers(projectsIds);//new Map<String, CollaborationGroupMember>();
        List<CollaborationGroupMember> chatterGroupsMembersToAdd = new List<CollaborationGroupMember>();
        Map<Id, Project__c> projects = new Map<Id, Project__c>([SELECT Id, (SELECT Id, Project__r.Name, User__c, Role__c FROM Project_Roles__r WHERE User__r.IsActive = true) FROM Project__c WHERE Id IN: projectsIds]);
        Boolean userLoggedInGroup = false;
        for (ChatterGroupInfo chatterGroupInfo : chatterGroupsInfo) {
            userLoggedInGroup = false;
            String chatterGroupName = chatterGroupInfo.ChatterGroup.Name;
            List<Project_Role__c> projectRoles = projects.get(chatterGroupInfo.projectId).Project_Roles__r;
            for (Project_Role__c projectRole : projectRoles) {
                String userId = projectRole.User__c;
                String userRole = projectRole.Role__c;
                String projectName = projectRole.Project__r.Name;
                String groupMemberId = chatterGroupName + '_' + userId;
                if (!chatterGroupsMembers.containsKey(groupMemberId) && chatterGroupName == projectName + profiles.get(userRole) && userId != chatterGroupInfo.chatterGroup.OwnerId){
                    CollaborationGroupMember chatterGroupMember = new CollaborationGroupMember(
                        CollaborationGroupId = chatterGroupInfo.chatterGroup.Id,
                        MemberId = userId
                    );
                    if (!chatterGroupsMembersToAdd.contains(chatterGroupMember)){
                        chatterGroupsMembersToAdd.add(chatterGroupMember);
                    }
                }
                if (!userLoggedInGroup){
                    userLoggedInGroup = userId.equals(UserInfo.getUserId()) || chatterGroupInfo.chatterGroup.OwnerId.equals(UserInfo.getUserId());
                }
            }
            if (!userLoggedInGroup){
                CollaborationGroupMember groupMember = new CollaborationGroupMember(
                    CollaborationGroupId = chatterGroupInfo.chatterGroup.Id,
                    MemberId = UserInfo.getUserId()
                );
                if (!chatterGroupsMembersToAdd.contains(groupMember) && UserInfo.getUserId() != chatterGroupInfo.chatterGroup.OwnerId){
                    chatterGroupsMembersToAdd.add(groupMember);
                    chatterGroupsMembersToDelete.add(groupMember);
                }
            }
        }
        if (!chatterGroupsMembersToAdd.isEmpty()){
            insert chatterGroupsMembersToAdd;
        }
    }

    private static Map<String, CollaborationGroupMember> getChatterGroupsMembers(Set<Id> projectIds) {
        Map<String, CollaborationGroupMember> groupsMembers = new Map<String, CollaborationGroupMember>();
        Set<String> projectsNames = new Set<String>();
        for (Project__c project : [SELECT Id, Name FROM Project__c WHERE Id IN: projectIds]) {
            for (String profileLabel : profiles.values()) {
                projectsNames.add(project.Name + profileLabel);
            }
        }
        
        for (CollaborationGroupMember cgm : [SELECT Id, MemberId, CollaborationGroup.Name FROM CollaborationGroupMember WHERE CollaborationGroup.Name IN: projectsNames]) {
            groupsMembers.put(cgm.CollaborationGroup.Name + '_' + cgm.MemberId, cgm);
        }

        return groupsMembers;
    }

    private static void postToChatter(List<ChatterGroupInfo> chatterGroupsInfo) {
        List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();
        for (ChatterGroupInfo chatterGroupInfo : chatterGroupsInfo) {
            CollaborationGroup chatterGroup = chatterGroupInfo.chatterGroup;
            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            
            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
            
            ConnectApi.TextSegmentInput firstTextSegmentInput = new ConnectApi.TextSegmentInput();
            firstTextSegmentInput.text = 'Hey ' + chatterGroupInfo.projectName + ' team here: ';
            messageBodyInput.messageSegments.add(firstTextSegmentInput);

            mentionSegmentInput.id = chatterGroup.Id;
            messageBodyInput.messageSegments.add(mentionSegmentInput);

            ConnectApi.TextSegmentInput nextTextSegmentInput = new ConnectApi.TextSegmentInput();
            nextTextSegmentInput.text = ' is your new Chatter group to collaborate in';
            messageBodyInput.messageSegments.add(nextTextSegmentInput);
            
            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = chatterGroupInfo.projectId;
    
            ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);
            batchInputs.add(batchInput);
        }
        if (!Test.isRunningTest()){
            ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputs);
        }
    }

    private class ChatterGroupInfo {
        String projectId { set; get; }
        String projectName { set; get; }
        CollaborationGroup chatterGroup { set; get; }
        private ChatterGroupInfo(String pProjectId, String pProjectName, CollaborationGroup pGroup){
            projectId = pProjectId;
            projectName = pProjectName;
            chatterGroup = pGroup;
        }
    }
}