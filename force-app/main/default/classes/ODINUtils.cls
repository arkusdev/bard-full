public with sharing class ODINUtils {

    public static Map<String, String> GetRegionsLabels(List<Financial__c> financials) {
        Map<String, Datetime> regionsLastModifiedDates = new Map<String, Datetime>();
        for (Financial__c projectFinancial : financials) {
            String key = projectFinancial.Region__c;
            if (!regionsLastModifiedDates.containsKey(key)){
                regionsLastModifiedDates.put(key, projectFinancial.LastModifiedDate);
            }
        }

        Map<String, String> regions = new Map<String, String>();
        Schema.DescribeFieldResult fieldResult = Financial__c.Region__c.getDescribe();

        for (Schema.PicklistEntry pickListVal : fieldResult.getPicklistValues()) {
            Datetime financialLastModifiedDate = regionsLastModifiedDates.get(pickListVal.getLabel());
            String label = pickListVal.getLabel() + (financialLastModifiedDate != null ? ' - ' + financialLastModifiedDate.date().format() : '');
            regions.put(pickListVal.getValue(), label);
        }
        return regions;
    }
}