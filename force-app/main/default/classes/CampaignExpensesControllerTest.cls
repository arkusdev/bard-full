@isTest
public class CampaignExpensesControllerTest {
	@isTest
	static void allowAccess_UnitTest() { 
		User u = [
			SELECT Id, Name, Phone, Email, Title, IsActive
			FROM User 
			WHERE Email = 'bd@apostletech.com'
		][0]; 
		
		System.runAs(u) { 
			Map<string, boolean> accessMap = CampaignExpensesController.allowAccess();

			System.assertEquals(6, accessMap.size());
		}		
	}
}