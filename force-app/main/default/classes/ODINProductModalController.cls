public with sharing class ODINProductModalController {

    @AuraEnabled
    public static List<String> GetProductTypes() {
        
        List<String> types = new List<String>();
        Schema.DescribeFieldResult fieldResult = Proposed_Product__c.Type__c.getDescribe();

        for (Schema.PicklistEntry pickListVal : fieldResult.getPicklistValues()) 
            types.add(pickListVal.getLabel());

        return types;
    }

    @AuraEnabled
    public static List<Proposed_Product__c> GetProducts(String recordId) {
        List<Proposed_Product__c> ppList = new List<Proposed_Product__c>();
        for (Proposed_Product__c pp : [select   Name, 
                                                (
                                                    select Id 
                                                    from Financials__r 
                                                    where RecordType.DeveloperName = 'Product' 
                                                    LIMIT 1
                                                ) 
                                        from Proposed_Product__c 
                                        where Project__c =: recordId]) {
            if (pp.Financials__r.size() > 0)
                ppList.add(pp);
        }
        return ppList;
    }

    @AuraEnabled
    public static FinancialsWrapper GetFinancials(String recordId, String productId) {
        FinancialsWrapper wrapper   = new FinancialsWrapper();
        wrapper.sampleFinancial     = new Financial__c();
        wrapper.years               = new List<String>();
        wrapper.financials          = new List<Financial__c>();

        for (Financial__c financial : [select   Year__c, 
                                                Estimated_Unit_Cost__c,
                                                Cannibalized_Unit_Cost__c,
                                                Unit_Growth__c,
                                                Cannibalized_Unit_Growth__c, 
                                                Proposed_Product__r.Name
                                        from Financial__c
                                        where Proposed_Product__c =: productId
                                        and Project__c =: recordId
                                        and RecordType.DeveloperName = 'Product'
                                        ORDER BY Year__c ASC
                                        LIMIT 10]) {
            wrapper.financials.add(financial);
            wrapper.years.add(financial.Year__c);
        }
        wrapper.product = new Proposed_Product__c();
        wrapper.product.Name = wrapper.financials[0].Proposed_Product__r.Name;
        return wrapper;
    }

    @AuraEnabled
    public static FinancialsWrapper CreateProduct(String recordId, String productName, String type, Financial__c sampleFinancial){

        Project__c project = [select Launch_Year__c from Project__c where Id =: recordId];

        FinancialsWrapper wrapper   = new FinancialsWrapper();
        wrapper.sampleFinancial     = new Financial__c();
        wrapper.years               = new List<String>();
        wrapper.financials          = new List<Financial__c>();
        Integer year                = String.isBlank(project.Launch_Year__c) ? Date.today().year() : Integer.valueOf(project.Launch_Year__c);

        wrapper.product             = new Proposed_Product__c();
        wrapper.product.Project__c  = recordId;
        wrapper.product.Name        = productName;
        wrapper.product.Type__c     = type;

        Double estUnit      = sampleFinancial.Estimated_Unit_Cost__c;
        Double cannUnit     = sampleFinancial.Cannibalized_Unit_Cost__c;

        RecordType recordType = [select Id 
                                from RecordType 
                                where SObjectType = 'Financial__c' 
                                and RecordType.DeveloperName = 'Product'];    

        for (Integer i = 0; i < 10; i++) {

            wrapper.years.add(String.valueOf(year + i));

            Financial__c financial                  = new Financial__c();
            financial.Project__c                    = recordId;
            financial.Year__c                       = String.valueOf(year + i);
            financial.Year_Sequence__c              = i + 1;
            financial.Estimated_Unit_Cost__c        = estUnit;
            financial.Cannibalized_Unit_Cost__c     = cannUnit;
            financial.Unit_Growth__c                = sampleFinancial.Unit_Growth__c;
            financial.Cannibalized_Unit_Growth__c   = sampleFinancial.Cannibalized_Unit_Growth__c;
            financial.RecordTypeId                  = recordType.Id;
            wrapper.financials.add(financial);
            
            if (sampleFinancial.Unit_Growth__c != null)
                estUnit     = ((estUnit * sampleFinancial.Unit_Growth__c) / 100) + estUnit;
            
            if (sampleFinancial.Cannibalized_Unit_Growth__c != null)
                cannUnit    = ((cannUnit * sampleFinancial.Cannibalized_Unit_Growth__c) / 100) + cannUnit;
        }
        
        return wrapper;
    }

    @AuraEnabled
    public static void SaveFinancials(List<Financial__c> financials, Proposed_Product__c product){
        insert product;

        Financial__c lastFinancial = financials[financials.size() - 1];
        if (lastFinancial.Estimated_Unit_Cost__c != null || lastFinancial.Cannibalized_Unit_Cost__c != null) {
            
            for (Integer i = 1; i <= 5; i++) {
                
                Financial__c financial                  = new Financial__c();
                financial.Project__c                    = lastFinancial.Project__c;
                financial.Year__c                       = String.valueOf(Integer.valueOf(lastFinancial.Year__c) + i);
                financial.Year_Sequence__c              = lastFinancial.Year_Sequence__c + i;
                financial.Estimated_Unit_Cost__c        = lastFinancial.Estimated_Unit_Cost__c;
                financial.Cannibalized_Unit_Cost__c     = lastFinancial.Cannibalized_Unit_Cost__c;
                financial.Unit_Growth__c                = lastFinancial.Unit_Growth__c;
                financial.Cannibalized_Unit_Growth__c   = lastFinancial.Cannibalized_Unit_Growth__c;
                financial.RecordTypeId                  = lastFinancial.RecordTypeId;
                financials.add(financial);
            }
        }

        for (Financial__c financial : financials)
            financial.Proposed_Product__c = product.Id;
            
        insert financials;
    }

    @AuraEnabled
    public static void UpdateFinancials(List<Financial__c> financials){ 
        update financials;
    }

    public class FinancialsWrapper {
        @AuraEnabled
        public List<String> years;
        @AuraEnabled
        public List<Financial__c> financials;
        @AuraEnabled
        public Proposed_Product__c product;
        @AuraEnabled
        public Financial__c sampleFinancial;
    }
}