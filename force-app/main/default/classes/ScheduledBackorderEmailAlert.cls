/**
 * @description class scheduled to run from 10:15 AM ET to 11 every 5 minutes to run the class
 */
global with sharing class ScheduledBackorderEmailAlert implements Schedulable {

  /**
   * @description execute
   * @param an instance of SchedulableContext
   * */
  global void execute(SchedulableContext sc) {
    BackorderEmailAlert.backorderEmailAlertExecute();
  }

}