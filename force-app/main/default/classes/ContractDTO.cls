public class ContractDTO  {
	@auraEnabled 
	public Id contractId { get; set; } 
	@auraEnabled 
	public string contractNumber { get; set; } 
	@auraEnabled 
	public string contractServices { get; set; } 
	@auraEnabled 
	public Date startDate { get; set; } 
	@auraEnabled 
	public Date endDate { get; set; } 
	@auraEnabled 
	public Date campaignStartDate { get; set; } 
	@auraEnabled 
	public Date campaignEndDate { get; set; } 
	@auraEnabled 
	public boolean alertExpiringContractFlg { get; set; } 
	@auraEnabled 
	public boolean alertExpiredContractFlg { get; set; } 
	@auraEnabled 
	public string pageURL { get; set; }

	public ContractDTO(Contract c) {
		this.contractId = c.Id;
		this.contractNumber = c.ContractNumber;
		this.contractServices = (c.Contract_Services__c != null ? c.Contract_Services__c.replace(';', ', ') : '');
		this.startDate = c.StartDate;
		this.endDate = c.EndDate;
		this.campaignStartDate = null;
		this.campaignEndDate = null;
		this.alertExpiringContractFlg = false;
		this.alertExpiredContractFlg = false;		
		this.pageURL = PageNavUtil.getURL(c.Id);
	}

	public ContractDTO(Contract c, date campaignStartDate, date campaignEndDate) {
		this.contractId = c.Id;
		this.contractNumber = c.ContractNumber;
		this.contractServices = (c.Contract_Services__c != null ? c.Contract_Services__c.replace(';', ', ') : '');
		this.startDate = c.StartDate;
		this.endDate = c.EndDate;
		this.campaignStartDate = campaignStartDate;
		this.campaignEndDate = campaignEndDate;
		this.alertExpiringContractFlg = (this.endDate >= Date.today() && this.endDate <= Date.today().addDays(90));
		if (this.campaignStartDate != null && this.campaignEndDate != null) {
			this.alertExpiredContractFlg = ((this.startDate >= this.campaignStartDate && this.startDate <= this.campaignEndDate) || 
											(this.endDate >= this.campaignStartDate && this.endDate <= this.campaignEndDate) || 
											(this.endDate < this.campaignStartDate && this.endDate < this.campaignEndDate));				
		} else {
			this.alertExpiredContractFlg = false;
		}

		this.pageURL = PageNavUtil.getURL(c.Id);
	}
}