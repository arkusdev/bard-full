public class CampaignMemberTriggerHandler extends TriggerHandler {
	private final string Waitlist = 'Waitlist';
	private final string Attendee = 'Attendee';

	/* overrides */
	protected override void afterInsert() {
		Map<Id, Set<Id>> campaignMemberMap = getCampaignMemberMap(true);

		insertCampaignMemberDocuments(campaignMemberMap);
		setProctorName((List<CampaignMember>)trigger.new);
	}

	protected override void afterUpdate() {
		Map<string, Map<Id, Set<Id>>> campaignMemberTypeMap = getCampaignMemberTypeMap();

		if (!campaignMemberTypeMap.isEmpty()) {
			if (campaignMemberTypeMap.containsKey(this.Waitlist)) {
				deleteCampaignMeals(campaignMemberTypeMap.get(this.Waitlist));
				deleteCampaignFlights(campaignMemberTypeMap.get(this.Waitlist));
				deleteCampaignTransportations(campaignMemberTypeMap.get(this.Waitlist));
				deleteCampaignIncidentals(campaignMemberTypeMap.get(this.Waitlist));
				deleteCampaignHotels(campaignMemberTypeMap.get(this.Waitlist));
				deleteCampaignProctorFees(campaignMemberTypeMap.get(this.Waitlist)); 
				deleteCampaignMemberDocuments(campaignMemberTypeMap.get(this.Waitlist));			
			}

			if (campaignMemberTypeMap.containsKey(this.Attendee)) {
				insertCampaignMemberDocuments(campaignMemberTypeMap.get(this.Attendee));	
			}
		}	
	}

	protected override void afterDelete() {
		Map<Id, Set<Id>> campaignMemberMap = getCampaignMemberMap(false);

		if (!campaignMemberMap.isEmpty()) {
			deleteCampaignMeals(campaignMemberMap);
			deleteCampaignFlights(campaignMemberMap);
			deleteCampaignTransportations(campaignMemberMap);
			deleteCampaignIncidentals(campaignMemberMap);
			deleteCampaignHotels(campaignMemberMap);
			deleteCampaignProctorFees(campaignMemberMap); 
			deleteCampaignMemberDocuments(campaignMemberMap);		
		}

		setProctorName((List<CampaignMember>)trigger.old);
	}

	/* private methods */
	private Map<string, Map<Id, Set<Id>>> getCampaignMemberTypeMap() { 
		Map<string, Map<Id, Set<Id>>> campaignMemberMap = new Map<string, Map<Id, Set<Id>>>();

		Map<Id, CampaignMember> newMap = (Map<Id, CampaignMember>)trigger.newMap;
		Map<Id, CampaignMember> oldMap = (Map<Id, CampaignMember>)trigger.oldMap;

		List<CampaignMember> waitlist = new List<CampaignMember>();
		List<CampaignMember> attendee = new List<CampaignMember>();

		for(Id cid :newMap.keySet()) {
			if (oldMap.containsKey(cid)) {
				if (oldMap.get(cid).Type__c.equalsIgnoreCase(this.Attendee) && newMap.get(cid).Type__c.equalsIgnoreCase(this.Waitlist)) { 
					waitlist.add(newMap.get(cid));	
				} else if (oldMap.get(cid).Type__c.equalsIgnoreCase(this.Waitlist) && newMap.get(cid).Type__c.equalsIgnoreCase(this.Attendee)) { 
					attendee.add(newMap.get(cid));
				}
			} 
		}
		
		if (!waitlist.isEmpty()) {
			for(CampaignMember w: waitlist) {
				if (!campaignMemberMap.containsKey(this.Waitlist)) {
					campaignMemberMap.put(this.Waitlist, new Map<Id, Set<Id>> {
						w.CampaignId => new Set<Id> { w.ContactId }
					});
				} else {
					if (!campaignMemberMap.get(this.Waitlist).containsKey(w.CampaignId)) {
						campaignMemberMap.get(this.Waitlist).put(w.CampaignId, new Set<Id> { w.ContactId });
					} else {
						campaignMemberMap.get(this.Waitlist).get(w.CampaignId).add(w.ContactId);
					}
				}
			}
		}	

		if (!attendee.isEmpty()) {
			for(CampaignMember a: attendee) {
				if (!campaignMemberMap.containsKey(this.Attendee)) {
					campaignMemberMap.put(this.Attendee, new Map<Id, Set<Id>> {
						a.CampaignId => new Set<Id> { a.ContactId }
					});
				} else {
					if (!campaignMemberMap.get(this.Attendee).containsKey(a.CampaignId)) {
						campaignMemberMap.get(this.Attendee).put(a.CampaignId, new Set<Id> { a.ContactId });
					} else {
						campaignMemberMap.get(this.Attendee).get(a.CampaignId).add(a.ContactId);
					}
				}
			}
		}
		
		return campaignMemberMap;	
	}

	private Map<Id, Set<Id>> getCampaignMemberMap(boolean attendeeFlg) {
		List<CampaignMember> cms = ((List<CampaignMember>)(trigger.isDelete ? trigger.old : trigger.new));

		Map<Id, Set<Id>> campaignMemberMap = new Map<Id, Set<Id>>();
		
		for(CampaignMember cm: cms) {
			if(attendeeFlg && cm.Type__c != this.Attendee) { continue; }

			if (!campaignMemberMap.keySet().contains(cm.CampaignId)) {
				campaignMemberMap.put(cm.CampaignId, new Set<Id>{ cm.ContactId });
			} else {
				campaignMemberMap.get(cm.CampaignId).add(cm.ContactId);
			}
		}

		return campaignMemberMap;
	}

	private void insertCampaignMemberDocuments(Map<Id, Set<Id>> campaignMemberMap) {
		if (!campaignMemberMap.isEmpty()) { 
			List<Campaign_Document__c> campaignDocs = [
				SELECT Id, Campaign__c
				FROM Campaign_Document__c
				WHERE Campaign__c IN :campaignMemberMap.keySet()
			];

			if (!campaignDocs.isEmpty()) {
				Map<Id, Set<Id>> campaignDocMap = new Map<Id, Set<Id>>();	

				for(Campaign_Document__c cd :campaignDocs) {
					if (!campaignDocMap.containsKey(cd.Campaign__c)) {
						campaignDocMap.put(cd.Campaign__c, new Set<Id>{ cd.Id });
					} else {
						campaignDocMap.get(cd.Campaign__c).add(cd.Id);
					}
				}

				if (!campaignDocMap.isEmpty()) {
					List<Campaign_Attendee_Document__c> memberDocs = new List<Campaign_Attendee_Document__c>();

					for(Id campaignId :campaignMemberMap.keySet()) {
						if (campaignDocMap.containsKey(campaignId)) {
							for(Id contactId :campaignMemberMap.get(campaignId)) {
								for(Id docId :campaignDocMap.get(campaignId)) {
									memberDocs.add(new Campaign_Attendee_Document__c(
										Campaign_Document__c = docId,
										Contact__c = contactId,
										Received__c = false
									));
								}
							}
						}
					}

					if (!memberDocs.isEmpty()) {
						insert memberDocs;
					}
				}
			}		
		}
	}

	private void deleteCampaignMeals(Map<Id, Set<Id>> campaignMemberMap) {
		if (!campaignMemberMap.isEmpty()) {
			//Next find all of the meals & associated attendees under every campaign in the map
			List<Campaign_Meal__c> meals = [
				SELECT Id, Campaign__c,
					(SELECT Id, Contact__c FROM Campaign_Meal_Attendees__r WHERE User__c = NULL)
				FROM Campaign_Meal__c
				WHERE Campaign__c IN :campaignMemberMap.keySet()
				ORDER BY Campaign__c
			];

			if (!meals.isEmpty()) {
				//Now iterate over the meals & associated attendees
				//Check if each contact exists in the map and add the id of the meal rec to be deleted
				List<Campaign_Meal_Attendee__c> attendees = new List<Campaign_Meal_Attendee__c>();

				for(Campaign_Meal__c cm: meals) {
					if (campaignMemberMap.keySet().contains(cm.Campaign__c)) {
						for(Campaign_Meal_Attendee__c cma : cm.Campaign_Meal_Attendees__r) {
							if (campaignMemberMap.get(cm.Campaign__c).contains(cma.Contact__c)) {
								attendees.add(new Campaign_Meal_Attendee__c(
									Id = cma.Id
								));
							}					
						}						
					}
				}

				if (!attendees.isEmpty()) {
					delete attendees;
				}
			}
		}
	}

	private void deleteCampaignFlights(Map<Id, Set<Id>> campaignMemberMap) {
		if (!campaignMemberMap.isEmpty()) {
			//Next find all of the flights associated to each contact in the map
			List<Campaign_Flight__c> flights = [
				SELECT Id, Campaign__c, Contact__c
				FROM Campaign_Flight__c
				WHERE Campaign__c IN :campaignMemberMap.keySet()
				ORDER BY Campaign__c
			];

			if (!flights.isEmpty()) {
				//Now iterate over the flights & associated attendees
				//Check if each contact exists in the map and add the id of the meal rec to be deleted
				List<Campaign_Flight__c> fd = new List<Campaign_Flight__c>();

				for(Campaign_Flight__c cm: flights) {
					if (campaignMemberMap.keySet().contains(cm.Campaign__c)) {
						if (campaignMemberMap.get(cm.Campaign__c).contains(cm.Contact__c)) {
							fd.add(new Campaign_Flight__c(
								id = cm.Id
							));
						}
					}
				}

				if (!fd.isEmpty()) {
					delete fd;
				}
			}
		}
	}

	private void deleteCampaignTransportations(Map<Id, Set<Id>> campaignMemberMap) {
		if (!campaignMemberMap.isEmpty()) {
			//Next find all of the flights & associated attendees under every campaign in the map
			List<Campaign_Transportation__c> trans = [
				SELECT Id, Campaign__c,
					(SELECT Id, Contact__c FROM Campaign_Transportation_Attendees__r WHERE User__c = NULL)
				FROM Campaign_Transportation__c
				WHERE Campaign__c IN :campaignMemberMap.keySet()
				ORDER BY Campaign__c
			];

			if (!trans.isEmpty()) {
				//Now iterate over the trans & associated attendees
				//Check if each contact exists in the map and add the id of the meal rec to be deleted
				List<Campaign_Transportation_Attendee__c> attendees = new List<Campaign_Transportation_Attendee__c>();

				for(Campaign_Transportation__c cm: trans) {
					if (campaignMemberMap.keySet().contains(cm.Campaign__c)) {
						for(Campaign_Transportation_Attendee__c cta : cm.Campaign_Transportation_Attendees__r) {
							if (campaignMemberMap.get(cm.Campaign__c).contains(cta.Contact__c)) {
								attendees.add(new Campaign_Transportation_Attendee__c(
									Id = cta.Id
								));
							}					
						}						
					}
				}

				if (!attendees.isEmpty()) {
					delete attendees;
				}
			}
		}
	}

	private void deleteCampaignIncidentals(Map<Id, Set<Id>> campaignMemberMap) {
		if (!campaignMemberMap.isEmpty()) {
			//Next find all of the incidentals & associated attendees under every campaign in the map
			List<Campaign_Incidental__c> incidentals = [
				SELECT Id, Campaign__c,
					(SELECT Id, Contact__c FROM Campaign_Incidental_Attendees__r WHERE User__c = NULL)
				FROM Campaign_Incidental__c
				WHERE Campaign__c IN :campaignMemberMap.keySet()
				ORDER BY Campaign__c
			];

			if (!incidentals.isEmpty()) {
				//Now iterate over the incidentals & associated attendees
				//Check if each contact exists in the map and add the id of the meal rec to be deleted
				List<Campaign_Incidental_Attendee__c> attendees = new List<Campaign_Incidental_Attendee__c>();

				for(Campaign_Incidental__c cm: incidentals) {
					if (campaignMemberMap.keySet().contains(cm.Campaign__c)) {
						for(Campaign_Incidental_Attendee__c cia : cm.Campaign_Incidental_Attendees__r) {
							if (campaignMemberMap.get(cm.Campaign__c).contains(cia.Contact__c)) {
								attendees.add(new Campaign_Incidental_Attendee__c(
									Id = cia.Id
								));
							}					
						}						
					}
				}

				if (!attendees.isEmpty()) {
					delete attendees;
				}
			}
		}
	}

	private void deleteCampaignHotels(Map<Id, Set<Id>> campaignMemberMap) {
		if (!campaignMemberMap.isEmpty()) {
			//Next find all of the hotels & associated attendees under every campaign in the map
			List<Campaign_Hotel__c> hotels = [
				SELECT Id, Campaign__c,
					(SELECT Id, Contact__c FROM Campaign_Hotel_Attendees__r WHERE User__c = NULL)
				FROM Campaign_Hotel__c
				WHERE Campaign__c IN :campaignMemberMap.keySet()
				ORDER BY Campaign__c
			];

			if (!hotels.isEmpty()) {
				//Now iterate over the hotels & associated attendees
				//Check if each contact exists in the map and add the id of the meal rec to be deleted
				List<Campaign_Hotel_Attendee__c> attendees = new List<Campaign_Hotel_Attendee__c>();

				for(Campaign_Hotel__c ch: hotels) {
					if (campaignMemberMap.keySet().contains(ch.Campaign__c)) {
						for(Campaign_Hotel_Attendee__c cha : ch.Campaign_Hotel_Attendees__r) {
							if (campaignMemberMap.get(ch.Campaign__c).contains(cha.Contact__c)) {
								attendees.add(new Campaign_Hotel_Attendee__c(
									Id = cha.Id
								));
							}					
						}						
					}
				}

				if (!attendees.isEmpty()) {
					delete attendees;
				}
			}
		}
	}

	private void deleteCampaignProctorFees(Map<Id, Set<Id>> campaignMemberMap) {
		if (!campaignMemberMap.isEmpty()) {
			//Next find all of the proctor fees under every campaign in the map
			List<Campaign_Proctor_Fee__c> fees = [
				SELECT Id, Campaign__c, Proctor__c
				FROM Campaign_Proctor_Fee__c
				WHERE Campaign__c IN :campaignMemberMap.keySet()
				ORDER BY Campaign__c
			];

			if (!fees.isEmpty()) {
				//Now iterate over the fees & check if each proctor exists in the map
				//Check if each contact exists in the map and add the id of the meal rec to be deleted
				List<Campaign_Proctor_Fee__c> proctors = new List<Campaign_Proctor_Fee__c>();

				for(Campaign_Proctor_Fee__c pf: fees) {
					if (campaignMemberMap.keySet().contains(pf.Campaign__c)) {
						if (campaignMemberMap.get(pf.Campaign__c).contains(pf.Proctor__c)) {
							proctors.add(new Campaign_Proctor_Fee__c(
								Id = pf.Id
							));
						}
					}
				}

				if (!proctors.isEmpty()) {
					delete proctors;
				}
			}
		}
	}

	private void deleteCampaignMemberDocuments(Map<Id, Set<Id>> campaignMemberMap) {
		if (!campaignMemberMap.isEmpty()) {
			//Next find all of the member docs under every campaign member in the map
			List<Campaign_Attendee_Document__c> docs = [
				SELECT Id, Campaign_Document__r.Campaign__c, Contact__c
				FROM Campaign_Attendee_Document__c
				WHERE Campaign_Document__r.Campaign__c IN :campaignMemberMap.keySet()
				AND User__c = NULL
				ORDER BY Campaign_Document__r.Name
			];

			if(!docs.isEmpty()) {
				//Now iterate over the docs & check if each contact exists in the map
				//Check if each contact exists in the map and add the id of the doc rec to be deleted
				List<Campaign_Attendee_Document__c> memberDocs = new List<Campaign_Attendee_Document__c>();

				for(Campaign_Attendee_Document__c cmd :docs) {
					if (campaignMemberMap.keySet().contains(cmd.Campaign_Document__r.Campaign__c)) {
						if (campaignMemberMap.get(cmd.Campaign_Document__r.Campaign__c).contains(cmd.Contact__c)) {
							memberDocs.add(new Campaign_Attendee_Document__c(
								Id = cmd.Id
							));
						}
					}
				}

				if (!memberDocs.isEmpty()) {
					delete memberDocs;
				}
			}
		}
	}

    public static void setProctorName(List<CampaignMember> memberList){
        set<Id> campaignIds = new set<Id>();

        for(CampaignMember cm : memberList){
            if(cm.type__c == 'Proctor')
                campaignIds.add(cm.campaignId);
        }
        
        List<Campaign> campaignList = [
			select id, Course_Proctor__c, 
				(select id, type__c, contactId, Contact.Name from campaignmembers where type__c =: 'Proctor' order by createddate ASC limit 1) 
            from campaign 
			where id in: campaignids
		];
        
        for(Campaign c : campaignList){
            c.Course_Proctor__c = '';
            if(c.campaignMembers.size()>0) {
                if(c.campaignMembers[0].contactId != null)
                    c.Course_Proctor__c = c.campaignMembers[0].contact.Name;
            }
        }
        
        if(campaignList.size() > 0)
            update campaignList;
    }
}