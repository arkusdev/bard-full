public class HCPExpenseController
{
    public string campaignId{Get;set;}
    public campaign campInfo{get;set;}
    public List<campaignMember> memberList{Get;set;}
    public List<innerClass> innerList{get;set;}
    public decimal totalCost{get;set;}
    public string reportType{get;set;}
    public string reportName{Get;set;}
    
    //Addition functionality
    public decimal additionalMealCost{get;set;}
    public HCPExpenseController()
    {
        totalCost = 0;
        additionalMealCost = 0;
        set<Id> conKeyset = new set<Id>();
        set<Id> userKeyset = new set<Id>();
        Map<Id,campaignMember> contactMap = new Map<Id,campaignMember>();
        Map<Id,campaign_user__c> userMap= new Map<Id,campaign_user__c>();
        innerList = new List<innerClass>();
        campInfo = new campaign();
        campaignId = system.currentPagereference().getParameters().get('id');
        reportType = system.currentPagereference().getParameters().get('type');
        memberList = new List<CampaignMember>();
        
        campInfo = [select id,Classification__c,name,SubLedger__c,StartDate,Products_Used__c,Course_Location__c,Course_Location_State__c,
                    (select id,contactId,campaign.facility__r.billingcity,campaign.facility__r.billingstate from campaignMembers where contactId != null and type__c != 'Waitlist'),
                    (select id,User__c,campaign__r.facility__r.billingcity,campaign__r.facility__r.billingstate from Campaign_Users__r) from campaign where id =: campaignId];
        
        if(campInfo.StartDate != null){
            String formatedDate = campInfo.startDate.year()+'-'+campInfo.startDate.month()+'-'+campInfo.startDate.day();
            reportName = campInfo.Name+'-'+formatedDate+'-'+campInfo.SubLedger__c;
        }
        else
            reportName = campInfo.Name+'-'+campInfo.SubLedger__c;
        
        memberList = campInfo.campaignMembers;        
        
        for(campaignMember cm : memberList){
            contactMap.put(cm.contactId,cm);
        }
        if(!contactMap.isEmpty())
            conKeyset = contactMap.keyset();
        
        String conStringQuery = 'SELECT Id,Covered_Recipient_Type__c,LastName,MiddleName,FirstName,suffix,Global_ID__c ';
        //Meals
        if(reportType == 'Meals' || reportType == 'All')
            conStringQuery += ', (SELECT Id,User__c,contact__c,Campaign_Meal__r.Cost_Per_Attendee__c,Campaign_Meal__r.Additional_Attendees__c,Campaign_Meal__r.Type__c,Campaign_Meal__c,campaign_Meal__r.Additional_Attendee_Cost__c FROM Campaign_Meal_Attendees__r WHERE Campaign_Meal__r.Campaign__c =: campaignId)';
        //Flights
        if(reportType == 'Flights' || reportType == 'All')
            conStringQuery += ', (SELECT id,Contact__c,Cost__c,Campaign__c,Campaign__r.facility__r.billingcity,Campaign__r.facility__r.billingState from Campaign_Flights__r WHERE Campaign__c =: campaignId)';
        //Hotels
        if(reportType == 'Hotels' || reportType == 'All')
            conStringQuery += ', (SELECT id,Contact__c,Campaign_Hotel__r.campaign__c,Cost__c from Campaign_Hotel_Attendees__r where Campaign_Hotel__r.campaign__c =: campaignId)';
        //Transportation
        if(reportType == 'Transportation' || reportType == 'All')
            conStringQuery += ', (SELECT id,Campaign_Transportation__r.campaign__c,Campaign_Transportation__r.Total_Cost__c from Campaign_Transportation_Attendee__r where Campaign_Transportation__r.campaign__c =: campaignId)';
        //Incidental
        if(reportType == 'Additional Course Costs' || reportType == 'All')
            conStringQuery += ', (SELECT id,Campaign_Incidental__r.campaign__c,Campaign_Incidental__r.Total_Cost__c,Campaign_Incidental__r.Transfer_of_Value__c,Campaign_Incidental__r.Cost_Per_Attendee__c from Campaign_Incidental_Attendees__r where Campaign_Incidental__r.campaign__c =: campaignId)';
        //Procure
        if(reportType == 'Proctor Fees' || reportType == 'All')
            conStringQuery += ', (SELECT id,Total_Cost__c,campaign__c,Transfer_of_Value__c from Campaign_Proctor_Fees__r where campaign__c =: campaignId)';
        
        conStringQuery += 'FROM Contact WHERE Id IN: conKeyset';
        system.debug('***'+conStringQuery);
        List<Contact> conList = database.query(conStringQuery);
        for(Contact c : conList)
        {
            InnerClass inn = new InnerClass();
            inn.conInfo = c;
            
            
            //Calculate Meals per attendee
            for(Campaign_Meal_Attendee__c cma : c.Campaign_Meal_Attendees__r)
            {   
                Decimal totalCost = cma.Campaign_Meal__r.Cost_Per_Attendee__c != null ? cma.Campaign_Meal__r.Cost_Per_Attendee__c : 0;
                
                inn.mealsDetails.transactionType = 'Food and Beverage';
                inn.mealsDetails.totalCost += totalCost;      
                inn.additionalMealCost = cma.campaign_Meal__r.Additional_Attendee_Cost__c;          
            }
            if(!c.Campaign_Meal_Attendees__r.isEmpty())
                inn.costDetailsList.add(inn.mealsDetails);
            
            
            //Calculate Flights per attendee
            for(Campaign_Flight__c cf : c.Campaign_Flights__r)
            {   
                Decimal totalCost = cf.Cost__c != null ? cf.Cost__c : 0;
                inn.flightDetails.transactionType = 'Travel';
                inn.flightDetails.totalCost += totalCost;
                inn.additionalFlightCost = 0;
                inn.flightDetails.city = string.isNotBlank(contactMap.get(c.id).campaign.facility__r.billingcity) ? contactMap.get(c.id).campaign.facility__r.billingcity : '';
                inn.flightDetails.state = string.isNotBlank(contactMap.get(c.id).campaign.facility__r.billingState) ? contactMap.get(c.id).campaign.facility__r.billingState : '';
            }
            if(!c.Campaign_Flights__r.isEmpty())
                inn.costDetailsList.add(inn.flightDetails); 
            
            
            //Calculate Hotels per attendee
            for(Campaign_Hotel_Attendee__c cha : c.Campaign_Hotel_Attendees__r)
            {   
                Decimal totalCost = cha.Cost__c != null ? cha.Cost__c : 0;
                inn.hotelDetails.transactionType = 'Travel';
                inn.hotelDetails.totalCost += totalCost;
                inn.additionalHotelCost = 0;
                inn.hotelDetails.city = string.isNotBlank(contactMap.get(c.id).campaign.facility__r.billingcity) ? contactMap.get(c.id).campaign.facility__r.billingcity : '';
                inn.hotelDetails.state = string.isNotBlank(contactMap.get(c.id).campaign.facility__r.billingState) ? contactMap.get(c.id).campaign.facility__r.billingState : '';
            }
            if(!c.Campaign_Hotel_Attendees__r.isEmpty())
                inn.costDetailsList.add(inn.hotelDetails);
            
            
            //Calculate Transport details per attendee
            for(Campaign_Transportation_Attendee__c cta: c.Campaign_Transportation_Attendee__r)
            {   
                Decimal totalCost = cta.Campaign_Transportation__r.Total_Cost__c != null ? cta.Campaign_Transportation__r.Total_Cost__c : 0;
                inn.transportDetails.transactionType = 'Travel';
                inn.transportDetails.city = string.isNotBlank(contactMap.get(c.id).campaign.facility__r.billingcity) ? contactMap.get(c.id).campaign.facility__r.billingcity : '';
                inn.transportDetails.state = string.isNotBlank(contactMap.get(c.id).campaign.facility__r.billingState) ? contactMap.get(c.id).campaign.facility__r.billingState : '';
                inn.transportDetails.totalCost += totalCost;
                inn.additionalTransportCost = 0;
            }
            if(!c.Campaign_Transportation_Attendee__r.isEmpty())
                inn.costDetailsList.add(inn.transportDetails); 
                
            
            //Calculate Incident details per attendee
            for(Campaign_Incidental_Attendee__c cia: c.Campaign_Incidental_Attendees__r)
            {   
                Decimal totalCost = cia.Campaign_Incidental__r.Cost_Per_Attendee__c != null ? cia.Campaign_Incidental__r.Cost_Per_Attendee__c : 0;
                inn.incidentDetails.transactionType = cia.Campaign_Incidental__r.Transfer_of_Value__c != null ? cia.Campaign_Incidental__r.Transfer_of_Value__c : 'No value';
                inn.incidentDetails.totalCost += totalCost;
                inn.additionalIncidentCost = 0;
            }
            if(!c.Campaign_Incidental_Attendees__r.isEmpty())
                inn.costDetailsList.add(inn.incidentDetails); 
            
            
            //Calculate Proctor details per attendee
            for(Campaign_Proctor_Fee__c cpf: c.Campaign_Proctor_Fees__r)
            {   
                Decimal totalCost = cpf.Total_Cost__c != null ? cpf.Total_Cost__c : 0;
                inn.proctorDetails.transactionType = cpf.Transfer_of_Value__c <> null?cpf.Transfer_of_Value__c:'';
                inn.proctorDetails.totalCost += totalCost;
                inn.additionalProctorCost = 0;
            }
            if(!c.Campaign_Proctor_Fees__r.isEmpty())
                inn.costDetailsList.add(inn.proctorDetails);
                
                
            inn.totalCost = (inn.mealsDetails.totalCost + inn.flightDetails.totalCost + inn.hotelDetails.totalCost + inn.incidentDetails.totalCost + inn.transportDetails.totalCost + inn.proctorDetails.totalCost);
            totalCost += inn.totalCost;
            innerList.add(inn);        
        }
        
        List<Campaign_User__c> userList = campInfo.Campaign_Users__r;
        for(Campaign_User__c cu : userList){
            userMap.put(cu.user__c,cu);            
        }
        if(!userMap.isEmpty()){
            userKeySet = userMap.keyset();
        }
        
        string userStringQuery = 'select id,firstname,lastname,MiddleName,suffix';
        //Meals
        if(reportType == 'Meals' || reportType == 'All')
            userStringQuery += ', (SELECT Id,User__c,Campaign_Meal__r.Cost_Per_Attendee__c,Campaign_Meal__r.Type__c,Campaign_Meal__c,campaign_Meal__r.Additional_Attendee_Cost__c FROM Campaign_Meal_Attendees__r WHERE Campaign_Meal__r.Campaign__c =: campaignId)';
        //Flights
        if(reportType == 'Flights' || reportType == 'All')
            userStringQuery += ', (SELECT id,user__c,Cost__c,Campaign__c from Campaign_Flights__r WHERE Campaign__c =: campaignId)';
        //Hotels
        if(reportType == 'Hotels' || reportType == 'All')
            userStringQuery += ', (SELECT id,user__c,Campaign_Hotel__r.campaign__c,Cost__c from Campaign_Hotel_Attendees__r where Campaign_Hotel__r.campaign__c =: campaignId)';
        //Tranportation
        if(reportType == 'Transportation' || reportType == 'All')
            userStringQuery += ', (SELECT id,Campaign_Transportation__r.campaign__c,Campaign_Transportation__r.Total_Cost__c from Campaign_Transportation_Attendee__r where Campaign_Transportation__r.campaign__c =: campaignId)';
        //Incidents
        if(reportType == 'Additional Course Costs' || reportType == 'All')
            userStringQuery += ', (SELECT id,Campaign_Incidental__r.campaign__c,Campaign_Incidental__r.Total_Cost__c,Campaign_Incidental__r.Transfer_of_Value__c from Campaign_Incidental_Attendees__r where Campaign_Incidental__r.campaign__c =: campaignId)';
        userStringQuery += ' from user where id in: userKeySet';
        List<User> user_List = database.query(userStringQuery);
        for(User u : user_List)
        {
            innerClass inn = new innerClass();
            inn.userInfo = u;
            
            //Calculate Meals per attendee
            for(Campaign_Meal_Attendee__c cma : u.Campaign_Meal_Attendees__r)
            {   
                Decimal totalCost = cma.Campaign_Meal__r.Cost_Per_Attendee__c != null ? cma.Campaign_Meal__r.Cost_Per_Attendee__c : 0;                
                inn.mealsDetails.transactionType = 'Food and Beverage';
                inn.mealsDetails.totalCost += totalCost;   
                inn.additionalMealCost = cma.campaign_Meal__r.Additional_Attendee_Cost__c;             
            }
            if(!u.Campaign_Meal_Attendees__r.isEmpty())
                inn.costDetailsList.add(inn.mealsDetails);
            
            
            //Calculate Flights per attendee
            for(Campaign_Flight__c cf : u.Campaign_Flights__r)
            {   
                Decimal totalCost = cf.Cost__c != null ? cf.Cost__c : 0;
                inn.flightDetails.transactionType = 'Travel';
                inn.flightDetails.city = string.isNotBlank(userMap.get(u.id).campaign__r.facility__r.billingcity) ? userMap.get(u.id).campaign__r.facility__r.billingcity : '';
                inn.flightDetails.state = string.isNotBlank(userMap.get(u.id).campaign__r.facility__r.billingState) ? userMap.get(u.id).campaign__r.facility__r.billingState : '';
                inn.flightDetails.totalCost += totalCost;
                inn.additionalFlightCost = 0;
            }
            if(!u.Campaign_Flights__r.isEmpty())
                inn.costDetailsList.add(inn.flightDetails); 
            
            
            //Calculate Hotels per attendee
            for(Campaign_Hotel_Attendee__c cha : u.Campaign_Hotel_Attendees__r)
            {   
                Decimal totalCost = cha.Cost__c != null ? cha.Cost__c : 0;
                inn.hotelDetails.transactionType = 'Travel';
                inn.hotelDetails.city = string.isNotBlank(userMap.get(u.id).campaign__r.facility__r.billingcity) ? userMap.get(u.id).campaign__r.facility__r.billingcity : '';
                inn.hotelDetails.state = string.isNotBlank(userMap.get(u.id).campaign__r.facility__r.billingState) ? userMap.get(u.id).campaign__r.facility__r.billingState : '';
                inn.hotelDetails.totalCost += totalCost;
                inn.additionalHotelCost = 0;
            }
            if(!u.Campaign_Hotel_Attendees__r.isEmpty())
                inn.costDetailsList.add(inn.hotelDetails);
            
            
            //Calculate Transport details per attendee
            for(Campaign_Transportation_Attendee__c cta: u.Campaign_Transportation_Attendee__r)
            {   
                Decimal totalCost = cta.Campaign_Transportation__r.Total_Cost__c != null ? cta.Campaign_Transportation__r.Total_Cost__c : 0;
                inn.transportDetails.transactionType = 'Travel';
                inn.transportDetails.city = string.isNotBlank(userMap.get(u.id).campaign__r.facility__r.billingcity) ? userMap.get(u.id).campaign__r.facility__r.billingcity : '';
                inn.transportDetails.state = string.isNotBlank(userMap.get(u.id).campaign__r.facility__r.billingState) ? userMap.get(u.id).campaign__r.facility__r.billingState : '';
                inn.transportDetails.totalCost += totalCost;
                inn.additionalTransportCost = 0;
            }
            if(!u.Campaign_Transportation_Attendee__r.isEmpty())
                inn.costDetailsList.add(inn.transportDetails); 
                
            
            //Calculate Incident details per attendee
            for(Campaign_Incidental_Attendee__c cia: u.Campaign_Incidental_Attendees__r)
            {   
                Decimal totalCost = cia.Campaign_Incidental__r.Total_Cost__c != null ? cia.Campaign_Incidental__r.Total_Cost__c : 0;
                inn.incidentDetails.transactionType = cia.Campaign_Incidental__r.Transfer_of_Value__c != null ? cia.Campaign_Incidental__r.Transfer_of_Value__c : 'No value';
                inn.incidentDetails.totalCost += totalCost;
                inn.additionalIncidentCost = 0;
            }
            if(!u.Campaign_Incidental_Attendees__r.isEmpty())
                inn.costDetailsList.add(inn.incidentDetails); 
            
            
            
            inn.totalCost = (inn.mealsDetails.totalCost + inn.flightDetails.totalCost + inn.hotelDetails.totalCost + inn.incidentDetails.totalCost + inn.transportDetails.totalCost);
            totalCost += inn.totalCost;
            innerList.add(inn);
        }        
    }
    
    public class innerClass
    {
        public contact conInfo{get;set;}
        public user userInfo{get;set;}
        public Decimal totalCost{set;get;}
        public String city{get;set;}
        public String state{get;set;}
        
        public Decimal additionalMealCost{set;get;}
        public Decimal additionalFlightCost{set;get;}
        public Decimal additionalHotelCost{set;get;}
        public Decimal additionalTransportCost{set;get;}
        public Decimal additionalIncidentCost{set;get;}
        public Decimal additionalProctorCost{set;get;}
        
        public CostDetailInner mealsDetails{set;get;}
        public CostDetailInner flightDetails{set;get;}
        public CostDetailInner hotelDetails{set;get;}
        public CostDetailInner transportDetails{set;get;}
        public CostDetailInner incidentDetails{set;get;}
        public CostDetailInner proctorDetails{set;get;}        
        public list<CostDetailInner> costDetailsList{set;get;}
        public innerClass()
        {
            totalCost = 0;
            additionalMealCost = 0;
            additionalFlightCost = 0;
            additionalHotelCost = 0;
            additionalTransportCost = 0;
            additionalIncidentCost = 0;
            additionalProctorCost = 0;
            mealsDetails = new CostDetailInner();
            flightDetails = new CostDetailInner();
            hotelDetails = new CostDetailInner();
            transportDetails = new CostDetailInner(); 
            incidentDetails = new CostDetailInner();
            proctorDetails = new CostDetailInner();
            costDetailsList = new list<CostDetailInner>();
        }
    }
    public class CostDetailInner
    {
        public String transactionType{get;set;}
        public Decimal totalCost{get;set;}
        public String city{get;set;}
        public String state{get;set;}
        public CostDetailInner()
        {
            this.totalCost = 0;
        }
    }
}