@isTest
public class UpcomingCoursesControllerTest {
    @testSetup
    static void createResources() {
        
        User u = [
            SELECT Id, Name, Phone, Email, Title, IsActive
            FROM User 
            WHERE (isActive = true) AND ((Email = 'bd@apostletech.com') OR (Email = 'bd@apostletech.com.invalid') OR (Email = 'virginia.young@bd.com'))
        ][0]; 

        System.runAs(u) {
        
            List<Account> accts = TestDataFactory.createAccounts(1);

            List<Contact> proctorContacts = TestDataFactory.createContacts(accts, 2);
            List<Contact> attendeeContacts = TestDataFactory.createContacts(accts, 3);
            List<Contact> waitlistContacts = TestDataFactory.createContacts(accts, 1);
            List<Contact> availContacts = TestDataFactory.createContacts(accts, 10);

            List<User> bdUsers = TestDataFactory.createUsers(2);
            List<User> availUsers = TestDataFactory.createUsers(5);     

            List<Campaign> campaigns = TestDataFactory.createCampaigns(accts[0], 1);

            List<CampaignMember> proctorMembers = TestDataFactory.createCampaignMembers(campaigns[0], proctorContacts, 'Proctor');
            List<CampaignMember> attendeeMembers = TestDataFactory.createCampaignMembers(campaigns[0], attendeeContacts, 'Attendee');
            List<CampaignMember> waitlistMembers = TestDataFactory.createCampaignMembers(campaigns[0], waitlistContacts, 'Waitlist');

            TestDataFactory.createCampaignUsers(campaigns[0], bdUsers);
        }
    }

    @isTest
    static void fetchproductCategories_UnitTest() {
        List<string> pc = UpcomingCoursesController.fetchProductCategories('Biopsy');

        System.assertEquals(4, pc.size());

        System.assertEquals('Biopsy', pc.get(0));
        System.assertEquals('Capital', pc.get(1));
        System.assertEquals('Senorx', pc.get(2));   
    }

    @isTest
    static void fetchPickListValues_UnitTest() {
        List<string> fields = new List<String> {
            'division', 
            'product_category'
        };

         Map<string, Map<string, string>> values = UpcomingCoursesController.fetchPickListValues(fields);

         System.assertEquals(2, values.size());

        Boolean success = values.containsKey('product_category') && values.get('product_category').size() > 0;
        System.assert(success);
         /*for(string key :values.keySet()) {
            if(key.equalsIgnoreCase('product_category')) {
                System.assertEquals(11, values.get(key).size());
                break;
            }
         }  ?????*/
    }

    @isTest
    static void fetchUpcomingCampaignsByContact_UnitTest() {
        //Get the existing campaign & attendees
        Campaign camp = [
            SELECT Id
            FROM Campaign
        ][0];

        List<Contact> contacts = [
            SELECT Id
            FROM Contact
            WHERE Id IN (
                SELECT ContactId FROM CampaignMember WHERE CampaignId = :camp.Id AND Type__c = 'Attendee'
            )
        ];

        //Set up a new future dated campaign 
        Account acct = [
            SELECT Id
            FROM Account
            LIMIT 1
        ][0];

        List<Campaign> campaigns = TestDataFactory.createCampaigns(acct, 1);

        for(Campaign c :campaigns) {
            c.StartDate = Date.today().addDays(3);
            c.EndDate = Date.today().addDays(5);
        }

        update campaigns;

        //Assign the members from original campaign to the future dated one
        List<CampaignMember> attendeeMembers = TestDataFactory.createCampaignMembers(campaigns[0], contacts, 'Attendee');

        List<CampaignDTO> dto = UpcomingCoursesController.fetchUpcomingCampaignsByContact(attendeeMembers[0].ContactId);

        System.assertEquals(2, dto.size());
    }

    @isTest
    static void fetchUpcomingCampaigns_UnitTest() {
        //Get the existing campaign & attendees
        Campaign camp = [
            SELECT Id
            FROM Campaign
        ][0];

        List<Contact> contacts = [
            SELECT Id
            FROM Contact
            WHERE Id IN (
                SELECT ContactId FROM CampaignMember WHERE CampaignId = :camp.Id AND Type__c = 'Attendee'
            )
        ];

        //Set up a new future dated campaign 
        Account acct = [
            SELECT Id
            FROM Account
            LIMIT 1
        ][0];

        List<Campaign> campaigns = TestDataFactory.createCampaigns(acct, 1);

        for(Campaign c :campaigns) {
            c.StartDate = Date.today().addDays(3);
            c.EndDate = Date.today().addDays(5);
        }

        update campaigns;

        //Assign the members from original campaign to the future dated one
        List<CampaignMember> attendeeMembers = TestDataFactory.createCampaignMembers(campaigns[0], contacts, 'Attendee');

        List<SearchParamViewModel> params = new List<SearchParamViewModel> {
            new SearchParamViewModel('course_title', 'Test Campaign'),
            new SearchParamViewModel('division', 'Biopsy'),
            new SearchParamViewModel('product_category', 'Capital')
        };

        List<CampaignDTO> dto = UpcomingCoursesController.fetchUpcomingCampaigns(attendeeMembers[0].ContactId, JSON.serialize(params));

        System.assertEquals(2, dto.size());
    }

    @isTest
    static void addMember_UnitTest() {
        Campaign c = [
            SELECT Id
            FROM Campaign
        ][0];

        Contact con = [
            SELECT Id, Name
            FROM Contact 
            WHERE Id NOT IN (
                SELECT ContactId FROM CampaignMember WHERE CampaignId = :c.Id
            )
            LIMIT 1
        ][0];

        string memberName = UpcomingCoursesController.addMember(c.Id, con.Id, 'Attendee');

        System.assertEquals(con.Name, memberName);
    }

    @isTest
    static void removeMember_UnitTest() {
        Campaign c = [
            SELECT Id
            FROM Campaign
        ][0];
        
        CampaignMember cm = [
            SELECT ContactId, Contact.Name
            FROM CampaignMember 
            WHERE CampaignId = :c.Id 
            AND Type__c = 'Attendee'
        ][0];

        string memberName = UpcomingCoursesController.removeMember(c.Id, cm.ContactId, 'Attendee');

        System.assertEquals(cm.Contact.Name, memberName);
    }
    
    @isTest
    private static void getContact () {
        List<Contact> contacts = [SELECT Id FROM Contact WHERE FirstName LIKE 'FirstName%' AND LastName LIKE 'LastName%'];
        System.assert(!contacts.isEmpty());
        
        Contact con = contacts.get(0);
        Contact resultContact = UpcomingCoursesController.getContactFields(con.Id);
        System.assertNotEquals(NULL, resultContact);
    }
}