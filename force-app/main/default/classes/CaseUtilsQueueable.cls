public class CaseUtilsQueueable implements Queueable {
	
	private List<Case> 		newCaseList; 
	private Map<Id, Case> 	oldCaseMap;
	private List<String> 	addresses;
	private Boolean 		createTrigger;

	public CaseUtilsQueueable(Boolean createTrigger, List<Case> newCaseList, Map<Id, Case> oldCaseMap) {
		this.createTrigger 	= createTrigger;
		this.newCaseList 	= newCaseList;
		this.oldCaseMap 	= oldCaseMap;
	}

	public void execute(QueueableContext context) {
        if (this.createTrigger) {
        	this.CreateProductUsed();
        } else {
        	this.UpdateProductUsed();
        }
	}

	private void CreateProductUsed() {
		this.newCaseList = [select Product_Group_1__c, Quantity_1__c, Product_Group_2__c, Quantity_2__c, Product_Group_3__c, Quantity_3__c,
									Product_Group_4__c, Quantity_4__c, Product_Group_5__c, Quantity_5__c, Product_Group_6__c, Quantity_6__c,
									Product_Group_7__c, Quantity_7__c from Case where Id IN: this.newCaseList];
		Set<String> productNames = new Set<String>();
		for (Case c : this.newCaseList) {
			productNames.add(c.Product_Group_1__c);
			productNames.add(c.Product_Group_2__c);
			productNames.add(c.Product_Group_3__c);
			productNames.add(c.Product_Group_4__c);
			productNames.add(c.Product_Group_5__c);
			productNames.add(c.Product_Group_6__c);
			productNames.add(c.Product_Group_7__c);
		}
		Map<String, Id> productMap = new Map<String, Id>();
		for (Product__c p : [select Name from Product__c where Name IN: productNames]) 
				productMap.put(p.Name, p.Id);
		
		Messaging.SingleEmailMessage[] emailsToSend = new Messaging.SingleEmailMessage[]{};				
		List<Product_Used__c> puList = new List<Product_Used__c>();

		this.addresses = new List<String>();
		for (ProductUsed_Fail_SentTo_Email__mdt address : [select Address__c from ProductUsed_Fail_SentTo_Email__mdt])	
			this.addresses.add(address.Address__c.replace(' ', ''));

		Map<Id, Case> caseForUpdate = new Map<Id, Case>();
		for (Case c : this.newCaseList) {
			
			if (!String.isBlank(c.Product_Group_1__c)) {
				
				if (productMap.containsKey(c.Product_Group_1__c)) {
					Product_Used__c pu = new Product_Used__c();
					pu.Product__c = productMap.get(c.Product_Group_1__c);
					pu.Case__c = c.Id;
					pu.Quantity__c = Decimal.valueOf(c.Quantity_1__c);
					puList.add(pu);
				} else {
					emailsToSend.add(createErrorEmail(c.Id, c.Product_Group_1__c, c.Quantity_1__c));
					c.Product_Group_1__c = null;
					c.Quantity_1__c = null;
					caseForUpdate.put(c.Id, c);
				}
			}

			if (!String.isBlank(c.Product_Group_2__c)) {
				
				if (productMap.containsKey(c.Product_Group_2__c)) {
					Product_Used__c pu = new Product_Used__c();
					pu.Product__c = productMap.get(c.Product_Group_2__c);
					pu.Case__c = c.Id;
					pu.Quantity__c = Decimal.valueOf(c.Quantity_2__c);
					puList.add(pu);
				} else {
					emailsToSend.add(createErrorEmail(c.Id, c.Product_Group_2__c, c.Quantity_2__c));
					c.Product_Group_2__c = null;
					c.Quantity_2__c = null;
					caseForUpdate.put(c.Id, c);
				}
			}

			if (!String.isBlank(c.Product_Group_3__c)) {
				
				if (productMap.containsKey(c.Product_Group_3__c)) {
					Product_Used__c pu = new Product_Used__c();
					pu.Product__c = productMap.get(c.Product_Group_3__c);
					pu.Case__c = c.Id;
					pu.Quantity__c = Decimal.valueOf(c.Quantity_3__c);
					puList.add(pu);
				} else {
					emailsToSend.add(createErrorEmail(c.Id, c.Product_Group_3__c, c.Quantity_3__c));
					c.Product_Group_3__c = null;
					c.Quantity_3__c = null;
					caseForUpdate.put(c.Id, c);
				}
			}

			if (!String.isBlank(c.Product_Group_4__c)) {
				
				if (productMap.containsKey(c.Product_Group_4__c)) {
					Product_Used__c pu = new Product_Used__c();
					pu.Product__c = productMap.get(c.Product_Group_4__c);
					pu.Case__c = c.Id;
					pu.Quantity__c = Decimal.valueOf(c.Quantity_4__c);
					puList.add(pu);
				} else {
					emailsToSend.add(createErrorEmail(c.Id, c.Product_Group_4__c, c.Quantity_4__c));
					c.Product_Group_4__c = null;
					c.Quantity_4__c = null;
					caseForUpdate.put(c.Id, c);
				}
			}

			if (!String.isBlank(c.Product_Group_5__c)) {
				
				if (productMap.containsKey(c.Product_Group_5__c)) {
					Product_Used__c pu = new Product_Used__c();
					pu.Product__c = productMap.get(c.Product_Group_5__c);
					pu.Case__c = c.Id;
					pu.Quantity__c = Decimal.valueOf(c.Quantity_5__c);
					puList.add(pu);
				} else {
					emailsToSend.add(createErrorEmail(c.Id, c.Product_Group_5__c, c.Quantity_5__c));
					c.Product_Group_5__c = null;
					c.Quantity_5__c = null;
					caseForUpdate.put(c.Id, c);
				}
			}

			if (!String.isBlank(c.Product_Group_6__c)) {
				
				if (productMap.containsKey(c.Product_Group_6__c)) {
					Product_Used__c pu = new Product_Used__c();
					pu.Product__c = productMap.get(c.Product_Group_6__c);
					pu.Case__c = c.Id;
					pu.Quantity__c = Decimal.valueOf(c.Quantity_6__c);
					puList.add(pu);
				} else {
					emailsToSend.add(createErrorEmail(c.Id, c.Product_Group_6__c, c.Quantity_6__c));
					c.Product_Group_6__c = null;
					c.Quantity_6__c = null;
					caseForUpdate.put(c.Id, c);
				}
			}

			if (!String.isBlank(c.Product_Group_7__c)) {
				
				if (productMap.containsKey(c.Product_Group_7__c)) {
					Product_Used__c pu = new Product_Used__c();
					pu.Product__c = productMap.get(c.Product_Group_7__c);
					pu.Case__c = c.Id;
					pu.Quantity__c = Decimal.valueOf(c.Quantity_7__c);
					puList.add(pu);
				} else {
					emailsToSend.add(createErrorEmail(c.Id, c.Product_Group_7__c, c.Quantity_7__c));
					c.Product_Group_7__c = null;
					c.Quantity_7__c = null;
					caseForUpdate.put(c.Id, c);
				}
			}
		}

		insert puList;

		CaseUtils.STOP_TRIGGER = true;
		update caseForUpdate.values();
		CaseUtils.STOP_TRIGGER = false;

        Messaging.sendEmail(emailsToSend);
	}

	public void UpdateProductUsed() {
		this.newCaseList = [select Product_Group_1__c, Quantity_1__c, Product_Group_2__c, Quantity_2__c, Product_Group_3__c, Quantity_3__c,
									Product_Group_4__c, Quantity_4__c, Product_Group_5__c, Quantity_5__c, Product_Group_6__c, Quantity_6__c,
									Product_Group_7__c, Quantity_7__c from Case where Id IN: this.newCaseList];

		Set<String> productNames = new Set<String>();
		Set<Id> caseIds = new Set<Id>();
		for (Case c : this.newCaseList) {
			
			if (c.Product_Group_1__c != this.oldCaseMap.get(c.Id).Product_Group_1__c || c.Quantity_1__c != this.oldCaseMap.get(c.Id).Quantity_1__c) {
				productNames.add(c.Product_Group_1__c);	
				productNames.add(this.oldCaseMap.get(c.Id).Product_Group_1__c);	
				caseIds.add(c.Id);
			}
			if (c.Product_Group_2__c != this.oldCaseMap.get(c.Id).Product_Group_2__c || c.Quantity_2__c != this.oldCaseMap.get(c.Id).Quantity_2__c) {
				productNames.add(c.Product_Group_2__c);	
				productNames.add(this.oldCaseMap.get(c.Id).Product_Group_2__c);	
				caseIds.add(c.Id);
			}
			if (c.Product_Group_3__c != this.oldCaseMap.get(c.Id).Product_Group_3__c || c.Quantity_3__c != this.oldCaseMap.get(c.Id).Quantity_3__c) {
				productNames.add(c.Product_Group_3__c);	
				productNames.add(this.oldCaseMap.get(c.Id).Product_Group_3__c);	
				caseIds.add(c.Id);
			}
			if (c.Product_Group_4__c != this.oldCaseMap.get(c.Id).Product_Group_4__c || c.Quantity_4__c != this.oldCaseMap.get(c.Id).Quantity_4__c) {
				productNames.add(c.Product_Group_4__c);	
				productNames.add(this.oldCaseMap.get(c.Id).Product_Group_4__c);	
				caseIds.add(c.Id);
			}
			if (c.Product_Group_5__c != this.oldCaseMap.get(c.Id).Product_Group_5__c || c.Quantity_5__c != this.oldCaseMap.get(c.Id).Quantity_5__c) {
				productNames.add(c.Product_Group_5__c);	
				productNames.add(this.oldCaseMap.get(c.Id).Product_Group_5__c);	
				caseIds.add(c.Id);
			}
			if (c.Product_Group_6__c != this.oldCaseMap.get(c.Id).Product_Group_6__c || c.Quantity_6__c != this.oldCaseMap.get(c.Id).Quantity_6__c) {
				productNames.add(c.Product_Group_6__c);	
				productNames.add(this.oldCaseMap.get(c.Id).Product_Group_6__c);	
				caseIds.add(c.Id);
			}
			if (c.Product_Group_7__c != this.oldCaseMap.get(c.Id).Product_Group_7__c || c.Quantity_7__c != this.oldCaseMap.get(c.Id).Quantity_7__c) {
				productNames.add(c.Product_Group_7__c);	
				productNames.add(this.oldCaseMap.get(c.Id).Product_Group_7__c);	
				caseIds.add(c.Id);
			}
		}

		Map<Id, Map<String, Product_Used__c>> caseToProductUsedMap = new Map<Id, Map<String, Product_Used__c>>();
		for (Product_Used__c pu : [select Quantity__c, Product__r.Name, Case__c from Product_Used__c where Case__c IN: caseIds and Product__r.Name IN: productNames]) {
			if (caseToProductUsedMap.containsKey(pu.Case__c)) {
				caseToProductUsedMap.get(pu.Case__c).put(pu.Product__r.Name, pu);
			} else {
				Map<String, Product_Used__c> ppuMap = new Map<String, Product_Used__c>();
				ppuMap.put(pu.Product__r.Name, pu);
				caseToProductUsedMap.put(pu.Case__c, ppuMap);
			}	
		}

		Map<String, Id> productMap = new Map<String, Id>();
		for (Product__c p : [select Name from Product__c where Name IN: productNames]) 
				productMap.put(p.Name, p.Id);

		Messaging.SingleEmailMessage[] emailsToSend = new Messaging.SingleEmailMessage[]{};				
		List<Product_Used__c> puList = new List<Product_Used__c>();

		this.addresses = new List<String>();
		for (ProductUsed_Fail_SentTo_Email__mdt address : [select Address__c from ProductUsed_Fail_SentTo_Email__mdt])	
			this.addresses.add(address.Address__c);
			
		List<Product_Used__c> productUsedForUpsert = new List<Product_Used__c>();
		List<Product_Used__c> productUsedForDelete = new List<Product_Used__c>();
		
		Map<Id, Case> caseForUpdate = new Map<Id, Case>();
		system.debug('!!!!!!!this.oldCaseMap: ' + this.oldCaseMap);
		system.debug('!!!!!!!caseToProductUsedMap: ' + caseToProductUsedMap);
		system.debug('!!!!!!!this.newCaseList: ' + this.newCaseList);
		for (Case c : this.newCaseList) {
			
			if (String.isBlank(c.Product_Group_1__c) && !String.isBlank(this.oldCaseMap.get(c.Id).Product_Group_1__c)) {
				productUsedForDelete.add(caseToProductUsedMap.get(c.Id).get(this.oldCaseMap.get(c.Id).Product_Group_1__c));
			} else if (c.Product_Group_1__c != this.oldCaseMap.get(c.Id).Product_Group_1__c) {
				if (productMap.containsKey(c.Product_Group_1__c)) {
					Product_Used__c pu = new Product_Used__c();
					pu.Product__c = productMap.get(c.Product_Group_1__c);
					pu.Case__c = c.Id;
					pu.Quantity__c = Decimal.valueOf(c.Quantity_1__c);
					productUsedForUpsert.add(pu);
					if (!String.isBlank(this.oldCaseMap.get(c.Id).Product_Group_1__c))
						productUsedForDelete.add(caseToProductUsedMap.get(c.Id).get(this.oldCaseMap.get(c.Id).Product_Group_1__c));
				} else {
					emailsToSend.add(createErrorEmail(c.Id, c.Product_Group_1__c, c.Quantity_1__c));
					c.Product_Group_1__c = this.oldCaseMap.get(c.Id).Product_Group_1__c;
					c.Quantity_1__c = this.oldCaseMap.get(c.Id).Quantity_1__c;
					caseForUpdate.put(c.Id, c);
				}
			} else if (c.Quantity_1__c != this.oldCaseMap.get(c.Id).Quantity_1__c) {
				caseToProductUsedMap.get(c.Id).get(c.Product_Group_1__c).Quantity__c = Integer.valueof(c.Quantity_1__c);
				productUsedForUpsert.add(caseToProductUsedMap.get(c.Id).get(c.Product_Group_1__c));
			}

			if (String.isBlank(c.Product_Group_2__c) && !String.isBlank(this.oldCaseMap.get(c.Id).Product_Group_2__c)) {
				productUsedForDelete.add(caseToProductUsedMap.get(c.Id).get(this.oldCaseMap.get(c.Id).Product_Group_2__c));
			} else if (c.Product_Group_2__c != this.oldCaseMap.get(c.Id).Product_Group_2__c) {
				if (productMap.containsKey(c.Product_Group_2__c)) {
					Product_Used__c pu = new Product_Used__c();
					pu.Product__c = productMap.get(c.Product_Group_2__c);
					pu.Case__c = c.Id;
					pu.Quantity__c = Decimal.valueOf(c.Quantity_2__c);
					productUsedForUpsert.add(pu);
					if (!String.isBlank(this.oldCaseMap.get(c.Id).Product_Group_2__c))
						productUsedForDelete.add(caseToProductUsedMap.get(c.Id).get(this.oldCaseMap.get(c.Id).Product_Group_2__c));
				} else {
					emailsToSend.add(createErrorEmail(c.Id, c.Product_Group_2__c, c.Quantity_2__c));
					c.Product_Group_2__c = this.oldCaseMap.get(c.Id).Product_Group_2__c;
					c.Quantity_2__c = this.oldCaseMap.get(c.Id).Quantity_2__c;
					caseForUpdate.put(c.Id, c);
				}
			} else if (c.Quantity_2__c != this.oldCaseMap.get(c.Id).Quantity_2__c) {
				caseToProductUsedMap.get(c.Id).get(c.Product_Group_2__c).Quantity__c = Integer.valueof(c.Quantity_2__c);
				productUsedForUpsert.add(caseToProductUsedMap.get(c.Id).get(c.Product_Group_2__c));
			}

			if (String.isBlank(c.Product_Group_3__c) && !String.isBlank(this.oldCaseMap.get(c.Id).Product_Group_3__c)) {
				productUsedForDelete.add(caseToProductUsedMap.get(c.Id).get(this.oldCaseMap.get(c.Id).Product_Group_3__c));
			} else if (c.Product_Group_3__c != this.oldCaseMap.get(c.Id).Product_Group_3__c) {
				if (productMap.containsKey(c.Product_Group_3__c)) {
					Product_Used__c pu = new Product_Used__c();
					pu.Product__c = productMap.get(c.Product_Group_3__c);
					pu.Case__c = c.Id;
					pu.Quantity__c = Decimal.valueOf(c.Quantity_3__c);
					productUsedForUpsert.add(pu);
					if (!String.isBlank(this.oldCaseMap.get(c.Id).Product_Group_3__c))
						productUsedForDelete.add(caseToProductUsedMap.get(c.Id).get(this.oldCaseMap.get(c.Id).Product_Group_3__c));
				} else {
					emailsToSend.add(createErrorEmail(c.Id, c.Product_Group_3__c, c.Quantity_3__c));
					c.Product_Group_3__c = this.oldCaseMap.get(c.Id).Product_Group_3__c;
					c.Quantity_3__c = this.oldCaseMap.get(c.Id).Quantity_3__c;
					caseForUpdate.put(c.Id, c);
				}
			} else if (c.Quantity_3__c != this.oldCaseMap.get(c.Id).Quantity_3__c) {
				caseToProductUsedMap.get(c.Id).get(c.Product_Group_3__c).Quantity__c = Integer.valueof(c.Quantity_3__c);
				productUsedForUpsert.add(caseToProductUsedMap.get(c.Id).get(c.Product_Group_3__c));
			}

			if (String.isBlank(c.Product_Group_4__c) && !String.isBlank(this.oldCaseMap.get(c.Id).Product_Group_4__c)) {
				productUsedForDelete.add(caseToProductUsedMap.get(c.Id).get(this.oldCaseMap.get(c.Id).Product_Group_4__c));
			} else if (c.Product_Group_4__c != this.oldCaseMap.get(c.Id).Product_Group_4__c) {
				if (productMap.containsKey(c.Product_Group_4__c)) {
					Product_Used__c pu = new Product_Used__c();
					pu.Product__c = productMap.get(c.Product_Group_4__c);
					pu.Case__c = c.Id;
					pu.Quantity__c = Decimal.valueOf(c.Quantity_4__c);
					productUsedForUpsert.add(pu);
					if (!String.isBlank(this.oldCaseMap.get(c.Id).Product_Group_4__c))
						productUsedForDelete.add(caseToProductUsedMap.get(c.Id).get(this.oldCaseMap.get(c.Id).Product_Group_4__c));
				} else {
					emailsToSend.add(createErrorEmail(c.Id, c.Product_Group_4__c, c.Quantity_4__c));
					c.Product_Group_4__c = this.oldCaseMap.get(c.Id).Product_Group_4__c;
					c.Quantity_4__c = this.oldCaseMap.get(c.Id).Quantity_4__c;
					caseForUpdate.put(c.Id, c);
				}
			} else if (c.Quantity_4__c != this.oldCaseMap.get(c.Id).Quantity_4__c) {
				caseToProductUsedMap.get(c.Id).get(c.Product_Group_4__c).Quantity__c = Integer.valueof(c.Quantity_4__c);
				productUsedForUpsert.add(caseToProductUsedMap.get(c.Id).get(c.Product_Group_4__c));
			}

			if (String.isBlank(c.Product_Group_5__c) && !String.isBlank(this.oldCaseMap.get(c.Id).Product_Group_5__c)) {
				productUsedForDelete.add(caseToProductUsedMap.get(c.Id).get(this.oldCaseMap.get(c.Id).Product_Group_5__c));
			} else if (c.Product_Group_5__c != this.oldCaseMap.get(c.Id).Product_Group_5__c) {
				if (productMap.containsKey(c.Product_Group_5__c)) {
					Product_Used__c pu = new Product_Used__c();
					pu.Product__c = productMap.get(c.Product_Group_5__c);
					pu.Case__c = c.Id;
					pu.Quantity__c = Decimal.valueOf(c.Quantity_5__c);
					productUsedForUpsert.add(pu);
					if (!String.isBlank(this.oldCaseMap.get(c.Id).Product_Group_5__c))
						productUsedForDelete.add(caseToProductUsedMap.get(c.Id).get(this.oldCaseMap.get(c.Id).Product_Group_5__c));
				} else {
					emailsToSend.add(createErrorEmail(c.Id, c.Product_Group_5__c, c.Quantity_5__c));
					c.Product_Group_5__c = this.oldCaseMap.get(c.Id).Product_Group_5__c;
					c.Quantity_5__c = this.oldCaseMap.get(c.Id).Quantity_5__c;
					caseForUpdate.put(c.Id, c);
				}
			} else if (c.Quantity_5__c != this.oldCaseMap.get(c.Id).Quantity_5__c) {
				caseToProductUsedMap.get(c.Id).get(c.Product_Group_5__c).Quantity__c = Integer.valueof(c.Quantity_5__c);
				productUsedForUpsert.add(caseToProductUsedMap.get(c.Id).get(c.Product_Group_5__c));
			}

			if (String.isBlank(c.Product_Group_6__c) && !String.isBlank(this.oldCaseMap.get(c.Id).Product_Group_6__c)) {
				productUsedForDelete.add(caseToProductUsedMap.get(c.Id).get(this.oldCaseMap.get(c.Id).Product_Group_6__c));
			} else if (c.Product_Group_6__c != this.oldCaseMap.get(c.Id).Product_Group_6__c) {
				if (productMap.containsKey(c.Product_Group_6__c)) {
					Product_Used__c pu = new Product_Used__c();
					pu.Product__c = productMap.get(c.Product_Group_6__c);
					pu.Case__c = c.Id;
					pu.Quantity__c = Decimal.valueOf(c.Quantity_6__c);
					productUsedForUpsert.add(pu);
					if (!String.isBlank(this.oldCaseMap.get(c.Id).Product_Group_6__c))
						productUsedForDelete.add(caseToProductUsedMap.get(c.Id).get(this.oldCaseMap.get(c.Id).Product_Group_6__c));
				} else {
					emailsToSend.add(createErrorEmail(c.Id, c.Product_Group_6__c, c.Quantity_6__c));
					c.Product_Group_6__c = this.oldCaseMap.get(c.Id).Product_Group_6__c;
					c.Quantity_6__c = this.oldCaseMap.get(c.Id).Quantity_6__c;
					caseForUpdate.put(c.Id, c);
				}
			} else if (c.Quantity_6__c != this.oldCaseMap.get(c.Id).Quantity_6__c) {
				caseToProductUsedMap.get(c.Id).get(c.Product_Group_6__c).Quantity__c = Integer.valueof(c.Quantity_6__c);
				productUsedForUpsert.add(caseToProductUsedMap.get(c.Id).get(c.Product_Group_6__c));
			}

			if (String.isBlank(c.Product_Group_7__c) && !String.isBlank(this.oldCaseMap.get(c.Id).Product_Group_7__c)) {
				productUsedForDelete.add(caseToProductUsedMap.get(c.Id).get(this.oldCaseMap.get(c.Id).Product_Group_7__c));
			} else if (c.Product_Group_7__c != this.oldCaseMap.get(c.Id).Product_Group_7__c) {
				if (productMap.containsKey(c.Product_Group_7__c)) {
					Product_Used__c pu = new Product_Used__c();
					pu.Product__c = productMap.get(c.Product_Group_7__c);
					pu.Case__c = c.Id;
					pu.Quantity__c = Decimal.valueOf(c.Quantity_7__c);
					productUsedForUpsert.add(pu);
					if (!String.isBlank(this.oldCaseMap.get(c.Id).Product_Group_7__c))
						productUsedForDelete.add(caseToProductUsedMap.get(c.Id).get(this.oldCaseMap.get(c.Id).Product_Group_7__c));
				} else {
					emailsToSend.add(createErrorEmail(c.Id, c.Product_Group_7__c, c.Quantity_7__c));
					c.Product_Group_7__c = this.oldCaseMap.get(c.Id).Product_Group_7__c;
					c.Quantity_7__c = this.oldCaseMap.get(c.Id).Quantity_7__c;
					caseForUpdate.put(c.Id, c);
				}
			} else if (c.Quantity_7__c != this.oldCaseMap.get(c.Id).Quantity_7__c) {
				caseToProductUsedMap.get(c.Id).get(c.Product_Group_7__c).Quantity__c = Integer.valueof(c.Quantity_7__c);
				productUsedForUpsert.add(caseToProductUsedMap.get(c.Id).get(c.Product_Group_7__c));
			}
		}
		
		ProductUsedUtils.STOP_TRIGGER = true;
		delete productUsedForDelete;
       	ProductUsedUtils.STOP_TRIGGER = true;
		upsert productUsedForUpsert;
		ProductUsedUtils.STOP_TRIGGER = false;
		
		CaseUtils.STOP_TRIGGER = true;
		update caseForUpdate.values();
		CaseUtils.STOP_TRIGGER = false;

		Messaging.sendEmail(emailsToSend);
	}

	private Messaging.SingleEmailMessage createErrorEmail(Id cId, String pName, String pQuantity) {
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		email.setToAddresses(this.addresses);
    	email.setSubject('Product Name Not Found');
		email.setPlainTextBody('A user tried to create a Product Used for the Case "' + cId + '" while offline and it failed because the Picklist value "' + pName + '" does not exist as a record in the Product object. Quantity was ' + pQuantity + '.');
		return email;
	}
}