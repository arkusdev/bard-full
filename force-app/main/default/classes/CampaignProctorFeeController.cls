public class CampaignProctorFeeController {
	@auraEnabled
	public static List<CampaignProctorFeeDTO> fetchCampaignProctorFees(id campaignId) {
		List<CampaignProctorFeeDTO> dtos = new List<CampaignProctorFeeDTO>();

		if (UserUtil.canAccessProctorExpenses()) {
			List<Campaign_Proctor_Fee__c> fees = [
				SELECT Id, Campaign__c, Name, Proctor__c, Proctor__r.Name, Total_Cost__c, Transfer_of_Value__c, Notes__c
				FROM Campaign_Proctor_Fee__c
				WHERE Campaign__c = :campaignId
			];

			for(Campaign_Proctor_Fee__c ci :fees) {
				dtos.add(new CampaignProctorFeeDTO(ci));
			}			
		}	

		return dtos;
	}

	@auraEnabled
	public static CampaignProctorFeeDTO fetchCampaignProctorFee(id feeId) {
		try {

			if (!UserUtil.canAccessProctorExpenses()) { return null; }

			Campaign_Proctor_Fee__c fee = [
				SELECT Id, Campaign__c, Name, Proctor__c, Proctor__r.Name, Total_Cost__c, Transfer_of_Value__c, Notes__c
				FROM Campaign_Proctor_Fee__c
				WHERE Id = :feeId
			][0];

			List<CampaignMember> availProctors = fetchCampaignProctors(fee.Campaign__c);
			
			CampaignProctorFeeDTO dto = new CampaignProctorFeeDTO(fee, availProctors);

			return dto;			
					
		} catch(QueryException ex) {
			return null;		
		}
	}

	@auraEnabled
	public static CampaignProctorFeeDTO createNewCampaignProctorFee(id campaignId) {
		if (!UserUtil.canUpsertProctorExpenses()) { return null; }

		Campaign_Proctor_Fee__c fee = new Campaign_Proctor_Fee__c(
			Campaign__c = campaignId
		);

		List<CampaignMember> availProctors = fetchCampaignProctors(campaignId);

		CampaignProctorFeeDTO dto = new CampaignProctorFeeDTO(fee, availProctors);

		return dto;
	}

	private static List<CampaignMember> fetchCampaignProctors(id campaignId) { 
		return [
			SELECT ContactId, Contact.Name 
			FROM CampaignMember 
			WHERE CampaignId = :campaignId
			AND Type__c = 'Proctor'
		];	
	}

	@auraEnabled
	public static void deleteCampaignProctorFee(id feeId) {
		if (UserUtil.canDeleteProctorExpenses()) { 
			Campaign_Proctor_Fee__c cpf = new Campaign_Proctor_Fee__c(
				Id = feeId
			);

			delete cpf;		
		}
	}

	@auraEnabled
	public static void saveCampaignProctorFee(string dto) {
		if (UserUtil.canUpsertProctorExpenses()) {
			CampaignProctorFeeDTO fee = ((CampaignProctorFeeDTO)System.JSON.deserializeStrict(dto, CampaignProctorFeeDTO.Class));

			Campaign_Proctor_Fee__c cpf = new Campaign_Proctor_Fee__c(
				Id = fee.feeId,
				Name = fee.proctorName + ' Proctor Fee',
				Campaign__c = fee.campaignId,
				Proctor__c = fee.proctorId,
				Total_Cost__c = fee.totalCost,
				Transfer_of_Value__c = fee.transferOfValue,
				Notes__c = fee.notes
			);

			upsert cpf;		
		}
	}
}