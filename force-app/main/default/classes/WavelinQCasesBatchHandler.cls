public with sharing class WavelinQCasesBatchHandler {

    private static String frenchUserId = Wavelinq_Achievement_Settings__c.getValues('Wavelinq Setting').French_Id__c;
    private static String finchUserId = Wavelinq_Achievement_Settings__c.getValues('Wavelinq Setting').Finch_Id__c;
    private static String notificationTypeId = [SELECT Id FROM CustomNotificationType WHERE DeveloperName = 'WaveLinQ_Tier_Status'].Id;
    private static final Decimal SILVER_LEVEL = Wavelinq_Achievement_Settings__c.getValues('Wavelinq Setting').Silver_level__c;
    private static final Decimal GOLD_LEVEL = Wavelinq_Achievement_Settings__c.getValues('Wavelinq Setting').Gold_level__c;
    private static final Decimal PLATINUM_LEVEL = Wavelinq_Achievement_Settings__c.getValues('Wavelinq Setting').Platinum_level__c;
    private static final Decimal DIAMOND_LEVEL = Wavelinq_Achievement_Settings__c.getValues('Wavelinq Setting').Diamond_level__c;
    private static final String VASCULAR_EAST_ROLE = 'Vascular East - RM';
    private static final String VASCULAR_WEST_ROLE = 'Vascular West - RM';
    private static final String VASCULAR_DIRECTOR_ROLE = 'Vascular Director';
    private static final String NOTIFICATION_SUBJECT = Wavelinq_Achievement_Settings__c.getValues('Wavelinq Setting').Notification_subject__c;
    private static String notificationMessage = Wavelinq_Achievement_Settings__c.getValues('Wavelinq Setting').Notification_message__c;
    private static final Decimal BATCH_START_TIME = Wavelinq_Achievement_Settings__c.getValues('Wavelinq Setting').Batch_Start_Hour__c;
    private static final Decimal BATCH_END_TIME = Wavelinq_Achievement_Settings__c.getValues('Wavelinq Setting').Batch_End_Hour__c;
    private static final String[] BATCH_WEEK_DAYS = Wavelinq_Achievement_Settings__c.getValues('Wavelinq Setting').Batch_Week_Days__c.replaceAll(' ', '').split(',');

    public static Boolean executeBatch(){
        Date d = System.today();
        Datetime dt = (Date)d;
        String dayOfWeek = dt.format('E');
        Integer hour = System.now().hour();
        return Test.isRunningTest() || (BATCH_WEEK_DAYS.contains(dayOfWeek) && (hour >= BATCH_START_TIME && hour <= BATCH_END_TIME));
    }

    public static List<Account> getAccounts(){
        List<AggregateResult> cases = [SELECT AccountId, COUNT(Id)
                                        FROM Case
                                        WHERE RecordType.Name = 'BD Wavelinq'
                                        AND Type = 'WavelinQ - Proctor'
                                        AND Status = 'Completed'
                                        AND Case_Date__c = THIS_FISCAL_YEAR
                                        GROUP BY AccountId];

        Set<String> accsIds = new Set<String>();
        for (AggregateResult pCase : cases) {
            accsIds.add(String.valueOf(pCase.get('AccountId')));
        }

        List<Account> accsToUpdate = 
                [SELECT Id,
                Name,
                WavelinQ_Completed_Case_Count__c,
                WavelinQ_Last_Status_Change__c, 
                WavelinQ_Previous_Status__c,
                WavelinQ_Achievement_Status__c,
                (
                    SELECT Id
                    FROM Cases
                    WHERE RecordType.Name = 'BD Wavelinq'
                    AND Type = 'WavelinQ - Proctor'
                    AND Status = 'Completed'
                    AND Case_Date__c = THIS_FISCAL_YEAR
                ),
                Vascular_Territory__r.Territory_Manager__r.Name
                FROM Account
                WHERE Id IN: accsIds];
        return accsToUpdate;
    }

    public static Map<Id, AccountInfo> updateAccounts(List<Account> accounts){
        List<Account> accountsToUpdate = new List<Account>();
        Set<Id> accountsToUpdateIds = new Set<Id>();
        Map<Id, AccountInfo> accountsUpdated = new Map<Id, AccountInfo>();

        for (Account acc : accounts) {
            Integer casesCount = acc.Cases.size();
            Boolean sendNotification = false;
            if (casesCount != acc.WavelinQ_Completed_Case_Count__c){
                if (statusUpdated(acc.WavelinQ_Completed_Case_Count__c == null ? 0 : acc.WavelinQ_Completed_Case_Count__c, casesCount)){
                    acc.WavelinQ_Previous_Status__c = acc.WavelinQ_Achievement_Status__c;
                    acc.WavelinQ_Last_Status_Change__c = Datetime.now();
                    sendNotification = (casesCount != null && acc.WavelinQ_Completed_Case_Count__c == null) || (casesCount > acc.WavelinQ_Completed_Case_Count__c);
                }
                acc.WavelinQ_Completed_Case_Count__c = acc.Cases.size();
                accountsUpdated.put(acc.Id, new AccountInfo(acc.Name, acc.WavelinQ_Completed_Case_Count__c, acc.WavelinQ_Achievement_Status__c, sendNotification, acc.Vascular_Territory__r.Territory_Manager__r.Name));
                accountsToUpdate.add(acc);
                accountsToUpdateIds.add(acc.Id);
            }
        }
        if (Account.sObjectType.getDescribe().isUpdateable()){
            update accountsToUpdate;
        }

        for (Account acc : [SELECT Id,
                Name,
                WavelinQ_Completed_Case_Count__c,
                WavelinQ_Last_Status_Change__c, 
                WavelinQ_Previous_Status__c,
                WavelinQ_Achievement_Status__c,
                Vascular_TM__r.Name
                FROM Account
                WHERE Id IN: accountsToUpdateIds]){
                    AccountInfo originalAcc = accountsUpdated.get(acc.Id);
                    originalAcc.casesCount = acc.WavelinQ_Completed_Case_Count__c;
                    originalAcc.status = acc.WavelinQ_Achievement_Status__c;
                }
        return accountsUpdated;
    }

    public static void handleNotifications(Map<Id, AccountInfo> accounts){
        List<Notification> notifications = new List<Notification>();
        Map<String, Set<String>> roleSubordinates = UserSubordinatesUtilities.getRoleSubordinateUsers();
        Map<String, Set<String>> vascularUsers = getVascularUsers(roleSubordinates);
        Map<Id, Map<String, Set<String>>> accountUsers = getAccountUsers(accounts.keySet(), roleSubordinates);

        for (Id accId : accounts.keySet()) {
            AccountInfo acc = accounts.get(accId);
            if (acc.sendNotification){
                Notification notification = new Notification();
                notification.typeId = notificationTypeId;
                notification.title = NOTIFICATION_SUBJECT;
                String notificationMessageBody = notificationMessage.replace('<<account name>>', acc.name).replace('<<status>>', acc.status);
                notificationMessageBody = acc.vascularTM != null ? notificationMessageBody.replace('<<vascular TM>>', acc.vascularTM) : notificationMessageBody.replace('<<vascular TM>>', 'Vascular TM');
                notification.body = notificationMessageBody;
                notification.targetId = accId;
                
                switch on acc.status {
                    when 'Silver' {
                        if (accountUsers.get(accId) != null){
                            Set<String> dmUsers = accountUsers.get(accId).get('DM');
                            if (dmUsers != null){
                                notification.userIds.addAll(dmUsers);
                            }
                        }
                    }

                    when 'Gold' {
                        if (accountUsers.get(accId) != null){
                            Set<String> rmUsers = accountUsers.get(accId).get('RM');
                            if (rmUsers != null){
                                notification.userIds.addAll(rmUsers);
                            }
                            notification.userIds.add(frenchUserId);
                        }
                    }

                    when 'Platinum' {
                        Set<String> vascularEastRoles =  vascularUsers.get(VASCULAR_EAST_ROLE);
                        Set<String> vascularWestRoles =  vascularUsers.get(VASCULAR_WEST_ROLE);
                        if (vascularEastRoles != null){
                            notification.userIds.addAll(vascularEastRoles);
                        }
                        if (vascularWestRoles != null){
                            notification.userIds.addAll(vascularWestRoles);
                        }
                        notification.userIds.add(frenchUserId);
                        notification.userIds.add(finchUserId);
                    }

                    when 'Diamond' {
                        Set<String> vascularDirectorRoles = vascularUsers.get(VASCULAR_DIRECTOR_ROLE);
                        if (vascularDirectorRoles != null){
                            notification.userIds.addAll(vascularDirectorRoles);
                        }
                        notification.userIds.add(frenchUserId);
                        notification.userIds.add(finchUserId);
                    }
                }
                notifications.add(notification);
            }
        }
        sendNotifications(notifications);
    }

    private static Map<Id, Map<String, Set<String>>> getAccountUsers(Set<Id> accountsIds, Map<String, Set<String>> roles) {
        Map<Id, Map<String, Set<String>>> userSubordinates = new Map<Id, Map<String, Set<String>>>();
        for (Account acc : [SELECT Id, Territory__r.Regional_Manager__c,
                            Territory__r.District_Manager__c, Territory__r.District_Manager__r.UserRoleId, Territory__r.Regional_Manager__r.UserRoleId,
                            Peripheral_Territory__r.District_Manager__c, Peripheral_Territory__r.District_Manager__r.UserRoleId, Peripheral_Territory__r.Regional_Manager__r.UserRoleId,
                            Vascular_Territory__r.District_Manager__c, Vascular_Territory__r.District_Manager__r.UserRoleId, Vascular_Territory__r.Regional_Manager__r.UserRoleId
                            FROM Account 
                            WHERE Id IN: accountsIds]) {

            Set<String> biopsyRMUsers = roles.get(acc.Territory__r.Regional_Manager__r.UserRoleId);
            Set<String> peripheralRMUsers = roles.get(acc.Peripheral_Territory__r.Regional_Manager__r.UserRoleId);
            Set<String> vascularRMUsers = roles.get(acc.Vascular_Territory__r.Regional_Manager__r.UserRoleId);
            Set<String> rmUsers = new Set<String>();
            
            if (acc.Territory__r.Regional_Manager__c != null){
                rmUsers.add(acc.Territory__r.Regional_Manager__c);
            }
            if (acc.Peripheral_Territory__r.Regional_Manager__c != null){
                rmUsers.add(acc.Peripheral_Territory__r.Regional_Manager__c);
            }
            if (acc.Vascular_Territory__r.Regional_Manager__c != null){
                rmUsers.add(acc.Vascular_Territory__r.Regional_Manager__c);
            }

            if (biopsyRMUsers != null){
                rmUsers.addAll(biopsyRMUsers);
            }

            if (peripheralRMUsers != null){
                rmUsers.addAll(peripheralRMUsers);
            }

            if (vascularRMUsers != null){
                rmUsers.addAll(vascularRMUsers);
            }

            if (rmUsers.size() > 0){
                userSubordinates.put(acc.Id,
                    new Map<String, Set<String>>{'RM' => rmUsers}
                );
            }

            Set<String> biopsyDMUsers = roles.get(acc.Territory__r.District_Manager__r.UserRoleId);
            Set<String> peripheralDMUsers = roles.get(acc.Peripheral_Territory__r.District_Manager__r.UserRoleId);
            Set<String> vascularDMUsers = roles.get(acc.Vascular_Territory__r.District_Manager__r.UserRoleId);
            Set<String> dmUsers = new Set<String>();

            if (acc.Territory__r.District_Manager__c != null){
                dmUsers.add(acc.Territory__r.District_Manager__c);
            }
            if (acc.Peripheral_Territory__r.District_Manager__c != null){
                dmUsers.add(acc.Peripheral_Territory__r.District_Manager__c);
            }
            if (acc.Vascular_Territory__r.District_Manager__c != null){
                dmUsers.add(acc.Vascular_Territory__r.District_Manager__c);
            }

            if (biopsyDMUsers != null){
                dmUsers.addAll(biopsyDMUsers);
            }

            if (peripheralDMUsers != null){
                dmUsers.addAll(peripheralDMUsers);
            }

            if (vascularDMUsers != null){
                dmUsers.addAll(vascularDMUsers);
            }

            if (dmUsers.size() > 0){
                if (userSubordinates.containsKey(acc.Id)){
                    userSubordinates.get(acc.Id).putAll(
                        new Map<String, Set<String>>{'DM' => dmUsers}
                    );
                }else{
                    userSubordinates.put(acc.Id,
                        new Map<String, Set<String>>{'DM' => dmUsers}
                    );
                }
            }
        }
        return userSubordinates;
    }

    private static Map<String, Set<String>> getVascularUsers(Map<String, Set<String>> roles) {
        
        Map<String, Set<String>> userSubordinates = new Map<String, Set<String>>();

        Set<String> vascularRoles = new Set<String>{ VASCULAR_EAST_ROLE, VASCULAR_WEST_ROLE, VASCULAR_DIRECTOR_ROLE};

        for (User user : [SELECT Id, UserRole.Id, UserRole.Name
                         FROM User
                         WHERE UserRole.Name IN: 
                         vascularRoles]) {
            Set<String> usersFromRole = new Set<String>();
            usersFromRole.add(user.Id);
            usersFromRole.addAll(roles.get(user.UserRole.Id));
            
            if (userSubordinates.containsKey(user.UserRole.Name)){
                userSubordinates.get(user.UserRole.Name).addAll(usersFromRole);
            }else{
                userSubordinates.put(user.UserRole.Name, usersFromRole);
            }
        }

        return userSubordinates;
    }

    private static Boolean statusUpdated(Decimal oldValue, Decimal newValue){
        Boolean silverCond = (oldValue < SILVER_LEVEL && newValue >= SILVER_LEVEL) || (newValue < SILVER_LEVEL && oldValue >= SILVER_LEVEL);
        Boolean goldCond = (oldValue < GOLD_LEVEL && newValue >= GOLD_LEVEL) || (newValue < GOLD_LEVEL && oldValue >= GOLD_LEVEL);
        Boolean platinumCond = (oldValue < PLATINUM_LEVEL && newValue >= PLATINUM_LEVEL) || (newValue < PLATINUM_LEVEL && oldValue >= PLATINUM_LEVEL);
        Boolean diamondCond = (oldValue < DIAMOND_LEVEL && newValue >= DIAMOND_LEVEL) || (newValue < DIAMOND_LEVEL && oldValue >= DIAMOND_LEVEL);
        return (oldValue == 0 && newValue == 0) || silverCond || goldCond || platinumCond || diamondCond;
    }

    private static void sendNotifications(List<Notification> notifications) {
        Database.executeBatch(new WavelinQSendNotificationsBatch(notifications), 10);
    }

    public class AccountInfo {
        public String name { set; get; }
        public String status { set; get; }
        public String vascularTM { set; get; }
        public Decimal casesCount { set; get; }
        public Boolean sendNotification { set; get; }

        public AccountInfo(String pName, Decimal pCasesCount, String pStatus, Boolean pSendNotification, String pVascularTM) {
            this.name = pName;
            this.casesCount = pCasesCount;
            this.status = pStatus;
            this.sendNotification = pSendNotification;
            this.vascularTM = pVascularTM;
        }
    }

    public class Notification {
        public Set<String> userIds { set; get; }
        public Id typeId { set; get; }
        public String title { set; get; }
        public String body { set; get; }
        public String targetId { set; get; }

        public Notification() {
            this.userIds = new Set<String>();
            this.body = '';
            this.title = '';
        }
    }
}