public class CampaignLedgerController {
	static Map<string, Map<string, Map<string, decimal>>> detailMap = new Map<string, Map<string, Map<string, decimal>>>();

	private static Campaign getCampaign(id campaignId) {
		return [
			SELECT Id, Name,
				(SELECT ContactId, Contact.Name, Type__c FROM CampaignMembers WHERE Type__c != 'Waitlist'),
				(SELECT User__c, User__r.Name FROM Campaign_Users__r),
				(SELECT Contact__c, Contact__r.Name, User__c, User__r.Name, Cost__c FROM Campaign_Flights__r),
				(SELECT Total_Cost__c FROM Campaign_Meals__r),
				(SELECT Total_Cost__c FROM Campaign_Transportations__r),
				(SELECT Total_Cost__c FROM Campaign_Incidentals__r),
				(SELECT Total_Cost__c FROM Campaign_Hotels__r),
				(SELECT Total_Cost__c FROM Campaign_Proctor_Fees__r)
			FROM Campaign
			WHERE Id = :campaignId		
		][0];
	}

	@auraEnabled
	public static Map<string, decimal> fetchSummaryTotals(id campaignId) {
		Map<string, decimal> summaryMap = new Map<string, decimal>();

		Campaign c = getCampaign(campaignId);
		decimal subTotal = 0;
		decimal total = 0;

		//Flights aggregration
		if (UserUtil.canAccessFlightExpenses()) { 
			for(Campaign_Flight__c cf :c.Campaign_Flights__r) {
				subTotal += cf.Cost__c;
			}

			summaryMap.put('Flights', subTotal);
			total += subTotal;
			subTotal = 0;		
		}

		//Meals
		if (UserUtil.canAccessMealExpenses()) { 
			for(Campaign_Meal__c cm :c.Campaign_Meals__r) {
				subTotal += cm.Total_Cost__c;
			}

			summaryMap.put('Meals', subTotal);
			total += subTotal;
			subTotal = 0;
		}


		//Transportation
		if (UserUtil.canAccessTransportationExpenses()) { 
			for(Campaign_Transportation__c ct :c.Campaign_Transportations__r) {
				subTotal += ct.Total_Cost__c;
			}

			summaryMap.put('Transportation', subTotal);
			total += subTotal;
			subTotal = 0;		
		}

		//Incidentals
		if (UserUtil.canAccessIncidentalExpenses()) { 
			for(Campaign_Incidental__c ci :c.Campaign_Incidentals__r) {
				subTotal += ci.Total_Cost__c;
			}

			summaryMap.put('Additional Costs', subTotal);
			total += subTotal;
			subTotal = 0;		
		}

		//Hotels
		if (UserUtil.canAccessHotelExpenses()) { 
			for(Campaign_Hotel__c ch :c.Campaign_Hotels__r) {
				subTotal += ch.Total_Cost__c;
			}

			summaryMap.put('Hotels', subTotal);
			total += subTotal;
			subTotal = 0;		
		}
		
		//Proctor - if allowed
		if (UserUtil.canAccessProctorExpenses()) {
			for(Campaign_Proctor_Fee__c cp :c.Campaign_Proctor_Fees__r) {
				subTotal += cp.Total_Cost__c;
			}

			summaryMap.put('Proctor', subTotal);
			total += subTotal;
			subTotal = 0;		
		}

		summaryMap.put('Total', total);

		return summaryMap;
	}

	@auraEnabled
	public static Map<string, Map<string, Map<string, decimal>>> fetchDetailTotals(id campaignId) {
		//Map Keys (in order):
		//Member Type (Proctor, Attendee, BD Staff)
		//Identifier (Id|Name)
		//Expense Type (Meals, Flights, Hotels, Incidentals, Transportation)

		detailMap.put('Proctor', new Map<string, Map<string, decimal>>());
		detailMap.put('Attendee', new Map<string, Map<string, decimal>>());
		detailMap.put('Staff', new Map<string, Map<string, decimal>>());

		Campaign c = getCampaign(campaignId);

		Set<Id> contacts = new Set<Id>();
		Set<Id> proctors = new Set<Id>();
		Set<Id> attendees = new Set<Id>();
		Set<Id> users = new Set<Id>();

		for(CampaignMember cm :c.CampaignMembers) {
			if (!contacts.contains(cm.ContactId)) { contacts.add(cm.ContactId); }

			if (cm.Type__c.equalsIgnoreCase('Proctor') && !proctors.contains(cm.ContactId)) {
				proctors.add(cm.ContactId);
			} else if(cm.Type__c.equalsIgnoreCase('Attendee') && !attendees.contains(cm.ContactId)) {
				attendees.add(cm.ContactId);
			}
		}

		for(Campaign_User__c cu :c.Campaign_Users__r) {
			if (!users.contains(cu.User__c)) { users.add(cu.User__c); }			
		}

		//Flights aggregration
		if (UserUtil.canAccessFlightExpenses()) { 
			for(Campaign_Flight__c cf :c.Campaign_Flights__r) {
				if (cf.Contact__c != null) {
					string key = generateKey(cf.Contact__c, cf.Contact__r.Name);

					if (proctors.contains(cf.Contact__c)) {
						recalcDetailLedger('Proctor', key, 'Flights', cf.Cost__c);
					} else if (attendees.contains(cf.Contact__c)) {
						recalcDetailLedger('Attendee', key, 'Flights', cf.Cost__c);
					}
				} else if (cf.User__c != null) {
					if (users.contains(cf.User__c)) {
						string key = generateKey(cf.User__c, cf.User__r.Name);

						recalcDetailLedger('Staff', key, 'Flights', cf.Cost__c);
					}
				}
			}		
		}

		//Meals aggregration
		if (UserUtil.canAccessMealExpenses()) { 
			List<Campaign_Meal__c> meals = [
				SELECT Cost_Per_Attendee__c,
					(SELECT Contact__c, Contact__r.Name, User__c, User__r.Name 
					 FROM Campaign_Meal_Attendees__r 
					 WHERE (Contact__c IN :contacts OR User__c IN :users))
				FROM Campaign_Meal__c
				WHERE Campaign__r.Id =: campaignId
				ORDER BY Type__c, Meal_Date__c		
			];

			for(Campaign_Meal__c cm :meals) {
				for(Campaign_Meal_Attendee__c cma :cm.Campaign_Meal_Attendees__r) {
					if (cma.Contact__c != null) {
						string key = generateKey(cma.Contact__c, cma.Contact__r.Name);

						if (proctors.contains(cma.Contact__c)) {
							recalcDetailLedger('Proctor', key, 'Meals', cm.Cost_Per_Attendee__c);
						} else if (attendees.contains(cma.Contact__c)) {
							recalcDetailLedger('Attendee', key, 'Meals', cm.Cost_Per_Attendee__c);
						}
					} else if (cma.User__c != null) {
						if (users.contains(cma.User__c)) {
							string key = generateKey(cma.User__c, cma.User__r.Name);

							recalcDetailLedger('Staff', key, 'Meals', cm.Cost_Per_Attendee__c);
						}				
					}
				}
			}		
		}

		//Transportation
		if (UserUtil.canAccessTransportationExpenses()) { 
			List<Campaign_Transportation__c> trans = [
				SELECT Cost_Per_Attendee__c,
					(SELECT Contact__c, Contact__r.Name, User__c, User__r.Name 
					 FROM Campaign_Transportation_Attendees__r 
					 WHERE (Contact__c IN :contacts OR User__c IN :users))
				FROM Campaign_Transportation__c
				WHERE Campaign__r.Id = :campaignId
				ORDER BY Pickup_Date_Time__c		
			];

			for(Campaign_Transportation__c ct :trans) {
				for(Campaign_Transportation_Attendee__c cta :ct.Campaign_Transportation_Attendees__r) {
					if (cta.Contact__c != null) {
						string key = generateKey(cta.Contact__c, cta.Contact__r.Name);

						if (proctors.contains(cta.Contact__c)) {
							recalcDetailLedger('Proctor', key, 'Transportation', ct.Cost_Per_Attendee__c);
						} else if (attendees.contains(cta.Contact__c)) {
							recalcDetailLedger('Attendee', key, 'Transportation', ct.Cost_Per_Attendee__c);
						}
					} else if (cta.User__c != null) {
						if (users.contains(cta.User__c)) {
							string key = generateKey(cta.User__c, cta.User__r.Name);

							recalcDetailLedger('Staff', key, 'Transportation', ct.Cost_Per_Attendee__c);
						}				
					}
				}
			}		
		}

		//Incidentals
		if (UserUtil.canAccessIncidentalExpenses()) { 
			List<Campaign_Incidental__c> incidentals = [
				SELECT Cost_Per_Attendee__c,
					(SELECT Contact__c, Contact__r.Name, User__c, User__r.Name 
					 FROM Campaign_Incidental_Attendees__r 
					 WHERE (Contact__c IN :contacts OR User__c IN :users))
				FROM Campaign_Incidental__c
				WHERE Campaign__r.Id = :campaignId
				ORDER BY Purchase_Date_Time__c		
			];

			for(Campaign_Incidental__c ci :incidentals) {
				for(Campaign_Incidental_Attendee__c cia :ci.Campaign_Incidental_Attendees__r) {
					if (cia.Contact__c != null) {
						string key = generateKey(cia.Contact__c, cia.Contact__r.Name);

						if (proctors.contains(cia.Contact__c)) {
							recalcDetailLedger('Proctor', key, 'Incidentals', ci.Cost_Per_Attendee__c);
						} else if (attendees.contains(cia.Contact__c)) {
							recalcDetailLedger('Attendee', key, 'Incidentals', ci.Cost_Per_Attendee__c);
						}
					} else if (cia.User__c != null) {
						if (users.contains(cia.User__c)) {
							string key = generateKey(cia.User__c, cia.User__r.Name);

							recalcDetailLedger('Staff', key, 'Incidentals', ci.Cost_Per_Attendee__c);
						}				
					}
				}
			}		
		}

		//Hotels
		if (UserUtil.canAccessHotelExpenses()) { 
			List<Campaign_Hotel__c> hotels = [
				SELECT Id, Name,
					(SELECT Contact__c, Contact__r.Name, User__c, User__r.Name, Cost__c, Present__c 
					 FROM Campaign_Hotel_Attendees__r 
					 WHERE (Contact__c IN :contacts OR User__c IN :users))
				FROM Campaign_Hotel__c
				WHERE Campaign__r.Id = :campaignId
				ORDER BY Check_In_Date__c		
			];

			for(Campaign_Hotel__c ch :hotels) {
				for(Campaign_Hotel_Attendee__c cha :ch.Campaign_Hotel_Attendees__r) {
					if (cha.Contact__c != null) {
						string key = generateKey(cha.Contact__c, cha.Contact__r.Name);

						if (cha.Present__c) {
							if (proctors.contains(cha.Contact__c)) {
								recalcDetailLedger('Proctor', key, 'Hotel - Present', cha.Cost__c);
							} else if (attendees.contains(cha.Contact__c)) {
								recalcDetailLedger('Attendee', key, 'Hotel - Present', cha.Cost__c);
							}
						} else {
							if (proctors.contains(cha.Contact__c)) {
								recalcDetailLedger('Proctor', key, 'Hotel - Not Present', cha.Cost__c);
							} else if (attendees.contains(cha.Contact__c)) {
								recalcDetailLedger('Attendee', key, 'Hotel - Not Present', cha.Cost__c);
							}					
						}
					} else if (cha.User__c != null) {
						if (users.contains(cha.User__c)) {
							string key = generateKey(cha.User__c, cha.User__r.Name);

							if (cha.Present__c) { 
								recalcDetailLedger('Staff', key, 'Hotel - Present', cha.Cost__c);
							} else {
								recalcDetailLedger('Staff', key, 'Hotel - Not Present', cha.Cost__c);
							}
						}				
					}
				}
			}		
		}

		//Totals
		for(string memberType :detailMap.keySet()) {
			for(string memberIdentifier :detailMap.get(memberType).keySet()) {
				decimal total = 0;

				for(string expenseType :detailMap.get(memberType).get(memberIdentifier).keySet()) {
					total += detailMap.get(memberType).get(memberIdentifier).get(expenseType);
				}

				detailMap.get(memberType).get(memberIdentifier).put('Total', total);
			}
		}

		return detailMap;
	}

	private static void recalcDetailLedger(string memberType, string memberIdentifier, string expenseType, decimal cost) {
		if (!detailMap.get(memberType).containsKey(memberIdentifier)) {
			detailMap.get(memberType).put(memberIdentifier, new Map<string, decimal>{ expenseType => cost });
		} else {
			if (!detailMap.get(memberType).get(memberIdentifier).containsKey(expenseType)) {
				detailMap.get(memberType).get(memberIdentifier).put(expenseType, cost);
			} else {
				decimal totalCost = (detailMap.get(memberType).get(memberIdentifier).get(expenseType) + cost);
				detailMap.get(memberType).get(memberIdentifier).put(expenseType, totalCost);			
			}
		}		
	}

	private static string generateKey(id recId, string recName) {
		return string.valueOf(recId) + '|' + recName;
	}
}