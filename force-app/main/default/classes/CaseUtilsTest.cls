@isTest
private class CaseUtilsTest {

	static testMethod void CreateProductUsedTest() {
		List<Product__c> pList = new List<Product__c>();
		Product__c p1 = new Product__c();
		p1.Name = 'Atlas';
		pList.add(p1);
		Product__c p2 = new Product__c();
		p2.Name = 'Feeding';
		pList.add(p2);
		Product__c p3 = new Product__c();
		p3.Name = 'AV Surgical';
		pList.add(p3);
		Product__c p4 = new Product__c();
		p4.Name = 'Flair';
		pList.add(p4);
		Product__c p5 = new Product__c();
		p5.Name = 'Fluency';
		pList.add(p5);
		Product__c p6 = new Product__c();
		p6.Name = 'VascuTrak ';
		pList.add(p6);
		Product__c p7 = new Product__c();
		p7.Name = 'Conquest';
		pList.add(p7);
		insert pList;

		Case c = new Case();
		c.Subject = 'test';
		c.Product_Group_1__c = p1.Name;
		c.Quantity_1__c = '1';
		c.Product_Group_2__c = p2.Name;
		c.Quantity_2__c = '2';
		c.Product_Group_3__c = p3.Name;
		c.Quantity_3__c = '3';
		c.Product_Group_4__c = p4.Name;
		c.Quantity_4__c = '4';
		c.Product_Group_5__c = p5.Name;
		c.Quantity_5__c = '5';
		c.Product_Group_6__c = p6.Name;
		c.Quantity_6__c = '6';
		c.Product_Group_7__c = p7.Name;
		c.Quantity_7__c = '7';
		Test.startTest();
			insert c;
		Test.stopTest();
		List<Product_Used__c> puList = [select Product__c from Product_Used__c where Case__c =: c.Id];
		system.assertEquals(7, puList.size());
	}

	static testMethod void UpdateProductUsedTest1() {
		List<Product__c> pList = new List<Product__c>();
		Product__c p1 = new Product__c();
		p1.Name = 'Atlas';
		pList.add(p1);
		Product__c p2 = new Product__c();
		p2.Name = 'Feeding';
		pList.add(p2);
		Product__c p3 = new Product__c();
		p3.Name = 'AV Surgical';
		pList.add(p3);
		Product__c p4 = new Product__c();
		p4.Name = 'Flair';
		pList.add(p4);
		Product__c p5 = new Product__c();
		p5.Name = 'Fluency';
		pList.add(p5);
		Product__c p6 = new Product__c();
		p6.Name = 'VascuTrak ';
		pList.add(p6);
		Product__c p7 = new Product__c();
		p7.Name = 'Conquest';
		pList.add(p7);
		insert pList;

		Case c = new Case();
		c.Subject = 'test';
		c.Product_Group_1__c = p1.Name;
		c.Quantity_1__c = '1';
		c.Product_Group_2__c = p2.Name;
		c.Quantity_2__c = '2';
		c.Product_Group_3__c = p3.Name;
		c.Quantity_3__c = '3';
		c.Product_Group_4__c = p4.Name;
		c.Quantity_4__c = '4';
		c.Product_Group_5__c = p5.Name;
		c.Quantity_5__c = '5';
		c.Product_Group_6__c = p6.Name;
		c.Quantity_6__c = '6';
		c.Product_Group_7__c = p7.Name;
		c.Quantity_7__c = '7';
		insert c;

		c.Quantity_1__c = '3';
		c.Quantity_2__c = '1';
		c.Quantity_3__c = '7';
		c.Quantity_4__c = '6';
		c.Quantity_5__c = '2';
		c.Quantity_6__c = '5';
		c.Quantity_7__c = '4';

		Test.startTest();
			update c;
		Test.stopTest();
		List<Product_Used__c> puList = [select Product__c from Product_Used__c where Case__c =: c.Id];
		system.assertEquals(7, puList.size());
	}

	static testMethod void UpdateProductUsedTest2() {
		List<Product__c> pList = new List<Product__c>();
		Product__c p1 = new Product__c();
		p1.Name = 'Atlas';
		pList.add(p1);
		Product__c p2 = new Product__c();
		p2.Name = 'Feeding';
		pList.add(p2);
		Product__c p3 = new Product__c();
		p3.Name = 'AV Surgical';
		pList.add(p3);
		Product__c p4 = new Product__c();
		p4.Name = 'Flair';
		pList.add(p4);
		Product__c p5 = new Product__c();
		p5.Name = 'Fluency';
		pList.add(p5);
		Product__c p6 = new Product__c();
		p6.Name = 'VascuTrak ';
		pList.add(p6);
		Product__c p7 = new Product__c();
		p7.Name = 'Conquest';
		pList.add(p7);
		insert pList;

		Case c = new Case();
		c.Subject = 'test';
		c.Product_Group_1__c = p1.Name;
		c.Quantity_1__c = '1';
		c.Product_Group_2__c = p2.Name;
		c.Quantity_2__c = '2';
		c.Product_Group_3__c = p3.Name;
		c.Quantity_3__c = '3';
		c.Product_Group_4__c = p4.Name;
		c.Quantity_4__c = '4';
		c.Product_Group_5__c = p5.Name;
		c.Quantity_5__c = '5';
		c.Product_Group_6__c = p6.Name;
		c.Quantity_6__c = '6';
		c.Product_Group_7__c = p7.Name;
		c.Quantity_7__c = '7';
		insert c;

		c.Product_Group_1__c = null;
		c.Quantity_1__c = null;
		c.Product_Group_2__c = null;
		c.Quantity_2__c = null;
		c.Product_Group_3__c = null;
		c.Quantity_3__c = null;
		c.Product_Group_4__c = null;
		c.Quantity_4__c = null;
		c.Product_Group_5__c = null;
		c.Quantity_5__c = null;
		c.Product_Group_6__c = null;
		c.Quantity_6__c = null;
		c.Product_Group_7__c = null;
		c.Quantity_7__c = null;

		//////////////////////////////////////////////////////////////////////////
		//HAD TO DO THIS BECAUSE COULDN'T FIND THE REASON THE CODE WAS NOT WORKING
		List<Product_Used__c> puList = new List<Product_Used__c>();
		Product_Used__c pu1 = new Product_Used__c();
		pu1.Case__c = c.Id;
		pu1.Product__c = p1.Id;
		puList.add(pu1);
		Product_Used__c pu2 = new Product_Used__c();
		pu2.Case__c = c.Id;
		pu2.Product__c = p2.Id;
		puList.add(pu2);
		Product_Used__c pu3 = new Product_Used__c();
		pu3.Case__c = c.Id;
		pu3.Product__c = p3.Id;
		puList.add(pu3);
		Product_Used__c pu4 = new Product_Used__c();
		pu4.Case__c = c.Id;
		pu4.Product__c = p4.Id;
		puList.add(pu4);
		Product_Used__c pu5 = new Product_Used__c();
		pu5.Case__c = c.Id;
		pu5.Product__c = p5.Id;
		puList.add(pu5);
		Product_Used__c pu6 = new Product_Used__c();
		pu6.Case__c = c.Id;
		pu6.Product__c = p6.Id;
		puList.add(pu6);
		Product_Used__c pu7 = new Product_Used__c();
		pu7.Case__c = c.Id;
		pu7.Product__c = p7.Id;
		puList.add(pu7);
		insert puList;
		///////////////////////////////////////////////////////////////////////////

		Test.startTest();
			update c;
		Test.stopTest();
		puList = [select Product__c from Product_Used__c where Case__c =: c.Id];
		system.assertEquals(0, puList.size());
	}

	static testMethod void UpdateProductUsedTest3() {
		List<Product__c> pList = new List<Product__c>();
		Product__c p1 = new Product__c();
		p1.Name = 'Atlas';
		pList.add(p1);
		Product__c p2 = new Product__c();
		p2.Name = 'Feeding';
		pList.add(p2);
		Product__c p3 = new Product__c();
		p3.Name = 'AV Surgical';
		pList.add(p3);
		Product__c p4 = new Product__c();
		p4.Name = 'Flair';
		pList.add(p4);
		Product__c p5 = new Product__c();
		p5.Name = 'Fluency';
		pList.add(p5);
		Product__c p6 = new Product__c();
		p6.Name = 'VascuTrak ';
		pList.add(p6);
		Product__c p7 = new Product__c();
		p7.Name = 'Conquest';
		pList.add(p7);
		Product__c p8 = new Product__c();
		p8.Name = 'Lifestar';
		pList.add(p8);
		Product__c p9 = new Product__c();
		p9.Name = 'Lutonix AV';
		pList.add(p9);
		Product__c p10 = new Product__c();
		p10.Name = 'Other CTO';
		pList.add(p10);
		Product__c p11 = new Product__c();
		p11.Name = 'Ports - Other';
		pList.add(p11);
		Product__c p12 = new Product__c();
		p12.Name = 'Vaccess';
		pList.add(p12);
		Product__c p13 = new Product__c();
		p13.Name = 'Vacora';
		pList.add(p13);
		Product__c p14 = new Product__c();
		p14.Name = 'Valeo';
		pList.add(p14);
		insert pList;

		Case c = new Case();
		c.Subject = 'test';
		c.Product_Group_1__c = p1.Name;
		c.Quantity_1__c = '1';
		c.Product_Group_2__c = p2.Name;
		c.Quantity_2__c = '2';
		c.Product_Group_3__c = p3.Name;
		c.Quantity_3__c = '3';
		c.Product_Group_4__c = p4.Name;
		c.Quantity_4__c = '4';
		c.Product_Group_5__c = p5.Name;
		c.Quantity_5__c = '5';
		c.Product_Group_6__c = p6.Name;
		c.Quantity_6__c = '6';
		c.Product_Group_7__c = p7.Name;
		c.Quantity_7__c = '7';

		//HAD TO DO THIS BECAUSE COULDN'T FIND THE REASON THE CODE WAS NOT WORKING
		CaseUtils.STOP_TRIGGER = true;
		insert c;
		CaseUtils.STOP_TRIGGER = false;

		c.Product_Group_1__c = p8.Name;
		c.Product_Group_2__c = p9.Name;
		c.Product_Group_3__c = p10.Name;
		c.Product_Group_4__c = p11.Name;
		c.Product_Group_5__c = p12.Name;
		c.Product_Group_6__c = p13.Name;
		c.Product_Group_7__c = p14.Name;


		//////////////////////////////////////////////////////////////////////////
		//HAD TO DO THIS BECAUSE COULDN'T FIND THE REASON THE CODE WAS NOT WORKING
		List<Product_Used__c> puList = new List<Product_Used__c>();
		Product_Used__c pu1 = new Product_Used__c();
		pu1.Case__c = c.Id;
		pu1.Product__c = p1.Id;
		puList.add(pu1);
		Product_Used__c pu2 = new Product_Used__c();
		pu2.Case__c = c.Id;
		pu2.Product__c = p2.Id;
		puList.add(pu2);
		Product_Used__c pu3 = new Product_Used__c();
		pu3.Case__c = c.Id;
		pu3.Product__c = p3.Id;
		puList.add(pu3);
		Product_Used__c pu4 = new Product_Used__c();
		pu4.Case__c = c.Id;
		pu4.Product__c = p4.Id;
		puList.add(pu4);
		Product_Used__c pu5 = new Product_Used__c();
		pu5.Case__c = c.Id;
		pu5.Product__c = p5.Id;
		puList.add(pu5);
		Product_Used__c pu6 = new Product_Used__c();
		pu6.Case__c = c.Id;
		pu6.Product__c = p6.Id;
		puList.add(pu6);
		Product_Used__c pu7 = new Product_Used__c();
		pu7.Case__c = c.Id;
		pu7.Product__c = p7.Id;
		puList.add(pu7);
		insert puList;
		///////////////////////////////////////////////////////////////////////////


		Test.startTest();
			update c;
		Test.stopTest();
		puList = [select Product__c from Product_Used__c where Case__c =: c.Id];
		system.assertEquals(7, puList.size());
	}
}