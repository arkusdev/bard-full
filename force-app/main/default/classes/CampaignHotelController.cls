public class CampaignHotelController {
	@auraEnabled
	public static List<CampaignHotelDTO> fetchCampaignHotels(id campaignId) {
		List<CampaignHotelDTO> dtos = new List<CampaignHotelDTO>();

		if (UserUtil.canAccessHotelExpenses()) { 
			List<Campaign_Hotel__c> hotels = [
				SELECT Id, Campaign__c, Name, Check_In_Date__c, Check_Out_Date__c, Room_Block__c,
				Notes__c, Total_Attendees__c, Attendee_Present_Count__c, Attendee_Absent_Count__c, 
				Additional_Attendees__c, Additional_Attendees_Cost__c, Total_Cost__c, Total_Present_Cost__c, 
				Total_Attendee_Cost__c, Total_Absent_Cost__c, Day_Count__c,
					(SELECT Id, Contact__c, Contact__r.Name, User__c, User__r.Name, Name, 
							Reservation_Date__c, Present__c, Cost__c, Confirmation_Number__c
							FROM Campaign_Hotel_Attendees__r 
							ORDER BY Reservation_Date__c)
				FROM Campaign_Hotel__c
				WHERE Campaign__c = :campaignId
			];

			for(Campaign_Hotel__c ch :hotels) {
				dtos.add(new CampaignHotelDTO(ch));
			}		
		}

		return dtos;
	}

	@auraEnabled
	public static CampaignHotelDTO fetchCampaignHotel(id hotelId) {
		try {

			if (!UserUtil.canAccessHotelExpenses()) { return null; }

			Campaign_Hotel__c hotel = [
				SELECT Id, Campaign__c, Name, Check_In_Date__c, Check_Out_Date__c, Room_Block__c,
				Notes__c, Total_Attendees__c, Attendee_Present_Count__c, Attendee_Absent_Count__c, 
				Additional_Attendees__c, Additional_Attendees_Cost__c, Total_Cost__c, Total_Present_Cost__c, 
				Total_Attendee_Cost__c, Total_Absent_Cost__c, Day_Count__c,
					(SELECT Id, Contact__c, Contact__r.Name, User__c, User__r.Name, Name, 
							Reservation_Date__c, Present__c, Cost__c, Confirmation_Number__c
							FROM Campaign_Hotel_Attendees__r 
							ORDER BY Reservation_Date__c)
				FROM Campaign_Hotel__c
				WHERE Id = :hotelId
			][0];

			Set<Id> contacts = new Set<Id>();
			Set<Id> users = new Set<Id>();

			for(Campaign_Hotel_Attendee__c cha :hotel.Campaign_Hotel_Attendees__r) {
				if (cha.Contact__c != null) {
					contacts.add(cha.Contact__c);
				} else {
					users.add(cha.User__c);
				}
			}			

			List<CampaignMember> availMembers = (contacts.isEmpty() ? fetchCampaignMembers(hotel.Campaign__c) : fetchCampaignMembers(hotel.Campaign__c, contacts));
			List<Campaign_User__c> availUsers = (users.isEmpty() ? fetchCampaignUsers(hotel.Campaign__c) : fetchCampaignUsers(hotel.Campaign__c, users));

			CampaignHotelDTO dto = new CampaignHotelDTO(hotel, availMembers, availUsers);

			return dto;		

		} catch(QueryException ex) {
			return null;
		}
	}

	@auraEnabled
	public static CampaignHotelDTO createNewCampaignHotel(id campaignId) {
		if (!UserUtil.canUpsertHotelExpenses()) { return null; }

		Campaign_Hotel__c hotel = new Campaign_Hotel__c(
			Campaign__c = campaignId
		);

		List<CampaignMember> availMembers = fetchCampaignMembers(campaignId);
		List<Campaign_User__c> availUsers = fetchCampaignUsers(campaignId);

		CampaignHotelDTO dto = new CampaignHotelDTO(hotel, availMembers, availUsers);

		return dto;
	}

	private static List<CampaignMember> fetchCampaignMembers(id campaignId, Set<Id> contacts) { 
		return [
			SELECT ContactId, Contact.Name, Type__c
			FROM CampaignMember 
			WHERE CampaignId = :campaignId
			AND ContactId NOT IN :contacts
			AND Type__c != 'Waitlist'
		];
	}

	private static List<Campaign_User__c> fetchCampaignUsers(id campaignId, Set<Id> users) { 
		return [
			SELECT User__c, User__r.Name
			FROM Campaign_User__c
			WHERE Campaign__c = :campaignId
			AND User__c NOT IN :users
		];
	}

	private static List<CampaignMember> fetchCampaignMembers(id campaignId) { 
		return [
			SELECT ContactId, Contact.Name, Type__c
			FROM CampaignMember 
			WHERE CampaignId = :campaignId
			AND Type__c != 'Waitlist'
		];	
	}

	private static List<Campaign_User__c> fetchCampaignUsers(id campaignId) { 
		return [
			SELECT User__c, User__r.Name
			FROM Campaign_User__c
			WHERE Campaign__c = :campaignId
		];
	}

	@auraEnabled
	public static void deleteCampaignHotel(id hotelId) {
		if (UserUtil.canDeleteHotelExpenses()) { 
			Campaign_Hotel__c ch = new Campaign_Hotel__c(
				Id = hotelId
			);

			delete ch;		
		}
	}

	@auraEnabled
	public static void saveCampaignHotel(string dto) {
		if (UserUtil.canUpsertHotelExpenses()) { 
			CampaignHotelDTO hotel = ((CampaignHotelDTO)System.JSON.deserializeStrict(dto, CampaignHotelDTO.Class));

			Campaign_Hotel__c ch = new Campaign_Hotel__c(
				Id = hotel.hotelId,
				Campaign__c = hotel.campaignId,
				Name = hotel.name,
				Check_In_Date__c = hotel.checkInDate,
				Check_Out_Date__c = hotel.checkOutDate,
				Room_Block__c = hotel.roomBlock,
				Additional_Attendees__c = hotel.additionalAttendees,
				Additional_Attendees_Cost__c = hotel.additionalAttendeesCost,
				Notes__c = hotel.notes
			);

			upsert ch;

			saveCampaignHotelMembers(hotel, ch);
			saveCampaignHotelUsers(hotel, ch);		
		}
	}

	private static void saveCampaignHotelMembers(CampaignHotelDTO hotel, Campaign_Hotel__c cf) {
		if (!hotel.selectedMembers.isEmpty()) {
			Set<Id> selected = new Set<Id>();

			for(CampaignHotelAttendeeDTO cfa :hotel.selectedMembers) {
				if(!selected.contains(cfa.attendeeId)) {
					selected.add(cfa.attendeeId);
				}
			}

			//First let's remove any members that were removed entirely or had a date removed
			List<Campaign_Hotel_Attendee__c> allMembers = [
				SELECT Id, Contact__c, Reservation_Date__c
				FROM Campaign_Hotel_Attendee__c
				WHERE Campaign_Hotel__c = :cf.Id 
				AND User__c = NULL
				ORDER BY Contact__c, Reservation_Date__c
			];

			List<Id> hotelIds = new List<Id>();

			for(Campaign_Hotel_Attendee__c ch :allMembers) {
				if((!hotel.containsAttendee(ch.Contact__c, 'Contact')) || 
				   (!hotel.containsAttendeeReservation(ch.Contact__c, ch.Reservation_Date__c, 'Contact'))) {
					hotelIds.add(ch.Id);
				}
			}

			List<Campaign_Hotel_Attendee__c> deleteMembers = new List<Campaign_Hotel_Attendee__c>();

			for(Id item :hotelIds) {
				deleteMembers.add(new Campaign_Hotel_Attendee__c(
					Id = item
				));
			}

			if (!deleteMembers.isEmpty()) {
				delete deleteMembers;
			} 

			List<Campaign_Hotel_Attendee__c> members = new List<Campaign_Hotel_Attendee__c>();

			for(CampaignHotelAttendeeDTO cha :hotel.selectedMembers) {
				members.add(new Campaign_Hotel_Attendee__c(
					Id = cha.hotelAttendeeId,
					Campaign_Hotel__c = cf.Id,
					Name = cha.attendeeId + ': ' + cf.Name,
					Contact__c = cha.attendeeId,
					Reservation_Date__c = cha.reservationDate,
					Cost__c = cha.cost,
					Present__c = cha.present,
					Confirmation_Number__c = cha.confirmationNumber
				));	
			}

			if (!members.isEmpty()) {
				upsert members;
			}
		} else {
			List<Campaign_Hotel_Attendee__c> deleteMembers = [
				SELECT Id 
				FROM Campaign_Hotel_Attendee__c
				WHERE Campaign_Hotel__c = :cf.Id 
				AND Contact__c != NULL
				AND User__c = NULL
			];

			if (!deleteMembers.isEmpty()) {
				delete deleteMembers;
			}
		}
	}

	private static void saveCampaignHotelUsers(CampaignHotelDTO hotel, Campaign_Hotel__c cf) {
		if (!hotel.selectedUsers.isEmpty()) {
			Set<Id> selected = new Set<Id>();

			for(CampaignHotelAttendeeDTO cfa :hotel.selectedUsers) {
				if(!selected.contains(cfa.attendeeId)) {
					selected.add(cfa.attendeeId);
				}
			}

			//First let's remove any members that were removed entirely or had a date removed
			List<Campaign_Hotel_Attendee__c> allUsers = [
				SELECT Id, User__c, Reservation_Date__c
				FROM Campaign_Hotel_Attendee__c
				WHERE Campaign_Hotel__c = :cf.Id 
				AND Contact__c = NULL
				ORDER BY User__c, Reservation_Date__c
			];

			List<Id> hotelIds = new List<Id>();

			for(Campaign_Hotel_Attendee__c ch :allUsers) {
				if((!hotel.containsAttendee(ch.User__c, 'User')) || 
				   (!hotel.containsAttendeeReservation(ch.User__c, ch.Reservation_Date__c, 'User'))) {
					hotelIds.add(ch.Id);
				}
			}

			List<Campaign_Hotel_Attendee__c> deleteUsers = new List<Campaign_Hotel_Attendee__c>();

			for(Id item :hotelIds) {
				deleteUsers.add(new Campaign_Hotel_Attendee__c(
					Id = item
				));
			}

			System.debug('Chad');
			System.debug(deleteUsers);

			if (!deleteUsers.isEmpty()) {
				delete deleteUsers;
			} 

			List<Campaign_Hotel_Attendee__c> users = new List<Campaign_Hotel_Attendee__c>();

			for(CampaignHotelAttendeeDTO cha :hotel.selectedUsers) {
				users.add(new Campaign_Hotel_Attendee__c(
					Id = cha.hotelAttendeeId,
					Campaign_Hotel__c = cf.Id,
					Name = cha.attendeeId + ': ' + cf.Name,
					User__c = cha.attendeeId,
					Reservation_Date__c = cha.reservationDate,
					Cost__c = cha.cost,
					Present__c = cha.present,
					Confirmation_Number__c = cha.confirmationNumber
				));	
			}

			if (!users.isEmpty()) {
				upsert users;
			}
		} else {
			List<Campaign_Hotel_Attendee__c> deleteUsers = [
				SELECT Id 
				FROM Campaign_Hotel_Attendee__c
				WHERE Campaign_Hotel__c = :cf.Id 
				AND User__c != NULL
				AND Contact__c = NULL
			];

			if (!deleteUsers.isEmpty()) {
				delete deleteUsers;
			}
		}
	}
}