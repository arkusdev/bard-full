public with sharing class WavelinQCasesBatch implements Database.Batchable<sObject>, Database.Stateful{
    public Map<Id, WavelinQCasesBatchHandler.AccountInfo> accsUpdated;

    public WavelinQCasesBatch(){
        this.accsUpdated = new Map<Id, WavelinQCasesBatchHandler.AccountInfo>();
    }


    public List<Account> start(Database.BatchableContext bc) {
        return WavelinQCasesBatchHandler.getAccounts();
    }

    public void execute(Database.BatchableContext bc, List<Account> accounts) {
        if (WavelinQCasesBatchHandler.executeBatch()){
            Map<Id, WavelinQCasesBatchHandler.AccountInfo> accountsUpdated = WavelinQCasesBatchHandler.updateAccounts(accounts);
            this.accsUpdated.putAll(accountsUpdated);
        }
    }

    public void finish(Database.BatchableContext bc) {
        Integer accountsUpdatedSize = this.accsUpdated.size();
        System.debug('Accounts updated: ' + accountsUpdatedSize);
        if (accountsUpdatedSize > 0) {
            WavelinQCasesBatchHandler.handleNotifications(this.accsUpdated);
        }
    }
}