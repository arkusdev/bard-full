public class KOLSearchController {
	@auraEnabled
	public static List<PersonDTO> fetchKOLContacts(string params, string contractServiceDesc) {
		List<SearchParamViewModel> paramsVM = ((List<SearchParamViewModel>)System.JSON.deserializeStrict(params, List<SearchParamViewModel>.Class));
		List<PersonDTO> dtos = new List<PersonDTO>();

		string selectStatement = 'SELECT Id, Name, Phone, Email, Contact_Photo__c, Contact_Bio__c, CV_on_File__c, ' +
								 'Account.Name, (SELECT Id, Contract_Services__c, ContractNumber, StartDate, EndDate FROM Contracts__r) ' +
								 'FROM Contact WHERE ';
		string whereClause = '';

		//Query directly against the contact object
		for(SearchParamViewModel vm :paramsVM) {
			if(vm.ParamName.equals(FieldNameUtil.facility_type)) {
				//Picklist is filled with 3 bucketed values that are hard coded here
				Map<string, List<string>> bucketMap = new Map<string, List<string>> {
					'Hospital' => new List<string>{ 'Hospital', 'VA Hospital' },
					'OBL' => new List<string>{ 'Surgery Center', 'Lab', 'Access Centers' },
					'All' => new List<string>{ 'Hospital', 'VA Hospital', 'Surgery Center', 'Lab', 'Access Centers' }
				};

				string facilityTypes = '';

				if (bucketMap.containsKey(vm.ParamValue)) {
					for(string ft :bucketMap.get(vm.ParamValue)) {
						facilityTypes += '\'' + ft + '\',';
					}				
				}

				if(!string.isBlank(facilityTypes)) {
					facilityTypes = facilityTypes.substring(0, facilityTypes.length() - 1);

					whereClause += 'Account.JDE_Facility_Type__c IN (' + facilityTypes + ') AND ';
				}

			} else if(vm.ParamName.equals(FieldNameUtil.provider_name)) {
				string providerName = '%' + string.escapeSingleQuotes(vm.ParamValue) + '%';
				whereClause += 'Name LIKE :providerName AND ';

			} else if(vm.ParamName.equals(FieldNameUtil.surgeon_specialty)) {
				string surgeonSpecialty = string.escapeSingleQuotes(vm.ParamValue);
				whereClause += 'Surgeon_Specialty__c = :surgeonSpecialty AND ';			

			} else if(vm.ParamName.equals(FieldNameUtil.surgical_technique)) {
				string surgicalTechnique = string.escapeSingleQuotes(vm.ParamValue);
				whereClause += 'Surgical_Technique__c = :surgicalTechnique AND ';						

			} else if(vm.ParamName.equals(FieldNameUtil.kol_flg)) {
				boolean kolFlg = (vm.ParamValue == 'true' ? true : false);
				whereClause += 'Is_KOL__c = :kolFlg AND ';
			}		
		}

		//This method will create a map of dynamic queries to run (4 at the most so it's okay inside this loop) and will
		//return a list of contact ID's for each query.  Each list of ID's is appended to the overall where clause in this 
		//method.  If one of the searches yields no results then no results will be returned for KOL Search either
		Map<string, string> queryMap = fetchContactList(paramsVM, contractServiceDesc);

		if (!queryMap.isEmpty()) {
			Map<string, Set<Object>> resultsMap = new Map<string, Set<Object>>();

			for(string obj :queryMap.keySet()) {
				Set<Object> results = new Set<Object>();
				List<SObject> objs = Database.query(queryMap.get(obj));

				for(SObject o :objs) {
					Schema.SObjectType ot = o.getSObjectType();
					Map<String, Object> fieldMap = o.getPopulatedFieldsAsMap();
					for(string key :fieldMap.keySet()) {
						if (ot == Contact.sObjectType) {
							if (key.equalsIgnoreCase('Id')) { 
								if (!results.contains(fieldMap.get(key))) {
									results.add(fieldMap.get(key));
								}
							}
						} else {
							if (key.equalsIgnoreCase('Contact__c')) {
								if (!results.contains(fieldMap.get(key))) {
									results.add(fieldMap.get(key));
								}
							}
						}
					}
				}
				resultsMap.put(obj, results);
			}

			if (!resultsMap.isEmpty()) {
				for(string key :resultsMap.keySet()) { 
					if (!resultsMap.get(key).isEmpty()) {
						string cids = '';

						for(Object cid :resultsMap.get(key)) {
							cids += '\'' + cid.toString() + '\',';
						}

						cids = cids.substring(0, cids.length() - 1);

						whereClause += 'Id IN (' + cids + ') AND ';
					} else {
						return dtos; //no matching contacts found w/ the given criteria so no need to look w/ the other criteria	
					}
				}
			} else {
				return dtos; //no matching contacts found w/ the given criteria so no need to look w/ the other criteria
			}
		}

		if (!string.isBlank(whereClause)) {
			whereClause = whereClause.substring(0, whereClause.length() - 4); //Remove the last AND

			selectStatement += whereClause + 'ORDER BY Name';

			try {
				List<Contact> contacts = Database.query(selectStatement);	

				for(Contact c :contacts) {
					dtos.add(new PersonDTO(c));
				}

			} catch(Exception ex) {
				System.debug(ex.getMessage() + ex.getLineNumber());
			}		
		}

		return dtos;		
	}

	private static Map<string, string> fetchContactList(List<SearchParamViewModel> viewModels, string contractServiceDesc) {
		Map<string, string> queryMap = new Map<string, string>();
		string divisionObj = 'Division__c';
		string contractObj = 'Contract';
		string contactObj = 'Contact';
		string licenseObj = 'Contact_License__c';
		
		//This method will reduce the amount of SOQL queries to get a list of contact id's to search above
		//Can't do dynamic variable binding here.  Bummer.
		for(SearchParamViewModel vm :viewModels) {
			if(vm.ParamName.equals(FieldNameUtil.division)) {
				string qry = 'SELECT Contact__c FROM Division__c WHERE Division__c = ' + '\'' + string.escapeSingleQuotes(vm.ParamValue) + '\'';

				if (queryMap.containsKey(divisionObj)) {
					qry = queryMap.get(divisionObj);
					qry += ' AND Division__c = ' + '\'' + string.escapeSingleQuotes(vm.ParamValue) + '\'';
				}

				queryMap.put(divisionObj, qry);

			} else if(vm.ParamName.equals(FieldNameUtil.product_category)) {
				string qry = 'SELECT Contact__c FROM Division__c WHERE Product_Category__c = ' + '\'' + string.escapeSingleQuotes(vm.ParamValue) + '\'';

				if (queryMap.containsKey(divisionObj)) {
					qry = queryMap.get(divisionObj);
					qry += ' AND Product_Category__c = ' + '\'' + string.escapeSingleQuotes(vm.ParamValue) + '\'';
				}

				queryMap.put(divisionObj, qry);

			} else if(vm.ParamName.equals(FieldNameUtil.contract_services)) {
				string contractServices = '';
				string contractServiceDescQry = ' ';

				for(string service :vm.ParamValue.split(';')) {
					contractServices += '\'' + service + '\',';

					if (service.equalsIgnoreCase('Other') && string.isBlank(contractServiceDescQry.trim())) {
						contractServiceDescQry = ' AND Contract_Services_Description__c LIKE ' + '\'%' + string.escapeSingleQuotes(contractServiceDesc) + '%\'';
					}
				}

				contractServices = contractServices.substring(0, contractServices.length() - 1);

				string qry = 'SELECT Contact__c FROM Contract WHERE Contract_Services__c INCLUDES (' + contractServices + ')';

				if (queryMap.containsKey(contractObj)) {
					qry = queryMap.get(contractObj);
					qry += ' AND Contract_Services__c INCLUDES (' + contractServices + ')';
				}
                
				if (!string.isBlank(contractServiceDescQry.trim())) {
					qry += contractServiceDescQry;
				}                

				queryMap.put(contractObj, qry);

			} else if(vm.ParamName.equals(FieldNameUtil.active_bd_contract)) {
				string qry = '';

				if (vm.ParamValue.equalsIgnoreCase('None')) {
					//Note that we're hitting the Contact object here - not Contract - and we'll always add it to the map
					queryMap.put(contactObj, 'SELECT Id FROM Contact WHERE Id NOT IN (SELECT Contact__c FROM Contract)');
				} else {
					if (!queryMap.containsKey(contractObj)) {
						if(vm.ParamValue == 'Active') {
							qry = 'SELECT Contact__c FROM Contract WHERE StartDate <= Today AND EndDate >= Today';
						} else if(vm.ParamValue == 'Expired') {
							qry = 'SELECT Contact__c FROM Contract WHERE ((StartDate < Today AND EndDate < Today) OR (StartDate > Today AND EndDate > Today))';
						}
					} else {
						qry = queryMap.get(contractObj);

						if(vm.ParamValue == 'Active') {
							qry += ' AND StartDate <= Today AND EndDate >= Today';
						} else if(vm.ParamValue == 'Expired') {
							qry += ' AND ((StartDate < Today AND EndDate < Today) OR (StartDate > Today AND EndDate > Today))';
						}
					}				

					queryMap.put(contractObj, qry);
				}
				
			} else if(vm.ParamName.equals(FieldNameUtil.licensed_regions)) {
				string licensedRegions = '';

				for(string region :vm.ParamValue.split(';')) {
					licensedRegions += '\'' + region + '\',';
				}

				licensedRegions = licensedRegions.substring(0, licensedRegions.length() - 1);

				string qry = 'SELECT Contact__c FROM Contact_License__c WHERE Licensed_State__r.Licensed_Region__r.Name IN (' + licensedRegions + ')';

				if (queryMap.containsKey(licenseObj)) {
					qry = queryMap.get(licenseObj);
					qry += ' AND Licensed_State__r.Licensed_Region__r.Name IN (' + licensedRegions + ')';
				}

				queryMap.put(licenseObj, qry);

			} else if(vm.ParamName.equals(FieldNameUtil.licensed_states)) {
				string licensedStates = '';

				for(string state :vm.ParamValue.split(';')) {
					licensedStates += '\'' + state + '\',';
				}

				licensedStates = licensedStates.substring(0, licensedStates.length() - 1);

				string qry = 'SELECT Contact__c FROM Contact_License__c WHERE Licensed_State__r.State_Abbreviation__c IN (' + licensedStates + ')';

				if (queryMap.containsKey(licenseObj)) {
					qry = queryMap.get(licenseObj);
					qry += ' AND Licensed_State__r.State_Abbreviation__c IN (' + licensedStates + ')';
				}

				queryMap.put(licenseObj, qry);
			}	
		}

		return queryMap;
	}

	@auraEnabled
	public static List<string> fetchProductCategories(string divisionName) {
		return PicklistUtil.fetchProductCategories(divisionName);
    }

	@auraEnabled
	public static Map<string, string> fetchLicensedStates(string regionNames) {
		return PicklistUtil.fetchLicensedStates(regionNames);
    }

	@auraEnabled
	public static Map<string, Map<string, string>> fetchPickListValues(List<string> fieldNames) {
		return PicklistUtil.fetchPickListValues(fieldNames);		
	}
}