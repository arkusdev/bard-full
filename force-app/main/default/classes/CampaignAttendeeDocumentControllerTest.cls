@isTest
public class CampaignAttendeeDocumentControllerTest {
	@testSetup
	static void createResources() {
		User u = [
			SELECT Id, Name, Phone, Email, Title, IsActive
			FROM User 
			WHERE Email = 'bd@apostletech.com'
		][0]; 

		System.runAs(u) {
			List<Account> accts = TestDataFactory.createAccounts(1);

			List<Contact> proctorContacts = TestDataFactory.createContacts(accts, 1);
			List<Contact> attendeeContacts = TestDataFactory.createContacts(accts, 4);

			List<User> bdUsers = TestDataFactory.createUsers(2);

			List<Campaign> campaigns = TestDataFactory.createCampaigns(accts[0], 1);

			TestDataFactory.createCampaignDocuments(campaigns[0], 3);

			List<CampaignMember> proctorMembers = TestDataFactory.createCampaignMembers(campaigns[0], proctorContacts, 'Proctor');
			List<CampaignMember> attendeeMembers = TestDataFactory.createCampaignMembers(campaigns[0], attendeeContacts, 'Attendee');

			List<Campaign_User__c> userMember = TestDataFactory.createCampaignUsers(campaigns[0], bdUsers);

			TestDataFactory.createCampaignDocuments(campaigns[0], 1);
		}
	}

	@isTest
	static void fetchCampaignAttendeeDocuments_Attendee_UnitTest() {
		Campaign c = [
			SELECT Id
			FROM Campaign
		][0];

		CampaignMember cm = [
			SELECT ContactId
			FROM CampaignMember
			WHERE CampaignId = :c.Id
			AND Type__c = 'Attendee'
		][0];

		List<CampaignAttendeeDocumentDTO> dtos = CampaignAttendeeDocumentController.fetchCampaignAttendeeDocuments(cm.ContactId, c.Id, 'attendee');

		System.assertEquals(4, dtos.size());
	}

	@isTest
	static void fetchCampaignAttendeeDocuments_User_UnitTest() {
		Campaign c = [
			SELECT Id
			FROM Campaign
		][0];

		Campaign_User__c cu = [
			SELECT User__c
			FROM Campaign_User__c
			WHERE Campaign__c = :c.Id
		][0];

		List<CampaignAttendeeDocumentDTO> dtos = CampaignAttendeeDocumentController.fetchCampaignAttendeeDocuments(cu.User__c, c.Id, 'user');

		System.assertEquals(4, dtos.size());
	}
	
	@isTest
	static void saveCampaignAttendeeDocuments_UnitTest() {
		Campaign c = [
			SELECT Id
			FROM Campaign
		][0];

		CampaignMember cm = [
			SELECT ContactId
			FROM CampaignMember
			WHERE CampaignId = :c.Id
			AND Type__c = 'Attendee'
		][0];

		List<CampaignAttendeeDocumentDTO> dtos = CampaignAttendeeDocumentController.fetchCampaignAttendeeDocuments(cm.ContactId, c.Id, 'attendee');
		
		for(CampaignAttendeeDocumentDTO cad :dtos) {
			cad.received = true;
		}

		CampaignAttendeeDocumentController.saveCampaignAttendeeDocuments(JSON.serialize(dtos));

		dtos = CampaignAttendeeDocumentController.fetchCampaignAttendeeDocuments(cm.ContactId, c.Id, 'attendee');

		System.assertEquals(4, dtos.size());

		for(CampaignAttendeeDocumentDTO cad :dtos) {
			System.assertEquals(true, cad.received);
		}
	}
}