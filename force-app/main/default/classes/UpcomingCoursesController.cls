public class UpcomingCoursesController {
    @AuraEnabled
    public static Contact getContactFields (String contactId) {
        Contact ct = new Contact();
        List<Contact> results = [SELECT Email, MobilePhone, Surgeon_Specialty__c FROM Contact WHERE Id =:contactId LIMIT 1];
        if (!results.isEmpty()) ct = results.get(0);
        return ct;
    }
	@auraEnabled
	public static List<string> fetchproductCategories(string divisionName) {
		return PicklistUtil.fetchproductCategories(divisionName);
    }

	@auraEnabled
	public static Map<string, Map<string, string>> fetchPickListValues(List<string> fieldNames) {
		return PicklistUtil.fetchPickListValues(fieldNames);		
	}

	@auraEnabled
	public static List<CampaignDTO> fetchUpcomingCampaignsByContact(Id contactId) {
		List<CampaignDTO> dtos = new List<CampaignDTO>();
		
		try {
			List<Campaign> cs = [
				SELECT Name, Technique__c, Classification__c, Product_Category__c, SubLedger__c, Facility__c, StartDate, Remaining_Seats__c, Description, Owner.Name, 
					(SELECT Contact.Name FROM CampaignMembers WHERE Type__c = 'Proctor')
				FROM Campaign
				WHERE Id IN (SELECT CampaignId FROM CampaignMember WHERE ContactId = :contactId AND Campaign.StartDate >= :Date.today())			
			];

			for(Campaign c :cs) {
				dtos.add(new CampaignDTO(c));
			}	
		} catch(Exception ex) {
			throw new AuraHandledException('Darn it! Something went wrong: ' + ex.getMessage());  
		}

		return dtos;
	}

	@auraEnabled
	public static List<CampaignDTO> fetchUpcomingCampaigns(Id contactId, string params) {
		List<SearchParamViewModel> paramsVM = ((List<SearchParamViewModel>)System.JSON.deserializeStrict(params, List<SearchParamViewModel>.Class));
		List<CampaignDTO> dtos = new List<CampaignDTO>();
		
		try {

			string selectStatement = 'SELECT Name, Technique__c, Classification__c, Product_Category__c, StartDate, Remaining_Seats__c, ' +
									 'Facility__c, SubLedger__c, ' + 
									 '(SELECT ContactId FROM CampaignMembers) ' +
									 'FROM Campaign ';

			string whereClause = 'WHERE StartDate >= Today AND Status = \'' + string.escapeSingleQuotes('Registrations Open') + '\' AND ';

			for(SearchParamViewModel vm :paramsVM) {
				if(vm.ParamName.equals(FieldNameUtil.course_title)) {
					string courseTitle = '%' + string.escapeSingleQuotes(vm.ParamValue) + '%';
					whereClause += 'Name LIKE :courseTitle AND ';

				} else if(vm.ParamName.equals(FieldNameUtil.division)) {
					string division = string.escapeSingleQuotes(vm.ParamValue);
					whereClause += 'Classification__c = :division AND ';			

				} else if(vm.ParamName.equals(FieldNameUtil.product_category)) {
					string productCategory = string.escapeSingleQuotes(vm.ParamValue);
					whereClause += 'Product_Category__c = :productCategory AND ';						

				}
			}

			whereClause = whereClause.substring(0, whereClause.length() - 4); //Remove the last AND

			selectStatement += whereClause + 'ORDER BY Name';

			List<Campaign> cs = Database.query(selectStatement);	

			for(Campaign c :cs) {
				dtos.add(new CampaignDTO(c, contactId));
			}	

		} catch(Exception ex) {
			throw new AuraHandledException('Darn it! Something went wrong: ' + ex.getMessage());  
		}

		return dtos;
	}

	@auraEnabled
	public static string addMember(Id campaignId, Id memberId, string type) { 
		CampaignMemberController.addMember(campaignId, memberId, type);

		return getMemberName(memberId);
	}

	@auraEnabled
	public static string removeMember(Id campaignId, Id memberId, string type) { 
		CampaignMemberController.removeMember(campaignId, memberId, type);

		return getMemberName(memberId);
	}

	private static string getMemberName(Id contactId) {
		string contactName = '';

		try {
			Contact c = [
				SELECT Name 
				FROM Contact 
				WHERE Id= :contactId
			][0];

			contactName = c.Name;
		} catch(Exception ex) {
			//Do nothing
		}

		return contactName;
	}
}