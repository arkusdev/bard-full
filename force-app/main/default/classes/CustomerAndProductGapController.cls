/**
* @author      Federico Seco <federico.seco@arkusinc.com>
* @version     1.0
*/
public with sharing class CustomerAndProductGapController {
    /**
  * Entry method that should be the only one exposed to the interface.
  * It's intended to fill a table with sales data.
  * Each row will represent an account/product and each column a calculation.
  *
  * Results will depend on the user selected and the period.
  * The user id will generate a list of users all the way down the
  * role hierarchy tree.
  * The period option could be either 'month', 'quarter', or 'year'
  *
  * The method will throw an exception if any object or field is not
  * accessible by the current user.
  */
    @AuraEnabled(Cacheable=true)
    public static CalculationsResponse getCalculations(String userId, String period, String view, String accountID, Boolean distributor, Boolean topUser) {
        CalculationsResponse response = new CalculationsResponse();        
        try {
            if (topUser){
                CapGapUtil.fillResponseInfo(userId, period, response);
            }
            fillTableDataAggr(response, period, view, accountID, distributor, userId);
        }catch(Exception exc){
            String message = exc.getMessage();
            String excMessage = 'Some objects or fields are not accessible: ' + message;
            throw new AuraHandledException(excMessage);
        }
        return response;
    }

    /**
  * receives a response object and a String representing a period (month, quarter or year)
  *
  * Completes the passed object (by reference). Doesn't return a value.
  */
    private static void fillTableDataAggr (CalculationsResponse response, String period, String view, String accountID, Boolean distributor, String userId) {
        if (response.currentWorkingDays == 0 && period.equals('month')) return;

        List<String> userIds = new List<String>{ userId };

        if (view == 'opportunity') {
            response.oppsList = CaPGapUtil.getOpportunities(userIds);
        } else {
            response.catList = CaPGapUtil.getCategories();
        }

        response.irsaAggr = CaPGapUtil.getIRSASalesAggr(userIds, period, view, accountID, distributor, response.thisMonthTotalSellingDays, response.thisMonthOngoingSellingDays);
    	response.shadAggr = CaPGapUtil.getPriorFiscalYearSalesAggr(userIds, view, accountID);
    }
    public class CalculationsResponse {
        @AuraEnabled
        public Integer                          currentWorkingDays          { get; set; }
        @AuraEnabled
        public Integer                          totalWorkingDays            { get; set; }
        @AuraEnabled
        public Integer                          thisMonthTotalSellingDays   { get; set; }
        @AuraEnabled
        public Integer                          thisMonthOngoingSellingDays { get; set; }
        @AuraEnabled
        public Integer                          daysInPriorYear             { get; set; }
        @AuraEnabled
        public List<User>                       users                       { get; set; }
        @AuraEnabled
        public List<AggregateResult>            irsaAggr                    { get; set; }
        @AuraEnabled
        public List<AggregateResult>            shadAggr                    { get; set; }
        @AuraEnabled
        public List<Opportunity>                oppsList                    { get; set; }
        @AuraEnabled
        public List<Category_Combination__mdt>  catList                     { get; set; }

        public CalculationsResponse () {
            this.irsaAggr   = new List<AggregateResult>();
            this.shadAggr   = new List<AggregateResult>();
            this.oppsList   = new List<Opportunity>();
            this.catList    = new List<Category_Combination__mdt>();
        }
    }
    /*public class ARWrapper {
        public String Account__c { get; set; }
        public String accName { get; set; }
        public String accShip { get; set; }
        public String accCity { get; set; }
        public String Product_Group__c { get; set; }
        public String sales { get; set; }
        public String CAT__c { get; set; }
        public String Reporting_Product_Group__c { get; set; }
    }*/
}