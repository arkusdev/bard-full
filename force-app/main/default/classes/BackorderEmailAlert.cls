/**
 * @description class to notify that new backorders needs to be loaded
 */
public with sharing class BackorderEmailAlert {
  /**
   * @description to store information about Backorder Email Alert Settings
   */
  private static Backorder_Email_Alert_Settings__mdt cmdtSettings = getGeneralSettings();

  /**
   * @description to store information about recipients
   */
  private static List<String> emailAddresses = getEmailAddresses();

  /**
   * @description to store information about Backorder Email Alert Settings
   */
  private static Backorder_Email_Alert__c cmSettings = getCustomSettings();

  /**
   * @description CustomException
   * */
  public class CustomException extends Exception {
  }

  /**
   * @description BackorderEmailAlert constructors
   */
  public static void backorderEmailAlertExecute() {
    try {
      boolean isTheLastRun = false;
      checkPermissions();
      AggregateResult results = [SELECT Count(Id) total FROM Backorder__c];
  
      Integer numberOfRecords = (Integer) results.get('total');

      if(numberOfRecords == cmSettings.Number_of_Records__c && cmSettings.Number_of_Records__c > 0){
        sendEmail(null);
        isTheLastRun = true;
      } else {
        isTheLastRun = scheduleInFiveMinutes();
      }

      cmSettings.Number_of_Records__c = isTheLastRun ? 0 : numberOfRecords;
      update cmSettings;

    } catch (Exception ex) {
      sendEmail(ex);
    }
  }

  private static void checkPermissions() {
    if (
      !(Schema.sObjectType.Backorder_Email_Alert_Settings__mdt.fields.Email_Template_Id__c.isAccessible() &&
      Schema.sObjectType.Backorder__c.fields.Id.isAccessible() &&
      Schema.sObjectType.Backorder_Email_Alert_Email_Address__mdt.fields.Email__c.isAccessible() &&
      Schema.sObjectType.EmailTemplate.fields.Id.isAccessible())
    ) {
      throw new CustomException(
        'Permission Expection: The user does not have sufficient permissions.'
      );
    }
  }

  /**
   * @description getGeneralSettings
   * @return an instance of Backorder_Email_Alert_Settings__mdt
   */
  private static Backorder_Email_Alert_Settings__mdt getGeneralSettings() {
    Backorder_Email_Alert_Settings__mdt bCMTD = [SELECT Limit_Hour__c, Email_Template_Id__c FROM Backorder_Email_Alert_Settings__mdt LIMIT 1];

    return bCMTD;
  }

  /**
   * @description getCustomSettings
   * @return an instance of Backorder_Email_Alert__c
   */
  private static Backorder_Email_Alert__c getCustomSettings(){
      return  Backorder_Email_Alert__c.getOrgDefaults();
  }

  /**
   * @description getEmailAddresses
   * @return a List of Strings that contains the recipients
   */
  private static List<String> getEmailAddresses(){
    List<Backorder_Email_Alert_Email_Address__mdt> emailList = [SELECT Email__c FROM Backorder_Email_Alert_Email_Address__mdt];
    List<String> eAdd = new List<String>();
    for(Backorder_Email_Alert_Email_Address__mdt email: emailList){
      eAdd.add(email.Email__c);
    }

    return eAdd;
  }

  /**
   * @description sendEmail
   * @param an instance of Exception
   */
  public static void sendEmail(Exception exc) {
    List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage>();
    String templateId = cmdtSettings.Email_Template_Id__c;

    if (exc != null) {
      emailsToSend.add(CreateEmail(null, exc));
    } else if (templateId != null && !Test.isRunningTest()) {
      emailsToSend.add(CreateEmail(templateId, null));
    } else {
      emailsToSend.add(
        CreateEmail(null, new CustomException('Email Template not found.'))
      );
    }

    if (emailsToSend.size() > 0 && emailAddresses.size() > 0) {
      Messaging.sendEmail(emailsToSend);
    }
  }
    
    /**
     * @description CreateEmail
     * @param a contact Id
     * @param an email address
     * @param the template Id
     * @return a SingleEmailMessage
     */
    public static Messaging.SingleEmailMessage createEmail(String templateId, Exception exc) {
      Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
      
      OrgWideEmailAddress[] owea = [ SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = 'SFDC Updated' ];
      if(!owea.isEmpty()){
          email.setOrgWideEmailAddressId(owea.get(0).Id);
      }
      if(exc == null) {
        email.saveAsActivity = false;
        email.setTemplateId(templateId);
        email.setTargetObjectId(UserInfo.getUserId());
      } else {
        email.setSubject('BackorderEmailAlert Exception.');
        email.setHtmlBody(generateHtmlBody(exc));
      }
      email.setToAddresses(emailAddresses);
      return email;
    }

  /**
   * @description scheduleInFiveMinutes
   */
  private static boolean scheduleInFiveMinutes(){
    Datetime inFiveMin = Datetime.now().addMinutes(5);
    String jobName = 'Scheduled Backorder Email Alert - ' + inFiveMin.format();
    String croninFiveMin =
      '0 ' +
      String.valueOf(inFiveMin.minute()) +
      ' ' +
      String.valueof(inFiveMin.hour()) +
      ' ' +
      String.valueof(inFiveMin.day()) +
      ' ' +
      String.valueof(inFiveMin.month()) +
      ' ' +
      ' ? ' +
      String.valueof(inFiveMin.year());

    Datetime endTime = Datetime.newInstance(inFiveMin.year(), inFiveMin.month(), inFiveMin.day(), (Integer) cmdtSettings.Limit_Hour__c, 15, 0);

    if (inFiveMin <= endTime) {
      System.schedule(jobName, croninFiveMin, new ScheduledBackorderEmailAlert());
      return false;
    } else {
      return true;
    }
  }
    
  /**
   * @description generateHtmlBody
   * @param an instance of Exception
   * @return the Html email body
   */
  private static String generateHtmlBody(Exception exc) {
    String body = 'An error occurred while trying to run the BackorderEmailAlert class.<br>';
    body += 'Message: ' + exc.getMessage();
    body += '. StackTraceString: ' + exc.getStackTraceString();
    body += '. TypeName: ' + exc.getTypeName();
    return body;
  }

}