global class ScheduledSalesDataEmailNotification implements Schedulable {
	global void execute(SchedulableContext sc) {
		
		Sales_Data_Email_Notification__c settings = [SELECT Last_Notification_Sent__c, Sales_Threshold__c, Limit_Hour__c  
													FROM Sales_Data_Email_Notification__c][0];

		List<Sales_Data_Email_Notification_RecordType__mdt> listSDER = [SELECT Id, RecordType_Id__c FROM Sales_Data_Email_Notification_RecordType__mdt];
		Set<String> recordTypeIds = new Set<String>();

		for(Sales_Data_Email_Notification_RecordType__mdt sder: listSDER){
			recordTypeIds.add(sder.RecordType_Id__c);
		}

		Integer salesCreatedToday = [SELECT count() FROM Sales__c WHERE CreatedDate = TODAY AND RecordTypeId IN :recordTypeIds];
		Integer salesCreatedInTheLast5Min = [SELECT count() FROM Sales__c WHERE CreatedDate > :Datetime.now().addMinutes(-5) AND RecordTypeId IN :recordTypeIds];


		//if number of sales created today passes the threshold and no notification were sent today and less than 20 records were created in the last 5, this last is to prevent sending while the dataloader it's still loading
		if (salesCreatedToday >= settings.Sales_Threshold__c && settings.Last_Notification_Sent__c != System.today() && salesCreatedInTheLast5Min < 20){
            system.debug('Conditions to send emails were met.');
			SalesDataEmailNotificationBatch b = new SalesDataEmailNotificationBatch();
			database.executebatch(b);
		} else {
			try{
				system.debug('Conditions to send emails were NOT met.');
				system.debug('Sales created today: ' + salesCreatedToday);
				system.debug('Settings threshold: ' + settings.Sales_Threshold__c);
				system.debug('Last notification: ' + settings.Last_Notification_Sent__c);
				system.debug('Sales in the last 5 minutes: ' + salesCreatedInTheLast5Min);
				system.debug('Limit hour: ' + settings.Limit_Hour__c);
				//re-schedule in 15 min if below limit hour
				Datetime in15 = Datetime.now().addMinutes(15);
				Time limitTime = Time.newInstance(Integer.valueOf(settings.Limit_Hour__c), 0, 0, 0);
				system.debug('In 15: ' + in15);
				system.debug('limitTime variable: ' + limitTime);
				if(in15.time() <= limitTime){
					String jobName = 'Scheduled Sales Data Email Notification' + ' - ' + in15.format();
					String cron = '0 ' 
								+ String.valueOf(in15.minute()) + ' ' 
								+ String.valueof(in15.hour()) + ' '
								+ String.valueof(in15.day()) + ' '
								+ String.valueof(in15.month()) + ' '
								+ ' ? '
								+ String.valueof(in15.year());
					List<CronJobDetail> cjd = [SELECT Name FROM CronJobDetail WHERE Name =:jobName];
					system.debug('Job already found: ' + (cjd.size() > 0 ? 'Yes' : 'No'));
					if(!(cjd.size() > 0)){
						System.schedule(jobName, cron, new ScheduledSalesDataEmailNotification());

					}else{
						createErrorMessage('Job already scheduled!');
					}		 
				}else {
					createErrorMessage('Time Limit Exceeded!');
				}
			}catch(Exception e){
				createErrorMessage(e.getMessage());
			}
		}
	}

	global static void  createErrorMessage(String msg){
		Email_Notification_Error__c error = new Email_Notification_Error__c();
		error.ErrorMessage__c = 'Job already scheduled!';
		insert error;
	}
}