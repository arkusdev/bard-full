global class AutoChatterGroupsBatch implements Database.Batchable<sObject> {
    // add Database.Stateful above to make data transcend batches
    String query;

    global AutoChatterGroupsBatch() {
        query = 'SELECT Name, OwnerId FROM Project__c ORDER BY Name ASC';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        // execute code
        ProjectUtils.CreateChatterGroups((List<Project__c>)scope);
    }
    
    global void finish(Database.BatchableContext BC) {
        // finish code
    }
}