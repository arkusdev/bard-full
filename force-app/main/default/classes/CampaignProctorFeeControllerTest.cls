@isTest
public class CampaignProctorFeeControllerTest {
	@testSetup
	static void createResources() {
		User u = [
			SELECT Id, Name, Phone, Email, Title, IsActive
			FROM User 
			WHERE Email = 'bd@apostletech.com'
		][0]; 

		System.runAs(u) {
			List<Account> accts = TestDataFactory.createAccounts(1);
			List<Contact> proctorContacts = TestDataFactory.createContacts(accts, 1);
			List<User> users = TestDataFactory.createUsers(1);
			List<Campaign> campaigns = TestDataFactory.createCampaigns(accts[0], 1);
			List<CampaignMember> proctorMembers = TestDataFactory.createCampaignMembers(campaigns[0], proctorContacts, 'Proctor');
			List<Campaign_Proctor_Fee__c> fees = TestDataFactory.createCampaignProctorFees(campaigns[0], proctorMembers);
		}
	}


	@isTest
	static void fetchCampaignProctorFees_UnitTest() {
		Campaign c = [
			SELECT Id
			FROM Campaign
		][0];

		List<CampaignProctorFeeDTO> fees = CampaignProctorFeeController.fetchCampaignProctorFees(c.Id);

		System.assertEquals(1, fees.size());
	}

	@isTest
	static void fetchCampaignProctorFee_UnitTest() {
		Campaign_Proctor_Fee__c fee = [
			SELECT Id, Campaign__c
			FROM Campaign_Proctor_Fee__c
			LIMIT 1
		];

		CampaignProctorFeeDTO dto = CampaignProctorFeeController.fetchCampaignProctorFee(fee.Id);

		System.assertEquals(fee.Campaign__c, dto.campaignId);
	}

	@isTest
	static void saveCampaignProctorFee_UnitTest() {
		Campaign c = [
			SELECT Id
			FROM Campaign
		][0];
		
		CampaignProctorFeeDTO dto = CampaignProctorFeeController.createNewCampaignProctorFee(c.Id);

		CampaignMember proctor = [
			SELECT ContactId, Contact.Name
			FROM CampaignMember
			WHERE CampaignId = :c.Id
			AND Type__c = 'Proctor'
			LIMIT 1
		][0];

		dto.proctorId = proctor.ContactId;
		dto.proctorName = proctor.Contact.Name;
		dto.totalCost = 1000.00;
		dto.transferOfValue = 'Consulting';
		dto.notes = 'Test notes';

		CampaignProctorFeeController.saveCampaignProctorFee(JSON.serialize(dto));

		System.assertEquals(c.Id, dto.campaignId);

		string whereClause = dto.name + ' Proctor Fee';

		Campaign_Proctor_Fee__c cpf = [
			SELECT Id, Campaign__c
			FROM Campaign_Proctor_Fee__c
			WHERE Name = :whereClause
			LIMIT 1
		];

		CampaignProctorFeeDTO fee = CampaignProctorFeeController.fetchCampaignProctorFee(cpf.Id);

		fee.totalCost = 1234.00;

		CampaignProctorFeeController.saveCampaignProctorFee(JSON.serialize(fee));
	}

	@isTest
	static void deleteCampaignProctorFee_UnitTest() {
		Campaign_Proctor_Fee__c fee = [
			SELECT Id, Campaign__c
			FROM Campaign_Proctor_Fee__c
			LIMIT 1
		];

		CampaignProctorFeeController.deleteCampaignProctorFee(fee.Id);
	}
}