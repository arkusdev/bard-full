public with sharing class IRSASalesSummaryController {
	
    private enum TIME_RANGE {THIS_MONTH, YESTERDAY, THIS_WEEK, WEEK_1, WEEK_2, WEEK_3, WEEK_4, WEEK_5, WEEK_6}
    
    ///////////////////////////////////////////////////////////////////////////////////////////////
    //                              PRODUCT GROUP SUMMARY METHODS
    //////////////////////////////////////////////////////////////////////////////////////////////

    @AuraEnabled(Cacheable=true)
    public static List<PGResponse> getPGResponse(String account, String userId){
        Map<TIME_RANGE,List<AggregateResult>> timeRangesMap= new Map<TIME_RANGE,List<AggregateResult>>();

        timeRangesMap.put(TIME_RANGE.THIS_MONTH, querySalesByAccountTimeRangeAndTerritory(TIME_RANGE.THIS_MONTH, account,  null, null, userId));
        timeRangesMap.put(TIME_RANGE.YESTERDAY, querySalesByAccountTimeRangeAndTerritory(TIME_RANGE.YESTERDAY, account,  null, null, userId));
        List<Range> ranges = getWeekRanges();
        for(Integer i = 0; i < ranges.size(); i++){
            switch on i {
               when 0 {
                   timeRangesMap.put(TIME_RANGE.WEEK_1, querySalesByAccountTimeRangeAndTerritory(TIME_RANGE.WEEK_1, account, ranges[i].startWeek,ranges[i].endWeek, userId));
               }
               when 1 {
                   timeRangesMap.put(TIME_RANGE.WEEK_2, querySalesByAccountTimeRangeAndTerritory(TIME_RANGE.WEEK_2, account, ranges[i].startWeek,ranges[i].endWeek, userId));
               }
               when 2 {
                   timeRangesMap.put(TIME_RANGE.WEEK_3, querySalesByAccountTimeRangeAndTerritory(TIME_RANGE.WEEK_3, account, ranges[i].startWeek,ranges[i].endWeek, userId));
               }
               when 3 {
                   timeRangesMap.put(TIME_RANGE.WEEK_4, querySalesByAccountTimeRangeAndTerritory(TIME_RANGE.WEEK_4, account, ranges[i].startWeek,ranges[i].endWeek, userId));
               }
               when 4 {
                   timeRangesMap.put(TIME_RANGE.WEEK_5, querySalesByAccountTimeRangeAndTerritory(TIME_RANGE.WEEK_5, account, ranges[i].startWeek,ranges[i].endWeek, userId));
               }
               when 5 {
                   timeRangesMap.put(TIME_RANGE.WEEK_6, querySalesByAccountTimeRangeAndTerritory(TIME_RANGE.WEEK_6, account, ranges[i].startWeek,ranges[i].endWeek, userId));
               }
            }
        }
        return createPGResponse(timeRangesMap);
    }

    public static Set<String> getDependantTerritories(String userId){
        List<User> users = null;
        if(!userId.equals('all')){
            users = getUserHierarchy(userId);
        }else{
            users = getUserHierarchy(null);
        }
        Set<String> territories = new Set<String>();
        for(User u : users){
            //we grab the territories here
            if(u.UserRole.Name.endsWith('- TM')){
                territories.add(u.Territory__c);
            }
        }
        return territories;
    }

    @AuraEnabled(Cacheable=true)
    public static List<User> getUserHierarchy(String userId) {
        userId = String.isBlank(userId) ? UserInfo.getUserId() : userId;
        User selectedUser = [SELECT Id, Territory__c, Name, UserRoleId, Alias, UserRole.Name, UserRole.ParentRoleId FROM User WHERE Id =:userId][0];
        Boolean isCS = selectedUser.Alias.startsWith('CS') && selectedUser.UserRole.Name.endsWith('CS');
        if (isCS){
            Id parentRoleId = [SELECT UserRole.ParentRoleId FROM User WHERE Id =: UserInfo.getUserId()].UserRole.ParentRoleId;
            List<User> userParents = [SELECT Id, Territory__c, Name, UserRoleId, Alias, UserRole.Name, UserRole.ParentRoleId FROM User WHERE UserRole.Id =: parentRoleId WITH SECURITY_ENFORCED];
            if (!userParents.isEmpty()){
                selectedUser = userParents[0];
            }
        }
        Map<String, Integer> roles = PerDaySalesUtil.getAllSubRolesFromUser(selectedUser.UserRoleId);
        List<User> users = new List<User>{selectedUser};
        users.addAll([SELECT Id, Territory__c, Name, UserRoleId, Alias, UserRole.Name FROM User WHERE UserRoleId IN :roles.keyset() AND UserRoleId != :selectedUser.UserRoleId AND isActive = true ORDER BY Territory__c]);
    
        for (User u : users) {
            u.Alias += ' - level: ' + String.valueOf(roles.get(u.UserRoleId));
        }
        return users;
    }

    @AuraEnabled(Cacheable=true)
    public static List<Range> getWeekRanges(){
        List<Range> ranges = new List<Range>();
        Date thisMonth = Date.today();
        Date firstDayOfMonth = thisMonth.toStartOfMonth();
        
        //End of the first week of the month
        Date firstFridayOfTheMonth = (firstDayOfMonth.addDays(7)).toStartOfWeek().addDays(-2);
        //Get all the days in the first week of the month, week starts on saturday and ends on friday
        // the saturday and rest of the day can be out of the month except for the friday
        Range firstRange = new Range(firstFridayOfTheMonth.addDays(-6), firstFridayOfTheMonth);
        ranges.add(firstRange);

        //Add all weeks that are full in thisMonth
        Date weekStart = firstFridayOfTheMonth.addDays(1);
        Date weekEnd = weekStart.addDays(6);
        //return only the weeks of the past and the current one
        Boolean thisWeek = false;
        while(weekStart.month() == thisMonth.month() && !thisWeek){
            Range range = new Range(weekStart, weekEnd);
            ranges.add(range);
            weekStart = weekEnd.addDays(1);
            weekEnd = weekStart.addDays(6);
            thisWeek = range.thisWeek;
        }
        return ranges;
    }

    private static List<AggregateResult> querySalesByAccountTimeRangeAndTerritory(TIME_RANGE timeRange, String accountId, Date startRange, Date endRange, String userId){
        Id irsaRecordTypeId = Schema.SObjectType.Sales__c.getRecordTypeInfosByName().get('IRSA').getRecordTypeId();
        Set<String> territories = getDependantTerritories(userId);
        String  query =  'SELECT Product_Group__c, CAT__c, SUM(Sales__c), SUM(Units__c) ';
                query += 'FROM Sales__c ';
                query += 'WHERE RecordTypeId = \''+irsaRecordTypeId+'\' AND ';
                if(timeRange == TIME_RANGE.THIS_MONTH){
                    query+='Invoice_Date__c = THIS_MONTH';
                }else if(timeRange == TIME_RANGE.YESTERDAY){
                    query+='Invoice_Date__c = YESTERDAY';
                }else if(timeRange == TIME_RANGE.WEEK_1 || timeRange == TIME_RANGE.WEEK_2 || timeRange == TIME_RANGE.WEEK_3 ||
                         timeRange == TIME_RANGE.WEEK_4 || timeRange == TIME_RANGE.WEEK_5 || timeRange == TIME_RANGE.WEEK_6){
                    query+='Invoice_Date__c >= :startRange AND Invoice_Date__c <= :endRange';
                }
                if(accountId != 'all'){
                    query+=' AND Account__c = \''+accountId+'\'';
                }

                String inClause = String.format( '(\'\'{0}\'\')', new List<String> { String.join( new List<String>(territories) , '\',\'') });
                query+=' AND Territory__c IN ' + inClause;
                
                query += ' GROUP BY Product_Group__c, CAT__c';
                query += ' ORDER BY Product_Group__c';
        return Database.query(query);
    }

    private static List<PGResponse> createPGResponse(Map<TIME_RANGE, List<AggregateResult>> timeRangesMap){
        Map<String, PGResponse> productToPGResponse = new Map<String, PGResponse>();
        
        for(TIME_RANGE timeRange: timeRangesMap.keySet()){
            for(AggregateResult result : timeRangesMap.get(timeRange)){
                String productKey = (String) result.get('Product_Group__c');
                String category = (String) result.get('CAT__c');
                PGResponse temp = productToPGResponse.get(productKey);
                if(productToPGResponse.containsKey(productKey)){
                    // the product exists in the map, check if the category exists, if not add it
                    if(String.isNotEmpty(category) && !(productToPGResponse.get(productKey).categories).contains(category)){
                        productTOPGResponse.get(productKey).categories += ' / '+category;
                    }
                }else{
                    //the product_group does not exist in the answer yet
                    productToPGResponse.put(productKey, new PGResponse(productKey ,category));
                }
                //add the result values to the correct product/category/timeRange
                addValuesToPGResponse(productToPGResponse.get(productKey), timeRange, result);
            }
        }
        return productToPGResponse.values();
    }

    private static void addValuesToPGResponse(PGResponse pgr, TIME_RANGE timeRange, AggregateResult result){
        
        switch on timeRange {
            when THIS_MONTH {
                pgr.month_sales += (Decimal) result.get('expr0');
                pgr.month_units += Integer.valueOf(result.get('expr1'));
            }
            when YESTERDAY {
                pgr.yesterday_sales += (Decimal) result.get('expr0');
                pgr.yesterday_units += Integer.valueOf(result.get('expr1'));
            }
            when WEEK_1 {
                pgr.weekdate1_sales += (Decimal) result.get('expr0');
                pgr.weekdate1_units += Integer.valueOf(result.get('expr1'));
            }
            when WEEK_2 {
                pgr.weekdate2_sales += (Decimal) result.get('expr0');
                pgr.weekdate2_units += Integer.valueOf(result.get('expr1'));
            }
            when WEEK_3 {
                pgr.weekdate3_sales += (Decimal) result.get('expr0');
                pgr.weekdate3_units += Integer.valueOf(result.get('expr1'));
            }
            when WEEK_4 {
                pgr.weekdate4_sales += (Decimal) result.get('expr0');
                pgr.weekdate4_units += Integer.valueOf(result.get('expr1'));
            }
            when WEEK_5 {
                pgr.weekdate5_sales += (Decimal) result.get('expr0');
                pgr.weekdate5_units += Integer.valueOf(result.get('expr1'));
            }
            when WEEK_6 {
                pgr.weekdate6_sales += (Decimal) result.get('expr0');
                pgr.weekdate6_units += Integer.valueOf(result.get('expr1'));
            }
        }
        
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //                              ACCOUNT SUMMARY METHODS
    //////////////////////////////////////////////////////////////////////////////////////////////

    @AuraEnabled(Cacheable=true)
    public static List<AGResponse> getAGResponse(String account, String timeRange, String userId, String product){
        List<Sales__c> sales = new List<Sales__c>();
        if(timeRange == 'this_month'){
            sales = queryAccountSalesByTimeRangeAndProduct(TIME_RANGE.THIS_MONTH, account, userId, product, null, null);
        } else if(timeRange == 'yesterday'){
            sales = queryAccountSalesByTimeRangeAndProduct(TIME_RANGE.YESTERDAY, account, userId, product, null, null);
        } else if(timeRange == 'this_week'){
            List<Range> ranges = getWeekRanges();
            Range thisWeek = getThisWeekRange(ranges);
            sales = queryAccountSalesByTimeRangeAndProduct(TIME_RANGE.THIS_WEEK, account, userId, product, thisWeek.startWeek, thisWeek.endWeek);
        }
        return createAGResponse(sales);
    }

    private static Range getThisWeekRange(List<Range> ranges){
        for(Range ran: ranges){
            if(ran.thisWeek){
                return ran;
            }
        }
        return null;
    }

    private static List<Sales__c> queryAccountSalesByTimeRangeAndProduct(TIME_RANGE timeRange, String accountId, String userId, String product, Date startRange, Date endRange){
        Id irsaRecordTypeId = Schema.SObjectType.Sales__c.getRecordTypeInfosByName().get('IRSA').getRecordTypeId();

        Set<String> territories = getDependantTerritories(userId);

        String  query =  'SELECT Id, Account__r.Name, Account__r.ShippingCity, Account__r.ShippingState, Account__r.Ship_to_ID__c, ';
                query += 'Order_Number__c, Invoice_Number__c, Invoice_Date__c, Item_Number__c, Item_Description__c, Product_Group__c, Ship_to_ID__c, Units__c, Sales__c ';
                query += 'FROM Sales__c ';
                query += 'WHERE RecordTypeId = \''+irsaRecordTypeId+'\' AND ';
                if(timeRange == TIME_RANGE.THIS_MONTH){
                    query+='Invoice_Date__c = THIS_MONTH';
                }else if(timeRange == TIME_RANGE.YESTERDAY){
                    query+='Invoice_Date__c = YESTERDAY';
                }else if(timeRange == TIME_RANGE.THIS_WEEK){
                    query+='Invoice_Date__c >= :startRange AND Invoice_Date__c <= :endRange';
                }
                if(accountId != 'all'){
                    query+=' AND Account__c = \''+accountId+'\'';
                }
                if(product != 'all'){
                    query+=' AND Product_Group__c = \''+product+'\'';
                }

                String inClause = String.format( '(\'\'{0}\'\')', new List<String> { String.join( new List<String>(territories) , '\',\'') });
                query+=' AND Territory__c IN ' + inClause;
                
                query += ' ORDER BY Account__r.Name, Invoice_Date__c, Product_Group__c';
        return Database.query(query);
    }

    private static List<AGResponse> createAGResponse(List<Sales__c> sales){
        List<AGResponse> AGResponseList = new List<AGResponse>();
        Map<String, List<Sales__c>> accountNameToSales = new Map<String, List<Sales__c>>();
        
        for(Sales__c sale: sales){
            List<Sales__c> temp = accountNameToSales.get(sale.Account__r.Name);
            if(temp == null){
                accountNameToSales.put(sale.Account__r.Name, new List<Sales__c>{sale});
            }else{
                temp.add(sale);
            }
        }
        for(String key: accountNameToSales.keySet()){
            Decimal accountSales = 0;
            for(Sales__c sale: accountNameToSales.get(key)){
                accountSales += sale.sales__c;
            }
            Sales__c firstSale = accountNameToSales.get(key)[0];
            AGResponse agr = new AGResponse(firstSale.Account__r.Name, 
                                            firstSale.Account__r.ShippingCity, 
                                            firstSale.Account__r.ShippingState,
                                            firstSale.Account__r.Ship_to_ID__c,
                                            accountSales, 
                                            accountNameToSales.get(key));
            AGResponseList.add(agr);
        }
        return AGResponseList;
    }

    @AuraEnabled(Cacheable=true)
    public static List<Territory__c> getTerritories(){
        Id irsaRecordTypeId = Schema.SObjectType.Sales__c.getRecordTypeInfosByName().get('IRSA').getRecordTypeId();
        List<Sales__c> mysales = [SELECT Territory__c, Owner.Name 
                                    FROM Sales__c 
                                    WHERE Invoice_Date__c = THIS_MONTH AND RecordTypeId = :irsaRecordTypeId AND OwnerId =:UserInfo.getUserId()];
        Set<String> territoryNumbers = new Set<String>();
        Map<String, String> territoryNames = new Map<String, String>();
        for(Sales__c sale:mysales){
            territoryNumbers.add(sale.Territory__c);
            territoryNames.put(sale.Territory__c, sale.Territory__c+' - '+sale.Owner.Name);
        }
        List<Territory__c> territories = [SELECT Id, Name, Number__c FROM Territory__c WHERE Number__c IN :territoryNumbers ORDER BY Name];
        //change name to display in cmp
        for(Territory__c terr: territories){
            terr.Name = territoryNames.get(terr.Number__c);
        }
        return territories;
    }

    @AuraEnabled
    public static List<User> getUsers () {
        List<User> users = getUserHierarchy(null);
        List<User> result = new List<User>();

        Boolean isRM = users[0].UserRole.Name.endsWith('- RM');
        Boolean isDM = users[0].UserRole.Name.endsWith('- DM');
        Boolean isTM = users[0].UserRole.Name.endsWith('- TM');
        if (isTM) {
            result.add(users[0]);
        } else if (isDM) {
            for (User u : users) {
                if (u.UserRole.Name.endsWith('- TM')) result.add(u);
            }
        } else if (isRM) {
            for (User u : users) {
                if (u.UserRole.Name.endsWith('- DM')) result.add(u);
            }
        }
        return result;
    }

    @AuraEnabled(Cacheable=true)
    public static List<Account> getAccounts(){
        Id irsaRecordTypeId = Schema.SObjectType.Sales__c.getRecordTypeInfosByName().get('IRSA').getRecordTypeId();
        Set<String> territories = getDependantTerritories(UserInfo.getUserId());

        List<Sales__c> mysales = [SELECT Account__c
                                    FROM Sales__c 
                                    WHERE Invoice_Date__c = THIS_MONTH AND RecordTypeId = :irsaRecordTypeId AND Territory__c IN :territories];
        Set<String> accountIds = new Set<String>();
        for(Sales__c sale:mysales){
            accountIds.add(sale.Account__c);
        }
        List<Account> accounts = [SELECT Id, Name, Ship_to_ID__c FROM Account WHERE Id IN :accountIds ORDER BY Name];

        for(Account acc: accounts){
            acc.Name = acc.Name+' - ('+acc.Ship_to_ID__c+')';
        }
        return accounts;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    //                              INNER CLASSES
    //////////////////////////////////////////////////////////////////////////////////////////////

    public class Range {
        @AuraEnabled
        public Date startWeek;
        @AuraEnabled
        public Date endWeek;
        @AuraEnabled
        public Boolean thisWeek;

        public Range(Date s, Date e) {
            startWeek = s;
            endWeek = e;
            thisWeek = s <= Date.today() && Date.today() <= e;
        }
    }

    public class PGResponse {
        @AuraEnabled
        public String                              product                  { get; set; }
        @AuraEnabled
        public String                              categories               { get; set; }
        @AuraEnabled
        public Integer                             month_units              { get; set; }
        @AuraEnabled
        public Decimal                             month_sales              { get; set; }
        @AuraEnabled
        public Integer                             yesterday_units          { get; set; }
        @AuraEnabled
        public Decimal                             yesterday_sales          { get; set; }
        @AuraEnabled
        public Integer                             weekdate1_units          { get; set; }
        @AuraEnabled
        public Decimal                             weekdate1_sales          { get; set; }
        @AuraEnabled
        public Integer                             weekdate2_units          { get; set; }
        @AuraEnabled
        public Decimal                             weekdate2_sales          { get; set; }
        @AuraEnabled
        public Integer                             weekdate3_units          { get; set; }
        @AuraEnabled
        public Decimal                             weekdate3_sales          { get; set; }
        @AuraEnabled
        public Integer                             weekdate4_units          { get; set; }
        @AuraEnabled
        public Decimal                             weekdate4_sales          { get; set; }
        @AuraEnabled
        public Integer                             weekdate5_units          { get; set; }
        @AuraEnabled
        public Decimal                             weekdate5_sales          { get; set; }
         @AuraEnabled
        public Integer                             weekdate6_units          { get; set; }
        @AuraEnabled
        public Decimal                             weekdate6_sales          { get; set; }

        public PGResponse(String productKey, String firstCategory) {
            product = productKey;
            categories = firstCategory;
            month_units = 0;
            month_sales = 0;
            yesterday_units = 0;
            yesterday_sales = 0;
            weekdate1_units = 0;
            weekdate1_sales = 0;
            weekdate2_units = 0;
            weekdate2_sales = 0;
            weekdate3_units = 0;
            weekdate3_sales = 0;
            weekdate4_units = 0;
            weekdate4_sales = 0;
            weekdate5_units = 0;
            weekdate5_sales = 0;
            weekdate6_units = 0;
            weekdate6_sales = 0;
        }
    }

    public class AGResponse {
        @AuraEnabled
        public String                      accountName     { get; set; }
        @AuraEnabled
        public String                      accountCity     { get; set; }
        @AuraEnabled
        public String                      accountState    { get; set; }
        @AuraEnabled
        public String                      shipTo          { get; set; }
        @AuraEnabled
        public Decimal                     accountSales    { get; set; }
        @AuraEnabled
        public List<Sales__c>              sales           { get; set; }

        public AGResponse(String accName, String accCity, String accState, String sTo, Decimal accSales, List<Sales__c> saleList) {
            accountName = accName;
            accountCity = accCity;
            accountState = accState;
            shipTo = sTo;
            accountSales = accSales;
            sales = saleList;
        }
    }
}