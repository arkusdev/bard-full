@isTest
public class CustomerAndProductGapControllerTest {
    @testSetup
    public static void setup () {
        Test.startTest();
        CustomerAndProductGapTestUtil.createData();
        Test.stopTest();
    }
    
    @isTest
    public static void getCalcsYearCustomer () {
        User u = CustomerAndProductGapTestUtil.getDMUser();
        Test.startTest();
        CustomerAndProductGapController.CalculationsResponse response = CustomerAndProductGapController.getCalculations(u.Id, 'year','customer', null, true, true);
        Test.stopTest();

        System.assertNotEquals(NULL, response);
        System.assertEquals(1, response.irsaAggr.size());
        System.assertEquals(1, response.shadAggr.size());
    }
    
    @isTest
    public static void getCalcsYearProduct () {
        User u = CustomerAndProductGapTestUtil.getDMUser();
        Test.startTest();
        CustomerAndProductGapController.CalculationsResponse response = CustomerAndProductGapController.getCalculations(u.Id, 'year','product', null, true, true);
        Test.stopTest();
        System.assertNotEquals(NULL, response);
        System.assertEquals(1, response.irsaAggr.size());
        System.assertEquals(1, response.shadAggr.size());
    }
    
	@isTest
    public static void getCalcsQuarterCustomer () {
        // Test support for quarters
        Test.startTest();
        CustomerAndProductGapController.CalculationsResponse response = CustomerAndProductGapController.getCalculations(UserInfo.getUserId(), 'quarter','customer', null, true, true);
        Test.stopTest();
        System.assertNotEquals(NULL, response);
    }

    @isTest
    public static void getCalcsMonthProduct () {
        // Test support for months
        Test.startTest();
        CustomerAndProductGapController.CalculationsResponse response = CustomerAndProductGapController.getCalculations(UserInfo.getUserId(), 'month','product', null, true, true);
        Test.stopTest();
        System.assertNotEquals(NULL, response);
    }

    @isTest
    public static void getCalcsException () {
        String exType = '';

        try {
            CustomerAndProductGapController.getCalculations(UserInfo.getUserId(), 'invalid_period','invalid_view', null, true, true);
        } catch (Exception ex) {
            exType = ex.getTypeName();
        }
        
        System.assertEquals('System.AuraHandledException', exType);
    }

    @isTest
    public static void noAccessToFieldException () {
        String exType = '';
        String exMessage = '';
        Profile p = [SELECT Id FROM Profile WHERE Name='Chatter Free User']; 
        User bcsm = new User(Alias='brdst',
                          Email='brdst@testorg.com', 
                          EmailEncodingKey='UTF-8',
                          LastName='Testing',
                          LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US',
                          ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName='brdst@testorg.com');
        insert bcsm;
        Test.startTest();
        bcsm.FirstName = 'noAccessToFieldException';
        update bcsm;
        System.runAs(bcsm) {
            try {
                CustomerAndProductGapController.getCalculations(bcsm.Id, 'month','customer', null, true, true);
            } catch (AuraHandledException ex) {
                exType = ex.getTypeName();
            }

            System.assertEquals('System.AuraHandledException', exType);
        }
        Test.stopTest();
    }
}