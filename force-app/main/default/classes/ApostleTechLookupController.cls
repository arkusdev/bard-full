public class ApostleTechLookupController {
	@auraEnabled
    public static List<sObject> fetchLookUpValues(string searchKeyword, string objName) {
        String searchKey = searchKeyword + '%';

        String sosl = 'FIND {'+ searchKeyword + '} IN NAME FIELDS RETURNING ' + objName + ' (Name ';
        sosl += 'WHERE Name LIKE \'' + searchKey + '\' )';
        //sosl += 'ORDER BY CreatedDate DESC LIMIT 5)';
        
        List<List<SObject>> soslResult = search.query(sosl);
      	return soslResult.get(0);
        /*
        List<sObject> objList = new List <sObject>();
        string soql =  'SELECT Id, Name ' +
					   'FROM ' + objName + ' ' +
					   'WHERE Name LIKE :searchKey ' +
					   'ORDER BY CreatedDate ' +
					   'DESC LIMIT 5';

        List<sObject> objs = Database.query(soql);
        
        for(sObject obj :objs) {
            objList.add(obj);
        }

        return objList;*/
    }

	@auraEnabled
    public static List<sObject> fetchLookUpValues(string searchKeyword, string objName, List<String> omitIdList) {
        String searchKey = searchKeyword + '%';
        String omitString = '(\'' + String.join(omitIdList, '\',\'') + '\') ';

        String sosl = 'FIND {'+ searchKeyword + '} IN NAME FIELDS RETURNING ' + objName + ' (Name ';
        if (objName.equals('Contact')) sosl += ', FirstName, LastName, Title, AccountId, Email, Phone, OwnerId ';
        sosl += 'WHERE Name LIKE \'' + searchKey + '\' ';
        sosl += 'AND Id NOT IN ' + omitString + ')';
        //sosl += 'ORDER BY CreatedDate DESC LIMIT 5)';
        
        List<List<SObject>> soslResult = search.query(sosl);
        
        if (objName.equals('Contact')) {
            List<String> accIds = new List<String>();
            List<String> uIds = new List<String>();
            for (Contact contact : (List<Contact>)soslResult.get(0)) {
                uIds.add(contact.OwnerId);
                accIds.add(contact.AccountId);
            }
            Map<Id, User> users = new Map<Id, User>([SELECT Name FROM User WHERE Id IN :uIds]);
            Map<Id, Account> accounts = new Map<Id, Account>([SELECT Name FROM Account WHERE Id IN :accIds]);

            for (Contact contact : (List<Contact>)soslResult.get(0)) {
                contact.Owner = users.get(contact.OwnerId);
                contact.Account = accounts.get(contact.AccountId);
            }
        }
      	return soslResult.get(0);
        /*List<sObject> objList = new List <sObject>();
      
        string soql =  'SELECT Id, Name ' +
					   'FROM ' + objName + ' ' +
					   'WHERE Name LIKE :searchKey ' +
					   'AND Id NOT IN :omitIdList ' +
					   'ORDER BY CreatedDate ' +
					   'DESC LIMIT 5';

        List<sObject> objs = Database.query(soql);
        
        for(sObject obj :objs) {
            objList.add(obj);
        }

        return objList;*/
    }
}