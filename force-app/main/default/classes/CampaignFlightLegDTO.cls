public class CampaignFlightLegDTO {
	@auraEnabled 
	public Id flightLegId { get; set; } 		
	@auraEnabled 
	public string name { get; set; }	
	@auraEnabled 
	public string origin { get; set; }
	@auraEnabled 
	public string destination { get; set; }
	@auraEnabled 
	public datetime departDateTime { get; set; }
	@auraEnabled 
	public datetime arriveDateTime { get; set; }
	@auraEnabled 
	public string airline { get; set; }
	@auraEnabled 
	public string confirmationNumber { get; set; }
	@auraEnabled 
	public string flightNumber { get; set; }
	@auraEnabled 
	public string gate { get; set; }
	@auraEnabled 
	public string notes { get; set; }
	@auraEnabled 
	public string terminal { get; set; }
	@auraEnabled 
	public string type { get; set; }

	public CampaignFlightLegDTO(string type) {
		this.type = type;
	}

	public CampaignFlightLegDTO(Campaign_Flight_Leg__c fl) {
		this.flightLegId = fl.Id;
		this.name = fl.Name;
		this.origin = fl.Origin__c;
		this.destination = fl.Destination__c;
		this.departDateTime = fl.Depart_Date_Time__c;
		this.arriveDateTime = fl.Arrive_Date_Time__c;
		this.airline = fl.Airline__c;
		this.confirmationNumber = fl.Confirmation_Number__c;
		this.flightNumber = fl.Flight_Number__c;
		this.gate = fl.Gate__c;
		this.terminal = fl.Terminal__c;
		this.type = fl.Type__c;
		this.notes = fl.Notes__c;
	}
}