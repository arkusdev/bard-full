/**
* @author      Federico Seco <federico.seco@arkusinc.com>
* @version     1.0
*/
public with sharing class PerDaySalesController {
    private static Integer currentWorkingDays;
    private static Integer totalWorkingDays;
    private static Integer thisMonthCurrentDays;
    private static Integer thisMonthTotalDays;
    /**
  * Entry method that should be the only one exposed to the interface.
  * It's intended to fill a table with sales data.
  * Each row will represent a category and each column a calculation.
  *
  * Results will depend on the user selected and the period.
  * The user id will generate a list of users all the way down the
  * role hierarchy tree.
  * The period option could be either 'month', 'quarter', or 'year'
  *
  * The method will throw an exception if any object or field is not
  * accessible by the current user.
  */
    @AuraEnabled
    public static CalculationsResponse getCalculations(String userId, String period, Boolean distributor) {
        CalculationsResponse response = new CalculationsResponse();        
        String[] accessDenied = PerDaySalesUtil.checkAccessibility();

        if (accessDenied.size() == 0) {
            try {
                response.headerInfo = getPeriodInfo(period);
                response.users = PerDaySalesUtil.getUsers(userId);
                fillTableData(response, period, distributor);
            } catch (Exception ex) {
                throw new AuraHandledException('Line ' + ex.getLineNumber() + ': ' + ex.getMessage());
            }
        } else {
            String failedFields = String.join(accessDenied, ', ');
            String message = 'Some objects or fields are not accessible by the current user: ' + failedFields;
            AuraHandledException ex = new AuraHandledException(message);
            ex.setMessage(message);
            throw ex;
        }

        return response;
    }

    /**
  * receives a response object and a String representing a period (month, quarter or year)
  *
  * Completes the passed object (by reference). Doesn't return a value.
  */
    private static void fillTableData (CalculationsResponse response, String period, Boolean distributor) {
        if (response.headerInfo.currentWorkingDays == 0 && period.equals('month')) return;

        List<String> userIds = PerDaySalesUtil.getUserIdsFromUsers(response.users);
        currentWorkingDays = response.headerInfo.currentWorkingDays;
        totalWorkingDays = response.headerInfo.totalWorkingDays;
        thisMonthCurrentDays = response.headerInfo.thisMonthCurrentDays;
        thisMonthTotalDays = response.headerInfo.thisMonthTotalDays;
        Calculation totals = new Calculation();
        totals.catName = 'Total';


        PerDaySalesUtil.QuotasData quotas = PerDaySalesUtil.getQuotas(userIds, period, distributor);
        Map<String, Calculation> result = getQuotaMapFromList(quotas.currentPeriodSales);
        Map<String, Calculation> resultForQuotaAndBase = getQuotaMapFromPriorPeriod(result, quotas);
        List<Sales__c> sales = PerDaySalesUtil.getSalesByCategory(userIds, period, result.keyset(), distributor, currentWorkingDays, totalWorkingDays);
        Map<String, Sales__c> irsaMap = getSalesMapFromList(sales);

        Integer allWorkingDays = getYearWorkingDays();

        for (Calculation quota : result.values()) {
            Sales__c irsa = irsaMap.get(quota.catName);
            if (irsa == NULL) {
                irsa = new Sales__c(Sales__c=0);
            }

            Decimal calculatedQuotaPrior = quota.quotaPrior + quota.basePrior;
            Decimal calculatedQuotaThisMonth = quota.quotaThisMonth + quota.baseThisMonth;
            

            quota.mtdBase = getCalculationForBase(resultForQuotaAndBase.get(quota.catName), period, allWorkingDays);
            quota.mtdQuota = getCalculationForQuota(resultForQuotaAndBase.get(quota.catName), quota.mtdBase, period);
            
            quota.mtdSales = irsa.Sales__c.round(System.RoundingMode.FLOOR);
            //quota.mtdBase = (quota.basePrior + (quota.baseThisMonth / thisMonthTotalDays * thisMonthCurrentDays)).round(System.RoundingMode.FLOOR);//((quota.mtdBase / totalWorkingDays) * currentWorkingDays).round();
            quota.mtdVsBase = (irsa.Sales__c - quota.mtdBase) < 0 ? (irsa.Sales__c - quota.mtdBase).round(System.RoundingMode.HALF_UP) : (irsa.Sales__c - quota.mtdBase).round(System.RoundingMode.FLOOR);
            //quota.mtdQuota = (calculatedQuotaPrior + (calculatedQuotaThisMonth / thisMonthTotalDays * thisMonthCurrentDays)).round(System.RoundingMode.FLOOR);//((quota.mtdVsQuota / totalWorkingDays) * currentWorkingDays).round();
            quota.mtdVsQuota = (quota.mtdSales - quota.mtdQuota) < 0 ? (quota.mtdSales - quota.mtdQuota).round(System.RoundingMode.HALF_UP) : (quota.mtdSales - quota.mtdQuota).round(System.RoundingMode.FLOOR);
            quota.dailyAvg = currentWorkingDays > 0 ? (irsa.Sales__c / currentWorkingDays).round(System.RoundingMode.FLOOR) : 0;
            quota.projection = currentWorkingDays > 0 ? (irsa.Sales__c / currentWorkingDays * totalWorkingDays).round(System.RoundingMode.FLOOR) : 0;

            quota.mtdFullQuota = calculatedQuotaPrior + calculatedQuotaThisMonth;
            quota.periodVsQuota = quota.projection - quota.mtdFullQuota;
            
            // sum totals
            totals.dailyAvg += quota.dailyAvg;
            totals.projection += quota.projection;
            totals.mtdVsBase += quota.mtdVsBase;
            totals.mtdVsQuota += quota.mtdVsQuota;
            totals.mtdSales += quota.mtdSales;
            totals.mtdBase += quota.mtdBase;
            totals.mtdQuota += quota.mtdQuota;

            totals.mtdFullQuota += quota.mtdFullQuota;
            totals.periodVsQuota += quota.periodVsQuota;
        }

        result.put('Total', totals);

        response.tableData = result.values();
    }
    
    private static Map<String, Sales__c> getSalesMapFromList(List<Sales__c> sales) {
        Map<String, Sales__c> result = new Map<String, Sales__c>();
        for (Sales__c s : sales) {
            result.put(s.CAT__c, s);
        }
        return result;
    }

    private static Map<String, Calculation> getQuotaMapFromList(List<Sales__c> quotas) {
        Map<String, Calculation> result = new Map<String, Calculation>();
        Map<String, List<Calculation>> quotaMap = new Map<String, List<Calculation>>();

        for (Sales__c q : quotas) {
            Calculation c = new Calculation();
            c.mtdBase = q.Base__c;
            c.mtdQuota = q.Quota__c;
            c.invoiceDate = q.Invoice_Date__c;
            c.catName = q.CAT__c;

            if (quotaMap.containsKey(q.CAT__c))
                quotaMap.get(q.CAT__c).add(c);
            else
                quotaMap.put(q.CAT__c, new List<Calculation>{c});
        }

        for (List<Calculation> quotaList : quotaMap.values()) {
            Calculation calc = new Calculation();
            for (Calculation innerC : quotaList) {
                calc.catName = innerC.catName;
                calc.mtdBase += innerC.mtdBase;
                if (innerC.invoiceDate.toStartOfMonth() < Date.today().toStartOfMonth()) {
                    calc.quotaPrior += innerC.mtdQuota;
                    calc.basePrior += innerC.mtdBase;
                } else if (innerC.invoiceDate.toStartOfMonth() == Date.today().toStartOfMonth()) {
                    calc.quotaThisMonth += innerC.mtdQuota;
                    calc.baseThisMonth += innerC.mtdBase;
                }
            }
            result.put(calc.catName, calc);
        }

        return result;
    }

    private static Map<String, Calculation> getQuotaMapFromPriorPeriod(Map<String, Calculation> result, PerDaySalesUtil.QuotasData quotas) {
        Map<String, List<Calculation>> priorQuotaMap = new Map<String, List<Calculation>>();

        for (Sales__c q : quotas.priorPeriodSales) {
            Calculation c = new Calculation();
            c.catName = q.CAT__c;
            c.mtdBase = q.Base__c;

            if (priorQuotaMap.containsKey(q.CAT__c)){
                priorQuotaMap.get(q.CAT__c).add(c);
            }else{
                priorQuotaMap.put(q.CAT__c, new List<Calculation>{c});
            }
        }

        for (Calculation quota : result.values()) {
            Calculation calc = new Calculation();
            calc.catName = quota.catName;
            calc.mtdBase = quota.mtdBase;
            calc.mtdQuota = quota.mtdQuota;
            calc.baseThisMonth = quota.baseThisMonth;
            calc.quotaThisMonth = quota.quotaThisMonth;
            calc.invoiceDate = quota.invoiceDate;
            if (priorQuotaMap.containsKey(calc.catName)){
                for (Calculation innerC : priorQuotaMap.get(calc.catName)) {
                    calc.basePrior += innerC.mtdBase;
                }
            }
            result.put(calc.catName, calc);
        }

        return result;
    }

  /**
  * Gets all the information about time frame and calculates how to display the text.
  *
  * returns a PeriodInfo object
  */
    private static PeriodInfo getPeriodInfo (String period) {
        PeriodInfo info = new PeriodInfo();
        PerDaySalesUtil.fillWorkingDays(period, info);
        info.currentDate = (Date.today()).format();

        Integer thisYear = (Date.today()).year();
        if (period.equals('month')) {
            info.currentMonthYear = ((Datetime)Date.today()).formatGmt('MMMM yyyy');
        } else if (period.equals('quarter')) {
            Integer quarters = Integer.valueOf([SELECT count(Id) quarter FROM Period WHERE Type= 'quarter' AND StartDate = THIS_FISCAL_YEAR AND EndDate = THIS_FISCAL_YEAR and StartDate < TODAY][0].get('quarter'));
            //Integer thisMonth = (Date.today()).month();
            String quarter = '';

            // Independent conditions due to uncertainty of time during tests (coverage)
            /*if (thisMonth < 4) quarter = '1st';
            if (thisMonth > 3 && thisMonth < 7) quarter = '2nd';
            if (thisMonth > 6 && thisMonth < 10) quarter = '3rd';
            if (thisMonth > 9) quarter = '4th';*/
            if (quarters == 1) quarter = '1st';
            else if (quarters == 2) quarter = '2nd';
            else if (quarters == 3) quarter = '3rd';
            else if (quarters == 4) quarter = '4th';
            
            info.currentMonthYear = quarter + ' quarter ' + thisYear;
        } else if (period.equals('year')) {
            info.currentMonthYear = '' + thisYear;
        }
        return info;
    }

    private static Decimal getCalculationForBase(Calculation quota, String period, Integer allWorkingDays) {
        Decimal annualizedBase = quota.basePrior / Date.today().month() * 12;
        Decimal annualizedDailyBase = annualizedBase / allWorkingDays;
        // Decimal annualizedDailyBase = annualizedBase / 251;
        Decimal mtdBase;
        switch on period {
            when 'month' {
                mtdBase = (thisMonthCurrentDays * annualizedDailyBase).round(System.RoundingMode.FLOOR);
            }
            when 'quarter' {
                mtdBase = (quota.basePrior + ((quota.baseThisMonth/thisMonthTotalDays) * thisMonthCurrentDays)).round(System.RoundingMode.FLOOR);
            }
            when 'year' {
                mtdBase = quota.basePrior + ((quota.baseThisMonth / thisMonthTotalDays) * thisMonthCurrentDays).round(System.RoundingMode.FLOOR);
            }
        }
        return mtdBase;
    }

    private static Decimal getCalculationForQuota(Calculation quota, Decimal mtdBase, String period) {
        Decimal mtdQuota;
        switch on period {
            when 'month' {
                mtdQuota = mtdBase + ((quota.quotaThisMonth/totalWorkingDays) * thisMonthCurrentDays).round(System.RoundingMode.FLOOR);
            }
            when 'quarter' {
                mtdQuota = mtdBase + (quota.quotaPrior + ((quota.quotaThisMonth/thisMonthTotalDays) * thisMonthCurrentDays)).round(System.RoundingMode.FLOOR);
            }
            when 'year' {
                mtdQuota = mtdBase + (quota.quotaPrior + (quota.quotaThisMonth / thisMonthTotalDays) * thisMonthCurrentDays).round(System.RoundingMode.FLOOR);
            }
        }
        return mtdQuota;
    }

    /**
     * @description Next method will return all working days in an year
     * Will exlude Saturday/Sunday and holidays defined on setup
     * @return Integer, number of working days
     */
    private static Integer getYearWorkingDays(){
        Date todaysDate = Date.today();
        Date startDate = Date.newInstance(todaysDate.year() - 1, todaysDate.month() + 1, 1);
        Date endDate = Date.newInstance(todaysDate.year(), todaysDate.month(), Date.daysInMonth(todaysDate.year(), todaysDate.month()));

        Set<String> freeDaysSet = new Set<String>();

        for(Holiday freeDay : [SELECT ActivityDate FROM Holiday 
                                WHERE (ActivityDate >= :startDate AND ActivityDate <= :endDate) OR IsRecurrence = true]){
            freeDaysSet.add(String.valueOf(freeDay.ActivityDate.month()) + String.valueOf(freeDay.ActivityDate.day()));
        }
        Integer workingDays = 0;
        for(integer i=0; i <= startDate.daysBetween(endDate); i++){
            if(((DateTime)startDate.addDays(i)).format('EEEE') != 'Saturday' && ((DateTime)startDate.addDays(i)).format('EEEE') !='Sunday'){
                workingDays = workingDays + 1;
            }
        }
        workingDays -= freeDaysSet.size();
        
        return workingDays;
    }

    public class PeriodInfo {
        @AuraEnabled
        public string currentMonthYear { get; set; }
        @AuraEnabled
        public string currentDate { get; set; }
        @AuraEnabled
        public Integer totalWorkingDays { get; set; }
        @AuraEnabled
        public Integer currentWorkingDays { get; set; }
        @AuraEnabled
        public Integer thisMonthTotalDays { get; set; }
        @AuraEnabled
        public Integer thisMonthCurrentDays { get; set; }
    }

    public class CalculationsResponse {
        @AuraEnabled
        public PeriodInfo headerInfo { get; set; }
        @AuraEnabled
        public List<User> users { get; set; }
        @AuraEnabled
        public List<Calculation> tableData { get; set; }

        public CalculationsResponse () {
            this.tableData = new List<Calculation>();
        }
    }

    public class Calculation {
        @AuraEnabled
        public String catName           { get; set; }
        @AuraEnabled
        public Decimal dailyAvg         { get; set; }
        @AuraEnabled
        public Decimal projection       { get; set; }
        @AuraEnabled
        public Decimal mtdSales         { get; set; }
        @AuraEnabled
        public Decimal mtdBase          { get; set; }
        @AuraEnabled
        public Decimal mtdQuota         { get; set; }
        @AuraEnabled
        public Decimal mtdVsBase        { get; set; }
        @AuraEnabled
        public Decimal mtdVsQuota       { get; set; }
        @AuraEnabled
        public Decimal mtdFullQuota     { get; set; }
        @AuraEnabled
        public Date invoiceDate         { get; set; }
        @AuraEnabled
        public Decimal quotaPrior       { get; set; }
        @AuraEnabled
        public Decimal quotaThisMonth   { get; set; }
        @AuraEnabled
        public Decimal basePrior        { get; set; }
        @AuraEnabled
        public Decimal baseThisMonth    { get; set; }
        @AuraEnabled
        public Decimal periodVsQuota    { get; set; }
        
        public Calculation () {
            this.dailyAvg       = 0;
            this.projection     = 0;
            this.mtdSales       = 0;
            this.mtdVsBase      = 0;
            this.mtdVsQuota     = 0;
            this.mtdBase        = 0;
            this.mtdQuota       = 0;
            this.mtdFullQuota   = 0;
            this.quotaPrior     = 0;
            this.quotaThisMonth = 0;
            this.basePrior      = 0;
            this.baseThisMonth  = 0;
            this.periodVsQuota  = 0;
        }
    }
}