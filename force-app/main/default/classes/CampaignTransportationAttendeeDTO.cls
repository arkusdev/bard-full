public class CampaignTransportationAttendeeDTO {
	@auraEnabled 
	public Id value { get; set; }
	@auraEnabled 
	public string label { get; set; }

	public CampaignTransportationAttendeeDTO(Id attendeeId, string attendeeName) {
		this.value = attendeeId;
		this.label = attendeeName;
	}

	public CampaignTransportationAttendeeDTO(CampaignMember cm) {
		this.value = cm.ContactId;
		this.label = cm.Contact.name;
	}

	public CampaignTransportationAttendeeDTO(Campaign_User__c cu) {
		this.value = cu.User__c;
		this.label = cu.User__r.name;
	}
}