public class CampaignIncidentalController {
	@auraEnabled
	public static List<CampaignIncidentalDTO> fetchCampaignIncidentals(id campaignId) {
		List<CampaignIncidentalDTO> dtos = new List<CampaignIncidentalDTO>();

		if (UserUtil.canAccessIncidentalExpenses()) { 
			List<Campaign_Incidental__c> incidentals = [
				SELECT Id, Campaign__c, Name, Description__c, Purchase_Date_Time__c, Attendee_Count__c, Cost_Per_Attendee__c, Total_Cost__c, Transfer_of_Value__c, Notes__c, 
					(SELECT Contact__c, Contact__r.Name, User__c, User__r.Name FROM Campaign_Incidental_Attendees__r)
				FROM Campaign_Incidental__c
				WHERE Campaign__c = :campaignId
			];

			for(Campaign_Incidental__c ci :incidentals) {
				dtos.add(new CampaignIncidentalDTO(ci));
			}		
		}

		return dtos;
	}

	@auraEnabled
	public static CampaignIncidentalDTO fetchCampaignIncidental(id incidentalId) {
		try {

			if (!UserUtil.canAccessIncidentalExpenses()) { return null; }

			Campaign_Incidental__c incidental = [
			SELECT Id, Campaign__c, Name, Description__c, Purchase_Date_Time__c, Attendee_Count__c, Cost_Per_Attendee__c, Total_Cost__c, Transfer_of_Value__c, Notes__c, 
				(SELECT Contact__c, Contact__r.Name, User__c, User__r.Name FROM Campaign_Incidental_Attendees__r)
			FROM Campaign_Incidental__c
			WHERE Id = :incidentalId
			][0];

			List<CampaignMember> availMembers = fetchCampaignMembers(incidental.Campaign__c);
			List<Campaign_User__c> availUsers = fetchCampaignUsers(incidental.Campaign__c);
			
			CampaignIncidentalDTO dto = new CampaignIncidentalDTO(incidental, availMembers, availUsers);

			return dto;		

		} catch(QueryException ex) {
			return null;		
		}
	}

	@auraEnabled
	public static CampaignIncidentalDTO createNewCampaignIncidental(id campaignId) {
		if (!UserUtil.canUpsertIncidentalExpenses()) { return null; }

		Campaign_Incidental__c incidental = new Campaign_Incidental__c(
			Campaign__c = campaignId
		);

		List<CampaignMember> availMembers = fetchCampaignMembers(campaignId);
		List<Campaign_User__c> availUsers = fetchCampaignUsers(campaignId);

		CampaignIncidentalDTO dto = new CampaignIncidentalDTO(incidental, availMembers, availUsers);

		return dto;
	}

	private static List<CampaignMember> fetchCampaignMembers(id campaignId) { 
		return [
			SELECT ContactId, Contact.Name 
			FROM CampaignMember 
			WHERE CampaignId = :campaignId
			AND Type__c != 'Waitlist'
		];	
	}

	private static List<Campaign_User__c> fetchCampaignUsers(id campaignId) { 
		return [
			SELECT User__c, User__r.Name 
			FROM Campaign_User__c
			WHERE Campaign__c = :campaignId
		];
	}

	@auraEnabled
	public static void deleteCampaignIncidental(id incidentalId) {
		if (UserUtil.canDeleteIncidentalExpenses()) { 
			Campaign_Incidental__c ci = new Campaign_Incidental__c(
				Id = incidentalId
			);

			delete ci;		
		}
	}

	@auraEnabled
	public static void saveCampaignIncidental(string dto) {
		if (UserUtil.canUpsertIncidentalExpenses()) { 
			CampaignIncidentalDTO incidental = ((CampaignIncidentalDTO)System.JSON.deserializeStrict(dto, CampaignIncidentalDTO.Class));

			string name = 'Campaign Incidental';

			if (incidental.description != null && incidental.purchaseDateTime != null) {
				if(incidental.description.length() >= 40) {
					name = incidental.description.substring(0, 36) + '... - ' + incidental.purchaseDateTime.format();
				} else {
					name = incidental.description + ' - ' + incidental.purchaseDateTime.format();
				}
			}

			Campaign_Incidental__c ci = new Campaign_Incidental__c(
				Id = incidental.incidentalId,
				Name = name,
				Campaign__c = incidental.campaignId,
				Description__c = incidental.description,
				Purchase_Date_Time__c = incidental.purchaseDateTime,
				Total_Cost__c = incidental.totalCost,
				Transfer_of_Value__c = incidental.transferOfValue,
				Notes__c = incidental.notes
			);

			upsert ci;

			saveCampaignIncidentalMembers(incidental, ci);
			saveCampaignIncidentalUsers(incidental, ci);		
		}
	}

	private static void saveCampaignIncidentalMembers(CampaignIncidentalDTO incidental, Campaign_Incidental__c cm) {
		if (!incidental.selectedMembers.isEmpty()) {
			Set<Id> selected = new Set<Id>();
			Set<Id> existing = new Set<Id>();

			for(CampaignIncidentalAttendeeDTO cma :incidental.selectedMembers) {
				selected.add(cma.value);
			}

			//First let's remove any members that were removed
			List<Campaign_Incidental_Attendee__c> deleteMembers = [
				SELECT Id 
				FROM Campaign_Incidental_Attendee__c
				WHERE Campaign_Incidental__c = :cm.Id 
				AND Contact__c NOT IN :selected
				AND User__c = NULL
			];

			if (!deleteMembers.isEmpty()) {
				delete deleteMembers;
			}

			List<Campaign_Incidental_Attendee__c> existingMembers = [
				SELECT Id, Contact__c 
				FROM Campaign_Incidental_Attendee__c
				WHERE Campaign_Incidental__c = :cm.Id 
				AND Contact__c IN :selected
			];

			List<Campaign_Incidental_Attendee__c> newMembers = new List<Campaign_Incidental_Attendee__c>();

			if (!existingMembers.isEmpty()) {
				for(Campaign_Incidental_Attendee__c em :existingMembers) {
					if (!existing.contains(em.Contact__c)) { 
						existing.add(em.Contact__c); 
					}
				}

				for(CampaignIncidentalAttendeeDTO cma :incidental.selectedMembers) {
					if (!existing.contains(cma.value)) {
						newMembers.add(new Campaign_Incidental_Attendee__c(
							Campaign_Incidental__c = cm.Id,
							Name = cma.value + ': ' + cm.Name,
							Contact__c = cma.value
						));					
					}
				}
			} else {
				for(CampaignIncidentalAttendeeDTO cma :incidental.selectedMembers) {
					newMembers.add(new Campaign_Incidental_Attendee__c(
						Campaign_Incidental__c = cm.Id,
						Name = cma.value + ': ' + cm.Name,
						Contact__c = cma.value
					));
				}				
			}

			if (!newMembers.isEmpty()) {
				insert newMembers;
			}
		} else {
			List<Campaign_Incidental_Attendee__c> deleteMembers = [
				SELECT Id 
				FROM Campaign_Incidental_Attendee__c
				WHERE Campaign_Incidental__c = :cm.Id 
				AND Contact__c != NULL
				AND User__c = NULL
			];

			if (!deleteMembers.isEmpty()) {
				delete deleteMembers;
			}
		}
	}

	private static void saveCampaignIncidentalUsers(CampaignIncidentalDTO incidental, Campaign_Incidental__c cm) {
		if (!incidental.selectedUsers.isEmpty()) {
			Set<Id> selected = new Set<Id>();
			Set<Id> existing = new Set<Id>();

			for(CampaignIncidentalAttendeeDTO cma :incidental.selectedUsers) {
				selected.add(cma.value);
			}

			//First let's remove any members that were removed
			List<Campaign_Incidental_Attendee__c> deleteUsers = [
				SELECT Id 
				FROM Campaign_Incidental_Attendee__c
				WHERE Campaign_Incidental__c = :cm.Id 
				AND User__c NOT IN :selected
				AND Contact__c = NULL
			];

			if (!deleteUsers.isEmpty()) {
				delete deleteUsers;
			}

			List<Campaign_Incidental_Attendee__c> existingMembers = [
				SELECT Id, User__c 
				FROM Campaign_Incidental_Attendee__c
				WHERE Campaign_Incidental__c = :cm.Id 
				AND User__c IN :selected
			];

			List<Campaign_Incidental_Attendee__c> newMembers = new List<Campaign_Incidental_Attendee__c>();

			if (!existingMembers.isEmpty()) {
				for(Campaign_Incidental_Attendee__c em :existingMembers) {
					if (!existing.contains(em.User__c)) { 
						existing.add(em.User__c); 
					}
				}

				for(CampaignIncidentalAttendeeDTO cma :incidental.selectedUsers) {
					if (!existing.contains(cma.value)) {
						newMembers.add(new Campaign_Incidental_Attendee__c(
							Campaign_Incidental__c = cm.Id,
							Name = cma.value + ': ' + cm.Name,
							User__c = cma.value
						));					
					}
				}
			} else {
				for(CampaignIncidentalAttendeeDTO cma :incidental.selectedUsers) {
					newMembers.add(new Campaign_Incidental_Attendee__c(
						Campaign_Incidental__c = cm.Id,
						Name = cma.value + ': ' + cm.Name,
						User__c = cma.value
					));
				}				
			}

			if (!newMembers.isEmpty()) {
				insert newMembers;
			}
		} else {
			List<Campaign_Incidental_Attendee__c> deleteUsers = [
				SELECT Id 
				FROM Campaign_Incidental_Attendee__c
				WHERE Campaign_Incidental__c = :cm.Id 
				AND User__c != NULL
				AND Contact__c = NULL
			];

			if (!deleteUsers.isEmpty()) {
				delete deleteUsers;
			}
		}
	}
}