Public without sharing class KOLNominationPDF{
    public string RecordId{get;set;}
    public KOL_Nomination__c KOL{get;set;}
    public String NomineeFullName{get;set;}
    public String RequestorFullName{get;set;}
    public String ManagerFullName{get;set;}
    Public String OfficeAddress{get;set;}
    Public Integer ClinicalExpRatingDMTotal{get;set;}
    Public Integer ClinicalExpRatingRMTotal{get;set;}
    Public Integer ImpactRatingDMTotal{get;set;}
    Public Integer ImpactRatingRMTotal{get;set;}
    Public Integer ImpactRatingPTTotal{get;set;}
    //Public String effectiveDate{get;set;}
    //Public String DMSubmissionDate{get;set;}
    //Public String RMSubmissionDate{get;set;}
    public String newString{get;set;}
    public List<String> RequestedHCPServices{get;set;}
    List<String> picklistValues = new List<String>();
    public KOLNominationPDF(){
        RecordId  = ApexPages.CurrentPage().getparameters().get('id');
        KOL = [Select Id,Name,Contact__c,CreatedBy.FirstName,CreatedBy.LastName,CreatedBy.Title,CreatedBy.Manager.FirstName,
                CreatedBy.Manager.LastName,CreatedDate,HCP_First_Name__c,Contact__r.FirstName,Contact__r.LastName,Contact__r.Title,
                HCP_Title__c,HCP_Email_Address__c,HCP_Clinical_Specialty__c,HCP_Cell_Number__c,
                HCP_s_NPI_Number__c,HCP_is_Currently_Employed__c,Employment_Details__c,HCPLicensedState__c,
                Office_Name__c,Office_Contact_Name__c,Office_Contact_Number__c,Office_Street_Address__c,
                Office_City__c,Office_State__c,Office_Zip__c,HCP_Speaking_Qualifications__c,
                Office_Phone_Number__c,Office_Fax_Number__c,RequestedHCPServices__c,HCP_Qualified_Subject__c,
                HCP_Qualifications_for_Services__c,HCP_Educational_Need_to_Fulfill__c,
                Title_of_HCP_s_Presentation__c,Preferred_CTO_Products__c,PreferredAVAccessProducts__c,
                CTOMonthlyCaseVolume__c,AVAccessMonthlyCaseVolume__c,PreferredSFAProducts__c,
                PreferredCLIBTKPedalAccessProducts__c,SFAMonthlyCaseVolume__c,CLIMonthlyCaseVolume__c,
                LengthoftimeusingSFAProducts__c,LengthoftimeusingCLIBTKProducts__c,PreferredVenousAccessProducts__c,
                PreferredCardiovascularProducts__c,VenousAccessMonthlyCaseVolume__c,CardiovascularMonthlyCaseVolume__c,
                LengthoftimeusingVenousAccProducts__c,LengthoftimeusingCardiovascProducts__c,CTOProductsTime__c,AVProductsTime__c,
                PreferredBreastSoftTissueProducts__c,BreastSoftTissueMonthlyCaseVolume__c,LengthoftimeusingBreastProducts__c,
                Techniques_OtherInfo__c,Numberofcasesobserved__c,TechniquesObservedByRequestor__c,FacilityType__c,
                Maximumnumberofattendees__c,CanFacilitydoHandsOnTraining__c,LiveAnimalCapacity__c,Cadaver_Capacity__c,Accessibilityfromnearestairport__c,
                DMMonthlyCaseVolumewProducts__c,DMRatingExpwithRelevantProducts__c,DM_Level_Knowledge__c,
                DM_Clinical_Experience__c,DM_surgical_Skills__c,DM_Speaking_Skills__c,RM_Level_Knowledge__c,RM_Clinical_Experience__c,
                RM_surgical_Skills__c,RM_Speaking_Skills__c,RM_Relevant_Products__c,RMRatingMonthlyCaseVolume__c,DM_Society_Membership__c,
                DM_Publication_Exp__c,DM_Society_EXP__c,DM_Formal_Clinical_EXP__c,DM_Formal_Pre_Clinical_EXP__c,DM_International_EXP__c,
                DM_Reputation__c,RM_SocietyImpactMembership__c,RM_Publication_Exp__c,RM_Society_EXP__c,RM_Formal_Clinical_EXP__c,
                RM_Formal_Pre_Clinical_EXP__c,RM_International_EXP__c,RM_Reputation__c,PT_SocietyImpactMembership__c,
                PT_Publication_Exp__c,PT_Society_EXP__c,PT_Formal_Clinical_EXP__c,PT_Formal_Pre_Clinical_EXP__c,
                PT_International_EXP__c,PT_Reputation__c,EffectiveDate__c,DmObservedHCPsOperativeTechniques__c,
                HCPhaspriorrelationshipwithBDBARD__c,HCPPublications__c,PTOnly_OtherHCPOnRetainer__c,PTONLY_Aretheresimilarconsultants__c,
                PTONLYIstrainingconductedatHCPFacility__c,PTONLYSuppliesAndEquipment__c,PTONLY_LivePatients__c,PTONLY_Howistrainingconducted__c,
                PTONOLY_NeedHCPsadvancesurgerysched__c,DM_Submission_Date__c,RM_Submission_Date__c,DM_has_observed_HCP__c,
                ListHCPPublicationsandTopics__c,ListHCPPresentations__c
                  from KOL_Nomination__c where id=: RecordId];
                
        //Datetime dt = KOL.EffectiveDate__c;
        //effectiveDate = dt.format('MMMM dd, YYYY');
        
        
        RequestorFullName = validateString(KOL.CreatedBy.FirstName)+' '+KOL.CreatedBy.LastName;
        NomineeFullName = validateString(KOL.Contact__r.FirstName)+' '+KOL.Contact__r.LastName+' '+validateString(KOL.Contact__r.Title);
        ManagerFullName = validateString(KOL.CreatedBy.Manager.FirstName)+' '+KOL.CreatedBy.Manager.LastName;
        OfficeAddress = validateString(KOL.Office_City__c)+','+validateString(KOL.Office_State__c)+','+validateString(KOL.Office_Zip__c);
        ClinicalExpRatingDMTotal = validateInteger(KOL.DMMonthlyCaseVolumewProducts__c) + validateInteger(KOL.DMRatingExpwithRelevantProducts__c) + validateInteger(KOL.DM_Level_Knowledge__c) + validateInteger(KOL.DM_Clinical_Experience__c) + validateInteger(KOL.DM_surgical_Skills__c) + validateInteger(KOL.DM_Speaking_Skills__c);
        ClinicalExpRatingRMTotal = validateInteger(KOL.RMRatingMonthlyCaseVolume__c) + validateInteger(KOL.RM_Relevant_Products__c) + validateInteger(KOL.RM_Level_Knowledge__c) + validateInteger(KOL.RM_Clinical_Experience__c) + validateInteger(KOL.RM_surgical_Skills__c) + validateInteger(KOL.RM_Speaking_Skills__c);
        
        ImpactRatingDMTotal = validateInteger(KOL.DM_Society_Membership__c) + validateInteger(KOL.DM_Publication_Exp__c) + validateInteger(KOL.DM_Society_EXP__c) + validateInteger(KOL.DM_Formal_Clinical_EXP__c) + validateInteger(KOL.DM_Formal_Pre_Clinical_EXP__c) + validateInteger(KOL.DM_International_EXP__c) + validateInteger(KOL.DM_Reputation__c);
        ImpactRatingRMTotal = validateInteger(KOL.RM_SocietyImpactMembership__c) + validateInteger(KOL.RM_Publication_Exp__c) + validateInteger(KOL.RM_Society_EXP__c) + validateInteger(KOL.RM_Formal_Clinical_EXP__c) + validateInteger(KOL.RM_Formal_Pre_Clinical_EXP__c) + validateInteger(KOL.RM_International_EXP__c) + validateInteger(KOL.RM_Reputation__c);
        ImpactRatingPTTotal = validateInteger(KOL.PT_SocietyImpactMembership__c) + validateInteger(KOL.PT_Publication_Exp__c) + validateInteger(KOL.PT_Society_EXP__c) + validateInteger(KOL.PT_Formal_Clinical_EXP__c) + validateInteger(KOL.PT_Formal_Pre_Clinical_EXP__c) + validateInteger(KOL.PT_International_EXP__c) + validateInteger(KOL.PT_Reputation__c);
        
        //Showing multipicklist values as bulleted List view.
        newString = KOL.RequestedHCPServices__c;
        RequestedHCPServices = new List<String>();
        List<String> picklistValues = new List<String>();
        if(newString != null && newString != '')
            newString.split(';');
        for(integer i =0; i<picklistValues.Size(); i++){
            RequestedHCPServices.add(picklistValues[i]+'\n');
        }
    }
    
    public static Integer validateInteger(string KOLFieldVal){
        if(string.isNotBlank(KOLFieldVal)){
            return integer.valueOf(KOLFieldVal);
        }
        else
            return 0;
    }
    
    public static string validateString(string fieldValue){
        if(string.isNotBlank(fieldValue)){
            return fieldValue;
        }
        else
            return '';
    }
    public List<String> getFinalString(){
        return RequestedHCPServices;
    }
}