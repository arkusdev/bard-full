/*
    Created By : Apostletech Dev
    Description : Used to calculate the expenses with respect to type like meals, hotels, transport etc and can be downloaded as Excel
                    or can be viewd as tabular format by using filters as dropdown.
                    --> Class can be invoked from HCP_Report and HCP_Report_View VF pages. These VF pages are invoked from 
                        HCPAddendumExportButton lightning component --> Card #5675
    Class Name : HCPReportController
*/
public class HCPReportController
{
    public CampaignResponse mealsInfo{set;get;}
    public CampaignResponse hotelsInfo{set;get;}
    public CampaignResponse transportInfo{set;get;}
    public CampaignResponse incidentInfo{set;get;}
    public CampaignResponse proctorInfo{set;get;}
    public CampaignResponse flightsInfo{set;get;}
    public Decimal totalCost{set;get;}
    public campaign campInfo{get;set;}
    public string reportName{Get;set;}
    public string reportType{get;set;}
    
    //Constrcutor which holds the data from campaign record
    public HCPReportController()
    {
        totalCost = 0;
        campInfo = new campaign();
        String campaignId = ApexPages.currentPage().getParameters().get('Id');
        reportType = system.currentPagereference().getParameters().get('type');
        campInfo = [select id,Classification__c,name,SubLedger__c,StartDate,Products_Used__c,Course_Location__c,Course_Location_State__c
                    from campaign where id =: campaignId];
        
        if(campInfo.StartDate != null){
            String formatedDate = campInfo.startDate.year()+'-'+campInfo.startDate.month()+'-'+campInfo.startDate.day();
            reportName = campInfo.Name+'-'+formatedDate+'-'+campInfo.SubLedger__c;
        }
        else
            reportName = campInfo.Name+'-'+campInfo.SubLedger__c;
        
        //Meals Query
        if(reportType == 'Meals' || reportType == 'All'){    
            list<Campaign_Meal__c> meals = [SELECT Id,campaign__r.Classification__c,Additional_Attendees__c,Cost_Per_Attendee__c,Campaign__r.Course_Location__c,Campaign__r.Course_Location_State__c,Campaign__r.Products_Used__c,Additional_Attendee_Cost__c,Total_Cost__c,
                                            (SELECT Id,Contact__c,contact__r.Covered_Recipient_Type__c,Contact__r.Global_ID__c,Contact__r.FirstName,Contact__r.LastName,Contact__r.Suffix,Contact__r.MiddleName,
                                             user__c,User__r.FirstName,User__r.LastName,User__r.MiddleName,User__r.Suffix FROM Campaign_Meal_Attendees__r) 
                                            FROM Campaign_Meal__c WHERE Campaign__c =: campaignId];
            mealsInfo = processMeals(meals);
        }
        
        // Flights Query
        if(reportType == 'Flights' || reportType == 'All'){
            list<Campaign_Flight__c> flights = [SELECT Id,Cost__c,Campaign__r.Classification__c, Campaign__r.Course_Location__c,Campaign__r.Course_Location_State__c,Campaign__r.Products_Used__c,
                                                Contact__c,contact__r.Covered_Recipient_Type__c,Contact__r.Global_ID__c,Contact__r.FirstName,Contact__r.LastName,Contact__r.Suffix,Contact__r.MiddleName,
                                                user__c,User__r.FirstName,User__r.LastName,User__r.MiddleName,User__r.Suffix 
                                                FROM Campaign_Flight__c WHERE Campaign__c =: campaignId];
            flightsInfo = processFlights(flights);
        }
        
        //Hotels Query
        if(reportType == 'Hotels' || reportType == 'All'){
            list<Campaign_Hotel__c> hotels = [select id,Total_Present_Cost__c,campaign__r.Classification__c,Additional_Attendees_Cost__c,Additional_Attendees__c,Campaign__r.Course_Location__c,Campaign__r.Course_Location_State__c,Campaign__r.Products_Used__c,Total_Cost__c,
                                              (SELECT Id,Cost__c,Contact__c,Present__c,contact__r.Covered_Recipient_Type__c,Contact__r.Global_ID__c,Contact__r.FirstName,Contact__r.LastName,Contact__r.Suffix,Contact__r.MiddleName,
                                               user__c,User__r.FirstName,User__r.LastName,User__r.MiddleName,User__r.Suffix FROM Campaign_Hotel_Attendees__r where Present__c =: true) 
                                                FROM Campaign_Hotel__c where Campaign__c =: campaignId];
            
            hotelsInfo = processHotels(hotels);
        }
        
        //Transportation Query
        if(reportType == 'Transportation' || reportType == 'All'){
            list<Campaign_Transportation__c> transport = [select id,campaign__r.Classification__c,Additional_Attendees_Cost__c,Cost_Per_Attendee__c,Additional_Attendees__c,Campaign__r.Course_Location__c,Campaign__r.Course_Location_State__c,Campaign__r.Products_Used__c,Total_Cost__c,
                                              (SELECT Id,Contact__c,contact__r.Covered_Recipient_Type__c,Contact__r.Global_ID__c,Contact__r.FirstName,Contact__r.LastName,Contact__r.Suffix,Contact__r.MiddleName,
                                               user__c,User__r.FirstName,User__r.LastName,User__r.MiddleName,User__r.Suffix FROM Campaign_Transportation_Attendees__r) 
                                                FROM Campaign_Transportation__c where Campaign__c =: campaignId];
            
            transportInfo = processTransportation(transport);
        }
        
        //Incidents Query
        if(reportType == 'Additional Course Costs' || reportType == 'All'){
            list<Campaign_Incidental__c> incidents = [select id,campaign__r.Classification__c,Cost_Per_Attendee__c,Campaign__r.Course_Location__c,Transfer_of_Value__c,Campaign__r.Course_Location_State__c,Campaign__r.Products_Used__c,Total_Cost__c,
                                              (SELECT Id,Contact__c,contact__r.Covered_Recipient_Type__c,Contact__r.Global_ID__c,Contact__r.FirstName,Contact__r.LastName,Contact__r.Suffix,Contact__r.MiddleName,
                                               user__c,User__r.FirstName,User__r.LastName,User__r.MiddleName,User__r.Suffix FROM Campaign_Incidental_Attendees__r) 
                                                FROM Campaign_Incidental__c where Campaign__c =: campaignId];
            
            incidentInfo = processIncidents(incidents);
        }
        
        //Proctor Query
        if(reportType == 'Proctor Fees' || reportType == 'All'){
            list<Campaign_Proctor_Fee__c> proctors = [select id,campaign__r.Classification__c,Proctor__c,Transfer_of_Value__c,Total_Cost__c,Campaign__r.Course_Location__c,Campaign__r.Course_Location_State__c,Campaign__r.Products_Used__c,
                                                        Proctor__r.Covered_Recipient_Type__c,Proctor__r.Global_ID__c,Proctor__r.FirstName,Proctor__r.LastName,Proctor__r.Suffix,Proctor__r.MiddleName
                                                      FROM Campaign_Proctor_Fee__c where Campaign__c =: campaignId];
            
            proctorInfo = processProctors(proctors);
        }
        
    }
    
    //Meals functionality
    private CampaignResponse processMeals(list<Campaign_Meal__c> meals)
    {
        map<Id,Data> tempMap = new map<Id,Data>();
        Decimal additionalAttendeeCost = 0;
        Decimal totalAttendeeCost = 0;
        for(Campaign_Meal__c cm : meals)
        {
            for(Campaign_Meal_Attendee__c cma : cm.Campaign_Meal_Attendees__r)
            {
                if(cma.contact__c != null)
                {
                    if(tempMap.containsKey(cma.Contact__c))
                    {
                        Data contactData = tempMap.get(cma.Contact__c);
                        contactData.addValue(cm.Cost_Per_Attendee__c);
                        tempMap.put(cma.Contact__c, contactData);
                    }
                    else
                    {
                        Data contactData = new data(cma.contact__r.Covered_Recipient_Type__c, cma.contact__r.lastName, cma.contact__r.firstName, cma.contact__r.middleName, cma.contact__r.suffix, cma.contact__r.Global_ID__c, cm.Campaign__r.Course_Location__c, cm.Campaign__r.Course_Location_State__c, 'Food and Beverage', cm.campaign__r.Classification__c, cm.Campaign__r.Products_Used__c,cm.Cost_Per_Attendee__c);                        
                        totalAttendeeCost = cm.Total_Cost__c;
                        tempMap.put(cma.Contact__c, contactData);
                    }
                    
                }
                else if(cma.user__c != null)
                {
                    if(tempMap.containsKey(cma.User__c))
                    {
                        Data contactData = tempMap.get(cma.User__c);
                        contactData.addValue(cm.Cost_Per_Attendee__c);
                        tempMap.put(cma.User__c, contactData);
                    }
                    else
                    {
                        Data contactData = new data('', cma.user__r.lastName, cma.user__r.firstName, cma.user__r.middleName, cma.user__r.suffix, '', cm.Campaign__r.Course_Location__c, cm.Campaign__r.Course_Location_State__c, 'Food and Beverage', cm.campaign__r.Classification__c, cm.Campaign__r.Products_Used__c, cm.Cost_Per_Attendee__c);                        
                        totalAttendeeCost = cm.Total_Cost__c;
                        tempMap.put(cma.user__c, contactData);
                    }
                }
            }
            if(additionalAttendeeCost == 0)
                additionalAttendeeCost = cm.Additional_Attendee_Cost__c != null ? cm.Additional_Attendee_Cost__c : 0;
            else
                additionalAttendeeCost += cm.Additional_Attendee_Cost__c;
            totalCost += cm.Total_Cost__c;
        }
        return new CampaignResponse(additionalAttendeeCost, totalAttendeeCost, tempMap.values());
    }
    
    //Hotels functionality
    private CampaignResponse processHotels(list<Campaign_Hotel__c> hotels)
    {
        map<Id,Data> tempMap = new map<Id,Data>();
        Decimal additionalAttendeeCost = 0;
        Decimal totalAttendeeCost = 0;
        for(Campaign_Hotel__c cm : hotels)
        {
            for(Campaign_Hotel_Attendee__c cma : cm.Campaign_Hotel_Attendees__r)
            {
                if(cma.contact__c != null)
                {
                    if(tempMap.containsKey(cma.Contact__c))
                    {
                        Data contactData = tempMap.get(cma.Contact__c);
                        contactData.addValue(cma.Cost__c);
                        tempMap.put(cma.Contact__c, contactData);
                    }
                    else
                    {
                        Data contactData = new data(cma.contact__r.Covered_Recipient_Type__c, cma.contact__r.lastName, cma.contact__r.firstName, cma.contact__r.middleName, cma.contact__r.suffix, cma.contact__r.Global_ID__c, cm.Campaign__r.Course_Location__c, cm.Campaign__r.Course_Location_State__c, 'Travel', cm.campaign__r.Classification__c, cm.Campaign__r.Products_Used__c, cma.Cost__c);                        
                        totalAttendeeCost = cma.Cost__c;
                        tempMap.put(cma.Contact__c, contactData);
                    }
                    
                }
                else if(cma.user__c != null)
                {
                    if(tempMap.containsKey(cma.User__c))
                    {
                        Data contactData = tempMap.get(cma.User__c);
                        contactData.addValue(cma.Cost__c);
                        tempMap.put(cma.User__c, contactData);
                    }
                    else
                    {
                        Data contactData = new data('', cma.user__r.lastName, cma.user__r.firstName, cma.user__r.middleName, cma.user__r.suffix, '', cm.Campaign__r.Course_Location__c, cm.Campaign__r.Course_Location_State__c, 'Travel', cm.campaign__r.Classification__c, cm.Campaign__r.Products_Used__c, cma.Cost__c);                        
                        totalAttendeeCost = cma.Cost__c;
                        tempMap.put(cma.user__c, contactData);
                    }
                }
            }
            
            if(additionalAttendeeCost == 0)
                additionalAttendeeCost = cm.Additional_Attendees_Cost__c != null ? cm.Additional_Attendees_Cost__c : 0;
            else
                additionalAttendeeCost += cm.Additional_Attendees_Cost__c;                
            
            totalCost += cm.Total_Present_Cost__c;
        }
        return new CampaignResponse(additionalAttendeeCost, totalAttendeeCost, tempMap.values());
    }
    
    //Transportation functionality
    private CampaignResponse processTransportation(list<Campaign_Transportation__c> transport)
    {
        map<Id,Data> tempMap = new map<Id,Data>();
        Decimal additionalAttendeeCost = 0;
        Decimal totalAttendeeCost = 0;
        for(Campaign_Transportation__c cm : transport)
        {
            for(Campaign_Transportation_Attendee__c cma : cm.Campaign_Transportation_Attendees__r)
            {
                if(cma.contact__c != null)
                {
                    if(tempMap.containsKey(cma.Contact__c))
                    {
                        Data contactData = tempMap.get(cma.Contact__c);
                        contactData.addValue(cm.Cost_Per_Attendee__c);
                        tempMap.put(cma.Contact__c, contactData);
                    }
                    else
                    {
                        Data contactData = new data(cma.contact__r.Covered_Recipient_Type__c, cma.contact__r.lastName, cma.contact__r.firstName, cma.contact__r.middleName, cma.contact__r.suffix, cma.contact__r.Global_ID__c, cm.Campaign__r.Course_Location__c, cm.Campaign__r.Course_Location_State__c, 'Travel', cm.campaign__r.Classification__c, cm.Campaign__r.Products_Used__c, cm.Cost_Per_Attendee__c);                        
                        totalAttendeeCost = cm.Cost_Per_Attendee__c;
                        tempMap.put(cma.Contact__c, contactData);
                    }
                    
                }
                else if(cma.user__c != null)
                {
                    if(tempMap.containsKey(cma.User__c))
                    {
                        Data contactData = tempMap.get(cma.User__c);
                        contactData.addValue(cm.Cost_Per_Attendee__c);
                        tempMap.put(cma.User__c, contactData);
                    }
                    else
                    {
                        Data contactData = new data('', cma.user__r.lastName, cma.user__r.firstName, cma.user__r.middleName, cma.user__r.suffix, '', cm.Campaign__r.Course_Location__c, cm.Campaign__r.Course_Location_State__c, 'Travel', cm.campaign__r.Classification__c, cm.Campaign__r.Products_Used__c, cm.Cost_Per_Attendee__c);                        
                        totalAttendeeCost = cm.Cost_Per_Attendee__c;
                        tempMap.put(cma.user__c, contactData);
                    }
                }
            }
            
            if(additionalAttendeeCost == 0)
                additionalAttendeeCost = cm.Additional_Attendees_Cost__c != null ? cm.Additional_Attendees_Cost__c : 0;
            else
                additionalAttendeeCost += cm.Additional_Attendees_Cost__c;
                
            totalCost += cm.Total_Cost__c;
        }
        return new CampaignResponse(additionalAttendeeCost, totalAttendeeCost, tempMap.values());
    }
    
    
    //Incidentfunctionality
    private CampaignResponse processIncidents(list<Campaign_Incidental__c> incident)
    {
        map<Id,Data> tempMap = new map<Id,Data>();
        Decimal additionalAttendeeCost = 0;
        Decimal totalAttendeeCost = 0;
        for(Campaign_Incidental__c cm : incident)
        {
            String transferValue = string.isNotBlank(cm.Transfer_of_Value__c) ? cm.Transfer_of_Value__c : 'No Value';
            for(Campaign_Incidental_Attendee__c cma : cm.Campaign_Incidental_Attendees__r)
            {                
                if(cma.contact__c != null)
                {
                    if(tempMap.containsKey(cma.Contact__c))
                    {
                        Data contactData = tempMap.get(cma.Contact__c);
                        contactData.addValue(cm.Cost_Per_Attendee__c);
                        tempMap.put(cma.Contact__c, contactData);
                    }
                    else
                    {
                        Data contactData = new data(cma.contact__r.Covered_Recipient_Type__c, cma.contact__r.lastName, cma.contact__r.firstName, cma.contact__r.middleName, cma.contact__r.suffix, cma.contact__r.Global_ID__c, cm.Campaign__r.Course_Location__c, cm.Campaign__r.Course_Location_State__c, transferValue, cm.campaign__r.Classification__c, cm.Campaign__r.Products_Used__c, cm.Cost_Per_Attendee__c);                        
                        totalAttendeeCost = cm.Cost_Per_Attendee__c;
                        tempMap.put(cma.Contact__c, contactData);
                    }
                    
                }
                else if(cma.user__c != null)
                {
                    if(tempMap.containsKey(cma.User__c))
                    {
                        Data contactData = tempMap.get(cma.User__c);
                        contactData.addValue(cm.Cost_Per_Attendee__c);
                        tempMap.put(cma.User__c, contactData);
                    }
                    else
                    {
                        Data contactData = new data('', cma.user__r.lastName, cma.user__r.firstName, cma.user__r.middleName, cma.user__r.suffix, '', cm.Campaign__r.Course_Location__c, cm.Campaign__r.Course_Location_State__c, transferValue, cm.campaign__r.Classification__c, cm.Campaign__r.Products_Used__c, cm.Cost_Per_Attendee__c);                        
                        totalAttendeeCost = cm.Cost_Per_Attendee__c;
                        tempMap.put(cma.user__c, contactData);
                    }
                }
            }
                
            totalCost += cm.Total_Cost__c;
        }
        return new CampaignResponse(additionalAttendeeCost, totalAttendeeCost, tempMap.values());
    }
    
    //Proctor functionality
    private CampaignResponse processProctors(list<Campaign_Proctor_Fee__c> proctor)
    {
        map<Id,Data> tempMap = new map<Id,Data>();
        Decimal additionalAttendeeCost = 0;
        Decimal totalAttendeeCost = 0;
        for(Campaign_Proctor_Fee__c cma : proctor)
        {
            String transferValue = string.isNotBlank(cma.Transfer_of_Value__c) ? cma.Transfer_of_Value__c : 'No Value';
            if(cma.Proctor__c != null)
            {
                if(tempMap.containsKey(cma.Proctor__c))
                {
                    Data contactData = tempMap.get(cma.Proctor__c);
                    contactData.addValue(cma.Total_Cost__c);
                    tempMap.put(cma.Proctor__c, contactData);
                }
                else
                {
                    Data contactData = new data(cma.Proctor__r.Covered_Recipient_Type__c, cma.Proctor__r.lastName, cma.Proctor__r.firstName, cma.Proctor__r.middleName, cma.Proctor__r.suffix, cma.Proctor__r.Global_ID__c, cma.Campaign__r.Course_Location__c, cma.Campaign__r.Course_Location_State__c, transferValue, cma.campaign__r.Classification__c, cma.Campaign__r.Products_Used__c, cma.Total_Cost__c);                        
                    totalAttendeeCost = cma.Total_Cost__c;
                    tempMap.put(cma.Proctor__c, contactData);
                }
                
            }                
            totalCost += cma.Total_Cost__c;
        }
        return new CampaignResponse(additionalAttendeeCost, totalAttendeeCost, tempMap.values());
    }
    
    //Flights functionality
    private CampaignResponse processFlights(list<Campaign_Flight__c> flights)
    {
        map<Id,Data> tempMap = new map<Id,Data>();
        Decimal additionalAttendeeCost = 0;
        Decimal totalAttendeeCost = 0;
        for(Campaign_Flight__c cma : flights)
        {
            if(cma.contact__c != null)
            {
                if(tempMap.containsKey(cma.contact__c))
                {
                    Data contactData = tempMap.get(cma.contact__c);
                    contactData.addValue(cma.Cost__c);
                    tempMap.put(cma.contact__c, contactData);
                }
                else
                {
                    Data contactData = new data(cma.contact__r.Covered_Recipient_Type__c, cma.contact__r.lastName, cma.contact__r.firstName, cma.contact__r.middleName, cma.contact__r.suffix, cma.contact__r.Global_ID__c, cma.Campaign__r.Course_Location__c, cma.Campaign__r.Course_Location_State__c, 'Travel', cma.campaign__r.Classification__c,cma.Campaign__r.Products_Used__c, cma.Cost__c);                        
                    totalAttendeeCost = cma.Cost__c;
                    tempMap.put(cma.Contact__c, contactData);
                }
                
            } 
            if(cma.user__c != null)
            {
                if(tempMap.containsKey(cma.user__c))
                {
                    Data contactData = tempMap.get(cma.user__c);
                    contactData.addValue(cma.Cost__c);
                    tempMap.put(cma.user__c, contactData);
                }
                else
                {
                    Data contactData = new data('', cma.user__r.lastName, cma.user__r.firstName, cma.user__r.middleName, cma.user__r.suffix, '', cma.Campaign__r.Course_Location__c, cma.Campaign__r.Course_Location_State__c, 'Travel', cma.campaign__r.Classification__c, cma.Campaign__r.Products_Used__c, cma.Cost__c);
                    totalAttendeeCost = cma.Cost__c;
                    tempMap.put(cma.user__c, contactData);
                }
                
            }                
            totalCost += cma.Cost__c;
        }
        return new CampaignResponse(additionalAttendeeCost, totalAttendeeCost, tempMap.values());
    }
    
        
    public class CampaignResponse
    {
        public Decimal additionalAttendeeCost{set;get;}
        public Decimal totalAttendeeCost{set;get;}
        public list<Data> response{set;get;}
        public CampaignResponse(Decimal additionalAttendeeCost, Decimal totalAttendeeCost, list<Data> response)
        {
            this.additionalAttendeeCost = additionalAttendeeCost;
            this.totalAttendeeCost = totalAttendeeCost;
            this.response = response;
        }
    }
    
    public class Data
    {
        public String coveredRecipientType{set;get;}
        public String lastOrHospital{set;get;}
        public String firstName{set;get;}
        public String middleName{set;get;}
        public String suffix{set;get;}
        public String bardGlobalId{set;get;}
        public String city{set;get;}
        public String stateOrCountry{set;get;}
        public String transferOfValue{set;get;}
        public String productGroup{set;get;}
        public String marketedName{set;get;}
        public Decimal value{set;get;}
        
        public data(String coveredRecipientType, String lastOrHospital, String firstName, String middleName, String suffix, String bardGlobalId, String city, String stateOrCountry, String transferOfValue, String productGroup, String marketedName, Decimal value)
        {
            this.coveredRecipientType = coveredRecipientType; 
            this.lastOrHospital = lastOrHospital; 
            this.firstName  = firstName; 
            this.middleName = middleName;
            this.suffix = suffix;
            this.bardGlobalId = bardGlobalId;
            this.city = city;
            this.stateOrCountry = stateOrCountry; 
            this.transferOfValue = transferOfValue;
            this.productGroup = productGroup;
            this.marketedName = marketedName;
            this.value = value;
        }
        
        public void addValue(Decimal val)
        {
            this.value += val;
        }
    }
}