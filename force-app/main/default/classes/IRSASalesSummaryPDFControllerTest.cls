@isTest
public class IRSASalesSummaryPDFControllerTest {
    @testSetup
    static void createResources() {
        Profile adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        UserRole tmRole = new UserRole(Name= 'Western - FSP - TM');
        insert tmRole;
        UserRole rmRole = new UserRole(Name= 'Peripheral West - RM');
        insert rmRole;
        UserRole csRole = new UserRole(Name= 'Peripheral West - CS', ParentRoleId = tmRole.Id);
        insert csRole;
        User u = new User(LastName = 'TestSalesExecutive',
                            Alias = 'TM tse',
                            Email = 'test.sales.executive@perdaysales.com',
                            Username = 'test.sales.executive@perdaysales.com',
                            Territory__c = '977',
                            UserRoleId = tmRole.Id,
                            ProfileId = adminProfile.Id,
                            CompanyName = 'TEST',
                            Title = 'title',
                            TimeZoneSidKey = 'America/Los_Angeles',
                            EmailEncodingKey = 'UTF-8',
                            LanguageLocaleKey = 'en_US',
                            LocaleSidKey = 'en_US'
                          );
        insert u;

        User csUser = new User(LastName = 'TestCSUser',
                            Alias = 'CS User',
                            Email = 'test.cs@perdaysales.com',
                            Username = 'test.cs@perdaysales.com',
                            UserRoleId = csRole.Id,
                            ProfileId = adminProfile.Id,
                            CompanyName = 'TEST',
                            Title = 'title',
                            TimeZoneSidKey = 'America/Los_Angeles',
                            EmailEncodingKey = 'UTF-8',
                            LanguageLocaleKey = 'en_US',
                            LocaleSidKey = 'en_US'
                          );
        insert csUser;
        
        User rm = new User(LastName = 'TestSalesExecutiveRM',
                          Alias = 'RM tse',
                          Email = 'test.sales.executive.rm@perdaysales.com',
                          Username = 'test.sales.executive.rm@perdaysales.com',
                          UserRoleId = rmRole.Id,
                          ProfileId = adminProfile.Id,
                          CompanyName = 'TEST',
                          Title = 'title',
                          TimeZoneSidKey = 'America/Los_Angeles',
                          EmailEncodingKey = 'UTF-8',
                          LanguageLocaleKey = 'en_US',
                          LocaleSidKey = 'en_US'
                         );
        insert rm;
        
        System.runAs(u) {
            RecordType irsaRT = [SELECT Id FROM RecordType WHERE Name = 'IRSA' AND SObjectType = 'Sales__c' LIMIT 1];
            Account acc1 = new Account(Name='TestAccount1');
            acc1.Ship_to_ID__c = 'ThIsIsTeStAcCoUnT1';
            acc1.BillingCity = 'City';
            insert acc1;

            List<Sales__c> sales = new List<Sales__c>();
            
            Date invoiceDate = Date.today().addDays(-1);
            Sales__c sale1 = new Sales__c();
            sale1.Sales__c = 100;
            sale1.Units__c = 3;
            sale1.OwnerId = u.Id;
            sale1.RecordTypeId = irsaRT.Id;
            sale1.Invoice_Date__c = invoiceDate;
            sale1.Account__c = acc1.Id;
            sale1.Product_Group__c = 'Product_Group2';
            sale1.CAT__c = 'CAT3';
            sale1.Territory__c = '977';

            Sales__c sale2 = new Sales__c();
            sale2.Sales__c = 100;
            sale2.Units__c = 2;
            sale2.OwnerId = u.Id;
            sale2.RecordTypeId = irsaRT.Id;
            sale2.Invoice_Date__c = invoiceDate;
            sale2.Account__c = acc1.Id;
            sale2.Product_Group__c = 'Product_Group2';
            sale2.CAT__c = 'CAT3';
            sale2.Territory__c = '977';
            
            Sales__c sale3 = new Sales__c();
            sale3.Sales__c = 100;
            sale3.Units__c = 10;
            sale3.OwnerId = u.Id;
            sale3.RecordTypeId = irsaRT.Id;
            sale3.Invoice_Date__c = invoiceDate;
            sale3.Account__c = acc1.Id;
            sale3.Product_Group__c = 'Product_Group1';
            sale3.CAT__c = 'CAT1';
            sale3.Territory__c = '977';

            Sales__c sale4 = new Sales__c();
            sale4.Sales__c = 100;
            sale4.Units__c = 12;
            sale4.OwnerId = u.Id;
            sale4.RecordTypeId = irsaRT.Id;
            sale4.Invoice_Date__c = invoiceDate;
            sale4.Account__c = acc1.Id;
            sale4.Product_Group__c = 'Product_Group1';
            sale4.CAT__c = 'CAT1';
            sale4.Territory__c = '977';


            sales.add(sale1);
            sales.add(sale2);
            sales.add(sale3);
            sales.add(sale4);
            insert sales;


            Territory__c terr = new Territory__c(Number__c='977');
            insert terr;
        }
    }
    
    @isTest
    static void controllerTest() {
        User u = [SELECT Id FROM User WHERE LastName = 'TestCSUser' LIMIT 1];
        System.runAs(u){
            PageReference tpageRef = Page.IRSASalesSummaryPDF;
            Test.setCurrentPage(tpageRef);

            ApexPages.currentPage().getParameters().put('sa', 'all');
            ApexPages.currentPage().getParameters().put('su', 'all');
            ApexPages.currentPage().getParameters().put('st', 'this_month');
            ApexPages.currentPage().getParameters().put('sp', 'all');

            IRSASalesSummaryPDFController controller = new IRSASalesSummaryPDFController();
        }
    }
}