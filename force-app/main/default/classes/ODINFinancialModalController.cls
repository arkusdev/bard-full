public with sharing class ODINFinancialModalController {

    @AuraEnabled
    public static Map<String, String> GetRegionOptions(String projectId) {
        Map<String, Datetime> regionsLastModifiedDates = new Map<String, Datetime>();

        List<Financial__c> projectFinancials = [SELECT Id, Region__c, LastModifiedDate FROM Financial__c 
                                                WHERE Project__c =: projectId
                                                AND RecordType.DeveloperName = 'Global_Market_Assessment'
                                                WITH SECURITY_ENFORCED
                                                ORDER BY LastModifiedDate];

        return ODINUtils.GetRegionsLabels(projectFinancials);
    }

    @AuraEnabled
    public static Interest GetInterestOptions(String recordId, String region){
        Interest interest = new Interest();
        interest.levels = new List<String>();

        Schema.DescribeFieldResult fieldResult = Interest__c.Level__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for (Schema.PicklistEntry pickListVal : ple) {
			interest.levels.add(pickListVal.getLabel());
		}
        
        List<Interest__c> interestList = [select    Level__c,
                                                    Market__c,
                                                    Region__c,
                                                    Project__c,
                                                    No_Interest_Reason__c,
                                                    No_Interest_Reason_Text__c  
                                            from Interest__c
                                            where Project__c =: recordId
                                            and Region__c =: region
                                            and Country__c = null
                                            LIMIT 1];
        if (interestList.size() == 1) {
            interest.preSelectedLevel = interestList[0];
        } else {
            interest.preSelectedLevel = new Interest__c();
            interest.preSelectedLevel.Project__c = recordId;
            interest.preSelectedLevel.Region__c = region;
        }

        return interest;
    }

    @AuraEnabled
    public static QuestionWrapper GetProjectQuestions(String recordId, String region) {
        QuestionWrapper wrapper     = new QuestionWrapper();
        wrapper.questions           = new List<QuestionWAnswer>();
        wrapper.requiredQuestions   = new List<QuestionWAnswer>();

        for (Question__c question : [select Question__c, 
                                            Required__c,
                                            (select Region__c, 
                                                    Answer__c,
                                                    Question__c 
                                            from Answers__r 
                                            where Region__c =: region
                                            and Country__c = null
                                            LIMIT 1) 
                                    from Question__c 
                                    where Project__c =: recordId
                                    and Show_In_Global_Market_Assessment__c = true]) {
            QuestionWAnswer qwa = new QuestionWAnswer();
            qwa.question = question;
            if (question.Answers__r.size() == 1) {
                qwa.answer = question.Answers__r[0];
            } else {
                qwa.answer = new Answer__c();
                qwa.answer.Region__c = region;
                qwa.answer.Question__c = question.Id;
            }
            if (question.Required__c) {
                wrapper.requiredQuestions.add(qwa);
            } else {
                wrapper.questions.add(qwa);
            }
        }
        return wrapper;
    }

    @AuraEnabled
    public static FinancialInfoWrapper GetFinancialInfo(String recordId, String region) {
        
        FinancialInfoWrapper wrapper = new FinancialInfoWrapper();
        wrapper.years = new List<String>();
        Schema.DescribeFieldResult fieldResult = Financial__c.Year__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : ple){
			wrapper.years.add(pickListVal.getLabel());
		}

        Project__c project = [select Name, Launch_Year__c from Project__c where Id =: recordId];

        wrapper.financial           = new Financial();
        wrapper.financial.name      = project.Name;
        wrapper.financial.financial = new Financial__c();

        List<Financial__c> financialList = new List<Financial__c>();
        for (Financial__c financial : [select Name, Units__c, ASP__c, Unit_Growth__c, Year__c
                                        from Financial__c 
                                        where Project__c =: recordId 
                                        and Region__c =: region 
                                        and Country__c = null
                                        and RecordType.DeveloperName = 'Global_Market_Assessment'
                                        and Year__c IN: wrapper.years
                                        ORDER BY LastModifiedDate DESC, Year__c ASC]) {
            Financial financialToList = new Financial(financial.name, (Integer)(financial.ASP__c), (Integer)(financial.Unit_Growth__c), financial);
            wrapper.financialsProjectPerYearList.add(financialToList);
            financialList.add(financial);
            if (wrapper.startYear == null){
                wrapper.startYear = financial.Year__c;
            }
        }

        if (!financialList.isEmpty()){
            wrapper.hasFinancials = true;
        }else {
            wrapper.startYear = project.Launch_Year__c;
        }
        return wrapper;
    }

    @AuraEnabled
    public static FinancialInfoWrapper CreateFinancialInfo(String recordId, String region, String jsonWrapper) {
        FinancialInfoWrapper wrapper;
        List<Financial> financialsProjectPerYear = new List<Financial>();
        Boolean hasFinancials = false;
        Map<String, Financial__c> financialsPerYear = new Map<String, Financial__c>();
        String[] years = new String[]{};
        try{
            wrapper = (FinancialInfoWrapper)JSON.deserialize(jsonWrapper, FinancialInfoWrapper.class);
        }catch(Exception exc){
            throw new AuraHandledException(System.Label.Max_Integer_Error_Message);
        }
        wrapper.years = new List<String>();
        
        Integer startYear = Integer.valueOf(wrapper.startYear);
        for (Integer i = 0; i < 5; i++) {
            years.add(String.valueOf(startYear + i));
        }
        List<Financial__c> allFinancials = [SELECT Id, Year__c, Project__c, Project__r.Name, Units__c, ASP__c, Unit_Growth__c, Region__c, Potential_BD_Share__c 
                                            FROM Financial__c 
                                            WHERE Project__c =: recordId 
                                            AND Region__c =: region
                                            AND Country__c = null 
                                            AND Year__c IN: years
                                            AND RecordType.DeveloperName = 'Global_Market_Assessment'
                                            ORDER BY LastModifiedDate DESC, Year__c ASC
                                            LIMIT 5];

        for (Financial__c financial : allFinancials) {
            financialsPerYear.put(financial.Year__c, financial);
        }
        Integer index = 0;
        RecordType recordType = [select Id 
                                    from RecordType 
                                    where SObjectType = 'Financial__c' 
                                    and RecordType.DeveloperName = 'Global_Market_Assessment'];    
        Double units    = wrapper.financial.financial.Units__c;
        for (String year : years) {
            if (financialsPerYear.containsKey(year) && index != 0){
                if (!hasFinancials){
                    hasFinancials = true;
                }
                Financial__c financial = financialsPerYear.get(year);
                Financial__c existingFinancial = wrapper.financialsProjectPerYearList[index].financial;
                Financial newInner              = new Financial();
                newInner.name                   = financial.Project__r.Name;
                newInner.financial              = financial;
                newinner.aspPerUnits            = existingFinancial.ASP__c != null && existingFinancial.Units__c != null ? (Integer)(existingFinancial.ASP__c * existingFinancial.Units__c) : 0;
                
                if (newInner.financial.Potential_BD_Share__c == null) {
                    newInner.financial.Potential_BD_Share__c    = 0;
                    newInner.marketSizePerPotential             = 0;
                } else  {
                    newInner.marketSizePerPotential = (Integer)((newInner.aspPerUnits * newInner.financial.Potential_BD_Share__c) / 100);
                }
                financialsProjectPerYear.add(newInner);
                wrapper.years.add(financial.Year__c);
            }else {
                Boolean setDefaultValues = !hasFinancials || index == 0;
                Integer currentYear = Integer.valueOf(wrapper.startYear) + index;
                Financial__c newFinancial           = new Financial__c();
                newFinancial.Year__c                = String.valueOf(currentYear);
                newFinancial.Year_Sequence__c       = index + 1;
                newFinancial.Project__c             = recordId;
                newFinancial.Unit_Growth__c         = setDefaultValues ? wrapper.financial.financial.Unit_Growth__c : null;
                newFinancial.Region__c              = region;
                newFinancial.Potential_BD_Share__c  = 0;
                newFinancial.RecordTypeId           = recordType.Id;
                newFinancial.ASP__c                 = setDefaultValues ? wrapper.financial.financial.ASP__c : null;

                newFinancial.Units__c               = setDefaultValues ? Integer.valueOf(units) : null;

                Financial newInner              = new Financial();
                newInner.name                   = wrapper.financial.name;
                newInner.financial              = newFinancial;
                newInner.aspPerUnits            = newFinancial.ASP__c != null && newFinancial.Units__c != null ? (Integer)(newFinancial.ASP__c * newFinancial.Units__c) : 0;
                newInner.marketSizePerPotential = setDefaultValues ? (Integer)((newInner.aspPerUnits * newInner.financial.Potential_BD_Share__c) / 100) : null;
                
                financialsProjectPerYear.add(newInner);
                wrapper.years.add(newFinancial.Year__c);
                units   = setDefaultValues && units != null ? ((units * (wrapper.financial.financial.Unit_Growth__c != null ? wrapper.financial.financial.Unit_Growth__c : 0)) / 100) + units : null;
            }
            index++;
        }
        wrapper.financialsProjectPerYearList.clear();
        wrapper.financialsProjectPerYearList.addAll(financialsProjectPerYear);
        return wrapper;
    }

    @AuraEnabled
    public static void UpdateFinancialInfo(String jsonWrapper, Interest__c interest, List<Answer__c> answers) {
        SaveInterest(interest);
        
        SaveAnswers(answers);
        FinancialInfoWrapper wrapper;
        try{
            wrapper = (FinancialInfoWrapper)JSON.deserialize(jsonWrapper, FinancialInfoWrapper.class);
        }catch(Exception exc){
            throw new AuraHandledException(System.Label.Max_Integer_Error_Message);
        }
        List<Financial__c> financialList = new List<Financial__c>();
        for (Financial financial : wrapper.financialsProjectPerYearList){
            financialList.add(financial.financial);
        }

        upsert financialList;
    }

    @AuraEnabled
    public static void SaveInterest(Interest__c interest) {
        upsert interest;
    }

    private static void SaveAnswers(List<Answer__c> answers) {

        List<Answer__c> answerList = new List<Answer__c>();
        for (Answer__c answer : answers) {
            if (answer.Answer__c != null || answer.Id != null) 
                answerList.add(answer);
        }

        upsert answerList;
    }

    @AuraEnabled
    public static FinancialInfo getFinancialInfoByProjectAndRegion(String recordId, String region) {
        FinancialInfoWrapper financialInfo = GetFinancialInfo(recordId, region);
        QuestionWrapper projectQuestions = GetProjectQuestions(recordId, region);
        Interest interest = GetInterestOptions(recordId, region);
        return new FinancialInfo(financialInfo, projectQuestions, interest);
    }


    /////////////////////////////////INNER CLASSES/////////////////////////////////

    public class Interest {
        @AuraEnabled
        public Interest__c  preSelectedLevel;
        @AuraEnabled
        public String       selectedLevel;
        @AuraEnabled
        public List<String> levels;
    }

    public class QuestionWrapper {
        @AuraEnabled
        public List<QuestionWAnswer>    questions;
        @AuraEnabled
        public List<QuestionWAnswer>    requiredQuestions;
    }

    public class QuestionWAnswer {
        @AuraEnabled
        public Question__c  question;
        @AuraEnabled
        public Answer__c    answer;
    }

    public class FinancialInfoWrapper {
        @AuraEnabled
        public List<String>             years;
        @AuraEnabled
        public String                   startYear;
        @AuraEnabled
        public Boolean                  hasFinancials;
        @AuraEnabled
        public Financial                financial;
        @AuraEnabled
        public List<Financial>          financialsProjectPerYearList;
        public FinancialInfoWrapper(){
            financialsProjectPerYearList = new List<Financial>();
        }
    }

    public class Financial {
        @AuraEnabled
        public String       name { set; get; }
        @AuraEnabled
        public Integer      aspPerUnits { set; get; }
        @AuraEnabled
        public Integer      marketSizePerPotential { set; get; }
        @AuraEnabled
        public Financial__c financial { set; get; }
        public Financial(String pName, Integer pAspPerUnits, Integer pMarketSizePerPotential, Financial__c pFinancial){
            name = pName;
            aspPerUnits = pAspPerUnits;
            marketSizePerPotential = pMarketSizePerPotential;
            financial = pFinancial;
        }
        public Financial(){}
    }

    public class FinancialInfo {
        @AuraEnabled
        public FinancialInfoWrapper financialInfoWrapper { set; get; }
        @AuraEnabled
        public QuestionWrapper projectQuestions { set; get; }
        @AuraEnabled
        public Interest interest { set; get; }
        public FinancialInfo(FinancialInfoWrapper pFinancialInfoWrapper, QuestionWrapper pProjectQuestions, Interest pInterest){
            financialInfoWrapper = pFinancialInfoWrapper;
            projectQuestions = pProjectQuestions;
            interest = pInterest;
        }
    }
}