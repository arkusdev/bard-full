public class CampaignTransportationController {
	@auraEnabled
	public static List<CampaignTransportationDTO> fetchCampaignTransportations(id campaignId) {
		List<CampaignTransportationDTO> dtos = new List<CampaignTransportationDTO>();

		if (UserUtil.canAccessTransportationExpenses()) {
			List<Campaign_Transportation__c> trans = [
				SELECT Id, Campaign__c, Name, Pickup_Location__c, Pickup_Date_Time__c, Drop_Off_Location__c, Reservation_Number__c,
				Total_Cost__c, Additional_Attendees__c, Attendee_Count__c, Total_Attendees__c, Cost_Per_Attendee__c, Notes__c,
					(SELECT Contact__c, Contact__r.Name, User__c, User__r.Name FROM Campaign_Transportation_Attendees__r)
				FROM Campaign_Transportation__c
				WHERE Campaign__c = :campaignId
			];

			for(Campaign_Transportation__c ct :trans) {
				dtos.add(new CampaignTransportationDTO(ct));
			}		
		}

		return dtos;
	}

	@auraEnabled
	public static CampaignTransportationDTO fetchCampaignTransportation(id transId) {
		try {

			if (!UserUtil.canAccessTransportationExpenses()) { return null; }

			Campaign_Transportation__c tran = [
				SELECT Id, Campaign__c, Name, Pickup_Location__c, Pickup_Date_Time__c, Drop_Off_Location__c, Reservation_Number__c,
				Total_Cost__c, Additional_Attendees__c, Attendee_Count__c, Total_Attendees__c, Cost_Per_Attendee__c, Notes__c,
					(SELECT Contact__c, Contact__r.Name, User__c, User__r.Name FROM Campaign_Transportation_Attendees__r)
				FROM Campaign_Transportation__c
				WHERE Id = :transId
			][0];

			List<CampaignMember> availMembers = fetchCampaignMembers(tran.Campaign__c);
			List<Campaign_User__c> availUsers = fetchCampaignUsers(tran.Campaign__c);
			
			CampaignTransportationDTO dto = new CampaignTransportationDTO(tran, availMembers, availUsers);

			return dto;		

		} catch(QueryException ex) {
			return null;		
		}
	}

	@auraEnabled
	public static CampaignTransportationDTO createNewCampaignTransportation(id campaignId) {
		if (!UserUtil.canUpsertTransportationExpenses()) { return null; }

		Campaign_Transportation__c tran = new Campaign_Transportation__c(
			Campaign__c = campaignId
		);

		List<CampaignMember> availMembers = fetchCampaignMembers(campaignId);
		List<Campaign_User__c> availUsers = fetchCampaignUsers(campaignId);

		CampaignTransportationDTO dto = new CampaignTransportationDTO(tran, availMembers, availUsers);

		return dto;
	}

	private static List<CampaignMember> fetchCampaignMembers(id campaignId) { 
		return [
			SELECT ContactId, Contact.Name 
			FROM CampaignMember 
			WHERE CampaignId = :campaignId
			AND Type__c != 'Waitlist'
		];	
	}

	private static List<Campaign_User__c> fetchCampaignUsers(id campaignId) { 
		return [
			SELECT User__c, User__r.Name 
			FROM Campaign_User__c
			WHERE Campaign__c = :campaignId
		];
	}

	@auraEnabled
	public static void deleteCampaignTransportation(id transId) {
		if (UserUtil.canDeleteTransportationExpenses()) { 
			Campaign_Transportation__c ct = new Campaign_Transportation__c(
				Id = transId
			);

			delete ct;		
		}
	}

	@auraEnabled
	public static void saveCampaignTransportation(string dto) {
		if (UserUtil.canUpsertTransportationExpenses()) { 
			CampaignTransportationDTO tran = ((CampaignTransportationDTO)System.JSON.deserializeStrict(dto, CampaignTransportationDTO.Class));

			Campaign_Transportation__c ct = new Campaign_Transportation__c(
				Id = tran.transportationId,
				Campaign__c = tran.campaignId,
				Name = tran.pickUpLocation + ' - ' + tran.dropOffLocation,
				Pickup_Location__c = tran.pickUpLocation,
				Pickup_Date_Time__c = tran.pickUpDateTime,
				Drop_Off_Location__c = tran.dropOffLocation,
				Reservation_Number__c = tran.reservationNumber,
				Notes__c = tran.notes,
				Additional_Attendees__c = (tran.additionalAttendees != null ? tran.additionalAttendees : 0),
				Total_Cost__c = tran.totalCost
			);

			upsert ct;

			saveCampaignTransportationMembers(tran, ct);
			saveCampaignTransportationUsers(tran, ct);		
		}
	}

	private static void saveCampaignTransportationMembers(CampaignTransportationDTO tran, Campaign_Transportation__c cm) {
		if (!tran.selectedMembers.isEmpty()) {
			Set<Id> selected = new Set<Id>();
			Set<Id> existing = new Set<Id>();

			for(CampaignTransportationAttendeeDTO cma :tran.selectedMembers) {
				selected.add(cma.value);
			}

			//First let's remove any members that were removed
			List<Campaign_Transportation_Attendee__c> deleteMembers = [
				SELECT Id 
				FROM Campaign_Transportation_Attendee__c
				WHERE Campaign_Transportation__c = :cm.Id 
				AND Contact__c NOT IN :selected
				AND User__c = NULL
			];

			if (!deleteMembers.isEmpty()) {
				delete deleteMembers;
			}

			List<Campaign_Transportation_Attendee__c> existingMembers = [
				SELECT Id, Contact__c 
				FROM Campaign_Transportation_Attendee__c
				WHERE Campaign_Transportation__c = :cm.Id 
				AND Contact__c IN :selected
			];

			List<Campaign_Transportation_Attendee__c> newMembers = new List<Campaign_Transportation_Attendee__c>();

			if (!existingMembers.isEmpty()) {
				for(Campaign_Transportation_Attendee__c em :existingMembers) {
					if (!existing.contains(em.Contact__c)) { 
						existing.add(em.Contact__c); 
					}
				}

				for(CampaignTransportationAttendeeDTO cta :tran.selectedMembers) {
					if (!existing.contains(cta.value)) {
						newMembers.add(new Campaign_Transportation_Attendee__c(
							Campaign_Transportation__c = cm.Id,
							Name = cta.value + ': ' + cm.Name,
							Contact__c = cta.value
						));					
					}
				}
			} else {
				for(CampaignTransportationAttendeeDTO cta :tran.selectedMembers) {
					newMembers.add(new Campaign_Transportation_Attendee__c(
						Campaign_Transportation__c = cm.Id,
						Name = cta.value + ': ' + cm.Name,
						Contact__c = cta.value
					));
				}				
			}

			if (!newMembers.isEmpty()) {
				insert newMembers;
			}
		} else {
			List<Campaign_Transportation_Attendee__c> deleteMembers = [
				SELECT Id 
				FROM Campaign_Transportation_Attendee__c
				WHERE Campaign_Transportation__c = :cm.Id 
				AND Contact__c != NULL
				AND User__c = NULL
			];

			if (!deleteMembers.isEmpty()) {
				delete deleteMembers;
			}
		}
	}

	private static void saveCampaignTransportationUsers(CampaignTransportationDTO tran, Campaign_Transportation__c cm) {
		if (!tran.selectedUsers.isEmpty()) {
			Set<Id> selected = new Set<Id>();
			Set<Id> existing = new Set<Id>();

			for(CampaignTransportationAttendeeDTO cma :tran.selectedUsers) {
				selected.add(cma.value);
			}

			//First let's remove any members that were removed
			List<Campaign_Transportation_Attendee__c> deleteUsers = [
				SELECT Id 
				FROM Campaign_Transportation_Attendee__c
				WHERE Campaign_Transportation__c = :cm.Id 
				AND User__c NOT IN :selected
				AND Contact__c = NULL
			];

			if (!deleteUsers.isEmpty()) {
				delete deleteUsers;
			}

			List<Campaign_Transportation_Attendee__c> existingUsers = [
				SELECT Id, User__c 
				FROM Campaign_Transportation_Attendee__c
				WHERE Campaign_Transportation__c = :cm.Id 
				AND User__c IN :selected
			];

			List<Campaign_Transportation_Attendee__c> newUsers = new List<Campaign_Transportation_Attendee__c>();

			if (!existingUsers.isEmpty()) {
				for(Campaign_Transportation_Attendee__c em :existingUsers) {
					if (!existing.contains(em.User__c)) { 
						existing.add(em.User__c); 
					}
				}

				for(CampaignTransportationAttendeeDTO cta :tran.selectedUsers) {
					if (!existing.contains(cta.value)) {
						newUsers.add(new Campaign_Transportation_Attendee__c(
							Campaign_Transportation__c = cm.Id,
							Name = cta.value + ': ' + cm.Name,
							User__c = cta.value
						));					
					}
				}
			} else {
				for(CampaignTransportationAttendeeDTO cta :tran.selectedUsers) {
					newUsers.add(new Campaign_Transportation_Attendee__c(
						Campaign_Transportation__c = cm.Id,
						Name = cta.value + ': ' + cm.Name,
						User__c = cta.value
					));
				}				
			}

			if (!newUsers.isEmpty()) {
				insert newUsers;
			}
		} else {
			List<Campaign_Transportation_Attendee__c> deleteUsers = [
				SELECT Id 
				FROM Campaign_Transportation_Attendee__c
				WHERE Campaign_Transportation__c = :cm.Id 
				AND User__c != NULL
				AND Contact__c = NULL
			];

			if (!deleteUsers.isEmpty()) {
				delete deleteUsers;
			}
		}
	}
}