@isTest
private class SalesDataEmailNotificationTest {
	
    @isTest
    private static void testSalesDataEmailNotification () {
    	List<User> users = TestDataFactory.createUsers(2);

    	UserRole ur = new UserRole(
            Name = 'BD Test SubRole',
            ParentRoleId = users[0].UserRoleId
        );
        insert ur;

        users[1].UserRoleId = ur.Id;
        update users;

        Integer emailsBefore = Limits.getEmailInvocations();

        System.runAs(users[0]){
        	List<Sales__c> salesList = TestDataFactory.createSales(3);
        	List<EmailTemplate> eTemplates = TestDataFactory.createEmailTemplates(2);

	        Sales_Data_Email_Notification__c settings = new Sales_Data_Email_Notification__c(
	        										Email_Template_Id__c = eTemplates[0].Id,
	        										Email_Template_Id_For_List_of_Emails__c = eTemplates[1].Id,
	        										Role_And_Below_Id__c= users[0].UserRoleId,
	        										Sales_Threshold__c = 3,
                                                    Limit_Hour__c= 23,
                									Last_Notification_Sent__c = Date.today().addDays(-1)
	        										);
	        insert settings;
        }

        Test.startTest();
	        ScheduledSalesDataEmailNotification ssden = new ScheduledSalesDataEmailNotification();
	        String CRON_EXP = '0 0 7 * * ? ' + (Date.today().year() + 1);
	        String jobId = System.schedule('SalesDataEmailNotificationTest', CRON_EXP, ssden);
	        System.runAs(users[0]){
	        	ssden.execute(NULL);
	        }
        Test.stopTest();
    }
}