@isTest
public class CampaignLedgerControllerTest {
	@testSetup
	static void createResources() {
		User u = [
			SELECT Id, Name, Phone, Email, Title, IsActive
			FROM User 
			WHERE Email = 'bd@apostletech.com'
		][0]; 

		System.runAs(u) {
			List<Account> accts = TestDataFactory.createAccounts(1);

			List<Contact> proctorContacts = TestDataFactory.createContacts(accts, 1);
			List<Contact> attendeeContacts = TestDataFactory.createContacts(accts, 4);

			List<User> bdUsers = TestDataFactory.createUsers(2);

			List<Campaign> campaigns = TestDataFactory.createCampaigns(accts[0], 1);

			List<CampaignMember> proctorMembers = TestDataFactory.createCampaignMembers(campaigns[0], proctorContacts, 'Proctor');
			List<CampaignMember> attendeeMembers = TestDataFactory.createCampaignMembers(campaigns[0], attendeeContacts, 'Attendee');

			List<Campaign_User__c> userMembers = TestDataFactory.createCampaignUsers(campaigns[0], bdUsers);

			List<Campaign_Meal__c> meals = TestDataFactory.createCampaignMeals(campaigns[0], 1);
			TestDataFactory.createCampaignMealAttendee(meals[0], proctorMembers);
			TestDataFactory.createCampaignMealAttendee(meals[0], attendeeMembers);
			TestDataFactory.createCampaignMealAttendee(meals[0], userMembers);

			List<Campaign_Flight__c> proctorFlights = TestDataFactory.createCampaignFlights(campaigns[0], proctorMembers, 'Attendee');
			TestDataFactory.createCampaignFlightLegs(proctorFlights);
			List<Campaign_Flight__c> attendeeFlights = TestDataFactory.createCampaignFlights(campaigns[0], attendeeMembers, 'Attendee');
			TestDataFactory.createCampaignFlightLegs(attendeeFlights);
			List<Campaign_Flight__c> staffFlights = TestDataFactory.createCampaignFlights(campaigns[0], userMembers, 'BD Staff');
			TestDataFactory.createCampaignFlightLegs(staffFlights);

			List<Campaign_Hotel__c> hotels = TestDataFactory.createCampaignHotels(campaigns[0], 1);
			TestDataFactory.createCampaignHotelAttendees(hotels[0], proctorMembers);
			TestDataFactory.createCampaignHotelAttendees(hotels[0], attendeeMembers);
			TestDataFactory.createCampaignHotelAttendees(hotels[0], userMembers);

			List<Campaign_Incidental__c> incidentals = TestDataFactory.createCampaignIncidental(campaigns[0], 1);
			TestDataFactory.createCampaignIncidentalAttendee(incidentals[0], proctorMembers);
			TestDataFactory.createCampaignIncidentalAttendee(incidentals[0], attendeeMembers);
			TestDataFactory.createCampaignIncidentalAttendee(incidentals[0], userMembers);

			List<Campaign_Transportation__c> trans = TestDataFactory.createCampaignTransportations(campaigns[0], 1);
			TestDataFactory.createCampaignTransportationAttendee(trans[0], proctorMembers);
			TestDataFactory.createCampaignTransportationAttendee(trans[0], attendeeMembers);
			TestDataFactory.createCampaignTransportationAttendee(trans[0], userMembers);

			List<Campaign_Proctor_Fee__c> fees = TestDataFactory.createCampaignProctorFees(campaigns[0], proctorMembers);

			List<Campaign_Hotel_Attendee__c> proctorHotels = TestDataFactory.createCampaignHotelAttendees(hotels[0], proctorMembers);
			List<Campaign_Hotel_Attendee__c> attendeeHotels =TestDataFactory.createCampaignHotelAttendees(hotels[0], attendeeMembers);
			List<Campaign_Hotel_Attendee__c> userHotels =TestDataFactory.createCampaignHotelAttendees(hotels[0], userMembers);

			for(Campaign_Hotel_Attendee__c ph :proctorHotels) {
				ph.Present__c = false;
			}

			for(Campaign_Hotel_Attendee__c ah :attendeeHotels) {
				ah.Present__c = false;
			}

			for(Campaign_Hotel_Attendee__c uh :userHotels) {
				uh.Present__c = false;
			}

			update proctorHotels;
			update attendeeHotels;
			update userHotels;
		}
	}

	@isTest
	static void fetchSummaryTotals_UnitTest() {
		Campaign c = [
			SELECT Id
			FROM Campaign
		][0];

		User u = [
			SELECT Id, Name, Phone, Email, Title, IsActive
			FROM User 
			WHERE Email = 'bd@apostletech.com'
		][0]; 

		System.runAs(u) { 
			Map<string, decimal> summaryMap = CampaignLedgerController.fetchSummaryTotals(c.Id);

			System.assertEquals(7, summaryMap.size());	
		}
	}

	@isTest
	static void fetchDetailTotals_UnitTest() {
		Campaign c = [
			SELECT Id
			FROM Campaign
		][0];

		User u = [
			SELECT Id, Name, Phone, Email, Title, IsActive
			FROM User 
			WHERE Email = 'bd@apostletech.com'
		][0]; 

		System.runAs(u) { 
			Map<string, Map<string, Map<string, decimal>>> detailMap = CampaignLedgerController.fetchDetailTotals(c.Id);

			System.assertEquals(3, detailMap.size());	
		}
	}
}