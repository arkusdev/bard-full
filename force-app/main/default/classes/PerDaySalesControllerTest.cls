@isTest
public class PerDaySalesControllerTest {
    @testSetup
    public static void setup() {
        Test.startTest();
        PerDaySalesTestUtil.createData();
        Test.stopTest();
    }

	@isTest // Test as Sales Executive
    public static void getCalcsAsDM () {
        // Test as SE, year, results in 4 rows (categories cat1, cat2, No Matching IRSA, total)
        // Response should include at least the users created in setup
        // Assert there's no row with category 'No Matching Quota'
        // Assert value results
        Test.startTest();
        User dm = PerDaySalesTestUtil.getDMUser();
        PerDaySalesController.CalculationsResponse response = PerDaySalesController.getCalculations(dm.Id, 'year', true);
        Test.stopTest();
        
        System.assert(response.users.size() >= 2);
        System.assertEquals(4, response.tableData.size());

        Boolean IRSANoMatchingQuotaPassed = true;
        PerDaySalesController.Calculation totalsRow = new PerDaySalesController.Calculation();
        for (PerDaySalesController.Calculation c : response.tableData) {
            if (c.catName.equals('No Matching Quota') && IRSANoMatchingQuotaPassed) IRSANoMatchingQuotaPassed = false;
            if (c.catName.equals('Total')) totalsRow = c;
        }
        System.assert(IRSANoMatchingQuotaPassed);
        // All three rows summed up: (100/10) + (100/10) + 0
        System.assertEquals(20, totalsRow.dailyAvg);
        // 100 + 100
        System.assertEquals(200, totalsRow.mtdSales);
        System.assertEquals(totalsRow.mtdSales - totalsRow.mtdBase, totalsRow.mtdVsBase);
        System.assertEquals(totalsRow.mtdSales - totalsRow.mtdQuota, totalsRow.mtdVsQuota);
        // (100/10*30) + (100/10*30) + 0
        System.assertEquals(600, totalsRow.projection);
    }

    @isTest // Test as Sales Executive
    public static void getCalcsAsTM () {
        // Test as BCSM, year, results in 2 rows (categories cat2, total)
        // Response should include at least the user created in setup
        // Assert value results
        Test.startTest();
        User tm = PerDaySalesTestUtil.getTMUser();
        PerDaySalesController.CalculationsResponse response = PerDaySalesController.getCalculations(tm.Id, 'year', true);
        Test.stopTest();

        System.assert(response.users.size() >= 1);
        System.assertEquals(2, response.tableData.size());

        PerDaySalesController.Calculation totalsRow = new PerDaySalesController.Calculation();
        for (PerDaySalesController.Calculation c : response.tableData) {
            if (c.catName.equals('Total')) totalsRow = c;
        }

        // All three rows summed up: (100/10) + 0
        System.assertEquals(10, totalsRow.dailyAvg);
        // 100
        System.assertEquals(100, totalsRow.mtdSales);
        System.assertEquals(totalsRow.mtdSales - totalsRow.mtdBase, totalsRow.mtdVsBase);
        System.assertEquals(totalsRow.mtdSales - totalsRow.mtdQuota, totalsRow.mtdVsQuota);
        // (100/10*30)
        System.assertEquals(300, totalsRow.projection);
    }

    @isTest
    public static void getCalcsQuarter () {
        // Test support for quarters
        Test.startTest();
        PerDaySalesController.CalculationsResponse response = PerDaySalesController.getCalculations(UserInfo.getUserId(), 'quarter', true);
        Test.stopTest();
        System.assertNotEquals(NULL, response);
    }

    @isTest
    public static void getCalcsMonth () {
        // Test support for months
        Test.startTest();
        PerDaySalesController.CalculationsResponse response = PerDaySalesController.getCalculations(UserInfo.getUserId(), 'month', true);
        Test.stopTest();
        System.assertNotEquals(NULL, response);
    }

    @isTest
    public static void getCalcsException () {
        String exType = '';

        try {
            PerDaySalesController.getCalculations(UserInfo.getUserId(), 'invalid_period', true);
        } catch (Exception ex) {
            exType = ex.getTypeName();
        }
        
        System.assertEquals('System.AuraHandledException', exType);
    }
    
    @isTest
    public static void noAccessToObjectException () {
        String exType = '';
        String exMessage = '';
        User bcsm = PerDaySalesTestUtil.getTMUser();
        Test.startTest();
        bcsm.FirstName = 'noAccessToObjectException';
        update bcsm;
        System.runAs(bcsm) {
            try {
                PerDaySalesController.getCalculations(bcsm.Id, 'month', true);
            } catch (AuraHandledException ex) {
                exType = ex.getTypeName();
                exMessage = ex.getMessage();
            }

            System.assertEquals('System.AuraHandledException', exType);
            System.assert(exMessage.contains('Some objects or fields are not accessible'));
        }
        Test.stopTest();
    }

    @isTest
    public static void noAccessToFieldException () {
        String exType = '';
        String exMessage = '';
        User bcsm = PerDaySalesTestUtil.getTMUser();
        Test.startTest();
        bcsm.FirstName = 'noAccessToFieldException';
        update bcsm;
        System.runAs(bcsm) {
            try {
                PerDaySalesController.getCalculations(bcsm.Id, 'month', true);
            } catch (AuraHandledException ex) {
                exType = ex.getTypeName();
                exMessage = ex.getMessage();
            }

            System.assertEquals('System.AuraHandledException', exType);
            System.assert(exMessage.contains('Some objects or fields are not accessible'));
        }
        Test.stopTest();
    }

    @isTest
    public static void getParentRoles () {
        Set<Id> roleIds = new Set<Id>();
        User bcsm = PerDaySalesTestUtil.getTMUser();
        roleIds.add(bcsm.UserRoleId);

        Test.startTest();
        Set<Id> result = PerDaySalesUtil.getParentRoleId(roleIds);
        Test.stopTest();
        
        System.assert(result.size() > 0);
    }
    
    @isTest
    public static void testHolidaysUtil () {
        Date activity = Date.today().addMonths(-1);
        Date startDate = Date.today().toStartOfMonth().addYears(-1);
        Date endDate = Date.today();
        String monthOfYear;
        for (String key : HolidaysUtil.getMonthMap().keyset()) {
            if (HolidaysUtil.getMonthMap().get(key) == activity.month()) monthOfYear = key;
        }
        Holiday h = new Holiday(
            ActivityDate=activity,
            RecurrenceStartDate=activity,
            RecurrenceDayOfMonth=1,
            RecurrenceDayOfWeekMask=1,
            RecurrenceMonthOfYear=monthOfYear,
            RecurrenceInterval=10,
            RecurrenceInstance='First'
        );

        Map<Date, Holiday> mapresult = HolidaysUtil.getHolidaysForDates(startDate, endDate, false);
        System.assertNotEquals(NULL, mapresult);

        List<Date> result = HolidaysUtil.getMonthlyHoliday(h, startDate, endDate);
        System.assert(result.size() > 0);

        result = HolidaysUtil.getMonthlyNthHoliday(h, startDate, endDate);
        System.assert(result.size() > 0);
        
        h.RecurrenceInstance = 'Last';
        result = HolidaysUtil.getMonthlyNthHoliday(h, startDate, endDate);
        System.assert(result.size() > 0);
        
        result = HolidaysUtil.getWeeklyHoliday(h, startDate, endDate);
        System.assert(result.size() > 0);
        
        result = HolidaysUtil.getDailyHoliday(h, startDate, endDate);
        System.assert(result.size() > 0);
        
        List<Integer> bits = HolidaysUtil.seperateBitMask(64);
        system.assertEquals(64, bits.get(0));
    }
}