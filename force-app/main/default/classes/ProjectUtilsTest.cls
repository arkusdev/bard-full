@IsTest
private class ProjectUtilsTest {
    
    static testMethod void AddProjectRolesTest() {

        Schema.DescribeFieldResult fieldResult = ODIN_Role_Assignment_Mapping__mdt.Product__c.getDescribe();
		String productValue = fieldResult.getPicklistValues()[0].getLabel();

        Project__c project = new Project__c();
        project.Platform_Disease_State__c = productValue;
        project.Name = 'Test_1';

        Test.startTest();
            insert project;
        Test.stopTest();

        Set<Id> activeUsers = new Map<Id, User>([SELECT Id FROM User WHERE IsActive = TRUE]).keySet();

        List<ODIN_Role_Assignment_Mapping__mdt> mappingList = [select   Region__c, 
                                                                        User_ID__c 
                                                                from ODIN_Role_Assignment_Mapping__mdt 
                                                                where Product__c =: productValue
                                                                and User_ID__c != null
                                                                and User_ID__c IN: activeUsers
                                                                and User_ID__c !=: UserInfo.getUserId()];

        List<AggregateResult> roleList = [select User__c, Country__c, Region__c from Project_Role__c where Project__c =: project.Id GROUP BY User__c, Country__c, Region__c];

        system.assertEquals(11, roleList.size());
    }

    static testMethod void ChangeLaunchYearFinancialsTest() {
        Project__c project = new Project__c();
        project.Name = 'test_2';
        project.Launch_Year__c = String.valueOf(Date.today().year());
        insert project;

        List<Proposed_Product__c> productList = new List<Proposed_Product__c>();
        Proposed_Product__c product = new Proposed_Product__c();
        product.Name = 'test_3';
        product.Project__c = project.Id;
        productList.add(product);

        Proposed_Product__c product2 = new Proposed_Product__c();
        product2.Name = 'test2';
        product2.Project__c = project.Id;
        productList.add(product2);

        insert productList;

        RecordType rtPr = [select Id from RecordType where SObjectType = 'Financial__c' and RecordType.DeveloperName = 'Product'];
        RecordType rtGMA = [select Id from RecordType where SObjectType = 'Financial__c' and RecordType.DeveloperName = 'Global_Market_Assessment'];
        RecordType rtGF = [select Id from RecordType where SObjectType = 'Financial__c' and RecordType.DeveloperName = 'Global_Financial'];

        List<Financial__c> financialList = new List<Financial__c>();
        for (Integer i = 0; i < 15; i++) {
            Financial__c financial = new Financial__c();
            financial.Project__c = project.Id;
            financial.Proposed_Product__c = product.Id;
            financial.RecordTypeId = rtPr.Id;
            financial.Year__c = String.valueOf(Integer.valueOf(project.Launch_Year__c) + i);
            financialList.add(financial);
        }
        insert financialList;

        List<Financial__c> globalFinancialList = new List<Financial__c>();
        for (Financial__c financial : financialList) {
            Financial__c financial2 = new Financial__c();
            financial2.Project__c = project.Id;
            financial2.Proposed_Product__c = product.Id;
            financial2.Country__c = 'Canada';
            financial2.Region__c = 'Canada';
            financial2.Year__c = financial.Year__c;
            financial2.Units__c = 10;
            financial2.ASP__c = 20;
            financial2.Product__c = financial.Id;
            financial2.RecordTypeId = rtGF.Id;
            globalFinancialList.add(financial2);
        }
        insert globalFinancialList;

        financialList = new List<Financial__c>();
        for (Integer i = 0; i < 15; i++) {

            Financial__c financial2 = new Financial__c();
            financial2.Project__c = project.Id;
            financial2.Proposed_Product__c = product2.Id;
            financial2.RecordTypeId = rtPr.Id;
            financial2.Year__c = String.valueOf(Integer.valueOf(project.Launch_Year__c) + i);
            financialList.add(financial2);
        }
        insert financialList;

        globalFinancialList = new List<Financial__c>();
        for (Financial__c financial : financialList) {
            Financial__c financial2 = new Financial__c();
            financial2.Project__c = project.Id;
            financial2.Proposed_Product__c = product2.Id;
            financial2.Country__c = 'United States';
            financial2.Region__c = 'United States';
            financial2.Year__c = financial.Year__c;
            financial2.Units__c = 10;
            financial2.ASP__c = 20;
            financial2.Product__c = financial.Id;
            financial2.RecordTypeId = rtGF.Id;
            globalFinancialList.add(financial2);
        }
        insert globalFinancialList;

        financialList = new List<Financial__c>();
        for (Integer i = 0; i < 5; i++) {
            Financial__c financial = new Financial__c();
            financial.Project__c = project.Id;
            financial.RecordTypeId = rtGMA.Id;
            financial.Region__c = 'Canada';
            financial.Year__c = String.valueOf(Integer.valueOf(project.Launch_Year__c) + i);
            financialList.add(financial);

            Financial__c financial2 = new Financial__c();
            financial2.Project__c = project.Id;
            financial2.RecordTypeId = rtGMA.Id;
            financial2.Region__c = 'United States';
            financial2.Year__c = String.valueOf(Integer.valueOf(project.Launch_Year__c) + i);
            financialList.add(financial2);
        }
        insert financialList;

        Test.startTest();
            project.Launch_Year__c = String.valueOf(Date.today().year() + 1);
            update project;
        Test.stopTest();

        List<Financial__c> financialGMAList = [select   Year__c,
                                                        Region__c 
                                                from Financial__c 
                                                where Project__c =: project.Id 
                                                and RecordTypeId =: rtGMA.Id 
                                                ORDER BY Region__c, Year__c ASC];
        List<Financial__c> financialPrList = [select    Year__c,
                                                        Proposed_Product__c
                                                from Financial__c 
                                                where Project__c =: project.Id 
                                                and RecordTypeId =: rtPr.Id 
                                                and Proposed_Product__c =: product.Id
                                                ORDER BY Proposed_Product__c, Year__c ASC];   
        Integer count = 0;
        String previousRegion;
        for (Financial__c financial : financialGMAList) {
            if (!String.IsBlank(previousRegion) && previousRegion != financial.Region__c) 
                count = 0;

            system.assertEquals(String.valueOf(Integer.valueOf(project.Launch_Year__c) + count), financial.Year__c);
            count++;

            previousRegion = financial.Region__c;
        }

        count = 0;

        Id previousProduct;
        for (Financial__c financial : financialPrList) {
            if (!String.IsBlank(previousProduct) && previousProduct != financial.Proposed_Product__c) 
                count = 0;

            system.assertEquals(String.valueOf(Integer.valueOf(project.Launch_Year__c) + count), financial.Year__c);
            count++;

            previousProduct = financial.Proposed_Product__c;
        }
    }
}