public class SalesDataEmailNotificationUtils {
    
    public static Set<ID> getAllSubRoleIds(Set<ID> roleIds) {

        Set<ID> currentRoleIds = new Set<ID>();
        // get all of the roles underneath the passed roles
        for(UserRole userRole :[SELECT Id FROM UserRole WHERE ParentRoleId IN :roleIds AND ParentRoleID != null]) {
            currentRoleIds.add(userRole.Id);
        }
        // recursive
        if(currentRoleIds.size() > 0) {
            currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds));
        }
        return currentRoleIds;
    }

    public static Messaging.SingleEmailMessage CreateEmail(Id contactId, String emailAddress, String templateId){
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where DisplayName = 'SFDC Updated'];
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setTemplateId(templateId);
        email.setToAddresses(new String[] {emailAddress});
        if(contactId == null){
            email.setTargetObjectId(UserInfo.getUserId());
            email.setSaveAsActivity(false);
            email.setTreatTargetObjectAsRecipient(false);
        }else{
            email.setTargetObjectId(contactId);
        }
        if (!owea.isEmpty()){
            email.setOrgWideEmailAddressId(owea.get(0).Id);
        }
        return email;
    }
}