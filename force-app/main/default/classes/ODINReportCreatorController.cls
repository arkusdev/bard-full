public with sharing class ODINReportCreatorController {
    
    public String               reportId        { get; set; }
    public String               projectId       { get; set; }
    public List<String>         columnHeaders   { get; set; }
    public List<List<String>>   matrix          { get; set; }
    public List<Filter>         filters         { get; set; }
    
    public ODINReportCreatorController() {
        this.reportId   = ApexPages.currentPage().getParameters().get('reportId');
        this.projectId  = ApexPages.currentPage().getParameters().get('projectId');
        this.GetReport();
    }

    @AuraEnabled
    public static List<Report> GetReportList() {
        List<Id> repordIds = new List<Id>();
        for (ODIN_Report_Setting__mdt mdt : [select ReportID__c, 
                                                    Testing__c 
                                            from ODIN_Report_Setting__mdt]) {
            if (Test.isRunningTest()) {
                if (mdt.Testing__c)
                    repordIds.add(mdt.ReportID__c);
            } else {
                if (!mdt.Testing__c)
                    repordIds.add(mdt.ReportID__c);
            }
        }
        return [select Id, Name from Report where Id IN: repordIds];
    }

    private void GetReport() {

        Reports.ReportDescribeResult describe = Reports.ReportManager.describeReport(reportId);
        
        Reports.ReportMetadata reportMd = describe.getReportMetadata();

        this.filters = new List<Filter>();
        for (Reports.ReportFilter filter : reportMd.getReportFilters()) {
            
            Filter innerFilter = new Filter();
            innerFilter.operator = filter.getOperator();
            innerFilter.value = filter.getValue();
            
            if (filter.getColumn() == 'FK_CUSTENT_ID' || filter.getColumn() == 'Financial__c.Project__c') {
                
                filter.setValue(projectId);
                innerFilter.value = projectId;
                innerFilter.column = 'Financial__c.Project__c';
            
            } else if (filter.getColumn() == 'CUST_RECORDTYPE') {
            
                innerFilter.column = 'Financial__c.RecordType';
            } else {
            
                innerFilter.column = filter.getColumn();
            }
            this.filters.add(innerFilter);
        }

        Reports.ReportResults results = Reports.ReportManager.runReport(reportId, reportMd);
        
        Map<String, Reports.ReportFact> factSumMap = results.getFactMap();


        Reports.ReportExtendedMetadata metadata = results.getReportExtendedMetadata();

        Map<String, Reports.AggregateColumn> columns = metadata.getAggregateColumnInfo();

        columnHeaders = new List<String>();
        
        for (Reports.GroupingInfo gi : reportMd.getGroupingsDown()){ 
            columnHeaders.add(metadata.getGroupingColumnInfo().get(gi.getName()).getLabel());
            System.debug('gi grouping down: ' + metadata.getGroupingColumnInfo().get(gi.getName()).getLabel());
        }
        for (Reports.GroupingInfo gi : reportMd.getGroupingsAcross()) {
            columnHeaders.add(metadata.getGroupingColumnInfo().get(gi.getName()).getLabel() + ' -->');
            System.debug('gi grouping across: ' + metadata.getGroupingColumnInfo().get(gi.getName()).getLabel());
        }
        
        List<Reports.GroupingValue> groupingAcross = results.getGroupingsAcross().getGroupings();
        for (Reports.GroupingValue gv : groupingAcross){
            columnHeaders.add(gv.getLabel());
            System.debug('gv.getLabel(): ' + gv.getLabel());
        }

        columnHeaders.add('Total');    

        matrix = new List<List<String>>();

        List<String> aggregates = reportMd.getAggregates();

        Boolean oneLevel = false;

        for (Reports.GroupingValue gv1 : results.getGroupingsDown().getGroupings()) {
            System.debug('gv1: ' + gv1.getLabel());
            List<Reports.GroupingValue> groupings1 = gv1.getGroupings();
            System.debug('groupings1: ' + groupings1);
            if (groupings1 != null && !groupings1.isEmpty()) {
                
                for (Reports.GroupingValue gv2 : groupings1) {
                    Integer i = 0;
                    for (String aggregate : aggregates) {
                        
                        List<String> rowList = new List<String>();
                        rowList.add(gv1.getLabel());
                        rowList.add(gv2.getLabel());
                        rowList.add(columns.get(aggregate).getLabel());

                        
                        String key;
                        for (Integer j = 0; j < groupingAcross.size(); j++) {

                            key = gv2.getKey() + '!' + j;
                            if (factSumMap.containsKey(key) && factSumMap.get(key).getAggregates()[i] != null)
                                rowList.add(factSumMap.get(key).getAggregates()[i].getLabel());
                            
                        }
                        key = gv2.getKey() + '!T';
                        if (factSumMap.containsKey(key) && factSumMap.get(key).getAggregates()[i] != null)
                            rowList.add(factSumMap.get(key).getAggregates()[i].getLabel());
                        
                        i++;
                        matrix.add(rowList);
                        
                    }

                }

                Integer i = 0;
                    
                for (String aggregate : aggregates) {
                    
                    List<String> rowList = new List<String>();
                    rowList.add(gv1.getLabel());
                    rowList.add('Subtotal');
                    rowList.add(columns.get(aggregate).getLabel());

                    
                    String key;
                    for (Integer j = 0; j < groupingAcross.size(); j++) {

                        key = gv1.getKey() + '!' + j;
                        if (factSumMap.containsKey(key) && factSumMap.get(key).getAggregates()[i] != null)
                            rowList.add(factSumMap.get(key).getAggregates()[i].getLabel());
                        
                    }
                    key = gv1.getKey() + '!T';
                    if (factSumMap.containsKey(key) && factSumMap.get(key).getAggregates()[i] != null)
                        rowList.add(factSumMap.get(key).getAggregates()[i].getLabel());
                    
                    i++;
                    matrix.add(rowList);
                    
                }
            } else {
                oneLevel = true;
                Integer i = 0;
                    
                for (String aggregate : aggregates) {
                    
                    List<String> rowList = new List<String>();
                    rowList.add(gv1.getLabel());
                    rowList.add(columns.get(aggregate).getLabel());
                    System.debug('gv1.getLabel(): ' + gv1.getLabel());
                    System.debug('columns.get(aggregate).getLabel(): ' + columns.get(aggregate).getLabel());

                    
                    String key;
                    for (Integer j = 0; j < groupingAcross.size(); j++) {
                        key = gv1.getKey() + '!' + j;
                        if (factSumMap.containsKey(key) && factSumMap.get(key).getAggregates()[i] != null){
                            rowList.add(factSumMap.get(key).getAggregates()[i].getLabel());
                            System.debug('factSumMap!: ' + factSumMap.get(key).getAggregates()[i].getLabel());
                        }
                        
                    }
                    key = gv1.getKey() + '!T';
                    if (factSumMap.containsKey(key) && factSumMap.get(key).getAggregates()[i] != null){
                        rowList.add(factSumMap.get(key).getAggregates()[i].getLabel());
                        System.debug('factSumMap!T: ' + factSumMap.get(key).getAggregates()[i].getLabel());
                    }
                    
                    i++;
                    matrix.add(rowList);
                    
                }
            }


        }

        Integer i = 0;
                
        for (String aggregate : aggregates) {
            
            List<String> rowList = new List<String>();
            rowList.add('Total');
            if (!oneLevel)
                rowList.add('');
            rowList.add(columns.get(aggregate).getLabel());

            
            String key;
            for (Integer j = 0; j < groupingAcross.size(); j++) {

                key = 'T!' + j;
                if (factSumMap.containsKey(key) && factSumMap.get(key).getAggregates()[i] != null){
                    rowList.add(factSumMap.get(key).getAggregates()[i].getLabel());
                    System.debug('T!factSumMap.get(key).getAggregates()[i].getLabel(): ' + factSumMap.get(key).getAggregates()[i].getLabel());
                }
                
            }
            key = 'T!T';
            if (factSumMap.containsKey(key) && factSumMap.get(key).getAggregates()[i] != null){
                rowList.add(factSumMap.get(key).getAggregates()[i].getLabel());
                System.debug('T!TfactSumMap.get(key).getAggregates()[i].getLabel(): ' + factSumMap.get(key).getAggregates()[i].getLabel());
            }
            
            i++;
            matrix.add(rowList);
            
        }

    }

    public class Filter {
        public String column    { get; set; }
        public String operator  { get; set; }
        public String value     { get; set; }
    }

}