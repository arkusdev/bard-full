public class CampaignFlightTriggerHandler extends TriggerHandler {
/* overrides */
	protected override void afterInsert() {
		SynchCampaignCost();
	}

	protected override void afterUpdate() {
		SynchCampaignCost();
	}

	protected override void afterDelete() {
		SynchCampaignCost();
	}

	/* private methods */
	private void SynchCampaignCost() {
		Set<Id> campaignIds = new Set<Id>();

		if (trigger.isInsert) {
			for(Campaign_Flight__c item : (List<Campaign_Flight__c>)trigger.new) {
				campaignIds.add(item.Campaign__c);
			}
		} else if (trigger.isUpdate) {
            for(Id item :((Map<Id, Campaign_Flight__c>)trigger.newMap).keySet()) {
				Campaign_Flight__c obj = ((Map<Id, Campaign_Flight__c>)trigger.newMap).get(item);

                if (((Map<Id, Campaign_Flight__c>)trigger.oldMap).get(item).Cost__c != obj.Cost__c) {
					campaignIds.add(obj.Campaign__c);
                }
            } 			
		} else {
			for(Campaign_Flight__c item : (List<Campaign_Flight__c>)trigger.old) {
				campaignIds.add(item.Campaign__c);
			}			
		}

		if(!campaignIds.isEmpty()) {
			CampaignTriggerUtil.UpdateCampaignCost(campaignIds);
		}
	}
}