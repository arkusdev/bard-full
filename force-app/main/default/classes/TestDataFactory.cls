@isTest
public class TestDataFactory  {
    public static List<Account> createAccounts(integer numAccts) {
        List<Account> accts = new List<Account>();

        for(integer i = 0; i <= numAccts - 1; i++) {
            accts.add(new Account(
                Name = 'Test Account # ' + string.valueOf(i + 1),
                JDE_Facility_Type__c = 'Hospital'
            ));
        }

        insert accts;

        return accts;
    }

    public static List<Contact> createContacts(List<Account> accts, integer contactsPerAcct) {
        List<Contact> contacts = new List<Contact>();

        for(Account acct :accts) {
            for(integer i = 0; i <= contactsPerAcct - 1; i++) {
                contacts.add(new Contact(
                    LastName = 'LastName' + (string.valueOf(i + 1) + string.valueOf(contactsPerAcct)),
                    FirstName = 'FirstName' + (string.valueOf(i + 1) + string.valueOf(contactsPerAcct)),
                    AccountId = acct.Id,
                    Email = 'email' + (string.valueOf(i + 1) + string.valueOf(contactsPerAcct)) + '@test.com',
                    Phone = '5555555555' + (string.valueOf(i + 1) + string.valueOf(contactsPerAcct)),
                    MobilePhone = '5555555555' + (string.valueOf(i + 1) + string.valueOf(contactsPerAcct)),
                    Contact_Photo__c = null,
                    Contact_Bio__c = 'Test contact bio for test contact # ' + (string.valueOf(i + 1) + string.valueOf(contactsPerAcct)),
                    Department__c = 'CARDIOLOGY',
                    Surgeon_Specialty__c = 'CARDIOVASCULAR DISEASE', // 'Cardiologist',
                    Surgical_Technique__c = 'CTO',
                    Is_KOL__c = true
                ));
            }
        }
        insert contacts;

        return contacts;
    }

    public static List<User> createUsers(integer numUsers) {
        Profile p = [SELECT Id FROM Profile  WHERE Name  = 'Bard - Physician Training'];

        UserRole ur = new UserRole(
            Name = 'BD Test Role'
        );

        insert ur;

        string orgId = UserInfo.getOrganizationId();
        string dateString = string.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        integer randomInt = integer.valueOf(math.rint(math.random()*1000000));
        string uniqueName = orgId + dateString + randomInt;

        List<User> users = new List<User>();

        for(integer i = 0; i <= numUsers - 1; i++) {
            User u = new User(
                FirstName = 'FirstName' + string.valueOf(i + 1),
                LastName = 'LastName' + string.valueOf(i + 1),
                Alias = uniqueName.substring(18, 23),
                Email = uniqueName + string.valueOf(i + 1) + '@test' + orgId + '.com',
                Username = uniqueName + string.valueOf(i + 1) + '@test' + orgId + '.com',
                CommunityNickname = uniqueName + string.valueOf(i + 1),
                EmailEncodingKey = 'ISO-8859-1',
                ProfileId = p.Id,
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                UserRoleId = ur.Id
            );
            users.add(u);
        }
        insert users;

        return users;
    }

    public static List<Contract> createContracts(List<Contact> contacts, integer contractsPerContact, boolean otherFlg) {
        List<Contract> contracts = new List<Contract>();

        for(Contact con :contacts) {
            for(integer i = 0; i <= contractsPerContact - 1; i++) {
                contracts.add(new Contract(
                    Contact__c = con.Id,
                    Status = 'Draft',
                    AccountId = con.AccountId,
                    Contract_Services__c = (otherFlg ? 'Other' : 'Speaking'),
                    Contract_Services_Description__c = (otherFlg ? 'Test Description' : ''),
                    StartDate = Date.today(),
                    ContractTerm = 2
                ));
            }
        }

        insert contracts;

        return contracts;
    }   

    public static void createDivisions(List<Contact> contacts, integer divsPerContact) {
        List<Division__c> divs = new List<Division__c>();

        for(Contact con :contacts) {
            for(integer i = 0; i <= divsPerContact - 1; i++) {
                divs.add(new Division__c(
                    Contact__c = con.Id,
                    Division__c = 'Biopsy',
                    Product_Category__c = 'Capital'
                ));
            }
        }

        insert divs;
    }

    public static void createContactLicenses(List<Contact> contacts, integer licPerContact) {
        Licensed_Region__c region = new Licensed_Region__c(
            Name = 'Mountain'
        );

        insert region;
        
        Licensed_State__c state = new Licensed_State__c(
            Licensed_Region__c = region.Id,
            Name = 'Arizona',
            State_Abbreviation__c = 'AZ'
        );

        insert state;

        List<Contact_License__c> lics = new List<Contact_License__c>();

        for(Contact con :contacts) {
            for(integer i = 0; i <= licPerContact - 1; i++) {
                lics.add(new Contact_License__c(
                    Contact__c = con.Id,
                    Licensed_State__c = state.Id,
                    Name = 'Test License #' + string.valueOf(i + 1),
                    Expiration_Date__c = Date.today().addMonths(2)              
                ));
            }
        }

        insert lics;
    }

    public static List<Campaign> createCampaigns(Account acct, integer numCampaigns) {
        List<Campaign> campaigns = new List<Campaign>();

        for(integer i = 0; i <= numCampaigns - 1; i++) {
            campaigns.add(new Campaign(
                Name = 'Test Campaign # ' + string.valueOf(i + 1),
                Facility__c = acct.Id,
                Technique__c = 'CTO',
                Classification__c = 'Biopsy',
                Product_Category__c = 'Capital',
                SubLedger__c = '12345',
                Available_Seats__c = 4,
                Status = 'Registrations Open',
                StartDate = system.today(),
                EndDate = System.today().addDays(2)
            ));
        }

        insert campaigns;

        return campaigns;   
    }

    public static List<Campaign_Document__c> createCampaignDocuments(Campaign camp, integer numDoc) {
        List<Campaign_Document__c> docs = new List<Campaign_Document__c>();

        for(integer i = 0; i <= numDoc - 1; i++) {
            docs.add(new Campaign_Document__c(
                Campaign__c = camp.Id,
                Name = 'Test Doc #' + string.valueOf(i + 1)
            ));
        }

        insert docs;

        return docs;
    }

    public static List<Campaign_Attendee_Document__c> createCampaignMemberDocuments(Campaign camp, List<CampaignMember> members) {
        List<Campaign_Attendee_Document__c> memberDocs = new List<Campaign_Attendee_Document__c>();
        
        for(CampaignMember cm :members) {
            for(Campaign_Document__c cd :camp.Campaign_Documents__r) {
                memberDocs.add(new Campaign_Attendee_Document__c(
                    Campaign_Document__c = cd.Id,
                    Contact__c = cm.ContactId,
                    Received__c = false
                ));         
            }
        }
        
        insert memberDocs;
        
        return memberDocs;  
    }

    public static List<Campaign_Attendee_Document__c> createCampaignUserDocuments(Campaign camp, List<Campaign_User__c> users) {
        List<Campaign_Attendee_Document__c> userDocs = new List<Campaign_Attendee_Document__c>();
        
        for(Campaign_User__c cu :users) {
            for(Campaign_Document__c cd :camp.Campaign_Documents__r) {
                userDocs.add(new Campaign_Attendee_Document__c(
                    Campaign_Document__c = cd.Id,
                    User__c = cu.User__c,
                    Received__c = false
                ));         
            }
        }
        
        insert userDocs;
        
        return userDocs;        
    }

    public static List<CampaignMember> createCampaignMembers(Campaign camp, List<Contact> contacts, string type) {
        List<CampaignMember> cms = new List<CampaignMember>();

        for(Contact c :contacts) {
            cms.add(new CampaignMember(
                CampaignId = camp.Id,
                ContactId = c.Id,
                Type__c = type
            ));                 
        }

        insert cms;

        return cms;
    }

    public static List<Campaign_User__c> createCampaignUsers(Campaign camp, List<User> users) {
        List<Campaign_User__c> cus = new List<Campaign_User__c>();

        for(User u :users) {
            cus.add(new Campaign_User__c(
                Name = string.valueOf(camp.Id) + ' - ' + string.valueOf(u.Id),
                Campaign__c = camp.Id,
                User__c = u.Id
            ));
        }

        insert cus;

        return cus; 
    }
    
    public static List<Campaign_Meal__c>  createCampaignMeals(Campaign camp, integer numCampMeals) {
        List<Campaign_Meal__c> cMeals = new List<Campaign_Meal__c>();
        
        for(integer i = 0; i <= numCampMeals - 1; i++) {
            cMeals.add(new Campaign_Meal__c(
                Name = 'Test Campaign Meal# ' + string.valueOf(i + 1),
                Campaign__c = camp.Id,
                Location_Name__c = 'Texas',
                Total_Cost__c = 50.00,
                Meal_Date__c = System.Today(),
                Type__c = 'Lunch',
                Additional_Attendees__c = 2
            ));
        }

        insert cMeals;
        
        return cMeals;
    }
    
    public static List<Campaign_Meal_Attendee__c> createCampaignMealAttendee(Campaign_Meal__c meal, List<CampaignMember> members) {
        List<Campaign_Meal_Attendee__c> attendees = new List<Campaign_Meal_Attendee__c>();
        
        for(CampaignMember cm :members) {
            attendees.add(new Campaign_Meal_Attendee__c(
                Name = cm.Contact.Name,
                Campaign_Meal__c = meal.Id,
                Contact__c = cm.ContactId
            ));
        }
        
        insert attendees;
        
        return attendees;
    }

    public static List<Campaign_Meal_Attendee__c> createCampaignMealAttendee(Campaign_Meal__c meal, List<Campaign_User__c> users) {
        List<Campaign_Meal_Attendee__c> attendees = new List<Campaign_Meal_Attendee__c>();
        
        for(Campaign_User__c cu :users) {
            attendees.add(new Campaign_Meal_Attendee__c(
                Name = cu.User__r.Name,
                Campaign_Meal__c = meal.Id,
                User__c = cu.User__c
            ));
        }
        
        insert attendees;
        
        return attendees;
    }
    
    public static List<Campaign_Flight__c> createCampaignFlights(Campaign camp, List<CampaignMember> members, string type) {
        List<Campaign_Flight__c> flights = new List<Campaign_Flight__c>();
        
        for(CampaignMember cm :members) {
            flights.add(new Campaign_Flight__c(
                Name = 'Flight for ' + cm.Contact.Name,
                Campaign__c = camp.Id,
                Cost__c = 50.00,
                Type__c = type,
                Contact__c = cm.ContactId
            ));
        }
        
        insert flights;

        return flights;
    }

    public static List<Campaign_Flight__c> createCampaignFlights(Campaign camp, List<Campaign_User__c> users, string type) {
        List<Campaign_Flight__c> flights = new List<Campaign_Flight__c>();
        
        for(Campaign_User__c u :users) {
            flights.add(new Campaign_Flight__c(
                Name = u.User__r.Name,
                Campaign__c = camp.Id,
                Cost__c = 50.00,
                Type__c = type,
                User__c = u.User__c
            ));
        }
        
        insert flights;

        return flights;
    }

    public static List<Campaign_Flight_Leg__c> createCampaignFlightLegs(List<Campaign_Flight__c> flights) {
        List<Campaign_Flight_Leg__c> legs = new List<Campaign_Flight_Leg__c>();

        for(Campaign_Flight__c cf: flights) {
            legs.add(new Campaign_Flight_Leg__c(
                Campaign_Flight__c = cf.Id,
                Name = 'Arrving flight for ' + cf.Name,
                Origin__c = 'Phoenix, AZ',
                Destination__c = 'Los Angeles, CA',
                Depart_Date_Time__c = Datetime.now(),
                Arrive_Date_Time__c = Datetime.now().addHours(1),
                Airline__c = 'Southwest',
                Confirmation_Number__c = '12345',
                Flight_Number__c = '7890',
                Gate__c = 'A4',
                Terminal__c = '4',
                Type__c = 'Arriving',
                Notes__c = ''
            )); 

            legs.add(new Campaign_Flight_Leg__c(
                Campaign_Flight__c = cf.Id,
                Name = 'Departing flight for ' + cf.Name,
                Origin__c = 'Los Angeles, CA',
                Destination__c = 'Phoenix, AZ',
                Depart_Date_Time__c = Datetime.now().addDays(2),
                Arrive_Date_Time__c = Datetime.now().addDays(2).addHours(1),
                Airline__c = 'Southwest',
                Confirmation_Number__c = '12345',
                Flight_Number__c = '7890',
                Gate__c = 'A4',
                Terminal__c = '4',
                Type__c = 'Departing',
                Notes__c = ''
            ));
        }

        insert legs;    

        return legs;
    }
    
    public static List<Campaign_Hotel__c> createCampaignHotels(Campaign camp, integer numCampHotels) {
        List<Campaign_Hotel__c> cHotels = new List<Campaign_Hotel__c>();
        
        for(integer i = 0; i <= numCampHotels - 1; i++) {
            cHotels.add(new Campaign_Hotel__c(
                Name = 'Test Campaign Hotel# ' + string.valueOf(i + 1),
                Campaign__c = camp.Id,
                Room_Block__c = 'Room Blocked',
                Check_In_Date__c = System.today().addDays(1),
                Check_Out_Date__c = System.today().addDays(4)
            ));
        }
        
        insert cHotels;
        
        return cHotels;
    }
    
    public static List<Campaign_Hotel_Attendee__c> createCampaignHotelAttendees(Campaign_Hotel__c hotel, List<CampaignMember> members) {
        List<Campaign_Hotel_Attendee__c> attendees = new List<Campaign_Hotel_Attendee__c>();
        
        for(CampaignMember cm :members) {
            attendees.add(new Campaign_Hotel_Attendee__c(
                Name = cm.Contact.Name,
                Contact__c = cm.ContactId,
                Confirmation_Number__c = '12345',
                Campaign_Hotel__c = hotel.Id,
                Reservation_Date__c = hotel.Check_In_Date__c,
                Cost__c = 150.00,
                Present__c = true
            ));
        }
        
        insert attendees;
        
        return attendees;
    }

    public static List<Campaign_Hotel_Attendee__c> createCampaignHotelAttendees(Campaign_Hotel__c hotel, List<Campaign_User__c> users) {
        List<Campaign_Hotel_Attendee__c> attendees = new List<Campaign_Hotel_Attendee__c>();
        
        for(Campaign_User__c cu :users) {
            attendees.add(new Campaign_Hotel_Attendee__c(
                Name = cu.User__r.Name,
                User__c = cu.User__c,
                Confirmation_Number__c = '12345',
                Campaign_Hotel__c = hotel.Id,
                Reservation_Date__c = hotel.Check_In_Date__c,
                Cost__c = 150.00,
                Present__c = true
            ));
        }
        
        insert attendees;
        
        return attendees;
    }
    
    public static List<Campaign_Transportation__c> createCampaignTransportations(Campaign camp, integer numCampTrans) { 
        List<Campaign_Transportation__c> cTransport = new List<Campaign_Transportation__c>();
        
        for(integer i = 0; i <= numCampTrans - 1; i++) {
            cTransport.add(new Campaign_Transportation__c(
                Name = 'Test Campaign Transportation# ' + string.valueOf(i + 1),
                Campaign__c = camp.Id,
                Pickup_Location__c = 'Pick Location',
                Drop_Off_Location__c = 'Drop Location',
                Pickup_Date_Time__c = System.today().addDays(4),
                Total_Cost__c = 250.00
            ));
        }
        
        insert cTransport;
        
        return cTransport;
    }
    
    public static List<Campaign_Transportation_Attendee__c> createCampaignTransportationAttendee(Campaign_Transportation__c trans, List<CampaignMember> members) {
        List<Campaign_Transportation_Attendee__c> attendees = new List<Campaign_Transportation_Attendee__c>();
        
        for(CampaignMember cm :members) {
            attendees.add(new Campaign_Transportation_Attendee__c(
                Name = cm.Contact.Name,
                Campaign_Transportation__c = trans.Id,
                Contact__c = cm.ContactId
            ));
        }
        
        insert attendees;
        
        return attendees;
    }
        
    public static List<Campaign_Transportation_Attendee__c> createCampaignTransportationAttendee(Campaign_Transportation__c trans, List<Campaign_User__c> users) {
        List<Campaign_Transportation_Attendee__c> attendees = new List<Campaign_Transportation_Attendee__c>();
        
        for(Campaign_User__c cu :users) {
            attendees.add(new Campaign_Transportation_Attendee__c(
                Name = cu.User__r.Name,
                Campaign_Transportation__c = trans.Id,
                User__c = cu.User__c
            ));
        }
        
        insert attendees;
        
        return attendees;
    }
    
    public static List<Campaign_Incidental__c> createCampaignIncidental(Campaign camp, integer numCampInc) {
        List<Campaign_Incidental__c> cIncidental = new List<Campaign_Incidental__c>();
        
        for(integer i = 0; i <= numCampInc - 1; i++) {
            cIncidental.add(new Campaign_Incidental__c(
                Name = 'Test Campaign Incidental# ' + string.valueOf(i + 1),
                Campaign__c = camp.Id,
                Description__c = 'Test Desc',
                Purchase_Date_Time__c = system.Now(),
                Total_Cost__c = 50.00
            ));
        }
        
        insert cIncidental;
        
        return cIncidental;
    }
    
    public static List<Campaign_Incidental_Attendee__c> createCampaignIncidentalAttendee(Campaign_Incidental__c incidental, List<CampaignMember> members) {
        List<Campaign_Incidental_Attendee__c> incidentals = new List<Campaign_Incidental_Attendee__c>();
        
        for(CampaignMember cm :members) {
            incidentals.add(new Campaign_Incidental_Attendee__c(
                Name = cm.Contact.Name,
                Campaign_Incidental__c = incidental.Id,
                Contact__c = cm.ContactId
            ));
        }
        
        insert incidentals;
        
        return incidentals;
    }

    public static List<Campaign_Incidental_Attendee__c> createCampaignIncidentalAttendee(Campaign_Incidental__c incidental, List<Campaign_User__c> users) {
        List<Campaign_Incidental_Attendee__c> incidentals = new List<Campaign_Incidental_Attendee__c>();
        
        for(Campaign_User__c cm :users) {
            incidentals.add(new Campaign_Incidental_Attendee__c(
                Name = cm.User__r.Name,
                Campaign_Incidental__c = incidental.Id,
                User__c = cm.User__c
            ));
        }
        
        insert incidentals;
        
        return incidentals;
    }
    
    public static List<Campaign_Proctor_Fee__c> createCampaignProctorFees(Campaign camp, List<CampaignMember> proctors) {
        List<Campaign_Proctor_Fee__c> cProcFee = new List<Campaign_Proctor_Fee__c>();
        
        for(CampaignMember cm :proctors) {
            cProcFee.add(new Campaign_Proctor_Fee__c(
                Name = cm.Contact.Name + ' Proctor Fee',
                Campaign__c = camp.Id,
                Proctor__c = cm.ContactId,
                Total_Cost__c = 50.00
            ));
        }
        
        insert cProcFee;
        
        return cProcFee;
    }

    public static List<Sales__c> createSales(Integer amount) {
        List<String> recordTypeIds = new List<String>();
        for(Sales_Data_Email_Notification_RecordType__mdt sder: [SELECT RecordType_Id__c FROM Sales_Data_Email_Notification_RecordType__mdt]){
            recordTypeIds.add(sder.RecordType_Id__c);
        }
        String rt = recordTypeIds.size() > 0 ? recordTypeIds[0] : [SELECT Id FROM RecordType WHERE Name = 'IRSA'].Id;
        List<Sales__c> sales = new List<Sales__c>();

        for(integer i = 0; i <= amount - 1; i++) {
            sales.add(new Sales__c(RecordTypeId=rt));
        }

        insert sales;
        return sales;
    }

    public static List<EmailTemplate> createEmailTemplates(Integer amount){
        List<EmailTemplate> emails = new List<EmailTemplate>();
        for(integer i = 0; i <= amount - 1; i++) {
            EmailTemplate eTemplate = new EmailTemplate();
            eTemplate.isActive = true;
            eTemplate.Name = 'name'+i;
            eTemplate.DeveloperName = 'unique_name_'+i;
            eTemplate.TemplateType = 'text';
            eTemplate.FolderId = UserInfo.getUserId();
            emails.add(eTemplate);
        }

        insert emails;
        return emails;
    }
}