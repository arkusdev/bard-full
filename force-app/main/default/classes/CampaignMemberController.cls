public class CampaignMemberController  {
    private static final string proctor_tab = 'proctor';
    private static final string attendee_tab = 'attendee';
    private static final string waitlist_tab = 'waitlist';
    
    @auraEnabled
    public static Boolean hasAvailableSeats(Id campaignId) {
        Boolean availFlg = true;
        
        try {
            Campaign c = [
                SELECT Remaining_Seats__c
                FROM Campaign
                WHERE Id = :campaignId
            ][0];
            
            if (c != null) {
                availFlg = (c.Remaining_Seats__c > 0);
            }
            
        } catch(Exception ex) {
            throw new AuraHandledException('Darn it! Something went wrong: ' + ex.getMessage());  
        }
        
        return availFlg;
    }
    
    @auraEnabled
    public static Map<string, List<PersonDTO>> fetchAllCampaignMembers(id campaignId, string type) {
        //Invoked on CampignMember.cmp init.
        //The Map key corresponds to the aura:attribute being set within the component
        Map<string, List<PersonDTO>> dtoMap = new Map<string, List<PersonDTO>>();
        Set<Id> campaignContactIds = new Set<Id>();
        Set<Id> contactIds = new Set<Id>();
        Set<Id> userIds = new Set<Id>();
        
        try {
            if (!type.equalsIgnoreCase('BD Staff')) { 
                //Fetch a list of contactIds, types, & userIds already associated to the course
                List<CampaignMember> members = [
                    SELECT ContactId, Type__c, Campaign.StartDate, Campaign.EndDate
                    FROM CampaignMember 
                    WHERE CampaignId = :campaignId
                    AND ContactId != NULL
                ];
                
                date startDate;
                date endDate;

                for(CampaignMember m :members) {
                    if (!campaignContactIds.contains(m.ContactId) && !String.isBlank(m.Type__c) && m.Type__c.equalsIgnoreCase(type)) {
                        campaignContactIds.add(m.ContactId);
                    }
                    if (!contactIds.contains(m.ContactId)) {
                        contactIds.add(m.ContactId);
                    }
                    if (startDate == null && endDate == null) {
                        startDate = m.Campaign.StartDate;
                        endDate = m.Campaign.EndDate;
                    }
                }
                
                //First, get a list of contacts already associated to the campaign 
                List<PersonDTO> campaignContactDTOs = getCampaignContacts(campaignContactIds, campaignId, startDate, endDate, type);
                dtoMap.put('campaignMembers', campaignContactDTOs);     
                
                //Next, return the list of contacts to omit
                List<PersonDTO> omitDTOs = new List<PersonDTO>();

                for(Id oc :contactIds) {
                    omitDTOs.add(new PersonDTO(oc, '', true));
                }

                dtoMap.put('omitIdList', omitDTOs);     

            } else {
                List<Campaign_User__c> users = [
                    SELECT User__c
                    FROM Campaign_User__c
                    WHERE Campaign__c = :campaignId
                    AND User__c != NULL
                ];
                
                for(Campaign_User__c u :users) {
                    if (!userIds.contains(u.User__c)) {
                        userIds.add(u.User__c);
                    }
                }
                              
                //First, the users associated to the campaign
                List<PersonDTO> campaignUserDTOs = getCampaignUsers(userIds, campaignId);
                dtoMap.put('campaignMembers', campaignUserDTOs);        
                
                //Next, return the list of users to omit
                List<PersonDTO> omitDTOs = new List<PersonDTO>();

                for(Id ou :userIds) {
                    omitDTOs.add(new PersonDTO(ou, '', true));
                }

                dtoMap.put('omitIdList', omitDTOs);     
            }
            
        } catch(Exception ex) {
            throw new AuraHandledException('Darn it! Something went wrong: ' + ex.getMessage());  
        }
        
        return dtoMap;
    }
    
    private static List<PersonDTO> getCampaignContacts(Set<Id> campaignContactIds, Id campaignId, date startDate, date endDate, string type) {
        List<PersonDTO> dtos = new List<PersonDTO>();   
        
        List<Contact> contacts = [
            SELECT Id, Name, Phone, Email, Contact_Photo__c, Contact_Bio__c, CV_on_File__c, Account.Name,
            (SELECT Contract_Services__c, ContractNumber, StartDate, EndDate FROM Contracts__r) 
            FROM Contact 
            WHERE Id IN :campaignContactIds 
            ORDER BY Name
        ];

        Map<Id, Set<Id>> overlapMap = new Map<Id, Set<Id>>();

        if(startDate != null && endDate != null) {
            List<CampaignMember> overlapList = [
                SELECT ContactId, CampaignId 
                FROM CampaignMember
                WHERE ContactId IN :campaignContactIds AND CampaignId <> :campaignId
                AND ((Campaign.StartDate >= :startDate AND Campaign.StartDate <= :endDate)
                      OR (Campaign.EndDate >= :startDate AND Campaign.EndDate <= :endDate)
                      OR (Campaign.StartDate <= :startDate AND Campaign.EndDate >= :endDate))               
            ];

            for(CampaignMember cm: overlapList) {
                if (!overlapMap.containsKey(cm.ContactId)) {
                    overlapMap.put(cm.ContactId, new Set<Id>{ cm.CampaignId });
                } else {
                    overlapMap.get(cm.ContactId).add(cm.CampaignId);
                }
            }       
        }
        
        Map<Id, Map<Id, Campaign_Attendee_Document__c>> docMap = new Map<Id, Map<Id, Campaign_Attendee_Document__c>>();

        if(type.equalsIgnoreCase('Attendee')) {
            List<Campaign_Attendee_Document__c> docs = [
                SELECT Id, Contact__c, Received__c, Campaign_Document__r.Campaign__r.Credentials_Due_Date__c
                FROM Campaign_Attendee_Document__c
                WHERE Campaign_Document__r.Campaign__c = :campaignId
                AND Contact__c IN :campaignContactIds
                AND User__c = NULL
                ORDER BY Contact__c
            ];

            for(Campaign_Attendee_Document__c cmd: docs) {
                if (!docMap.containsKey(cmd.Contact__c)) {
                    docMap.put(cmd.Contact__c, new Map<Id, Campaign_Attendee_Document__c> { cmd.Id => cmd });
                } else {
                    docMap.get(cmd.Contact__c).put(cmd.Id, cmd);
                }
            }
        }
    
        for(Contact c :contacts) {
            dtos.add(new PersonDTO(c, startDate, endDate, overlapMap, docMap));
        }

        return dtos;
    }
    
    private static List<PersonDTO> getCampaignUsers(Set<Id> campaignUserIds, Id campaignId) {
        List<PersonDTO> dtos = new List<PersonDTO>();
        
        List<User> users = [
            SELECT Id, Name, Title, Phone, Email, IsActive
            FROM User
            WHERE Id IN :campaignUserIds 
            ORDER BY Name
        ];

        Map<Id, Map<Id, Campaign_Attendee_Document__c>> docMap = new Map<Id, Map<Id, Campaign_Attendee_Document__c>>();

        List<Campaign_Attendee_Document__c> docs = [
            SELECT Id, User__c, Received__c, Campaign_Document__r.Campaign__r.Credentials_Due_Date__c
            FROM Campaign_Attendee_Document__c
            WHERE Campaign_Document__r.Campaign__c = :campaignId
            AND User__c IN :campaignUserIds
            AND Contact__c = NULL
            ORDER BY User__c
        ];

        for(Campaign_Attendee_Document__c cmd: docs) {
            if (!docMap.containsKey(cmd.User__c)) {
                docMap.put(cmd.User__c, new Map<Id, Campaign_Attendee_Document__c> { cmd.Id => cmd });
            } else {
                docMap.get(cmd.User__c).put(cmd.Id, cmd);
            }
        }
        
        for(User u :users) {
            dtos.add(new PersonDTO(u, docMap));
        }
        
        return dtos;
    }

    private static boolean validateRegistration(Id campaignId, string type) {
        if (type.equalsIgnoreCase('Attendee')) {
            Campaign camp = [
                SELECT Remaining_Seats__c
                FROM Campaign
                WHERE Id = :campaignId
            ][0];
        
            if (camp.Remaining_Seats__c <= 0) {
                throw new CampaignMaxCapacityException('This Course has no available seats remaining.');
            }       
        }
        
        return true;
    }
    
    @TestVisible
    private static String createStatus (String label, String campaignId) {
        Map<String, CampaignMemberStatus> campaignMemberStatusByLabel = new Map<String, CampaignMemberStatus>();
        for (CampaignMemberStatus cms : [SELECT Id, Label, SortOrder FROM CampaignMemberStatus WHERE CampaignID=:campaignId ORDER BY SortOrder ASC]) {
            campaignMemberStatusByLabel.put(cms.Label, cms);
        }
        List<CampaignMemberStatus> campaignMemberStatusToUpdate = new List<CampaignMemberStatus>();
        if (!campaignMemberStatusByLabel.containsKey(label)) {
            CampaignMemberStatus newStatus = new CampaignMemberStatus(CampaignID=campaignId,Label=label,
                                                                      IsDefault=false,HasResponded=false);
            
            List<CampaignMemberStatus> campaignMemberStatusList = campaignMemberStatusByLabel.values();
            Integer listSize = campaignMemberStatusList.size();
            Integer newSortOrder = label.equalsIgnoreCase('Registered') ? 3 : 4;
            for (Integer i = 1; i <= listSize; i++) {
                CampaignMemberStatus cmsElement = campaignMemberStatusList[i - 1];
                if (i >= newSortOrder){
                    cmsElement.SortOrder = i + 1;
                    campaignMemberStatusToUpdate.add(cmsElement);
                }
            }
            if (!campaignMemberStatusToUpdate.isEmpty() && Schema.sObjectType.CampaignMemberStatus.isUpdateable()){
                update campaignMemberStatusToUpdate;
            }
            newStatus.SortOrder = newSortOrder;
            if (Schema.sObjectType.CampaignMemberStatus.isCreateable()){
                insert newStatus;
            }
        }
        return label;
    }
    @auraEnabled
    public static void addMember(Id campaignId, Id memberId, string type) {
        try {

            if (!type.equalsIgnoreCase('BD Staff')) {
                if (validateRegistration(campaignId, type)) {
                    String status = 'Sent';
                    if (type.equalsIgnoreCase('Attendee'))  status = createStatus('Registered', campaignId);
                    if (type.equalsIgnoreCase('Waitlist')) status = createStatus('Waitlist', campaignId);
                    CampaignMember cm = new CampaignMember(
                        CampaignId = campaignId,
                        ContactId = memberId,
                        Type__c = type,
                        Status = status
                    );
                    
                    insert cm;
                    //sendEmail(cm.Id, memberId);
                    processAndSendEmail(cm, memberId);
                }
            } else {
                //Adding internal BD Staff has no requirements
                Campaign_User__c cu = new Campaign_User__c(
                    Name = string.valueOf(campaignId) + ' - ' + string.valueOf(memberId),
                    Campaign__c = campaignId,
                    User__c = memberId
                );
                
                insert cu;
            }
            
        } catch(CampaignMaxCapacityException ex) {
            CampaignExceptionData data = new CampaignExceptionData('Max Capacity', ex.getMessage(), 'MAX_CAPACITY');
            
            throw new AuraHandledException(JSON.serialize(data));
        } catch(Exception ex) {
            CampaignExceptionData data = new CampaignExceptionData('Unexpected Error', ex.getMessage(), 'UNEXPECTED_ERROR');
            
            throw new AuraHandledException(JSON.serialize(data));           
        }
    }
  
    @auraEnabled
    public static void updateMember(Id campaignId, Id memberId, string type) {
        try {
            
            validateRegistration(campaignId, type);
            
            CampaignMember cc = [
                SELECT Id, Name, CampaignId, ContactId, Type__c, Status 
                FROM CampaignMember
                WHERE CampaignId = :campaignId AND ContactId = :memberId
            ][0];
            
            cc.Type__c = type;
            if(type == 'Attendee'){
                cc.Status = 'Registered';
            }else if(type == 'Waitlist'){
                cc.Status = 'Waitlist';
            }
            update cc;      
            
        } catch(CampaignMaxCapacityException ex) {
            CampaignExceptionData data = new CampaignExceptionData('Max Capacity', ex.getMessage(), 'MAX_CAPACITY');
            
            throw new AuraHandledException(JSON.serialize(data));
        } catch(Exception ex) {
            CampaignExceptionData data = new CampaignExceptionData('Unexpected Error', ex.getMessage(), 'UNEXPECTED_ERROR');
            
            throw new AuraHandledException(JSON.serialize(data));           
        }
    }
    
    @auraEnabled
    public static void removeMember(Id campaignId, Id memberId, string type) {
        if (!type.equalsIgnoreCase('BD Staff')) {
            List<CampaignMember> cc = [
                SELECT Id
                FROM CampaignMember
                WHERE CampaignId = :campaignId AND ContactId = :memberId
            ];      
            
            if (!cc.isEmpty()) {
                delete cc;
            }
        } else {
            List<Campaign_User__c> cu = [
                SELECT Id
                FROM Campaign_User__c
                WHERE Campaign__c = :campaignId AND User__c = :memberId
            ];
            
            if (!cu.isEmpty()) {
                delete cu;
            }
        }
    }

    @auraEnabled
    public static boolean hasExpenseItems(Id campaignId, Id memberId) {
        List<Campaign_Meal_Attendee__c> meals = [
            SELECT Id 
            FROM Campaign_Meal_Attendee__c 
            WHERE Campaign_Meal__r.Campaign__c = :campaignId
            AND Contact__c = :memberId
        ];

        if (!meals.isEmpty()) { return true; }

        List<Campaign_Flight__c> flights = [
            SELECT Id 
            FROM Campaign_Flight__c
            WHERE Campaign__c = :campaignId
            AND Contact__c = :memberId
        ];

        if (!flights.isEmpty()) { return true; }

        List<Campaign_Hotel_Attendee__c> hotels = [
            SELECT Id 
            FROM Campaign_Hotel_Attendee__c 
            WHERE Campaign_Hotel__r.Campaign__c = :campaignId
            AND Contact__c = :memberId
        ];

        if (!hotels.isEmpty()) { return true; }

        List<Campaign_Transportation_Attendee__c> trans = [
            SELECT Id 
            FROM Campaign_Transportation_Attendee__c 
            WHERE Campaign_Transportation__r.Campaign__c = :campaignId
            AND Contact__c = :memberId
        ];

        if (!trans.isEmpty()) { return true; }

        List<Campaign_Incidental_Attendee__c> incidentals = [
            SELECT Id 
            FROM Campaign_Incidental_Attendee__c 
            WHERE Campaign_Incidental__r.Campaign__c = :campaignId
            AND Contact__c = :memberId
        ];

        if (!incidentals.isEmpty()) { return true; }

        return false;
    }
    
    /*private static void sendEmail (String campaignId, String contactId) {
        List<Campaign_Nominee_Email__mdt> settings = [SELECT Email__c, Email_Template__c FROM Campaign_Nominee_Email__mdt WHERE Active__c = TRUE LIMIT 1];
        if (settings.isEmpty()) return;

        Campaign_Nominee_Email__mdt mdt = settings.get(0);
        EmailTemplate emailTemplate = [SELECT Id, Markup, Body, HtmlValue FROM EmailTemplate WHERE DeveloperName = :mdt.Email_Template__c];

        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.setTargetObjectId(contactId); 
        message.setSenderDisplayName('System Information'); 
        message.setSaveAsActivity(false); 
        
        message.setTemplateID(emailTemplate.Id); 
        message.setWhatId(campaignId); //This is important for the merge fields in template to work
        message.toAddresses = new String[] { mdt.Email__c };
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> { message };
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        
        if (results[0].success) {
            System.debug('Email successfully sent to ' + mdt.Email__c);
        } else {
            System.debug('The email failed to send: ' +  results[0].errors[0].message);
        }
    }
    private static void processAndSendEmail2(CampaignMember cm, String contactId) {
        List<Campaign_Nominee_Email__mdt> settings = [SELECT Email__c, Email_Template__c FROM Campaign_Nominee_Email__mdt WHERE Active__c = TRUE LIMIT 1];
        if (settings.isEmpty()) return;

        Campaign_Nominee_Email__mdt mdt = settings.get(0);
        EmailTemplate emailTemplate = [SELECT Id, Markup, Body, HtmlValue FROM EmailTemplate WHERE DeveloperName = :mdt.Email_Template__c];
        
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.setSaveAsActivity(false); 
        message.setSenderDisplayName('Campaign Information');
        //message.setPlainTextBody(emailTemplate.Body);
        //message.setHtmlBody(emailTemplate.HtmlValue);
        message.setTreatBodiesAsTemplate(true);
        message.toAddresses = new String[] { mdt.Email__c };

        List<Messaging.RenderEmailTemplateBodyResult> rendered = Messaging.renderEmailTemplate(contactId, cm.Id, new List<String>{ emailTemplate.HtmlValue, emailTemplate.Body });
        for (Integer i = 0; i < rendered.size(); i++) {
            if (rendered[i].getSuccess()) {
                if (i < 1) message.setHtmlBody(rendered[i].getMergedBody());
                else message.setPlainTextBody(rendered[i].getMergedBody());
            } else {
                for (Messaging.RenderEmailTemplateError error : rendered[i].getErrors()) system.debug('error merging template: ' + error.getMessage());
            }
        }
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> { message };
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);

        if (results[0].success) {
            System.debug('Email successfully sent to ' + mdt.Email__c);
        } else {
            System.debug('The email failed to send: ' +  results[0].errors[0].message);
        }
    }*/
    private static void processAndSendEmail(CampaignMember cm, String contactId) {
        List<Campaign_Nominee_Email__mdt> settings = [SELECT Email__c, Email_Template__c FROM Campaign_Nominee_Email__mdt WHERE Active__c = TRUE LIMIT 1];
        if (settings.isEmpty()) return;

        Campaign_Nominee_Email__mdt mdt = settings.get(0);
        EmailTemplate emailTemplate = [SELECT Id, Markup, Body, HtmlValue FROM EmailTemplate WHERE DeveloperName = :mdt.Email_Template__c];
        
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.setSaveAsActivity(false); 
        message.setSenderDisplayName('Campaign Information');
        message.setPlainTextBody(processTemplate(cm, contactId, emailTemplate.Body));
        //message.setHtmlBody(processTemplate(cm, contactId, emailTemplate.HtmlValue));
        message.toAddresses = new String[] { mdt.Email__c };
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> { message };
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        
        if (results[0].success) {
            System.debug('Email successfully sent to ' + mdt.Email__c);
        } else {
            System.debug('The email failed to send: ' +  results[0].errors[0].message);
        }
    }
    private static String processTemplate(CampaignMember cm, String contactId, String template) {
        Map<String, Object> cmMap = cm.getPopulatedFieldsAsMap();
        Contact con = [SELECT Id, Name, AccountName__c FROM Contact WHERE Id =:contactId LIMIT 1];
        Map<String, Object> conMap = con.getPopulatedFieldsAsMap();
        Campaign camp = [SELECT Id, Name, Status FROM Campaign WHERE Id =:cm.CampaignId LIMIT 1];
        Map<String, Object> campMap = camp.getPopulatedFieldsAsMap();

        Boolean replacedAll = false;
        while (!replacedAll) {
            String exp = template.substringAfter('{!').substringBefore('}');
            system.debug('exp: ' + exp);
            if (exp.trim().length() > 0) {
                String obj = exp.split('\\.')[0];
                String field = exp.split('\\.')[1];
                String target = '\\{!\\s*' + exp + '\\s*\\}';
                String replacement = '';
                if (field.equals('Link')) field = 'Id';
                if (obj.equals('CampaignMember')) {
                    replacement = (String)cmMap.get(field);
                } else if (obj.equals('Contact')) {
                    replacement = (String)conMap.get(field);
                } else if (obj.equals('Campaign')) {
                    replacement = (String)campMap.get(field);
                }

                if (field.equals('Id')) replacement = System.Url.getSalesforceBaseUrl().toExternalForm() + '/' + replacement;// todo: create valid url
                if (replacement == null && replacement.length() < 1) replacement = '';
                template = template.replaceAll(target, replacement);
            } else {
                replacedAll = true;
            }
        }
        return template;
    }
}