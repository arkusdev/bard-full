public class UserUtil {
	public static boolean canAccessMealExpenses() { 
		return Schema.sObjectType.Campaign_Meal__c.isAccessible();	
	}

	public static boolean canUpsertMealExpenses() {
		return Schema.sObjectType.Campaign_Meal__c.isCreateable() &&
			   Schema.sObjectType.Campaign_Meal__c.isUpdateable();
	}

	public static boolean canDeleteMealExpenses() {
		return Schema.sObjectType.Campaign_Meal__c.isDeletable();
	}

	public static boolean canAccessFlightExpenses() { 
		return Schema.sObjectType.Campaign_Flight__c.isAccessible();	
	}

	public static boolean canUpsertFlightExpenses() {
		return Schema.sObjectType.Campaign_Flight__c.isCreateable() &&
			   Schema.sObjectType.Campaign_Flight__c.isUpdateable();
	}

	public static boolean canDeleteFlightExpenses() {
		return Schema.sObjectType.Campaign_Flight__c.isDeletable();
	}

	public static boolean canAccessHotelExpenses() { 
		return Schema.sObjectType.Campaign_Hotel__c.isAccessible();	
	}

	public static boolean canUpsertHotelExpenses() {
		return Schema.sObjectType.Campaign_Hotel__c.isCreateable() &&
			   Schema.sObjectType.Campaign_Hotel__c.isUpdateable();
	}

	public static boolean canDeleteHotelExpenses() {
		return Schema.sObjectType.Campaign_Hotel__c.isDeletable();
	}

	public static boolean canAccessTransportationExpenses() { 
		return Schema.sObjectType.Campaign_Transportation__c.isAccessible();	
	}

	public static boolean canUpsertTransportationExpenses() {
		return Schema.sObjectType.Campaign_Transportation__c.isCreateable() &&
			   Schema.sObjectType.Campaign_Transportation__c.isUpdateable();
	}

	public static boolean canDeleteTransportationExpenses() {
		return Schema.sObjectType.Campaign_Transportation__c.isDeletable();
	}

	public static boolean canAccessIncidentalExpenses() { 
		return Schema.sObjectType.Campaign_Incidental__c.isAccessible();	
	}

	public static boolean canUpsertIncidentalExpenses() {
		return Schema.sObjectType.Campaign_Incidental__c.isCreateable() &&
			   Schema.sObjectType.Campaign_Incidental__c.isUpdateable();
	}

	public static boolean canDeleteIncidentalExpenses() {
		return Schema.sObjectType.Campaign_Incidental__c.isDeletable();
	}

	public static boolean canAccessProctorExpenses() {
		return Schema.sObjectType.Campaign_Proctor_Fee__c.isAccessible();
	}

	public static boolean canUpsertProctorExpenses() {
		return Schema.sObjectType.Campaign_Proctor_Fee__c.isCreateable() && 
			   Schema.sObjectType.Campaign_Proctor_Fee__c.isUpdateable();
	}

	public static boolean canDeleteProctorExpenses() {
		return Schema.sObjectType.Campaign_Proctor_Fee__c.isDeletable();
	}
}