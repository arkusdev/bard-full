@isTest
public class DMManagerEmailUpdate_Test {
   
    
   static testMethod void test1(){
        
        UserRole dmrole= new UserRole(Name= 'Appalachian - FSP - DM');
        insert dmrole;
        UserRole rmrole= new UserRole(Name= 'Peripheral East - RM');
        insert rmrole;
        UserRole rmManagerrole= new UserRole(Name= 'Sales Executive');
        insert rmManagerrole;
        Profile up = [SELECT Id,Name FROM Profile WHERE Name='Bard - Physician Training'];
        Profile managerp = [SELECT Id,Name FROM Profile WHERE Name='Bard - Biopsy Profile'];
        
        String orgId=UserInfo.getOrganizationId(); 
        String dateString=String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','') ;
        Integer RandomId=Integer.valueOf(Math.rint(Math.random()*1000000)); 
        String uniqueName=orgId+dateString+RandomId;
        
        User rmManager=new User(firstname = 'RM', 
                                lastName = 'Manager', 
                                email = 'RMManager'+ uniqueName + '@test' + orgId + '.org', 
                                Username = 'RMManager'+ uniqueName + '@test' + orgId + '.org', 
                                EmailEncodingKey = 'ISO-8859-1', 
                                Alias = uniqueName.substring(18, 23), 
                                TimeZoneSidKey = 'America/Los_Angeles', 
                                LocaleSidKey = 'en_US', 
                                LanguageLocaleKey = 'en_US', 
                                UserRoleId = rmManagerrole.Id,
                                ProfileId = managerp.Id
                               ); 
        
        
        insert rmManager;
        
        User rmUser=new User(firstname = 'RM', 
                             lastName = 'User', 
                             email = 'RM'+ uniqueName + '@test' + orgId + '.org', 
                             Username = 'RM'+ uniqueName + '@test' + orgId + '.org', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             Alias = uniqueName.substring(18, 23), 
                             TimeZoneSidKey = 'America/Los_Angeles', 
                             LocaleSidKey = 'en_US', 
                             LanguageLocaleKey = 'en_US', 
                             UserRoleId = rmrole.Id,
                             ProfileId = up.Id,
                             ManagerId = rmManager.Id
                            ); 
        
        
        insert rmUser;
        
        User dmUser=new User(firstname = 'ABC', 
                             lastName = 'XYZ', 
                             email = uniqueName + '@test' + orgId + '.org', 
                             Username = uniqueName + '@test' + orgId + '.org', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             Alias = uniqueName.substring(18, 23), 
                             TimeZoneSidKey = 'America/Los_Angeles', 
                             LocaleSidKey = 'en_US', 
                             LanguageLocaleKey = 'en_US', 
                             UserRoleId = dmrole.Id,
                             ProfileId = up.Id,
                             ManagerId = rmUser.Id
                            ); 
        
        
        insert dmUser;
        
        KOL_Nomination__c kol = new KOL_Nomination__c();        
        try{
            
            System.runAs(dmUser){
                
                Account accountObj1 = new Account(Name = 'Test User1');
                insert accountObj1;
                Contact conObj1 = new Contact(lastName = 'Test Contact1', AccountId = accountObj1.Id);
                insert conObj1;
                
                KOL_Nomination__c kol1 = new KOL_Nomination__c();
                kol1.Contact__c = conObj1.Id;
                kol1.CLIMonthlyCaseVolume__c = '5';
                kol1.BreastSoftTissueMonthlyCaseVolume__c = '3';
                kol1.CTOMonthlyCaseVolume__c = '2';
                kol1.DMMonthlyCaseVolumewProducts__c = '1';
                kol1.VenousAccessMonthlyCaseVolume__c = '4';
                kol1.HCP_Clinical_Specialty__c ='5';
                kol1.Office_Name__c = 'test office';
                kol1.Office_Contact_Number__c = '1234567899';
                kol1.Office_Phone_Number__c = '1234567899';
                kol1.Office_Contact_Name__c = 'test off cont';
                kol1.Office_Fax_Number__c = '1234567899';
                kol1.Office_Zip__c = '123456';
                kol1.AVAccessMonthlyCaseVolume__c = '3';
                kol1.CardiovascularMonthlyCaseVolume__c ='4';
                kol1.Numberofcasesobserved__c = 2.00;
                kol1.SFAMonthlyCaseVolume__c = '1';
                kol1.DM_Clinical_Experience__c = '3';
                kol1.DMRatingExpwithRelevantProducts__c = '2';
                kol1.DM_Formal_Clinical_EXP__c = '3';
                kol1.DM_Formal_Pre_Clinical_EXP__c = '2';
                kol1.HCP_Clinical_Specialty__c = 'Other';
                kol1.DM_International_EXP__c = '1';
                kol1.DM_Level_Knowledge__c = '5';
                kol1.DM_Publication_Exp__c = '3';
                kol1.DM_Reputation__c = '4';
                kol1.DM_Society_Membership__c = '2';
                kol1.DM_Society_EXP__c = '4';
                kol1.DM_Speaking_Skills__c = '1';
                kol1.DM_surgical_Skills__c = '3';
                kol1.RequestedHCPServices__c = 'Case Observations;Clinical Investigator';
                insert kol1;
                
                ContentVersion contentVersion = new ContentVersion(
                    Title = 'Penguins',
                    PathOnClient = 'Penguins.jpg',
                    VersionData = Blob.valueOf('Test Content'),
                    IsMajorVersion = true
                );
                insert contentVersion;    
                List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
                
                //create ContentDocumentLink  record 
                ContentDocumentLink cdl = New ContentDocumentLink();
                cdl.LinkedEntityId = kol1.id;
                cdl.ContentDocumentId = documents[0].Id;
                cdl.shareType = 'V';
                insert cdl;
                
                kol1.Send_Approval_to_RM__c = 'Yes';
                kol.NominationStage__c = 'Complete';
                update kol1;
                System.runAs(rmUser){
                    Account accountObj = new Account(Name = 'Test User');
                    insert accountObj;
                    Contact conObj = new Contact(lastName = 'Test Contact', AccountId = accountObj.Id);
                    insert conObj;
                    
                    
                    kol.Contact__c = conObj.Id;
                    kol.CLIMonthlyCaseVolume__c = '5';
                    kol.BreastSoftTissueMonthlyCaseVolume__c = '3';
                    kol.CTOMonthlyCaseVolume__c = '2';
                    kol.DMMonthlyCaseVolumewProducts__c = '1';
                    kol.VenousAccessMonthlyCaseVolume__c = '4';
                    kol.HCP_Clinical_Specialty__c ='5';
                    kol.Office_Name__c = 'test office';
                    kol.Office_Contact_Number__c = '1234567899';
                    kol.Office_Phone_Number__c = '1234567899';
                    kol.Office_Contact_Name__c = 'test off cont';
                    kol.Office_Fax_Number__c = '1234567899';
                    kol.Office_Zip__c = '123456';
                    kol.AVAccessMonthlyCaseVolume__c = '3';
                    kol.CardiovascularMonthlyCaseVolume__c ='4';
                    kol.Numberofcasesobserved__c = 2.00;
                    kol.SFAMonthlyCaseVolume__c = '1';
                    kol.DM_Clinical_Experience__c = '3';
                    kol.DMRatingExpwithRelevantProducts__c = '2';
                    kol.DM_Formal_Clinical_EXP__c = '3';
                    kol.DM_Formal_Pre_Clinical_EXP__c = '2';
                    kol.HCP_Clinical_Specialty__c = 'Other';
                    kol.DM_International_EXP__c = '1';
                    kol.DM_Level_Knowledge__c = '5';
                    kol.DM_Publication_Exp__c = '3';
                    kol.DM_Reputation__c = '4';
                    kol.DM_Society_Membership__c = '2';
                    kol.DM_Society_EXP__c = '4';
                    kol.DM_Speaking_Skills__c = '1';
                    kol.DM_surgical_Skills__c = '3';
                    kol.RequestedHCPServices__c = 'Case Observations;Clinical Investigator';
                    insert kol;
                    
                    
                    kol.Send_Approval_to_RM_Manager__c = 'Yes';
                    update kol;
                    
                    
                    List<KOL_Nomination__c> kolList = new List<KOL_Nomination__c>();
                    kolList.add(kol1);
                    DMManagerEmailUpdate.attachmentValiation(kolList);
                    
                    
                    
                }
                
            }            
            
        }
        catch(Exception e){
            
        }
        
    }
    
    static testmethod void test3(){
        
        Account accountObj1 = new Account(Name = 'Test User1');
                insert accountObj1;
                Contact conObj1 = new Contact(lastName = 'Test Contact1', AccountId = accountObj1.Id);
                insert conObj1;
                
                KOL_Nomination__c kol1 = new KOL_Nomination__c();
                kol1.Contact__c = conObj1.Id;
                kol1.CLIMonthlyCaseVolume__c = '5';
                kol1.BreastSoftTissueMonthlyCaseVolume__c = '3';
                kol1.CTOMonthlyCaseVolume__c = '2';
                kol1.DMMonthlyCaseVolumewProducts__c = '1';
                kol1.VenousAccessMonthlyCaseVolume__c = '4';
                kol1.HCP_Clinical_Specialty__c ='5';
                kol1.Office_Name__c = 'test office';
                kol1.Office_Contact_Number__c = '1234567899';
                kol1.Office_Phone_Number__c = '1234567899';
                kol1.Office_Contact_Name__c = 'test off cont';
                kol1.Office_Fax_Number__c = '1234567899';
                kol1.Office_Zip__c = '123456';
                kol1.AVAccessMonthlyCaseVolume__c = '3';
                kol1.CardiovascularMonthlyCaseVolume__c ='4';
                kol1.Numberofcasesobserved__c = 2.00;
                kol1.SFAMonthlyCaseVolume__c = '1';
                kol1.DM_Clinical_Experience__c = '3';
                kol1.DMRatingExpwithRelevantProducts__c = '2';
                kol1.DM_Formal_Clinical_EXP__c = '3';
                kol1.DM_Formal_Pre_Clinical_EXP__c = '2';
                kol1.HCP_Clinical_Specialty__c = 'Other';
                kol1.DM_International_EXP__c = '1';
                kol1.DM_Level_Knowledge__c = '5';
                kol1.DM_Publication_Exp__c = '3';
                kol1.DM_Reputation__c = '4';
                kol1.DM_Society_Membership__c = '2';
                kol1.DM_Society_EXP__c = '4';
                kol1.DM_Speaking_Skills__c = '1';
                kol1.DM_surgical_Skills__c = '3';
        		kol1.RequestedHCPServices__c = 'Case Observations;Clinical Investigator';
                insert kol1;
                
                
                ApexPages.CurrentPage().getparameters().put('id',kol1.id);
                KOLNominationPDF kol = new KOLNominationPDF();
                
                ContentVersion contentVersion = new ContentVersion(
                    Title = 'Penguins',
                    PathOnClient = 'Penguins.jpg',
                    VersionData = Blob.valueOf('Test Content'),
                    IsMajorVersion = true
                );
                insert contentVersion;    
                List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
                
                //create ContentDocumentLink  record 
                ContentDocumentLink cdl = New ContentDocumentLink();
                cdl.LinkedEntityId = kol1.id;
                cdl.ContentDocumentId = documents[0].Id;
                cdl.shareType = 'V';
                insert cdl;
                
                KOLNominationButtonCTRL.checkProfiles(kol1.id);
                KOLNominationButtonCTRL.changeKOLStatus(kol1);
                KOLNominationButtonCTRL.updateRMCommentsApex(kol1);
                kol1.Send_Approval_to_RM__c = 'Yes';
                kol1.NominationStage__c = 'Complete';
                update kol1;
                List<KOL_Nomination__c> kolList = new List<KOL_Nomination__c>();
                kolList.add(kol1);
                DMManagerEmailUpdate.attachmentValiation(kolList);
    }
}