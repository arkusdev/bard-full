public class CampaignIncidentalDTO {
	@auraEnabled 
	public Id incidentalId { get; set; } 
	@auraEnabled 
	public Id campaignId { get; set; } 
	@auraEnabled 
	public string name { get; set; }
	@auraEnabled 
	public string description { get; set; }
	@auraEnabled 
	public datetime purchaseDateTime { get; set; }
	@auraEnabled 
	public string purchaseDateTimeDisplay { get; set; }
	@auraEnabled 
	public decimal totalCost { get; set; }
	@auraEnabled
	public string totalCostDisplay { get; set; }
	@auraEnabled 
	public string transferOfValue { get; set; }
	@auraEnabled 
	public string notes { get; set; }
	@auraEnabled 
	public List<CampaignIncidentalAttendeeDTO> availableMembers { get; set; }
	@auraEnabled 
	public List<CampaignIncidentalAttendeeDTO> selectedMembers { get; set; }
	@auraEnabled 
	public List<CampaignIncidentalAttendeeDTO> availableUsers { get; set; }
	@auraEnabled 
	public List<CampaignIncidentalAttendeeDTO> selectedUsers { get; set; }
	@auraEnabled
	public string attendeesHelpText { get; set; }
	@auraEnabled 
	public decimal totalAttendees { get; set; }
	@auraEnabled 
	public decimal costPerAttendee { get; set; }
	@auraEnabled
	public string costPerAttendeeDisplay { get; set; }
	@auraEnabled 
	public boolean isNew { get; set; }
	@auraEnabled
	public Map<string, string> transferOfValueOptions { get; set; }

	List<string> args = new string[] { '0','number', '###,###,##0.00' };

	public CampaignIncidentalDTO(Campaign_Incidental__c ci) {
		this.initDTO(ci);
	}

	public CampaignIncidentalDTO(Campaign_Incidental__c ci, List<CampaignMember> availMembers, List<Campaign_User__c> availUsers) {
		this.initDTO(ci);

		if (!availMembers.isEmpty()) {
			for(CampaignMember am :availMembers) {
				this.availableMembers.add(new CampaignIncidentalAttendeeDTO(am));
			}		
		}

		if (!availUsers.isEmpty()) {
			for(Campaign_User__c au :availUsers) {
				this.availableUsers.add(new CampaignIncidentalAttendeeDTO(au));
			}		
		}
	}

	private void initDTO(Campaign_Incidental__c ci) {
		this.incidentalId = ci.Id;
		this.isNew = (ci.Id == null);
		this.campaignId = ci.Campaign__c;
		this.name = ci.Name;
		this.description = ci.Description__c;
		this.purchaseDateTime = ci.Purchase_Date_Time__c;
		this.purchaseDateTimeDisplay = (this.purchaseDateTime != null ? this.purchaseDateTime.format() : '');
		this.totalCost = (ci.Total_Cost__c != null ? ci.Total_Cost__c : 0);
		this.transferOfValue = ci.Transfer_of_Value__c;
		this.totalAttendees = (ci.Attendee_Count__c != null ? ci.Attendee_Count__c : 0);
		this.costPerAttendee = (ci.Cost_Per_Attendee__c != null ? ci.Cost_Per_Attendee__c : 0);
		this.notes = ci.Notes__c;
		this.selectedMembers = new List<CampaignIncidentalAttendeeDTO>();
		this.selectedUsers = new List<CampaignIncidentalAttendeeDTO>();
		this.availableMembers = new List<CampaignIncidentalAttendeeDTO>();
		this.availableUsers = new List<CampaignIncidentalAttendeeDTO>();
		this.attendeesHelpText = '';

		if (!ci.Campaign_Incidental_Attendees__r.isEmpty()) {
			for(Campaign_Incidental_Attendee__c cia :ci.Campaign_Incidental_Attendees__r) {
				if (cia.Contact__c != null) {
					this.selectedMembers.add(new CampaignIncidentalAttendeeDTO(cia.Contact__c, cia.Contact__r.Name));
					this.attendeesHelpText += cia.Contact__r.Name + ' | ';
				} else {
					this.selectedUsers.add(new CampaignIncidentalAttendeeDTO(cia.User__c, cia.User__r.Name));
					this.attendeesHelpText += cia.User__r.Name + ' | ';
				}
			}

			this.attendeesHelpText = this.attendeesHelpText.substring(0, this.attendeesHelpText.length() - 3);
		} else {
			this.attendeesHelpText = 'No attendees assigned.';
		}

		this.totalCostDisplay = '$' + string.format(this.totalCost.format(), args);
		this.costPerAttendeeDisplay = '$' + string.format(this.costPerAttendee.format(), args);

		this.transferOfValueOptions = PicklistUtil.fetchTransferOfValueOptions('Campaign_Incidental__c');
	}
}