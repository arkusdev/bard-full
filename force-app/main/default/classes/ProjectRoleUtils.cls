public with sharing class ProjectRoleUtils {

    static Map<String, String> profiles = new Map<String, String>{ 'Marketing' => '', 'Market Access/HEOR​' => ' – MA/HEOR'};

    public static void handleAfterInsert(List<Project_Role__c> projectRoles) {
        Map<String, CollaborationGroupMember> membersToAdd = new Map<String, CollaborationGroupMember>();
        
        Map<String, Set<Id>> projectMembers = new Map<String, Set<Id>>();

        for (Project_Role__c projectRole : [
                SELECT Id, Project__r.Name, User__c, Role__c
                FROM Project_Role__c
                WHERE Id IN: projectRoles
            ]) {
            String projectName = (projectRole.Project__r.Name + profiles.get(projectRole.Role__c)).toLowerCase();
            System.debug('****0* ' + projectName + ' ' + projectRole.User__c);
            if (projectMembers.containsKey(projectName)){
                projectMembers.get(projectName).add(projectRole.User__c);
            }else {
                projectMembers.put(projectName, new Set<Id>{ projectRole.User__c });
            }
        }

        for (CollaborationGroup collaborationGroup : [
            SELECT Id, Name, OwnerId, (SELECT MemberId FROM GroupMembers)
            FROM CollaborationGroup
            WHERE Name IN: projectMembers.keySet()
        ]) {
            System.debug('****1a* ' + collaborationGroup.Name);
            Set<Id> groupMembers = projectMembers.get(collaborationGroup.Name.toLowerCase());
            if (groupMembers != null){
                System.debug('****1b* ' + collaborationGroup.Name);
                for (Id memberId : groupMembers) {
                    System.debug('****1c* ' + collaborationGroup.Name);
                    if (!membersToAdd.containsKey(memberId+collaborationGroup.Name) && memberId != collaborationGroup.OwnerId){
                        System.debug('****1d* ' + collaborationGroup.Name);
                        if (!memberInGroup(collaborationGroup.GroupMembers, memberId)){
                            CollaborationGroupMember newMember = new CollaborationGroupMember(
                                MemberId = memberId,
                                CollaborationGroupId = collaborationGroup.Id
                            );
                            membersToAdd.put(memberId+collaborationGroup.Name, newMember);
                            System.debug('***2* ' + memberId + ' ' + collaborationGroup.Id);
                        }
                    }
                }
            }
        }
        if (!membersToAdd.isEmpty()){
            insert membersToAdd.values();
        }
    }

    private static Boolean memberInGroup(List<CollaborationGroupMember> members, Id memberId) {
        for (CollaborationGroupMember member : members) {
            if (member.MemberId == memberId){
                return true;
            }
        }
        return false;
    }

    public static void handleAfterUpdate(Map<Id, Project_Role__c> oldProjectRoles, Map<Id, Project_Role__c> newProjectRoles) {
        Map<String, Map<Id, Id>> oldToNewMembers = new Map<String, Map<Id, Id>>();
        Map<String, Set<Id>> projectMembersToAdd = new Map<String, Set<Id>>();

        Map<String, List<Project_Role__c>> usersByRoleInProject = getUsersByRoleInProject(oldProjectRoles.keySet());

        for (Project_Role__c projectRole : [
                SELECT Id, Project__c, Project__r.Name, User__c, Role__c
                FROM Project_Role__c
                WHERE Id IN: oldProjectRoles.keySet()
            ]) {
            String projectName = (projectRole.Project__r.Name + profiles.get(projectRole.Role__c)).toLowerCase();
            Id oldMemberId = oldProjectRoles.get(projectRole.Id).User__c;
            Id newMemberId = newProjectRoles.get(projectRole.Id).User__c;
            String projectRoleKey = projectRole.Project__c + '_' + oldMemberId + '_' + projectRole.Role__c;
            Boolean deleteOldMember = !usersByRoleInProject.containsKey(projectRoleKey) || usersByRoleInProject.get(projectRoleKey).isEmpty();
            if (oldMemberId != newMemberId){
                if (deleteOldMember){
                    if (oldToNewMembers.containsKey(projectName)){
                        oldToNewMembers.get(projectName).put(oldMemberId, newMemberId);
                    }else {
                        oldToNewMembers.put(projectName, new Map<Id, Id>{ oldMemberId => newMemberId });
                    }
                }else {
                    if (projectMembersToAdd.containsKey(projectName)){
                        projectMembersToAdd.get(projectName).add(newMemberId);
                    }else {
                        projectMembersToAdd.put(projectName, new Set<Id>{ newMemberId });
                    }
                }
            }else {
                String oldMemberRole = oldProjectRoles.get(projectRole.Id).Role__c;
                String newMemberRole = newProjectRoles.get(projectRole.Id).Role__c;
                if (oldMemberRole != newMemberRole){
                    if (profiles.containsKey(newMemberRole)){
                        projectMembersToAdd.put(projectName, new Set<Id>{ newMemberId });
                    }
                    if (profiles.containsKey(oldMemberRole)){
                        String oldProjectName = (projectRole.Project__r.Name + profiles.get(oldMemberRole)).toLowerCase();
                        if (oldToNewMembers.containsKey(oldProjectName)){
                            oldToNewMembers.get(oldProjectName).put(oldMemberId, null);
                        }else {
                            oldToNewMembers.put(oldProjectName, new Map<Id, Id>{ oldMemberId => null });
                        }
                    }
                }
            }
        }

        ChatterGroupMembersInfo groupMembersInfo = getGroupMembers(oldToNewMembers, projectMembersToAdd);

        if (!groupMembersInfo.membersToDelete.isEmpty()){
            delete groupMembersInfo.membersToDelete;
        }
        if (!groupMembersInfo.membersToAdd.isEmpty()){
            insert groupMembersInfo.membersToAdd;
        }
    }

    
    public static void handleBeforeDelete(Map<Id, Project_Role__c> projectRoles) {
        List<CollaborationGroupMember> membersToDelete = new List<CollaborationGroupMember>();
        Map<String, List<Id>> projectUsers = new Map<String, List<Id>>();

        Map<String, List<Project_Role__c>> usersByRoleInProject = getUsersByRoleInProject(projectRoles.keySet());

        for (Project_Role__c projectRole : [
            SELECT Id, Project__c, Project__r.Name, User__c, Role__c
            FROM Project_Role__c
            WHERE Id IN: projectRoles.values()
        ]) {
            Id userId = projectRole.User__c;
            String userByRoleInProject = projectRole.Project__c + '_' + projectRole.User__c + '_' + projectRole.Role__c;
            if (usersByRoleInProject.containsKey(userByRoleInProject) && usersByRoleInProject.get(userByRoleInProject).size() == 1){
            String projectName = (projectRole.Project__r.Name + profiles.get(projectRole.Role__c)).toLowerCase();
                if (projectUsers.containsKey(projectName)){
                    projectUsers.get(projectName).add(userId);
                }else{
                    projectUsers.put(projectName, new List<Id>{userId});
                }
            }
        }

        for (CollaborationGroup collaborationGroup : [
            SELECT Id, Name, (SELECT Id, MemberId FROM GroupMembers)
            FROM CollaborationGroup
            WHERE Name IN: projectUsers.keySet()
        ]) {
            for (CollaborationGroupMember groupMember : collaborationGroup.GroupMembers) {
                if (projectUsers.get(collaborationGroup.Name.toLowerCase()).contains(groupMember.MemberId)){
                    membersToDelete.add(groupMember);
                }
            }
        }

        if (!membersToDelete.isEmpty()){
            delete membersToDelete;
        }
    }

    private static Map<String, List<Project_Role__c>> getUsersByRoleInProject(Set<Id> projectRolesIds) {
        Set<Id> projectsIds = new Set<Id>();
        Map<String, List<Project_Role__c>> usersByRoleInProject = new Map<String, List<Project_Role__c>>();
        
        for (Project_Role__c projectRole : [
            SELECT Id, Project__c
            FROM Project_Role__c
            WHERE Id IN: projectRolesIds
        ]) {
            projectsIds.add(projectRole.Project__c);
        }

        for (Project_Role__c projectRole : [
                SELECT Id, Role__c, Project__c, User__c
                FROM Project_Role__c
                WHERE Project__c IN: projectsIds
            ]) {
                String key = projectRole.Project__c + '_' + projectRole.User__c + '_' + projectRole.Role__c;
                if (usersByRoleInProject.containsKey(key)){
                    usersByRoleInProject.get(key).add(projectRole);
                }else {
                    usersByRoleInProject.put(key, new List<Project_Role__c>{projectRole});
                }
        }
        return usersByRoleInProject;
    }

    private static ChatterGroupMembersInfo getGroupMembers(Map<String, Map<Id, Id>> oldToNewMembers, Map<String, Set<Id>> projectMembersToAdd) {
        ChatterGroupMembersInfo newChatterGroupMembersInfo = new ChatterGroupMembersInfo();
        for (CollaborationGroup collaborationGroup : [
            SELECT Id, Name, (SELECT CollaborationGroupId, MemberId FROM GroupMembers)
            FROM CollaborationGroup
            WHERE Name IN: oldToNewMembers.keySet() OR Name IN: projectMembersToAdd.keySet()
        ]) {
            String projectName = collaborationGroup.Name.toLowerCase();
            List<CollaborationGroupMember> groupMembers = collaborationGroup.GroupMembers;
            for (CollaborationGroupMember groupMember : groupMembers) {
                Map<Id, Id> memberToUpdate = oldToNewMembers.get(projectName);
                if (memberToUpdate != null){
                    Id newMemberId = memberToUpdate.get(groupMember.MemberId);
                    if (memberToUpdate.containsKey(groupMember.MemberId)){
                        newChatterGroupMembersInfo.membersToDelete.add(groupMember);
                        if (newMemberId != null){
                            if (projectMembersToAdd.containsKey(projectName)){
                                projectMembersToAdd.get(projectName).add(newMemberId);
                            }else {
                                projectMembersToAdd.put(projectName, new Set<Id>{newMemberId});
                            }
                        }
                    }
                }
                if (projectMembersToAdd.containsKey(projectName) && projectMembersToAdd.get(projectName).contains(groupMember.MemberId)){
                    projectMembersToAdd.get(projectName).remove(groupMember.MemberId);
                }
            }
            if (projectMembersToAdd.containsKey(projectName) && !projectMembersToAdd.get(projectName).isEmpty()){
                for (Id newMemberId : projectMembersToAdd.get(projectName)) {
                    newChatterGroupMembersInfo.membersToAdd.add(new CollaborationGroupMember(
                        CollaborationGroupId = collaborationGroup.Id,
                        MemberId = newMemberId
                    ));
                }
            }
        }
        return newChatterGroupMembersInfo;
    }

    class ChatterGroupMembersInfo {
        List<CollaborationGroupMember> membersToDelete { set; get; }
        List<CollaborationGroupMember> membersToAdd { set; get; }
        ChatterGroupMembersInfo(){
            membersToDelete = new List<CollaborationGroupMember>();
            membersToAdd = new List<CollaborationGroupMember>();
        }
    }
}