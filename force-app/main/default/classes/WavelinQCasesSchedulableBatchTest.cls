@isTest
private class WavelinQCasesSchedulableBatchTest {
    @TestSetup
    static void makeData(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias='testman',
                          Email='test@testman.com', 
                          EmailEncodingKey='UTF-8',
                          LastName='Testing',
                          LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US',
                          ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName='test@testman.com');
        insert u;

        UserRole testRole = new UserRole(Name = 'Test Role');
        insert testRole;

        UserRole rmRole = new UserRole(Name = 'RM Role', ParentRoleId = testRole.Id);
        insert rmRole;

        UserRole dmRole = new UserRole(Name = 'DM Role', ParentRoleId = rmRole.Id);
        insert dmRole;

        User regionalManager = new User(Alias='regional',
                                        Email='regional@testman.com', 
                                        EmailEncodingKey='UTF-8',
                                        LastName='Regional',
                                        LanguageLocaleKey='en_US', 
                                        LocaleSidKey='en_US',
                                        ProfileId = p.Id, 
                                        TimeZoneSidKey='America/Los_Angeles',
                                        UserName='regional@testman.com',
                                        UserRoleId = rmRole.Id);
        insert regionalManager;

        User districtManager = new User(Alias='district',
                                        Email='district@testman.com', 
                                        EmailEncodingKey='UTF-8',
                                        LastName='District',
                                        LanguageLocaleKey='en_US', 
                                        LocaleSidKey='en_US',
                                        ProfileId = p.Id, 
                                        TimeZoneSidKey='America/Los_Angeles',
                                        UserName='district@testman.com',
                                        UserRoleId = dmRole.Id);
        insert districtManager;
    }
    @isTest
    static void scheduleWavelinQCases(){
        makeCustomSettingData();
        User regionalManager = [SELECT Id FROM User WHERE UserName = 'regional@testman.com'];
        User districtManager = [SELECT Id FROM User WHERE UserName = 'district@testman.com'];

        Territory__c territory = new Territory__c(Regional_Manager__c = regionalManager.Id, District_Manager__c = districtManager.Id);
        insert territory;

        Account goldAcc = new Account(Name = 'Testing account 1',
                                      WavelinQ_Completed_Case_Count__c = 10,
                                      Territory__c = territory.Id);
        insert goldAcc;

        Account platinumAcc = new Account(Name = 'Testing account 2',
                                          WavelinQ_Completed_Case_Count__c = 10,
                                          Territory__c = territory.Id);
        insert platinumAcc;

        Product__c p1 = new Product__c();
		p1.Name = 'WavelinQ 4F';
		insert p1;

        Id wavelinqRecordType = [SELECT Id FROM RecordType WHERE SObjectType = 'Case' AND Name = 'BD WavelinQ'].Id;

        List<Case> goldCases = new List<Case>();
        for (Integer i = 0; i < 20; i++) {
            Case c = new Case();
            c.RecordTypeId = wavelinqRecordType;
            c.Product_Group_1__c = p1.Name;
		    c.Quantity_1__c = '1';
            c.Subject = 'test' + i;
            c.Type = 'WavelinQ - Proctor';
            c.Status = 'Completed';
            c.Case_Date__c = Datetime.now();
            c.AccountId = goldAcc.Id;
            goldCases.add(c);
        }
        insert goldCases;

        List<Case> platinumCases = new List<Case>();
        for (Integer i = 20; i < 55; i++) {
            Case c = new Case();
            c.RecordTypeId = wavelinqRecordType;
            c.Product_Group_1__c = p1.Name;
		    c.Quantity_1__c = '1';
            c.Subject = 'test' + i;
            c.Type = 'WavelinQ - Proctor';
            c.Status = 'Completed';
            c.Case_Date__c = Datetime.now();
            c.AccountId = platinumAcc.Id;
            platinumCases.add(c);
        }
        insert platinumCases;

        Test.startTest();
            Database.executeBatch(new WavelinQCasesBatch());
        Test.stopTest();
        Account acc1 = [SELECT WavelinQ_Completed_Case_Count__c, WavelinQ_Last_Status_Change__c,
                        WavelinQ_Previous_Status__c, WavelinQ_Achievement_Status__c FROM Account WHERE Name = 'Testing account 1'];
        Account acc2 = [SELECT WavelinQ_Completed_Case_Count__c, WavelinQ_Last_Status_Change__c,
                        WavelinQ_Previous_Status__c, WavelinQ_Achievement_Status__c FROM Account WHERE Name = 'Testing account 2'];
        System.assertEquals(20, acc1.WavelinQ_Completed_Case_Count__c, 'The completed case count must be 20');
        System.assertEquals('Gold', acc1.WavelinQ_Achievement_Status__c, 'The status of wavelinq must be gold');
        System.assertEquals(35, acc2.WavelinQ_Completed_Case_Count__c, 'The completed case count must be 35');
        System.assertEquals('Platinum', acc2.WavelinQ_Achievement_Status__c, 'The status of wavelinq must be platinum');
    }

    @isTest
    static void testSchedulableProcess(){
        makeCustomSettingData();
        Test.startTest();
            WavelinQCasesSchedulableBatch scheduleCases = new WavelinQCasesSchedulableBatch();
                    
            String seconds = Datetime.now().second() == 60 ? '0' : String.valueOf(Datetime.now().second() + 1);
            String hour = String.valueOf(Datetime.now().hour());
            String min = seconds == '0' ? String.valueOf(Datetime.now().minute() + 1) : String.valueOf(Datetime.now().minute()); 
            String ss = seconds;
            
            String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
            System.schedule('Schedule WavelinQ cases process at ' + String.valueOf(Datetime.now()), nextFireTime, scheduleCases);
        Test.stopTest();
        System.assert(scheduleCases != null, 'The schedule batch is null');
    }

    static void makeCustomSettingData(){
        insert new Wavelinq_Achievement_Settings__c(
            Name = 'Wavelinq Setting',
            Silver_level__c = 10,
            Gold_level__c = 20,
            Platinum_level__c = 35,
            Diamond_level__c = 50,
            Notification_subject__c = 'Test subject',
            Notification_message__c = 'Congratulations to <<vascular TM>> and <<account name>> for achieving <<status>> Level!',
            French_Id__c = [SELECT Id FROM User LIMIT 1].Id,
            Finch_Id__c = [SELECT Id FROM User LIMIT 1].Id,
            Batch_Week_Days__c = 'Mon,Tue,Wed,Thu,Fri,Sat,Sun',
            Batch_Start_Hour__c = 0,
            Batch_End_Hour__c = 23
        );
    }
}