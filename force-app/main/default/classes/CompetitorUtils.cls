public class CompetitorUtils {
    
    public static void CompetitorProductRollup() {

        List<Id> oppIds = new List<Id>();

        if (trigger.isInsert) {
            
            for (Competitor__c competitor : (List<Competitor__c>) trigger.new) {
                
                if (!String.isBlank(competitor.Product__c))
                    oppIds.add(competitor.Opportunity__c);
            }

        } else if (trigger.isUpdate) {
            
            for (Competitor__c competitor : (List<Competitor__c>) trigger.new) {
                
                if (competitor.Product__c != ((Competitor__c)trigger.oldMap.get(competitor.Id)).Product__c) 
                    oppIds.add(competitor.Opportunity__c);
            }
        } else if (trigger.isDelete) {
            
            for (Competitor__c competitor : (List<Competitor__c>) trigger.old) 
                oppIds.add(competitor.Opportunity__c);
        }

        if (!oppIds.isEmpty())
            AddRemoveProductOnOpp(oppIds);
    }

    public static void AddRemoveProductOnOpp(List<Id> oppIds) {
        
        List<Opportunity> oppList = new List<Opportunity>();
        for (Opportunity opportunity : [select  Competitor_Product__c, 
                                        (
                                            select Product__c 
                                            from Competitors__r
                                        ) 
                                from Opportunity 
                                where Id IN: oppIds]) {
            
            Set<String> productSet = new Set<String>();
            
            for (Competitor__c competitor : opportunity.Competitors__r) {
                
                if (!String.isBlank(competitor.Product__c))
                    productSet.add(competitor.Product__c);
            }
            
            opportunity.Competitor_Product__c = String.join(new List<String>(productSet), ', ');
            oppList.add(opportunity);
        }
        update oppList;
    }
}