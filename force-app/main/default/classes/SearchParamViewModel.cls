public class SearchParamViewModel {
	public string ParamName { get; set; }
	public string ParamValue { get; set; }

	public SearchParamViewModel(string paramName, string paramValue) {
		this.ParamName = paramName;
		this.ParamValue = paramValue;
	}
}