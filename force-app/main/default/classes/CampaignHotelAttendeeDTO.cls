public class CampaignHotelAttendeeDTO {
	@auraEnabled 
	public Id hotelAttendeeId { get; set; }
	@auraEnabled 
	public Id attendeeId { get; set; }
	@auraEnabled 
	public string attendeeName { get; set; }
	@auraEnabled 
	public string name { get; set; }
	@auraEnabled 
	public date reservationDate { get; set; }
	@auraEnabled 
	public string reservationDateDisplay { get; set; }
	@auraEnabled 
	public decimal cost { get; set; }
	@auraEnabled
	public string costDisplay { get; set; }
	@auraEnabled 
	public boolean present { get; set; }
	@auraEnabled
	public string type { get; set; }
	@auraEnabled 
	public string confirmationNumber { get; set; }

	public CampaignHotelAttendeeDTO(Id attendeeId, string attendeeName, Campaign_Hotel_Attendee__c cha) {
		this.hotelAttendeeId = cha.Id;
		this.attendeeId = attendeeId;
		this.attendeeName = attendeeName;
		this.name = cha.Name;
		this.reservationDate = cha.Reservation_Date__c;
		this.reservationDateDisplay = (cha.Reservation_Date__c != null ? cha.Reservation_Date__c.format() : '');
		this.cost = (cha.Cost__c != null ? cha.Cost__c : 0);
		this.present = cha.Present__c;
		this.confirmationNumber = cha.Confirmation_Number__c;
		
		this.costDisplay = '$' + string.valueOf(this.cost);
	}

	public CampaignHotelAttendeeDTO(Id attendeeId, string attendeeName, string name, date reservationDate, decimal cost, boolean present) {
		this.attendeeId = attendeeId;
		this.attendeeName = attendeeName;
		this.name = name;
		this.reservationDate = reservationDate;
		this.reservationDateDisplay = (reservationDate != null ? reservationDate.format() : '');
		this.cost = cost;
		this.present = present;
		
		this.costDisplay = '$' + string.valueOf(this.cost);
	}

	public CampaignHotelAttendeeDTO(CampaignMember cm) {
		this.attendeeId = cm.ContactId;
		this.attendeeName = cm.Contact.Name;
		this.type = cm.Type__c;
	}

	public CampaignHotelAttendeeDTO(Campaign_User__c cu) {
		this.attendeeId = cu.User__c;
		this.attendeeName = cu.User__r.Name;
	}
}