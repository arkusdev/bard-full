@isTest
public with sharing class BackorderEmailAlertTest {
    
    @testSetup
    static void setup() {
        Account acc = new Account(Name = 'Testing account');
        insert acc;
        
        List<Backorder__c> backorders = new List<Backorder__c>();
        for (Integer i = 1; i < 21; i++) {
            Backorder__c backorder = new Backorder__c(Account__c = acc.Id);
            backorders.add(backorder);
        }
        
        insert backorders;
        
        Backorder_Email_Alert__c testing = new Backorder_Email_Alert__c(
            Number_of_Records__c = 20
        );
        
        insert testing;
        
    }
    
    @isTest
    static void scheduledBackorderEmailAlertTest() {
        Datetime dateTNow = Datetime.now().addMinutes(1);
        String jobName = 'Scheduled Backorder Email Alert - ' + dateTNow.format();
        String crondateTNow =
            '0 ' +
            String.valueOf(dateTNow.minute()) +
            ' ' +
            String.valueof(dateTNow.hour()) +
            ' ' +
            String.valueof(dateTNow.day()) +
            ' ' +
            String.valueof(dateTNow.month()) +
            ' ' +
            ' ? ' +
            String.valueof(dateTNow.year());
        Test.startTest();
        System.schedule(jobName, crondateTNow, new ScheduledBackorderEmailAlert());
        Test.stopTest();
        
        List<CronJobDetail> cjd = [
            SELECT Name
            FROM CronJobDetail
            WHERE Name LIKE '%Scheduled Backorder Email Alert%'
        ];
        Backorder_Email_Alert__c testing = Backorder_Email_Alert__c.getOrgDefaults();
        System.assertEquals(0, testing.Number_of_Records__c, 'The custom setting was not updated.');
        System.assert(cjd.size() > 0, 'There should be a class scheduled.');
    }
    
    @isTest
    static void scheduleInFiveMinutes() {
        
        Backorder_Email_Alert__c testing = Backorder_Email_Alert__c.getOrgDefaults();
        testing.Number_of_Records__c = 2;
        update testing;
        
        Test.startTest();
        ScheduledBackorderEmailAlert back = new ScheduledBackorderEmailAlert();
        Back.execute(null);
        Integer emailsSent = Limits.getEmailInvocations();
        Test.stopTest();
        
        List<CronJobDetail> cjd = [
            SELECT Name
            FROM CronJobDetail
            WHERE Name LIKE '%Scheduled Backorder Email Alert%'
        ];
        
        System.assert(cjd.size() > 0, 'The class should have been scheduled to run in 5 minutes.');
        testing = Backorder_Email_Alert__c.getOrgDefaults();
        System.assertEquals(0, testing.Number_of_Records__c, 'The custom setting was not updated.');
        System.assertEquals(0, emailsSent, 'No email should have been sent.');
    }
    
    @isTest
    static void backorderEmailAlerteException() {
        Test.startTest();
        BackorderEmailAlert.sendEmail(new TestException('Test Exception'));
        Integer emailsSent = Limits.getEmailInvocations();
        Test.stopTest();
        
        System.assert(emailsSent > 0, 'An email exception should have been sent.');
    }
    
   /**
    * @description TestException
	*/
    private class TestException extends Exception {
    }
}