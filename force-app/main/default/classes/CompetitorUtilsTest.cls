@IsTest
private class CompetitorUtilsTest {

    static testMethod void CompetitorProductRollupInsertTest() {
        Account account = new Account();
        account.Name = 'test';
        insert account;

        Opportunity opportunity = new Opportunity();
        opportunity.Name = 'test';
        opportunity.AccountId = account.Id;
        opportunity.StageName = 'Open';
        opportunity.CloseDate = Date.today();
        insert opportunity;

        String product = 'Armada 14';
        
        Competitor__c competitor = new Competitor__c();
        competitor.Competitor__c = 'Abbott';
        competitor.Opportunity__c = opportunity.Id;
        competitor.Product__c = product;
        competitor.RecordTypeId = [select Id from RecordType where SObjectType = 'Competitor__c' and DeveloperName = 'Peripheral'].Id;

        Test.startTest();
            insert competitor;
        Test.stopTest();

        opportunity = [select Competitor_Product__c from Opportunity where Id =: opportunity.Id];

        system.assertEquals(product, opportunity.Competitor_Product__c);
    }

    static testMethod void CompetitorProductRollupInsertMultipleTest() {
        Account account = new Account();
        account.Name = 'test';
        insert account;

        Opportunity opportunity = new Opportunity();
        opportunity.Name = 'test';
        opportunity.AccountId = account.Id;
        opportunity.StageName = 'Open';
        opportunity.CloseDate = Date.today();
        insert opportunity;

        Id recordTypeId = [select Id from RecordType where SObjectType = 'Competitor__c' and DeveloperName = 'Peripheral'].Id;

        List<Competitor__c> competitorList = new List<Competitor__c>();
        String product = 'Armada 14';
        Competitor__c competitor = new Competitor__c();
        competitor.Competitor__c = 'Abbott';
        competitor.Opportunity__c = opportunity.Id;
        competitor.Product__c = product;
        competitor.RecordTypeId = recordTypeId;
        competitorList.add(competitor);

        String product2 = 'Absolute Pro';
        Competitor__c competitor2 = new Competitor__c();
        competitor2.Competitor__c = 'Abbott';
        competitor2.Opportunity__c = opportunity.Id;
        competitor2.Product__c = product2;
        competitor2.RecordTypeId = recordTypeId;
        competitorList.add(competitor2);

        Test.startTest();
            insert competitorList;
        Test.stopTest();

        opportunity = [select Competitor_Product__c from Opportunity where Id =: opportunity.Id];

        system.assert(opportunity.Competitor_Product__c.contains(product));
        system.assert(opportunity.Competitor_Product__c.contains(product2));
    }

    static testMethod void CompetitorProductRollupUpdateTest() {
        Account account = new Account();
        account.Name = 'test';
        insert account;

        Opportunity opportunity = new Opportunity();
        opportunity.Name = 'test';
        opportunity.AccountId = account.Id;
        opportunity.StageName = 'Open';
        opportunity.CloseDate = Date.today();
        insert opportunity;

        String product = 'Armada 14';
        
        Competitor__c competitor = new Competitor__c();
        competitor.Competitor__c = 'Abbott';
        competitor.Opportunity__c = opportunity.Id;
        competitor.Product__c = product;
        competitor.RecordTypeId = [select Id from RecordType where SObjectType = 'Competitor__c' and DeveloperName = 'Peripheral'].Id;
        insert competitor;

        product = 'Absolute Pro';
        competitor.Product__c = product;

        Test.startTest();
            update competitor;
        Test.stopTest();

        opportunity = [select Competitor_Product__c from Opportunity where Id =: opportunity.Id];

        system.assertEquals(product, opportunity.Competitor_Product__c);
    }

    static testMethod void CompetitorProductRollupDeleteTest() {
        Account account = new Account();
        account.Name = 'test';
        insert account;

        Opportunity opportunity = new Opportunity();
        opportunity.Name = 'test';
        opportunity.AccountId = account.Id;
        opportunity.StageName = 'Open';
        opportunity.CloseDate = Date.today();
        insert opportunity;

        String product = 'Armada 14';
        
        Competitor__c competitor = new Competitor__c();
        competitor.Competitor__c = 'Abbott';
        competitor.Opportunity__c = opportunity.Id;
        competitor.Product__c = product;
        competitor.RecordTypeId = [select Id from RecordType where SObjectType = 'Competitor__c' and DeveloperName = 'Peripheral'].Id;
        insert competitor;

        Test.startTest();
            delete competitor;
        Test.stopTest();

        opportunity = [select Competitor_Product__c from Opportunity where Id =: opportunity.Id];

        system.assertEquals(null, opportunity.Competitor_Product__c);
    }

    static testMethod void CompetitorProductRollupDeleteMultipleTest() {
        Account account = new Account();
        account.Name = 'test';
        insert account;

        Opportunity opportunity = new Opportunity();
        opportunity.Name = 'test';
        opportunity.AccountId = account.Id;
        opportunity.StageName = 'Open';
        opportunity.CloseDate = Date.today();
        insert opportunity;

        Id recordTypeId = [select Id from RecordType where SObjectType = 'Competitor__c' and DeveloperName = 'Peripheral'].Id;

        List<Competitor__c> competitorList = new List<Competitor__c>();
        String product = 'Armada 14';
        Competitor__c competitor = new Competitor__c();
        competitor.Competitor__c = 'Abbott';
        competitor.Opportunity__c = opportunity.Id;
        competitor.Product__c = product;
        competitor.RecordTypeId = recordTypeId;
        competitorList.add(competitor);

        String product2 = 'Absolute Pro';
        Competitor__c competitor2 = new Competitor__c();
        competitor2.Competitor__c = 'Abbott';
        competitor2.Opportunity__c = opportunity.Id;
        competitor2.Product__c = product2;
        competitor2.RecordTypeId = recordTypeId;
        competitorList.add(competitor2);

        insert competitorList;

        Test.startTest();
            delete competitor2;
        Test.stopTest();

        opportunity = [select Competitor_Product__c from Opportunity where Id =: opportunity.Id];

        system.assert(opportunity.Competitor_Product__c.contains(product));
        system.assert(!opportunity.Competitor_Product__c.contains(product2));
    }

}