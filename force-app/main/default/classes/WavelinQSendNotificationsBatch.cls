public with sharing class WavelinQSendNotificationsBatch implements Database.Batchable<WavelinQCasesBatchHandler.Notification>, Database.Stateful {
    
    List<WavelinQCasesBatchHandler.Notification> notifications { set; get; }
    
    public WavelinQSendNotificationsBatch(List<WavelinQCasesBatchHandler.Notification> pNotifications) {
        notifications = pNotifications;
    }

    public List<WavelinQCasesBatchHandler.Notification> start(Database.BatchableContext bc) {
        return notifications;
    }

    public void execute(Database.BatchableContext bc, List<WavelinQCasesBatchHandler.Notification> notifications) {
        for (WavelinQCasesBatchHandler.Notification notification : notifications) {
            Messaging.CustomNotification obj = new Messaging.CustomNotification();
        
            // Custom notification type Id
            obj.setNotificationTypeId(notification.typeId);
            
            // when we click on the notification it will redirect to the specified targetId
            obj.setTargetId(notification.targetId);
            
            // Notification Title
            obj.setTitle(notification.title);
            
            // Notification Body
            obj.setBody(notification.body);
            
            // send used to send the notification, pass the set of user ids , Group or Queue member
            obj.send(notification.userIds);
        }
    }

    public void finish(Database.BatchableContext bc) {
        System.debug(LoggingLevel.INFO, 'Notifications sent: ' + notifications.size());
    }
}