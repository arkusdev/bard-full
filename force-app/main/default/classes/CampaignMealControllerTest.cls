@isTest
public class CampaignMealControllerTest {
	@testSetup
	static void createResources() {
		User u = [
			SELECT Id, Name, Phone, Email, Title, IsActive
			FROM User 
			WHERE Email = 'bd@apostletech.com'
		][0]; 

		System.runAs(u) {
			List<Account> accts = TestDataFactory.createAccounts(1);

			List<Contact> proctorContacts = TestDataFactory.createContacts(accts, 1);
			List<Contact> attendeeContacts = TestDataFactory.createContacts(accts, 4);
			
			List<User> bdUsers = TestDataFactory.createUsers(2);

			List<Contact> contacts = TestDataFactory.createContacts(accts, 1);
			List<User> users = TestDataFactory.createUsers(1);

			List<Campaign> campaigns = TestDataFactory.createCampaigns(accts[0], 1);

			List<CampaignMember> proctorMembers = TestDataFactory.createCampaignMembers(campaigns[0], proctorContacts, 'Proctor');
			List<CampaignMember> attendeeMembers = TestDataFactory.createCampaignMembers(campaigns[0], attendeeContacts, 'Attendee');

			List<Campaign_User__c> userMember = TestDataFactory.createCampaignUsers(campaigns[0], bdUsers);

			List<Campaign_Meal__c> meals = TestDataFactory.createCampaignMeals(campaigns[0], 1);

			TestDataFactory.createCampaignMealAttendee(meals[0], proctorMembers);
			TestDataFactory.createCampaignMealAttendee(meals[0], attendeeMembers);

			TestDataFactory.createCampaignMealAttendee(meals[0], userMember);
		}
	}

	@isTest
	static void fetchCampaignMeals_UnitTest() {
		Campaign c = [
			SELECT Id
			FROM Campaign
		][0];

		List<CampaignMealDTO> meals = CampaignMealController.fetchCampaignMealsByType(c.Id, 'Lunch');

		System.assertEquals(1, meals.size());

		for(CampaignMealDTO m :meals) {
			System.assertEquals(5, m.selectedMembers.size());
			System.assertEquals(2, m.selectedUsers.size());
		}
	}

	@isTest
	static void fetchCampaignMeal_UnitTest() {
		Campaign_Meal__c meal = [
			SELECT Id, Campaign__c
			FROM Campaign_Meal__c
			LIMIT 1
		];

		CampaignMealDTO dto = CampaignMealController.fetchCampaignMeal(meal.Id);

		System.assertEquals(meal.Campaign__c, dto.campaignId);
	}

	@isTest
	static void saveCampaignMeal_UnitTest() {
		Campaign c = [
			SELECT Id
			FROM Campaign
		][0];
		
		CampaignMealDTO dto = CampaignMealController.createNewCampaignMeal(c.Id, 'Lunch');

		dto.streetAddress = '1234 Main St';
		dto.city = 'Phoenix';
		dto.state = 'AZ';
		dto.postalCode = '85001';
		dto.country = 'US';
		dto.notes = 'Test';
		dto.additionalAttendees = 2;
		dto.locationName = 'Location';
		dto.mealDate = Date.today();
		dto.totalCost = 150.00;

		//Create members/users
		for(CampaignMealAttendeeDTO cma :dto.availableMembers) {
			dto.selectedMembers.add(new CampaignMealAttendeeDTO(cma.value, cma.label));
		}

		for(CampaignMealAttendeeDTO cma :dto.availableUsers) {
			dto.selectedUsers.add(new CampaignMealAttendeeDTO(cma.value, cma.label));
		}

		CampaignMealController.saveCampaignMeal(JSON.serialize(dto));

		System.assertEquals(c.Id, dto.campaignId);

		string whereClause = dto.type + ' - ' + dto.mealDate.format();

		//Now remove a single member/user & then add a new member and recall the save operation
		Campaign_Meal__c cm = [
			SELECT Id, Campaign__c
			FROM Campaign_Meal__c
			WHERE Name = :whereClause
			LIMIT 1
		];

		Contact con = [
			SELECT Id, Name
			FROM Contact 
			WHERE Id NOT IN (
				SELECT ContactId FROM CampaignMember WHERE CampaignId = :c.Id
			)
			LIMIT 1
		][0];

		User u = [
			SELECT Id, Name
			FROM User 
			WHERE Id NOT IN (
				SELECT User__c FROM Campaign_User__c WHERE Campaign__c = :c.Id
			)
			LIMIT 1
		][0];

		CampaignMealDTO meal = CampaignMealController.fetchCampaignMeal(cm.Id);
		
		meal.selectedMembers.remove(0);
		meal.selectedUsers.remove(0);

		meal.selectedMembers.add(new CampaignMealAttendeeDTO(con.Id, con.Name));
		meal.selectedUsers.add(new CampaignMealAttendeeDTO(u.Id, u.Name));

		meal.totalCost = 100;

		CampaignMealController.saveCampaignMeal(JSON.serialize(meal));

		//Remove them all for code coverage here
		meal.selectedMembers.clear();
		meal.selectedUsers.clear();
		
		CampaignMealController.saveCampaignMeal(JSON.serialize(meal));

		System.assertEquals(cm.Id, meal.mealId);
	}

	@isTest
	static void deleteCampaignMeal_UnitTest() {
		Campaign_Meal__c meal = [
			SELECT Id, Campaign__c
			FROM Campaign_Meal__c
			LIMIT 1
		];

		CampaignMealController.deleteCampaignMeal(meal.Id);
	}

	@isTest
	static void fetchAllStateOptions() {
		Map<string, string> states = CampaignMealController.fetchAllStateOptions();

		System.assertEquals(52, states.size());
	}
}