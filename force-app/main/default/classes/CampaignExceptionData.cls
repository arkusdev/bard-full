//https://developer.salesforce.com/blogs/2017/09/error-handling-best-practices-lightning-apex.html

//Wrapper class for custom proctor exceptions
public class CampaignExceptionData {
	public string name { get; set; }
	public string message { get; set; }
	public string code { get; set; }

	public CampaignExceptionData(string name, string message, string code) {
		this.name = name;
		this.message = message;
		this.code = code;
	}
}