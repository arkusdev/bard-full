public virtual class TriggerHandler {
	//current context of the trigger
	@testVisible
	private TriggerContext context;
	@testVisible
	private boolean isExecuting;
	
	//constructor
	public TriggerHandler() {
		this.setTriggerContext();
	}
	
	/* instance methods & props	*/
	//main method to be called during execution
	public void run() {	
		if (this.context == TriggerContext.BEFORE_INSERT) {
			this.beforeInsert();
		} else if (this.context == TriggerContext.BEFORE_UPDATE) {
			this.beforeUpdate();
		} else if (this.context == TriggerContext.BEFORE_DELETE) {
			this.beforeDelete();
		} else if (this.context == TriggerContext.AFTER_INSERT) {
			this.afterInsert();
		} else if (this.context == TriggerContext.AFTER_UPDATE) {
			this.afterUpdate();
		} else if (this.context == TriggerContext.AFTER_DELETE) {
			this.afterDelete();
		} else if (this.context == TriggerContext.AFTER_UNDELETE) {
			this.afterUndelete();
		} 
	}
		
	@testVisible
	private void setTriggerContext() {
		this.setTriggerContext(null, false);
	}
	
	@testVisible
	private void setTriggerContext(string ctx, boolean testMode) {
		if(!Trigger.isExecuting && !testMode) {
			this.isExecuting = false;
			return;
		} else {
			this.isExecuting = true;
		}
		
		if (ctx == null) { ctx = ''; }
		
		if (Trigger.isExecuting) {
			if ((Trigger.isBefore && Trigger.isInsert) || ctx == 'before insert') {
				this.context = TriggerContext.BEFORE_INSERT;
			} else if ((Trigger.isBefore && Trigger.isUpdate) || ctx == 'before update') {
				this.context = TriggerContext.BEFORE_UPDATE;
			} else if ((Trigger.isBefore && Trigger.isDelete) || ctx == 'before delete') {
				this.context = TriggerContext.BEFORE_DELETE;
			} else if ((Trigger.isAfter && Trigger.isInsert) || ctx == 'after insert') {
				this.context = TriggerContext.AFTER_INSERT;
			} else if ((Trigger.isAfter && Trigger.isUpdate) || ctx == 'after update') {
				this.context = TriggerContext.AFTER_UPDATE;
			} else if ((Trigger.isAfter && Trigger.isDelete) || ctx == 'after delete') {
				this.context = TriggerContext.AFTER_DELETE;
			} else if ((Trigger.isAfter && Trigger.isUndelete) || ctx == 'after undelete') {
				this.context = TriggerContext.AFTER_UNDELETE;
			} 
		}
	}
	
	/* overridable methods for implementation */
	@testVisible
	protected virtual void beforeInsert() {}
	
	@testVisible
	protected virtual void beforeUpdate() {}
	
	@testVisible
	protected virtual void beforeDelete() {}
	
	@testVisible
	protected virtual void afterInsert() {}
	
	@testVisible
	protected virtual void afterUpdate() {}
	
	@testVisible
	protected virtual void afterDelete() {}
	
	@testVisible
	protected virtual void afterUndelete() {}						
	 
   @testVisible
   private enum TriggerContext {
   		BEFORE_INSERT, 
   		BEFORE_UPDATE, 
   		BEFORE_DELETE,
   		AFTER_INSERT, 
   		AFTER_UPDATE, 
   		AFTER_DELETE, 
   		AFTER_UNDELETE
   }
   
   public class TriggerHandlerException extends Exception {} 
}