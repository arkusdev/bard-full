public class CampaignFlightDTO {
	@auraEnabled 
	public Id flightId { get; set; } 
	@auraEnabled 
	public Id campaignId { get; set; } 
	@auraEnabled 
	public Id attendeeId { get; set; } 
	@auraEnabled 
	public string attendeeName { get; set; } 
	@auraEnabled 
	public string name { get; set; }
	@auraEnabled 
	public string type { get; set; }
	@auraEnabled 
	public decimal cost { get; set; }
	@auraEnabled
	public string costDisplay { get; set; }
	@auraEnabled 
	public string arrivingAirline { get; set; }
	@auraEnabled 
	public string departingAirline { get; set; }
	@auraEnabled 
	public string arrivingInfo { get; set; }
	@auraEnabled 
	public string departingInfo { get; set; }
	@auraEnabled 
	public List<CampaignFlightLegDTO> flightLegs { get; set; }
	@auraEnabled 
	public Map<Id, string> availablePassengers { get; set; }
	@auraEnabled 
	public boolean isNew { get; set; }

	List<string> args = new string[] { '0','number', '###,###,##0.00' };

	public CampaignFlightDTO(Campaign_Flight__c cf) {
		this.initDTO(cf);
	}

	public CampaignFlightDTO(Campaign_Flight__c cf, Map<Id, string> availPassengers) {
		this.initDTO(cf);
		this.availablePassengers = new Map<Id, string>(availPassengers);
	}

	private void initDTO(Campaign_Flight__c cf) {
		this.flightId = cf.Id;
		this.isNew = (cf.Id == null);
		this.campaignId = cf.Campaign__c;
		this.attendeeId = (cf.Type__c.equalsIgnoreCase('Attendee') ? cf.Contact__c : cf.User__c);
		this.attendeeName = (cf.Type__c.equalsIgnoreCase('Attendee') ? cf.Contact__r.Name : cf.User__r.Name);
		this.type = cf.Type__c;
		this.name = cf.Name;
		this.cost = (cf.Cost__c != null ? cf.Cost__c : 0);

		this.flightLegs = new List<CampaignFlightLegDTO>();

		if (!cf.Campaign_Flight_Legs__r.isEmpty()) {
			for(Campaign_Flight_Leg__c cfl :cf.Campaign_Flight_Legs__r) {
				this.flightLegs.add(new CampaignFlightLegDTO(cfl));

				if (cfl.Type__c.equalsIgnoreCase('Arriving') && string.isBlank(this.arrivingAirline)) {
					this.arrivingAirline = 'Arrival: ' + cfl.Airline__c + '   |   Confirm # ' + cfl.Confirmation_Number__c;
				}

				if (cfl.Type__c.equalsIgnoreCase('Arriving') && string.isBlank(this.arrivingInfo)) {
					this.arrivingInfo = 'Dep: ' + cfl.Depart_Date_Time__c.format() + '   |   Arr: ' + cfl.Arrive_Date_Time__c.format();
				}

				if (cfl.Type__c.equalsIgnoreCase('Departing') && string.isBlank(this.departingAirline)) {
					this.departingAirline = 'Departure: ' + cfl.Airline__c + '   |   Confirm # ' + cfl.Confirmation_Number__c;
				}

				if (cfl.Type__c.equalsIgnoreCase('Departing') && string.isBlank(this.departingInfo)) {
					this.departingInfo = 'Dep: ' + cfl.Depart_Date_Time__c.format() + '   |   Arr: ' + cfl.Arrive_Date_Time__c.format();
				}
			}
		} else {
			//Always ensure there are two legs
			this.flightLegs.add(new CampaignFlightLegDTO('Arriving'));
			this.flightLegs.add(new CampaignFlightLegDTO('Departing'));
		}

		this.costDisplay = '$' + string.format(this.cost.format(), args);
	}
}