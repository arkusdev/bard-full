@isTest
public class IRSASalesSummaryControllerTest {

    @testSetup
    public static void setup () {
        Profile adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        UserRole tmRole = new UserRole(Name= 'Western - FSP - TM');
        insert tmRole;
        UserRole rmRole = new UserRole(Name= 'Peripheral West - RM');
        insert rmRole;
        UserRole csRole = new UserRole(Name= 'Peripheral West - CS', ParentRoleId = tmRole.Id);
        insert csRole;
        User u = new User(LastName = 'TestSalesExecutive',
                            Alias = 'TM tse',
                            Email = 'test.sales.executive@perdaysales.com',
                            Username = 'test.sales.executive@perdaysales.com',
                            Territory__c = '977',
                            UserRoleId = tmRole.Id,
                            ProfileId = adminProfile.Id,
                            CompanyName = 'TEST',
                            Title = 'title',
                            TimeZoneSidKey = 'America/Los_Angeles',
                            EmailEncodingKey = 'UTF-8',
                            LanguageLocaleKey = 'en_US',
                            LocaleSidKey = 'en_US'
                          );
        insert u;

        User csUser = new User(LastName = 'TestCSUser',
                            Alias = 'CS User',
                            Email = 'test.cs@perdaysales.com',
                            Username = 'test.cs@perdaysales.com',
                            UserRoleId = csRole.Id,
                            ProfileId = adminProfile.Id,
                            CompanyName = 'TEST',
                            Title = 'title',
                            TimeZoneSidKey = 'America/Los_Angeles',
                            EmailEncodingKey = 'UTF-8',
                            LanguageLocaleKey = 'en_US',
                            LocaleSidKey = 'en_US'
                          );
        insert csUser;
        
        User rm = new User(LastName = 'TestSalesExecutiveRM',
                          Alias = 'RM tse',
                          Email = 'test.sales.executive.rm@perdaysales.com',
                          Username = 'test.sales.executive.rm@perdaysales.com',
                          UserRoleId = rmRole.Id,
                          ProfileId = adminProfile.Id,
                          CompanyName = 'TEST',
                          Title = 'title',
                          TimeZoneSidKey = 'America/Los_Angeles',
                          EmailEncodingKey = 'UTF-8',
                          LanguageLocaleKey = 'en_US',
                          LocaleSidKey = 'en_US'
                         );
        insert rm;
        
        System.runAs(u) {
            RecordType irsaRT = [SELECT Id FROM RecordType WHERE Name = 'IRSA' AND SObjectType = 'Sales__c' LIMIT 1];
            Account acc1 = new Account(Name='TestAccount1');
            acc1.Ship_to_ID__c = 'ThIsIsTeStAcCoUnT1';
            acc1.BillingCity = 'City';
            insert acc1;

            List<Sales__c> sales = new List<Sales__c>();
            
            Date invoiceDate = Date.today().addDays(-1);
            Sales__c sale1 = new Sales__c();
            sale1.Sales__c = 100;
            sale1.Units__c = 3;
            sale1.OwnerId = u.Id;
            sale1.RecordTypeId = irsaRT.Id;
            sale1.Invoice_Date__c = invoiceDate;
            sale1.Account__c = acc1.Id;
            sale1.Product_Group__c = 'Product_Group2';
            sale1.CAT__c = 'CAT3';
            sale1.Territory__c = '977';

            Sales__c sale2 = new Sales__c();
            sale2.Sales__c = 100;
            sale2.Units__c = 2;
            sale2.OwnerId = u.Id;
            sale2.RecordTypeId = irsaRT.Id;
            sale2.Invoice_Date__c = invoiceDate;
            sale2.Account__c = acc1.Id;
            sale2.Product_Group__c = 'Product_Group2';
            sale2.CAT__c = 'CAT3';
            sale2.Territory__c = '977';
            
            Sales__c sale3 = new Sales__c();
            sale3.Sales__c = 100;
            sale3.Units__c = 10;
            sale3.OwnerId = u.Id;
            sale3.RecordTypeId = irsaRT.Id;
            sale3.Invoice_Date__c = invoiceDate;
            sale3.Account__c = acc1.Id;
            sale3.Product_Group__c = 'Product_Group1';
            sale3.CAT__c = 'CAT1';
            sale3.Territory__c = '977';

            Sales__c sale4 = new Sales__c();
            sale4.Sales__c = 100;
            sale4.Units__c = 12;
            sale4.OwnerId = u.Id;
            sale4.RecordTypeId = irsaRT.Id;
            sale4.Invoice_Date__c = invoiceDate;
            sale4.Account__c = acc1.Id;
            sale4.Product_Group__c = 'Product_Group1';
            sale4.CAT__c = 'CAT1';
            sale4.Territory__c = '977';


            sales.add(sale1);
            sales.add(sale2);
            sales.add(sale3);
            sales.add(sale4);
            insert sales;


            Territory__c terr = new Territory__c(Number__c='977');
            insert terr;
        }
    }

	@isTest
    public static void getPGResponse(){
        User u = [SELECT Id FROM User WHERE LastName = 'TestSalesExecutive' LIMIT 1];
        System.runAs(u){
            List<IRSASalesSummaryController.PGResponse> pgr =  IRSASalesSummaryController.getPGResponse('all', 'all');
            System.assertEquals(2, pgr.size());
            System.assertEquals('Product_Group1', pgr[0].product);
            System.assertEquals('Product_Group2', pgr[1].product);
            System.assert(200 == pgr[0].month_sales || 0 == pgr[0].month_sales);
            System.assert(200 == pgr[1].month_sales || 0 == pgr[1].month_sales);
            System.assert(5 == pgr[1].month_units || 0 == pgr[1].month_units);
            System.assert(22 == pgr[0].month_units || 0 == pgr[0].month_units);
        }
    }

    @isTest
    public static void getPGForCSResponse(){
        User u = [SELECT Id FROM User WHERE LastName = 'TestCSUser' LIMIT 1];
        System.runAs(u){
            List<IRSASalesSummaryController.PGResponse> pgr =  IRSASalesSummaryController.getPGResponse('all', 'all');
            System.assertEquals(2, pgr.size());
            System.assertEquals('Product_Group1', pgr[0].product);
            System.assertEquals('Product_Group2', pgr[1].product);
            System.assert(200 == pgr[0].month_sales || 0 == pgr[0].month_sales);
            System.assert(200 == pgr[1].month_sales || 0 == pgr[1].month_sales);
            System.assert(5 == pgr[1].month_units || 0 == pgr[1].month_units);
            System.assert(22 == pgr[0].month_units || 0 == pgr[0].month_units);
        }
    }

    @isTest
    public static void getAGResponseThisMonth(){
        User u = [SELECT Id FROM User WHERE LastName = 'TestSalesExecutive' LIMIT 1];
        System.runAs(u){
            List<IRSASalesSummaryController.AGResponse> pgr =  IRSASalesSummaryController.getAGResponse('all', 'this_month', 'all', 'all');
            System.assert(pgr.size() == 0 || pgr.size() == 1);
            System.assert(pgr.size() == 0 || 'TestAccount1' == pgr[0].accountName);
            System.assert(pgr.size() == 0 || 400 == pgr[0].accountSales);
            System.assert(pgr.size() == 0 || pgr.size() == 1); //if yesterday (all sales invoice date) it's last month the amount is 0
            System.assert(pgr.size () == 0 || 'TestAccount1' == pgr[0].accountName);
            System.assert(pgr.size () == 0 || 400 == pgr[0].accountSales);
        }
    }
    @isTest
    public static void getAGResponseThisWeek(){
        User u = [SELECT Id FROM User WHERE LastName = 'TestSalesExecutive' LIMIT 1];
        System.runAs(u){
            List<IRSASalesSummaryController.AGResponse> pgr =  IRSASalesSummaryController.getAGResponse('all', 'this_week', 'all', 'all');
            System.assert(pgr.size() == 0 || pgr.size() == 1);
            System.assert(pgr.size() == 0 || 'TestAccount1' == pgr[0].accountName);
            System.assert(pgr.size() == 0 || 400 == pgr[0].accountSales);
        }
    }
    @isTest
    public static void getAGResponseYesterday(){
        User u = [SELECT Id FROM User WHERE LastName = 'TestSalesExecutive' LIMIT 1];
        System.runAs(u){
            List<IRSASalesSummaryController.AGResponse> pgr =  IRSASalesSummaryController.getAGResponse('all', 'yesterday', 'all', 'all');
            System.assert(pgr.size() == 0 || pgr.size() == 1);
            System.assert(pgr.size() == 0 || 'TestAccount1' == pgr[0].accountName);
            System.assert(pgr.size() == 0 || 400 == pgr[0].accountSales);
        }
    }
    @isTest
    public static void getTerritories(){
        User u = [SELECT Id FROM User WHERE LastName = 'TestSalesExecutive' LIMIT 1];
        System.runAs(u){
            List<Territory__c> territories =  IRSASalesSummaryController.getTerritories();
            System.assert(territories.size() == 0 || territories.size() == 1);
            System.assert(territories.size() == 0 || '977' == territories[0].Number__c);
        }
    }
    @isTest
    public static void getUsers(){
        User u = [SELECT Id FROM User WHERE LastName = 'TestSalesExecutive' LIMIT 1];
        System.runAs(u){
            List<User> users =  IRSASalesSummaryController.getUsers();
            System.assert(users.size() == 0 || users.size() == 1);
            System.assert(users.size() == 0 || '977' == users[0].Territory__c);
        }
    }
    @isTest
    public static void getAccounts(){
        User u = [SELECT Id FROM User WHERE LastName = 'TestSalesExecutive' LIMIT 1];
        System.runAs(u){
            List<Account> accounts =  IRSASalesSummaryController.getAccounts();
            System.assert(accounts.size() == 0 || accounts.size() == 1);
            System.assert(accounts.size() == 0 || 'ThIsIsTeStAcCoUnT1' == accounts[0].Ship_to_ID__c);
        }
    }
}