public class CampaignAttendeeDocumentDTO {
	@auraEnabled 
	public Id memberDocId { get; set; } 
	@auraEnabled 
	public Id campaignDocId { get; set; } 
	@auraEnabled 
	public string campaignDocName { get; set; } 
	@auraEnabled 
	public string name { get; set; }
	@auraEnabled 
	public Id contactId { get; set; }
	@auraEnabled 
	public string contactName { get; set; }
	@auraEnabled 
	public boolean received { get; set; }

	public CampaignAttendeeDocumentDTO(Campaign_Attendee_Document__c cmd) {
		this.memberDocId = cmd.Id;
		this.campaignDocId = cmd.Campaign_Document__c;
		this.campaignDocName = cmd.Campaign_Document__r.Name;
		this.name = cmd.Name;
		this.contactId = cmd.Contact__c;
		this.contactName = cmd.Contact__r.Name;
		this.received = cmd.Received__c;
	}
}