public with sharing class IRSASalesSummaryPDFController {
    
    public String selectedAccount {get;set;}
    public String selectedUser {get;set;}
    public String selectedTimeRange {get;set;}
    public String selectedProduct {get;set;}
    public Decimal yesterdayTotal {get;set;}
    public Decimal monthTotal {get;set;}
    public Decimal weekdate1Total {get;set;}
    public Decimal weekdate2Total {get;set;}
    public Decimal weekdate3Total {get;set;}
    public Decimal weekdate4Total {get;set;}
    public Decimal weekdate5Total {get;set;}
    public Decimal weekdate6Total {get;set;}
    public Decimal thisWeekdateTotal {get;set;}
    public List<IRSASalesSummaryController.PGResponse> PGResponse {get;set;}
    public List<IRSASalesSummaryController.AGResponse> AGResponse {get;set;}
    public List<Date> endDates {get;set;}
    
    
    public IRSASalesSummaryPDFController() {
        this.selectedAccount = System.currentPageReference().getParameters().get('sa');
        this.selectedUser = System.currentPageReference().getParameters().get('su');
        this.selectedTimeRange = System.currentPageReference().getParameters().get('st');
        this.selectedProduct = System.currentPageReference().getParameters().get('sp');
        
        
        this.PGResponse = IRSASalesSummaryController.getPGResponse(this.selectedAccount,this.selectedUser);
        this.AGResponse = IRSASalesSummaryController.getAGResponse(this.selectedAccount, this.selectedTimeRange, this.selectedUser, this.selectedProduct);

        this.yesterdayTotal = 0.00;
        this.monthTotal = 0.00;
        this.weekdate1Total = 0.00;
        this.weekdate2Total = 0.00;
        this.weekdate3Total = 0.00;
        this.weekdate4Total = 0.00;
        this.weekdate5Total = 0.00;
        this.weekdate6Total = 0.00;
        this.thisWeekdateTotal = 0.00;
        for(IRSASalesSummaryController.PGResponse product : this.PGResponse) {
            this.yesterdayTotal += product.yesterday_sales; this.monthTotal += product.month_sales; this.weekdate1Total += product.weekdate1_sales; this.weekdate2Total += product.weekdate2_sales; this.weekdate3Total += product.weekdate3_sales; this.weekdate4Total += product.weekdate4_sales; this.weekdate5Total += product.weekdate5_sales; this.weekdate6Total += product.weekdate6_sales;
        }

        List<Date> endDates = new List<Date>();
        for(IRSASalesSummaryController.Range range : IRSASalesSummaryController.getWeekRanges()) {
            endDates.add(range.endWeek);
        }
        this.endDates = endDates;

        switch on (endDates.size()) {
            when 1 { this.thisWeekdateTotal = this.weekdate1Total; } when 2 { this.thisWeekdateTotal = this.weekdate2Total; } when 3 { this.thisWeekdateTotal = this.weekdate3Total; } when 4 { this.thisWeekdateTotal = this.weekdate4Total; } when 5 { this.thisWeekdateTotal = this.weekdate5Total; } when 6 { this.thisWeekdateTotal = this.weekdate6Total; }
        }
    }
}