global with sharing class WavelinQCasesSchedulableBatch implements Schedulable {
    global void execute(SchedulableContext sc){
        Database.executeBatch(new WavelinQCasesBatch(), 5000);
    }
}