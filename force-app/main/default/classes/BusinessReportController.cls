public with sharing class BusinessReportController {
	@AuraEnabled
    public static List<Opportunity> getOpportunities () {
        Date startingPoint = (Date.today()).toStartOfMonth().addMonths(-12);

        List<Opportunity> results = [SELECT Id,
                                     Name,
                                     Account.Name,
                                     CloseDate,
                                     Amount,
                                     of_Units__c,
                                     Product_Category__c,
                                     Product_Group__c
                                     FROM Opportunity
                                     WHERE OwnerId = :UserInfo.getUserId()
                                     AND StageName = 'Closed Won'
                                     AND CloseDate >=:startingPoint
                                     ORDER BY Name];
        return results;
    }

    @AuraEnabled
    public static SalesResult getSales(Datetime closeDate, String accountId) {
        Map<String, Map<String, Decimal>> sales = new Map<String, Map<String, Decimal>>();
        Map<String, Date> months = new Map<String, Date>();
        Date cd = Date.newInstance(closeDate.year(), closeDate.month(), closeDate.day());
        String userId = UserInfo.getUserId();

        String query = 'SELECT Category__c, SUM(Sales__c) total, CALENDAR_YEAR(Invoice_Date__c) year, CALENDAR_MONTH(Invoice_Date__c) month ';
        query += 'FROM Sales__c WHERE Invoice_Date__c >=:cd AND OwnerId=\'' + userId + '\' AND Account__c=\'' + accountId + '\' AND Category__c != NULL ';
        query += 'AND ((Invoice_Date__c=THIS_MONTH AND RecordType.Name=\'IRSA\') OR (Invoice_Date__c<THIS_MONTH AND RecordType.Name=\'SHAD\')) ';
        query += 'GROUP BY Category__c, CALENDAR_YEAR(Invoice_Date__c), CALENDAR_MONTH(Invoice_Date__c) ';
        query += 'ORDER BY Category__c, CALENDAR_YEAR(Invoice_Date__c), CALENDAR_MONTH(Invoice_Date__c)';

        for (AggregateResult ar : Database.query(query)) {
            String sDate = String.valueOf(ar.get('year')) + '-' + String.valueOf(ar.get('month'));
            String cat = String.valueOf(ar.get('Category__c'));
            Decimal total = (Decimal)ar.get('total');

            if (sales.containsKey(cat)) {
                sales.get(cat).put(sDate, total);
            } else {
                sales.put(cat, new Map<String, Decimal>{sDate=>total});
            }

            if (!months.containsKey(sDate)) {
                months.put(sDate, Date.newInstance(Integer.valueOf(ar.get('year')), Integer.valueOf(ar.get('month')), 1));
            }
        }
        
        SalesResult sr = new SalesResult();
        for (String saleKey : sales.keyset()) {
            List<Decimal> cmValues = new List<Decimal>();
            for (String mKey : months.keyset()) {
                Decimal catMonthValue = sales.get(saleKey).containsKey(mkey) ? sales.get(saleKey).get(mkey) : 0;
                cmValues.add(catMonthValue);
            }
            SalesWrapper sw = new SalesWrapper();
            sw.category = saleKey;
            sw.values = cmValues;
            sr.sales.add(sw);
        }
        sr.months = months.values();

        return sr;
    }
    @testVisible
    class SalesResult {
        @AuraEnabled
        public List<SalesWrapper> sales 	{ get; set; }
        @AuraEnabled
        public List<Date> months 			{ get; set; }

        public SalesResult () {
            sales = new List<SalesWrapper>();
            months = new List<Date>();
        }
    }
    @testVisible
    class SalesWrapper {
        @AuraEnabled
        public List<Decimal> values	{ get; set; }
        @AuraEnabled
        public String category				{ get; set; }

        public SalesWrapper () {
            values = new List<Decimal>();
        }
    }
}