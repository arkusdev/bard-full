@isTest
public class CampaignIncidentalControllerTest {
	@testSetup
	static void createResources() {
		User u = [
			SELECT Id, Name, Phone, Email, Title, IsActive
			FROM User 
			WHERE Email = 'bd@apostletech.com'
		][0]; 

		System.runAs(u) {
			List<Account> accts = TestDataFactory.createAccounts(1);

			List<Contact> proctorContacts = TestDataFactory.createContacts(accts, 1);
			List<Contact> attendeeContacts = TestDataFactory.createContacts(accts, 4);

			List<User> bdUsers = TestDataFactory.createUsers(2);

			List<Contact> contacts = TestDataFactory.createContacts(accts, 1);
			List<User> users = TestDataFactory.createUsers(1);

			List<Campaign> campaigns = TestDataFactory.createCampaigns(accts[0], 1);

			List<CampaignMember> proctorMembers = TestDataFactory.createCampaignMembers(campaigns[0], proctorContacts, 'Proctor');
			List<CampaignMember> attendeeMembers = TestDataFactory.createCampaignMembers(campaigns[0], attendeeContacts, 'Attendee');

			List<Campaign_User__c> userMember = TestDataFactory.createCampaignUsers(campaigns[0], bdUsers);

			List<Campaign_Incidental__c> incidentals = TestDataFactory.createCampaignIncidental(campaigns[0], 1);

			TestDataFactory.createCampaignIncidentalAttendee(incidentals[0], proctorMembers);
			TestDataFactory.createCampaignIncidentalAttendee(incidentals[0], attendeeMembers);

			TestDataFactory.createCampaignIncidentalAttendee(incidentals[0], userMember);
		}
	}

	@isTest
	static void fetchCampaignIncidentals_UnitTest() {
		Campaign c = [
			SELECT Id
			FROM Campaign
		][0];

		List<CampaignIncidentalDTO> incidentals = CampaignIncidentalController.fetchCampaignIncidentals(c.Id);

		System.assertEquals(1, incidentals.size());

		for(CampaignIncidentalDTO i :incidentals) {
			System.assertEquals(5, i.selectedMembers.size());
			System.assertEquals(2, i.selectedUsers.size());
		}
	}

	@isTest
	static void fetchCampaignIncidental_UnitTest() {
		Campaign_Incidental__c incidental = [
			SELECT Id, Campaign__c
			FROM Campaign_Incidental__c
			LIMIT 1
		];

		CampaignIncidentalDTO dto = CampaignIncidentalController.fetchCampaignIncidental(incidental.Id);

		System.assertEquals(incidental.Campaign__c, dto.campaignId);
	}

	@isTest
	static void saveCampaignIncidental_UnitTest() {
		Campaign c = [
			SELECT Id
			FROM Campaign
		][0];
		
		CampaignIncidentalDTO dto = CampaignIncidentalController.createNewCampaignIncidental(c.Id);

		dto.description = 'Test Description Incidendal';
		dto.purchaseDateTime = Datetime.now();
		dto.totalCost = 15.00;
		dto.transferOfValue = 'Consulting';
		dto.notes = 'Test notes';

		//Create members/users
		for(CampaignIncidentalAttendeeDTO cia :dto.availableMembers) {
			dto.selectedMembers.add(new CampaignIncidentalAttendeeDTO(cia.value, cia.label));
		}

		for(CampaignIncidentalAttendeeDTO cia :dto.availableUsers) {
			dto.selectedUsers.add(new CampaignIncidentalAttendeeDTO(cia.value, cia.label));
		}

		CampaignIncidentalController.saveCampaignIncidental(JSON.serialize(dto));

		System.assertEquals(c.Id, dto.campaignId);

		string whereClause = dto.description + ' - ' + dto.purchaseDateTime.format();

		//Now remove a single member/user and recall the save operation
		Campaign_Incidental__c ci = [
			SELECT Id, Campaign__c
			FROM Campaign_Incidental__c
			WHERE Name = :whereClause
			LIMIT 1
		];

		Contact con = [
			SELECT Id, Name
			FROM Contact 
			WHERE Id NOT IN (
				SELECT ContactId FROM CampaignMember WHERE CampaignId = :c.Id
			)
			LIMIT 1
		][0];

		User u = [
			SELECT Id, Name
			FROM User 
			WHERE Id NOT IN (
				SELECT User__c FROM Campaign_User__c WHERE Campaign__c = :c.Id
			)
			LIMIT 1
		][0];

		CampaignIncidentalDTO incidental = CampaignIncidentalController.fetchCampaignIncidental(ci.Id);
		
		incidental.selectedMembers.remove(0);
		incidental.selectedUsers.remove(0);

		incidental.selectedMembers.add(new CampaignIncidentalAttendeeDTO(con.Id, con.Name));
		incidental.selectedUsers.add(new CampaignIncidentalAttendeeDTO(u.Id, u.Name));

		//Test the name functionality
		incidental.description = 'Entering a long test description here in order to fire the if description.length() >= 50 code block when creating a name for this incidental';
		incidental.totalCost = 100;

		CampaignIncidentalController.saveCampaignIncidental(JSON.serialize(incidental));

		//Remove them all for code coverage here
		incidental.selectedMembers.clear();
		incidental.selectedUsers.clear();
		
		CampaignIncidentalController.saveCampaignIncidental(JSON.serialize(incidental));

		System.assertEquals(ci.Id, incidental.incidentalId);
	}

	@isTest
	static void deleteCampaignIncidental_UnitTest() {
		Campaign_Incidental__c incidental = [
			SELECT Id, Campaign__c
			FROM Campaign_Incidental__c
			LIMIT 1
		];

		CampaignIncidentalController.deleteCampaignIncidental(incidental.id);
	}
}