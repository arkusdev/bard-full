/*
    Class Name : CampaignMemberTrigger
    Created by : Apostle tech dev
*/
public class campaignMemberHandler{
    
    /*Insert method functionality
        This method used to copy the proctor name to the campaign record -- 6th Spet 2018 -- Start
    */    
    public static void afterMethods(List<CampaignMember> memberList){
        set<Id> campaignIds = new set<Id>();
        for(CampaignMember cm : memberList){
            if(cm.type__c == 'Proctor')
                campaignIds.add(cm.campaignId);
        }
        
        List<Campaign> campaignList = [select id,Course_Proctor__c, (select id,type__c,contactId,Contact.Name from campaignmembers where
                                         type__c =: 'Proctor' order by createddate ASC limit 1) 
                                        from campaign where id in: campaignids];
        
        //List<Campaign> campaignsToUpdate = new List<Campaign>();
        for(Campaign c : campaignList){
            c.Course_Proctor__c = '';
            if(c.campaignMembers.size()>0){
                if(c.campaignMembers[0].contactId != null)
                    c.Course_Proctor__c = c.campaignMembers[0].contact.Name;
            }
        }
        
        if(campaignList.size() > 0)
            update campaignList;
    }
    
    //-- 6th Spet 2018 -- END
}