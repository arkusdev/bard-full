@IsTest
private class CompetitorProductRollupBatchTest {

    static testMethod void CompetitorProductRollupBatchTest() {
        
        Account account = new Account();
        account.Name = 'test';
        insert account;

        Opportunity opportunity = new Opportunity();
        opportunity.Name = 'test';
        opportunity.AccountId = account.Id;
        opportunity.StageName = 'Open';
        opportunity.CloseDate = Date.today();
        insert opportunity;

        String product = 'Armada 14';
        
        Competitor__c competitor = new Competitor__c();
        competitor.Competitor__c = 'Abbott';
        competitor.Opportunity__c = opportunity.Id;
        competitor.Product__c = product;
        competitor.RecordTypeId = [select Id from RecordType where SObjectType = 'Competitor__c' and DeveloperName = 'Peripheral'].Id;
        insert competitor;

        opportunity = [select Competitor_Product__c from Opportunity where Id =: opportunity.Id];
        opportunity.Competitor_Product__c = null;
        update opportunity;

        Test.startTest();
            Database.executeBatch(new CompetitorProductRollupBatch());
        Test.stopTest();

        opportunity = [select Competitor_Product__c from Opportunity where Id =: opportunity.Id];

        system.assertEquals(product, opportunity.Competitor_Product__c);
    }
}