@isTest
public class CampaignCloneControllerTest {
	@testSetup
	static void createResources() {
		User u = [
			SELECT Id, Name, Phone, Email, Title, IsActive
			FROM User 
			WHERE Email = 'bd@apostletech.com'
		][0]; 

		System.runAs(u) {
			List<Account> accts = TestDataFactory.createAccounts(1);

			List<Contact> proctorContacts = TestDataFactory.createContacts(accts, 1);
			List<Contact> attendeeContacts = TestDataFactory.createContacts(accts, 4);

			List<User> bdUsers = TestDataFactory.createUsers(2);

			List<Campaign> campaigns = TestDataFactory.createCampaigns(accts[0], 1);

			TestDataFactory.createCampaignDocuments(campaigns[0], 3);

			TestDataFactory.createCampaignMembers(campaigns[0], attendeeContacts, 'Attendee');
		}
	}

	@isTest
	static void cloneCampaign_UnitTest() {
		Account a = [
			SELECT Id
			FROM Account
		][0];

		Campaign c = [
			SELECT Name, Technique__c, Classification__c, Product_Category__c, SubLedger__c, Facility__c, StartDate, Remaining_Seats__c, Description, Owner.Name, 
				(SELECT Contact.Name FROM CampaignMembers WHERE Type__c = 'Proctor')
			FROM Campaign
		][0];

		CampaignDTO dto = new CampaignDTO(c);

		dto.campaignName = 'Cloned Campaign';
		dto.facilityId = a.Id;
		dto.technique = 'CTO';
		dto.subLedger = '123456';

		Id newId = CampaignCloneController.cloneCampaign(c.Id, JSON.serialize(dto));

		Campaign c2 = [
			SELECT Id
			FROM Campaign
			WHERE Id != :c.Id
		][0];

		System.assertEquals(newId, c2.id);
	}

	@isTest
	static void initListVariables_UnitTest() {
		Campaign c = [
			SELECT Id
			FROM Campaign
		][0];

		Map<string, Map<string, string>> varMap = CampaignCloneController.initListVariables(c.Id);

		System.assertEquals(2, varMap.size());
	}
}