public class CampaignTransportationDTO {
	@auraEnabled 
	public Id transportationId { get; set; } 
	@auraEnabled 
	public Id campaignId { get; set; } 
	@auraEnabled 
	public string name { get; set; }
	@auraEnabled 
	public string dropOffLocation { get; set; }
	@auraEnabled 
	public string pickUpLocation { get; set; }
	@auraEnabled 
	public datetime pickUpDateTime { get; set; }
	@auraEnabled 
	public string reservationNumber { get; set; }
	@auraEnabled 
	public string notes { get; set; }
	@auraEnabled 
	public decimal additionalAttendees { get; set; }
	@auraEnabled 
	public decimal totalCost { get; set; }
	@auraEnabled
	public string totalCostDisplay { get; set; }
	@auraEnabled 
	public List<CampaignTransportationAttendeeDTO> availableMembers { get; set; }
	@auraEnabled 
	public List<CampaignTransportationAttendeeDTO> selectedMembers { get; set; }
	@auraEnabled 
	public List<CampaignTransportationAttendeeDTO> availableUsers { get; set; }
	@auraEnabled 
	public List<CampaignTransportationAttendeeDTO> selectedUsers { get; set; }
	@auraEnabled
	public string attendeesHelpText { get; set; }
	@auraEnabled 
	public decimal totalAttendees { get; set; }
	@auraEnabled 
	public decimal costPerAttendee { get; set; }
	@auraEnabled
	public string costPerAttendeeDisplay { get; set; }
	@auraEnabled 
	public boolean isNew { get; set; }

	List<string> args = new string[] { '0','number', '###,###,##0.00' };

	public CampaignTransportationDTO(Campaign_Transportation__c ct) {
		this.initDTO(ct);
	}

	public CampaignTransportationDTO(Campaign_Transportation__c ct, List<CampaignMember> availMembers, List<Campaign_User__c> availUsers) {
		this.initDTO(ct);

		if (!availMembers.isEmpty()) {
			for(CampaignMember am :availMembers) {
				this.availableMembers.add(new CampaignTransportationAttendeeDTO(am));
			}		
		}

		if (!availUsers.isEmpty()) {
			for(Campaign_User__c au :availUsers) {
				this.availableUsers.add(new CampaignTransportationAttendeeDTO(au));
			}		
		}
	}

	private void initDTO(Campaign_Transportation__c ct) {
		decimal cost = (ct.Cost_Per_Attendee__c != null ? ct.Cost_Per_Attendee__c : 0);

		this.transportationId = ct.Id;
		this.isNew = (ct.Id == null);
		this.campaignId = ct.Campaign__c;
		this.name = ct.Name;
		this.pickUpLocation = ct.Pickup_Location__c;
		this.pickUpDateTime = ct.Pickup_Date_Time__c;
		this.dropOffLocation = ct.Drop_Off_Location__c;
		this.reservationNumber = ct.Reservation_Number__c;
		this.notes = ct.Notes__c;
		this.totalCost = (ct.Total_Cost__c != null ? ct.Total_Cost__c : 0);
		this.additionalAttendees = (ct.Additional_Attendees__c != null ? ct.Additional_Attendees__c : 0);
		this.totalAttendees = (ct.Total_Attendees__c != null ? ct.Total_Attendees__c : 0);
		this.costPerAttendee = cost.setScale(2, RoundingMode.HALF_UP);
		this.selectedMembers = new List<CampaignTransportationAttendeeDTO>();
		this.selectedUsers = new List<CampaignTransportationAttendeeDTO>();
		this.availableMembers = new List<CampaignTransportationAttendeeDTO>();
		this.availableUsers = new List<CampaignTransportationAttendeeDTO>();
		this.attendeesHelpText = '';

		if (!ct.Campaign_Transportation_Attendees__r.isEmpty()) {
			for(Campaign_Transportation_Attendee__c cta :ct.Campaign_Transportation_Attendees__r) {
				if (cta.Contact__c != null) {
					this.selectedMembers.add(new CampaignTransportationAttendeeDTO(cta.Contact__c, cta.Contact__r.Name));
					this.attendeesHelpText += cta.Contact__r.Name + ' | ';
				} else {
					this.selectedUsers.add(new CampaignTransportationAttendeeDTO(cta.User__c, cta.User__r.Name));
					this.attendeesHelpText += cta.User__r.Name + ' | ';
				}
			}

			this.attendeesHelpText = this.attendeesHelpText.substring(0, this.attendeesHelpText.length() - 3);

			if (this.additionalAttendees > 0) {
				this.attendeesHelpText += '.  Plus ' + string.valueOf(this.additionalAttendees) + ' additional attendees.';			
			}
		} else {
			this.attendeesHelpText = 'No attendees assigned.';
		}

		this.totalCostDisplay = '$' + string.format(this.totalCost.format(), args);
		this.costPerAttendeeDisplay = '$' + string.format(this.costPerAttendee.format(), args);
	}
}