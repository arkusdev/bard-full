@isTest
public class CampaignFlightControllerTest {
	@testSetup
	static void createResources() {
		User u = [
			SELECT Id, Name, Phone, Email, Title, IsActive
			FROM User 
			WHERE Email = 'bd@apostletech.com'
		][0]; 

		System.runAs(u) {
			List<Account> accts = TestDataFactory.createAccounts(1);

			List<Contact> proctorContacts = TestDataFactory.createContacts(accts, 1);
			List<Contact> attendeeContacts = TestDataFactory.createContacts(accts, 4);

			List<User> bdUsers = TestDataFactory.createUsers(2);

			List<Campaign> campaigns = TestDataFactory.createCampaigns(accts[0], 1);

			List<CampaignMember> proctorMembers = TestDataFactory.createCampaignMembers(campaigns[0], proctorContacts, 'Proctor');
			List<CampaignMember> attendeeMembers = TestDataFactory.createCampaignMembers(campaigns[0], attendeeContacts, 'Attendee');

			List<Campaign_User__c> userMember = TestDataFactory.createCampaignUsers(campaigns[0], bdUsers);

			List<Campaign_Flight__c> proctorFlights = TestDataFactory.createCampaignFlights(campaigns[0], proctorMembers, 'Attendee');
			List<Campaign_Flight__c> attendeeFlights = TestDataFactory.createCampaignFlights(campaigns[0], attendeeMembers, 'Attendee');
			List<Campaign_Flight__c> userFlights = TestDataFactory.createCampaignFlights(campaigns[0], userMember, 'BD Staff');

			TestDataFactory.createCampaignFlightLegs(proctorFlights);
			TestDataFactory.createCampaignFlightLegs(attendeeFlights);
			TestDataFactory.createCampaignFlightLegs(userFlights);
		}
	}

	@isTest
	static void fetchCampaignFlightsByType_UnitTest() {
		Campaign c = [
			SELECT Id
			FROM Campaign
		][0];

		List<CampaignFlightDTO> attendees = CampaignFlightController.fetchCampaignFlightsByType(c.Id, 'Attendee');

		System.assertEquals(5, attendees.size());

		for(CampaignFlightDTO f :attendees) {
			System.assertEquals(2, f.flightLegs.size());
		}

		List<CampaignFlightDTO> bdUsers = CampaignFlightController.fetchCampaignFlightsByType(c.Id, 'BD Staff');

		System.assertEquals(2, bdUsers.size());

		for(CampaignFlightDTO f :bdUsers) {
			System.assertEquals(2, f.flightLegs.size());
		}
	}

	@isTest
	static void fetchCampaignFlight_UnitTest() {
		Campaign_Flight__c attendeeFlight = [
			SELECT Id, Campaign__c
			FROM Campaign_Flight__c
			WHERE Type__c = 'Attendee'
			LIMIT 1
		];

		CampaignFlightDTO attendeeFlightDTO = CampaignFlightController.fetchCampaignFlight(attendeeFlight.Id);

		System.assertEquals(attendeeFlight.Campaign__c, attendeeFlightDTO.campaignId);

		Campaign_Flight__c userFlight = [
			SELECT Id, Campaign__c
			FROM Campaign_Flight__c
			WHERE Type__c = 'BD Staff'
			LIMIT 1
		];

		CampaignFlightDTO userFlightDTO = CampaignFlightController.fetchCampaignFlight(userFlight.Id);

		System.assertEquals(userFlight.Campaign__c, userFlightDTO.campaignId);
	}

	@isTest
	static void saveCampaignFlight_UnitTest() {
		Campaign c = [
			SELECT Id
			FROM Campaign
		][0];

		List<CampaignMember> attendee = [
			SELECT ContactId, Contact.Name
			FROM CampaignMember
			WHERE CampaignId = :c.Id
			AND Type__c = 'Attendee'
			LIMIT 1		
		];
		
		CampaignFlightDTO dto = CampaignFlightController.createNewCampaignFlight(c.Id, 'Attendee');

		dto.type = 'Attendee';
		dto.attendeeName = attendee[0].Contact.Name;
		dto.cost = 500.00;
		dto.attendeeId = attendee[0].ContactId;

		for(CampaignFlightLegDTO fl :dto.flightLegs) {
			fl.name = 'Test Leg Name';
			fl.origin = 'Origin';
			fl.destination = 'Destination';
			fl.departDateTime = Datetime.now();
			fl.arriveDateTime = Datetime.now().addHours(1);
			fl.airline = 'Southwest';
			fl.confirmationNumber = '12345';
			fl.flightNumber = '12345';
			fl.gate = 'A1';
			fl.terminal = '4';
			fl.notes = '';
		}

		CampaignFlightController.saveCampaignFlight(JSON.serialize(dto));

		Campaign_Flight__c cf = [
			SELECT Id, 
				(SELECT Id FROM Campaign_Flight_Legs__r)
			FROM Campaign_Flight__c
			WHERE Name = :attendee[0].Contact.Name
			LIMIT 1
		][0];		

		System.assertNotEquals(null, cf.Id);
		System.assertEquals(2, cf.Campaign_Flight_Legs__r.size());

		CampaignFlightDTO flight = CampaignFlightController.fetchCampaignFlight(cf.Id);

		flight.cost = 555.000;

		CampaignFlightController.saveCampaignFlight(JSON.serialize(flight));
	}

	@isTest
	static void deleteCampaignFlight_UnitTest() {
		Campaign_Flight__c flight = [
			SELECT Id
			FROM Campaign_Flight__c
			LIMIT 1
		];

		CampaignFlightController.deleteCampaignFlight(flight.id);
	}
}