public with sharing class PDS_UserSelectorController {
	@AuraEnabled
    public static PerDaySalesUtil.BRUser getUsers (Id userId) {
        PerDaySalesUtil.BRUser brUser = PerDaySalesUtil.getBRUser(userId);
        getAllUser(brUser.user);
        //getAllUser(brUser.subordinates[0].user);
        return brUser;
        /*List<User> users = PerDaySalesUtil.getUsers('00561000002wHmr');
        List<User> result = new List<User>();

        Boolean isRM = users[0].UserRole.Name.endsWith('RM');
        Boolean isDM = users[0].UserRole.Name.endsWith('- DM');
        Boolean isTM = users[0].UserRole.Name.endsWith('- TM');
        if (isTM) {
            result.add(users[0]);
        } else if (isDM) {
            result.add(getAllUser(users[0]));
            for (User u : users) {
                if (u.UserRole.Name.endsWith('- TM')){
                    result.add(u);
                }
            }
        } else if (isRM) {
            result.add(getAllUser(users[0]));
            for (User u : users) {
                if (u.UserRole.Name.endsWith('- DM')){
                    result.add(u);
                }
            }
        }
        BRUser allUsers = new BRUser(isRM, result);
        return allUsers;*/
    }
    private static User getAllUser(User all) {
        all.FirstName = '';
        all.LastName = 'All';
        return all;
    }
}