public with sharing class createBackorderScript {
  private static String accountName = 'Testing account BACKORDER';
  private static String backorderName = 'Testing backorder BACKORDER';
  /**
   * @description permissionException
   * */
  public class PermissionException extends Exception {
  }
  public createBackorderScript() {
    createData();
  }

  public static void createData() {
    checkPermissions();
    Account acc = new Account(Name = accountName);
    insert acc;

    List<Backorder__c> backorders = new List<Backorder__c>();
    for (Integer i = 1; i < 45; i++) {
      Backorder__c backorder = new Backorder__c(Account__c = acc.Id);
      backorders.add(backorder);
    }

    insert backorders;
  }

  public static void deleteData() {
    checkPermissions();
    List<Backorder__c> backorders = [
      SELECT Id
      FROM Backorder__c
      WHERE Account__r.Name = :accountName
    ];

    if (backorders.size() > 0) {
      delete backorders;
    }

    Account acc = [SELECT Id FROM Account WHERE Name = :accountName LIMIT 1];

    if (acc != null) {
      delete acc;
    }
  }

  private static void checkPermissions() {
    if (
      !(Schema.sObjectType.Account.fields.Id.isAccessible() &&
      Schema.sObjectType.Account.fields.Name.isCreateable() &&
      Schema.sObjectType.Backorder__c.fields.Id.isAccessible() &&
      Schema.sObjectType.Backorder__c.fields.Account__c.isCreateable() &&
      Schema.sObjectType.Account.isDeletable() &&
      Schema.sObjectType.Backorder__c.isDeletable() 
      )
    ) {
      throw new PermissionException(
        'The user does not have sufficient permissions.'
      );
    }
  }
}