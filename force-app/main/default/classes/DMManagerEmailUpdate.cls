/*
 Purpose: To udpate the RM and DM Values when Capitol - FSP - DM role user is logged in.
*/

public class DMManagerEmailUpdate{
    
    public static void ManagerEmailUpdate(List<KOL_Nomination__c> kolList){                     
        string DMmanagerEmail = system.label.DM_KOL_Roles;
        string RMmanagerEmail = system.label.RM_KOL_Roles;
        User managerInfo = [select id,userRole.Name,email,userRoleId,name,ManagerId,Manager.Email from user where id =: userinfo.getUserId()];
         
        for(KOL_Nomination__c kn : kolList){ 
            kn.Created_By_Email__c = userInfo.getUserEmail();
            if(managerInfo.userRoleId != null && DMmanagerEmail.contains(managerInfo.userRole.Name) && managerInfo.ManagerId != null ){
                kn.DM_Manager_Email__c = managerInfo.Manager.Email;
                kn.DM_Manager__c = managerInfo.ManagerId;
                kn.DM_Email__c = managerInfo.email;
                //if(kn.Send_Approval_to_RM__c=='Yes')
                    //kn.NominationStage__c = 'RM Review';
                
            }
            else if(managerInfo.userRoleId != null && RMmanagerEmail.contains(managerInfo.userRole.Name)  && managerInfo.ManagerId != null){
                kn.RM_Manager_Email__c = managerInfo.Manager.Email;
                kn.RM_Manager__c = managerInfo.ManagerId;
                kn.RM_Email__c = managerInfo.email;
                
                if(Trigger.isInsert)
                    kn.NominationStage__c = 'RM Review';
                    
                if(kn.Send_Approval_to_RM_Manager__c=='Yes'){
                    //kn.NominationStage__c = 'VP Review';
                    Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                    req1.setComments('Submitting Approval to VP Level Users');
                    req1.setObjectId(kn.id);
                    Approval.ProcessResult result = Approval.process(req1);
                }
            }
        }
    }
    
    //Valiation for checking Attachments before sending the approval
    
    public static void attachmentValiation(list<KOL_Nomination__c> KOlList){
        set<id> kids = new set<id>();
        for(KOL_Nomination__c kl : KOlList){
            if(kl.Send_Approval_to_RM__c=='Yes' && kl.id != null)
                kids.add(kl.id);
        }
        Map<Id,ContentDOcumentLink> documentMap = new Map<Id,ContentDOcumentLink>();
        if(kids.size() > 0){
            List<ContentDOcumentLink> documentList = [select LinkedEntityId,id,contentDocumentId from ContentDOcumentLink where LinkedEntityId in: Kids];
            for(contentDocumentLink cd : documentList){
                documentMap.put(cd.LinkedEntityId,cd);
            }
        }
        for(KOL_Nomination__c  kol : KOlList){
            if(kol.Send_Approval_to_RM__c=='Yes'){                
                if(!documentMap.containsKey(kol.id))
                    kol.addError('Please ensure you have uploaded the nominee W9 and CV to the nomination form.');
            }
        }
    }
    
    //To create KOL PDF document and attach to KOL record
    @Future(callout=true)
    public static void createKOLPDF(set<id> kolids)
    {
        List<KOL_Nomination__c> KOlList = [select id,NominationStage__c from KOL_Nomination__c where id in: kolids];
        
        List<ContentVersion> versionList = new List<ContentVersion>();
        for(KOL_Nomination__c kl : KOlList){
            if(kl.NominationStage__c == 'Complete'){                    
                PageReference pdf = Page.KOLNominationPDF;
                pdf.getParameters().put('id',kl.id);
                
                Blob body;
                if(!Test.isRunningTest())
                    body = pdf.getContent();
                else
                    body = blob.valueOf('unit test');
                
                ContentVersion conVer = new ContentVersion();
                conVer.ContentLocation = 'S';
                conVer.PathOnClient = 'Nomination.pdf';
                conVer.Title = 'KOL Nomination';
                conVer.VersionData = body;
                versionList.add(conVer);
            }
        }
        
        if(!versionList.isEmpty())
            insert versionList;        
        
        List<ContentVersion> afterVersionList = [select contentDocumentId,id from contentversion where id in: versionList];
        Map<Id,Id> documentLinkMap = new Map<Id,Id>();
        for(ContentVersion cv : afterVersionList){
            documentLinkMap.put(cv.id,cv.contentDocumentId);
        }
        
        List<ContentDocumentLink> documentLinkList = new List<ContentDocumentLink>();
        for(KOL_Nomination__c kl : KOlList){
            for(ContentVersion cv : versionList){
                if(documentLinkMap.containsKey(cv.id)){
                    ContentDocumentLink cdl = new ContentDocumentLink();
                    cdl.ContentDocumentId = documentLinkMap.get(cv.id);
                    cdl.LinkedEntityId = kl.id;
                    cdl.ShareType = 'I';
                    documentLinkList.add(cdl);
                }    
            }
        }
        
        if(!documentLinkList.isEmpty())
            insert documentLinkList;
    }

}