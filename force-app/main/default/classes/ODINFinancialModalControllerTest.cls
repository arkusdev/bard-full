@IsTest
private class ODINFinancialModalControllerTest {

    static testMethod void GetRegionOptionsTest() {
        Project__c project = new Project__c();
        project.Name = 'Test_1';
        insert project;

        Test.startTest();
            Map<String, String> regionList = ODINFinancialModalController.GetRegionOptions(project.Id);
        Test.stopTest();

        system.assertEquals(Financial__c.Region__c.getDescribe().getPicklistValues().size(), regionList.size());
    }

    static testMethod void GetInterestOptionsTest() {
        
        Project__c project = new Project__c();
        project.Name = 'Test_2';
        insert project;

        Schema.DescribeFieldResult fieldResult = Interest__c.Level__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<String> levelList = new List<String>();
		for( Schema.PicklistEntry pickListVal : ple)
			levelList.add(pickListVal.getLabel());

        Interest__c interest = new Interest__c();
        interest.Project__c = project.Id;
        interest.Level__c = levelList[0];
        interest.Region__c = Financial__c.Region__c.getDescribe().getPicklistValues()[0].getLabel();
        insert interest;

        Test.startTest();
            ODINFinancialModalController.Interest innerInterest = ODINFinancialModalController.GetInterestOptions(project.Id, interest.Region__c);
        Test.stopTest();

        system.assertEquals(levelList.size(), innerInterest.levels.size());
        system.assert(innerInterest.preSelectedLevel != null);
        system.assertEquals(interest.Id, innerInterest.preSelectedLevel.Id);
    }

    static testMethod void GetProjectQuestionsTest() {

        Project__c project = new Project__c();
        project.Name = 'Test_3';
        insert project;

        Question__c question = new Question__c();
        question.Question__c = 'Le question';
        question.Project__c = project.Id;
        question.Show_In_Global_Market_Assessment__c = true;
        insert question;

        Answer__c answer1 = new Answer__c();
        answer1.Question__c = question.Id;
        answer1.Answer__c = 'Answer 1';
        answer1.Region__c = Financial__c.Region__c.getDescribe().getPicklistValues()[0].getLabel();
        insert answer1;

        Test.startTest();
            ODINFinancialModalController.QuestionWrapper wrapper = ODINFinancialModalController.GetProjectQuestions(project.Id, answer1.Region__c);
        Test.stopTest();

        system.assert(wrapper.questions != null);

        for (ODINFinancialModalController.QuestionWAnswer qwa : wrapper.questions) {
            if (qwa.question.Id == question.Id)
                system.assertEquals(answer1.Id, qwa.answer.Id);
        }
    }

    static testMethod void GetFinancialInfoTest() {

        List<String> yearList = new List<String>();
        Schema.DescribeFieldResult fieldResult = Financial__c.Year__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : ple) 
			yearList.add(pickListVal.getLabel());
        
        Project__c project = new Project__c();
        project.Name = 'Test_4';
        project.Launch_Year__c = yearList[0];
        insert project;

        RecordType recordType = [select Id 
                                from RecordType 
                                where SObjectType = 'Financial__c' 
                                and RecordType.DeveloperName = 'Global_Market_Assessment'];  

        String region = Financial__c.Region__c.getDescribe().getPicklistValues()[0].getLabel();

        Test.startTest();
            ODINFinancialModalController.FinancialInfoWrapper wrapper = ODINFinancialModalController.GetFinancialInfo(project.Id, region);
        Test.stopTest();

        system.assertEquals(yearList.size(), wrapper.years.size());
        system.assertEquals(project.Launch_Year__c, wrapper.startYear);
        system.assertEquals(project.Name, wrapper.financial.name);
    }

    static testMethod void CreateFinancialInfoTest() {
        Project__c project = new Project__c();
        project.Name = 'Test_5';
        insert project;

        List<String> yearList = new List<String>();
        Schema.DescribeFieldResult fieldResult = Financial__c.Year__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : ple) 
			yearList.add(pickListVal.getLabel());

        RecordType recordType = [select Id 
                                from RecordType 
                                where SObjectType = 'Financial__c' 
                                and RecordType.DeveloperName = 'Global_Market_Assessment'];  

        String region = Financial__c.Region__c.getDescribe().getPicklistValues()[0].getLabel();
        List<Financial__c> financialList = new List<Financial__c>();
        for (Integer i = 0; i < 5; i++) {
            Financial__c financial = new Financial__c();
            financial.Project__c = project.Id;
            financial.Region__c = region;
            financial.Year__c = String.valueOf(Integer.valueOf(yearList[0]) + i);
            financial.RecordTypeId = recordType.Id;
            financialList.add(financial);
        }
        insert financialList;

        ODINFinancialModalController.FinancialInfoWrapper wrapper = ODINFinancialModalController.GetFinancialInfo(project.Id, region);

        String json = JSON.serialize(wrapper);

        Test.startTest();
            wrapper = ODINFinancialModalController.CreateFinancialInfo(project.Id, region, json);
        Test.stopTest();

        system.assertEquals(5, wrapper.years.size());
        system.assertEquals(5, wrapper.financialsProjectPerYearList.size());
        
    }

    static testMethod void UpdateFinancialInfoTest() {
        Project__c project = new Project__c();
        project.Name = 'Test_6';
        insert project;


        List<String> yearList = new List<String>();
        Schema.DescribeFieldResult fieldResult = Financial__c.Year__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : ple) {
			yearList.add(pickListVal.getLabel());
            break;
        }

        String region = Financial__c.Region__c.getDescribe().getPicklistValues()[0].getLabel();

        ODINFinancialModalController.FinancialInfoWrapper wrapper = ODINFinancialModalController.GetFinancialInfo(project.Id, region);

        wrapper.startYear = yearList[0];

        String jsonFinancial = JSON.serialize(wrapper);

        wrapper = ODINFinancialModalController.CreateFinancialInfo(project.Id, region, jsonFinancial);

        jsonFinancial = JSON.serialize(wrapper);

        fieldResult = Interest__c.Level__c.getDescribe();
		ple = fieldResult.getPicklistValues();
        List<String> levelList = new List<String>();
		for( Schema.PicklistEntry pickListVal : ple) {
			levelList.add(pickListVal.getLabel());
            break;
        }

        Interest__c interest = new Interest__c();
        interest.Project__c = project.Id;
        interest.Level__c = levelList[0];
        interest.Region__c = region;

        Map<Id, Question__c> questionMap = new Map<Id, Question__c>([select Id from Question__c where Project__c =: project.Id and Show_In_Global_Market_Assessment__c = true]);
        List<Answer__c> answerList = new List<Answer__c>();
        for (Question__c question : questionMap.values()) {
            Answer__c answer = new Answer__c();
            answer.Question__c = question.Id;
            answer.Answer__c = 'Answer ' + question.Id;
            answer.Region__c = region;
            answerList.add(answer);
        }

        Test.startTest();
            ODINFinancialModalController.UpdateFinancialInfo(jsonFinancial, interest, answerList);
        Test.stopTest();

        List<Interest__c> interestList = [select Id 
                                            from Interest__c 
                                            where Project__c =: project.Id 
                                            and Region__c =: region 
                                            and Level__c =: levelList[0]];
        answerList = [select Question__c 
                        from Answer__c 
                        where Question__c IN: questionMap.keySet()];

        RecordType recordType = [select Id 
                                from RecordType 
                                where SObjectType = 'Financial__c' 
                                and RecordType.DeveloperName = 'Global_Market_Assessment'];

        List<Financial__c> financialList = [select Id 
                                            from Financial__c 
                                            where Project__c =: project.Id
                                            and RecordTypeId =: recordType.Id];
        
        system.assertEquals(1, interestList.size());
        system.assertEquals(questionMap.size(), answerList.size());
        
        for (Answer__c answer : answerList)
            system.assert(questionMap.containsKey(answer.Question__c));

        system.assertEquals(5, financialList.size());
    }

    static testMethod void getFinancialInfoByProjectAndRegionTest() {
        Project__c project = new Project__c();
        project.Name = 'Test_1';
        insert project;

        RecordType recordType = [select Id 
                                from RecordType 
                                where SObjectType = 'Financial__c' 
                                and RecordType.DeveloperName = 'Global_Market_Assessment'];  

        Financial__c financial = new Financial__c();
        financial.Project__c = project.Id;
        financial.Region__c = 'United States';
        financial.Year__c = String.valueOf(Date.today().year());
        financial.RecordTypeId = recordType.Id;
        insert financial;

        Test.startTest();
            ODINFinancialModalController.FinancialInfo financialInfo =  ODINFinancialModalController.getFinancialInfoByProjectAndRegion(project.Id, 'United States');
        Test.stopTest();
        System.assert(!financialInfo.FinancialInfoWrapper.financialsProjectPerYearList.isEmpty(), 'No financials were added');
    }
}