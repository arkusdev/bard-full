public class CampaignMealDTO {
	@auraEnabled 
	public Id mealId { get; set; } 
	@auraEnabled 
	public Id campaignId { get; set; } 
	@auraEnabled 
	public string name { get; set; }
	@auraEnabled 
	public string locationName { get; set; }
	@auraEnabled 
	public date mealDate { get; set; }
	@auraEnabled 
	public string mealDateDisplay { get; set; }
	@auraEnabled 
	public decimal totalCost { get; set; }
	@auraEnabled
	public string totalCostDisplay { get; set; }
	@auraEnabled 
	public string type { get; set; }
	@auraEnabled 
	public string streetAddress { get; set; }
	@auraEnabled 
	public string city { get; set; }
	@auraEnabled 
	public string state { get; set; }
	@auraEnabled 
	public string postalCode { get; set; }
	@auraEnabled 
	public string country { get; set; }
	@auraEnabled 
	public string notes { get; set; }
	@auraEnabled 
	public decimal additionalAttendees { get; set; }
	@auraEnabled 
	public List<CampaignMealAttendeeDTO> availableMembers { get; set; }
	@auraEnabled 
	public List<CampaignMealAttendeeDTO> selectedMembers { get; set; }
	@auraEnabled 
	public List<CampaignMealAttendeeDTO> availableUsers { get; set; }
	@auraEnabled 
	public List<CampaignMealAttendeeDTO> selectedUsers { get; set; }
	@auraEnabled
	public string attendeesHelpText { get; set; }
	@auraEnabled 
	public decimal totalAttendees { get; set; }
	@auraEnabled 
	public decimal costPerAttendee { get; set; }
	@auraEnabled
	public string costPerAttendeeDisplay { get; set; }
	@auraEnabled 
	public boolean isNew { get; set; }

	List<string> args = new string[] { '0','number', '###,###,##0.00' };

	public CampaignMealDTO(Campaign_Meal__c cm) {
		this.initDTO(cm);
	}

	public CampaignMealDTO(Campaign_Meal__c cm, List<CampaignMember> availMembers, List<Campaign_User__c> availUsers) {
		this.initDTO(cm);

		if (!availMembers.isEmpty()) {
			for(CampaignMember am :availMembers) {
				this.availableMembers.add(new CampaignMealAttendeeDTO(am));
			}		
		}

		if (!availUsers.isEmpty()) {
			for(Campaign_User__c au :availUsers) {
				this.availableUsers.add(new CampaignMealAttendeeDTO(au));
			}		
		}
	}

	private void initDTO(Campaign_Meal__c cm) {
		decimal cost = (cm.Cost_Per_Attendee__c != null ? cm.Cost_Per_Attendee__c : 0);

		this.mealId = cm.Id;
		this.isNew = (cm.Id == null);
		this.campaignId = cm.Campaign__c;
		this.name = cm.Name;
		this.locationName = cm.Location_Name__c;
		this.mealDate = (cm.Meal_Date__c != null ? cm.Meal_Date__c : date.today());
		this.mealDateDisplay = this.mealDate.format();
		this.totalCost = (cm.Total_Cost__c != null ? cm.Total_Cost__c : 0);
		this.type = cm.Type__c;
		this.streetAddress = cm.Street_Address__c;
		this.city = cm.City__c;
		this.state = cm.State__c;
		this.postalCode = cm.Postal_Code__c;
		this.country = (string.isBlank(cm.Country__c) ? 'US' : cm.Country__c);
		this.notes = cm.Notes__c;
		this.additionalAttendees = (cm.Additional_Attendees__c != null ? cm.Additional_Attendees__c : 0);
		this.totalAttendees = (cm.Total_Attendees__c != null ? cm.Total_Attendees__c : 0);
		this.costPerAttendee = cost.setScale(2, RoundingMode.HALF_UP);
		this.selectedMembers = new List<CampaignMealAttendeeDTO>();
		this.selectedUsers = new List<CampaignMealAttendeeDTO>();
		this.availableMembers = new List<CampaignMealAttendeeDTO>();
		this.availableUsers = new List<CampaignMealAttendeeDTO>();
		this.attendeesHelpText = '';

		if (!cm.Campaign_Meal_Attendees__r.isEmpty()) {
			for(Campaign_Meal_Attendee__c cma :cm.Campaign_Meal_Attendees__r) {
				if (cma.Contact__c != null) {
					this.selectedMembers.add(new CampaignMealAttendeeDTO(cma.Contact__c, cma.Contact__r.Name));
					this.attendeesHelpText += cma.Contact__r.Name + ' | ';
				} else {
					this.selectedUsers.add(new CampaignMealAttendeeDTO(cma.User__c, cma.User__r.Name));
					this.attendeesHelpText += cma.User__r.Name + ' | ';
				}
			}

			this.attendeesHelpText = this.attendeesHelpText.substring(0, this.attendeesHelpText.length() - 3);

			if (this.additionalAttendees > 0) {
				this.attendeesHelpText += '.  Plus ' + string.valueOf(this.additionalAttendees) + ' additional attendees.';			
			}
		} else {
			this.attendeesHelpText = 'No attendees assigned.';
		}

		this.totalCostDisplay = '$' + string.format(this.totalCost.format(), args);
		this.costPerAttendeeDisplay = '$' + string.format(this.costPerAttendee.format(), args);
	}
}