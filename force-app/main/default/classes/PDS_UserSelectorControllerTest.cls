@isTest
public class PDS_UserSelectorControllerTest {
    @testSetup
    public static void setup() {
        Test.startTest();
        PerDaySalesTestUtil.createData();
        Test.stopTest();
    }
    @isTest
    public static void getUsersTM () {
        User tm = PerDaySalesTestUtil.getTMUser();
        PerDaySalesUtil.BRUser result;
        System.runAs(tm) {
            result = PDS_UserSelectorController.getUsers(tm.Id);
        }
        System.assertNotEquals(null, result);
    }
    @isTest
    public static void getUsersDM () { 
        User dm = PerDaySalesTestUtil.getDMUser();
        PerDaySalesUtil.BRUser result;
        System.runAs(dm) {
            result = PDS_UserSelectorController.getUsers(dm.Id);
        }
        System.assertNotEquals(null, result);
    }
}