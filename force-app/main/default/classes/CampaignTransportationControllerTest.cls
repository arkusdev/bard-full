@isTest
public class CampaignTransportationControllerTest {
	@testSetup
	static void createResources() {
		User u = [
			SELECT Id, Name, Phone, Email, Title, IsActive
			FROM User 
			WHERE Email = 'bd@apostletech.com'
		][0]; 

		System.runAs(u) {
			List<Account> accts = TestDataFactory.createAccounts(1);

			List<Contact> proctorContacts = TestDataFactory.createContacts(accts, 1);
			List<Contact> attendeeContacts = TestDataFactory.createContacts(accts, 4);

			List<User> bdUsers = TestDataFactory.createUsers(2);

			List<Contact> contacts = TestDataFactory.createContacts(accts, 1);
			List<User> users = TestDataFactory.createUsers(1);

			List<Campaign> campaigns = TestDataFactory.createCampaigns(accts[0], 1);

			List<CampaignMember> proctorMembers = TestDataFactory.createCampaignMembers(campaigns[0], proctorContacts, 'Proctor');
			List<CampaignMember> attendeeMembers = TestDataFactory.createCampaignMembers(campaigns[0], attendeeContacts, 'Attendee');

			List<Campaign_User__c> userMember = TestDataFactory.createCampaignUsers(campaigns[0], bdUsers);

			List<Campaign_Transportation__c> trans = TestDataFactory.createCampaignTransportations(campaigns[0], 1);

			TestDataFactory.createCampaignTransportationAttendee(trans[0], proctorMembers);
			TestDataFactory.createCampaignTransportationAttendee(trans[0], attendeeMembers);

			TestDataFactory.createCampaignTransportationAttendee(trans[0], userMember);
		}
	}

	@isTest
	static void fetchCampaignTransportations_UnitTest() {
		Campaign c = [
			SELECT Id
			FROM Campaign
		][0];

		List<CampaignTransportationDTO> trans = CampaignTransportationController.fetchCampaignTransportations(c.Id);

		System.assertEquals(1, trans.size());

		for(CampaignTransportationDTO t :trans) {
			System.assertEquals(5, t.selectedMembers.size());
			System.assertEquals(2, t.selectedUsers.size());
		}
	}

	@isTest
	static void fetchCampaignTransportation_UnitTest() {
		Campaign_Transportation__c tran = [
			SELECT Id, Campaign__c
			FROM Campaign_Transportation__c
			LIMIT 1
		];

		CampaignTransportationDTO dto = CampaignTransportationController.fetchCampaignTransportation(tran.Id);

		System.assertEquals(tran.Campaign__c, dto.campaignId);
	}

	@isTest
	static void saveCampaignTransportation_UnitTest() {
		Campaign c = [
			SELECT Id
			FROM Campaign
		][0];
		
		CampaignTransportationDTO dto = CampaignTransportationController.createNewCampaignTransportation(c.Id);

		dto.pickUpLocation = 'Pick Up Location';
		dto.pickUpDateTime = Datetime.now();
		dto.dropOffLocation = 'Drop Off Location';
		dto.reservationNumber = '12345';
		dto.notes = 'Test notes';
		dto.additionalAttendees = 2;
		dto.totalCost = 150.00;

		//Create members/users
		for(CampaignTransportationAttendeeDTO cta :dto.availableMembers) {
			dto.selectedMembers.add(new CampaignTransportationAttendeeDTO(cta.value, cta.label));
		}

		for(CampaignTransportationAttendeeDTO cta :dto.availableUsers) {
			dto.selectedUsers.add(new CampaignTransportationAttendeeDTO(cta.value, cta.label));
		}

		CampaignTransportationController.saveCampaignTransportation(JSON.serialize(dto));

		System.assertEquals(c.Id, dto.campaignId);

		string whereClause = dto.pickUpLocation + ' - ' + dto.dropOffLocation;

		//Now remove a single member/user and recall the save operation
		Campaign_Transportation__c ct = [
			SELECT Id, Campaign__c
			FROM Campaign_Transportation__c
			WHERE Name = :whereClause
			LIMIT 1
		];

		Contact con = [
			SELECT Id, Name
			FROM Contact 
			WHERE Id NOT IN (
				SELECT ContactId FROM CampaignMember WHERE CampaignId = :c.Id
			)
			LIMIT 1
		][0];

		User u = [
			SELECT Id, Name
			FROM User 
			WHERE Id NOT IN (
				SELECT User__c FROM Campaign_User__c WHERE Campaign__c = :c.Id
			)
			LIMIT 1
		][0];

		CampaignTransportationDTO tran = CampaignTransportationController.fetchCampaignTransportation(ct.Id);
		
		tran.selectedMembers.remove(0);
		tran.selectedUsers.remove(0);

		tran.selectedMembers.add(new CampaignTransportationAttendeeDTO(con.Id, con.Name));
		tran.selectedUsers.add(new CampaignTransportationAttendeeDTO(u.Id, u.Name));

		tran.totalCost = 300;

		CampaignTransportationController.saveCampaignTransportation(JSON.serialize(tran));

		//Remove them all for code coverage here
		tran.selectedMembers.clear();
		tran.selectedUsers.clear();
		
		CampaignTransportationController.saveCampaignTransportation(JSON.serialize(tran));

		System.assertEquals(ct.Id, tran.transportationId);
	}

	@isTest
	static void deleteCampaignTransportation_UnitTest() {
		Campaign_Transportation__c tran = [
			SELECT Id, Campaign__c
			FROM Campaign_Transportation__c
			LIMIT 1
		];

		CampaignTransportationController.deleteCampaignTransportation(tran.Id);
	}
}