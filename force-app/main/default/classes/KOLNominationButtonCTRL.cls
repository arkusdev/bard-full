public without sharing class KOLNominationButtonCTRL
{
    @AuraEnabled
    public static KOLwrapper checkProfiles(string KOLId)
    {        
        KOLwrapper wrap = new KOLwrapper();
        KOL_Nomination__c kolInfo = [select id,Send_Approval_to_RM__c,RM_Comments__c,Send_Approval_to_RM_Manager__c,NominationStage__c,Reviewed_Rejected_By__c from KOL_Nomination__c where id =: KOLId];
        List<ContentDOcumentLink> documentList = [select LinkedEntityId,id,contentDocumentId from ContentDOcumentLink where LinkedEntityId =: KOLId];
        wrap.kolInfo = kolInfo;
        wrap.DMRMName = '';
        wrap.hasKOLFile = documentList.size()>0 ? true : false;
        string DMmanagerEmail = system.label.DM_KOL_Roles;
        string RMmanagerEmail = system.label.RM_KOL_Roles;
        User managerInfo = [select id,userRole.Name,userRoleId,name,ManagerId,Manager.Email from user where id =: userinfo.getUserId()];
        if(managerInfo.userRoleId != null && DMmanagerEmail.contains(managerInfo.userRole.Name) && managerInfo.ManagerId != null ){
            wrap.DMRMName = managerInfo.userRole.Name;
        }
        else if(managerInfo.userRoleId != null && RMmanagerEmail.contains(managerInfo.userRole.Name)  && managerInfo.ManagerId != null){
            wrap.DMRMName = managerInfo.userRole.Name;
        }
        system.debug(wrap);
        return wrap;
    }
    
    public class KOLwrapper
    {
        @AuraEnabled public KOL_Nomination__c kolInfo{get;set;}
        @AuraEnabled public string DMRMName{get;set;}
        @AuraEnabled public boolean hasKOLFile{get;set;}
    }
    
    @AuraEnabled
    public static string changeKOLStatus(KOL_Nomination__c kolInfo){
        try{
            if(kolInfo != null)
                update kolInfo;
            return 'Success';
        }
        Catch(Exception e){
            return 'Fail :'+e.getMessage();
        }
    }
    
    @AuraEnabled
    public static string updateRMCommentsApex(KOL_Nomination__c kolInfo){
        try{
            if(kolInfo != null)
                update kolInfo;
            return 'Success';
        }
        Catch(Exception e){
            return 'Fail :'+e.getMessage();
        }
    }
}