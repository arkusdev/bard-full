global class SalesDataEmailNotificationBatch implements Database.Batchable<sObject>, Database.Stateful {
	
    global EmailTemplate template { get; set; }
    global Sales_Data_Email_Notification__c settings {get; set; }
    global Set<Id> userRolesToSendEmail {get; set; }
	
	global SalesDataEmailNotificationBatch() {
		settings = [SELECT Email_Template_Id__c, Role_And_Below_Id__c, Email_Template_Id_For_List_of_Emails__c, Last_Notification_Sent__c FROM Sales_Data_Email_Notification__c][0];
		template = [SELECT Id FROM EmailTemplate WHERE Id = :settings.Email_Template_Id__c];
		userRolesToSendEmail = SalesDataEmailNotificationUtils.getAllSubRoleIds(new Set<Id>{settings.Role_And_Below_Id__c});
		userRolesToSendEmail.add(settings.Role_And_Below_Id__c);//add the parent role
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id, Email, FirstName, LastName FROM User WHERE UserRoleId IN :userRolesToSendEmail';
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<User> scope) {
        List<BellNotificationForUser__e> notifications = new List<BellNotificationForUser__e>();
   		Map<Id,Contact> userTotempContactsMap = new Map<Id,Contact>();
		List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage>();
		for(User u: scope){
			Contact tempContact = new Contact(
	            Email=u.Email,
	            FirstName=u.FirstName,
	            LastName=u.LastName+'Sales Data Email Notification Batch: Delete',
	            Title='Sales Data Email Notification Batch: Delete'
	        );
	        userTotempContactsMap.put(u.Id,tempContact);
            notifications.add(new BellNotificationForUser__e(UserID__c=u.Id));
		}
		insert userTotempContactsMap.values();
		for(User u: scope){
			emailsToSend.add(SalesDataEmailNotificationUtils.CreateEmail(userTotempContactsMap.get(u.Id).Id, userTotempContactsMap.get(u.Id).Email, template.Id));
		}
       	Messaging.sendEmail(emailsToSend);
        EventBus.publish(notifications);
	}
	
	global void finish(Database.BatchableContext BC) {
		//check if there are more emails in the CMDT 
		List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage>();

		List<EmailTemplate> templates = [SELECT Id FROM EmailTemplate WHERE Id = :settings.Email_Template_Id_For_List_of_Emails__c];
		if(!templates.isEmpty()){
			List<Sales_Data_Email_Notification_Emails__mdt> emailList = [SELECT Email__c FROM Sales_Data_Email_Notification_Emails__mdt];
			for(Sales_Data_Email_Notification_Emails__mdt email: emailList){
				emailsToSend.add(SalesDataEmailNotificationUtils.CreateEmail(null, email.Email__c, templates[0].Id));
			}
			Messaging.sendEmail(emailsToSend);
		}

		//set the day to today
		settings.Last_Notification_Sent__c = Date.today();
		update settings;

		//delete temp contacts
		List<Contact> contacts = [SELECT Id FROM Contact WHERE Title='Sales Data Email Notification Batch: Delete' AND CreatedDate=TODAY];
        delete contacts;
	}
}