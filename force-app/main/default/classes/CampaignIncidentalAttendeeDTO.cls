public class CampaignIncidentalAttendeeDTO {
	@auraEnabled 
	public Id value { get; set; }
	@auraEnabled 
	public string label { get; set; }

	public CampaignIncidentalAttendeeDTO(Id attendeeId, string attendeeName) {
		this.value = attendeeId;
		this.label = attendeeName;
	}

	public CampaignIncidentalAttendeeDTO(CampaignMember cm) {
		this.value = cm.ContactId;
		this.label = cm.Contact.Name;
	}

	public CampaignIncidentalAttendeeDTO(Campaign_User__c cu) {
		this.value = cu.User__c;
		this.label = cu.User__r.Name;
	}
}