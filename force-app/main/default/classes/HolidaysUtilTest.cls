@isTest
public class HolidaysUtilTest {
    @isTest
    public static void testHolidaysUtil () {
        Date activity = Date.today().addMonths(-1);
        Date startDate = Date.today().toStartOfMonth().addYears(-1);
        Date endDate = Date.today();
        String monthOfYear;
        for (String key : HolidaysUtil.getMonthMap().keyset()) {
            if (HolidaysUtil.getMonthMap().get(key) == activity.month()) monthOfYear = key;
        }
        Holiday h = new Holiday(
            ActivityDate=activity,
            RecurrenceStartDate=activity,
            RecurrenceDayOfMonth=1,
            RecurrenceDayOfWeekMask=1,
            RecurrenceMonthOfYear=monthOfYear,
            RecurrenceInterval=10,
            RecurrenceInstance='First'
        );

        Map<Date, Holiday> mapresult = HolidaysUtil.getHolidaysForDates(startDate, endDate, false);
        System.assertNotEquals(NULL, mapresult);

        List<Date> result = HolidaysUtil.getMonthlyHoliday(h, startDate, endDate);
        System.assert(result.size() > 0);

        result = HolidaysUtil.getMonthlyNthHoliday(h, startDate, endDate);
        System.assert(result.size() > 0);
        
        h.RecurrenceInstance = 'Last';
        result = HolidaysUtil.getMonthlyNthHoliday(h, startDate, endDate);
        System.assert(result.size() > 0);
        
        result = HolidaysUtil.getWeeklyHoliday(h, startDate, endDate);
        System.assert(result.size() > 0);
        
        result = HolidaysUtil.getDailyHoliday(h, startDate, endDate);
        System.assert(result.size() > 0);
        
        List<Integer> bits = HolidaysUtil.seperateBitMask(64);
        system.assertEquals(64, bits.get(0));
    }
}