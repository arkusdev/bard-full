<apex:page controller="HCPReportController" showHeader="false" applyBodyTag="false" contentType="application/vnd.ms-excel#{!reportName}.xls">
    
    <head>
        <style>
            table, th, td { border: 1px solid black; border-collapse: collapse; }
        </style>
    </head>

    <apex:form >
        
        <table border="1">
            <tr>
                <th colspan="2" style="text-align:left;">Course Name</th>
                <th colspan="10" style="text-align:left;">{!campInfo.name}</th>
            </tr>
            <tr>
                <th colspan="2" style="text-align:left;">Subledger</th>
                <th colspan="10" style="text-align:left;">{!campInfo.SubLedger__c}</th>
            </tr>
            <tr>
                <th colspan="5" Style="text-align:center;background-color:#ddd">Name</th>
                <th Style="text-align:center"></th>
                <th colspan="2" Style="text-align:center;background-color:#ddd">Destination</th>
                <th Style="text-align:center"></th>
                <th Style="text-align:center"></th>
                <th Style="text-align:center"></th>
                <th Style="text-align:center"></th>
            </tr>
            <tr>
                <th Style="text-align:center">Covered Recipient Type</th>
                <th Style="text-align:center">Last, or Hospital</th>
                <th Style="text-align:center">First</th>
                <th Style="text-align:center">Middle initial</th>
                <th Style="text-align:center">Suffix</th>
                <th Style="text-align:center">Bard Global ID #</th>
                <th Style="text-align:center">City</th>
                <th Style="text-align:center">State or Country</th>
                <th Style="text-align:center">Transfer of Value</th>
                <th Style="text-align:center">Product Group</th>
                <th Style="text-align:center">Marketed Name</th>
                <th Style="text-align:center">$ Value</th>
            </tr>
            
            <apex:repeat value="{!mealsInfo.response}" var="meal">
                <tr>
                    <td>{!meal.coveredRecipientType}</td>
                    <td>{!meal.lastOrHospital}</td>
                    <td>{!meal.firstName}</td>
                    <td>{!meal.middleName}</td>
                    <td>{!meal.suffix}</td>
                    <td>{!meal.bardGlobalId}</td>
                    <td>{!meal.city}</td>
                    <td>{!meal.stateOrCountry}</td>
                    <td>{!meal.transferOfValue}</td>
                    <td>{!meal.productGroup}</td>
                    <td>{!meal.marketedName}</td>
                    <td style="text-align:right;">
                        <apex:outputText value="{0, number,currency}">
                            <apex:param value="{!meal.value}"/>
                        </apex:outputText>
                    </td>
                </tr>
            </apex:repeat>
            
            <apex:outputPanel rendered="{!if((reportType == 'All' || reportType == 'Meals'),true,false)}">
                <tr>                
                    <td style="text-align:right;font-weight:bold;" colspan ="11">Additional Meal Attendee Cost</td>
                    <td style="text-align:right;">
                        <apex:outputText value="{0, number,currency}">
                            <apex:param value="{!if(mealsInfo.additionalAttendeeCost != null,mealsInfo.additionalAttendeeCost,0)}"/>
                        </apex:outputText>
                    </td>
                </tr>
            </apex:outputPanel>
            
            <apex:repeat value="{!flightsInfo.response}" var="meal">
                <tr>
                    <td>{!meal.coveredRecipientType}</td>
                    <td>{!meal.lastOrHospital}</td>
                    <td>{!meal.firstName}</td>
                    <td>{!meal.middleName}</td>
                    <td>{!meal.suffix}</td>
                    <td>{!meal.bardGlobalId}</td>
                    <td>{!meal.city}</td>
                    <td>{!meal.stateOrCountry}</td>
                    <td>{!meal.transferOfValue}</td>
                    <td>{!meal.productGroup}</td>
                    <td>{!meal.marketedName}</td>
                    <td style="text-align:right;">
                        <apex:outputText value="{0, number,currency}">
                            <apex:param value="{!meal.value}"/>
                        </apex:outputText>
                    </td>
                </tr>
            </apex:repeat>
            
            <apex:repeat value="{!hotelsInfo.response}" var="meal">
                <tr>
                    <td>{!meal.coveredRecipientType}</td>
                    <td>{!meal.lastOrHospital}</td>
                    <td>{!meal.firstName}</td>
                    <td>{!meal.middleName}</td>
                    <td>{!meal.suffix}</td>
                    <td>{!meal.bardGlobalId}</td>
                    <td>{!meal.city}</td>
                    <td>{!meal.stateOrCountry}</td>
                    <td>{!meal.transferOfValue}</td>
                    <td>{!meal.productGroup}</td>
                    <td>{!meal.marketedName}</td>
                    <td style="text-align:right;">
                        <apex:outputText value="{0, number,currency}">
                            <apex:param value="{!meal.value}"/>
                        </apex:outputText>
                    </td>
                </tr>
            </apex:repeat>
            
            <apex:outputPanel rendered="{!if((reportType == 'All' || reportType == 'Hotels'),true,false)}">
            <tr>                
                <td style="text-align:right;font-weight:bold;" colspan ="11">Additional Hotel Attendee Cost</td>
                <td style="text-align:right;">
                    <apex:outputText value="{0, number,currency}">
                        <apex:param value="{!if(hotelsInfo.additionalAttendeeCost != null,hotelsInfo.additionalAttendeeCost,0)}"/>
                    </apex:outputText>
                </td>
            </tr>
            </apex:outputPanel>
            
            <apex:repeat value="{!transportInfo.response}" var="meal">
                <tr>
                    <td>{!meal.coveredRecipientType}</td>
                    <td>{!meal.lastOrHospital}</td>
                    <td>{!meal.firstName}</td>
                    <td>{!meal.middleName}</td>
                    <td>{!meal.suffix}</td>
                    <td>{!meal.bardGlobalId}</td>
                    <td>{!meal.city}</td>
                    <td>{!meal.stateOrCountry}</td>
                    <td>{!meal.transferOfValue}</td>
                    <td>{!meal.productGroup}</td>
                    <td>{!meal.marketedName}</td>
                    <td style="text-align:right;">
                        <apex:outputText value="{0, number,currency}">
                            <apex:param value="{!meal.value}"/>
                        </apex:outputText>
                    </td>
                </tr>
            </apex:repeat>
            
            <apex:outputPanel rendered="{!if((reportType == 'All' || reportType == 'Transportation'),true,false)}">
            <tr>
                <td style="text-align:right;font-weight:bold;" colspan ="11">Additional Transportation Attendee Cost</td>
                <td style="text-align:right;">
                    <apex:outputText value="{0, number,currency}">
                        <apex:param value="{!if(transportInfo.additionalAttendeeCost != null,transportInfo.additionalAttendeeCost,0)}"/>
                    </apex:outputText>
                </td>
            </tr>
            </apex:outputPanel>
            
            
            <apex:repeat value="{!incidentInfo.response}" var="meal">
                <tr>
                    <td>{!meal.coveredRecipientType}</td>
                    <td>{!meal.lastOrHospital}</td>
                    <td>{!meal.firstName}</td>
                    <td>{!meal.middleName}</td>
                    <td>{!meal.suffix}</td>
                    <td>{!meal.bardGlobalId}</td>
                    <td>{!meal.city}</td>
                    <td>{!meal.stateOrCountry}</td>
                    <td>{!meal.transferOfValue}</td>
                    <td>{!meal.productGroup}</td>
                    <td>{!meal.marketedName}</td>
                    <td style="text-align:right;">
                        <apex:outputText value="{0, number,currency}">
                            <apex:param value="{!meal.value}"/>
                        </apex:outputText>
                    </td>
                </tr>
            </apex:repeat>
            
            <apex:repeat value="{!proctorInfo.response}" var="meal">
                <tr>
                    <td>{!meal.coveredRecipientType}</td>
                    <td>{!meal.lastOrHospital}</td>
                    <td>{!meal.firstName}</td>
                    <td>{!meal.middleName}</td>
                    <td>{!meal.suffix}</td>
                    <td>{!meal.bardGlobalId}</td>
                    <td>{!meal.city}</td>
                    <td>{!meal.stateOrCountry}</td>
                    <td>{!meal.transferOfValue}</td>
                    <td>{!meal.productGroup}</td>
                    <td>{!meal.marketedName}</td>
                    <td style="text-align:right;">
                        <apex:outputText value="{0, number,currency}">
                            <apex:param value="{!meal.value}"/>
                        </apex:outputText>
                    </td>
                </tr>
            </apex:repeat>
            
            
            
            <tr>
                <td style="text-align:right;font-weight:bold;" colspan ="11">Total Cost</td>
                <td style="text-align:right;">
                    <apex:outputText value="{0, number,currency}" style="font-weight:bold;">
                        <apex:param value="{!totalCost}"/>
                    </apex:outputText>
                </td>
            </tr>
            
        </table>
         
    </apex:form>
</apex:page>