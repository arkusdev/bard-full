trigger ProductUsedTrigger on Product_Used__c (after update, before delete) {
	if (!ProductUsedUtils.STOP_TRIGGER) {
		if (trigger.isUpdate)
			ProductUsedUtils.UpdateCases(trigger.New);
		if (trigger.isDelete)
			ProductUsedUtils.CleanCases(Trigger.Old);
	}
}