trigger CampaignProctorFeeTrigger on Campaign_Proctor_Fee__c (after delete, after insert, after undelete, after update,
															  before delete, before insert, before update)  { 
	new CampaignProctorFeeTriggerHandler().run();															  
}