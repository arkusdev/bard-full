trigger ProjectTrigger on Project__c (after insert, after update) {
    if (trigger.isInsert && trigger.isAfter){
        ProjectUtils.AddProjectRoles();
        ProjectUtils.CreateChatterGroups(Trigger.new);
    }
    if (trigger.isUpdate && trigger.isAfter){
        ProjectUtils.handleAfterUpdate(Trigger.oldMap, Trigger.newMap);
        ProjectUtils.ChangeLaunchYearFinancials();        
    }
}