trigger CampaignHotelTrigger on Campaign_Hotel__c (after delete, after insert, after undelete, after update,
												   before delete, before insert, before update)  { 
	new CampaignHotelTriggerHandler().run();												   
}