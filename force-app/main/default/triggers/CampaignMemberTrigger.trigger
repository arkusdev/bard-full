trigger CampaignMemberTrigger on CampaignMember (after delete, after insert, after undelete, after update,
                                                 before delete, before insert, before update)  { 
    new CampaignMemberTriggerHandler().run();
}