trigger CampaignDocumentTrigger on Campaign_Document__c (after delete, after insert, after undelete, after update,
														 before delete, before insert, before update)  { 
	new CampaignDocumentTriggerHandler().run();														 
}