trigger DMManagerEmailUpdate on KOL_Nomination__c (before insert,before update,after update) {
    
    if(Trigger.isBefore && Trigger.isInsert){
        DMManagerEmailUpdate.ManagerEmailUpdate(trigger.new);
        //DMManagerEmailUpdate.attachmentValiation(trigger.new);
    }
    
    if(Trigger.isBefore && Trigger.isUpdate){
        DMManagerEmailUpdate.ManagerEmailUpdate(trigger.new);
        //DMManagerEmailUpdate.attachmentValiation(trigger.new);
    }
    
    if(Trigger.isAfter && Trigger.isUpdate){
        set<id> kids = new set<id>();
        for(KOL_Nomination__c kl : trigger.new){
            KOL_Nomination__c oldKOL = trigger.oldMap.get(kl.id);
            if(kl.NominationStage__c != oldKOL.NominationStage__c && kl.NominationStage__c == 'Complete')
                kids.add(kl.id);
        }
        if(kids.size()>0)
            DMManagerEmailUpdate.createKOLPDF(kids);
    }
}