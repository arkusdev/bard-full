trigger CampaignMealTrigger on Campaign_Meal__c (after delete, after insert, after undelete, after update,
												 before delete, before insert, before update)  { 
	new CampaignMealTriggerHandler().run();							 
}