trigger CampaignUserTrigger on Campaign_User__c (after delete, after insert, after undelete, after update,
												 before delete, before insert, before update)  { 
	new CampaignUserTriggerHandler().run();
}