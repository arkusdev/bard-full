trigger CampaignFlightTrigger on Campaign_Flight__c (after delete, after insert, after undelete, after update,
													 before delete, before insert, before update)  { 
	new CampaignFlightTriggerHandler().run();													 
}