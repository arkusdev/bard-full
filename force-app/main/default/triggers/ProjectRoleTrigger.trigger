trigger ProjectRoleTrigger on Project_Role__c (before delete, after insert, after update) {
    new ProjectRoleTriggerHandler().run();
}