trigger CaseTrigger on Case (after insert, after update) {
	if (!CaseUtils.STOP_TRIGGER) {
		if (trigger.isInsert) {
			//CaseUtilsQueueable s = new CaseUtilsQueueable(true, trigger.New, null);
			//s.execute(null) ;
			System.enqueueJob(new CaseUtilsQueueable(true, trigger.New, null));
		}
		if (trigger.isUpdate) {
			//CaseUtilsQueueable s = new CaseUtilsQueueable(false, trigger.New, trigger.OldMap);
			//s.execute(null) ;
			System.enqueueJob(new CaseUtilsQueueable(false, trigger.New, trigger.OldMap));
		}
	}
}