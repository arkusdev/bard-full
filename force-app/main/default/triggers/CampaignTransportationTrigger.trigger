trigger CampaignTransportationTrigger on Campaign_Transportation__c (after delete, after insert, after undelete, after update,
																	 before delete, before insert, before update)  { 
	new CampaignTransportationTriggerHandler().run();
}