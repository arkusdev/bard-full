trigger CampaignIncidentalTrigger on Campaign_Incidental__c (after delete, after insert, after undelete, after update,
															 before delete, before insert, before update)  { 
	new CampaignIncidentalTriggerHandler().run();	 
}