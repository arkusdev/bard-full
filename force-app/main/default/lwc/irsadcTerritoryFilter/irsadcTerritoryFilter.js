import { LightningElement, track, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { getRecord } from 'lightning/uiRecordApi';
import USER_ID from '@salesforce/user/Id';
import getUsers from '@salesforce/apex/IRSASalesSummaryController.getUsers';

import { fireEvent } from 'c/pubsub';

export default class IrsadcTerritoryFilter extends LightningElement {
    @track users = [{ label: 'All', value: 'all' }];
    @track collapsed = '';
    @track iconVariant = '';
    @track selectedUser
    @track error;
    role = null;
    @wire(CurrentPageReference) pageRef;
    @wire(getRecord, { recordId: USER_ID, fields: ['User.UserRole.Name'] })
    wireuser({ error, data }) {
        if (error) {
            this.error = error;
        } else if (data) {
            this.role = this.getCurrentUserRole(data.fields.UserRole.value ? data.fields.UserRole.value.fields.Name.value : '');
            if (this.role) {
                this.loadUsers();
            }
        }
    }

    getCurrentUserRole(roleName) {
        let tokens = roleName.split(' - ');
        if (tokens && tokens.length > 1) {
            return tokens[1];
        }
        return null;
    }

    loadUsers() {
        getUsers()
            .then(result => {
                this.users = result.map(element => {
                    let option = {'value': element.Id};
                    if(this.role === 'RM'){
                        option.label = element.Name;
                    }else{
                        option.label = element.Territory__c + ' - ' + element.Name;
                    }
                    return option;
                });
                this.users.unshift({ label: 'All', value: 'all'});
                this.selectedUser = this.users[0].value;
            })
            .catch(error => {
                console.log('ERROR: ' + JSON.stringify(error));
                this.error = error;
            });
    }
    changeUser(evt) {
        this.selectedUser = evt.target.value;
        fireEvent(this.pageRef, 'userChanged', this.selectedUser);
    }
    collapse() {
        if (this.collapsed === '') {
            this.collapsed = 'collapsed';
            this.iconVariant = 'inverse'
        } else if (this.collapsed === 'collapsed') {
            this.collapsed = '';
            this.iconVariant = ''
        }
    }
}