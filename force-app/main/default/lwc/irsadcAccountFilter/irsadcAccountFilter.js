import { LightningElement, track, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import getAccounts from '@salesforce/apex/IRSASalesSummaryController.getAccounts';
import { fireEvent } from 'c/pubsub';

export default class IrsadcAccountFilter extends LightningElement {
    @track accounts = [{ 'label': 'All', 'value': 'All' }];
    @track selectedAccount = 'All';
    @track collapsed = '';
    @track iconVariant = '';
    @track error;
    @wire(CurrentPageReference) pageRef;

    connectedCallback() {
        this.loadAccounts();
    }

    loadAccounts() {
        getAccounts()
            .then(result => {
                this.accounts = result.map(element => { return { 'label': element.Name, 'value': element.Id } });
                this.accounts.unshift({ label: 'All', value: 'All' });
                this.selectedAccount = this.accounts[0].value;
            })
            .catch(error => {
                this.error = error;
            });
    }
    changeAccount(evt) {
        this.selectedAccount = evt.target.value;
        fireEvent(this.pageRef, 'accountChanged', this.selectedAccount);
    }
    collapse() {
        if (this.collapsed === '') {
            this.collapsed = 'collapsed';
            this.iconVariant = 'inverse'
        } else if (this.collapsed === 'collapsed') {
            this.collapsed = '';
            this.iconVariant = ''
        }
    }
}