import { api, LightningElement } from 'lwc';

export default class CustomPrompt extends LightningElement {
    @api message;
    @api header;

    handleClick(){
        const closeEvent = new CustomEvent('close', {
            bubbles: true,
            composed: true
        });
        this.dispatchEvent(closeEvent);
    }
}