import { LightningElement, track, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import getAGResponse from '@salesforce/apex/IRSASalesSummaryController.getAGResponse';
import FORM_FACTOR from '@salesforce/client/formFactor';
import { registerListener, unregisterAllListeners } from 'c/pubsub';

export default class IrsadcAccountSummary extends LightningElement {
    @track themeDesktop = (FORM_FACTOR!='Small');
    @track timeRange = 'yesterday';
    @track product = 'all';
    @track account = 'all';
    @track userId = 'all';
    @track data;
    @track dataLoaded = false;
    
    @wire(CurrentPageReference) pageRef; // Required by pubsub

    @wire(getAGResponse, {account: '$account', timeRange: '$timeRange', userId: '$userId', product: '$product'})
    loadAG({error, data}){
        if (data) {
            this.data = JSON.parse(JSON.stringify(data));
            this.invoiceDateToText();
            this.dataLoaded = true;
        }else if(error){
            //TODO: handle error
            this.dataLoaded = true;
            const errorEvent = new CustomEvent('error', {
                detail: {
                    title: "Error retrieving the data",
                    message: error.body.message,
                    type: 'Error'
                }
            });
            this.dispatchEvent(errorEvent);
        }
    }
    connectedCallback(){
        registerListener('accountChanged', this.handleAccountChanged, this);
        registerListener('userChanged', this.handleUserChanged, this);
        registerListener('timeRangeChanged', this.handleTimeRangeChanged, this);
        registerListener('productChanged', this.handleProductChanged, this);
    }
    disconnectedCallback() {
		unregisterAllListeners(this);
    }
    handleAccountChanged(value) {
        this.dataLoaded = false;
        this.account = value;
    }
    handleUserChanged(value) {
        this.dataLoaded = false;
        this.userId = value;
    }
    handleTimeRangeChanged(value) {
        this.dataLoaded = false;
        this.timeRange = value;
    }
    handleProductChanged(value) {
        if(this.product !== value){
            this.dataLoaded = false;
            this.product = value;
        }
    }
    invoiceDateToText(){
        this.data.forEach(element => {
            element.accountName = element.accountName+' - '+element.accountCity+', '+element.accountState;
            element.sales.forEach(sale => {
                sale.Invoice_Date__c = this.invoiceDateToTextItem(sale.Invoice_Date__c);
                sale.Item_Number__c = sale.Item_Description__c ? sale.Item_Number__c+'  '+sale.Item_Description__c : (sale.Item_Number__c ? sale.Item_Number__c : '\xa0');
                sale.Order_Number__c = sale.Order_Number__c ? sale.Order_Number__c : '\xa0';
                sale.Invoice_Number__c = sale.Invoice_Number__c ? sale.Invoice_Number__c : '\xa0'; 
                sale.Product_Group__c = sale.Product_Group__c ? sale.Product_Group__c : '\xa0';
                sale.Units__c = sale.Units__c ? sale.Units__c : '\xa0';
                sale.Sales__c = sale.Sales__c ? sale.Sales__c : '\xa0';
            })
        });
    }
    invoiceDateToTextItem(dateString){
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        if(!dateString){
            return '\xa0';
        }
        return parseInt(dateString.split("-")[2],10)+' '+monthNames[parseInt(dateString.split("-")[1], 10) - 1];
        
    }
}