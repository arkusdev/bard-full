import { LightningElement } from 'lwc';

export default class ScrollerWrapper extends LightningElement {

    get iosClass(){
        if(/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream){
            return 'scrollable scrollingOnIOS'
        }
        return '';
    }
}