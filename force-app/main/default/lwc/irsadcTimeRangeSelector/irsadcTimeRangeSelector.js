import { CurrentPageReference } from 'lightning/navigation';
import { LightningElement, track, wire } from 'lwc';
import { fireEvent } from 'c/pubsub';

export default class IrsadcTimeRangeSelector extends LightningElement {
    @track viewOptions =[];
    @track selectedTimeRange = "yesterday";
    @track selectedTimeRangeLabel = "Y";
    @track collapsed = '';
    @track iconVariant = '';
    @wire(CurrentPageReference) pageRef;

    connectedCallback(){
        this.viewOptions = [
            {'label': 'Yesterday', 'value': 'yesterday'},
            {'label': 'This Week', 'value': 'this_week'},
            {'label': 'This Month', 'value': 'this_month'}
        ];
    }
    changeTimeRange(evt){
        this.selectedTimeRangeLabel = this.getLabelFromValue(evt.detail.value);
        this.selectedTimeRange = evt.detail.value;
        fireEvent(this.pageRef, 'timeRangeChanged', this.selectedTimeRange);
    }
    collapse(){
        if(this.collapsed === ''){
            this.collapsed = 'collapsed';
            this.iconVariant = 'inverse'
        }else if (this.collapsed === 'collapsed'){
            this.collapsed = '';
            this.iconVariant = ''
        } 
    }
    getLabelFromValue(value){
        if(value === 'yesterday'){
            return 'Y';
        }else if(value === 'this_week'){
            return 'W';
        }
        return 'M';
    }
}