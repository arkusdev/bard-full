import { LightningElement, track, wire } from 'lwc';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import FORM_FACTOR from '@salesforce/client/formFactor';
import { registerListener, unregisterAllListeners } from 'c/pubsub';
import { fireEvent } from 'c/pubsub';

export default class IrsadcFiltersContainer extends NavigationMixin(LightningElement) {
    @track selectedTimeRange = 'yesterday';
    @track selectedProduct = 'all';
    @track selectedAccount = 'all';
    @track selectedUserId = 'all';
    @track themeMobile = (FORM_FACTOR=='Small');
    @wire(CurrentPageReference) pageRef; // Required by pubsub

    refresh(){
       location.reload();
    }
    handlePrintViewClick() {

        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                url: '/apex/IRSASalesSummaryPDF?sa=' + this.selectedAccount + '&su=' + this.selectedUserId + '&st=' + this.selectedTimeRange + '&sp=' + this.selectedProduct
            }
        });
    }
    connectedCallback(){
        registerListener('accountChanged', this.handleAccountChanged, this);
        registerListener('userChanged', this.handleUserChanged, this);
        registerListener('timeRangeChanged', this.handleTimeRangeChanged, this);
        registerListener('productChanged', this.handleProductChanged, this);
    }
    disconnectedCallback() {
		unregisterAllListeners(this);
    }
    handleAccountChanged(value) {
        this.selectedAccount = value;
    }
    handleUserChanged(value) {
        this.selectedUserId = value;
    }
    handleTimeRangeChanged(value) {
        this.selectedTimeRange = value;
    }
    handleProductChanged(value) {
        this.selectedProduct = value;
    }
}