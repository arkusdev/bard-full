import { LightningElement, track, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import getPGResponse from '@salesforce/apex/IRSASalesSummaryController.getPGResponse';
import getWeekRanges from '@salesforce/apex/IRSASalesSummaryController.getWeekRanges';
import FORM_FACTOR from '@salesforce/client/formFactor';
import { registerListener, unregisterAllListeners } from 'c/pubsub';
import { fireEvent } from 'c/pubsub';


export default class IrsadcProductGroupSummary extends LightningElement {
    @track themeDesktop = (FORM_FACTOR!='Small');
    @track timeRange = 'yesterday';
    @track selectedProduct = 'all';
    @track account = 'all';
    @track userId = 'all';
    @track data;
    @track dataLoaded = false;
    @track weeks = [];
    @track weekHeaders = {
        week1: '',
        week2: '',
        week3: '',
        week4: '',
        week5: '',
        week6: ''
    }
    @track error;
    
    @track grandTotals = {
        yesterday:0,
        weekdate1:0,
        weekdate2:0,
        weekdate3:0,
        total:0
    };

    @wire(CurrentPageReference) pageRef; // Required by pubsub

    @wire(getPGResponse, {account: '$account', userId: '$userId'})
    loadPG({error, data}){
        if (data) {
            this.data = data;
            this.recalculateTotals();
            this.dataLoaded = true;
        }else if(error){
            //TODO: handle error
        }
    }
    @wire(getWeekRanges, {})
    loadWeekRanges({error, data}){
        if (data) {
            this.weeks = data;
            this.weekHeaders.week1 = this.weeks[0] ? this.getWeekHeaderDate(this.weeks[0].endWeek) : '';
            this.weekHeaders.week2 = this.weeks[1] ? this.getWeekHeaderDate(this.weeks[1].endWeek) : '';
            this.weekHeaders.week3 = this.weeks[2] ? this.getWeekHeaderDate(this.weeks[2].endWeek) : '';
            this.weekHeaders.week4 = this.weeks[3] ? this.getWeekHeaderDate(this.weeks[3].endWeek) : '';
            this.weekHeaders.week5 = this.weeks[4] ? this.getWeekHeaderDate(this.weeks[4].endWeek) : '';
            this.weekHeaders.week6 = this.weeks[5] ? this.getWeekHeaderDate(this.weeks[5].endWeek) : '';
        }else if(error){
            //TODO: handle error
        }
    }  
    
    get weekdateClass1() { 
        return this.showWeek(1)  ? this.getColumnSize(1) : 'removeColumn';
    }
    get weekdateClass2() { 
        return this.showWeek(2) ? this.getColumnSize(2) : 'removeColumn';
    }
    get weekdateClass3() { 
        return this.showWeek(3) ? this.getColumnSize(3) : 'removeColumn';
    }
    get weekdateClass4() { 
        return this.showWeek(4) ? this.getColumnSize(4) : 'removeColumn';
    }
    get weekdateClass5() { 
        return this.showWeek(5) ? this.getColumnSize(5) : 'removeColumn';
    }
    get weekdateClass6() { 
        return this.showWeek(6) ? this.getColumnSize(6) : 'removeColumn';
    }

    connectedCallback(){
        registerListener('accountChanged', this.handleAccountChanged, this);
        registerListener('userChanged', this.handleUserChanged, this);
        registerListener('timeRangeChanged', this.handleTimeRangeChanged, this);
    }
    disconnectedCallback() {
		unregisterAllListeners(this);
    }
    handleAccountChanged(value) {
        this.dataLoaded = false;
        this.account = value;
    }
    handleUserChanged(value) {
        this.dataLoaded = false;
        this.userId = value;
    }
    handleTimeRangeChanged(value) {
        this.timeRange = value;
    }
    changeProduct(evt){
        this.selectedProduct = evt.target.value;
        fireEvent(this.pageRef, 'productChanged', this.selectedProduct);
    }
    
    recalculateTotals(){
        this.grandTotals = {
            yesterday:0,
            weekdate1:0,
            weekdate2:0,
            weekdate3:0,
            weekdate4:0,
            weekdate5:0,
            weekdate6:0,
            month:0
        };
        this.data.forEach(item => {
            this.recalculateTotalsForEach(item);
        });
        this.grandTotals.total = this.grandTotals.yesterday + this.grandTotals.weekdate1 + this.grandTotals.weekdate2 + this.grandTotals.weekdate3
        + this.grandTotals.weekdate4 + this.grandTotals.weekdate5 + this.grandTotals.weekdate6
        this.tableLoadingState = false;
    }
    recalculateTotalsForEach(item){
        this.grandTotals.yesterday += item.yesterday_sales;
        this.grandTotals.weekdate1 += item.weekdate1_sales;
        this.grandTotals.weekdate2 += item.weekdate2_sales;
        this.grandTotals.weekdate3 += item.weekdate3_sales;
        this.grandTotals.weekdate4 += item.weekdate4_sales;
        this.grandTotals.weekdate5 += item.weekdate5_sales;
        this.grandTotals.weekdate6 += item.weekdate6_sales;
        this.grandTotals.month += item.month_sales;
    }
    showWeek(weekNumber){
        return this.weeks && this.weeks[weekNumber - 1] && (this.timeRange === 'this_month' || (this.timeRange === 'this_week' && this.weeks[weekNumber - 1].thisWeek));
    }
    getColumnSize(weekNumber){
        if(this.weeks && this.weeks[weekNumber - 1] && this.timeRange === 'this_week' && this.weeks[weekNumber - 1].thisWeek){
            return 'slds-size_2-of-12';
        }
        if(this.weeks.length === 6){
            return 'slds-size_1-of-12';
        }
        if(this.weeks.length === 5){
            return 'slds-size_1-of-10';
        }
        return 'slds-size_1-of-8';
    }
    getWeekHeaderDate(dateString){
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        return parseInt(dateString.split("-")[2],10)+' '+monthNames[parseInt(dateString.split("-")[1], 10) - 1];
    }
}