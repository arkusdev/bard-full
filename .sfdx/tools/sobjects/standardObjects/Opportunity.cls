// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Opportunity {
    global Id Id;
    global Boolean IsDeleted;
    global Account Account;
    global Id AccountId;
    global RecordType RecordType;
    global Id RecordTypeId;
    global String Name;
    global String Description;
    global String StageName;
    /* Input the estimated annual sales of this opportunity. For ESRD product WavelinQ your amount will be greater than the $70K initial purchase, or incremental sales if this is for an additional physician receiving training.
    */
    global Decimal Amount;
    global Double Probability;
    global Date CloseDate;
    global String Type;
    global String NextStep;
    global String LeadSource;
    global Boolean IsClosed;
    global Boolean IsWon;
    global String ForecastCategory;
    global String ForecastCategoryName;
    global Campaign Campaign;
    global Id CampaignId;
    global Boolean HasOpportunityLineItem;
    global Pricebook2 Pricebook2;
    global Id Pricebook2Id;
    global User Owner;
    global Id OwnerId;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Date LastActivityDate;
    global Integer FiscalQuarter;
    global Integer FiscalYear;
    global String Fiscal;
    global Contact Contact;
    global Id ContactId;
    global Datetime LastViewedDate;
    global Datetime LastReferencedDate;
    global Quote SyncedQuote;
    global Id SyncedQuoteId;
    global Boolean HasOpenActivity;
    global Boolean HasOverdueTask;
    /* The amount divided by the number of units.
    */
    global Decimal ASP__c;
    global String Org1ID__c;
    global String Opportunity_Alias__c;
    /* Clinical Champion, Lab Director, Materials Management
    */
    global Boolean Appropriate_Personnel_on_Board__c;
    global Boolean Codes_in_the_System__c;
    /* Annually
    */
    global Double Estimated_Procedures__c;
    /* Timeframe, Competitive Product Removed, Paid/Sample/Trade-out, Clinical Acceptance Defined
    */
    global Boolean Evaluation_Parameters_Agreed_Upon__c;
    global Date Evaluation_Start_Date__c;
    global String Legacy_ID__c;
    global String Lost_Category__c;
    /* Enter the reason why the opportunity was lost.
    */
    global String Lost_Details__c;
    global String Lost_Reason__c;
    global String PO_Number__c;
    global Date Presentation_Date__c;
    global Boolean Pricing_Approved_Established__c;
    global String Product_Category__c;
    global String Product_Group__c;
    global String Rating__c;
    global String Strategy__c;
    global Boolean VAC_Approved__c;
    global String Biopsy_District_Number__c;
    global String Account_Alias__c;
    global String Biopsy_District_Name__c;
    global Double of_Procedures_Worked__c;
    global Decimal Carry_Over__c;
    global Decimal Current_Year__c;
    /* A rollup summary of the total of related competitors.
    */
    global Double Competitors__c;
    global String Account__c;
    global Boolean Sent_to_the_Field__c;
    global Boolean Clinical_Champion_Established__c;
    global Boolean Codes_on_GPO_Contract__c;
    global Date Committee_Meeting_Date__c;
    global Boolean Conversion_Parameters_Agreed_Upon__c;
    global Boolean Document_Every_Case_with_MD_s_Signature__c;
    /* If Product Deems Necessary
    */
    global Boolean End_User_Support__c;
    global Boolean In_Committee__c;
    global Boolean In_Service_the_Staff__c;
    global Boolean Mid_Final_Evaluation_Progress_Report__c;
    global Boolean Reinforce_Messaging__c;
    global Double of_Units__c;
    global Double Annual_BTK_Case_Volume__c;
    global Double Annual_SFA_Case_Volume__c;
    global String Peripheral_District_Number__c;
    global String Peripheral_District__c;
    global String Vascular_District_Number__c;
    global String Vascular_District__c;
    /* This should be checked if correct.
    */
    global Boolean Peripheral_Territory_Correct__c;
    global Boolean Vascular_Territory_Correct__c;
    global Boolean Biopsy_Territory_Correct__c;
    /* Check mark identifies this opportunity as an A1 opportunity to support product launch profiling
    */
    global Boolean A1_Profiling_Opportunity__c;
    /* Approximate number of days to move through the VAC process
    */
    global String VAC_Timeline__c;
    /* The date you expect the customer to place their first order
    */
    global Date First_Order_Expected__c;
    /* # of units the customer will request on their first order
    */
    global Double of_Units_on_First_Order__c;
    /* If consignment is not needed select "No"
    */
    global String Consignment_Needed__c;
    /* # of units needed for initial provisioning of consignment
    */
    global Double of_Consignment_Units_Needed__c;
    global String BDPI_Sole_Source__c;
    global Double Annual_SFA_DCB_Case_Volume__c;
    /* Enter the Medtronic DCB share in this account
    */
    global Double MDT_DCB_Share__c;
    /* Enter the BD DCB share in this account
    */
    global Double BD_DCB_Share__c;
    /* Enter the Spectranetics DCB share in this account
    */
    global Double SPNC_DCB_Share__c;
    global String Current_BTK_Therapy__c;
    /* Please select N/A if there is no primary atherectomy vendor
    */
    global String Primary_Atherectomy_Vendor__c;
    global String GPO_IDN__c;
    /* Search for contact record for the physician
    */
    global Contact Physician_Champion__r;
    /* Search for contact record for the physician
    */
    global Id Physician_Champion__c;
    global String Consignment_Need__c;
    /* Check this box if you want SFDC to clone (create a copy) of this opportunity for Lutonix 018. If checked this means that this account is a target for both Lutonix 018 and Lutonix 014
    */
    global Boolean Create_LTX_018_Opportunity__c;
    global String Primary_BTK__c;
    global String First_Order__c;
    /* designated by marketing analysis
    */
    global Double Tier__c;
    global String Point_of_Care__c;
    global String MD_Coachability__c;
    /* Yes indicates that the MD is committed to attending off-site training and case proctoring
    */
    global String Willing_to_Attend_Training_Course__c;
    /* Identify who is currently creating surgical AVFs in the hospital
    */
    global String Who_Creates_AVF_Surgically__c;
    /* Identify the physician and facility completing the AVF
    */
    global String Who_Manages_AVF_Endovascularly__c;
    /* Names and email addresses
    */
    global String Nephrologist__c;
    /* Identify who is surgical collaborator. If AVF surgeon is the surgical collaborator enter physician name in both fields.
    */
    global String Surgical_Collaborator__c;
    /* Identify and provide email address for who is primary clinical coordinator for AV access care for the patient
    */
    global String Cannulation_Champion__c;
    /* Identify the dialysis centers in which you have contacted a cannulator to elevate receptivity
    */
    global String Targeted_Dialysis_Centers__c;
    /* How experienced is the physician with ultrasound guided access
    */
    global String Ultrasound_Experience__c;
    /* Identify the person and facility who will be screening the patients
    */
    global String Vein_Mapping_Lab__c;
    global String Confidence_in_Conversion__c;
    /* Customer Service checks this box when the initial order has been shipped
    */
    global Boolean Shipped_Initial_Bundle__c;
    global String Targeted_Department__c;
    global Double Annual_Chronic_Venous_Disease_Volume__c;
    global Double Annual_Chronic_Venous_Dis_Stent_Volume__c;
    /* Enter # of Wallstents on the shelf
    */
    global Double of_Wallstents__c;
    global String Wallstents_Acquired__c;
    global Double Unit_Price_Per_WallStent__c;
    /* Enter the number of Balloons on the shelf
    */
    global Double of_XXL_Balloons_On_Shelf__c;
    global String BSC_Relationship__c;
    global String Venovo_Adoption__c;
    /* Enter # of iliofermoral venous thrombectomy procedures completed monthly
    */
    global Double X1_of_Thrombectomy_Procedures__c;
    /* Select all products in use
    */
    global String X2_Thrombectomy_Products_In_Use__c;
    global String Elevation_Gauge_Size__c;
    /* Traditional Bundle  = 10 devices, capital equipment at no charge
Capital Purchase = 7 devices, discounted capital equipment cost
Fusion Bundle = 10 devices, capital equipment at no charge split into 2 orders
    */
    global String Quote_Type__c;
    /* Enter in the email address manually. This will not pull from the contact record.
    */
    global String Physician_Champion_Email__c;
    /* Enter a $ for Day 1 sales
    */
    global Decimal Day_1_Sales_Estimate__c;
    global String Activity_Comments__c;
    /* Enter estimated $ in sales week 1
    */
    global Decimal Week_1_Sales_Estimate__c;
    /* Enter estimated sales $ for month 1
    */
    global Decimal Month_1_Sales_Estimate__c;
    /* Enter estimates sales $ for month 2
    */
    global Decimal Month_2_Sales_Estimate__c;
    /* Check this box once Corporate Credit is Approved
    */
    global Boolean Corporate_Credit_Approved__c;
    /* Select "Purchase WavelinQ" if the purchase is an Initial Bundle for the Account. Select "No Purchase Necessary" when you need to register an additional physician at an account that has already purchased.
    */
    global String Purchase_Path__c;
    global String Primary_DCB_Vendor__c;
    global String Physician_Specialty__c;
    /* Annual coil & plug embolization procedures
    */
    global Double Coil_Plug_Procedures__c;
    /* Identify by name and email who will be the ultrasound champion for patient selection
    */
    global String Primary_Sonographer__c;
    /* Identify by name and email who is the lab manager
    */
    global String Lab_Manager__c;
    /* Annual splenic, hepatic, renal or internal iliac coil & plug procedures for trauma
    */
    global String Trauma__c;
    /* Annual upper or lower GI bleeds treated with coils or plugs
    */
    global String GI_Bleeds__c;
    /* Annual endoleak prevention cases or endoleak treatments of the internal iliacs or IMA using coils or plugs
    */
    global String Endoleaks__c;
    /* Annual GDA or RGA embolization using coils or plugs prior to Y90
    */
    global String Y90_Mapping__c;
    /* Annual PAVM procedures where coils or plugs are used
    */
    global String PAVMs__c;
    /* Select all that apply
    */
    global String Stocked_detachable_coils__c;
    global String Preferred_detachable_coil__c;
    /* Select all that apply
    */
    global String Stock_plugs__c;
    global String Preferred_plug__c;
    global String Plugs_acquired__c;
    global Double Annual_consigned_units__c;
    global Decimal Unit_Price_per_Amplatzer__c;
    global Decimal Unit_Price_per_MVP__c;
    global String First_Purchase__c;
    global String Specialty__c;
    /* Annual # of fistulas created by the General/Vascular Surgeon
    */
    global Double of_Fistulas_by_GS_VS__c;
    /* Annual # of permanent dialysis catheters the group places
    */
    global Double of_Dialysis_Catheters__c;
    /* Who completes ultrasound screenings Radiology or Separate Vascular Lab or Other?
    */
    global String Patient_Selection__c;
    /* Nephrologist commitment secured in sending patients. Meeting has already occurred with physician.
    */
    global Boolean Nephrologist_Commitment__c;
    /* Surgeon commitment secured to partner in program. Meeting has already occurred with physician.
    */
    global Boolean AVF_Surgeon_Commitment__c;
    /* Training date for ultrasound
    */
    global Date Ultrasound_Training_Date__c;
    /* List all attendees names and titles
    */
    global String Ultrasound_Training_Attendees__c;
    global Boolean X3_Patients_Screened__c;
    /* Date of kickoff dinner
    */
    global Date Dinner_Date__c;
    /* List all attendees names and titles
    */
    global String Dinner_Attendees__c;
    global Boolean Validated_Target__c;
    /* Enter the number of Stereotactic procedures done each week
    */
    global Double of_Stereotactic_Procedures__c;
    /* select the system in use
    */
    global String Stereotactic_System__c;
    /* Select which system one the presence of microcalifications is confirmed on
    */
    global String Microcalcifications_On__c;
    global String System_Location__c;
    global String Encor_Bundle_Opportunity__c;
    /* Select up to 2 preferred sheets
    */
    global String Guiding_Sheaths_Used__c;
    /* Enter the prices for the sheaths selected and indicate the sheath name. For example: Destination = $X; Ansel  = $Y
    */
    global String Guiding_Sheath_Price__c;
    /* Choose up to 2 peferred sheaths
    */
    global String Introducer_Sheath_Used__c;
    /* Enter the prices of the sheaths selected above and indicate the name of the sheath. Example: GlideSheath Slender = $X; Cordis Avanti = $Y.
    */
    global String Introducer_Sheath_Price__c;
    /* Select the preferred thin-walled sheath used
    */
    global String Thin_Walled_Sheath__c;
    /* If the alternative access site is used primarily to cross lesions select,  Cross lesions only 
If the alternative access site is used to cross lesions and treat disease select, Cross lesions/treat disease
    */
    global String Alternative_Access_Site__c;
    /* Select the closure device preferences for peripheral procedures
    */
    global String Closure_Device_for_Procedures__c;
    /* The Halo One™ Thin-Walled Guiding Sheath is NOT indicated for use in the neurovasculature or the coronary vasculature. What % of your coronary diagnostics/interventions are done via radial access?
    */
    global Double of_Radial_Access__c;
    /* The Halo One™ Thin-Walled Guiding Sheath is NOT indicated for use in the neurovasculature or the coronary vasculature. Select all that applies.
    */
    global String Coronary_Radial_Introducer_Sheaths__c;
    /* Did you select other for the preferred guided sheath question? If so, then enter the name of the sheath here.
    */
    global String Guiding_Sheath_Other__c;
    /* Did you select Other as the preferred introducer sheath? If so, enter here the name of the preferred sheath.
    */
    global String Introducer_Sheath_Other__c;
    /* The Halo One™ Thin-Walled Guiding Sheath is NOT indicated for use in the neurovasculature or the coronary vasculature. Did you select Other for the preferred sheath for coronary procedures? If so, enter the name of the preferred sheath here.
    */
    global String Coronary_Sheath_Other__c;
    global String Targeted_Procedure__c;
    global Double of_Gamma_Detectors_Onsite__c;
    global Double of_Disposable_Sleeves_on_Shelf__c;
    global String Competition__c;
    /* For existing Dialysis Account: what is the Dialysis Market Share %
    */
    global Double Dialysis_Market_Share__c;
    global String Pediatric_Catheter_In_Use__c;
    /* What catheter did you use before?
    */
    global String Pediatric_Catheter_Used_Previously__c;
    /* Tell us why they switched catheters
    */
    global String Reason_for_Catheter_Change__c;
    /* What challenges do you/have you experienced with catheter placements, performance/use, and or exchanges in this patient population?
    */
    global String Catheter_Placement_Challenges__c;
    /* What size/s and length do you carry and routinely use?
    */
    global String Catheter_Sizes__c;
    /* What is the process for adding GP Slim to the shelf?
    */
    global String How_to_Add_Glidepath__c;
    /* What is your involvement in the process to convert this account to Glidepath Slim
    */
    global String What_is_your_involvement__c;
    /* How often are you placing lines for apheresis/hemoperfusion?
    */
    global String Frequency_Apheresis_Hemo_line_placement__c;
    /* Would you place more if the profile were lower than your current offering?
    */
    global String Usage_increase_with_lower_profile__c;
    /* Yes - BD dialysis catheters are in use in the hospital and we have to go through the VAC process 
No - BD dialysis catheters are in use in the hospital and we do not have to go through the VAC process 
NA - No BD dialysis catheters in use
    */
    global String Dialysis_Catheter_VAC__c;
    /* # of placements made monthly
    */
    global Double of_perm_cath_placements__c;
    /* where is the hospital are these procedures begin done?
    */
    global String Procedure_location__c;
    /* Other than yourself,  who places pediatric dialysis catheters?
    */
    global String Who_places_pediatric_dialysis_catheters__c;
    /* What role does the placer serve in the decision process?
    */
    global String Placers_Role_in_Decision__c;
    global String Competitor_Product__c;
    global Double Age__c;
    /* The date that the Opportunity went to Stage = Targeting
    */
    global Date Targeting_Date__c;
    /* Days since the Opportunity went to Targeting
    */
    global Double Days_Since_Targeting__c;
    global Double Closing_in__c;
    global Date Reverted_Date__c;
    /* Annual number of kits
    */
    global Double Bone_Marrow_Kits__c;
    /* Annual number of kits
    */
    global Double Bone_Lesion_Kits__c;
    /* Enter the annual volume of OnControl Marrow cases
    */
    global Double OnControl_Marrow_Cases__c;
    /* Enter the annual volume of OnControl Lesion cases
    */
    global Double OnControl_Lesion_Cases__c;
    global String Current_Bone_Devices__c;
    global Double OnControl_Drivers_Onsite__c;
    /* select the purchasing mix between 10g coaxial / 12 biopsy and 11g coaxial / 13g biopsy
    */
    global String Mix_of_Bone_Lesion_Kits__c;
    /* enter in an annual number for the # of units to be used by the physician champion at this account.
    */
    global Double units_used_by_Physician_Champion__c;
    global List<AccountPartner> AccountPartners;
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<Case> Cases__r;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<Competitor__c> Competitors__r;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<EmailMessage> Emails;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Event> Events;
    global List<EventRelation> EventRelations;
    global List<LookedUpFromActivity> Activities__r;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<OpportunityCompetitor> OpportunityCompetitors;
    global List<OpportunityContactRole> OpportunityContactRoles;
    global List<OpportunityFeed> Feeds;
    global List<OpportunityFieldHistory> Histories;
    global List<OpportunityHistory> OpportunityHistories;
    global List<OpportunityLineItem> OpportunityLineItems;
    global List<OpportunityPartner> OpportunityPartnersFrom;
    global List<OpportunityShare> Shares;
    global List<OpportunityTeamMember> OpportunityTeamMembers;
    global List<Partner> Partners;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<Quote> Quotes;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<Task> Tasks;
    global List<TaskRelation> TaskRelations;
    global List<TopicAssignment> TopicAssignments;
    global List<Case__hd> Parent;
    global List<ContentDistribution> RelatedRecord;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<ForecastingFact> Opportunity;
    global List<Lead> ConvertedOpportunity;
    global List<OpportunityContactRoleChangeEvent> Opportunity;
    global List<Opportunity__hd> Parent;
    global List<OutgoingEmail> RelatedTo;
    global List<QuoteChangeEvent> Opportunity;
    global List<TaskChangeEvent> What;
    global List<TaskRelationChangeEvent> Relation;

    global Opportunity () 
    {
    }
}