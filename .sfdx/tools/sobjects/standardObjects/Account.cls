// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Account {
    global Id Id;
    global Boolean IsDeleted;
    global Account MasterRecord;
    global Id MasterRecordId;
    global String Name;
    global String Type;
    global RecordType RecordType;
    global Id RecordTypeId;
    global Account Parent;
    global Id ParentId;
    global String BillingStreet;
    global String BillingCity;
    global String BillingState;
    global String BillingPostalCode;
    global String BillingCountry;
    global Double BillingLatitude;
    global Double BillingLongitude;
    global String BillingGeocodeAccuracy;
    global Address BillingAddress;
    global String ShippingStreet;
    global String ShippingCity;
    global String ShippingState;
    global String ShippingPostalCode;
    global String ShippingCountry;
    global Double ShippingLatitude;
    global Double ShippingLongitude;
    global String ShippingGeocodeAccuracy;
    global Address ShippingAddress;
    global String Phone;
    global String Fax;
    global String Website;
    global String PhotoUrl;
    global String Sic;
    global String Industry;
    global Decimal AnnualRevenue;
    global Integer NumberOfEmployees;
    global String Ownership;
    global String TickerSymbol;
    global String Description;
    global String Rating;
    global String Site;
    global User Owner;
    global Id OwnerId;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Date LastActivityDate;
    global Datetime LastViewedDate;
    global Datetime LastReferencedDate;
    global String JigsawCompanyId;
    global String AccountSource;
    global String SicDesc;
    global Decimal Biopsy_Prior_Year__c;
    /* If the Biopsy Priory year and YTD are less than zero.
    */
    global String Biopsy_Status__c;
    global Decimal Biopsy_YTD__c;
    global Decimal Peripheral_Prior_Year__c;
    /* If the Peripheral YTD and prior year sales are greater than zero.
    */
    global String Peripheral_Status__c;
    global Territory__c Peripheral_Territory__r;
    global Id Peripheral_Territory__c;
    global Decimal Peripheral_YTD__c;
    /* Biopsy + Vascular + Peripheral Prior Year
    */
    global Decimal Total_Prior_Year__c;
    /* Biopsy + Vascular + Peripheral YTD
    */
    global Decimal Total_YTD__c;
    global Decimal Vascular_Prior_Year__c;
    /* If the Vascular YTD and Prior year are above 0
    */
    global String Vascular_Status__c;
    global Territory__c Vascular_Territory__r;
    global Id Vascular_Territory__c;
    global Decimal Vascular_YTD__c;
    global Double Biopsy_Open_Opportuniites__c;
    global Double Peripheral_Open_Opportunities__c;
    global String Account_Rating__c;
    global Double Vascular_Open_Opportunities__c;
    global Date Date_Lost__c;
    global Boolean No_Contacts__c;
    global String Biopsy_District__c;
    global String Org1ID__c;
    global String Legacy_Vascular_TM_ID__c;
    global String Legacy_Sales_Person_2__c;
    global String Legacy_Pheripheral_TM1_ID__c;
    global String Peripheral_District__c;
    global Boolean BiopsyTMUpdated__c;
    global String Bill_to_Customer_Name__c;
    global String Bill_to_ID__c;
    global String CC9__c;
    global Decimal Capital_Equipment__c;
    global String Competitor_Product__c;
    global Double Competitor_Volumes__c;
    global Account Competitor__r;
    global Id Competitor__c;
    global String Customer_Type__c;
    global String Facility_Type__c;
    global String JDE_Facility_Type_Code__c;
    global String JDE_Facility_Type__c;
    global String JDE_Type__c;
    global String OBL_Hospital__c;
    global String Ship_to_ID__c;
    global Territory__c Territory__r;
    global Id Territory__c;
    global String Account_Alias__c;
    /* A rollup of number of opportunities.
    */
    global Double Opportunities__c;
    global Double Account__c;
    global String AccountNo__c;
    global String Active__c;
    global String GPO__c;
    global String Old_ID__c;
    global String Products_Discussed__c;
    global String SLA__c;
    global Contact Sales_Person_2__r;
    global Id Sales_Person_2__c;
    global Contact Vascular_TM__r;
    global Id Vascular_TM__c;
    global Decimal X2012_Drainage_Sales__c;
    global String Vascular_District__c;
    global Boolean Lutonix_220_At_Risk_Account__c;
    global String Risk_Initiative__c;
    global String Risk_Level__c;
    global String LTX_220_Codes_Delivered__c;
    global String LTX_Safety_Efficacy_Message_Delivered__c;
    /* Rank given to this account based on at risk sales $
    */
    global Double Initiative_Account_Rank__c;
    global Decimal X2017_BD_Lutonix_Sales__c;
    global Decimal X2018_YTD_BD_Lutonix_Sales__c;
    global Double Annual_Medtronic_Sales__c;
    global Decimal Annual_Spectranetics_Sales__c;
    global String Risk_Initiative_Notes__c;
    /* This account has been selected to be a focus for the AV Lutonix patient campaign
    */
    global Boolean AV_Lutonix_Patient_Campaign__c;
    global User User_assigned_to_AV_LTX_Campaign__r;
    global Id User_assigned_to_AV_LTX_Campaign__c;
    /* If you are a CS or ASR check this field to identify it as a FY19 focus account. You will want to select a maximum of 5 accounts.
    */
    global Boolean PAD_CS_ASR_Goal_Account__c;
    /* Select your user name to identify this account as one of your FY19 focus accounts.
    */
    global User CS_ASR__r;
    /* Select your user name to identify this account as one of your FY19 focus accounts.
    */
    global Id CS_ASR__c;
    /* Check this box if you are a CS or ASR to identify this as one of your FY19 goal accounts. Identify a maximum of 5 accounts.
    */
    global Boolean PAD_CS_ASR_Lifestream_Account__c;
    global Boolean PAD_C_S_ASR_Venovo_Account__c;
    global Double Backorders__c;
    global Facility__c Facility__r;
    global Id Facility__c;
    global Boolean Surpass_Defense_Account__c;
    global Boolean IR__c;
    global Boolean Infusion_Nurse__c;
    global Boolean Oncologist__c;
    global Boolean Supply_Chain__c;
    global Boolean PAD_CS_ASR_Lutonix_Account__c;
    global String Customer_ERP_ID__c;
    global String ERP_Customer_ID__c;
    global String Enterprise_Account_Number__c;
    global String DH_Hospital_ID__c;
    global String Enterprise_Account_Network__c;
    global String Enterprise_Account_Network_Name__c;
    global String Enterprise_Account_Network_Parent__c;
    global String Enterprise_Account_Network_Parent_Name__c;
    global Decimal EAN_Total_Sales__c;
    global String Surviving_Customer_Group__c;
    global String Surviving_Customer_Classification__c;
    global List<Account> Accounts__r;
    global List<Account> ChildAccounts;
    global List<AccountContactRelation> AccountContactRelations;
    global List<AccountContactRole> AccountContactRoles;
    global List<AccountFeed> Feeds;
    global List<AccountHistory> Histories;
    global List<AccountPartner> AccountPartnersFrom;
    global List<AccountPartner> AccountPartnersTo;
    global List<AccountShare> Shares;
    global List<AccountTeamMember> AccountTeamMembers;
    global List<ActivityHistory> ActivityHistories;
    global List<Asset> Assets;
    global List<AssociatedLocation> AssociatedLocations;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<Backorder__c> Backorders__r;
    global List<Campaign> Campaigns__r;
    global List<Case> Cases;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<Competitor__c> Competitors__r;
    global List<Contact> Contacts;
    global List<ContactPointEmail> ContactPointEmails;
    global List<ContactPointPhone> ContactPointPhones;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<Contract> Contracts;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Event> Events;
    global List<EventRelation> EventRelations;
    global List<Lead> Leads1__r;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<Opportunity> Opportunities;
    global List<OpportunityPartner> OpportunityPartnersTo;
    global List<Order> Orders;
    global List<Partner> PartnersFrom;
    global List<Partner> PartnersTo;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<Product_Competitor__c> Product_Competitors__r;
    global List<Product_Competitor__c> Product_Competitors1__r;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<Sales__c> Sales__r;
    global List<SocialPersona> Personas;
    global List<SocialPost> Posts;
    global List<Task> Tasks;
    global List<TaskRelation> TaskRelations;
    global List<TopicAssignment> TopicAssignments;
    global List<User> Users;
    global List<AcceptedEventRelation> Account;
    global List<AccountChangeEvent> Parent;
    global List<AccountContactRoleChangeEvent> Account;
    global List<ActivityHistory> PrimaryAccount;
    global List<AssetChangeEvent> Account;
    global List<ContentDistribution> RelatedRecord;
    global List<ContentVersion> FirstPublishLocation;
    global List<ContractChangeEvent> Account;
    global List<EventChangeEvent> What;
    global List<EventRelation> Account;
    global List<EventRelationChangeEvent> Relation;
    global List<EventWhoRelation> Account;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<Lead> ConvertedAccount;
    global List<OpenActivity> PrimaryAccount;
    global List<OpportunityChangeEvent> Account;
    global List<OutgoingEmail> RelatedTo;
    global List<Quote> Account;
    global List<TaskChangeEvent> What;
    global List<TaskRelation> Account;
    global List<TaskRelationChangeEvent> Relation;
    global List<TaskWhoRelation> Account;
    global List<UserRole> PortalAccount;

    global Account () 
    {
    }
}