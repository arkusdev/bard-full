// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Lead {
    global Id Id;
    global Boolean IsDeleted;
    global Lead MasterRecord;
    global Id MasterRecordId;
    global String LastName;
    global String FirstName;
    global String Salutation;
    global String MiddleName;
    global String Suffix;
    global String Name;
    global RecordType RecordType;
    global Id RecordTypeId;
    global String Title;
    global String Company;
    global String Street;
    global String City;
    global String State;
    global String PostalCode;
    global String Country;
    global Double Latitude;
    global Double Longitude;
    global String GeocodeAccuracy;
    global Address Address;
    global String Phone;
    global String MobilePhone;
    global String Fax;
    global String Email;
    global String Website;
    global String PhotoUrl;
    global String Description;
    global String LeadSource;
    global String Status;
    global String Industry;
    global String Rating;
    global Decimal AnnualRevenue;
    global Integer NumberOfEmployees;
    global SObject Owner;
    global Id OwnerId;
    global Boolean HasOptedOutOfEmail;
    global Boolean IsConverted;
    global Date ConvertedDate;
    global Account ConvertedAccount;
    global Id ConvertedAccountId;
    global Contact ConvertedContact;
    global Id ConvertedContactId;
    global Opportunity ConvertedOpportunity;
    global Id ConvertedOpportunityId;
    global Boolean IsUnreadByOwner;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Date LastActivityDate;
    global Boolean DoNotCall;
    global Boolean HasOptedOutOfFax;
    global Datetime LastViewedDate;
    global Datetime LastReferencedDate;
    global Date LastTransferDate;
    global String Jigsaw;
    global String JigsawContactId;
    global String EmailBouncedReason;
    global Datetime EmailBouncedDate;
    global Individual Individual;
    global Id IndividualId;
    /* Lead has been archived
    */
    global Boolean Archived_Lead__c;
    /* From the Lead Owner
    */
    global String Field_Sales_Divsion__c;
    global String Conference__c;
    global String WM4SF3__WalkMe_Engagement_Score__c;
    global Decimal Feeding_Sales__c;
    global String Competitor_Product__c;
    global String Org1ID__c;
    global String Legacy_Vascular_TM2__c;
    global String Legacy_BiopsyTM__c;
    global String Legacy_Peripheral_TM2__c;
    global Boolean BiopsyTMUpdated__c;
    global Boolean VascularTMUpdated__c;
    /* # of units used annually
    */
    global Double Competitor_Volumes__c;
    /* Who is the main competitor?
    */
    global Account Competitor__r;
    /* Who is the main competitor?
    */
    global Id Competitor__c;
    global String Department__c;
    global String Facility_Type__c;
    global String OBL_Hospital__c;
    global String Badge_ID__c;
    global String Capture_By__c;
    global Decimal AV_Graft_Sales__c;
    global String Category_Classification__c;
    global String Degree_1__c;
    global String Degree_2__c;
    global Double Dwell_Time__c;
    global String Follow_Up__c;
    global String Lead_ID__c;
    global String NPI_Number__c;
    global String Purchase_Authorization__c;
    global String Purchase_Timeframe__c;
    global String Registration_Type__c;
    global String Specialty__c;
    global String Capture_Date__c;
    global String Account__c;
    global Contact BiopsyTM__r;
    global Id BiopsyTM__c;
    global Double Days_Since_Last_Activity_Update__c;
    global Decimal Fabrics_Felts_Pledgets_Sales__c;
    global String GPO__c;
    global Datetime No_Message_Left__c;
    global Decimal PV_Graft_Sales__c;
    global Decimal Potential_Revenue__c;
    global String Product_Discussed__c;
    global Contact Vascular_TM2__r;
    global Id Vascular_TM2__c;
    global Decimal X2012_Drainage_Sales__c;
    global Contact Peripheral_TM2__r;
    global Id Peripheral_TM2__c;
    global Decimal Aspira_Sales__c;
    global List<AcceptedEventRelation> AcceptedEventRelations;
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CampaignMember> CampaignMembers;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DeclinedEventRelation> DeclinedEventRelations;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessageRelation> EmailMessageRelations;
    global List<EmailStatus> EmailStatuses;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Event> Events;
    global List<EventRelation> EventRelations;
    global List<EventWhoRelation> EventWhoRelations;
    global List<LeadFeed> Feeds;
    global List<LeadHistory> Histories;
    global List<LeadShare> Shares;
    global List<ListEmailIndividualRecipient> ListEmailIndividualRecipients;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<OutgoingEmailRelation> OutgoingEmailRelations;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<SocialPersona> Personas;
    global List<SocialPost> Posts;
    global List<Task> Tasks;
    global List<TaskRelation> TaskRelations;
    global List<TaskWhoRelation> TaskWhoRelations;
    global List<TopicAssignment> TopicAssignments;
    global List<UndecidedEventRelation> UndecidedEventRelations;
    global List<UserEmailPreferredPerson> PersonRecord;
    global List<ActivityHistory> PrimaryWho;
    global List<CampaignMember> LeadOrContact;
    global List<CampaignMemberChangeEvent> Lead;
    global List<ContentDistribution> RelatedRecord;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> Who;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<OpenActivity> PrimaryWho;
    global List<OutgoingEmail> Who;
    global List<TaskRelationChangeEvent> Relation;

    global Lead () 
    {
    }
}