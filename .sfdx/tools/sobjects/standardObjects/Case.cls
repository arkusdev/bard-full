// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Case {
    global Id Id;
    global Boolean IsDeleted;
    global Case MasterRecord;
    global Id MasterRecordId;
    global String CaseNumber;
    global Contact Contact;
    global Id ContactId;
    global Account Account;
    global Id AccountId;
    global SObject Source;
    global Id SourceId;
    global Community Community;
    global Id CommunityId;
    global String SuppliedName;
    global String SuppliedPhone;
    global String SuppliedCompany;
    global String Type;
    global RecordType RecordType;
    global Id RecordTypeId;
    global String Status;
    global String Reason;
    global String Origin;
    global String Language;
    global String Subject;
    global String Priority;
    global String Description;
    global Boolean IsClosed;
    global Datetime ClosedDate;
    global SObject Owner;
    global Id OwnerId;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global String ContactPhone;
    global String ContactMobile;
    global String ContactEmail;
    global String ContactFax;
    global String Comments;
    global Datetime LastViewedDate;
    global Datetime LastReferencedDate;
    global String CreatorFullPhotoUrl;
    global String CreatorSmallPhotoUrl;
    global String CreatorName;
    global String Product_Group__c;
    /* When was the date and time that this issue was identified?
    */
    global Datetime Issue_Identified__c;
    global String Product_Categroy__c;
    global String Sales_Team__c;
    global String Case_Outcome__c;
    /* Date and time the case occured
    */
    global Datetime Case_Date__c;
    global String Product_Group_1__c;
    global String Product_Group_2__c;
    global String Product_Group_3__c;
    global String Product_Group_4__c;
    global String Product_Group_5__c;
    global String Product_Group_6__c;
    global String Product_Group_7__c;
    global String Quantity_1__c;
    global String Quantity_2__c;
    global String Quantity_3__c;
    global String Quantity_4__c;
    global String Quantity_5__c;
    global String Quantity_6__c;
    global String Quantity_7__c;
    global String Account_Number__c;
    global Double Total_Cases__c;
    global Double Case_Entry_Days__c;
    global Campaign Campaign__r;
    global Id Campaign__c;
    /* From the related Campaign Type field.
    */
    global String Campaign_Type__c;
    /* Checked if the create date does not equal the Case Date.
    */
    global Double Days_Beyond_Compliance__c;
    /* A count of the number of products used
    */
    global Double Products_Used__c;
    global String CaseOwnerName__c;
    /* For WavelinQ cases identify the name of the BDPI certified trainer if present in the case
    */
    global User Certified_Trainer__r;
    /* For WavelinQ cases identify the name of the BDPI certified trainer if present in the case
    */
    global Id Certified_Trainer__c;
    /* Select Yes or No for a WavelinQ case. All other product cases do not require this field to be selected
    */
    global String WavelinQ_Brachial_Vein_Coiled__c;
    /* If known enter the name of the dialysis center that the patient will be going to
    */
    global String WavelinQ_Identify_Dialysis__c;
    global Double Avg_Entry_Time__c;
    global Datetime Case_End_Date__c;
    global String WavelinQ_Fistula_Creation_Site__c;
    global String WavelinQ_Patient_Dialysis_Status__c;
    /* This field will be checked for reporting of physician signoff
    */
    global Boolean WavelinQ_Signoff__c;
    /* Pick the EFS member to be notified about this case
    */
    global String EFS_Member__c;
    global User EFS__r;
    global Id EFS__c;
    global Opportunity Opportunity__r;
    global Id Opportunity__c;
    global Boolean EFS_is_Me__c;
    global String Vascular_District__c;
    global String Biopsy_DIstrict__c;
    global String Peripheral_District__c;
    /* This was a face to face interaction
    */
    global Boolean Face_to_Face__c;
    global String Case_Contact_Name__c;
    global String Case_Account_Name__c;
    global Datetime Face_to_Face_Logged__c;
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CaseComment> CaseComments;
    global List<CaseContactRole> CaseContactRoles;
    global List<CaseFeed> Feeds;
    global List<CaseHistory> Histories;
    global List<CaseShare> Shares;
    global List<CaseSolution> CaseSolutions;
    global List<CaseTeamMember> TeamMembers;
    global List<CaseTeamTemplateRecord> TeamTemplateRecords;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<EmailMessage> EmailMessages;
    global List<EmailMessage> Emails;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Event> Events;
    global List<EventRelation> EventRelations;
    global List<LookedUpFromActivity> Activities__r;
    global List<OpenActivity> OpenActivities;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<Product_Used__c> Products_Used__r;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<SocialPost> Posts;
    global List<Task> Tasks;
    global List<TaskRelation> TaskRelations;
    global List<TopicAssignment> TopicAssignments;
    global List<Case__hd> Parent;
    global List<ContentDistribution> RelatedRecord;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<Opportunity__hd> Parent;
    global List<TaskChangeEvent> What;
    global List<TaskRelationChangeEvent> Relation;

    global Case () 
    {
    }
}