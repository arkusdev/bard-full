declare module "@salesforce/label/c.DM_KOL_Roles" {
    var DM_KOL_Roles: string;
    export default DM_KOL_Roles;
}
declare module "@salesforce/label/c.RM_KOL_Roles" {
    var RM_KOL_Roles: string;
    export default RM_KOL_Roles;
}