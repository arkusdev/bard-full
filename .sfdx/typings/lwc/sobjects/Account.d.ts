declare module "@salesforce/schema/Account.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Account.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Account.MasterRecord" {
  const MasterRecord:any;
  export default MasterRecord;
}
declare module "@salesforce/schema/Account.MasterRecordId" {
  const MasterRecordId:any;
  export default MasterRecordId;
}
declare module "@salesforce/schema/Account.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Account.Type" {
  const Type:string;
  export default Type;
}
declare module "@salesforce/schema/Account.RecordType" {
  const RecordType:any;
  export default RecordType;
}
declare module "@salesforce/schema/Account.RecordTypeId" {
  const RecordTypeId:any;
  export default RecordTypeId;
}
declare module "@salesforce/schema/Account.Parent" {
  const Parent:any;
  export default Parent;
}
declare module "@salesforce/schema/Account.ParentId" {
  const ParentId:any;
  export default ParentId;
}
declare module "@salesforce/schema/Account.BillingStreet" {
  const BillingStreet:string;
  export default BillingStreet;
}
declare module "@salesforce/schema/Account.BillingCity" {
  const BillingCity:string;
  export default BillingCity;
}
declare module "@salesforce/schema/Account.BillingState" {
  const BillingState:string;
  export default BillingState;
}
declare module "@salesforce/schema/Account.BillingPostalCode" {
  const BillingPostalCode:string;
  export default BillingPostalCode;
}
declare module "@salesforce/schema/Account.BillingCountry" {
  const BillingCountry:string;
  export default BillingCountry;
}
declare module "@salesforce/schema/Account.BillingLatitude" {
  const BillingLatitude:number;
  export default BillingLatitude;
}
declare module "@salesforce/schema/Account.BillingLongitude" {
  const BillingLongitude:number;
  export default BillingLongitude;
}
declare module "@salesforce/schema/Account.BillingGeocodeAccuracy" {
  const BillingGeocodeAccuracy:string;
  export default BillingGeocodeAccuracy;
}
declare module "@salesforce/schema/Account.BillingAddress" {
  const BillingAddress:any;
  export default BillingAddress;
}
declare module "@salesforce/schema/Account.ShippingStreet" {
  const ShippingStreet:string;
  export default ShippingStreet;
}
declare module "@salesforce/schema/Account.ShippingCity" {
  const ShippingCity:string;
  export default ShippingCity;
}
declare module "@salesforce/schema/Account.ShippingState" {
  const ShippingState:string;
  export default ShippingState;
}
declare module "@salesforce/schema/Account.ShippingPostalCode" {
  const ShippingPostalCode:string;
  export default ShippingPostalCode;
}
declare module "@salesforce/schema/Account.ShippingCountry" {
  const ShippingCountry:string;
  export default ShippingCountry;
}
declare module "@salesforce/schema/Account.ShippingLatitude" {
  const ShippingLatitude:number;
  export default ShippingLatitude;
}
declare module "@salesforce/schema/Account.ShippingLongitude" {
  const ShippingLongitude:number;
  export default ShippingLongitude;
}
declare module "@salesforce/schema/Account.ShippingGeocodeAccuracy" {
  const ShippingGeocodeAccuracy:string;
  export default ShippingGeocodeAccuracy;
}
declare module "@salesforce/schema/Account.ShippingAddress" {
  const ShippingAddress:any;
  export default ShippingAddress;
}
declare module "@salesforce/schema/Account.Phone" {
  const Phone:string;
  export default Phone;
}
declare module "@salesforce/schema/Account.Fax" {
  const Fax:string;
  export default Fax;
}
declare module "@salesforce/schema/Account.Website" {
  const Website:string;
  export default Website;
}
declare module "@salesforce/schema/Account.PhotoUrl" {
  const PhotoUrl:string;
  export default PhotoUrl;
}
declare module "@salesforce/schema/Account.Sic" {
  const Sic:string;
  export default Sic;
}
declare module "@salesforce/schema/Account.Industry" {
  const Industry:string;
  export default Industry;
}
declare module "@salesforce/schema/Account.AnnualRevenue" {
  const AnnualRevenue:number;
  export default AnnualRevenue;
}
declare module "@salesforce/schema/Account.NumberOfEmployees" {
  const NumberOfEmployees:number;
  export default NumberOfEmployees;
}
declare module "@salesforce/schema/Account.Ownership" {
  const Ownership:string;
  export default Ownership;
}
declare module "@salesforce/schema/Account.TickerSymbol" {
  const TickerSymbol:string;
  export default TickerSymbol;
}
declare module "@salesforce/schema/Account.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/Account.Rating" {
  const Rating:string;
  export default Rating;
}
declare module "@salesforce/schema/Account.Site" {
  const Site:string;
  export default Site;
}
declare module "@salesforce/schema/Account.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Account.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Account.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Account.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Account.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Account.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Account.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Account.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Account.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Account.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Account.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Account.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Account.JigsawCompanyId" {
  const JigsawCompanyId:string;
  export default JigsawCompanyId;
}
declare module "@salesforce/schema/Account.AccountSource" {
  const AccountSource:string;
  export default AccountSource;
}
declare module "@salesforce/schema/Account.SicDesc" {
  const SicDesc:string;
  export default SicDesc;
}
declare module "@salesforce/schema/Account.Biopsy_Prior_Year__c" {
  const Biopsy_Prior_Year__c:number;
  export default Biopsy_Prior_Year__c;
}
declare module "@salesforce/schema/Account.Biopsy_Status__c" {
  const Biopsy_Status__c:string;
  export default Biopsy_Status__c;
}
declare module "@salesforce/schema/Account.Biopsy_YTD__c" {
  const Biopsy_YTD__c:number;
  export default Biopsy_YTD__c;
}
declare module "@salesforce/schema/Account.Peripheral_Prior_Year__c" {
  const Peripheral_Prior_Year__c:number;
  export default Peripheral_Prior_Year__c;
}
declare module "@salesforce/schema/Account.Peripheral_Status__c" {
  const Peripheral_Status__c:string;
  export default Peripheral_Status__c;
}
declare module "@salesforce/schema/Account.Peripheral_Territory__r" {
  const Peripheral_Territory__r:any;
  export default Peripheral_Territory__r;
}
declare module "@salesforce/schema/Account.Peripheral_Territory__c" {
  const Peripheral_Territory__c:any;
  export default Peripheral_Territory__c;
}
declare module "@salesforce/schema/Account.Peripheral_YTD__c" {
  const Peripheral_YTD__c:number;
  export default Peripheral_YTD__c;
}
declare module "@salesforce/schema/Account.Total_Prior_Year__c" {
  const Total_Prior_Year__c:number;
  export default Total_Prior_Year__c;
}
declare module "@salesforce/schema/Account.Total_YTD__c" {
  const Total_YTD__c:number;
  export default Total_YTD__c;
}
declare module "@salesforce/schema/Account.Vascular_Prior_Year__c" {
  const Vascular_Prior_Year__c:number;
  export default Vascular_Prior_Year__c;
}
declare module "@salesforce/schema/Account.Vascular_Status__c" {
  const Vascular_Status__c:string;
  export default Vascular_Status__c;
}
declare module "@salesforce/schema/Account.Vascular_Territory__r" {
  const Vascular_Territory__r:any;
  export default Vascular_Territory__r;
}
declare module "@salesforce/schema/Account.Vascular_Territory__c" {
  const Vascular_Territory__c:any;
  export default Vascular_Territory__c;
}
declare module "@salesforce/schema/Account.Vascular_YTD__c" {
  const Vascular_YTD__c:number;
  export default Vascular_YTD__c;
}
declare module "@salesforce/schema/Account.Biopsy_Open_Opportuniites__c" {
  const Biopsy_Open_Opportuniites__c:number;
  export default Biopsy_Open_Opportuniites__c;
}
declare module "@salesforce/schema/Account.Peripheral_Open_Opportunities__c" {
  const Peripheral_Open_Opportunities__c:number;
  export default Peripheral_Open_Opportunities__c;
}
declare module "@salesforce/schema/Account.Account_Rating__c" {
  const Account_Rating__c:string;
  export default Account_Rating__c;
}
declare module "@salesforce/schema/Account.Vascular_Open_Opportunities__c" {
  const Vascular_Open_Opportunities__c:number;
  export default Vascular_Open_Opportunities__c;
}
declare module "@salesforce/schema/Account.Date_Lost__c" {
  const Date_Lost__c:any;
  export default Date_Lost__c;
}
declare module "@salesforce/schema/Account.No_Contacts__c" {
  const No_Contacts__c:boolean;
  export default No_Contacts__c;
}
declare module "@salesforce/schema/Account.Biopsy_District__c" {
  const Biopsy_District__c:string;
  export default Biopsy_District__c;
}
declare module "@salesforce/schema/Account.Org1ID__c" {
  const Org1ID__c:string;
  export default Org1ID__c;
}
declare module "@salesforce/schema/Account.Legacy_Vascular_TM_ID__c" {
  const Legacy_Vascular_TM_ID__c:string;
  export default Legacy_Vascular_TM_ID__c;
}
declare module "@salesforce/schema/Account.Legacy_Sales_Person_2__c" {
  const Legacy_Sales_Person_2__c:string;
  export default Legacy_Sales_Person_2__c;
}
declare module "@salesforce/schema/Account.Legacy_Pheripheral_TM1_ID__c" {
  const Legacy_Pheripheral_TM1_ID__c:string;
  export default Legacy_Pheripheral_TM1_ID__c;
}
declare module "@salesforce/schema/Account.Peripheral_District__c" {
  const Peripheral_District__c:string;
  export default Peripheral_District__c;
}
declare module "@salesforce/schema/Account.BiopsyTMUpdated__c" {
  const BiopsyTMUpdated__c:boolean;
  export default BiopsyTMUpdated__c;
}
declare module "@salesforce/schema/Account.Bill_to_Customer_Name__c" {
  const Bill_to_Customer_Name__c:string;
  export default Bill_to_Customer_Name__c;
}
declare module "@salesforce/schema/Account.Bill_to_ID__c" {
  const Bill_to_ID__c:string;
  export default Bill_to_ID__c;
}
declare module "@salesforce/schema/Account.CC9__c" {
  const CC9__c:string;
  export default CC9__c;
}
declare module "@salesforce/schema/Account.Capital_Equipment__c" {
  const Capital_Equipment__c:number;
  export default Capital_Equipment__c;
}
declare module "@salesforce/schema/Account.Competitor_Product__c" {
  const Competitor_Product__c:string;
  export default Competitor_Product__c;
}
declare module "@salesforce/schema/Account.Competitor_Volumes__c" {
  const Competitor_Volumes__c:number;
  export default Competitor_Volumes__c;
}
declare module "@salesforce/schema/Account.Competitor__r" {
  const Competitor__r:any;
  export default Competitor__r;
}
declare module "@salesforce/schema/Account.Competitor__c" {
  const Competitor__c:any;
  export default Competitor__c;
}
declare module "@salesforce/schema/Account.Customer_Type__c" {
  const Customer_Type__c:string;
  export default Customer_Type__c;
}
declare module "@salesforce/schema/Account.Facility_Type__c" {
  const Facility_Type__c:string;
  export default Facility_Type__c;
}
declare module "@salesforce/schema/Account.JDE_Facility_Type_Code__c" {
  const JDE_Facility_Type_Code__c:string;
  export default JDE_Facility_Type_Code__c;
}
declare module "@salesforce/schema/Account.JDE_Facility_Type__c" {
  const JDE_Facility_Type__c:string;
  export default JDE_Facility_Type__c;
}
declare module "@salesforce/schema/Account.JDE_Type__c" {
  const JDE_Type__c:string;
  export default JDE_Type__c;
}
declare module "@salesforce/schema/Account.OBL_Hospital__c" {
  const OBL_Hospital__c:string;
  export default OBL_Hospital__c;
}
declare module "@salesforce/schema/Account.Ship_to_ID__c" {
  const Ship_to_ID__c:string;
  export default Ship_to_ID__c;
}
declare module "@salesforce/schema/Account.Territory__r" {
  const Territory__r:any;
  export default Territory__r;
}
declare module "@salesforce/schema/Account.Territory__c" {
  const Territory__c:any;
  export default Territory__c;
}
declare module "@salesforce/schema/Account.Account_Alias__c" {
  const Account_Alias__c:string;
  export default Account_Alias__c;
}
declare module "@salesforce/schema/Account.Opportunities__c" {
  const Opportunities__c:number;
  export default Opportunities__c;
}
declare module "@salesforce/schema/Account.Account__c" {
  const Account__c:number;
  export default Account__c;
}
declare module "@salesforce/schema/Account.AccountNo__c" {
  const AccountNo__c:string;
  export default AccountNo__c;
}
declare module "@salesforce/schema/Account.Active__c" {
  const Active__c:string;
  export default Active__c;
}
declare module "@salesforce/schema/Account.GPO__c" {
  const GPO__c:string;
  export default GPO__c;
}
declare module "@salesforce/schema/Account.Old_ID__c" {
  const Old_ID__c:string;
  export default Old_ID__c;
}
declare module "@salesforce/schema/Account.Products_Discussed__c" {
  const Products_Discussed__c:string;
  export default Products_Discussed__c;
}
declare module "@salesforce/schema/Account.SLA__c" {
  const SLA__c:string;
  export default SLA__c;
}
declare module "@salesforce/schema/Account.Sales_Person_2__r" {
  const Sales_Person_2__r:any;
  export default Sales_Person_2__r;
}
declare module "@salesforce/schema/Account.Sales_Person_2__c" {
  const Sales_Person_2__c:any;
  export default Sales_Person_2__c;
}
declare module "@salesforce/schema/Account.Vascular_TM__r" {
  const Vascular_TM__r:any;
  export default Vascular_TM__r;
}
declare module "@salesforce/schema/Account.Vascular_TM__c" {
  const Vascular_TM__c:any;
  export default Vascular_TM__c;
}
declare module "@salesforce/schema/Account.X2012_Drainage_Sales__c" {
  const X2012_Drainage_Sales__c:number;
  export default X2012_Drainage_Sales__c;
}
declare module "@salesforce/schema/Account.Vascular_District__c" {
  const Vascular_District__c:string;
  export default Vascular_District__c;
}
declare module "@salesforce/schema/Account.Lutonix_220_At_Risk_Account__c" {
  const Lutonix_220_At_Risk_Account__c:boolean;
  export default Lutonix_220_At_Risk_Account__c;
}
declare module "@salesforce/schema/Account.Risk_Initiative__c" {
  const Risk_Initiative__c:string;
  export default Risk_Initiative__c;
}
declare module "@salesforce/schema/Account.Risk_Level__c" {
  const Risk_Level__c:string;
  export default Risk_Level__c;
}
declare module "@salesforce/schema/Account.LTX_220_Codes_Delivered__c" {
  const LTX_220_Codes_Delivered__c:string;
  export default LTX_220_Codes_Delivered__c;
}
declare module "@salesforce/schema/Account.LTX_Safety_Efficacy_Message_Delivered__c" {
  const LTX_Safety_Efficacy_Message_Delivered__c:string;
  export default LTX_Safety_Efficacy_Message_Delivered__c;
}
declare module "@salesforce/schema/Account.Initiative_Account_Rank__c" {
  const Initiative_Account_Rank__c:number;
  export default Initiative_Account_Rank__c;
}
declare module "@salesforce/schema/Account.X2017_BD_Lutonix_Sales__c" {
  const X2017_BD_Lutonix_Sales__c:number;
  export default X2017_BD_Lutonix_Sales__c;
}
declare module "@salesforce/schema/Account.X2018_YTD_BD_Lutonix_Sales__c" {
  const X2018_YTD_BD_Lutonix_Sales__c:number;
  export default X2018_YTD_BD_Lutonix_Sales__c;
}
declare module "@salesforce/schema/Account.Annual_Medtronic_Sales__c" {
  const Annual_Medtronic_Sales__c:number;
  export default Annual_Medtronic_Sales__c;
}
declare module "@salesforce/schema/Account.Annual_Spectranetics_Sales__c" {
  const Annual_Spectranetics_Sales__c:number;
  export default Annual_Spectranetics_Sales__c;
}
declare module "@salesforce/schema/Account.Risk_Initiative_Notes__c" {
  const Risk_Initiative_Notes__c:string;
  export default Risk_Initiative_Notes__c;
}
declare module "@salesforce/schema/Account.AV_Lutonix_Patient_Campaign__c" {
  const AV_Lutonix_Patient_Campaign__c:boolean;
  export default AV_Lutonix_Patient_Campaign__c;
}
declare module "@salesforce/schema/Account.User_assigned_to_AV_LTX_Campaign__r" {
  const User_assigned_to_AV_LTX_Campaign__r:any;
  export default User_assigned_to_AV_LTX_Campaign__r;
}
declare module "@salesforce/schema/Account.User_assigned_to_AV_LTX_Campaign__c" {
  const User_assigned_to_AV_LTX_Campaign__c:any;
  export default User_assigned_to_AV_LTX_Campaign__c;
}
declare module "@salesforce/schema/Account.PAD_CS_ASR_Goal_Account__c" {
  const PAD_CS_ASR_Goal_Account__c:boolean;
  export default PAD_CS_ASR_Goal_Account__c;
}
declare module "@salesforce/schema/Account.CS_ASR__r" {
  const CS_ASR__r:any;
  export default CS_ASR__r;
}
declare module "@salesforce/schema/Account.CS_ASR__c" {
  const CS_ASR__c:any;
  export default CS_ASR__c;
}
declare module "@salesforce/schema/Account.PAD_CS_ASR_Lifestream_Account__c" {
  const PAD_CS_ASR_Lifestream_Account__c:boolean;
  export default PAD_CS_ASR_Lifestream_Account__c;
}
declare module "@salesforce/schema/Account.PAD_C_S_ASR_Venovo_Account__c" {
  const PAD_C_S_ASR_Venovo_Account__c:boolean;
  export default PAD_C_S_ASR_Venovo_Account__c;
}
declare module "@salesforce/schema/Account.Backorders__c" {
  const Backorders__c:number;
  export default Backorders__c;
}
declare module "@salesforce/schema/Account.Facility__r" {
  const Facility__r:any;
  export default Facility__r;
}
declare module "@salesforce/schema/Account.Facility__c" {
  const Facility__c:any;
  export default Facility__c;
}
declare module "@salesforce/schema/Account.Surpass_Defense_Account__c" {
  const Surpass_Defense_Account__c:boolean;
  export default Surpass_Defense_Account__c;
}
declare module "@salesforce/schema/Account.IR__c" {
  const IR__c:boolean;
  export default IR__c;
}
declare module "@salesforce/schema/Account.Infusion_Nurse__c" {
  const Infusion_Nurse__c:boolean;
  export default Infusion_Nurse__c;
}
declare module "@salesforce/schema/Account.Oncologist__c" {
  const Oncologist__c:boolean;
  export default Oncologist__c;
}
declare module "@salesforce/schema/Account.Supply_Chain__c" {
  const Supply_Chain__c:boolean;
  export default Supply_Chain__c;
}
declare module "@salesforce/schema/Account.PAD_CS_ASR_Lutonix_Account__c" {
  const PAD_CS_ASR_Lutonix_Account__c:boolean;
  export default PAD_CS_ASR_Lutonix_Account__c;
}
declare module "@salesforce/schema/Account.Customer_ERP_ID__c" {
  const Customer_ERP_ID__c:string;
  export default Customer_ERP_ID__c;
}
declare module "@salesforce/schema/Account.ERP_Customer_ID__c" {
  const ERP_Customer_ID__c:string;
  export default ERP_Customer_ID__c;
}
declare module "@salesforce/schema/Account.Enterprise_Account_Number__c" {
  const Enterprise_Account_Number__c:string;
  export default Enterprise_Account_Number__c;
}
declare module "@salesforce/schema/Account.DH_Hospital_ID__c" {
  const DH_Hospital_ID__c:string;
  export default DH_Hospital_ID__c;
}
declare module "@salesforce/schema/Account.Enterprise_Account_Network__c" {
  const Enterprise_Account_Network__c:string;
  export default Enterprise_Account_Network__c;
}
declare module "@salesforce/schema/Account.Enterprise_Account_Network_Name__c" {
  const Enterprise_Account_Network_Name__c:string;
  export default Enterprise_Account_Network_Name__c;
}
declare module "@salesforce/schema/Account.Enterprise_Account_Network_Parent__c" {
  const Enterprise_Account_Network_Parent__c:string;
  export default Enterprise_Account_Network_Parent__c;
}
declare module "@salesforce/schema/Account.Enterprise_Account_Network_Parent_Name__c" {
  const Enterprise_Account_Network_Parent_Name__c:string;
  export default Enterprise_Account_Network_Parent_Name__c;
}
declare module "@salesforce/schema/Account.EAN_Total_Sales__c" {
  const EAN_Total_Sales__c:number;
  export default EAN_Total_Sales__c;
}
declare module "@salesforce/schema/Account.Surviving_Customer_Group__c" {
  const Surviving_Customer_Group__c:string;
  export default Surviving_Customer_Group__c;
}
declare module "@salesforce/schema/Account.Surviving_Customer_Classification__c" {
  const Surviving_Customer_Classification__c:string;
  export default Surviving_Customer_Classification__c;
}
