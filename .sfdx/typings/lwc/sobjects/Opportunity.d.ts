declare module "@salesforce/schema/Opportunity.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Opportunity.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Opportunity.Account" {
  const Account:any;
  export default Account;
}
declare module "@salesforce/schema/Opportunity.AccountId" {
  const AccountId:any;
  export default AccountId;
}
declare module "@salesforce/schema/Opportunity.RecordType" {
  const RecordType:any;
  export default RecordType;
}
declare module "@salesforce/schema/Opportunity.RecordTypeId" {
  const RecordTypeId:any;
  export default RecordTypeId;
}
declare module "@salesforce/schema/Opportunity.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Opportunity.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/Opportunity.StageName" {
  const StageName:string;
  export default StageName;
}
declare module "@salesforce/schema/Opportunity.Amount" {
  const Amount:number;
  export default Amount;
}
declare module "@salesforce/schema/Opportunity.Probability" {
  const Probability:number;
  export default Probability;
}
declare module "@salesforce/schema/Opportunity.CloseDate" {
  const CloseDate:any;
  export default CloseDate;
}
declare module "@salesforce/schema/Opportunity.Type" {
  const Type:string;
  export default Type;
}
declare module "@salesforce/schema/Opportunity.NextStep" {
  const NextStep:string;
  export default NextStep;
}
declare module "@salesforce/schema/Opportunity.LeadSource" {
  const LeadSource:string;
  export default LeadSource;
}
declare module "@salesforce/schema/Opportunity.IsClosed" {
  const IsClosed:boolean;
  export default IsClosed;
}
declare module "@salesforce/schema/Opportunity.IsWon" {
  const IsWon:boolean;
  export default IsWon;
}
declare module "@salesforce/schema/Opportunity.ForecastCategory" {
  const ForecastCategory:string;
  export default ForecastCategory;
}
declare module "@salesforce/schema/Opportunity.ForecastCategoryName" {
  const ForecastCategoryName:string;
  export default ForecastCategoryName;
}
declare module "@salesforce/schema/Opportunity.Campaign" {
  const Campaign:any;
  export default Campaign;
}
declare module "@salesforce/schema/Opportunity.CampaignId" {
  const CampaignId:any;
  export default CampaignId;
}
declare module "@salesforce/schema/Opportunity.HasOpportunityLineItem" {
  const HasOpportunityLineItem:boolean;
  export default HasOpportunityLineItem;
}
declare module "@salesforce/schema/Opportunity.Pricebook2" {
  const Pricebook2:any;
  export default Pricebook2;
}
declare module "@salesforce/schema/Opportunity.Pricebook2Id" {
  const Pricebook2Id:any;
  export default Pricebook2Id;
}
declare module "@salesforce/schema/Opportunity.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Opportunity.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Opportunity.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Opportunity.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Opportunity.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Opportunity.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Opportunity.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Opportunity.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Opportunity.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Opportunity.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Opportunity.FiscalQuarter" {
  const FiscalQuarter:number;
  export default FiscalQuarter;
}
declare module "@salesforce/schema/Opportunity.FiscalYear" {
  const FiscalYear:number;
  export default FiscalYear;
}
declare module "@salesforce/schema/Opportunity.Fiscal" {
  const Fiscal:string;
  export default Fiscal;
}
declare module "@salesforce/schema/Opportunity.Contact" {
  const Contact:any;
  export default Contact;
}
declare module "@salesforce/schema/Opportunity.ContactId" {
  const ContactId:any;
  export default ContactId;
}
declare module "@salesforce/schema/Opportunity.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Opportunity.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Opportunity.SyncedQuote" {
  const SyncedQuote:any;
  export default SyncedQuote;
}
declare module "@salesforce/schema/Opportunity.SyncedQuoteId" {
  const SyncedQuoteId:any;
  export default SyncedQuoteId;
}
declare module "@salesforce/schema/Opportunity.HasOpenActivity" {
  const HasOpenActivity:boolean;
  export default HasOpenActivity;
}
declare module "@salesforce/schema/Opportunity.HasOverdueTask" {
  const HasOverdueTask:boolean;
  export default HasOverdueTask;
}
declare module "@salesforce/schema/Opportunity.ASP__c" {
  const ASP__c:number;
  export default ASP__c;
}
declare module "@salesforce/schema/Opportunity.Org1ID__c" {
  const Org1ID__c:string;
  export default Org1ID__c;
}
declare module "@salesforce/schema/Opportunity.Opportunity_Alias__c" {
  const Opportunity_Alias__c:string;
  export default Opportunity_Alias__c;
}
declare module "@salesforce/schema/Opportunity.Appropriate_Personnel_on_Board__c" {
  const Appropriate_Personnel_on_Board__c:boolean;
  export default Appropriate_Personnel_on_Board__c;
}
declare module "@salesforce/schema/Opportunity.Codes_in_the_System__c" {
  const Codes_in_the_System__c:boolean;
  export default Codes_in_the_System__c;
}
declare module "@salesforce/schema/Opportunity.Estimated_Procedures__c" {
  const Estimated_Procedures__c:number;
  export default Estimated_Procedures__c;
}
declare module "@salesforce/schema/Opportunity.Evaluation_Parameters_Agreed_Upon__c" {
  const Evaluation_Parameters_Agreed_Upon__c:boolean;
  export default Evaluation_Parameters_Agreed_Upon__c;
}
declare module "@salesforce/schema/Opportunity.Evaluation_Start_Date__c" {
  const Evaluation_Start_Date__c:any;
  export default Evaluation_Start_Date__c;
}
declare module "@salesforce/schema/Opportunity.Legacy_ID__c" {
  const Legacy_ID__c:string;
  export default Legacy_ID__c;
}
declare module "@salesforce/schema/Opportunity.Lost_Category__c" {
  const Lost_Category__c:string;
  export default Lost_Category__c;
}
declare module "@salesforce/schema/Opportunity.Lost_Details__c" {
  const Lost_Details__c:string;
  export default Lost_Details__c;
}
declare module "@salesforce/schema/Opportunity.Lost_Reason__c" {
  const Lost_Reason__c:string;
  export default Lost_Reason__c;
}
declare module "@salesforce/schema/Opportunity.PO_Number__c" {
  const PO_Number__c:string;
  export default PO_Number__c;
}
declare module "@salesforce/schema/Opportunity.Presentation_Date__c" {
  const Presentation_Date__c:any;
  export default Presentation_Date__c;
}
declare module "@salesforce/schema/Opportunity.Pricing_Approved_Established__c" {
  const Pricing_Approved_Established__c:boolean;
  export default Pricing_Approved_Established__c;
}
declare module "@salesforce/schema/Opportunity.Product_Category__c" {
  const Product_Category__c:string;
  export default Product_Category__c;
}
declare module "@salesforce/schema/Opportunity.Product_Group__c" {
  const Product_Group__c:string;
  export default Product_Group__c;
}
declare module "@salesforce/schema/Opportunity.Rating__c" {
  const Rating__c:string;
  export default Rating__c;
}
declare module "@salesforce/schema/Opportunity.Strategy__c" {
  const Strategy__c:string;
  export default Strategy__c;
}
declare module "@salesforce/schema/Opportunity.VAC_Approved__c" {
  const VAC_Approved__c:boolean;
  export default VAC_Approved__c;
}
declare module "@salesforce/schema/Opportunity.Biopsy_District_Number__c" {
  const Biopsy_District_Number__c:string;
  export default Biopsy_District_Number__c;
}
declare module "@salesforce/schema/Opportunity.Account_Alias__c" {
  const Account_Alias__c:string;
  export default Account_Alias__c;
}
declare module "@salesforce/schema/Opportunity.Biopsy_District_Name__c" {
  const Biopsy_District_Name__c:string;
  export default Biopsy_District_Name__c;
}
declare module "@salesforce/schema/Opportunity.of_Procedures_Worked__c" {
  const of_Procedures_Worked__c:number;
  export default of_Procedures_Worked__c;
}
declare module "@salesforce/schema/Opportunity.Carry_Over__c" {
  const Carry_Over__c:number;
  export default Carry_Over__c;
}
declare module "@salesforce/schema/Opportunity.Current_Year__c" {
  const Current_Year__c:number;
  export default Current_Year__c;
}
declare module "@salesforce/schema/Opportunity.Competitors__c" {
  const Competitors__c:number;
  export default Competitors__c;
}
declare module "@salesforce/schema/Opportunity.Account__c" {
  const Account__c:string;
  export default Account__c;
}
declare module "@salesforce/schema/Opportunity.Sent_to_the_Field__c" {
  const Sent_to_the_Field__c:boolean;
  export default Sent_to_the_Field__c;
}
declare module "@salesforce/schema/Opportunity.Clinical_Champion_Established__c" {
  const Clinical_Champion_Established__c:boolean;
  export default Clinical_Champion_Established__c;
}
declare module "@salesforce/schema/Opportunity.Codes_on_GPO_Contract__c" {
  const Codes_on_GPO_Contract__c:boolean;
  export default Codes_on_GPO_Contract__c;
}
declare module "@salesforce/schema/Opportunity.Committee_Meeting_Date__c" {
  const Committee_Meeting_Date__c:any;
  export default Committee_Meeting_Date__c;
}
declare module "@salesforce/schema/Opportunity.Conversion_Parameters_Agreed_Upon__c" {
  const Conversion_Parameters_Agreed_Upon__c:boolean;
  export default Conversion_Parameters_Agreed_Upon__c;
}
declare module "@salesforce/schema/Opportunity.Document_Every_Case_with_MD_s_Signature__c" {
  const Document_Every_Case_with_MD_s_Signature__c:boolean;
  export default Document_Every_Case_with_MD_s_Signature__c;
}
declare module "@salesforce/schema/Opportunity.End_User_Support__c" {
  const End_User_Support__c:boolean;
  export default End_User_Support__c;
}
declare module "@salesforce/schema/Opportunity.In_Committee__c" {
  const In_Committee__c:boolean;
  export default In_Committee__c;
}
declare module "@salesforce/schema/Opportunity.In_Service_the_Staff__c" {
  const In_Service_the_Staff__c:boolean;
  export default In_Service_the_Staff__c;
}
declare module "@salesforce/schema/Opportunity.Mid_Final_Evaluation_Progress_Report__c" {
  const Mid_Final_Evaluation_Progress_Report__c:boolean;
  export default Mid_Final_Evaluation_Progress_Report__c;
}
declare module "@salesforce/schema/Opportunity.Reinforce_Messaging__c" {
  const Reinforce_Messaging__c:boolean;
  export default Reinforce_Messaging__c;
}
declare module "@salesforce/schema/Opportunity.of_Units__c" {
  const of_Units__c:number;
  export default of_Units__c;
}
declare module "@salesforce/schema/Opportunity.Annual_BTK_Case_Volume__c" {
  const Annual_BTK_Case_Volume__c:number;
  export default Annual_BTK_Case_Volume__c;
}
declare module "@salesforce/schema/Opportunity.Annual_SFA_Case_Volume__c" {
  const Annual_SFA_Case_Volume__c:number;
  export default Annual_SFA_Case_Volume__c;
}
declare module "@salesforce/schema/Opportunity.Peripheral_District_Number__c" {
  const Peripheral_District_Number__c:string;
  export default Peripheral_District_Number__c;
}
declare module "@salesforce/schema/Opportunity.Peripheral_District__c" {
  const Peripheral_District__c:string;
  export default Peripheral_District__c;
}
declare module "@salesforce/schema/Opportunity.Vascular_District_Number__c" {
  const Vascular_District_Number__c:string;
  export default Vascular_District_Number__c;
}
declare module "@salesforce/schema/Opportunity.Vascular_District__c" {
  const Vascular_District__c:string;
  export default Vascular_District__c;
}
declare module "@salesforce/schema/Opportunity.Peripheral_Territory_Correct__c" {
  const Peripheral_Territory_Correct__c:boolean;
  export default Peripheral_Territory_Correct__c;
}
declare module "@salesforce/schema/Opportunity.Vascular_Territory_Correct__c" {
  const Vascular_Territory_Correct__c:boolean;
  export default Vascular_Territory_Correct__c;
}
declare module "@salesforce/schema/Opportunity.Biopsy_Territory_Correct__c" {
  const Biopsy_Territory_Correct__c:boolean;
  export default Biopsy_Territory_Correct__c;
}
declare module "@salesforce/schema/Opportunity.A1_Profiling_Opportunity__c" {
  const A1_Profiling_Opportunity__c:boolean;
  export default A1_Profiling_Opportunity__c;
}
declare module "@salesforce/schema/Opportunity.VAC_Timeline__c" {
  const VAC_Timeline__c:string;
  export default VAC_Timeline__c;
}
declare module "@salesforce/schema/Opportunity.First_Order_Expected__c" {
  const First_Order_Expected__c:any;
  export default First_Order_Expected__c;
}
declare module "@salesforce/schema/Opportunity.of_Units_on_First_Order__c" {
  const of_Units_on_First_Order__c:number;
  export default of_Units_on_First_Order__c;
}
declare module "@salesforce/schema/Opportunity.Consignment_Needed__c" {
  const Consignment_Needed__c:string;
  export default Consignment_Needed__c;
}
declare module "@salesforce/schema/Opportunity.of_Consignment_Units_Needed__c" {
  const of_Consignment_Units_Needed__c:number;
  export default of_Consignment_Units_Needed__c;
}
declare module "@salesforce/schema/Opportunity.BDPI_Sole_Source__c" {
  const BDPI_Sole_Source__c:string;
  export default BDPI_Sole_Source__c;
}
declare module "@salesforce/schema/Opportunity.Annual_SFA_DCB_Case_Volume__c" {
  const Annual_SFA_DCB_Case_Volume__c:number;
  export default Annual_SFA_DCB_Case_Volume__c;
}
declare module "@salesforce/schema/Opportunity.MDT_DCB_Share__c" {
  const MDT_DCB_Share__c:number;
  export default MDT_DCB_Share__c;
}
declare module "@salesforce/schema/Opportunity.BD_DCB_Share__c" {
  const BD_DCB_Share__c:number;
  export default BD_DCB_Share__c;
}
declare module "@salesforce/schema/Opportunity.SPNC_DCB_Share__c" {
  const SPNC_DCB_Share__c:number;
  export default SPNC_DCB_Share__c;
}
declare module "@salesforce/schema/Opportunity.Current_BTK_Therapy__c" {
  const Current_BTK_Therapy__c:string;
  export default Current_BTK_Therapy__c;
}
declare module "@salesforce/schema/Opportunity.Primary_Atherectomy_Vendor__c" {
  const Primary_Atherectomy_Vendor__c:string;
  export default Primary_Atherectomy_Vendor__c;
}
declare module "@salesforce/schema/Opportunity.GPO_IDN__c" {
  const GPO_IDN__c:string;
  export default GPO_IDN__c;
}
declare module "@salesforce/schema/Opportunity.Physician_Champion__r" {
  const Physician_Champion__r:any;
  export default Physician_Champion__r;
}
declare module "@salesforce/schema/Opportunity.Physician_Champion__c" {
  const Physician_Champion__c:any;
  export default Physician_Champion__c;
}
declare module "@salesforce/schema/Opportunity.Consignment_Need__c" {
  const Consignment_Need__c:string;
  export default Consignment_Need__c;
}
declare module "@salesforce/schema/Opportunity.Create_LTX_018_Opportunity__c" {
  const Create_LTX_018_Opportunity__c:boolean;
  export default Create_LTX_018_Opportunity__c;
}
declare module "@salesforce/schema/Opportunity.Primary_BTK__c" {
  const Primary_BTK__c:string;
  export default Primary_BTK__c;
}
declare module "@salesforce/schema/Opportunity.First_Order__c" {
  const First_Order__c:string;
  export default First_Order__c;
}
declare module "@salesforce/schema/Opportunity.Tier__c" {
  const Tier__c:number;
  export default Tier__c;
}
declare module "@salesforce/schema/Opportunity.Point_of_Care__c" {
  const Point_of_Care__c:string;
  export default Point_of_Care__c;
}
declare module "@salesforce/schema/Opportunity.MD_Coachability__c" {
  const MD_Coachability__c:string;
  export default MD_Coachability__c;
}
declare module "@salesforce/schema/Opportunity.Willing_to_Attend_Training_Course__c" {
  const Willing_to_Attend_Training_Course__c:string;
  export default Willing_to_Attend_Training_Course__c;
}
declare module "@salesforce/schema/Opportunity.Who_Creates_AVF_Surgically__c" {
  const Who_Creates_AVF_Surgically__c:string;
  export default Who_Creates_AVF_Surgically__c;
}
declare module "@salesforce/schema/Opportunity.Who_Manages_AVF_Endovascularly__c" {
  const Who_Manages_AVF_Endovascularly__c:string;
  export default Who_Manages_AVF_Endovascularly__c;
}
declare module "@salesforce/schema/Opportunity.Nephrologist__c" {
  const Nephrologist__c:string;
  export default Nephrologist__c;
}
declare module "@salesforce/schema/Opportunity.Surgical_Collaborator__c" {
  const Surgical_Collaborator__c:string;
  export default Surgical_Collaborator__c;
}
declare module "@salesforce/schema/Opportunity.Cannulation_Champion__c" {
  const Cannulation_Champion__c:string;
  export default Cannulation_Champion__c;
}
declare module "@salesforce/schema/Opportunity.Targeted_Dialysis_Centers__c" {
  const Targeted_Dialysis_Centers__c:string;
  export default Targeted_Dialysis_Centers__c;
}
declare module "@salesforce/schema/Opportunity.Ultrasound_Experience__c" {
  const Ultrasound_Experience__c:string;
  export default Ultrasound_Experience__c;
}
declare module "@salesforce/schema/Opportunity.Vein_Mapping_Lab__c" {
  const Vein_Mapping_Lab__c:string;
  export default Vein_Mapping_Lab__c;
}
declare module "@salesforce/schema/Opportunity.Confidence_in_Conversion__c" {
  const Confidence_in_Conversion__c:string;
  export default Confidence_in_Conversion__c;
}
declare module "@salesforce/schema/Opportunity.Shipped_Initial_Bundle__c" {
  const Shipped_Initial_Bundle__c:boolean;
  export default Shipped_Initial_Bundle__c;
}
declare module "@salesforce/schema/Opportunity.Targeted_Department__c" {
  const Targeted_Department__c:string;
  export default Targeted_Department__c;
}
declare module "@salesforce/schema/Opportunity.Annual_Chronic_Venous_Disease_Volume__c" {
  const Annual_Chronic_Venous_Disease_Volume__c:number;
  export default Annual_Chronic_Venous_Disease_Volume__c;
}
declare module "@salesforce/schema/Opportunity.Annual_Chronic_Venous_Dis_Stent_Volume__c" {
  const Annual_Chronic_Venous_Dis_Stent_Volume__c:number;
  export default Annual_Chronic_Venous_Dis_Stent_Volume__c;
}
declare module "@salesforce/schema/Opportunity.of_Wallstents__c" {
  const of_Wallstents__c:number;
  export default of_Wallstents__c;
}
declare module "@salesforce/schema/Opportunity.Wallstents_Acquired__c" {
  const Wallstents_Acquired__c:string;
  export default Wallstents_Acquired__c;
}
declare module "@salesforce/schema/Opportunity.Unit_Price_Per_WallStent__c" {
  const Unit_Price_Per_WallStent__c:number;
  export default Unit_Price_Per_WallStent__c;
}
declare module "@salesforce/schema/Opportunity.of_XXL_Balloons_On_Shelf__c" {
  const of_XXL_Balloons_On_Shelf__c:number;
  export default of_XXL_Balloons_On_Shelf__c;
}
declare module "@salesforce/schema/Opportunity.BSC_Relationship__c" {
  const BSC_Relationship__c:string;
  export default BSC_Relationship__c;
}
declare module "@salesforce/schema/Opportunity.Venovo_Adoption__c" {
  const Venovo_Adoption__c:string;
  export default Venovo_Adoption__c;
}
declare module "@salesforce/schema/Opportunity.X1_of_Thrombectomy_Procedures__c" {
  const X1_of_Thrombectomy_Procedures__c:number;
  export default X1_of_Thrombectomy_Procedures__c;
}
declare module "@salesforce/schema/Opportunity.X2_Thrombectomy_Products_In_Use__c" {
  const X2_Thrombectomy_Products_In_Use__c:string;
  export default X2_Thrombectomy_Products_In_Use__c;
}
declare module "@salesforce/schema/Opportunity.Elevation_Gauge_Size__c" {
  const Elevation_Gauge_Size__c:string;
  export default Elevation_Gauge_Size__c;
}
declare module "@salesforce/schema/Opportunity.Quote_Type__c" {
  const Quote_Type__c:string;
  export default Quote_Type__c;
}
declare module "@salesforce/schema/Opportunity.Physician_Champion_Email__c" {
  const Physician_Champion_Email__c:string;
  export default Physician_Champion_Email__c;
}
declare module "@salesforce/schema/Opportunity.Day_1_Sales_Estimate__c" {
  const Day_1_Sales_Estimate__c:number;
  export default Day_1_Sales_Estimate__c;
}
declare module "@salesforce/schema/Opportunity.Activity_Comments__c" {
  const Activity_Comments__c:string;
  export default Activity_Comments__c;
}
declare module "@salesforce/schema/Opportunity.Week_1_Sales_Estimate__c" {
  const Week_1_Sales_Estimate__c:number;
  export default Week_1_Sales_Estimate__c;
}
declare module "@salesforce/schema/Opportunity.Month_1_Sales_Estimate__c" {
  const Month_1_Sales_Estimate__c:number;
  export default Month_1_Sales_Estimate__c;
}
declare module "@salesforce/schema/Opportunity.Month_2_Sales_Estimate__c" {
  const Month_2_Sales_Estimate__c:number;
  export default Month_2_Sales_Estimate__c;
}
declare module "@salesforce/schema/Opportunity.Corporate_Credit_Approved__c" {
  const Corporate_Credit_Approved__c:boolean;
  export default Corporate_Credit_Approved__c;
}
declare module "@salesforce/schema/Opportunity.Purchase_Path__c" {
  const Purchase_Path__c:string;
  export default Purchase_Path__c;
}
declare module "@salesforce/schema/Opportunity.Primary_DCB_Vendor__c" {
  const Primary_DCB_Vendor__c:string;
  export default Primary_DCB_Vendor__c;
}
declare module "@salesforce/schema/Opportunity.Physician_Specialty__c" {
  const Physician_Specialty__c:string;
  export default Physician_Specialty__c;
}
declare module "@salesforce/schema/Opportunity.Coil_Plug_Procedures__c" {
  const Coil_Plug_Procedures__c:number;
  export default Coil_Plug_Procedures__c;
}
declare module "@salesforce/schema/Opportunity.Primary_Sonographer__c" {
  const Primary_Sonographer__c:string;
  export default Primary_Sonographer__c;
}
declare module "@salesforce/schema/Opportunity.Lab_Manager__c" {
  const Lab_Manager__c:string;
  export default Lab_Manager__c;
}
declare module "@salesforce/schema/Opportunity.Trauma__c" {
  const Trauma__c:string;
  export default Trauma__c;
}
declare module "@salesforce/schema/Opportunity.GI_Bleeds__c" {
  const GI_Bleeds__c:string;
  export default GI_Bleeds__c;
}
declare module "@salesforce/schema/Opportunity.Endoleaks__c" {
  const Endoleaks__c:string;
  export default Endoleaks__c;
}
declare module "@salesforce/schema/Opportunity.Y90_Mapping__c" {
  const Y90_Mapping__c:string;
  export default Y90_Mapping__c;
}
declare module "@salesforce/schema/Opportunity.PAVMs__c" {
  const PAVMs__c:string;
  export default PAVMs__c;
}
declare module "@salesforce/schema/Opportunity.Stocked_detachable_coils__c" {
  const Stocked_detachable_coils__c:string;
  export default Stocked_detachable_coils__c;
}
declare module "@salesforce/schema/Opportunity.Preferred_detachable_coil__c" {
  const Preferred_detachable_coil__c:string;
  export default Preferred_detachable_coil__c;
}
declare module "@salesforce/schema/Opportunity.Stock_plugs__c" {
  const Stock_plugs__c:string;
  export default Stock_plugs__c;
}
declare module "@salesforce/schema/Opportunity.Preferred_plug__c" {
  const Preferred_plug__c:string;
  export default Preferred_plug__c;
}
declare module "@salesforce/schema/Opportunity.Plugs_acquired__c" {
  const Plugs_acquired__c:string;
  export default Plugs_acquired__c;
}
declare module "@salesforce/schema/Opportunity.Annual_consigned_units__c" {
  const Annual_consigned_units__c:number;
  export default Annual_consigned_units__c;
}
declare module "@salesforce/schema/Opportunity.Unit_Price_per_Amplatzer__c" {
  const Unit_Price_per_Amplatzer__c:number;
  export default Unit_Price_per_Amplatzer__c;
}
declare module "@salesforce/schema/Opportunity.Unit_Price_per_MVP__c" {
  const Unit_Price_per_MVP__c:number;
  export default Unit_Price_per_MVP__c;
}
declare module "@salesforce/schema/Opportunity.First_Purchase__c" {
  const First_Purchase__c:string;
  export default First_Purchase__c;
}
declare module "@salesforce/schema/Opportunity.Specialty__c" {
  const Specialty__c:string;
  export default Specialty__c;
}
declare module "@salesforce/schema/Opportunity.of_Fistulas_by_GS_VS__c" {
  const of_Fistulas_by_GS_VS__c:number;
  export default of_Fistulas_by_GS_VS__c;
}
declare module "@salesforce/schema/Opportunity.of_Dialysis_Catheters__c" {
  const of_Dialysis_Catheters__c:number;
  export default of_Dialysis_Catheters__c;
}
declare module "@salesforce/schema/Opportunity.Patient_Selection__c" {
  const Patient_Selection__c:string;
  export default Patient_Selection__c;
}
declare module "@salesforce/schema/Opportunity.Nephrologist_Commitment__c" {
  const Nephrologist_Commitment__c:boolean;
  export default Nephrologist_Commitment__c;
}
declare module "@salesforce/schema/Opportunity.AVF_Surgeon_Commitment__c" {
  const AVF_Surgeon_Commitment__c:boolean;
  export default AVF_Surgeon_Commitment__c;
}
declare module "@salesforce/schema/Opportunity.Ultrasound_Training_Date__c" {
  const Ultrasound_Training_Date__c:any;
  export default Ultrasound_Training_Date__c;
}
declare module "@salesforce/schema/Opportunity.Ultrasound_Training_Attendees__c" {
  const Ultrasound_Training_Attendees__c:string;
  export default Ultrasound_Training_Attendees__c;
}
declare module "@salesforce/schema/Opportunity.X3_Patients_Screened__c" {
  const X3_Patients_Screened__c:boolean;
  export default X3_Patients_Screened__c;
}
declare module "@salesforce/schema/Opportunity.Dinner_Date__c" {
  const Dinner_Date__c:any;
  export default Dinner_Date__c;
}
declare module "@salesforce/schema/Opportunity.Dinner_Attendees__c" {
  const Dinner_Attendees__c:string;
  export default Dinner_Attendees__c;
}
declare module "@salesforce/schema/Opportunity.Validated_Target__c" {
  const Validated_Target__c:boolean;
  export default Validated_Target__c;
}
declare module "@salesforce/schema/Opportunity.of_Stereotactic_Procedures__c" {
  const of_Stereotactic_Procedures__c:number;
  export default of_Stereotactic_Procedures__c;
}
declare module "@salesforce/schema/Opportunity.Stereotactic_System__c" {
  const Stereotactic_System__c:string;
  export default Stereotactic_System__c;
}
declare module "@salesforce/schema/Opportunity.Microcalcifications_On__c" {
  const Microcalcifications_On__c:string;
  export default Microcalcifications_On__c;
}
declare module "@salesforce/schema/Opportunity.System_Location__c" {
  const System_Location__c:string;
  export default System_Location__c;
}
declare module "@salesforce/schema/Opportunity.Encor_Bundle_Opportunity__c" {
  const Encor_Bundle_Opportunity__c:string;
  export default Encor_Bundle_Opportunity__c;
}
declare module "@salesforce/schema/Opportunity.Guiding_Sheaths_Used__c" {
  const Guiding_Sheaths_Used__c:string;
  export default Guiding_Sheaths_Used__c;
}
declare module "@salesforce/schema/Opportunity.Guiding_Sheath_Price__c" {
  const Guiding_Sheath_Price__c:string;
  export default Guiding_Sheath_Price__c;
}
declare module "@salesforce/schema/Opportunity.Introducer_Sheath_Used__c" {
  const Introducer_Sheath_Used__c:string;
  export default Introducer_Sheath_Used__c;
}
declare module "@salesforce/schema/Opportunity.Introducer_Sheath_Price__c" {
  const Introducer_Sheath_Price__c:string;
  export default Introducer_Sheath_Price__c;
}
declare module "@salesforce/schema/Opportunity.Thin_Walled_Sheath__c" {
  const Thin_Walled_Sheath__c:string;
  export default Thin_Walled_Sheath__c;
}
declare module "@salesforce/schema/Opportunity.Alternative_Access_Site__c" {
  const Alternative_Access_Site__c:string;
  export default Alternative_Access_Site__c;
}
declare module "@salesforce/schema/Opportunity.Closure_Device_for_Procedures__c" {
  const Closure_Device_for_Procedures__c:string;
  export default Closure_Device_for_Procedures__c;
}
declare module "@salesforce/schema/Opportunity.of_Radial_Access__c" {
  const of_Radial_Access__c:number;
  export default of_Radial_Access__c;
}
declare module "@salesforce/schema/Opportunity.Coronary_Radial_Introducer_Sheaths__c" {
  const Coronary_Radial_Introducer_Sheaths__c:string;
  export default Coronary_Radial_Introducer_Sheaths__c;
}
declare module "@salesforce/schema/Opportunity.Guiding_Sheath_Other__c" {
  const Guiding_Sheath_Other__c:string;
  export default Guiding_Sheath_Other__c;
}
declare module "@salesforce/schema/Opportunity.Introducer_Sheath_Other__c" {
  const Introducer_Sheath_Other__c:string;
  export default Introducer_Sheath_Other__c;
}
declare module "@salesforce/schema/Opportunity.Coronary_Sheath_Other__c" {
  const Coronary_Sheath_Other__c:string;
  export default Coronary_Sheath_Other__c;
}
declare module "@salesforce/schema/Opportunity.Targeted_Procedure__c" {
  const Targeted_Procedure__c:string;
  export default Targeted_Procedure__c;
}
declare module "@salesforce/schema/Opportunity.of_Gamma_Detectors_Onsite__c" {
  const of_Gamma_Detectors_Onsite__c:number;
  export default of_Gamma_Detectors_Onsite__c;
}
declare module "@salesforce/schema/Opportunity.of_Disposable_Sleeves_on_Shelf__c" {
  const of_Disposable_Sleeves_on_Shelf__c:number;
  export default of_Disposable_Sleeves_on_Shelf__c;
}
declare module "@salesforce/schema/Opportunity.Competition__c" {
  const Competition__c:string;
  export default Competition__c;
}
declare module "@salesforce/schema/Opportunity.Dialysis_Market_Share__c" {
  const Dialysis_Market_Share__c:number;
  export default Dialysis_Market_Share__c;
}
declare module "@salesforce/schema/Opportunity.Pediatric_Catheter_In_Use__c" {
  const Pediatric_Catheter_In_Use__c:string;
  export default Pediatric_Catheter_In_Use__c;
}
declare module "@salesforce/schema/Opportunity.Pediatric_Catheter_Used_Previously__c" {
  const Pediatric_Catheter_Used_Previously__c:string;
  export default Pediatric_Catheter_Used_Previously__c;
}
declare module "@salesforce/schema/Opportunity.Reason_for_Catheter_Change__c" {
  const Reason_for_Catheter_Change__c:string;
  export default Reason_for_Catheter_Change__c;
}
declare module "@salesforce/schema/Opportunity.Catheter_Placement_Challenges__c" {
  const Catheter_Placement_Challenges__c:string;
  export default Catheter_Placement_Challenges__c;
}
declare module "@salesforce/schema/Opportunity.Catheter_Sizes__c" {
  const Catheter_Sizes__c:string;
  export default Catheter_Sizes__c;
}
declare module "@salesforce/schema/Opportunity.How_to_Add_Glidepath__c" {
  const How_to_Add_Glidepath__c:string;
  export default How_to_Add_Glidepath__c;
}
declare module "@salesforce/schema/Opportunity.What_is_your_involvement__c" {
  const What_is_your_involvement__c:string;
  export default What_is_your_involvement__c;
}
declare module "@salesforce/schema/Opportunity.Frequency_Apheresis_Hemo_line_placement__c" {
  const Frequency_Apheresis_Hemo_line_placement__c:string;
  export default Frequency_Apheresis_Hemo_line_placement__c;
}
declare module "@salesforce/schema/Opportunity.Usage_increase_with_lower_profile__c" {
  const Usage_increase_with_lower_profile__c:string;
  export default Usage_increase_with_lower_profile__c;
}
declare module "@salesforce/schema/Opportunity.Dialysis_Catheter_VAC__c" {
  const Dialysis_Catheter_VAC__c:string;
  export default Dialysis_Catheter_VAC__c;
}
declare module "@salesforce/schema/Opportunity.of_perm_cath_placements__c" {
  const of_perm_cath_placements__c:number;
  export default of_perm_cath_placements__c;
}
declare module "@salesforce/schema/Opportunity.Procedure_location__c" {
  const Procedure_location__c:string;
  export default Procedure_location__c;
}
declare module "@salesforce/schema/Opportunity.Who_places_pediatric_dialysis_catheters__c" {
  const Who_places_pediatric_dialysis_catheters__c:string;
  export default Who_places_pediatric_dialysis_catheters__c;
}
declare module "@salesforce/schema/Opportunity.Placers_Role_in_Decision__c" {
  const Placers_Role_in_Decision__c:string;
  export default Placers_Role_in_Decision__c;
}
declare module "@salesforce/schema/Opportunity.Competitor_Product__c" {
  const Competitor_Product__c:string;
  export default Competitor_Product__c;
}
declare module "@salesforce/schema/Opportunity.Age__c" {
  const Age__c:number;
  export default Age__c;
}
declare module "@salesforce/schema/Opportunity.Targeting_Date__c" {
  const Targeting_Date__c:any;
  export default Targeting_Date__c;
}
declare module "@salesforce/schema/Opportunity.Days_Since_Targeting__c" {
  const Days_Since_Targeting__c:number;
  export default Days_Since_Targeting__c;
}
declare module "@salesforce/schema/Opportunity.Closing_in__c" {
  const Closing_in__c:number;
  export default Closing_in__c;
}
declare module "@salesforce/schema/Opportunity.Reverted_Date__c" {
  const Reverted_Date__c:any;
  export default Reverted_Date__c;
}
declare module "@salesforce/schema/Opportunity.Bone_Marrow_Kits__c" {
  const Bone_Marrow_Kits__c:number;
  export default Bone_Marrow_Kits__c;
}
declare module "@salesforce/schema/Opportunity.Bone_Lesion_Kits__c" {
  const Bone_Lesion_Kits__c:number;
  export default Bone_Lesion_Kits__c;
}
declare module "@salesforce/schema/Opportunity.OnControl_Marrow_Cases__c" {
  const OnControl_Marrow_Cases__c:number;
  export default OnControl_Marrow_Cases__c;
}
declare module "@salesforce/schema/Opportunity.OnControl_Lesion_Cases__c" {
  const OnControl_Lesion_Cases__c:number;
  export default OnControl_Lesion_Cases__c;
}
declare module "@salesforce/schema/Opportunity.Current_Bone_Devices__c" {
  const Current_Bone_Devices__c:string;
  export default Current_Bone_Devices__c;
}
declare module "@salesforce/schema/Opportunity.OnControl_Drivers_Onsite__c" {
  const OnControl_Drivers_Onsite__c:number;
  export default OnControl_Drivers_Onsite__c;
}
declare module "@salesforce/schema/Opportunity.Mix_of_Bone_Lesion_Kits__c" {
  const Mix_of_Bone_Lesion_Kits__c:string;
  export default Mix_of_Bone_Lesion_Kits__c;
}
declare module "@salesforce/schema/Opportunity.units_used_by_Physician_Champion__c" {
  const units_used_by_Physician_Champion__c:number;
  export default units_used_by_Physician_Champion__c;
}
