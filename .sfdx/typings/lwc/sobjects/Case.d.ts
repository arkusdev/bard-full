declare module "@salesforce/schema/Case.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Case.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Case.MasterRecord" {
  const MasterRecord:any;
  export default MasterRecord;
}
declare module "@salesforce/schema/Case.MasterRecordId" {
  const MasterRecordId:any;
  export default MasterRecordId;
}
declare module "@salesforce/schema/Case.CaseNumber" {
  const CaseNumber:string;
  export default CaseNumber;
}
declare module "@salesforce/schema/Case.Contact" {
  const Contact:any;
  export default Contact;
}
declare module "@salesforce/schema/Case.ContactId" {
  const ContactId:any;
  export default ContactId;
}
declare module "@salesforce/schema/Case.Account" {
  const Account:any;
  export default Account;
}
declare module "@salesforce/schema/Case.AccountId" {
  const AccountId:any;
  export default AccountId;
}
declare module "@salesforce/schema/Case.Source" {
  const Source:any;
  export default Source;
}
declare module "@salesforce/schema/Case.SourceId" {
  const SourceId:any;
  export default SourceId;
}
declare module "@salesforce/schema/Case.Community" {
  const Community:any;
  export default Community;
}
declare module "@salesforce/schema/Case.CommunityId" {
  const CommunityId:any;
  export default CommunityId;
}
declare module "@salesforce/schema/Case.SuppliedName" {
  const SuppliedName:string;
  export default SuppliedName;
}
declare module "@salesforce/schema/Case.SuppliedPhone" {
  const SuppliedPhone:string;
  export default SuppliedPhone;
}
declare module "@salesforce/schema/Case.SuppliedCompany" {
  const SuppliedCompany:string;
  export default SuppliedCompany;
}
declare module "@salesforce/schema/Case.Type" {
  const Type:string;
  export default Type;
}
declare module "@salesforce/schema/Case.RecordType" {
  const RecordType:any;
  export default RecordType;
}
declare module "@salesforce/schema/Case.RecordTypeId" {
  const RecordTypeId:any;
  export default RecordTypeId;
}
declare module "@salesforce/schema/Case.Status" {
  const Status:string;
  export default Status;
}
declare module "@salesforce/schema/Case.Reason" {
  const Reason:string;
  export default Reason;
}
declare module "@salesforce/schema/Case.Origin" {
  const Origin:string;
  export default Origin;
}
declare module "@salesforce/schema/Case.Language" {
  const Language:string;
  export default Language;
}
declare module "@salesforce/schema/Case.Subject" {
  const Subject:string;
  export default Subject;
}
declare module "@salesforce/schema/Case.Priority" {
  const Priority:string;
  export default Priority;
}
declare module "@salesforce/schema/Case.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/Case.IsClosed" {
  const IsClosed:boolean;
  export default IsClosed;
}
declare module "@salesforce/schema/Case.ClosedDate" {
  const ClosedDate:any;
  export default ClosedDate;
}
declare module "@salesforce/schema/Case.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Case.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Case.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Case.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Case.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Case.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Case.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Case.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Case.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Case.ContactPhone" {
  const ContactPhone:string;
  export default ContactPhone;
}
declare module "@salesforce/schema/Case.ContactMobile" {
  const ContactMobile:string;
  export default ContactMobile;
}
declare module "@salesforce/schema/Case.ContactEmail" {
  const ContactEmail:string;
  export default ContactEmail;
}
declare module "@salesforce/schema/Case.ContactFax" {
  const ContactFax:string;
  export default ContactFax;
}
declare module "@salesforce/schema/Case.Comments" {
  const Comments:string;
  export default Comments;
}
declare module "@salesforce/schema/Case.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Case.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Case.CreatorFullPhotoUrl" {
  const CreatorFullPhotoUrl:string;
  export default CreatorFullPhotoUrl;
}
declare module "@salesforce/schema/Case.CreatorSmallPhotoUrl" {
  const CreatorSmallPhotoUrl:string;
  export default CreatorSmallPhotoUrl;
}
declare module "@salesforce/schema/Case.CreatorName" {
  const CreatorName:string;
  export default CreatorName;
}
declare module "@salesforce/schema/Case.Product_Group__c" {
  const Product_Group__c:string;
  export default Product_Group__c;
}
declare module "@salesforce/schema/Case.Issue_Identified__c" {
  const Issue_Identified__c:any;
  export default Issue_Identified__c;
}
declare module "@salesforce/schema/Case.Product_Categroy__c" {
  const Product_Categroy__c:string;
  export default Product_Categroy__c;
}
declare module "@salesforce/schema/Case.Sales_Team__c" {
  const Sales_Team__c:string;
  export default Sales_Team__c;
}
declare module "@salesforce/schema/Case.Case_Outcome__c" {
  const Case_Outcome__c:string;
  export default Case_Outcome__c;
}
declare module "@salesforce/schema/Case.Case_Date__c" {
  const Case_Date__c:any;
  export default Case_Date__c;
}
declare module "@salesforce/schema/Case.Product_Group_1__c" {
  const Product_Group_1__c:string;
  export default Product_Group_1__c;
}
declare module "@salesforce/schema/Case.Product_Group_2__c" {
  const Product_Group_2__c:string;
  export default Product_Group_2__c;
}
declare module "@salesforce/schema/Case.Product_Group_3__c" {
  const Product_Group_3__c:string;
  export default Product_Group_3__c;
}
declare module "@salesforce/schema/Case.Product_Group_4__c" {
  const Product_Group_4__c:string;
  export default Product_Group_4__c;
}
declare module "@salesforce/schema/Case.Product_Group_5__c" {
  const Product_Group_5__c:string;
  export default Product_Group_5__c;
}
declare module "@salesforce/schema/Case.Product_Group_6__c" {
  const Product_Group_6__c:string;
  export default Product_Group_6__c;
}
declare module "@salesforce/schema/Case.Product_Group_7__c" {
  const Product_Group_7__c:string;
  export default Product_Group_7__c;
}
declare module "@salesforce/schema/Case.Quantity_1__c" {
  const Quantity_1__c:string;
  export default Quantity_1__c;
}
declare module "@salesforce/schema/Case.Quantity_2__c" {
  const Quantity_2__c:string;
  export default Quantity_2__c;
}
declare module "@salesforce/schema/Case.Quantity_3__c" {
  const Quantity_3__c:string;
  export default Quantity_3__c;
}
declare module "@salesforce/schema/Case.Quantity_4__c" {
  const Quantity_4__c:string;
  export default Quantity_4__c;
}
declare module "@salesforce/schema/Case.Quantity_5__c" {
  const Quantity_5__c:string;
  export default Quantity_5__c;
}
declare module "@salesforce/schema/Case.Quantity_6__c" {
  const Quantity_6__c:string;
  export default Quantity_6__c;
}
declare module "@salesforce/schema/Case.Quantity_7__c" {
  const Quantity_7__c:string;
  export default Quantity_7__c;
}
declare module "@salesforce/schema/Case.Account_Number__c" {
  const Account_Number__c:string;
  export default Account_Number__c;
}
declare module "@salesforce/schema/Case.Total_Cases__c" {
  const Total_Cases__c:number;
  export default Total_Cases__c;
}
declare module "@salesforce/schema/Case.Case_Entry_Days__c" {
  const Case_Entry_Days__c:number;
  export default Case_Entry_Days__c;
}
declare module "@salesforce/schema/Case.Campaign__r" {
  const Campaign__r:any;
  export default Campaign__r;
}
declare module "@salesforce/schema/Case.Campaign__c" {
  const Campaign__c:any;
  export default Campaign__c;
}
declare module "@salesforce/schema/Case.Campaign_Type__c" {
  const Campaign_Type__c:string;
  export default Campaign_Type__c;
}
declare module "@salesforce/schema/Case.Days_Beyond_Compliance__c" {
  const Days_Beyond_Compliance__c:number;
  export default Days_Beyond_Compliance__c;
}
declare module "@salesforce/schema/Case.Products_Used__c" {
  const Products_Used__c:number;
  export default Products_Used__c;
}
declare module "@salesforce/schema/Case.CaseOwnerName__c" {
  const CaseOwnerName__c:string;
  export default CaseOwnerName__c;
}
declare module "@salesforce/schema/Case.Certified_Trainer__r" {
  const Certified_Trainer__r:any;
  export default Certified_Trainer__r;
}
declare module "@salesforce/schema/Case.Certified_Trainer__c" {
  const Certified_Trainer__c:any;
  export default Certified_Trainer__c;
}
declare module "@salesforce/schema/Case.WavelinQ_Brachial_Vein_Coiled__c" {
  const WavelinQ_Brachial_Vein_Coiled__c:string;
  export default WavelinQ_Brachial_Vein_Coiled__c;
}
declare module "@salesforce/schema/Case.WavelinQ_Identify_Dialysis__c" {
  const WavelinQ_Identify_Dialysis__c:string;
  export default WavelinQ_Identify_Dialysis__c;
}
declare module "@salesforce/schema/Case.Avg_Entry_Time__c" {
  const Avg_Entry_Time__c:number;
  export default Avg_Entry_Time__c;
}
declare module "@salesforce/schema/Case.Case_End_Date__c" {
  const Case_End_Date__c:any;
  export default Case_End_Date__c;
}
declare module "@salesforce/schema/Case.WavelinQ_Fistula_Creation_Site__c" {
  const WavelinQ_Fistula_Creation_Site__c:string;
  export default WavelinQ_Fistula_Creation_Site__c;
}
declare module "@salesforce/schema/Case.WavelinQ_Patient_Dialysis_Status__c" {
  const WavelinQ_Patient_Dialysis_Status__c:string;
  export default WavelinQ_Patient_Dialysis_Status__c;
}
declare module "@salesforce/schema/Case.WavelinQ_Signoff__c" {
  const WavelinQ_Signoff__c:boolean;
  export default WavelinQ_Signoff__c;
}
declare module "@salesforce/schema/Case.EFS_Member__c" {
  const EFS_Member__c:string;
  export default EFS_Member__c;
}
declare module "@salesforce/schema/Case.EFS__r" {
  const EFS__r:any;
  export default EFS__r;
}
declare module "@salesforce/schema/Case.EFS__c" {
  const EFS__c:any;
  export default EFS__c;
}
declare module "@salesforce/schema/Case.Opportunity__r" {
  const Opportunity__r:any;
  export default Opportunity__r;
}
declare module "@salesforce/schema/Case.Opportunity__c" {
  const Opportunity__c:any;
  export default Opportunity__c;
}
declare module "@salesforce/schema/Case.EFS_is_Me__c" {
  const EFS_is_Me__c:boolean;
  export default EFS_is_Me__c;
}
declare module "@salesforce/schema/Case.Vascular_District__c" {
  const Vascular_District__c:string;
  export default Vascular_District__c;
}
declare module "@salesforce/schema/Case.Biopsy_DIstrict__c" {
  const Biopsy_DIstrict__c:string;
  export default Biopsy_DIstrict__c;
}
declare module "@salesforce/schema/Case.Peripheral_District__c" {
  const Peripheral_District__c:string;
  export default Peripheral_District__c;
}
declare module "@salesforce/schema/Case.Face_to_Face__c" {
  const Face_to_Face__c:boolean;
  export default Face_to_Face__c;
}
declare module "@salesforce/schema/Case.Case_Contact_Name__c" {
  const Case_Contact_Name__c:string;
  export default Case_Contact_Name__c;
}
declare module "@salesforce/schema/Case.Case_Account_Name__c" {
  const Case_Account_Name__c:string;
  export default Case_Account_Name__c;
}
declare module "@salesforce/schema/Case.Face_to_Face_Logged__c" {
  const Face_to_Face_Logged__c:any;
  export default Face_to_Face_Logged__c;
}
