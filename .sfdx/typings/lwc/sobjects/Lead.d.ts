declare module "@salesforce/schema/Lead.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Lead.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Lead.MasterRecord" {
  const MasterRecord:any;
  export default MasterRecord;
}
declare module "@salesforce/schema/Lead.MasterRecordId" {
  const MasterRecordId:any;
  export default MasterRecordId;
}
declare module "@salesforce/schema/Lead.LastName" {
  const LastName:string;
  export default LastName;
}
declare module "@salesforce/schema/Lead.FirstName" {
  const FirstName:string;
  export default FirstName;
}
declare module "@salesforce/schema/Lead.Salutation" {
  const Salutation:string;
  export default Salutation;
}
declare module "@salesforce/schema/Lead.MiddleName" {
  const MiddleName:string;
  export default MiddleName;
}
declare module "@salesforce/schema/Lead.Suffix" {
  const Suffix:string;
  export default Suffix;
}
declare module "@salesforce/schema/Lead.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Lead.RecordType" {
  const RecordType:any;
  export default RecordType;
}
declare module "@salesforce/schema/Lead.RecordTypeId" {
  const RecordTypeId:any;
  export default RecordTypeId;
}
declare module "@salesforce/schema/Lead.Title" {
  const Title:string;
  export default Title;
}
declare module "@salesforce/schema/Lead.Company" {
  const Company:string;
  export default Company;
}
declare module "@salesforce/schema/Lead.Street" {
  const Street:string;
  export default Street;
}
declare module "@salesforce/schema/Lead.City" {
  const City:string;
  export default City;
}
declare module "@salesforce/schema/Lead.State" {
  const State:string;
  export default State;
}
declare module "@salesforce/schema/Lead.PostalCode" {
  const PostalCode:string;
  export default PostalCode;
}
declare module "@salesforce/schema/Lead.Country" {
  const Country:string;
  export default Country;
}
declare module "@salesforce/schema/Lead.Latitude" {
  const Latitude:number;
  export default Latitude;
}
declare module "@salesforce/schema/Lead.Longitude" {
  const Longitude:number;
  export default Longitude;
}
declare module "@salesforce/schema/Lead.GeocodeAccuracy" {
  const GeocodeAccuracy:string;
  export default GeocodeAccuracy;
}
declare module "@salesforce/schema/Lead.Address" {
  const Address:any;
  export default Address;
}
declare module "@salesforce/schema/Lead.Phone" {
  const Phone:string;
  export default Phone;
}
declare module "@salesforce/schema/Lead.MobilePhone" {
  const MobilePhone:string;
  export default MobilePhone;
}
declare module "@salesforce/schema/Lead.Fax" {
  const Fax:string;
  export default Fax;
}
declare module "@salesforce/schema/Lead.Email" {
  const Email:string;
  export default Email;
}
declare module "@salesforce/schema/Lead.Website" {
  const Website:string;
  export default Website;
}
declare module "@salesforce/schema/Lead.PhotoUrl" {
  const PhotoUrl:string;
  export default PhotoUrl;
}
declare module "@salesforce/schema/Lead.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/Lead.LeadSource" {
  const LeadSource:string;
  export default LeadSource;
}
declare module "@salesforce/schema/Lead.Status" {
  const Status:string;
  export default Status;
}
declare module "@salesforce/schema/Lead.Industry" {
  const Industry:string;
  export default Industry;
}
declare module "@salesforce/schema/Lead.Rating" {
  const Rating:string;
  export default Rating;
}
declare module "@salesforce/schema/Lead.AnnualRevenue" {
  const AnnualRevenue:number;
  export default AnnualRevenue;
}
declare module "@salesforce/schema/Lead.NumberOfEmployees" {
  const NumberOfEmployees:number;
  export default NumberOfEmployees;
}
declare module "@salesforce/schema/Lead.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Lead.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Lead.HasOptedOutOfEmail" {
  const HasOptedOutOfEmail:boolean;
  export default HasOptedOutOfEmail;
}
declare module "@salesforce/schema/Lead.IsConverted" {
  const IsConverted:boolean;
  export default IsConverted;
}
declare module "@salesforce/schema/Lead.ConvertedDate" {
  const ConvertedDate:any;
  export default ConvertedDate;
}
declare module "@salesforce/schema/Lead.ConvertedAccount" {
  const ConvertedAccount:any;
  export default ConvertedAccount;
}
declare module "@salesforce/schema/Lead.ConvertedAccountId" {
  const ConvertedAccountId:any;
  export default ConvertedAccountId;
}
declare module "@salesforce/schema/Lead.ConvertedContact" {
  const ConvertedContact:any;
  export default ConvertedContact;
}
declare module "@salesforce/schema/Lead.ConvertedContactId" {
  const ConvertedContactId:any;
  export default ConvertedContactId;
}
declare module "@salesforce/schema/Lead.ConvertedOpportunity" {
  const ConvertedOpportunity:any;
  export default ConvertedOpportunity;
}
declare module "@salesforce/schema/Lead.ConvertedOpportunityId" {
  const ConvertedOpportunityId:any;
  export default ConvertedOpportunityId;
}
declare module "@salesforce/schema/Lead.IsUnreadByOwner" {
  const IsUnreadByOwner:boolean;
  export default IsUnreadByOwner;
}
declare module "@salesforce/schema/Lead.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Lead.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Lead.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Lead.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Lead.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Lead.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Lead.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Lead.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Lead.DoNotCall" {
  const DoNotCall:boolean;
  export default DoNotCall;
}
declare module "@salesforce/schema/Lead.HasOptedOutOfFax" {
  const HasOptedOutOfFax:boolean;
  export default HasOptedOutOfFax;
}
declare module "@salesforce/schema/Lead.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Lead.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Lead.LastTransferDate" {
  const LastTransferDate:any;
  export default LastTransferDate;
}
declare module "@salesforce/schema/Lead.Jigsaw" {
  const Jigsaw:string;
  export default Jigsaw;
}
declare module "@salesforce/schema/Lead.JigsawContactId" {
  const JigsawContactId:string;
  export default JigsawContactId;
}
declare module "@salesforce/schema/Lead.EmailBouncedReason" {
  const EmailBouncedReason:string;
  export default EmailBouncedReason;
}
declare module "@salesforce/schema/Lead.EmailBouncedDate" {
  const EmailBouncedDate:any;
  export default EmailBouncedDate;
}
declare module "@salesforce/schema/Lead.Individual" {
  const Individual:any;
  export default Individual;
}
declare module "@salesforce/schema/Lead.IndividualId" {
  const IndividualId:any;
  export default IndividualId;
}
declare module "@salesforce/schema/Lead.Archived_Lead__c" {
  const Archived_Lead__c:boolean;
  export default Archived_Lead__c;
}
declare module "@salesforce/schema/Lead.Field_Sales_Divsion__c" {
  const Field_Sales_Divsion__c:string;
  export default Field_Sales_Divsion__c;
}
declare module "@salesforce/schema/Lead.Conference__c" {
  const Conference__c:string;
  export default Conference__c;
}
declare module "@salesforce/schema/Lead.WM4SF3__WalkMe_Engagement_Score__c" {
  const WM4SF3__WalkMe_Engagement_Score__c:string;
  export default WM4SF3__WalkMe_Engagement_Score__c;
}
declare module "@salesforce/schema/Lead.Feeding_Sales__c" {
  const Feeding_Sales__c:number;
  export default Feeding_Sales__c;
}
declare module "@salesforce/schema/Lead.Competitor_Product__c" {
  const Competitor_Product__c:string;
  export default Competitor_Product__c;
}
declare module "@salesforce/schema/Lead.Org1ID__c" {
  const Org1ID__c:string;
  export default Org1ID__c;
}
declare module "@salesforce/schema/Lead.Legacy_Vascular_TM2__c" {
  const Legacy_Vascular_TM2__c:string;
  export default Legacy_Vascular_TM2__c;
}
declare module "@salesforce/schema/Lead.Legacy_BiopsyTM__c" {
  const Legacy_BiopsyTM__c:string;
  export default Legacy_BiopsyTM__c;
}
declare module "@salesforce/schema/Lead.Legacy_Peripheral_TM2__c" {
  const Legacy_Peripheral_TM2__c:string;
  export default Legacy_Peripheral_TM2__c;
}
declare module "@salesforce/schema/Lead.BiopsyTMUpdated__c" {
  const BiopsyTMUpdated__c:boolean;
  export default BiopsyTMUpdated__c;
}
declare module "@salesforce/schema/Lead.VascularTMUpdated__c" {
  const VascularTMUpdated__c:boolean;
  export default VascularTMUpdated__c;
}
declare module "@salesforce/schema/Lead.Competitor_Volumes__c" {
  const Competitor_Volumes__c:number;
  export default Competitor_Volumes__c;
}
declare module "@salesforce/schema/Lead.Competitor__r" {
  const Competitor__r:any;
  export default Competitor__r;
}
declare module "@salesforce/schema/Lead.Competitor__c" {
  const Competitor__c:any;
  export default Competitor__c;
}
declare module "@salesforce/schema/Lead.Department__c" {
  const Department__c:string;
  export default Department__c;
}
declare module "@salesforce/schema/Lead.Facility_Type__c" {
  const Facility_Type__c:string;
  export default Facility_Type__c;
}
declare module "@salesforce/schema/Lead.OBL_Hospital__c" {
  const OBL_Hospital__c:string;
  export default OBL_Hospital__c;
}
declare module "@salesforce/schema/Lead.Badge_ID__c" {
  const Badge_ID__c:string;
  export default Badge_ID__c;
}
declare module "@salesforce/schema/Lead.Capture_By__c" {
  const Capture_By__c:string;
  export default Capture_By__c;
}
declare module "@salesforce/schema/Lead.AV_Graft_Sales__c" {
  const AV_Graft_Sales__c:number;
  export default AV_Graft_Sales__c;
}
declare module "@salesforce/schema/Lead.Category_Classification__c" {
  const Category_Classification__c:string;
  export default Category_Classification__c;
}
declare module "@salesforce/schema/Lead.Degree_1__c" {
  const Degree_1__c:string;
  export default Degree_1__c;
}
declare module "@salesforce/schema/Lead.Degree_2__c" {
  const Degree_2__c:string;
  export default Degree_2__c;
}
declare module "@salesforce/schema/Lead.Dwell_Time__c" {
  const Dwell_Time__c:number;
  export default Dwell_Time__c;
}
declare module "@salesforce/schema/Lead.Follow_Up__c" {
  const Follow_Up__c:string;
  export default Follow_Up__c;
}
declare module "@salesforce/schema/Lead.Lead_ID__c" {
  const Lead_ID__c:string;
  export default Lead_ID__c;
}
declare module "@salesforce/schema/Lead.NPI_Number__c" {
  const NPI_Number__c:string;
  export default NPI_Number__c;
}
declare module "@salesforce/schema/Lead.Purchase_Authorization__c" {
  const Purchase_Authorization__c:string;
  export default Purchase_Authorization__c;
}
declare module "@salesforce/schema/Lead.Purchase_Timeframe__c" {
  const Purchase_Timeframe__c:string;
  export default Purchase_Timeframe__c;
}
declare module "@salesforce/schema/Lead.Registration_Type__c" {
  const Registration_Type__c:string;
  export default Registration_Type__c;
}
declare module "@salesforce/schema/Lead.Specialty__c" {
  const Specialty__c:string;
  export default Specialty__c;
}
declare module "@salesforce/schema/Lead.Capture_Date__c" {
  const Capture_Date__c:string;
  export default Capture_Date__c;
}
declare module "@salesforce/schema/Lead.Account__c" {
  const Account__c:string;
  export default Account__c;
}
declare module "@salesforce/schema/Lead.BiopsyTM__r" {
  const BiopsyTM__r:any;
  export default BiopsyTM__r;
}
declare module "@salesforce/schema/Lead.BiopsyTM__c" {
  const BiopsyTM__c:any;
  export default BiopsyTM__c;
}
declare module "@salesforce/schema/Lead.Days_Since_Last_Activity_Update__c" {
  const Days_Since_Last_Activity_Update__c:number;
  export default Days_Since_Last_Activity_Update__c;
}
declare module "@salesforce/schema/Lead.Fabrics_Felts_Pledgets_Sales__c" {
  const Fabrics_Felts_Pledgets_Sales__c:number;
  export default Fabrics_Felts_Pledgets_Sales__c;
}
declare module "@salesforce/schema/Lead.GPO__c" {
  const GPO__c:string;
  export default GPO__c;
}
declare module "@salesforce/schema/Lead.No_Message_Left__c" {
  const No_Message_Left__c:any;
  export default No_Message_Left__c;
}
declare module "@salesforce/schema/Lead.PV_Graft_Sales__c" {
  const PV_Graft_Sales__c:number;
  export default PV_Graft_Sales__c;
}
declare module "@salesforce/schema/Lead.Potential_Revenue__c" {
  const Potential_Revenue__c:number;
  export default Potential_Revenue__c;
}
declare module "@salesforce/schema/Lead.Product_Discussed__c" {
  const Product_Discussed__c:string;
  export default Product_Discussed__c;
}
declare module "@salesforce/schema/Lead.Vascular_TM2__r" {
  const Vascular_TM2__r:any;
  export default Vascular_TM2__r;
}
declare module "@salesforce/schema/Lead.Vascular_TM2__c" {
  const Vascular_TM2__c:any;
  export default Vascular_TM2__c;
}
declare module "@salesforce/schema/Lead.X2012_Drainage_Sales__c" {
  const X2012_Drainage_Sales__c:number;
  export default X2012_Drainage_Sales__c;
}
declare module "@salesforce/schema/Lead.Peripheral_TM2__r" {
  const Peripheral_TM2__r:any;
  export default Peripheral_TM2__r;
}
declare module "@salesforce/schema/Lead.Peripheral_TM2__c" {
  const Peripheral_TM2__c:any;
  export default Peripheral_TM2__c;
}
declare module "@salesforce/schema/Lead.Aspira_Sales__c" {
  const Aspira_Sales__c:number;
  export default Aspira_Sales__c;
}
