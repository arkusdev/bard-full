declare module "@salesforce/schema/Contact.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Contact.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Contact.MasterRecord" {
  const MasterRecord:any;
  export default MasterRecord;
}
declare module "@salesforce/schema/Contact.MasterRecordId" {
  const MasterRecordId:any;
  export default MasterRecordId;
}
declare module "@salesforce/schema/Contact.Account" {
  const Account:any;
  export default Account;
}
declare module "@salesforce/schema/Contact.AccountId" {
  const AccountId:any;
  export default AccountId;
}
declare module "@salesforce/schema/Contact.LastName" {
  const LastName:string;
  export default LastName;
}
declare module "@salesforce/schema/Contact.FirstName" {
  const FirstName:string;
  export default FirstName;
}
declare module "@salesforce/schema/Contact.Salutation" {
  const Salutation:string;
  export default Salutation;
}
declare module "@salesforce/schema/Contact.MiddleName" {
  const MiddleName:string;
  export default MiddleName;
}
declare module "@salesforce/schema/Contact.Suffix" {
  const Suffix:string;
  export default Suffix;
}
declare module "@salesforce/schema/Contact.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Contact.OtherStreet" {
  const OtherStreet:string;
  export default OtherStreet;
}
declare module "@salesforce/schema/Contact.OtherCity" {
  const OtherCity:string;
  export default OtherCity;
}
declare module "@salesforce/schema/Contact.OtherState" {
  const OtherState:string;
  export default OtherState;
}
declare module "@salesforce/schema/Contact.OtherPostalCode" {
  const OtherPostalCode:string;
  export default OtherPostalCode;
}
declare module "@salesforce/schema/Contact.OtherCountry" {
  const OtherCountry:string;
  export default OtherCountry;
}
declare module "@salesforce/schema/Contact.OtherLatitude" {
  const OtherLatitude:number;
  export default OtherLatitude;
}
declare module "@salesforce/schema/Contact.OtherLongitude" {
  const OtherLongitude:number;
  export default OtherLongitude;
}
declare module "@salesforce/schema/Contact.OtherGeocodeAccuracy" {
  const OtherGeocodeAccuracy:string;
  export default OtherGeocodeAccuracy;
}
declare module "@salesforce/schema/Contact.OtherAddress" {
  const OtherAddress:any;
  export default OtherAddress;
}
declare module "@salesforce/schema/Contact.MailingStreet" {
  const MailingStreet:string;
  export default MailingStreet;
}
declare module "@salesforce/schema/Contact.MailingCity" {
  const MailingCity:string;
  export default MailingCity;
}
declare module "@salesforce/schema/Contact.MailingState" {
  const MailingState:string;
  export default MailingState;
}
declare module "@salesforce/schema/Contact.MailingPostalCode" {
  const MailingPostalCode:string;
  export default MailingPostalCode;
}
declare module "@salesforce/schema/Contact.MailingCountry" {
  const MailingCountry:string;
  export default MailingCountry;
}
declare module "@salesforce/schema/Contact.MailingLatitude" {
  const MailingLatitude:number;
  export default MailingLatitude;
}
declare module "@salesforce/schema/Contact.MailingLongitude" {
  const MailingLongitude:number;
  export default MailingLongitude;
}
declare module "@salesforce/schema/Contact.MailingGeocodeAccuracy" {
  const MailingGeocodeAccuracy:string;
  export default MailingGeocodeAccuracy;
}
declare module "@salesforce/schema/Contact.MailingAddress" {
  const MailingAddress:any;
  export default MailingAddress;
}
declare module "@salesforce/schema/Contact.Phone" {
  const Phone:string;
  export default Phone;
}
declare module "@salesforce/schema/Contact.Fax" {
  const Fax:string;
  export default Fax;
}
declare module "@salesforce/schema/Contact.MobilePhone" {
  const MobilePhone:string;
  export default MobilePhone;
}
declare module "@salesforce/schema/Contact.HomePhone" {
  const HomePhone:string;
  export default HomePhone;
}
declare module "@salesforce/schema/Contact.OtherPhone" {
  const OtherPhone:string;
  export default OtherPhone;
}
declare module "@salesforce/schema/Contact.AssistantPhone" {
  const AssistantPhone:string;
  export default AssistantPhone;
}
declare module "@salesforce/schema/Contact.ReportsTo" {
  const ReportsTo:any;
  export default ReportsTo;
}
declare module "@salesforce/schema/Contact.ReportsToId" {
  const ReportsToId:any;
  export default ReportsToId;
}
declare module "@salesforce/schema/Contact.Email" {
  const Email:string;
  export default Email;
}
declare module "@salesforce/schema/Contact.Title" {
  const Title:string;
  export default Title;
}
declare module "@salesforce/schema/Contact.Department" {
  const Department:string;
  export default Department;
}
declare module "@salesforce/schema/Contact.AssistantName" {
  const AssistantName:string;
  export default AssistantName;
}
declare module "@salesforce/schema/Contact.LeadSource" {
  const LeadSource:string;
  export default LeadSource;
}
declare module "@salesforce/schema/Contact.Birthdate" {
  const Birthdate:any;
  export default Birthdate;
}
declare module "@salesforce/schema/Contact.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/Contact.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Contact.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Contact.HasOptedOutOfEmail" {
  const HasOptedOutOfEmail:boolean;
  export default HasOptedOutOfEmail;
}
declare module "@salesforce/schema/Contact.HasOptedOutOfFax" {
  const HasOptedOutOfFax:boolean;
  export default HasOptedOutOfFax;
}
declare module "@salesforce/schema/Contact.DoNotCall" {
  const DoNotCall:boolean;
  export default DoNotCall;
}
declare module "@salesforce/schema/Contact.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Contact.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Contact.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Contact.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Contact.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Contact.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Contact.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Contact.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Contact.LastCURequestDate" {
  const LastCURequestDate:any;
  export default LastCURequestDate;
}
declare module "@salesforce/schema/Contact.LastCUUpdateDate" {
  const LastCUUpdateDate:any;
  export default LastCUUpdateDate;
}
declare module "@salesforce/schema/Contact.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Contact.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Contact.EmailBouncedReason" {
  const EmailBouncedReason:string;
  export default EmailBouncedReason;
}
declare module "@salesforce/schema/Contact.EmailBouncedDate" {
  const EmailBouncedDate:any;
  export default EmailBouncedDate;
}
declare module "@salesforce/schema/Contact.IsEmailBounced" {
  const IsEmailBounced:boolean;
  export default IsEmailBounced;
}
declare module "@salesforce/schema/Contact.PhotoUrl" {
  const PhotoUrl:string;
  export default PhotoUrl;
}
declare module "@salesforce/schema/Contact.Jigsaw" {
  const Jigsaw:string;
  export default Jigsaw;
}
declare module "@salesforce/schema/Contact.JigsawContactId" {
  const JigsawContactId:string;
  export default JigsawContactId;
}
declare module "@salesforce/schema/Contact.Individual" {
  const Individual:any;
  export default Individual;
}
declare module "@salesforce/schema/Contact.IndividualId" {
  const IndividualId:any;
  export default IndividualId;
}
declare module "@salesforce/schema/Contact.Contact_Bio__c" {
  const Contact_Bio__c:string;
  export default Contact_Bio__c;
}
declare module "@salesforce/schema/Contact.Contact_Photo__c" {
  const Contact_Photo__c:string;
  export default Contact_Photo__c;
}
declare module "@salesforce/schema/Contact.Is_KOL__c" {
  const Is_KOL__c:boolean;
  export default Is_KOL__c;
}
declare module "@salesforce/schema/Contact.Society_Registrations__c" {
  const Society_Registrations__c:string;
  export default Society_Registrations__c;
}
declare module "@salesforce/schema/Contact.Surgeon_Specialty__c" {
  const Surgeon_Specialty__c:string;
  export default Surgeon_Specialty__c;
}
declare module "@salesforce/schema/Contact.Surgical_Technique__c" {
  const Surgical_Technique__c:string;
  export default Surgical_Technique__c;
}
declare module "@salesforce/schema/Contact.Org1ID__c" {
  const Org1ID__c:string;
  export default Org1ID__c;
}
declare module "@salesforce/schema/Contact.Department__c" {
  const Department__c:string;
  export default Department__c;
}
declare module "@salesforce/schema/Contact.Division__c" {
  const Division__c:string;
  export default Division__c;
}
declare module "@salesforce/schema/Contact.Old_ID__c" {
  const Old_ID__c:string;
  export default Old_ID__c;
}
declare module "@salesforce/schema/Contact.NPI__c" {
  const NPI__c:string;
  export default NPI__c;
}
declare module "@salesforce/schema/Contact.NPI_Registry__c" {
  const NPI_Registry__c:string;
  export default NPI_Registry__c;
}
declare module "@salesforce/schema/Contact.CV_on_File__c" {
  const CV_on_File__c:boolean;
  export default CV_on_File__c;
}
declare module "@salesforce/schema/Contact.Global_ID__c" {
  const Global_ID__c:string;
  export default Global_ID__c;
}
declare module "@salesforce/schema/Contact.Tier__c" {
  const Tier__c:string;
  export default Tier__c;
}
declare module "@salesforce/schema/Contact.JDE_Vendor__c" {
  const JDE_Vendor__c:string;
  export default JDE_Vendor__c;
}
declare module "@salesforce/schema/Contact.WM4SF3__WalkMe_Engagement_Score__c" {
  const WM4SF3__WalkMe_Engagement_Score__c:string;
  export default WM4SF3__WalkMe_Engagement_Score__c;
}
declare module "@salesforce/schema/Contact.WavelinQ_Program_Completed__c" {
  const WavelinQ_Program_Completed__c:boolean;
  export default WavelinQ_Program_Completed__c;
}
declare module "@salesforce/schema/Contact.Biopsy_DIstrict__c" {
  const Biopsy_DIstrict__c:string;
  export default Biopsy_DIstrict__c;
}
declare module "@salesforce/schema/Contact.Peripheral_District__c" {
  const Peripheral_District__c:string;
  export default Peripheral_District__c;
}
declare module "@salesforce/schema/Contact.Vascular_District__c" {
  const Vascular_District__c:string;
  export default Vascular_District__c;
}
declare module "@salesforce/schema/Contact.Rotarex_Program_Completed__c" {
  const Rotarex_Program_Completed__c:boolean;
  export default Rotarex_Program_Completed__c;
}
