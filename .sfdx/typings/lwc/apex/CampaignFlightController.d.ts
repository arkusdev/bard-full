declare module "@salesforce/apex/CampaignFlightController.fetchCampaignFlightsByType" {
  export default function fetchCampaignFlightsByType(param: {campaignId: any, type: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignFlightController.fetchCampaignFlight" {
  export default function fetchCampaignFlight(param: {flightId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignFlightController.createNewCampaignFlight" {
  export default function createNewCampaignFlight(param: {campaignId: any, type: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignFlightController.deleteCampaignFlight" {
  export default function deleteCampaignFlight(param: {flightId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignFlightController.saveCampaignFlight" {
  export default function saveCampaignFlight(param: {dto: any}): Promise<any>;
}
