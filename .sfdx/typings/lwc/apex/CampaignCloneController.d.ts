declare module "@salesforce/apex/CampaignCloneController.cloneCampaign" {
  export default function cloneCampaign(param: {campaignId: any, dto: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignCloneController.initListVariables" {
  export default function initListVariables(param: {campaignId: any}): Promise<any>;
}
