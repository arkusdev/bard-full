declare module "@salesforce/apex/CampaignExpensesController.allowAccess" {
  export default function allowAccess(): Promise<any>;
}
