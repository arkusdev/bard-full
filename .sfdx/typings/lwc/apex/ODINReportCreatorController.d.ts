declare module "@salesforce/apex/ODINReportCreatorController.GetReportList" {
  export default function GetReportList(): Promise<any>;
}
