declare module "@salesforce/apex/CampaignTransportationController.fetchCampaignTransportations" {
  export default function fetchCampaignTransportations(param: {campaignId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignTransportationController.fetchCampaignTransportation" {
  export default function fetchCampaignTransportation(param: {transId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignTransportationController.createNewCampaignTransportation" {
  export default function createNewCampaignTransportation(param: {campaignId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignTransportationController.deleteCampaignTransportation" {
  export default function deleteCampaignTransportation(param: {transId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignTransportationController.saveCampaignTransportation" {
  export default function saveCampaignTransportation(param: {dto: any}): Promise<any>;
}
