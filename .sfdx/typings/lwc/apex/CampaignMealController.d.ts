declare module "@salesforce/apex/CampaignMealController.fetchCampaignMealsByType" {
  export default function fetchCampaignMealsByType(param: {campaignId: any, type: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignMealController.fetchCampaignMeal" {
  export default function fetchCampaignMeal(param: {mealId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignMealController.createNewCampaignMeal" {
  export default function createNewCampaignMeal(param: {campaignId: any, type: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignMealController.deleteCampaignMeal" {
  export default function deleteCampaignMeal(param: {mealId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignMealController.saveCampaignMeal" {
  export default function saveCampaignMeal(param: {dto: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignMealController.fetchAllStateOptions" {
  export default function fetchAllStateOptions(): Promise<any>;
}
