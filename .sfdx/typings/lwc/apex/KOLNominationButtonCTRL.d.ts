declare module "@salesforce/apex/KOLNominationButtonCTRL.checkProfiles" {
  export default function checkProfiles(param: {KOLId: any}): Promise<any>;
}
declare module "@salesforce/apex/KOLNominationButtonCTRL.changeKOLStatus" {
  export default function changeKOLStatus(param: {kolInfo: any}): Promise<any>;
}
declare module "@salesforce/apex/KOLNominationButtonCTRL.updateRMCommentsApex" {
  export default function updateRMCommentsApex(param: {kolInfo: any}): Promise<any>;
}
