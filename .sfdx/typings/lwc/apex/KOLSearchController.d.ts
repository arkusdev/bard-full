declare module "@salesforce/apex/KOLSearchController.fetchKOLContacts" {
  export default function fetchKOLContacts(param: {params: any, contractServiceDesc: any}): Promise<any>;
}
declare module "@salesforce/apex/KOLSearchController.fetchProductCategories" {
  export default function fetchProductCategories(param: {divisionName: any}): Promise<any>;
}
declare module "@salesforce/apex/KOLSearchController.fetchLicensedStates" {
  export default function fetchLicensedStates(param: {regionNames: any}): Promise<any>;
}
declare module "@salesforce/apex/KOLSearchController.fetchPickListValues" {
  export default function fetchPickListValues(param: {fieldNames: any}): Promise<any>;
}
