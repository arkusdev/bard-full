declare module "@salesforce/apex/CustomerAndProductGapController.getCalculations" {
  export default function getCalculations(param: {userId: any, period: any, view: any, accountID: any, distributor: any, topUser: any}): Promise<any>;
}
