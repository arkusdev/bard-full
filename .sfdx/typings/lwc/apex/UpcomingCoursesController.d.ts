declare module "@salesforce/apex/UpcomingCoursesController.getContactFields" {
  export default function getContactFields(param: {contactId: any}): Promise<any>;
}
declare module "@salesforce/apex/UpcomingCoursesController.fetchproductCategories" {
  export default function fetchproductCategories(param: {divisionName: any}): Promise<any>;
}
declare module "@salesforce/apex/UpcomingCoursesController.fetchPickListValues" {
  export default function fetchPickListValues(param: {fieldNames: any}): Promise<any>;
}
declare module "@salesforce/apex/UpcomingCoursesController.fetchUpcomingCampaignsByContact" {
  export default function fetchUpcomingCampaignsByContact(param: {contactId: any}): Promise<any>;
}
declare module "@salesforce/apex/UpcomingCoursesController.fetchUpcomingCampaigns" {
  export default function fetchUpcomingCampaigns(param: {contactId: any, params: any}): Promise<any>;
}
declare module "@salesforce/apex/UpcomingCoursesController.addMember" {
  export default function addMember(param: {campaignId: any, memberId: any, type: any}): Promise<any>;
}
declare module "@salesforce/apex/UpcomingCoursesController.removeMember" {
  export default function removeMember(param: {campaignId: any, memberId: any, type: any}): Promise<any>;
}
