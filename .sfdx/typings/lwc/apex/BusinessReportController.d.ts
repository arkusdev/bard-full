declare module "@salesforce/apex/BusinessReportController.getOpportunities" {
  export default function getOpportunities(): Promise<any>;
}
declare module "@salesforce/apex/BusinessReportController.getSales" {
  export default function getSales(param: {closeDate: any, accountId: any}): Promise<any>;
}
