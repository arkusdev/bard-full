declare module "@salesforce/apex/IRSASalesSummaryController.getPGResponse" {
  export default function getPGResponse(param: {account: any, userId: any}): Promise<any>;
}
declare module "@salesforce/apex/IRSASalesSummaryController.getUserHierarchy" {
  export default function getUserHierarchy(param: {userId: any}): Promise<any>;
}
declare module "@salesforce/apex/IRSASalesSummaryController.getWeekRanges" {
  export default function getWeekRanges(): Promise<any>;
}
declare module "@salesforce/apex/IRSASalesSummaryController.getAGResponse" {
  export default function getAGResponse(param: {account: any, timeRange: any, userId: any, product: any}): Promise<any>;
}
declare module "@salesforce/apex/IRSASalesSummaryController.getTerritories" {
  export default function getTerritories(): Promise<any>;
}
declare module "@salesforce/apex/IRSASalesSummaryController.getUsers" {
  export default function getUsers(): Promise<any>;
}
declare module "@salesforce/apex/IRSASalesSummaryController.getAccounts" {
  export default function getAccounts(): Promise<any>;
}
