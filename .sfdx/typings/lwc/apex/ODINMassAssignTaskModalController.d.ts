declare module "@salesforce/apex/ODINMassAssignTaskModalController.GetTeam" {
  export default function GetTeam(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/ODINMassAssignTaskModalController.Save" {
  export default function Save(param: {task: any, roles: any}): Promise<any>;
}
declare module "@salesforce/apex/ODINMassAssignTaskModalController.getTaskFormInfo" {
  export default function getTaskFormInfo(param: {projectId: any, team: any}): Promise<any>;
}
declare module "@salesforce/apex/ODINMassAssignTaskModalController.getTableData" {
  export default function getTableData(): Promise<any>;
}
