declare module "@salesforce/apex/PerDaySalesController.getCalculations" {
  export default function getCalculations(param: {userId: any, period: any, distributor: any}): Promise<any>;
}
