declare module "@salesforce/apex/CampaignMemberController.hasAvailableSeats" {
  export default function hasAvailableSeats(param: {campaignId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignMemberController.fetchAllCampaignMembers" {
  export default function fetchAllCampaignMembers(param: {campaignId: any, type: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignMemberController.addMember" {
  export default function addMember(param: {campaignId: any, memberId: any, type: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignMemberController.updateMember" {
  export default function updateMember(param: {campaignId: any, memberId: any, type: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignMemberController.removeMember" {
  export default function removeMember(param: {campaignId: any, memberId: any, type: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignMemberController.hasExpenseItems" {
  export default function hasExpenseItems(param: {campaignId: any, memberId: any}): Promise<any>;
}
