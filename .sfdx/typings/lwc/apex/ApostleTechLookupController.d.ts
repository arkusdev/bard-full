declare module "@salesforce/apex/ApostleTechLookupController.fetchLookUpValues" {
  export default function fetchLookUpValues(param: {searchKeyword: any, objName: any}): Promise<any>;
}
declare module "@salesforce/apex/ApostleTechLookupController.fetchLookUpValues" {
  export default function fetchLookUpValues(param: {searchKeyword: any, objName: any, omitIdList: any}): Promise<any>;
}
