declare module "@salesforce/apex/CampaignAttendeeDocumentController.fetchCampaignAttendeeDocuments" {
  export default function fetchCampaignAttendeeDocuments(param: {attendeeId: any, campaignId: any, type: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignAttendeeDocumentController.saveCampaignAttendeeDocuments" {
  export default function saveCampaignAttendeeDocuments(param: {dto: any}): Promise<any>;
}
