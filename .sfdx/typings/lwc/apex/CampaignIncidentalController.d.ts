declare module "@salesforce/apex/CampaignIncidentalController.fetchCampaignIncidentals" {
  export default function fetchCampaignIncidentals(param: {campaignId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignIncidentalController.fetchCampaignIncidental" {
  export default function fetchCampaignIncidental(param: {incidentalId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignIncidentalController.createNewCampaignIncidental" {
  export default function createNewCampaignIncidental(param: {campaignId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignIncidentalController.deleteCampaignIncidental" {
  export default function deleteCampaignIncidental(param: {incidentalId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignIncidentalController.saveCampaignIncidental" {
  export default function saveCampaignIncidental(param: {dto: any}): Promise<any>;
}
