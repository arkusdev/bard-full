declare module "@salesforce/apex/CampaignLedgerController.fetchSummaryTotals" {
  export default function fetchSummaryTotals(param: {campaignId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignLedgerController.fetchDetailTotals" {
  export default function fetchDetailTotals(param: {campaignId: any}): Promise<any>;
}
