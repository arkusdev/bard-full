declare module "@salesforce/apex/CampaignProctorFeeController.fetchCampaignProctorFees" {
  export default function fetchCampaignProctorFees(param: {campaignId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignProctorFeeController.fetchCampaignProctorFee" {
  export default function fetchCampaignProctorFee(param: {feeId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignProctorFeeController.createNewCampaignProctorFee" {
  export default function createNewCampaignProctorFee(param: {campaignId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignProctorFeeController.deleteCampaignProctorFee" {
  export default function deleteCampaignProctorFee(param: {feeId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignProctorFeeController.saveCampaignProctorFee" {
  export default function saveCampaignProctorFee(param: {dto: any}): Promise<any>;
}
