declare module "@salesforce/apex/ODINProductModalController.GetProductTypes" {
  export default function GetProductTypes(): Promise<any>;
}
declare module "@salesforce/apex/ODINProductModalController.GetProducts" {
  export default function GetProducts(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/ODINProductModalController.GetFinancials" {
  export default function GetFinancials(param: {recordId: any, productId: any}): Promise<any>;
}
declare module "@salesforce/apex/ODINProductModalController.CreateProduct" {
  export default function CreateProduct(param: {recordId: any, productName: any, type: any, sampleFinancial: any}): Promise<any>;
}
declare module "@salesforce/apex/ODINProductModalController.SaveFinancials" {
  export default function SaveFinancials(param: {financials: any, product: any}): Promise<any>;
}
declare module "@salesforce/apex/ODINProductModalController.UpdateFinancials" {
  export default function UpdateFinancials(param: {financials: any}): Promise<any>;
}
