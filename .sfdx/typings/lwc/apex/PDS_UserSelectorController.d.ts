declare module "@salesforce/apex/PDS_UserSelectorController.getUsers" {
  export default function getUsers(param: {userId: any}): Promise<any>;
}
