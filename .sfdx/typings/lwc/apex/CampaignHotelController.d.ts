declare module "@salesforce/apex/CampaignHotelController.fetchCampaignHotels" {
  export default function fetchCampaignHotels(param: {campaignId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignHotelController.fetchCampaignHotel" {
  export default function fetchCampaignHotel(param: {hotelId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignHotelController.createNewCampaignHotel" {
  export default function createNewCampaignHotel(param: {campaignId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignHotelController.deleteCampaignHotel" {
  export default function deleteCampaignHotel(param: {hotelId: any}): Promise<any>;
}
declare module "@salesforce/apex/CampaignHotelController.saveCampaignHotel" {
  export default function saveCampaignHotel(param: {dto: any}): Promise<any>;
}
