declare module "@salesforce/apex/ODINFinancialModalController.GetRegionOptions" {
  export default function GetRegionOptions(param: {projectId: any}): Promise<any>;
}
declare module "@salesforce/apex/ODINFinancialModalController.GetInterestOptions" {
  export default function GetInterestOptions(param: {recordId: any, region: any}): Promise<any>;
}
declare module "@salesforce/apex/ODINFinancialModalController.GetProjectQuestions" {
  export default function GetProjectQuestions(param: {recordId: any, region: any}): Promise<any>;
}
declare module "@salesforce/apex/ODINFinancialModalController.GetFinancialInfo" {
  export default function GetFinancialInfo(param: {recordId: any, region: any}): Promise<any>;
}
declare module "@salesforce/apex/ODINFinancialModalController.CreateFinancialInfo" {
  export default function CreateFinancialInfo(param: {recordId: any, region: any, jsonWrapper: any}): Promise<any>;
}
declare module "@salesforce/apex/ODINFinancialModalController.UpdateFinancialInfo" {
  export default function UpdateFinancialInfo(param: {jsonWrapper: any, interest: any, answers: any}): Promise<any>;
}
declare module "@salesforce/apex/ODINFinancialModalController.SaveInterest" {
  export default function SaveInterest(param: {interest: any}): Promise<any>;
}
declare module "@salesforce/apex/ODINFinancialModalController.getFinancialInfoByProjectAndRegion" {
  export default function getFinancialInfoByProjectAndRegion(param: {recordId: any, region: any}): Promise<any>;
}
