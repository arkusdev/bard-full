declare module "@salesforce/apex/ODINGlobalFinancialWizzardController.GetProjectInfo" {
  export default function GetProjectInfo(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/ODINGlobalFinancialWizzardController.GetInterestWYear" {
  export default function GetInterestWYear(param: {recordId: any, country: any}): Promise<any>;
}
declare module "@salesforce/apex/ODINGlobalFinancialWizzardController.GetFinancialInfo" {
  export default function GetFinancialInfo(param: {recordId: any, country: any, fromViewAll: any}): Promise<any>;
}
declare module "@salesforce/apex/ODINGlobalFinancialWizzardController.CreateFinancialInfo" {
  export default function CreateFinancialInfo(param: {sampleFinancial: any, financials: any}): Promise<any>;
}
declare module "@salesforce/apex/ODINGlobalFinancialWizzardController.GetProjectQuestions" {
  export default function GetProjectQuestions(param: {recordId: any, country: any, region: any}): Promise<any>;
}
declare module "@salesforce/apex/ODINGlobalFinancialWizzardController.FinalSave" {
  export default function FinalSave(param: {financialJson: any, answers: any, interest: any, sampleFinancial: any}): Promise<any>;
}
declare module "@salesforce/apex/ODINGlobalFinancialWizzardController.SaveInterest" {
  export default function SaveInterest(param: {interest: any}): Promise<any>;
}
declare module "@salesforce/apex/ODINGlobalFinancialWizzardController.getFinancialInfoByProjectAndCountry" {
  export default function getFinancialInfoByProjectAndCountry(param: {recordId: any, financial: any, country: any, region: any}): Promise<any>;
}
